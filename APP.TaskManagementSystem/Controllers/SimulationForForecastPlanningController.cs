﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SimulationForForecastPlanningController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        public SimulationForForecastPlanningController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetSimulationForForecastPlanning")]
        public List<ProductionSimulationGroupingModel> GetSimulationForForecastPlanning(long? id)
        {
            List<ProductionSimulationGroupingModel> productionSimulationGroupingModels = new List<ProductionSimulationGroupingModel>();
            var productionSimulationGrouping = _context.ProductionSimulationGrouping
                .Include(a => a.Item)
                .Include(b => b.ModifiedByUser)
                .Include(c => c.AddedByUser)
                .Include(d => d.StatusCode)
                .Include(e => e.TypeOfProdOrder)
                .Include(f => f.SellingStatus)
                .Include(g => g.ProductionSimulation)
                .Include(h => h.Company)
                .Include(i => i.Item.ProductGroupingNav)
                .Where(w => w.MethodCodeId == id)
                .ToList();
            var genericCodeSupplyToMultiple = _context.GenericCodeSupplyToMultiple.Include(g => g.GenericCode).ToList();

            productionSimulationGrouping.ForEach(s =>
            {
                var genericcodeId = s.Item?.ProductGroupingNav.First()?.GenericCodeSupplyToMultipleId;
                ProductionSimulationGroupingModel productionSimulationGroupingModel = new ProductionSimulationGroupingModel();
                productionSimulationGroupingModel.ProductionSimulationGroupingId = s.ProductionSimulationGroupingId;
                productionSimulationGroupingModel.ProductionSimulationId = s.ProductionSimulationId;
                productionSimulationGroupingModel.ProdOrderNo = s.ProdOrderNo;
                productionSimulationGroupingModel.ItemId = s.ItemId;
                productionSimulationGroupingModel.ItemNo = s.Item?.No;
                productionSimulationGroupingModel.Description = s.Description;
                productionSimulationGroupingModel.PackSize = s.PackSize;
                productionSimulationGroupingModel.Quantity = s.Quantity;
                productionSimulationGroupingModel.Uom = s.Uom;
                productionSimulationGroupingModel.PerQuantity = s.PerQuantity;
                productionSimulationGroupingModel.PerQtyUom = s.PerQtyUom;
                productionSimulationGroupingModel.BatchNo = s.BatchNo;
                productionSimulationGroupingModel.CompanyId = s.CompanyId;
                productionSimulationGroupingModel.StartingDate = s.StartingDate;
                productionSimulationGroupingModel.StatusCodeID = s.StatusCodeId;
                productionSimulationGroupingModel.AddedByUserID = s.AddedByUserId;
                productionSimulationGroupingModel.AddedDate = s.AddedDate;
                productionSimulationGroupingModel.ModifiedByUserID = s.ModifiedByUserId;
                productionSimulationGroupingModel.ModifiedDate = s.ModifiedDate;
                productionSimulationGroupingModel.CompanyId = s.CompanyId;
                productionSimulationGroupingModel.PlannedQty = s.PlannedQty;
                productionSimulationGroupingModel.OutputQty = s.OutputQty;
                productionSimulationGroupingModel.IsOutput = s.IsOutput;
                productionSimulationGroupingModel.ProcessDate = s.ProcessDate;
                productionSimulationGroupingModel.RePlanRefNo = s.RePlanRefNo;
                productionSimulationGroupingModel.BatchSize = s.BatchSize;
                productionSimulationGroupingModel.IsBmrticket = s.IsBmrticket;
                productionSimulationGroupingModel.Description2 = s.Description2;
                productionSimulationGroupingModel.TypeOfProdOrderId = s.TypeOfProdOrderId;
                productionSimulationGroupingModel.SellingStatusId = s.SellingStatusId;
                productionSimulationGroupingModel.PortalStatusNo = s.PortalStatusNo;
                productionSimulationGroupingModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                productionSimulationGroupingModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                productionSimulationGroupingModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                productionSimulationGroupingModel.TypeOfProdOrder = s.TypeOfProdOrder?.Value;
                productionSimulationGroupingModel.SellingStatus = s.SellingStatus?.Value;
                productionSimulationGroupingModel.MethodCodeId = s.MethodCodeId;
                productionSimulationGroupingModel.Dispense = s.Dispense;
                productionSimulationGroupingModel.GenericCode = genericCodeSupplyToMultiple?.FirstOrDefault(f => f.GenericCodeSupplyToMultipleId == genericcodeId)?.GenericCode?.Code;
                productionSimulationGroupingModels.Add(productionSimulationGroupingModel);
            });
            return productionSimulationGroupingModels;
        }
        [HttpPost]
        [Route("GetSimulationForForecastPlanningDate")]
        public List<ProductionSimulationGroupingModel> GetSimulationForForecastPlanningDate(DateRangeModel model)
        {
            List<ProductionSimulationGroupingModel> productionSimulationGroupingModels = new List<ProductionSimulationGroupingModel>();
            var productionSimulationGrouping = _context.OrderRequirementGroupingLine
                 .Include(a => a.ProductionSimulationGrouping)
                 .Include(b => b.Product)
                 .Include(e => e.ModifiedByUser)
                .Include(c => c.AddedByUser)
                .Include(d => d.StatusCode)
                .Include(e => e.ProductionSimulationGrouping.StatusCode)
                .Include(e => e.ProductionSimulationGrouping.TypeOfProdOrder)
                .Include(f => f.ProductionSimulationGrouping.SellingStatus)
                .Include(b => b.Product.ProductGroupingNav)
                 .Where(w => w.ProductionSimulationGrouping.MethodCodeId == model.MethodCodeId);
            List<long?> ids = new List<long?>() { 177, 178 };
            var applicationDetails = _context.ApplicationMasterDetail.Include(a => a.ApplicationMaster).Where(w => ids.Contains(w.ApplicationMaster.ApplicationMasterCodeId)).ToList();
            var productionSimulationGroupingHead = _context.ProductionSimulationGrouping
                .Include(a => a.Item)
                .Include(b => b.ModifiedByUser)
                .Include(c => c.AddedByUser)
                .Include(d => d.StatusCode)
                .Include(e => e.TypeOfProdOrder)
                .Include(f => f.SellingStatus)
                .Include(g => g.ProductionSimulation)
                .Include(h => h.Company)
                .Include(i => i.OrderRequirementGroupingLine)
                .Where(w => w.MethodCodeId == model.MethodCodeId && w.OrderRequirementGroupingLine.Count==0).ToList();
            if (model.NavItemIds != null && model.NavItemIds.Count > 0)
            {
                productionSimulationGrouping = productionSimulationGrouping.Where(w => model.NavItemIds.Contains(w.ProductId));
            }
            if (model.StartDate != null)
            {
                productionSimulationGrouping = productionSimulationGrouping.Where(w => w.ExpectedStartDate.Value.Date >= model.StartDate.Value.Date);
            }
            if (model.EndDate != null)
            {
                productionSimulationGrouping = productionSimulationGrouping.Where(w => w.ExpectedStartDate.Value.Date >= model.EndDate.Value.Date);
            }
            var genericCodeSupplyToMultiple = _context.GenericCodeSupplyToMultiple.Include(g => g.GenericCode).ToList();
            var productionSimulationGroupings = productionSimulationGrouping.ToList();
            productionSimulationGroupings.ForEach(s =>
            {
                var genericcodeId = s.Product?.ProductGroupingNav.First()?.GenericCodeSupplyToMultipleId;
                ProductionSimulationGroupingModel productionSimulationGroupingModel = new ProductionSimulationGroupingModel();
                productionSimulationGroupingModel.ProductionSimulationGroupingId = s.ProductionSimulationGroupingId.Value;
                productionSimulationGroupingModel.ProductionSimulationId = s.ProductionSimulationGrouping?.ProductionSimulationId;
                productionSimulationGroupingModel.ProdOrderNo = s.ProductionSimulationGrouping?.ProdOrderNo;
                productionSimulationGroupingModel.ItemId = s.ProductId;
                productionSimulationGroupingModel.ItemNo = s.Product?.No;
                productionSimulationGroupingModel.Description = s.Product?.Description;
                productionSimulationGroupingModel.PackSize = s.TicketBatchSizeId;
                productionSimulationGroupingModel.Quantity = s.ProductQty;
                productionSimulationGroupingModel.Uom = s.Product?.BaseUnitofMeasure;
                productionSimulationGroupingModel.PerQuantity = s.ProductQty;
                productionSimulationGroupingModel.PerQtyUom = applicationDetails != null ? applicationDetails.FirstOrDefault(w => w.ApplicationMasterDetailId == s.NavUomid)?.Value : "";
                productionSimulationGroupingModel.BatchNo = s.Product?.BatchNos;
                productionSimulationGroupingModel.CompanyId = s.ProductionSimulationGrouping?.CompanyId;
                productionSimulationGroupingModel.StartingDate = s.ExpectedStartDate;
                productionSimulationGroupingModel.ActualStartDate = s.ExpectedStartDate;
                productionSimulationGroupingModel.StatusCodeID = s.ProductionSimulationGrouping?.StatusCodeId;
                productionSimulationGroupingModel.AddedByUserID = s.AddedByUserId;
                productionSimulationGroupingModel.AddedDate = s.AddedDate;
                productionSimulationGroupingModel.ModifiedByUserID = s.ModifiedByUserId;
                productionSimulationGroupingModel.ModifiedDate = s.ModifiedDate;
                productionSimulationGroupingModel.InternalRef = s.Product?.InternalRef;
                productionSimulationGroupingModel.ShelfLife = s.Product?.ShelfLife;
                productionSimulationGroupingModel.LocationCode = applicationDetails != null ? applicationDetails.FirstOrDefault(w => w.ApplicationMasterDetailId == s.NavLocationId)?.Value : "";
                productionSimulationGroupingModel.PlannedQty = s.ProductionSimulationGrouping?.PlannedQty;
                productionSimulationGroupingModel.OutputQty = s.ProductionSimulationGrouping?.OutputQty;
                productionSimulationGroupingModel.IsOutput = s.ProductionSimulationGrouping?.IsOutput;
                productionSimulationGroupingModel.ProcessDate = s.ProductionSimulationGrouping?.ProcessDate;
                productionSimulationGroupingModel.RePlanRefNo = s.ProductionSimulationGrouping?.RePlanRefNo;
                productionSimulationGroupingModel.BatchSize = s.TicketBatchSizeId;
                productionSimulationGroupingModel.IsBmrticket = s.ProductionSimulationGrouping?.IsBmrticket;
                productionSimulationGroupingModel.Description2 = s.Product?.Description2;
                productionSimulationGroupingModel.TypeOfProdOrderId = s.ProductionSimulationGrouping?.TypeOfProdOrderId;
                productionSimulationGroupingModel.SellingStatusId = s.ProductionSimulationGrouping?.SellingStatusId;
                productionSimulationGroupingModel.PortalStatusNo = s.ProductionSimulationGrouping?.PortalStatusNo;
                productionSimulationGroupingModel.StatusCode = s.ProductionSimulationGrouping?.StatusCode != null ? s.ProductionSimulationGrouping?.StatusCode.CodeValue : "";
                productionSimulationGroupingModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                productionSimulationGroupingModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                productionSimulationGroupingModel.TypeOfProdOrder = s.ProductionSimulationGrouping?.TypeOfProdOrder?.Value;
                productionSimulationGroupingModel.SellingStatus = s.ProductionSimulationGrouping?.SellingStatus?.Value;
                productionSimulationGroupingModel.MethodCodeId = s.ProductionSimulationGrouping?.MethodCodeId;
                productionSimulationGroupingModel.Dispense = s.ProductionSimulationGrouping?.Dispense;
                productionSimulationGroupingModel.Remarks = s.Remarks;
                productionSimulationGroupingModel.GenericCode = genericCodeSupplyToMultiple?.FirstOrDefault(f => f.GenericCodeSupplyToMultipleId == genericcodeId)?.GenericCode?.Code;
                productionSimulationGroupingModels.Add(productionSimulationGroupingModel);
            });
            productionSimulationGroupingHead.ForEach(s =>
            {
               // var genericcodeId = s.Item?.ProductGroupingNav.First()?.GenericCodeSupplyToMultipleId;
                ProductionSimulationGroupingModel productionSimulationGroupingModel = new ProductionSimulationGroupingModel();
                productionSimulationGroupingModel.ProductionSimulationGroupingId = s.ProductionSimulationGroupingId;
                productionSimulationGroupingModel.ProductionSimulationId = s.ProductionSimulationId;
                productionSimulationGroupingModel.ProdOrderNo = s.ProdOrderNo;
                productionSimulationGroupingModel.ItemId = s.ItemId;
                productionSimulationGroupingModel.ItemNo = s.Item?.No;
                productionSimulationGroupingModel.Description = s.Description;
                productionSimulationGroupingModel.PackSize = s.PackSize;
                productionSimulationGroupingModel.Quantity = s.Quantity;
                productionSimulationGroupingModel.Uom = s.Uom;
                productionSimulationGroupingModel.PerQuantity = s.PerQuantity;
                productionSimulationGroupingModel.PerQtyUom = s.PerQtyUom;
                productionSimulationGroupingModel.BatchNo = s.BatchNo;
                productionSimulationGroupingModel.CompanyId = s.CompanyId;
                productionSimulationGroupingModel.StartingDate = s.StartingDate;
                productionSimulationGroupingModel.StatusCodeID = s.StatusCodeId;
                productionSimulationGroupingModel.AddedByUserID = s.AddedByUserId;
                productionSimulationGroupingModel.AddedDate = s.AddedDate;
                productionSimulationGroupingModel.ModifiedByUserID = s.ModifiedByUserId;
                productionSimulationGroupingModel.ModifiedDate = s.ModifiedDate;
                productionSimulationGroupingModel.CompanyId = s.CompanyId;
                productionSimulationGroupingModel.PlannedQty = s.PlannedQty;
                productionSimulationGroupingModel.OutputQty = s.OutputQty;
                productionSimulationGroupingModel.IsOutput = s.IsOutput;
                productionSimulationGroupingModel.ProcessDate = s.ProcessDate;
                productionSimulationGroupingModel.RePlanRefNo = s.RePlanRefNo;
                productionSimulationGroupingModel.BatchSize = s.BatchSize;
                productionSimulationGroupingModel.IsBmrticket = s.IsBmrticket;
                productionSimulationGroupingModel.Description2 = s.Description2;
                productionSimulationGroupingModel.TypeOfProdOrderId = s.TypeOfProdOrderId;
                productionSimulationGroupingModel.SellingStatusId = s.SellingStatusId;
                productionSimulationGroupingModel.PortalStatusNo = s.PortalStatusNo;
                productionSimulationGroupingModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                productionSimulationGroupingModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                productionSimulationGroupingModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                productionSimulationGroupingModel.TypeOfProdOrder = s.TypeOfProdOrder?.Value;
                productionSimulationGroupingModel.SellingStatus = s.SellingStatus?.Value;
                productionSimulationGroupingModel.MethodCodeId = s.MethodCodeId;
                productionSimulationGroupingModel.Dispense = s.Dispense;
                //productionSimulationGroupingModel.GenericCode = genericCodeSupplyToMultiple?.FirstOrDefault(f => f.GenericCodeSupplyToMultipleId == genericcodeId)?.GenericCode?.Code;
                productionSimulationGroupingModels.Add(productionSimulationGroupingModel);
            });
            return productionSimulationGroupingModels;
        }
        [HttpGet]
        [Route("GetSimulationForForecastPlanningByID")]
        public List<ProductionSimulationGroupingModel> GetSimulationForForecastPlanningByID(long? id)
        {
            List<ProductionSimulationGroupingModel> productionSimulationGroupingModels = new List<ProductionSimulationGroupingModel>();
            var productionSimulationGrouping = _context.ProductionSimulationGrouping
                .Include(a => a.Item)
                .Include(b => b.ModifiedByUser)
                .Include(c => c.AddedByUser)
                .Include(d => d.StatusCode)
                .Include(e => e.TypeOfProdOrder)
                .Include(f => f.SellingStatus)
                .Include(g => g.ProductionSimulation)
                .Include(h => h.Company)
                .Include(i => i.Item.ProductGroupingNav)
                .Where(w => w.ProductionSimulationGroupingId == id)
                .ToList();
            var genericCodeSupplyToMultiple = _context.GenericCodeSupplyToMultiple.Include(g => g.GenericCode).ToList();

            productionSimulationGrouping.ForEach(s =>
            {
                var genericcodeId = s.Item?.ProductGroupingNav.First()?.GenericCodeSupplyToMultipleId;
                ProductionSimulationGroupingModel productionSimulationGroupingModel = new ProductionSimulationGroupingModel();
                productionSimulationGroupingModel.ProductionSimulationGroupingId = s.ProductionSimulationGroupingId;
                productionSimulationGroupingModel.ProductionSimulationId = s.ProductionSimulationId;
                productionSimulationGroupingModel.ProdOrderNo = s.ProdOrderNo;
                productionSimulationGroupingModel.ItemId = s.ItemId;
                productionSimulationGroupingModel.ItemNo = s.Item?.No;
                productionSimulationGroupingModel.Description = s.Description;
                productionSimulationGroupingModel.PackSize = s.PackSize;
                productionSimulationGroupingModel.Quantity = s.Quantity;
                productionSimulationGroupingModel.Uom = s.Uom;
                productionSimulationGroupingModel.PerQuantity = s.PerQuantity;
                productionSimulationGroupingModel.PerQtyUom = s.PerQtyUom;
                productionSimulationGroupingModel.BatchNo = s.BatchNo;
                productionSimulationGroupingModel.StartingDate = s.StartingDate;
                productionSimulationGroupingModel.StatusCodeID = s.StatusCodeId;
                productionSimulationGroupingModel.AddedByUserID = s.AddedByUserId;
                productionSimulationGroupingModel.AddedDate = s.AddedDate;
                productionSimulationGroupingModel.ModifiedByUserID = s.ModifiedByUserId;
                productionSimulationGroupingModel.ModifiedDate = s.ModifiedDate;
                productionSimulationGroupingModel.CompanyId = s.CompanyId;
                productionSimulationGroupingModel.PlannedQty = s.PlannedQty;
                productionSimulationGroupingModel.OutputQty = s.OutputQty;
                productionSimulationGroupingModel.IsOutput = s.IsOutput;
                productionSimulationGroupingModel.ProcessDate = s.ProcessDate;
                productionSimulationGroupingModel.RePlanRefNo = s.RePlanRefNo;
                productionSimulationGroupingModel.BatchSize = s.BatchSize;
                productionSimulationGroupingModel.IsBmrticket = s.IsBmrticket;
                productionSimulationGroupingModel.Description2 = s.Description2;
                productionSimulationGroupingModel.TypeOfProdOrderId = s.TypeOfProdOrderId;
                productionSimulationGroupingModel.SellingStatusId = s.SellingStatusId;
                productionSimulationGroupingModel.PortalStatusNo = s.PortalStatusNo;
                productionSimulationGroupingModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                productionSimulationGroupingModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                productionSimulationGroupingModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                productionSimulationGroupingModel.TypeOfProdOrder = s.TypeOfProdOrder?.Value;
                productionSimulationGroupingModel.SellingStatus = s.SellingStatus?.Value;
                productionSimulationGroupingModel.MethodCodeId = s.MethodCodeId;
                productionSimulationGroupingModel.Dispense = s.Dispense;
                productionSimulationGroupingModel.GenericCode = genericCodeSupplyToMultiple?.FirstOrDefault(f => f.GenericCodeSupplyToMultipleId == genericcodeId)?.GenericCode?.Code;
                productionSimulationGroupingModels.Add(productionSimulationGroupingModel);
            });
            return productionSimulationGroupingModels;
        }
        [HttpPost]
        [Route("InsertSimulationForForecastPlanning")]
        public ProductionSimulationGroupingModel Post(ProductionSimulationGroupingModel value)
        {
            var profileNo = value.ProdOrderNo;
            if (value.ProductionSimulationGroupingId < 0)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID });
            }
            var productionSimulationGrouping = new ProductionSimulationGrouping
            {
                ProductionSimulationId = value.ProductionSimulationId,
                ProdOrderNo = profileNo,
                ItemId = value.ItemId,
                ItemNo = value.ItemNo,
                Description = value.Description,
                PackSize = value.PackSize,
                Quantity = value.Quantity,
                Uom = value.Uom,
                PerQuantity = value.PerQuantity,
                PerQtyUom = value.PerQtyUom,
                BatchNo = value.BatchNo,
                StartingDate = value.StartingDate,
                CompanyId = value.CompanyId,
                PlannedQty = value.PlannedQty,
                OutputQty = value.OutputQty,
                IsOutput = value.IsOutput,
                ProcessDate = value.ProcessDate,
                RePlanRefNo = value.RePlanRefNo,
                BatchSize = value.BatchSize,
                IsBmrticket = value.IsBmrticket,
                Description2 = value.Description2,
                SellingStatusId = value.SellingStatusId,
                TypeOfProdOrderId = value.TypeOfProdOrderId,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                PortalStatusNo = profileNo,
                MethodCodeId = value.MethodCodeId,
                Dispense = value.Dispense,
            };
            _context.ProductionSimulationGrouping.Add(productionSimulationGrouping);
            _context.SaveChanges();
            value.ProductionSimulationGroupingId = productionSimulationGrouping.ProductionSimulationGroupingId;
            value.PortalStatusNo = profileNo;
            value.ProdOrderNo = productionSimulationGrouping.PortalStatusNo;

            return value;
        }
        [HttpPut]
        [Route("UpdateSimulationForForecastPlanning")]
        public async Task<ProductionSimulationGroupingModel> Update(ProductionSimulationGroupingModel value)
        {
            var productionSimulationGrouping = _context.ProductionSimulationGrouping.SingleOrDefault(w => w.ProductionSimulationGroupingId == value.ProductionSimulationGroupingId);

            productionSimulationGrouping.ProductionSimulationId = value.ProductionSimulationId;
            productionSimulationGrouping.ProdOrderNo = value.ProdOrderNo;
            productionSimulationGrouping.ItemId = value.ItemId;
            productionSimulationGrouping.ItemNo = value.ItemNo;
            productionSimulationGrouping.Description = value.Description;
            productionSimulationGrouping.PackSize = value.PackSize;
            productionSimulationGrouping.Quantity = value.Quantity;
            productionSimulationGrouping.Uom = value.Uom;
            productionSimulationGrouping.PerQuantity = value.PerQuantity;
            productionSimulationGrouping.PerQtyUom = value.PerQtyUom;
            productionSimulationGrouping.BatchNo = value.BatchNo;
            productionSimulationGrouping.StartingDate = value.StartingDate;
            productionSimulationGrouping.CompanyId = value.CompanyId;
            productionSimulationGrouping.PlannedQty = value.PlannedQty;
            productionSimulationGrouping.OutputQty = value.OutputQty;
            productionSimulationGrouping.IsOutput = value.IsOutput;
            productionSimulationGrouping.ProcessDate = value.ProcessDate;
            productionSimulationGrouping.RePlanRefNo = value.RePlanRefNo;
            productionSimulationGrouping.BatchSize = value.BatchSize;
            productionSimulationGrouping.IsBmrticket = value.IsBmrticket;
            productionSimulationGrouping.Description2 = value.Description2;
            productionSimulationGrouping.SellingStatusId = value.SellingStatusId;
            productionSimulationGrouping.TypeOfProdOrderId = value.TypeOfProdOrderId;
            productionSimulationGrouping.StatusCodeId = value.StatusCodeID.Value;
            productionSimulationGrouping.ModifiedByUserId = value.ModifiedByUserID;
            productionSimulationGrouping.MethodCodeId = value.MethodCodeId;
            productionSimulationGrouping.ModifiedDate = DateTime.Now;
            productionSimulationGrouping.Dispense = value.Dispense;
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("DeleteSimulationForForecastPlanning")]
        public async Task<ProductionSimulationGroupingModel> Delete(ProductionSimulationGroupingModel value)
        {
            if (value.ProductionSimulationGroupingId > 0)
            {
                var productionSimulationGrouping = _context.ProductionSimulationGrouping.SingleOrDefault(w => w.ProductionSimulationGroupingId == value.ProductionSimulationGroupingId);
                productionSimulationGrouping.StatusCodeId = 2413;
                _context.SaveChanges();
            }
            return value;
        }
        [Route("GetItemInformation")]
        public List<ItemInformationModel> GetItemInfomation()
        {
            List<ItemInformationModel> itemInformationModels = new List<ItemInformationModel>();
            return itemInformationModels;
        }
        [HttpPost]
        [Route("InsertItemInformation")]
        public ItemInformationModel InsertItemInformation(ItemInformationModel value)
        {
            return value;
        }
        [HttpPut]
        [Route("UpdateItemInformation")]
        public async Task<ItemInformationModel> UpdateItemInformation(ItemInformationModel value)
        {
            return value;
        }
        /*[HttpDelete]
        [Route("DeleteItemInformation")]
        public ActionResult<string> DeleteItemInformation(int id)
        {
            
        }*/
        [Route("GetProdOrder")]
        public List<ProdOrderModel> GetProdOrder()
        {
            List<ProdOrderModel> prodOrderModel = new List<ProdOrderModel>();
            return prodOrderModel;
        }
        [HttpPost]
        [Route("InsertProdOrder")]
        public ProdOrderModel InsertProdOrder(ProdOrderModel value)
        {
            return value;
        }
        [HttpPut]
        [Route("UpdateProdOrder")]
        public async Task<ProdOrderModel> UpdateProdOrder(ProdOrderModel value)
        {
            return value;
        }
        /*[HttpDelete]
        [Route("DeleteProdOrder")]
        public ActionResult<string> DeleteItemInformation(int id)
        {
            
        }*/
    }
}
