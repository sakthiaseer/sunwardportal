﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CompanyEventsController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public CompanyEventsController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        //CompanyEventId
        //CompanyEventID
        [HttpGet]
        [Route("GetCompanyEvents")]
        public List<CompanyEventsModel> Get()
        {
            var companyEvents = _context.CompanyEvents.
              Include(a => a.AddedByUser)
              .Include(m => m.ModifiedByUser)
              .Include(s => s.StatusCode)
              .Include(p => p.EventType)
              .AsNoTracking().ToList();
            List<CompanyEventsModel> companyEventsModel = new List<CompanyEventsModel>();
            companyEvents.ForEach(s =>
            {
                CompanyEventsModel companyEventsModels = new CompanyEventsModel
                {
                    CompanyEventID = s.CompanyEventId,
                    EventTypeID = s.EventTypeId,
                    StartDate = s.StartDate,
                    CompanyID = s.CompanyId,
                    DeptOwner = s.DeptOwner,
                    Name = s.Name,
                    RepeatInfoID = s.RepeatInfoId,
                    StandardFrequencyID = s.StandardFrequencyId,
                    FullfilmentStatus = s.FullfilmentStatus,
                    NotLaterDays = s.NotLaterDays,
                    TriggerDate = s.TriggerDate,
                    TaskContent = s.TaskContent,
                    TaskDueDate = s.TaskDueDate,
                    AuthorityOrCompanyRequirement = s.AuthorityOrCompanyRequirement,
                    IsEffectProdSchdule = s.IsEffectProdSchdule,
                    RequiredPreparation = s.RequiredPreparation,
                    PreparationTypeID = s.PreparationTypeId,
                    PreparationDays = s.PreparationDays,
                    Reminderby = s.Reminderby,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    EventType = s.EventType != null ? s.EventType.CodeValue : "",

                };
                companyEventsModel.Add(companyEventsModels);
            });
            return companyEventsModel.OrderByDescending(a => a.CompanyEventID).ToList();

        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get CompanyEvents")]
        [HttpGet("GetCompanyEvents/{id:int}")]
        public ActionResult<CompanyEventsModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var companyEvents = _context.CompanyEvents.SingleOrDefault(p => p.CompanyEventId == id.Value);
            var result = _mapper.Map<CompanyEventsModel>(companyEvents);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CompanyEventsModel> GetData(SearchModel searchModel)
        {
            var companyEvents = new CompanyEvents();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        companyEvents = _context.CompanyEvents.OrderByDescending(o => o.CompanyEventId).FirstOrDefault();
                        break;
                    case "Last":
                        companyEvents = _context.CompanyEvents.OrderByDescending(o => o.CompanyEventId).LastOrDefault();
                        break;
                    case "Next":
                        companyEvents = _context.CompanyEvents.OrderByDescending(o => o.CompanyEventId).LastOrDefault();
                        break;
                    case "Previous":
                        companyEvents = _context.CompanyEvents.OrderByDescending(o => o.CompanyEventId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        companyEvents = _context.CompanyEvents.OrderByDescending(o => o.CompanyEventId).FirstOrDefault();
                        break;
                    case "Last":
                        companyEvents = _context.CompanyEvents.OrderByDescending(o => o.CompanyEventId).LastOrDefault();
                        break;
                    case "Next":
                        companyEvents = _context.CompanyEvents.OrderBy(o => o.CompanyEventId).FirstOrDefault(s => s.CompanyEventId > searchModel.Id);
                        break;
                    case "Previous":
                        companyEvents = _context.CompanyEvents.OrderByDescending(o => o.CompanyEventId).FirstOrDefault(s => s.CompanyEventId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CompanyEventsModel>(companyEvents);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertCompanyEvents")]
        public CompanyEventsModel Post(CompanyEventsModel value)
        {
            var companyEvents = new CompanyEvents
            {
                //CompanyEventId = value.CompanyEventID,
                //CountryOptions = value.CountryOptions,
                EventTypeId = value.EventTypeID,
                StartDate = value.StartDate,
                CompanyId = value.CompanyID,
                DeptOwner = value.DeptOwner,
                Name = value.Name,
                RepeatInfoId = value.RepeatInfoID,
                StandardFrequencyId = value.StandardFrequencyID,
                FullfilmentStatus = value.FullfilmentStatus,
                NotLaterDays = value.NotLaterDays,
                TriggerDate = value.TriggerDate,
                TaskContent = value.TaskContent,
                TaskDueDate = value.TaskDueDate,
                AuthorityOrCompanyRequirement = value.AuthorityOrCompanyRequirement,
                IsEffectProdSchdule = value.IsEffectProdSchdule,
                RequiredPreparation = value.RequiredPreparation,
                PreparationTypeId = value.PreparationTypeID,
                PreparationDays = value.PreparationDays,
                Reminderby = value.Reminderby,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value

            };
            _context.CompanyEvents.Add(companyEvents);
            _context.SaveChanges();
            value.CompanyEventID = companyEvents.CompanyEventId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateCompanyEvents")]
        public CompanyEventsModel Put(CompanyEventsModel value)
        {
            var companyEvents = _context.CompanyEvents.SingleOrDefault(p => p.CompanyEventId == value.CompanyEventID);
            //companyEvents.CompanyEventId = value.CompanyEventID;
            companyEvents.EventTypeId = value.EventTypeID;
            companyEvents.StartDate = value.StartDate;
            companyEvents.CompanyId = value.CompanyID;
            companyEvents.DeptOwner = value.DeptOwner;
            companyEvents.Name = value.Name;
            companyEvents.RepeatInfoId = value.RepeatInfoID;
            companyEvents.StandardFrequencyId = value.StandardFrequencyID;
            companyEvents.FullfilmentStatus = value.FullfilmentStatus;
            companyEvents.NotLaterDays = value.NotLaterDays;
            companyEvents.TriggerDate = value.TriggerDate;
            companyEvents.TaskContent = value.TaskContent;
            companyEvents.TaskDueDate = value.TaskDueDate;
            companyEvents.AuthorityOrCompanyRequirement = value.AuthorityOrCompanyRequirement;
            companyEvents.IsEffectProdSchdule = value.IsEffectProdSchdule;
            companyEvents.RequiredPreparation = value.RequiredPreparation;
            companyEvents.PreparationTypeId = value.PreparationTypeID;
            companyEvents.PreparationDays = value.PreparationDays;
            companyEvents.Reminderby = value.Reminderby;
            companyEvents.ModifiedByUserId = value.ModifiedByUserID;
            companyEvents.ModifiedDate = DateTime.Now;
            companyEvents.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteCompanyEvents")]
        public void Delete(int id)
        {
            var companyEvents = _context.CompanyEvents.SingleOrDefault(p => p.CompanyEventId == id);
            if (companyEvents != null)
            {
                _context.CompanyEvents.Remove(companyEvents);
                _context.SaveChanges();
            }
        }
    }
}