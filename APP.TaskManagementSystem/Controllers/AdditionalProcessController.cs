﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class AdditionalProcessController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public AdditionalProcessController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetAdditionalProcessList")]
        public List<AdditionalProcessModel> Get()
        {
            var AdditionalProcess = _context.AdditionalProcess.Include("AddedByUser").Include("ModifiedByUser").Select(s => new AdditionalProcessModel
            {
                AdditionalProcessID = s.AdditionalProcessId,               
                Code = s.Code,
                Description = s.Description,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser.UserName,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate

            }).OrderByDescending(o => o.AdditionalProcessID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<AdditionalProcessModel>>(AdditionalProcess);
            return AdditionalProcess;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get AdditionalProcess")]
        [HttpGet("GetAdditionalProcesss/{id:int}")]
        public ActionResult<AdditionalProcessModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var AdditionalProcess = _context.AdditionalProcess.SingleOrDefault(p => p.AdditionalProcessId == id.Value);
            var result = _mapper.Map<AdditionalProcessModel>(AdditionalProcess);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        // GET: api/Project
      
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<AdditionalProcessModel> GetData(SearchModel searchModel)
        {
            var AdditionalProcess = new AdditionalProcess();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        AdditionalProcess = _context.AdditionalProcess.OrderByDescending(o => o.AdditionalProcessId).FirstOrDefault();
                        break;
                    case "Last":
                        AdditionalProcess = _context.AdditionalProcess.OrderByDescending(o => o.AdditionalProcessId).LastOrDefault();
                        break;
                    case "Next":
                        AdditionalProcess = _context.AdditionalProcess.OrderByDescending(o => o.AdditionalProcessId).LastOrDefault();
                        break;
                    case "Previous":
                        AdditionalProcess = _context.AdditionalProcess.OrderByDescending(o => o.AdditionalProcessId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        AdditionalProcess = _context.AdditionalProcess.OrderByDescending(o => o.AdditionalProcessId).FirstOrDefault();
                        break;
                    case "Last":
                        AdditionalProcess = _context.AdditionalProcess.OrderByDescending(o => o.AdditionalProcessId).LastOrDefault();
                        break;
                    case "Next":
                        AdditionalProcess = _context.AdditionalProcess.OrderBy(o => o.AdditionalProcessId).FirstOrDefault(s => s.AdditionalProcessId > searchModel.Id);
                        break;
                    case "Previous":
                        AdditionalProcess = _context.AdditionalProcess.OrderByDescending(o => o.AdditionalProcessId).FirstOrDefault(s => s.AdditionalProcessId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<AdditionalProcessModel>(AdditionalProcess);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertAdditionalProcess")]
        public AdditionalProcessModel Post(AdditionalProcessModel value)
        {
            var AdditionalProcess = new AdditionalProcess
            {
                //AdditionalProcessId = value.AdditionalProcessID,
                //CountryOptions = value.CountryOptions,
               
                Code = value.Code,
                Description = value.Description,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value

            };
            _context.AdditionalProcess.Add(AdditionalProcess);
            _context.SaveChanges();
            value.AdditionalProcessID = AdditionalProcess.AdditionalProcessId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateAdditionalProcess")]
        public AdditionalProcessModel Put(AdditionalProcessModel value)
        {
            var AdditionalProcess = _context.AdditionalProcess.SingleOrDefault(p => p.AdditionalProcessId == value.AdditionalProcessID);
            //AdditionalProcess.AdditionalProcessId = value.AdditionalProcessID;
            AdditionalProcess.Code = value.Code;
            AdditionalProcess.ModifiedByUserId = value.ModifiedByUserID;
            AdditionalProcess.ModifiedDate = DateTime.Now;
            AdditionalProcess.Description = value.Description;
            AdditionalProcess.Code = value.Code;
            //AdditionalProcess.AddedByUserId = value.AddedByUserID;
            // AdditionalProcess.AddedDate = DateTime.Now;
            //project.Name = value.Name;
            //project.Description = value.Description;
            AdditionalProcess.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteAdditionalProcess")]
        public void Delete(int id)
        {
            var AdditionalProcess = _context.AdditionalProcess.SingleOrDefault(p => p.AdditionalProcessId == id);
            if (AdditionalProcess != null)
            {
                _context.AdditionalProcess.Remove(AdditionalProcess);
                _context.SaveChanges();
            }
        }
        }
}