﻿using APP.DataAccess.Models;
using APP.EntityModel;
using APP.EntityModels;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DownloadController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _hostingEnvironment;
        public DownloadController(CRT_TMSContext context, IMapper mapper, IWebHostEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _hostingEnvironment = host;
        }
        [HttpGet]
        [Route("DownLoadDocument")]
        public IActionResult DownLoadDocument(long id)
        {
            var document = _context.Documents.SingleOrDefault(t => t.DocumentId == id);
            if (document != null)
            {
                if (document.FilePath != null)
                {
                    string serverPath = _hostingEnvironment.ContentRootPath + @"\" + document.FilePath;
                    FileStream stream = System.IO.File.OpenRead(serverPath);
                    var br = new BinaryReader(stream);
                    Byte[] documents = br.ReadBytes((Int32)stream.Length);
                    Stream streams = new MemoryStream(documents);
                    if (streams == null)
                        return NotFound();
                    return Ok(streams);
                }
                else
                {
                    if (!document.ContentType.Contains("video/mp4"))
                    {
                        var file = document.IsCompressed.GetValueOrDefault(false) ? DocumentZipUnZip.Unzip(document.FileData) : document.FileData;

                        if (file != null)
                        {
                            Stream stream = new MemoryStream(file);

                            if (stream == null)
                                return NotFound();

                            return Ok(stream);
                        }
                    }
                    else
                    {
                        string[] subs = document.FileName.Split('.');
                        string serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\Videos\" + document.SessionId + "." + subs[1];
                        FileStream stream = System.IO.File.OpenRead(serverPath);
                        var br = new BinaryReader(stream);
                        Byte[] documents = br.ReadBytes((Int32)stream.Length);
                        Stream streams = new MemoryStream(documents);
                        if (streams == null)
                            return NotFound();

                        return Ok(streams);
                    }
                }
            }
            return NotFound();
        }
        [HttpGet]
        [Route("GetLatestDocument")]
        public IActionResult GetLatestDocument(long id)
        {
            var document = _context.Documents.SingleOrDefault(t => t.DocumentId == id);
            if (document != null && !document.IsLatest.GetValueOrDefault(false))
            {
                if (document.DocumentParentId != null)
                {
                    document = _context.Documents.FirstOrDefault(t => t.DocumentParentId == document.DocumentParentId && t.IsLatest == true);
                }
                else
                {
                    document = _context.Documents.FirstOrDefault(t => t.DocumentParentId == document.DocumentId && t.IsLatest == true);
                }
            }
            if (document != null && document.IsLatest == true)
            {
                var file = document.IsCompressed.GetValueOrDefault(false) ? DocumentZipUnZip.Unzip(document.FileData) : document.FileData;


                Stream stream = new MemoryStream(file);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }

       
        [HttpGet]
        [Route("DownLoadLatestDocument")]
        public IActionResult DownLoadLatestDocument(long id, long folderId)
        {
            var latestFile = _context.DocumentFolder.FirstOrDefault(d => d.DocumentFolderId == folderId);
            if (latestFile != null)
            {
                if (latestFile.PreviousDocumentId > 0)
                {
                    latestFile = _context.DocumentFolder.FirstOrDefault(d => d.PreviousDocumentId == latestFile.PreviousDocumentId && d.IsLatest == true);
                }
                id = latestFile.DocumentId.Value;
            }
            var document = _context.Documents.SingleOrDefault(t => t.DocumentId == id);
            if (document != null)
            {
                var file = document.IsCompressed.GetValueOrDefault(false) ? DocumentZipUnZip.Unzip(document.FileData) : document.FileData;

                var stream = new MemoryStream(file);

                return Ok(stream);
            }
            return NotFound();
        }
        [HttpGet]
        [Route("LatestDocumentInfo")]
        public LatestDocumentInfoModel LatestDocumentInfo(long id, long folderId, long userId)
        {
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).AsNoTracking().ToList();
            Folders folders = new Folders();
            TaskMaster taskMaster = new TaskMaster();
            var canDownload = false;
            var TaskAttachment = new TaskAttachment();
            var oldDocId = id;
            var latestFile = _context.DocumentFolder.FirstOrDefault(d => d.DocumentFolderId == folderId);
            if (latestFile != null)
            {
                if (latestFile.PreviousDocumentId > 0)
                {
                    latestFile = _context.DocumentFolder.FirstOrDefault(d => d.PreviousDocumentId == latestFile.PreviousDocumentId && d.IsLatest == true);
                }
                id = latestFile.DocumentId.Value;
                folders = _context.Folders.FirstOrDefault(f => f.FolderId == latestFile.FolderId);

                var docRole = _context.DocumentUserRole.Include("Role").FirstOrDefault(f => f.DocumentId == id && f.UserId == userId);

                if (docRole != null)
                {

                    var permission = _context.DocumentPermission.FirstOrDefault(f => f.DocumentRoleId == docRole.RoleId);
                    canDownload = permission.IsRead.GetValueOrDefault(false) || permission.IsEdit.GetValueOrDefault(false) || permission.IsUpdateDocument.GetValueOrDefault(false) ? true : false;
                }
                else
                {
                    canDownload = true;
                }
            }
            else
            {

                var previousDocID = _context.TaskAttachment.FirstOrDefault(f => f.DocumentId == id);
                if (previousDocID != null)
                {
                    if (folderId == -1)
                    {
                        var prevDocId = previousDocID.TaskAttachmentId;
                        if (previousDocID.PreviousDocumentId.HasValue)
                        {
                            prevDocId = previousDocID.PreviousDocumentId.Value;
                            TaskAttachment = _context.TaskAttachment.Include(u => u.UploadedByUser).FirstOrDefault(d => d.PreviousDocumentId == prevDocId && d.IsLatest == true);
                            id = TaskAttachment.DocumentId.GetValueOrDefault(0);
                        }
                        else
                        {
                            prevDocId = previousDocID.TaskAttachmentId;
                            var attach = _context.TaskAttachment.FirstOrDefault(d => d.PreviousDocumentId == prevDocId && d.IsLatest == true);
                            if (attach == null)
                            {
                                TaskAttachment = _context.TaskAttachment.FirstOrDefault(d => d.TaskAttachmentId == prevDocId && d.IsLatest == true);
                                id = TaskAttachment.DocumentId.GetValueOrDefault(0);
                            }
                            else
                            {
                                id = attach.DocumentId.GetValueOrDefault(0);
                                TaskAttachment = attach;
                            }
                        }
                    }
                    else if (folderId == -2)
                    {
                        TaskAttachment = previousDocID;
                    }

                    taskMaster = _context.TaskMaster.FirstOrDefault(f => f.TaskId == previousDocID.TaskMasterId);

                    var docAccess = _context.DocumentRights.Where(d => d.DocumentId == id);
                    if (docAccess != null && docAccess.Count() > 0)
                    {
                        var userrights = docAccess.FirstOrDefault(f => f.UserId == userId);
                        if (userrights != null)
                        {
                            if (userrights.IsRead.GetValueOrDefault(false) || userrights.IsReadWrite.GetValueOrDefault(false))
                            {
                                canDownload = true;
                            }
                        }
                        else
                        {
                            canDownload = false;
                        }

                    }
                    else
                    {
                        var assigned = _context.TaskAssigned.Any(a => (a.OnBehalfId == userId || a.UserId == userId || a.TaskOwnerId == userId) && a.TaskId == taskMaster.TaskId);
                        var assignedCC = _context.TaskMembers.Any(a => a.AssignedCc == userId && a.TaskId == taskMaster.TaskId);
                        canDownload = assigned || assignedCC ? true : false;
                    }


                }
            }
            LatestDocumentInfoModel LatestDocumentInfoModel = new LatestDocumentInfoModel();
            var document = _context.Documents.SingleOrDefault(t => t.DocumentId == id);
            if (document != null)
            {

                LatestDocumentInfoModel.DocumentId = document.DocumentId;
                LatestDocumentInfoModel.FileName = document.FileName;
                LatestDocumentInfoModel.ContentType = document.ContentType;
                LatestDocumentInfoModel.canDownload = canDownload;
                LatestDocumentInfoModel.FolderID = folders.FolderId > 0 ? folders.FolderId : 0;
                LatestDocumentInfoModel.MainFolderID = folders.MainFolderId > 0 ? folders.MainFolderId : 0;
                LatestDocumentInfoModel.FolderName = folders.FolderId > 0 ? folders.Name : string.Empty;
                LatestDocumentInfoModel.FolderAddedName = folders.FolderId > 0 ? (appUsers.FirstOrDefault(f => f.UserId == folders.AddedByUserId)?.UserName) : string.Empty;
                LatestDocumentInfoModel.FolderAddedDate = folders.FolderId > 0 ? folders.AddedDate.ToString() : string.Empty;
                LatestDocumentInfoModel.FolderModifiedName = folders.ModifiedByUserId != null ? folders.FolderId > 0 ? (appUsers.FirstOrDefault(f => f.UserId == folders.ModifiedByUserId)?.UserName) : string.Empty : ((appUsers.FirstOrDefault(f => f.UserId == document.ModifiedByUserId)?.UserName));
                LatestDocumentInfoModel.FolderModifiedDate = document.ModifiedDate == null ? folders.FolderId > 0 && folders.ModifiedDate != null ? folders.ModifiedDate.ToString() : string.Empty : document.ModifiedDate.ToString();
                LatestDocumentInfoModel.FolderReleaseVersionNo = folders.FolderId > 0 && document.DocumentFolder != null ? document.DocumentFolder.First(d => d.DocumentId == document.DocumentId).VersionNo : string.Empty;
                LatestDocumentInfoModel.TaskTitle = taskMaster.TaskId > 0 ? taskMaster.Title : string.Empty;
                LatestDocumentInfoModel.TaskID = taskMaster.TaskId > 0 ? taskMaster.TaskId : 0;
                LatestDocumentInfoModel.MainTaskID = taskMaster.TaskId > 0 ? taskMaster.MainTaskId : 0;
                LatestDocumentInfoModel.TaskAddedName = string.Empty;
                LatestDocumentInfoModel.TaskAddedDate = document.UploadDate;
                LatestDocumentInfoModel.TaskModifiedName = appUsers.FirstOrDefault(f => f.UserId == TaskAttachment.UploadedByUserId)?.UserName;
                LatestDocumentInfoModel.TaskModifiedDate = document.UploadDate;
                LatestDocumentInfoModel.TaskReleaseVersionNo = TaskAttachment.VersionNo;

            }
            if (LatestDocumentInfoModel != null)
            {
                if (folders != null)
                {
                    var folderslist = _context.Folders.AsNoTracking().ToList();
                    List<string> FolderPathLists = new List<string>();
                    if (folders.ParentFolderId != null)
                    {
                        FolderPathLists = GetAllFolderNames(folderslist, folders, FolderPathLists);
                    }
                    FolderPathLists.Add(folders.Name);
                    LatestDocumentInfoModel.DocumentPath = string.Join(" / ", FolderPathLists);
                }

            }
            return LatestDocumentInfoModel;
        }
        private List<string> GetAllFolderNames(List<Folders> foldersListItems, Folders s, List<string> foldersModel)
        {
            if (s.ParentFolderId != null)
            {
                var items = foldersListItems.FirstOrDefault(f => f.FolderId == s.ParentFolderId);
                GetAllFolderNames(foldersListItems, items, foldersModel);
                foldersModel.Add(items.Name);
            }
            return foldersModel;
        }

    }

    public class latestFile
    {
        public long DocumentId { get; set; }
        public long FolderId { get; set; }
    }

    public class eBookResult : IHttpActionResult
    {
        MemoryStream bookStuff;
        string PdfFileName;
        HttpRequestMessage httpRequestMessage;
        HttpResponseMessage httpResponseMessage;
        public eBookResult(MemoryStream data, HttpRequestMessage request, string filename)
        {
            bookStuff = data;
            httpRequestMessage = request;
            PdfFileName = filename;
        }
        public System.Threading.Tasks.Task<HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            httpResponseMessage = httpRequestMessage.CreateResponse(HttpStatusCode.OK);
            httpResponseMessage.Content = new StreamContent(bookStuff);
            //httpResponseMessage.Content = new ByteArrayContent(bookStuff.ToArray());  
            httpResponseMessage.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
            httpResponseMessage.Content.Headers.ContentDisposition.FileName = PdfFileName;
            httpResponseMessage.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");

            return System.Threading.Tasks.Task.FromResult(httpResponseMessage);
        }
    }


    public interface IHttpActionResult
    {
        Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken);
    }
}