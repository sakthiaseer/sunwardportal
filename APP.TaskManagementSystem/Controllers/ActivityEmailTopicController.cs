﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using APP.DataAccess.Models;
using AutoMapper;
using APP.EntityModel;
using APP.EntityModel.SearchParam;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActivityEmailTopicController : ControllerBase
    {
        
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ActivityEmailTopicController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpPost]
        [Route("GetActivityEmailTopic")]
        public ActivityEmailTopics GetActivityEmailTopic(SearchModel searchModel)
        {
            var activityEmailTopic = _context.ActivityEmailTopics.Where(a=>a.ActivityMasterId == searchModel.Id && a.ActivityType.ToLower() == searchModel.Action.ToLower()).FirstOrDefault();
            return activityEmailTopic;
        }
        [HttpPost]
        [Route("GetApplicationUsersForActivity")]
        public List<ApplicationUserModel> Get(GetEmailActivityUsersSearchModel getEmailActivityUsersSearchModel)
        {
            List<ApplicationUserModel> applicationUserModels = new List<ApplicationUserModel>();
            //var applicationUsers = _context.Employee.Include(u => u.User).Include("Designation").AsNoTracking().ToList();
            string sqlQuery = string.Empty;
            sqlQuery = "Select  * from view_GetEmployee where (Status!='Resign' or Status is null) and UserID is Not null";
            var appUser = _context.ApplicationUser.Select(a => new { a.UserId, a.SessionId }).Where(w => w.SessionId != null).ToList();
            var ResponseDutyIds = _context.ProductActivityCaseResponsDuty.ToList();
            var appUsersList = _context.ApplicationUser.ToList();
            var applicationUsers = _context.Set<view_GetEmployee>().FromSqlRaw(sqlQuery).AsQueryable().Distinct();
            applicationUsers.ToList().ForEach(s =>
            {
                var SessionId = appUser.SingleOrDefault(a => a.UserId == s.UserID)?.SessionId;
                ApplicationUserModel applicationUserModel = new ApplicationUserModel();
                applicationUserModel.UserID = s.UserID.Value;
                applicationUserModel.EmployeeID = s.EmployeeID;
                applicationUserModel.UserName = s.FirstName;
                applicationUserModel.DepartmentID = s.DepartmentID;
                applicationUserModel.StatusCodeID = s.StatusCodeID;
                applicationUserModel.AddedByUserID = s.AddedByUserID;
                applicationUserModel.ModifiedByUserID = s.ModifiedByUserID;
                applicationUserModel.AddedDate = s.AddedDate;
                applicationUserModel.ModifiedDate = s.ModifiedDate;
                applicationUserModel.NickName = s.NickName;
                applicationUserModel.LoginID = s.LoginID;
                applicationUserModel.FirstName = s.FirstName;
                applicationUserModel.PlantId = s.PlantID;
                applicationUserModel.SessionId = SessionId;
                applicationUserModel.Designation = s.DesignationName != null ? s.DesignationName : "No Designation";
                applicationUserModel.Name = s.FirstName + " | " + s.NickName + " | " + applicationUserModel.Designation;
                applicationUserModel.EmplyeeDropDown = s.FirstName + " | " + s.LastName + " | " + s.NickName;
                applicationUserModels.Add(applicationUserModel);
            });

            //var result = _mapper.Map<List<ApplicationUserModel>>(applicationUser);


            //var templateTestCaseCheckListResponse = _context.ProductActivityCaseRespons.Select(s => new { s.ProductActivityCaseResponsId, s.ProductActivityCaseId }).Where(w => w.ProductActivityCaseId==id).ToList();
            //var templateTestCaseCheckListResponseIds = templateTestCaseCheckListResponse.Select(s => s.ProductActivityCaseResponsId).ToList();
            //var templateTestCaseCheckListResponseDuty = _context.ProductActivityCaseResponsDuty.Select(s => new { s.ProductActivityCaseResponsDutyId, s.ProductActivityCaseResponsId }).Where(w => templateTestCaseCheckListResponseIds.Contains(w.ProductActivityCaseResponsId.Value)).ToList();
            //var templateTestCaseCheckListResponseDutyIds = templateTestCaseCheckListResponseDuty.Select(s => s.ProductActivityCaseResponsDutyId).ToList();
            //var templateTestCaseCheckListResponseResponsible = _context.ProductActivityCaseResponsResponsible.Where(w => templateTestCaseCheckListResponseDutyIds.Contains(w.ProductActivityCaseResponsDutyId.Value)).Select(s => new { s.EmployeeId, s.ProductActivityCaseResponsDutyId }).ToList();
            //var empIds = templateTestCaseCheckListResponseResponsible.Select(s => s.EmployeeId).ToList();
            //if (empIds.Any())
            //{
            //    var empUsers = _context.Employee.Where(w => empIds.Contains(w.EmployeeId) && w.UserId != null).Select(s => new { s.UserId, s.EmployeeId }).ToList();
            //    var userIds = empUsers.Select(s => s.UserId.GetValueOrDefault(0)).ToList();
            //    var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.SessionId }).Where(w => userIds.Contains(w.UserId) && w.SessionId != null).ToList();
            //    var templateTestCaseCheckListResponses = templateTestCaseCheckListResponse.Where(w => w.ProductActivityCaseId == id).Select(s => new { s.ProductActivityCaseResponsId, s.ProductActivityCaseId }).ToList();
            //    var templateTestCaseCheckListResponseIdss = templateTestCaseCheckListResponses.Select(s => s.ProductActivityCaseResponsId).ToList();
            //    var templateTestCaseCheckListResponseDutys = templateTestCaseCheckListResponseDuty.Where(w => templateTestCaseCheckListResponseIdss.Contains(w.ProductActivityCaseResponsId.Value)).Select(s => new { s.ProductActivityCaseResponsDutyId, s.ProductActivityCaseResponsId }).ToList();
            //    var templateTestCaseCheckListResponseDutyIdss = templateTestCaseCheckListResponseDutys.Select(s => s.ProductActivityCaseResponsDutyId).ToList();
            //    var templateTestCaseCheckListResponseResponsibles = templateTestCaseCheckListResponseResponsible.Where(w => templateTestCaseCheckListResponseDutyIdss.Contains(w.ProductActivityCaseResponsDutyId.Value)).Select(s => new { s.EmployeeId, s.ProductActivityCaseResponsDutyId }).ToList();
            //    var empIdss = templateTestCaseCheckListResponseResponsibles.Select(s => s.EmployeeId).ToList();
            //    var userIdss = empUsers.Where(w => empIdss.Contains(w.EmployeeId)).Select(s => s.UserId.GetValueOrDefault(0)).ToList();
            //    var appUserss = appUsers.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdss.Contains(w.UserId) && w.SessionId != null).ToList();
            //    List<long> responsibilityUsers = new List<long>();
            //    if (appUserss != null && appUserss.Count > 0)
            //    {
            //        var usersIdsBy = appUserss.Select(s => s.UserId).ToList();
            //        if (usersIdsBy != null && usersIdsBy.Count > 0)
            //        {
            //            responsibilityUsers.AddRange(usersIdsBy);
            //        }
            //    }

            //}
            var responsiblelist = _context.ProductActivityCaseResponsResponsible.ToList();
            var activityCaseList = _context.ProductActivityCase.ToList();
            List<long> responsibilityUsers = new List<long>();
            var prodactivityCategoryMultiplelist = _context.ProductActivityCaseCategoryMultiple.ToList();
            var prodactivityActionMultiplelist = _context.ProductActivityCaseActionMultiple.ToList();
            var emplist = _context.Employee.ToList();
            var activityCaseManufacturingProcessList = activityCaseList.Where(a => a.ManufacturingProcessChildId == getEmailActivityUsersSearchModel.ManufacturingProcessChildId).Select(s => s.ProductActivityCaseId).ToList();
            var activitycaseReponseIds = prodactivityCategoryMultiplelist.Where(p => activityCaseManufacturingProcessList.Contains(p.ProductActivityCaseId.Value) && p.CategoryActionId == getEmailActivityUsersSearchModel.CategoryActionId).Select(s => s.ProductActivityCaseId).ToList();
            var actionmultipleIds = prodactivityActionMultiplelist.Where(p => activityCaseManufacturingProcessList.Contains(p.ProductActivityCaseId.Value) && activitycaseReponseIds.Contains(p.ProductActivityCaseId) && p.ActionId == getEmailActivityUsersSearchModel.ActionId).Select(s => s.ProductActivityCaseId).ToList();
            if (actionmultipleIds != null && actionmultipleIds.Count > 0)
            {
                var productActivityCaseResponsDutyIds = ResponseDutyIds.Where(r => actionmultipleIds.Contains(r.ProductActivityCaseResponsId)).Select(r => r.ProductActivityCaseResponsDutyId).ToList();
                if (productActivityCaseResponsDutyIds != null && productActivityCaseResponsDutyIds.Count > 0)
                {
                    var empIds = responsiblelist.Where(r => productActivityCaseResponsDutyIds.Contains(r.ProductActivityCaseResponsDutyId.Value)).Select(s => s.EmployeeId).ToList();
                    var empUsers = emplist.Where(w => empIds.Contains(w.EmployeeId) && w.UserId != null).Select(s => new { s.UserId, s.EmployeeId }).ToList();
                    var userIdsList = empUsers.Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                    var appUsers = appUsersList.Select(s => new { s.UserId, s.SessionId }).Where(w => userIdsList.Contains(w.UserId) && w.SessionId != null).ToList();
                    if (appUsers != null && appUsers.Count > 0)
                    {
                        var usersIdsBy = appUsers.Select(s => s.UserId).ToList();
                        if (usersIdsBy != null && usersIdsBy.Count > 0)
                        {
                            responsibilityUsers.AddRange(usersIdsBy);
                        }
                    }
                    if (responsibilityUsers.Count > 0)
                    {
                        applicationUserModels = applicationUserModels.Where(a => responsibilityUsers.Contains(a.UserID)).ToList();
                    }
                }
               
               
            }
           
            return applicationUserModels;
          
        }

        [HttpPost]
        [Route("InsertActivityEmailTopic")]
        public ActivityEmailTopicModel Post(ActivityEmailTopicModel value)
        {

            var activity = new ProductionActivityAppLine();
            var routine = new ProductionActivityRoutineAppLine();
            if (value.ActivityType.ToLower() == "productionactivity")
            {
                activity = _context.ProductionActivityAppLine.Where(p=>p.ProductionActivityAppLineId == value.ActivityMasterId).FirstOrDefault();
            }
            if (value.ActivityType.ToLower() == "routineactivity")
            {
                routine = _context.ProductionActivityRoutineAppLine.Where(p => p.ProductionActivityRoutineAppLineId == value.ActivityMasterId).FirstOrDefault();
            }
                var SessionId = Guid.NewGuid();
            var ActivityEmailTopic = new ActivityEmailTopics
            {
                ActivityMasterId = value.ActivityMasterId,
                CategoryActionId  = value.CategoryActionId,
                ActivityType = value.ActivityType,  
                SubjectName = value.Subject,    
                FromId = value.FromId,
                ToIds = value.ToIds,
                CcIds = value.CcIds,
                ManufacturingProcessId = value.ManufacturingProcessId,  
                AddedByUserId = value.AddedByUserId,              
                AddedDate     = DateTime.Now,   
                StatusCodeId = value.StatusCodeId,  
                SessionId = SessionId,
                DocumentSessionId =activity!=null? activity.SessionId!=null? activity.SessionId : routine!=null?routine.SessionId!=null? routine.SessionId : null :null:null,
                Comment = activity != null ? activity.Comment != null ? activity.Comment : routine != null ? routine.Comment != null ? routine.Comment : "" : "" : "",
                ActionId = value.ActionId,
                IsDraft = false,
               
        };
            ActivityEmailTopic.BackUrl = value.BackUrl + ActivityEmailTopic.DocumentSessionId;
            _context.ActivityEmailTopics.Add(ActivityEmailTopic);
            value.SessionId = SessionId;
          
            value.ActivityEmailTopicId = ActivityEmailTopic.ActivityEmailTopicId;
            _context.SaveChanges();         
            return value;
        }



    }
}
