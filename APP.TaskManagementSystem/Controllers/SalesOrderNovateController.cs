using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.TaskManagementSystem.Helper;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SalesOrderNovateController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public SalesOrderNovateController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generateDocumentNoSeries)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generateDocumentNoSeries;
        }

        [HttpGet]
        [Route("GetSalesOrderEntryItems")]
        public List<SalesOrderEntryModel> GetSalesOrderEntryItems()
        {

            var SalesOrderEntrys = _context.SalesOrderEntry.Include(p => p.PurchaseItemSalesEntryLine).Where(w => w.StatusCodeId == 1420 || w.StatusCodeId == 1422)
              .AsNoTracking()
                .ToList();
            List<SalesOrderEntryModel> SalesOrderEntryModels = new List<SalesOrderEntryModel>();
            var productIds = SalesOrderEntrys.SelectMany(p => p.PurchaseItemSalesEntryLine).Select(p => p.ItemId);
            var products = _context.Navitems.Where(i => productIds.Contains(i.ItemId));
            SalesOrderEntrys.ForEach(s =>
            {
                SalesOrderEntryModel salesOrderEntryModel = new SalesOrderEntryModel();

                salesOrderEntryModel.SalesOrderEntryID = s.SalesOrderEntryId;

                salesOrderEntryModel.OrderNo = s.OrderNo;

                salesOrderEntryModel.StatusCodeID = s.StatusCodeId;

                if (s.PurchaseItemSalesEntryLine != null)
                {
                    salesOrderEntryModel.SalesEntryProducts = new List<SalesEntryProduct>();
                    s.PurchaseItemSalesEntryLine.ToList().ForEach(p =>
                    {
                        if (p.ItemId != null)
                        {
                            var productItem = products.FirstOrDefault(i => i.ItemId == p.ItemId);
                            salesOrderEntryModel.SalesEntryProducts.Add(new SalesEntryProduct
                            {
                                SalesOrderID = p.SalesOrderEntryId.Value,
                                PurchaseItemSalesEntryLineID = p.PurchaseItemSalesEntryLineId,
                                ItemID = p.ItemId ?? p.ItemId.Value,
                                ProductName = productItem.No + " | " + productItem.Description,
                                BUOM = productItem.BaseUnitofMeasure,
                                PUOM = productItem.PurchaseUom,
                                PackUom = productItem.PackUom,
                                Description = productItem.Description
                            });
                        }
                    });
                }

                SalesOrderEntryModels.Add(salesOrderEntryModel);
            });
            return SalesOrderEntryModels.OrderByDescending(a => a.SalesOrderEntryID).ToList();
        }
        [HttpGet]
        [Route("GetSalesOrderEntryCustomerItems")]
        public List<ContractDistributionSalesEntryLineModel> GetSalesOrderEntryCustomerItems(int? id)
        {

            var SalesOrderEntrys = _context.PurchaseItemSalesEntryLine.Include(s => s.ContractDistributionSalesEntryLine).Where(w => w.SalesOrderEntryId == id).AsNoTracking().ToList();
            List<ContractDistributionSalesEntryLineModel> contractDistributionSalesEntryLine = new List<ContractDistributionSalesEntryLineModel>();
            List<PurchaseItemSalesEntryLineModel> purchaseItemSalesEntryLineModels = new List<PurchaseItemSalesEntryLineModel>();
            var contractDistributionSalesEntryLines = _context.ContractDistributionSalesEntryLine.Include(c => c.Socustomer).AsNoTracking().ToList();
            SalesOrderEntrys.ForEach(s =>
            {
                PurchaseItemSalesEntryLineModel purchaseItemSalesEntryLineModel = new PurchaseItemSalesEntryLineModel();

                purchaseItemSalesEntryLineModel.PurchaseItemSalesEntryLineID = s.PurchaseItemSalesEntryLineId;

                purchaseItemSalesEntryLineModels.Add(purchaseItemSalesEntryLineModel);
            });
            if (purchaseItemSalesEntryLineModels.Count > 0)
            {
                SalesOrderEntrys.ForEach(s =>
                {
                    contractDistributionSalesEntryLines.Where(a => a.PurchaseItemSalesEntryLineId == s.PurchaseItemSalesEntryLineId).ToList().ForEach(h =>
                    {
                        ContractDistributionSalesEntryLineModel contractDistributionSalesEntryLineItems = new ContractDistributionSalesEntryLineModel();
                        contractDistributionSalesEntryLineItems.SOCustomerID = h.SocustomerId;
                        contractDistributionSalesEntryLineItems.TotalQty = h.TotalQty;
                        contractDistributionSalesEntryLineItems.PONumber = h.Ponumber;
                        contractDistributionSalesEntryLineItems.CustomerName = h.Socustomer != null ? h.Socustomer.CompanyName : "";
                        contractDistributionSalesEntryLine.Add(contractDistributionSalesEntryLineItems);
                    });
                });
            }
            return contractDistributionSalesEntryLine.Where(s => s.CustomerName != "").ToList();
        }

        [HttpGet]
        [Route("GetCustomerItemsByOrderNo")]
        public List<PurchaseItemSalesEntryLineModel> GetCustomerItemsByOrderNo(int? id)
        {

            var SalesOrderEntrys = _context.PurchaseItemSalesEntryLine.Include(s => s.ContractDistributionSalesEntryLine).Where(w => w.SalesOrderEntryId == id).AsNoTracking().ToList();
            List<ContractDistributionSalesEntryLineModel> contractDistributionSalesEntryLine = new List<ContractDistributionSalesEntryLineModel>();
            List<PurchaseItemSalesEntryLineModel> purchaseItemSalesEntryLineModels = new List<PurchaseItemSalesEntryLineModel>();
            var contractDistributionSalesEntryLines = _context.ContractDistributionSalesEntryLine.Include(c => c.Socustomer).AsNoTracking().ToList();
            SalesOrderEntrys.ForEach(s =>
            {
                PurchaseItemSalesEntryLineModel purchaseItemSalesEntryLineModel = new PurchaseItemSalesEntryLineModel();

                purchaseItemSalesEntryLineModel.PurchaseItemSalesEntryLineID = s.PurchaseItemSalesEntryLineId;
                purchaseItemSalesEntryLineModel.SoByCustomersId = s.SobyCustomersId;
                purchaseItemSalesEntryLineModel.ProductNO = s.SobyCustomers?.CustomerReferenceNo;
                purchaseItemSalesEntryLineModels.Add(purchaseItemSalesEntryLineModel);
            });

            return purchaseItemSalesEntryLineModels.ToList();
        }
        [HttpGet]
        [Route("GetDescriptionExceptFPandPRItems")]
        public List<NavItemModel> GetDescriptionExceptFPandPRItems()
        {
            var items = _context.Navitems.Where(f => f.StatusCodeId == 1 && f.CompanyId == 1 && f.PackUom != null).Select(s => new NavItemModel
            {
                ItemId = s.ItemId,
                No = s.No,
                ItemNoDetail = s.No + "  |  " + s.Description,
                InternalRef = s.No + " | " + s.Description + " | " + s.Description2,
                RelatedItemNo = s.RelatedItemNo,
                Description = s.Description,
                Description2 = s.Description2,
                PackUom = s.PackUom,
                BatchNos = s.BatchNos,
                BaseUnitofMeasure = s.BaseUnitofMeasure,
                ItemType = s.ItemType,


                PurchaseUOM = s.PurchaseUom,

            }).OrderByDescending(o => o.ItemId).AsNoTracking().ToList();
            return items;//.Where(f => !f.No.StartsWith("FP") || !f.No.StartsWith("PR"))
        }

        [HttpGet]
        [Route("GetSobyCustomers")]
        public List<SobyCustomersModel> GetSobyCustomersByRefNo()
        {           
            var sobyCustomers = _context.SobyCustomers
             .AsNoTracking().ToList();
            List<SobyCustomersModel> sobyCustomersModel = new List<SobyCustomersModel>();
            sobyCustomers.ForEach(s =>
            {
                SobyCustomersModel sobyCustomersModels = new SobyCustomersModel
                {
                    SobyCustomersId = s.SobyCustomersId,
                    MustSupplyInFull = s.MustSupplyInFull,


                };
                sobyCustomersModel.Add(sobyCustomersModels);
            });

            return sobyCustomersModel;
        }

        [HttpGet]
        [Route("GetSalesOrderNovateItems")]
        public List<SalesAdhocNovateModel> Get()
        {
           
            var salesAdhocNovate = _context.SalesAdhocNovate
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(i => i.Item)
                 .Include(c => c.ToCustomer)
                  .Include(c => c.FromCustomer)
                  .Include(c => c.SalesOrderEntry)
                  .Include(c => c.SalesOrderEntry.PurchaseItemSalesEntryLine)
                .AsNoTracking()
                .ToList();
            List<SalesAdhocNovateModel> salesAdhocNovateModels = new List<SalesAdhocNovateModel>();
            salesAdhocNovate.ForEach(s =>
            {
                SalesAdhocNovateModel salesAdhocNovateModel = new SalesAdhocNovateModel();

                salesAdhocNovateModel.SalesAdhocNovateId = s.SalesAdhocNovateId;
                salesAdhocNovateModel.SalesOrderEntryId = s.SalesOrderEntryId;
                salesAdhocNovateModel.Date = s.Date;
                salesAdhocNovateModel.ItemId = s.ItemId;
                salesAdhocNovateModel.Description = s.SalesOrderEntry?.PurchaseItemSalesEntryLine?.Where(s => s.SalesOrderEntryId == s.SalesOrderEntryId).Select(s => s.SobyCustomers?.Description).FirstOrDefault();
                salesAdhocNovateModel.UomId = s.SalesOrderEntry?.PurchaseItemSalesEntryLine?.Where(s => s.SalesOrderEntryId == s.SalesOrderEntryId).Select(s => s.SobyCustomers?.PerUomId).FirstOrDefault();
                salesAdhocNovateModel.FromCustomerId = s.FromCustomerId;
                salesAdhocNovateModel.FromCustomerName = s.FromCustomer != null ? s.FromCustomer.CompanyName : "";
                salesAdhocNovateModel.FromStartMonth = s.FromStartMonth;
                salesAdhocNovateModel.FromTotalQty = s.FromTotalQty;
                salesAdhocNovateModel.ToCustomerId = s.ToCustomerId;
                salesAdhocNovateModel.ToCustomerName = s.ToCustomer != null ? s.ToCustomer.CompanyName : "";
                salesAdhocNovateModel.ToStartMonth = s.ToStartMonth;
                salesAdhocNovateModel.ToTotalQty = s.ToTotalQty;
                salesAdhocNovateModel.IntoNoOfLots = s.IntoNoOfLots;
                salesAdhocNovateModel.NewPono = s.NewPono;
                salesAdhocNovateModel.SessionId = s.SessionId;
                salesAdhocNovateModel.StatusCodeID = s.StatusCodeId;
                salesAdhocNovateModel.AddedByUserID = s.AddedByUserId;
                salesAdhocNovateModel.ModifiedByUserID = s.ModifiedByUserId;
                salesAdhocNovateModel.AddedDate = s.AddedDate;
                salesAdhocNovateModel.ModifiedDate = s.ModifiedDate;
                salesAdhocNovateModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                salesAdhocNovateModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                salesAdhocNovateModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                salesAdhocNovateModel.ContractNo = s.SalesOrderEntry?.OrderNo;

                salesAdhocNovateModels.Add(salesAdhocNovateModel);
            });
            if (salesAdhocNovateModels != null && salesAdhocNovateModels.Count > 0)
            {
                salesAdhocNovateModels.ForEach(s =>
                {

                });
            }
            return salesAdhocNovateModels.OrderByDescending(a => a.SalesAdhocNovateId).ToList();
        }

        [HttpGet]
        [Route("GetSalesOrderNovateItemsByContractId")]
        public List<SalesAdhocNovateModel> GetSalesOrderNovateItemsByContractId(int id)
        {           
            var salesAdhocNovate = _context.SalesAdhocNovate
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(i => i.Item)
                 .Include(c => c.ToCustomer)
                  .Include(c => c.FromCustomer)
                  .Include(c => c.SalesOrderEntry)
                  .Include(c => c.SalesOrderEntry.PurchaseItemSalesEntryLine)
                  .Where(s=>s.SalesOrderEntryId == id)
                .AsNoTracking()
                .ToList();
            List<SalesAdhocNovateModel> salesAdhocNovateModels = new List<SalesAdhocNovateModel>();
            salesAdhocNovate.ForEach(s =>
            {
                SalesAdhocNovateModel salesAdhocNovateModel = new SalesAdhocNovateModel();

                salesAdhocNovateModel.SalesAdhocNovateId = s.SalesAdhocNovateId;
                salesAdhocNovateModel.SalesOrderEntryId = s.SalesOrderEntryId;
                salesAdhocNovateModel.Date = s.Date;
                salesAdhocNovateModel.ItemId = s.ItemId;
                salesAdhocNovateModel.Description = s.SalesOrderEntry?.PurchaseItemSalesEntryLine?.Where(s => s.SalesOrderEntryId == s.SalesOrderEntryId).Select(s => s.SobyCustomers?.Description).FirstOrDefault();
                salesAdhocNovateModel.UomId = s.SalesOrderEntry?.PurchaseItemSalesEntryLine?.Where(s => s.SalesOrderEntryId == s.SalesOrderEntryId).Select(s => s.SobyCustomers?.PerUomId).FirstOrDefault();
                salesAdhocNovateModel.FromCustomerId = s.FromCustomerId;
                salesAdhocNovateModel.FromCustomerName = s.FromCustomer != null ? s.FromCustomer.CompanyName : "";
                salesAdhocNovateModel.FromStartMonth = s.FromStartMonth;
                salesAdhocNovateModel.FromTotalQty = s.FromTotalQty;
                salesAdhocNovateModel.ToCustomerId = s.ToCustomerId;
                salesAdhocNovateModel.ToCustomerName = s.ToCustomer != null ? s.ToCustomer.CompanyName : "";
                salesAdhocNovateModel.ToStartMonth = s.ToStartMonth;
                salesAdhocNovateModel.ToTotalQty = s.ToTotalQty;
                salesAdhocNovateModel.IntoNoOfLots = s.IntoNoOfLots;
                salesAdhocNovateModel.NewPono = s.NewPono;
                salesAdhocNovateModel.SessionId = s.SessionId;
                salesAdhocNovateModel.StatusCodeID = s.StatusCodeId;
                salesAdhocNovateModel.AddedByUserID = s.AddedByUserId;
                salesAdhocNovateModel.ModifiedByUserID = s.ModifiedByUserId;
                salesAdhocNovateModel.AddedDate = s.AddedDate;
                salesAdhocNovateModel.ModifiedDate = s.ModifiedDate;
                salesAdhocNovateModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                salesAdhocNovateModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                salesAdhocNovateModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                salesAdhocNovateModel.ContractNo = s.SalesOrderEntry?.OrderNo;

                salesAdhocNovateModels.Add(salesAdhocNovateModel);
            });
           
            return salesAdhocNovateModels.OrderByDescending(a => a.SalesAdhocNovateId).ToList();
        }

        [HttpPost()]
        [Route("GetSalesAdhocNovateData")]
        public ActionResult<SalesAdhocNovateModel> GetQuotationHistoryData(SearchModel searchModel)
        {
            
            var SalesAdhocNovate = new SalesAdhocNovate();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        SalesAdhocNovate = _context.SalesAdhocNovate.Include(i => i.Item).OrderByDescending(o => o.SalesAdhocNovateId).FirstOrDefault();
                        break;
                    case "Last":
                        SalesAdhocNovate = _context.SalesAdhocNovate.Include(i => i.Item).OrderByDescending(o => o.SalesAdhocNovateId).LastOrDefault();
                        break;
                    case "Next":
                        SalesAdhocNovate = _context.SalesAdhocNovate.Include(i => i.Item).OrderByDescending(o => o.SalesAdhocNovateId).LastOrDefault();
                        break;
                    case "Previous":
                        SalesAdhocNovate = _context.SalesAdhocNovate.Include(i => i.Item).OrderByDescending(o => o.SalesAdhocNovateId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        SalesAdhocNovate = _context.SalesAdhocNovate.Include(i => i.Item).OrderByDescending(o => o.SalesAdhocNovateId).FirstOrDefault();
                        break;
                    case "Last":
                        SalesAdhocNovate = _context.SalesAdhocNovate.Include(i => i.Item).OrderByDescending(o => o.SalesAdhocNovateId).LastOrDefault();
                        break;
                    case "Next":
                        SalesAdhocNovate = _context.SalesAdhocNovate.Include(i => i.Item).OrderBy(o => o.SalesAdhocNovateId).FirstOrDefault(s => s.SalesAdhocNovateId > searchModel.Id);
                        break;
                    case "Previous":
                        SalesAdhocNovate = _context.SalesAdhocNovate.Include(i => i.Item).OrderByDescending(o => o.SalesAdhocNovateId).FirstOrDefault(s => s.SalesAdhocNovateId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<SalesAdhocNovateModel>(SalesAdhocNovate);
            if (result != null)
            {
                var masterDetailList = _context.ApplicationMasterDetail.Where(a=>a.ApplicationMasterDetailId == SalesAdhocNovate.Item.PerUomId).AsNoTracking().ToList();
                result.Description = SalesAdhocNovate.Item != null ? SalesAdhocNovate.Item.Description : "";
                result.Uom = masterDetailList.Where(a => a.ApplicationMasterDetailId == SalesAdhocNovate.Item.PerUomId).Select(a => a.Value).FirstOrDefault();
            }
            return result;
        }

        [HttpPost]
        [Route("InsertSalesAdhocNovate")]
        public SalesAdhocNovateModel Post(SalesAdhocNovateModel value)
        {

            var SessionId = Guid.NewGuid();
            if(value.Date!=null && value.FromCustomerId !=null && value.ToCustomerId!=null && value.FromStartMonth!=null && value.ToStartMonth!=null &&  value.ToTotalQty!=null && value.IntoNoOfLots!=null && value.FromTotalQty!=null && value.NewPono!=null)
            {
                value.StatusCodeID = 2081;

            }
            if(value.Date == null || value.FromCustomerId == null || value.ToCustomerId == null || value.FromStartMonth == null || value.ToStartMonth == null || value.ToTotalQty == null || value.IntoNoOfLots == null || value.FromTotalQty == null || value.NewPono == null)
            {
                value.StatusCodeID = 2080;
            }
            if(value.Date != null && value.FromCustomerId == null && value.ToCustomerId == null && value.FromStartMonth == null && value.ToStartMonth == null && value.ToTotalQty == null && value.IntoNoOfLots == null && value.FromTotalQty == null && value.NewPono == null)
            {
                value.StatusCodeID = 2080;
            }
            var salesAdhocNovate = new SalesAdhocNovate
            {
                // SalesAdhocNovateId = value.SalesAdhocNovateId,
                SalesOrderEntryId = value.SalesOrderEntryId,
                Date = value.Date,
                ItemId = value.ItemId,
                FromCustomerId = value.FromCustomerId,
                FromStartMonth = value.FromStartMonth,
                FromTotalQty = value.FromTotalQty,
                ToCustomerId = value.ToCustomerId,
                ToStartMonth = value.ToStartMonth,
                ToTotalQty = value.ToTotalQty,
                IntoNoOfLots = value.IntoNoOfLots,
                NewPono = value.NewPono,
                SessionId = SessionId,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.SalesAdhocNovate.Add(salesAdhocNovate);
            _context.SaveChanges();
            value.SessionId = SessionId;
            value.SalesAdhocNovateId = salesAdhocNovate.SalesAdhocNovateId;
            if(value.FromCustomerId>0)
            {
                value.FromCustomerName = _context.CompanyListing.Where(s => s.CompanyListingId == value.FromCustomerId).Select(s => s.CompanyName).FirstOrDefault();

            }
            if(value.ToCustomerId>0)
            {
                value.ToCustomerName = _context.CompanyListing.Where(s => s.CompanyListingId == value.ToCustomerId).Select(s => s.CompanyName).FirstOrDefault();
            }
            if(value.SalesOrderEntryId>0)
            {
                value.ContractNo = _context.SalesOrderEntry.Where(s => s.SalesOrderEntryId == value.SalesOrderEntryId).Select(s => s.OrderNo).FirstOrDefault();
            }
           
            return value;
        }
        [HttpPut]
        [Route("UpdateSalesAdhocNovate")]
        public SalesAdhocNovateModel Put(SalesAdhocNovateModel value)
        {
            var SessionId = Guid.NewGuid();
            if (value.SessionId == null)
            { value.SessionId = SessionId; }
            if (value.Date != null && value.FromCustomerId != null && value.ToCustomerId != null  && value.ToTotalQty != null  && value.FromTotalQty != null && value.NewPono != null)
            {
                value.StatusCodeID = 2082;

            }
            if (value.Date == null || value.FromCustomerId == null || value.ToCustomerId == null  || value.ToTotalQty == null ||  value.FromTotalQty == null || value.NewPono == null)
            {
                value.StatusCodeID = 2081;
            }
            if (value.Date != null && value.FromCustomerId == null && value.ToCustomerId == null  && value.ToTotalQty == null && value.FromTotalQty == null && value.NewPono == null)
            {
                value.StatusCodeID = 2080;
            }
            var salesAdhocNovate = _context.SalesAdhocNovate.SingleOrDefault(p => p.SalesAdhocNovateId == value.SalesAdhocNovateId);
            salesAdhocNovate.SalesOrderEntryId = value.SalesOrderEntryId;
            salesAdhocNovate.Date = value.Date;
            salesAdhocNovate.ItemId = value.ItemId;
            salesAdhocNovate.FromCustomerId = value.FromCustomerId;
            salesAdhocNovate.FromStartMonth = value.FromStartMonth;
            salesAdhocNovate.FromTotalQty = value.FromTotalQty;
            salesAdhocNovate.ToCustomerId = value.ToCustomerId;
            salesAdhocNovate.ToStartMonth = value.ToStartMonth;
            salesAdhocNovate.ToTotalQty = value.ToTotalQty;
            salesAdhocNovate.IntoNoOfLots = value.IntoNoOfLots;
            salesAdhocNovate.NewPono = value.NewPono;

            salesAdhocNovate.SessionId = value.SessionId;
            salesAdhocNovate.StatusCodeId = value.StatusCodeID.Value;
            salesAdhocNovate.ModifiedByUserId = value.ModifiedByUserID;
            salesAdhocNovate.ModifiedDate = DateTime.Now;
            _context.SaveChanges();

            return value;
        }
        [HttpDelete]
        [Route("DeleteSalesAdhocNovate")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var salesAdhocNovate = _context.SalesAdhocNovate.Where(p => p.SalesAdhocNovateId == id).FirstOrDefault();
                if (salesAdhocNovate != null)
                {
                    var query = string.Format("delete from SalesAdhocNovateAttachment Where SessionId =" + "'" + "{0}" + "'", salesAdhocNovate.SessionId);
                    var rowaffected = _context.Database.ExecuteSqlRaw(query);
                    if (rowaffected <= 0)
                    {
                        throw new Exception("Failed to delete document");
                    }
                    _context.SalesAdhocNovate.Remove(salesAdhocNovate);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpPost]
        [Route("UploadSalesAdhocNovateAttachments")]
        public IActionResult UploadSalesAdhocNovateAttachments(IFormCollection files)
        {
            var sessionID = new Guid(files["sessionID"].ToString());
            var userId = files["userId"].ToString();
            var fileProfileTypeId = Convert.ToInt32(files["fileProfileTypeId"].ToString());
            var screenId = files["screenID"].ToString();
            var profile = _context.FileProfileType.FirstOrDefault(f => f.FileProfileTypeId == fileProfileTypeId);
            string profileNo = "";
            if (profile != null)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = profile.ProfileId, StatusCodeID = 710 , Title = profile.Name});
            }
            files.Files.ToList().ForEach(f =>
            {
                var file = f;
                var fs = file.OpenReadStream();
                var br = new BinaryReader(fs);
                Byte[] document = br.ReadBytes((Int32)fs.Length);

                var documents = new SalesAdhocNovateAttachment
                {
                    FileName = file.FileName,
                    ContentType = file.ContentType,
                    FileData = document,
                    FileSize = fs.Length,
                    UploadDate = DateTime.Now,
                    SessionId = sessionID,
                    FileProfileTypeId = fileProfileTypeId,
                    ProfileNo = profileNo,
                    ScreenId = screenId,

                };

                _context.SalesAdhocNovateAttachment.Add(documents);
            });
            _context.SaveChanges();
            return Content(sessionID.ToString());

        }
        [HttpGet]
        [Route("GetSalesAdhocNovateAttachment")]
        public List<SalesAdhocNovateAttachmentModel> GetSalesAdhocNovateAttachment(int? id)
        {
            var salesAdhocNovate = _context.SalesAdhocNovate.Where(w => w.SalesAdhocNovateId == id).Select(s => s.SessionId).FirstOrDefault();
            var query = _context.SalesAdhocNovateAttachment.Select(s => new SalesAdhocNovateAttachmentModel
            {
                SessionId = s.SessionId,
                SalesAdhocNovateAttachmentId = s.SalesAdhocNovateAttachmentId,
                FileName = s.FileName,
                ContentType = s.ContentType,
                FileSize = s.FileSize,
                IsImage = s.ContentType.ToLower().Contains("image") ? true : false,
            }).Where(l => l.SessionId == salesAdhocNovate).OrderByDescending(o => o.SalesAdhocNovateAttachmentId).AsNoTracking().ToList();
            return query;
        }

        [HttpGet]
        [Route("GetSalesAdhocNovateAttachmentBySessionID")]
        public List<DocumentsModel> GetSalesAdhocNovateAttachmentBySessionID(Guid? sessionId, int userId)
        {
            var query = _context.SalesAdhocNovateAttachment.Include(p => p.FileProfileType).Select(s => new DocumentsModel
            {
                SessionId = s.SessionId,
                DocumentID = s.SalesAdhocNovateAttachmentId,
                FilterProfileTypeId = s.FileProfileTypeId,
                FileProfileTypeName = s.FileProfileType != null ? s.FileProfileType.Name : string.Empty,
                ProfileNo = s.ProfileNo,
                FileName = s.FileName,
                ContentType = s.ContentType,
                FileSize = s.FileSize,
                UploadDate = s.UploadDate,
                Uploaded = true,
                ScreenID = s.ScreenId,
                IsImage = s.ContentType.ToLower().Contains("image") ? true : false,
            }).Where(l => l.SessionId == sessionId).OrderByDescending(o => o.DocumentID).AsNoTracking().ToList();
            List<long?> filterProfileTypeIds = query.Where(f => f.FilterProfileTypeId != null).Select(f => f.FilterProfileTypeId).ToList();
            var roleItems = _context.DocumentUserRole.Where(f => filterProfileTypeIds.Contains(f.FileProfileTypeId)).ToList();
            query.ForEach(q =>
            {
                q.DocumentPermissionData = new DocumentPermissionModel();
                var roles = roleItems.Where(f => f.FileProfileTypeId == q.FilterProfileTypeId).ToList();
                if (roles.Count > 0)
                {
                    var roleItem = roles.FirstOrDefault(r => r.UserId == userId);
                    if (roleItem != null)
                    {
                        DocumentPermissionController DocumentPermission = new DocumentPermissionController(_context, _mapper);
                        q.DocumentPermissionData = DocumentPermission.GetDocumentPermissionByRoll((int)roleItem.RoleId);
                    }
                    else
                    {
                        q.DocumentPermissionData.IsCreateDocument = false;
                        q.DocumentPermissionData.IsRead = false;
                        q.DocumentPermissionData.IsDelete = false;
                    }
                }
                else
                {
                    q.DocumentPermissionData.IsCreateDocument = true;
                    q.DocumentPermissionData.IsRead = true;
                    q.DocumentPermissionData.IsDelete = true;
                }
            });
            return query;
        }
        [HttpGet]
        [Route("DownLoadSalesAdhocNovateAttachment")]
        public IActionResult DownLoadSalesAdhocNovateAttachment(long id)
        {
            var document = _context.SalesAdhocNovateAttachment.SingleOrDefault(t => t.SalesAdhocNovateAttachmentId == id);
            if (document != null)
            {
                Stream stream = new MemoryStream(document.FileData);

                if (stream == null)
                    return NotFound();

                return Ok(stream);
            }
            return NotFound();
        }
        [HttpDelete]
        [Route("DeleteSalesAdhocNovateAttachment")]
        public void DeleteSalesAdhocNovateAttachment(int id)
        {
            var query = string.Format("delete from SalesAdhocNovateAttachment Where SalesAdhocNovateAttachmentId =" + "{0}", id);

            var rowaffected = _context.Database.ExecuteSqlRaw(query);
            if (rowaffected <= 0)
            {
                throw new Exception("Failed to delete document");
            }
        }



    }
}