﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ClassificationBmrController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;

        public ClassificationBmrController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
        }
        [HttpGet]
        [Route("GetClassificationBmrByRefNo")]
        public List<ClassificationBmrModel> GetClassificationBmrByRefNo(int? id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var productSimulation = _context.ProductionSimulation.ToList();
            var classificationBmr = _context.ClassificationBmr
                 .Include(a => a.AddedByUser)
                 .Include(m => m.ModifiedByUser)
                 .Include(b => b.Company)
                 .Include(p => p.ProductionStatus)
                 .Include(c => c.NavItem)
                 .Include(r => r.Profile)
                 .Include(s => s.StatusCode).AsNoTracking().ToList();
            List<ClassificationBmrModel> classificationBmrModel = new List<ClassificationBmrModel>();
            classificationBmr.ForEach(s =>
            {
                ClassificationBmrModel classificationBmrModels = new ClassificationBmrModel
                {
                    ClassificationBmrId = s.ClassificationBmrId,
                    CompanyId = s.CompanyId,
                    NavItemId = s.NavItemId,
                    ManufacturingStartDate = s.ManufacturingStartDate,
                    BatchSize = s.BatchSize,
                    TypeOfProductionId = s.TypeOfProductionId,
                    ProductionStatusId = s.ProductionStatusId,
                    NavItemName = s.ProdOrderNo,
                    ProdOrderNo = s.ProdOrderNo,
                    CompanyDatabaseName = s.Company != null ? s.Company.PlantCode : null,
                    ProductionStatusName = s.ProductionStatus != null ? s.ProductionStatus.CodeValue : null,
                    TypeOfProductionName = s.TypeOfProductionId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeOfProductionId).Select(m => m.Value).FirstOrDefault() : "",
                    AddedByUserID = s.AddedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                    ItemClassificationMasterId = s.ItemClassificationMasterId,
                    ProfileName = s.Profile != null ? s.Profile.Name : "",
                    ProfileReferenceNo = s.ProfileLinkReferenceNo,
                    SessionId = s.SessionId,
                    ProductionSimulationDate = s.ProductionSimulationDate,
                    ProductName = productSimulation != null ? productSimulation.FirstOrDefault(p => p.ProdOrderNo == s.ProdOrderNo).Description : "",

                };
                classificationBmrModel.Add(classificationBmrModels);
            });
            return classificationBmrModel.Where(l => l.ItemClassificationMasterId == id).OrderByDescending(a => a.ClassificationBmrId).ToList();
        }
        [HttpGet]
        [Route("GetClassificationBmrByProductOrderNo")]
        public List<ClassificationBmrModel> GetClassificationBmrByProductOrderNo(string ProductOrderNo)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var productSimulation = _context.ProductionSimulation.ToList();
            var classificationBmr = _context.ClassificationBmr
                 .Include(a => a.AddedByUser)
                 .Include(m => m.ModifiedByUser)
                 .Include(b => b.Company)
                 .Include(p => p.ProductionStatus)
                 .Include(c => c.NavItem)
                 .Include(r => r.Profile)
                 .Include(s => s.StatusCode).AsNoTracking().ToList();
            List<ClassificationBmrModel> classificationBmrModel = new List<ClassificationBmrModel>();
            classificationBmr.ForEach(s =>
            {
                ClassificationBmrModel classificationBmrModels = new ClassificationBmrModel
                {
                    ClassificationBmrId = s.ClassificationBmrId,
                    CompanyId = s.CompanyId,
                    NavItemId = s.NavItemId,
                    ManufacturingStartDate = s.ManufacturingStartDate,
                    BatchSize = s.BatchSize,
                    TypeOfProductionId = s.TypeOfProductionId,
                    ProductionStatusId = s.ProductionStatusId,
                    NavItemName = s.ProdOrderNo,
                    ProdOrderNo = s.ProdOrderNo,
                    CompanyDatabaseName = s.Company != null ? s.Company.PlantCode : null,
                    ProductionStatusName = s.ProductionStatus != null ? s.ProductionStatus.CodeValue : null,
                    TypeOfProductionName = s.TypeOfProductionId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeOfProductionId).Select(m => m.Value).FirstOrDefault() : "",
                    AddedByUserID = s.AddedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                    ItemClassificationMasterId = s.ItemClassificationMasterId,
                    ProfileName = s.Profile != null ? s.Profile.Name : "",
                    ProfileReferenceNo = s.ProfileLinkReferenceNo,
                    SessionId = s.SessionId,
                    ProductionSimulationDate = s.ProductionSimulationDate,
                    ProductName = productSimulation != null ? productSimulation.FirstOrDefault(p => p.ProdOrderNo == s.ProdOrderNo).Description : "",

                };
                classificationBmrModel.Add(classificationBmrModels);
            });
            return classificationBmrModel.Where(l => l.ProdOrderNo == ProductOrderNo).OrderByDescending(a => a.ClassificationBmrId).ToList();
        }
        [HttpGet]
        [Route("GetClassificationBmrByTicketNo")]
        public List<ClassificationBmrModel> GetClassificationBmrByTicketNo(int? id)
        {
            var classificationBmr = _context.ClassificationBmr
                 .Include(b => b.Company)
                 .Include(r => r.Profile)
                 .Include(l => l.ClassificationBmrLine).Where(w => w.ItemClassificationMasterId == id).AsNoTracking().ToList();
            List<ClassificationBmrModel> classificationBmrModel = new List<ClassificationBmrModel>();
            List<long?> typeOfProductionIds = classificationBmr.Where(w => w.TypeOfProductionId != null).Select(a => a.TypeOfProductionId).Distinct().ToList();
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            if (typeOfProductionIds.Count > 0)
            {
                masterDetailList = _context.ApplicationMasterDetail.Where(w => typeOfProductionIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
            }
            var ProductionSimulation = _context.ProductionSimulation.Where(w => (classificationBmr.Select(a => a.ProdOrderNo).ToList()).Contains(w.ProdOrderNo) && w.IsBmrticket == true).AsNoTracking().ToList();
            classificationBmr.ForEach(s =>
            {
                if (s.ClassificationBmrLine.Count > 0)
                {
                    s.ClassificationBmrLine.ToList().ForEach(h =>
                    {
                        ClassificationBmrModel classificationBmrModels = new ClassificationBmrModel
                        {
                            TicketNo = ProductionSimulation != null ? (ProductionSimulation.Where(w => w.ProdOrderNo == s.ProdOrderNo && s.ProductionSimulationDate != null && w.StartingDate.Month == s.ProductionSimulationDate.Value.Month && w.StartingDate.Year == s.ProductionSimulationDate.Value.Year).Select(a => a.RePlanRefNo).FirstOrDefault()) : "",
                            ClassificationBmrId = s.ClassificationBmrId,
                            CompanyDatabaseName = s.Company != null ? s.Company.PlantCode : null,
                            ProdOrderNo = s != null ? s.ProdOrderNo : "",
                            BatchSize = s != null ? s.BatchSize : "",
                            Product = h.ProductName,
                            ProductUom = h.ProductUom,
                            BatchNo = h.BatchNo,
                            CompanyId = s.CompanyId,
                            NavItemId = s.NavItemId,
                            ManufacturingStartDate = s.ManufacturingStartDate,
                            TypeOfProductionId = s.TypeOfProductionId,
                            ProductionStatusId = s.ProductionStatusId,
                            NavItemName = s.ProdOrderNo,
                            TypeOfProductionName = s.TypeOfProductionId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeOfProductionId).Select(m => m.Value).FirstOrDefault() : "",
                            AddedByUserID = s.AddedByUserId,
                            StatusCodeID = s.StatusCodeId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.AddedDate,
                            ModifiedDate = s.ModifiedDate,
                            ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                            ItemClassificationMasterId = s.ItemClassificationMasterId,
                            ProfileName = s.Profile != null ? s.Profile.Name : "",
                            ProfileReferenceNo = s.ProfileLinkReferenceNo,
                            SessionId = s.SessionId,
                            ProductionSimulationDate = s.ProductionSimulationDate,
                        };
                        classificationBmrModel.Add(classificationBmrModels);
                    });

                }
                else
                {
                    ClassificationBmrModel classificationBmrModels = new ClassificationBmrModel
                    {
                        ClassificationBmrId = s.ClassificationBmrId,
                        CompanyId = s.CompanyId,
                        NavItemId = s.NavItemId,
                        ManufacturingStartDate = s.ManufacturingStartDate,
                        BatchSize = s.BatchSize,
                        TypeOfProductionId = s.TypeOfProductionId,
                        ProductionStatusId = s.ProductionStatusId,
                        NavItemName = s.ProdOrderNo,
                        ProdOrderNo = s.ProdOrderNo,
                        CompanyDatabaseName = s.Company != null ? s.Company.PlantCode : null,
                        TypeOfProductionName = s.TypeOfProductionId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeOfProductionId).Select(m => m.Value).FirstOrDefault() : "",
                        AddedByUserID = s.AddedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                        ItemClassificationMasterId = s.ItemClassificationMasterId,
                        ProfileName = s.Profile != null ? s.Profile.Name : "",
                        ProfileReferenceNo = s.ProfileLinkReferenceNo,
                        SessionId = s.SessionId,
                        ProductionSimulationDate = s.ProductionSimulationDate,
                    };
                    classificationBmrModel.Add(classificationBmrModels);
                }
            });
            return classificationBmrModel.OrderByDescending(a => a.ClassificationBmrId).ToList();
        }
        [HttpPost]
        [Route("GetClassificationBmrSearch")]
        public List<ClassificationBmrModel> GetClassificationBmrSearch(ClassificationBmrModel SearchModel)
        {
            var classificationBmrList = _context.ClassificationBmr.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(b => b.Company).Include(c => c.NavItem).Include(r => r.Profile).Include(s => s.StatusCode).Where(w => w.ClassificationBmrId > 0 && w.ItemClassificationMasterId == SearchModel.ClassificationBmrId);
            if (SearchModel.CompanyId != null)
            {
                classificationBmrList = classificationBmrList.Where(w => w.CompanyId == SearchModel.CompanyId);
            }
            if (SearchModel.TypeOfProductionId != null)
            {
                classificationBmrList = classificationBmrList.Where(w => w.TypeOfProductionId == SearchModel.TypeOfProductionId);
            }
            if (SearchModel.NavItemName != null)
            {
                classificationBmrList = classificationBmrList.Where(w => w.ProdOrderNo == SearchModel.NavItemName);
            }
            if (SearchModel.ProductionSimulationDate != null)
            {
                classificationBmrList = classificationBmrList.Where(w => w.ProductionSimulationDate.Value.Month == SearchModel.ProductionSimulationDate.Value.Month && w.ProductionSimulationDate.Value.Year == SearchModel.ProductionSimulationDate.Value.Year);
            }
            if (SearchModel.ProductionStatusId != null)
            {
                classificationBmrList = classificationBmrList.Where(w => w.ProductionStatusId == SearchModel.ProductionStatusId);
            }
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var classificationBmr = classificationBmrList.AsNoTracking().ToList();
            List<ClassificationBmrModel> classificationBmrModel = new List<ClassificationBmrModel>();
            classificationBmr.ForEach(s =>
            {
                ClassificationBmrModel classificationBmrModels = new ClassificationBmrModel
                {
                    ClassificationBmrId = s.ClassificationBmrId,
                    CompanyId = s.CompanyId,
                    NavItemId = s.NavItemId,
                    ManufacturingStartDate = s.ManufacturingStartDate,
                    BatchSize = s.BatchSize,
                    TypeOfProductionId = s.TypeOfProductionId,
                    ProductionStatusId = s.ProductionStatusId,
                    NavItemName = s.ProdOrderNo,
                    ProdOrderNo = s.ProdOrderNo,
                    CompanyDatabaseName = s.Company != null ? s.Company.PlantCode : null,
                    ProductionStatusName = s.ProductionStatus != null ? s.ProductionStatus.CodeValue : null,
                    TypeOfProductionName = s.TypeOfProductionId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.TypeOfProductionId).Select(m => m.Value).FirstOrDefault() : "",
                    AddedByUserID = s.AddedByUserId,
                    StatusCodeID = s.StatusCodeId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    ProfileLinkReferenceNo = s.ProfileLinkReferenceNo,
                    ItemClassificationMasterId = s.ItemClassificationMasterId,
                    ProfileName = s.Profile != null ? s.Profile.Name : "",
                    ProfileReferenceNo = s.ProfileLinkReferenceNo,
                    SessionId = s.SessionId,
                    ProductionSimulationDate = s.ProductionSimulationDate,
                };
                classificationBmrModel.Add(classificationBmrModels);
            });
            return classificationBmrModel.OrderByDescending(a => a.ClassificationBmrId).ToList();
        }
        [HttpGet]
        [Route("GetRefPlanProductionSimulation")]
        public List<ProductionSimulationModel> GetRefPlanProductionSimulation(int? id, DateTime ProductionSimulationDate)
        {
            List<ProductionSimulationModel> ProductionSimulationModels = new List<ProductionSimulationModel>();
            var ProductionSimulationModel = _context.ProductionSimulation.Where(w => w.CompanyId == id && w.StartingDate.Month == ProductionSimulationDate.Month && w.StartingDate.Year == ProductionSimulationDate.Year && w.IsBmrticket == true).Select(m => new { m.ProdOrderNo, m.RePlanRefNo, m.BatchNo, m.BatchSize, m.Description }).Distinct().ToList();
            ProductionSimulationModel.ForEach(s =>
            {
                ProductionSimulationModel ProductionSimulationModel = new ProductionSimulationModel();
                ProductionSimulationModel.ProdOrderNo = s.ProdOrderNo;
                ProductionSimulationModel.RePlanRefNo = s.RePlanRefNo;
                ProductionSimulationModel.ItemName = s.RePlanRefNo + "|" + s.Description;
                ProductionSimulationModel.BatchNo = s.BatchNo;
                ProductionSimulationModel.BatchSize = s.BatchSize;
                ProductionSimulationModels.Add(ProductionSimulationModel);
            });
            return ProductionSimulationModels;
        }
        [HttpPost]
        [Route("InsertClassificationBmr")]
        public ClassificationBmrModel Post(ClassificationBmrModel value)
        {
            var profileNo = "";
            if (value.ProfileID > 0)
            {
                profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.ProdOrderNo });
            }
            var SessionId = Guid.NewGuid();
            var classificationBmr = new ClassificationBmr
            {
                CompanyId = value.CompanyId,
                NavItemId = value.NavItemId,
                ManufacturingStartDate = value.ManufacturingStartDate,
                BatchSize = value.BatchSize,
                TypeOfProductionId = value.TypeOfProductionId,
                ProductionStatusId = value.ProductionStatusId,
                ProfileLinkReferenceNo = profileNo,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                ItemClassificationMasterId = value.ItemClassificationMasterId,
                ProfileId = value.ProfileID,
                ProdOrderNo = value.ProdOrderNo,
                SessionId = SessionId,
                ProductionSimulationDate = value.ProductionSimulationDate,
            };
            _context.ClassificationBmr.Add(classificationBmr);
            _context.SaveChanges();
            value.ProfileReferenceNo = classificationBmr.ProfileLinkReferenceNo;
            value.ProfileLinkReferenceNo = classificationBmr.ProfileLinkReferenceNo;
            value.ClassificationBmrId = classificationBmr.ClassificationBmrId;
            value.SessionId = SessionId;
            ClassificationBmrLine(classificationBmr);
            return value;
        }
        private void ClassificationBmrLine(ClassificationBmr ClassificationBmr)
        {
            var ProductionSimulation = _context.ProductionSimulation.Include(n => n.Item).Where(w => w.ProdOrderNo == ClassificationBmr.ProdOrderNo && w.CompanyId == ClassificationBmr.CompanyId && w.IsBmrticket == true).ToList();
            if (ProductionSimulation != null)
            {
                var classificationBmrLineRemove = _context.ClassificationBmrLine.Where(p => p.ClassificationBmrId == ClassificationBmr.ClassificationBmrId).ToList();
                if (classificationBmrLineRemove != null)
                {
                    _context.ClassificationBmrLine.RemoveRange(classificationBmrLineRemove);
                    _context.SaveChanges();
                }
                ProductionSimulation.ForEach(value =>
                {
                    var classificationBmrLine = new ClassificationBmrLine
                    {
                        ClassificationBmrId = ClassificationBmr.ClassificationBmrId,
                        BatchNo = value.BatchNo,
                        SupplyTo = value.Item != null ? value.Item.InternalRef : null,
                        ProductNo = value.Item != null ? value.Item.No : null,
                        ProductName = value.Item != null ? value.Item.Description : null,
                        StatusCodeId = ClassificationBmr.StatusCodeId,
                        AddedByUserId = ClassificationBmr.AddedByUserId,
                        AddedDate = DateTime.Now,
                        ProductUom = value.Item != null ? value.Item.BaseUnitofMeasure : null,
                    };
                    _context.ClassificationBmrLine.Add(classificationBmrLine);
                });
                _context.SaveChanges();
            };
        }
        [HttpPut]
        [Route("UpdateClassificationBmr")]
        public ClassificationBmrModel Put(ClassificationBmrModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var classificationBmr = _context.ClassificationBmr.SingleOrDefault(p => p.ClassificationBmrId == value.ClassificationBmrId);
            classificationBmr.CompanyId = value.CompanyId;
            classificationBmr.NavItemId = value.NavItemId;
            classificationBmr.ManufacturingStartDate = value.ManufacturingStartDate;
            classificationBmr.BatchSize = value.BatchSize;
            classificationBmr.TypeOfProductionId = value.TypeOfProductionId;
            classificationBmr.ProductionStatusId = value.ProductionStatusId;
            classificationBmr.ProfileLinkReferenceNo = value.ProfileLinkReferenceNo;
            classificationBmr.StatusCodeId = value.StatusCodeID.Value;
            classificationBmr.ModifiedByUserId = value.ModifiedByUserID;
            classificationBmr.ModifiedDate = DateTime.Now;
            classificationBmr.ProfileId = value.ProfileID;
            classificationBmr.ItemClassificationMasterId = value.ItemClassificationMasterId;
            classificationBmr.ProdOrderNo = value.ProdOrderNo;
            classificationBmr.ProductionSimulationDate = value.ProductionSimulationDate;
            _context.SaveChanges();
            ClassificationBmrLine(classificationBmr);
            return value;
        }
        [HttpDelete]
        [Route("DeleteClassificationBmr")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var classificationBmr = _context.ClassificationBmr.Where(p => p.ClassificationBmrId == id).FirstOrDefault();
                if (classificationBmr != null)
                {
                    var classificationBmrLine = _context.ClassificationBmrLine.Where(b => b.ClassificationBmrId == id).ToList();
                    if (classificationBmrLine != null)
                    {
                        _context.ClassificationBmrLine.RemoveRange(classificationBmrLine);
                        _context.SaveChanges();
                    }
                    _context.ClassificationBmr.Remove(classificationBmr);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}