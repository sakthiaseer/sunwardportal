﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using APP.BussinessObject;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class MasterInterCompanyPricingController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly TableVersionRepository _repository;

        public MasterInterCompanyPricingController(CRT_TMSContext context, IMapper mapper, TableVersionRepository repository)
        {
            _context = context;
            _mapper = mapper;
            _repository = repository;
        }
        [HttpGet]
        [Route("GetMasterInterCompanyPricingVersion")]
        public async Task<IEnumerable<TableDataVersionInfoModel<MasterInterCompanyPricingModel>>> GetMasterInterCompanyPricingVersion(string sessionID)
        {
            return await _repository.GetList<MasterInterCompanyPricingModel>(sessionID);
        }
        [HttpGet]
        [Route("GetMasterInterCompanyPricings")]
        public List<MasterInterCompanyPricingModel> Get()
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var masterInterCompanyPricings = _context.MasterInterCompanyPricing
                .Include(a => a.AddedByUser)
                .Include(m => m.MasterInterCompanyPricingLine)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(c => c.FromCompany)
                .Include(c => c.ToCompany)
                .AsNoTracking()
                .ToList();
            List<MasterInterCompanyPricingModel> masterInterCompanyPricingModels = new List<MasterInterCompanyPricingModel>();
            masterInterCompanyPricings.ForEach(s =>
            {
                MasterInterCompanyPricingModel masterInterCompanyPricingModel = new MasterInterCompanyPricingModel
                {
                    MasterInterCompanyPricingId = s.MasterInterCompanyPricingId,
                    ValidityFrom = s.ValidityFrom,
                    ValidityTo = s.ValiditiyTo,
                    IsNeedVersionFunction = s.IsNeedVersionFunction,
                    FromCompanyId = s.FromCompanyId,
                    FromCompany = s.FromCompany != null ? s.FromCompany.PlantCode : "",
                    ToCompanyId = s.ToCompanyId,
                    ToCompany = s.ToCompany != null ? s.ToCompany.PlantCode : "",
                    InterCompanyMinMargin = s.InterCompanyMinMargin,
                    InterCompanyProfitSharing = s.InterCompanyProfitSharing,
                    MinBillingToCustomer = s.MinBillingToCustomer,
                    CurrencyId = s.CurrencyId,
                    CurrencyName = masterDetailList.FirstOrDefault(c => c.ApplicationMasterDetailId == s.CurrencyId) != null ? masterDetailList.FirstOrDefault(c => c.ApplicationMasterDetailId == s.CurrencyId).Value : string.Empty,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    VersionSessionId = s.VersionSessionId,

                };
                if (s.MasterInterCompanyPricingLine != null && s.MasterInterCompanyPricingLine.Count > 0)
                {
                    masterInterCompanyPricingModel.MasterInterCompanyPricingLineModels = new List<MasterInterCompanyPricingLineModel>();
                    s.MasterInterCompanyPricingLine.ToList().ForEach(l =>
                    {
                        MasterInterCompanyPricingLineModel masterInterCompanyPricingLineModel = new MasterInterCompanyPricingLineModel();
                        masterInterCompanyPricingLineModel.SellingToId = l.SellingToId;
                        masterInterCompanyPricingLineModel.SellingCompany = l.SellingTo?.CompanyName;
                        masterInterCompanyPricingLineModel.CurrencyId = l.CurrencyId;
                        masterInterCompanyPricingLineModel.Currency = masterDetailList.FirstOrDefault(c => c.ApplicationMasterDetailId == l.CurrencyId) != null ? masterDetailList.FirstOrDefault(c => c.ApplicationMasterDetailId == l.CurrencyId).Value : "";
                        masterInterCompanyPricingLineModel.BillingPercentage = l.BillingPercentage;
                        masterInterCompanyPricingLineModel.StatusCodeID = l.StatusCodeId;
                        masterInterCompanyPricingLineModel.AddedByUserID = l.AddedByUserId;
                        masterInterCompanyPricingLineModel.ModifiedByUserID = l.ModifiedByUserId;
                        masterInterCompanyPricingLineModel.AddedDate = l.AddedDate;
                        masterInterCompanyPricingLineModel.ModifiedDate = l.ModifiedDate;
                        masterInterCompanyPricingLineModel.AddedByUser = l.AddedByUser != null ? l.AddedByUser.UserName : "";
                        masterInterCompanyPricingLineModel.ModifiedByUser = l.ModifiedByUser != null ? l.ModifiedByUser.UserName : "";
                        masterInterCompanyPricingLineModel.StatusCode = l.StatusCode != null ? l.StatusCode.CodeValue : "";
                        masterInterCompanyPricingModel.MasterInterCompanyPricingLineModels.Add(masterInterCompanyPricingLineModel);

                    });

                }
                masterInterCompanyPricingModels.Add(masterInterCompanyPricingModel);
            });
            return masterInterCompanyPricingModels.OrderByDescending(a => a.MasterInterCompanyPricingId).ToList();
        }

        [HttpPost()]
        [Route("GetMasterInterCompanyPricingData")]
        public ActionResult<MasterInterCompanyPricingModel> GetMasterInterCompanyPricingData(SearchModel searchModel)
        {
            var masterInterCompanyPricing = new MasterInterCompanyPricing();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        masterInterCompanyPricing = _context.MasterInterCompanyPricing.OrderByDescending(o => o.MasterInterCompanyPricingId).FirstOrDefault();
                        break;
                    case "Last":
                        masterInterCompanyPricing = _context.MasterInterCompanyPricing.OrderByDescending(o => o.MasterInterCompanyPricingId).LastOrDefault();
                        break;
                    case "Next":
                        masterInterCompanyPricing = _context.MasterInterCompanyPricing.OrderByDescending(o => o.MasterInterCompanyPricingId).LastOrDefault();
                        break;
                    case "Previous":
                        masterInterCompanyPricing = _context.MasterInterCompanyPricing.OrderByDescending(o => o.MasterInterCompanyPricingId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        masterInterCompanyPricing = _context.MasterInterCompanyPricing.OrderByDescending(o => o.MasterInterCompanyPricingId).FirstOrDefault();
                        break;
                    case "Last":
                        masterInterCompanyPricing = _context.MasterInterCompanyPricing.OrderByDescending(o => o.MasterInterCompanyPricingId).LastOrDefault();
                        break;
                    case "Next":
                        masterInterCompanyPricing = _context.MasterInterCompanyPricing.OrderBy(o => o.MasterInterCompanyPricingId).FirstOrDefault(s => s.MasterInterCompanyPricingId > searchModel.Id);
                        break;
                    case "Previous":
                        masterInterCompanyPricing = _context.MasterInterCompanyPricing.OrderByDescending(o => o.MasterInterCompanyPricingId).FirstOrDefault(s => s.MasterInterCompanyPricingId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<MasterInterCompanyPricingModel>(masterInterCompanyPricing);
            List<MasterInterCompanyPricingLineModel> masterInterCompanyPricingLineModels = new List<MasterInterCompanyPricingLineModel>();
            if (result != null)
            {
                if (result.MasterInterCompanyPricingId > 0)
                {
                    var masterInterCompanyPricingLines = _context.MasterInterCompanyPricingLine.Include(a => a.AddedByUser)
                                                            .Include(m => m.ModifiedByUser)
                                                            .Include(s => s.StatusCode)
                                                            .Include(c => c.SellingTo)
                                                            .Where(s => s.MasterInterCompanyPricingId == result.MasterInterCompanyPricingId).ToList();
                    if (masterInterCompanyPricingLines != null && masterInterCompanyPricingLines.Count > 0)
                    {
                        masterInterCompanyPricingLines.ForEach(s =>
                        {
                            MasterInterCompanyPricingLineModel masterInterCompanyPricingLineModel = new MasterInterCompanyPricingLineModel();
                            masterInterCompanyPricingLineModel.SellingToId = s.SellingToId;
                            masterInterCompanyPricingLineModel.SellingCompany = s.SellingTo?.CompanyName;
                            masterInterCompanyPricingLineModel.CurrencyId = s.CurrencyId;
                            masterInterCompanyPricingLineModel.Currency = applicationmasterdetail.FirstOrDefault(c => c.ApplicationMasterDetailId == s.CurrencyId) != null ? applicationmasterdetail.FirstOrDefault(c => c.ApplicationMasterDetailId == s.CurrencyId).Value : "";
                            masterInterCompanyPricingLineModel.BillingPercentage = s.BillingPercentage;
                            masterInterCompanyPricingLineModel.StatusCodeID = s.StatusCodeId;
                            masterInterCompanyPricingLineModel.AddedByUserID = s.AddedByUserId;
                            masterInterCompanyPricingLineModel.ModifiedByUserID = s.ModifiedByUserId;
                            masterInterCompanyPricingLineModel.AddedDate = s.AddedDate;
                            masterInterCompanyPricingLineModel.ModifiedDate = s.ModifiedDate;
                            masterInterCompanyPricingLineModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                            masterInterCompanyPricingLineModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                            masterInterCompanyPricingLineModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                            masterInterCompanyPricingLineModels.Add(masterInterCompanyPricingLineModel);

                        });

                    }
                }
            }
            if (masterInterCompanyPricingLineModels != null && masterInterCompanyPricingLineModels.Count > 0)
            {
                result.MasterInterCompanyPricingLineModels = masterInterCompanyPricingLineModels;
            }

            return result;
        }

        [HttpPost]
        [Route("InsertMasterInterCompanyPricing")]
        public MasterInterCompanyPricingModel Post(MasterInterCompanyPricingModel value)
        {
            var SessionId = Guid.NewGuid();
            var masterInterCompanyPricing = new MasterInterCompanyPricing
            {
                FromCompanyId = value.FromCompanyId,
                ToCompanyId = value.ToCompanyId,
                ValidityFrom = value.ValidityFrom,
                ValiditiyTo = value.ValidityTo,
                InterCompanyMinMargin = value.InterCompanyMinMargin,
                InterCompanyProfitSharing = value.InterCompanyProfitSharing,
                MinBillingToCustomer = value.MinBillingToCustomer,
                IsNeedVersionFunction = value.IsNeedVersionFunction,
                CurrencyId = value.CurrencyId,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                VersionSessionId = SessionId
            };
            _context.MasterInterCompanyPricing.Add(masterInterCompanyPricing);
            _context.SaveChanges();
            value.MasterInterCompanyPricingId = masterInterCompanyPricing.MasterInterCompanyPricingId;
            value.VersionSessionId = masterInterCompanyPricing.VersionSessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdateMasterInterCompanyPricing")]
        public async Task<MasterInterCompanyPricingModel> Put(MasterInterCompanyPricingModel value)
        {
            var masterInterCompanyPricing = _context.MasterInterCompanyPricing.SingleOrDefault(p => p.MasterInterCompanyPricingId == value.MasterInterCompanyPricingId);
            if (masterInterCompanyPricing != null)
            {
                if (value.SaveVersionData)
                {
                    value.MasterInterCompanyPricingLineModels = GetMasterInterCompanyPricingLines((int)value.MasterInterCompanyPricingId);
                    var versionInfo = new TableDataVersionInfoModel<MasterBlanketOrderModel>
                    {
                        JsonData = JsonSerializer.Serialize(value),
                        AddedByUserId = value.ModifiedByUserID.Value,
                        AddedByUserID = value.ModifiedByUserID.Value,
                        AddedDate = DateTime.Now,
                        SessionId = value.VersionSessionId.Value,
                        ScreenId = value.ScreenID,
                        TableName = "MasterInterCompanyPricing",
                        PrimaryKey = value.MasterInterCompanyPricingId,
                        ReferenceInfo = value.ReferenceInfo,
                    };
                    await _repository.Insert(versionInfo);
                }
                masterInterCompanyPricing.FromCompanyId = value.FromCompanyId;
                masterInterCompanyPricing.ToCompanyId = value.ToCompanyId;
                masterInterCompanyPricing.ValidityFrom = value.ValidityFrom;
                masterInterCompanyPricing.ValiditiyTo = value.ValidityTo;
                masterInterCompanyPricing.VersionSessionId = value.VersionSessionId.Value;
                masterInterCompanyPricing.InterCompanyMinMargin = value.InterCompanyMinMargin;
                masterInterCompanyPricing.InterCompanyProfitSharing = value.InterCompanyProfitSharing;
                masterInterCompanyPricing.MinBillingToCustomer = value.MinBillingToCustomer;
                masterInterCompanyPricing.IsNeedVersionFunction = value.IsNeedVersionFunction;
                masterInterCompanyPricing.StatusCodeId = value.StatusCodeID.Value;
                masterInterCompanyPricing.ModifiedByUserId = value.ModifiedByUserID;
                masterInterCompanyPricing.ModifiedDate = DateTime.Now;
                masterInterCompanyPricing.CurrencyId = value.CurrencyId;
                _context.SaveChanges();
            }
            return value;
        }

        private List<MasterInterCompanyPricingLineModel> GetMasterInterCompanyPricingLines(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var masterInterCompanyPricingLines = _context.MasterInterCompanyPricingLine
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(s => s.SellingTo)
                .AsNoTracking()
                .Where(m => m.MasterInterCompanyPricingId == id)
                .ToList();
            List<MasterInterCompanyPricingLineModel> masterInterCompanyPricingLineModels = new List<MasterInterCompanyPricingLineModel>();
            masterInterCompanyPricingLines.ForEach(s =>
            {
                MasterInterCompanyPricingLineModel masterInterCompanyPricingLineModel = new MasterInterCompanyPricingLineModel
                {
                    MasterInterCompanyPricingLineId = s.MasterInterCompanyPricingLineId,
                    MasterInterCompanyPricingId = s.MasterInterCompanyPricingId,
                    CurrencyId = s.CurrencyId,
                    Currency = masterDetailList.FirstOrDefault(m => m.ApplicationMasterDetailId == s.CurrencyId) != null ? masterDetailList.FirstOrDefault(m => m.ApplicationMasterDetailId == s.CurrencyId).Value : "",
                    BillingPercentage = s.BillingPercentage,
                    SellingToId = s.SellingToId,
                    SellingCompany = s.SellingTo?.CompanyName,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                };
                masterInterCompanyPricingLineModels.Add(masterInterCompanyPricingLineModel);
            });
            return masterInterCompanyPricingLineModels.OrderByDescending(a => a.MasterInterCompanyPricingLineId).ToList();
        }

        [HttpPut]
        [Route("UpdateMasterInterCompanyPricingVersion")]
        public MasterInterCompanyPricingModel UpdateMasterInterCompanyPricingVersion(MasterInterCompanyPricingModel value)
        {
            var masterInterCompanyPricing = _context.MasterInterCompanyPricing.Include(i => i.MasterInterCompanyPricingLine).SingleOrDefault(p => p.MasterInterCompanyPricingId == value.MasterInterCompanyPricingId);
            masterInterCompanyPricing.FromCompanyId = value.FromCompanyId;
            masterInterCompanyPricing.ToCompanyId = value.ToCompanyId;
            masterInterCompanyPricing.ValidityFrom = value.ValidityFrom;
            masterInterCompanyPricing.ValiditiyTo = value.ValidityTo;
            masterInterCompanyPricing.InterCompanyMinMargin = value.InterCompanyMinMargin;
            masterInterCompanyPricing.InterCompanyProfitSharing = value.InterCompanyProfitSharing;
            masterInterCompanyPricing.MinBillingToCustomer = value.MinBillingToCustomer;
            masterInterCompanyPricing.IsNeedVersionFunction = value.IsNeedVersionFunction;
            masterInterCompanyPricing.StatusCodeId = value.StatusCodeID.Value;
            masterInterCompanyPricing.ModifiedByUserId = value.ModifiedByUserID;
            masterInterCompanyPricing.ModifiedDate = DateTime.Now;
            masterInterCompanyPricing.CurrencyId = value.CurrencyId;
            _context.SaveChanges();
            if (masterInterCompanyPricing.MasterInterCompanyPricingLine != null)
            {
                _context.MasterInterCompanyPricingLine.RemoveRange(masterInterCompanyPricing.MasterInterCompanyPricingLine);
                _context.SaveChanges();
            }
            value.MasterInterCompanyPricingLineModels.ForEach(value =>
            {
                var masterInterCompanyPricingLine = new MasterInterCompanyPricingLine
                {
                    MasterInterCompanyPricingId = value.MasterInterCompanyPricingId,
                    SellingToId = value.SellingToId,
                    CurrencyId = value.CurrencyId,
                    BillingPercentage = value.BillingPercentage,
                    StatusCodeId = value.StatusCodeID.Value,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                };
                _context.MasterInterCompanyPricingLine.Add(masterInterCompanyPricingLine);
            });
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteMasterInterCompanyPricing")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var masterInterCompanyPricing = _context.MasterInterCompanyPricing.Include(l => l.MasterInterCompanyPricingLine).Where(p => p.MasterInterCompanyPricingId == id).FirstOrDefault();

                if (masterInterCompanyPricing != null)
                {
                    if (masterInterCompanyPricing.VersionSessionId != null)
                    {
                        var versions = _context.TableDataVersionInfo.Include(t => t.TempVersion).Where(f => f.SessionId == masterInterCompanyPricing.VersionSessionId).ToList();
                        _context.TableDataVersionInfo.RemoveRange(versions);
                    }
                    _context.MasterInterCompanyPricing.Remove(masterInterCompanyPricing);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPut]
        [Route("CreateVersion")]
        public async Task<MasterInterCompanyPricingModel> CreateVersion(MasterInterCompanyPricingModel value)
        {
            try
            {
                value.SessionId = value.VersionSessionId.Value;
                var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
                var versionData = _context.MasterInterCompanyPricing.Include(s => s.MasterInterCompanyPricingLine).SingleOrDefault(p => p.MasterInterCompanyPricingId == value.MasterInterCompanyPricingId);

                if (versionData != null)
                {
                    var verObject = _mapper.Map<MasterInterCompanyPricingModel>(versionData);
                    verObject.MasterInterCompanyPricingLineModels = new List<MasterInterCompanyPricingLineModel>();
                    versionData.MasterInterCompanyPricingLine.ToList().ForEach(s =>
                    {
                        MasterInterCompanyPricingLineModel masterInterCompanyPricingLineModel = new MasterInterCompanyPricingLineModel
                        {
                            MasterInterCompanyPricingLineId = s.MasterInterCompanyPricingLineId,
                            MasterInterCompanyPricingId = s.MasterInterCompanyPricingId,
                            CurrencyId = s.CurrencyId,
                            Currency = masterDetailList.FirstOrDefault(m => m.ApplicationMasterDetailId == s.CurrencyId) != null ? masterDetailList.FirstOrDefault(m => m.ApplicationMasterDetailId == s.CurrencyId).Value : "",
                            BillingPercentage = s.BillingPercentage,
                            SellingToId = s.SellingToId,
                            SellingCompany = s.SellingTo?.CompanyName,
                            StatusCodeID = s.StatusCodeId,
                            AddedByUserID = s.AddedByUserId,
                            ModifiedByUserID = s.ModifiedByUserId,
                            AddedDate = s.AddedDate,
                            ModifiedDate = s.ModifiedDate,
                            AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                            ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                            StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        };

                        verObject.MasterInterCompanyPricingLineModels.Add(masterInterCompanyPricingLineModel);
                    });


                    var versionInfo = new TableDataVersionInfoModel<MasterInterCompanyPricingModel>
                    {
                        JsonData = JsonSerializer.Serialize(verObject),
                        AddedByUserId = value.ModifiedByUserID.Value,
                        AddedByUserID = value.ModifiedByUserID.Value,
                        AddedDate = DateTime.Now,
                        SessionId = value.SessionId.Value,
                        ScreenId = value.ScreenID,
                        TableName = "MasterInterCompanyPricing",
                        PrimaryKey = value.MasterInterCompanyPricingId,
                        ReferenceInfo = value.ReferenceInfo,
                    };
                    await _repository.Insert(versionInfo);
                }
                return value;
            }
            catch (Exception ex)
            {
                throw new Exception("create Version failed.");
            }
        }

        [HttpPut]
        [Route("ApplyVersion")]
        public async Task<MasterInterCompanyPricingModel> ApplyVersion(MasterInterCompanyPricingModel value)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            try
            {
                var versionData = _context.MasterInterCompanyPricing.Include(s => s.MasterInterCompanyPricingLine).SingleOrDefault(p => p.MasterInterCompanyPricingId == value.MasterInterCompanyPricingId);

                if (versionData != null)
                {

                    versionData.StatusCodeId = value.StatusCodeID.Value;
                    versionData.ModifiedByUserId = value.ModifiedByUserID;
                    versionData.ModifiedDate = DateTime.Now;
                    versionData.VersionSessionId = value.VersionSessionId;

                    _context.MasterInterCompanyPricingLine.RemoveRange(versionData.MasterInterCompanyPricingLine);
                    _context.SaveChanges();

                    value.MasterInterCompanyPricingLineModels.ForEach(s =>
                    {
                        MasterInterCompanyPricingLine masterInterCompanyPricingLineModel = new MasterInterCompanyPricingLine
                        {
                            MasterInterCompanyPricingId = s.MasterInterCompanyPricingId,
                            CurrencyId = s.CurrencyId,
                            SellingToId = s.SellingToId,
                            BillingPercentage = s.BillingPercentage,
                            StatusCodeId = s.StatusCodeID.Value,
                            AddedDate = s.AddedDate.Value,
                            ModifiedDate = s.ModifiedDate,

                        };
                        _context.MasterInterCompanyPricingLine.Add(masterInterCompanyPricingLineModel);
                    });

                    _context.SaveChanges();
                }
                return value;
            }
            catch (Exception ex)
            {
                throw new Exception("Version update changes failed.");
            }
        }
        [HttpPut]
        [Route("TempVersion")]
        public async Task<long> TempVersion(MasterInterCompanyPricingModel value)
        {
            value.SessionId = value.VersionSessionId.Value;
            var masterInterCompanyPricing = _context.MasterInterCompanyPricing.SingleOrDefault(p => p.MasterInterCompanyPricingId == value.MasterInterCompanyPricingId);

            if (masterInterCompanyPricing != null)
            {
                var verObject = _mapper.Map<MasterInterCompanyPricingModel>(masterInterCompanyPricing);
                if (verObject.MasterInterCompanyPricingLineModels != null && verObject.MasterInterCompanyPricingLineModels.Count > 0)
                {
                    verObject.MasterInterCompanyPricingLineModels.ForEach(s =>
                    {
                        MasterInterCompanyPricingLine masterInterCompanyPricingLineModel = new MasterInterCompanyPricingLine
                        {
                            MasterInterCompanyPricingId = s.MasterInterCompanyPricingId,
                            CurrencyId = s.CurrencyId,
                            SellingToId = s.SellingToId,
                            BillingPercentage = s.BillingPercentage,
                            StatusCodeId = s.StatusCodeID.Value,
                            AddedDate = s.AddedDate.Value,
                            ModifiedDate = s.ModifiedDate,

                        };
                        _context.MasterInterCompanyPricingLine.Add(masterInterCompanyPricingLineModel);
                    });
                }
                var versionInfo = new TableDataVersionInfoModel<MasterInterCompanyPricingModel>
                {
                    JsonData = JsonSerializer.Serialize(verObject),
                    AddedByUserId = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedByUserID = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedDate = DateTime.Now,
                    SessionId = value.SessionId.Value,
                    ScreenId = value.ScreenID,
                    TableName = "MasterInterCompanyPricing",
                    PrimaryKey = value.MasterInterCompanyPricingId,
                    ReferenceInfo = "Temp Version - undo",
                };
                var result = await _repository.Insert(versionInfo);
                return result.VersionTableId;
            }
            return -1;
        }
        [HttpGet]
        [Route("UndoVersion")]
        public async Task<MasterInterCompanyPricingModel> UndoVersion(int Id)
        {
            try
            {
                var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

                if (tableData != null)
                {
                    var verObject = JsonSerializer.Deserialize<MasterInterCompanyPricingModel>(tableData.JsonData);

                    var masterInterCompanyPricing = _context.MasterInterCompanyPricing.Include(s=>s.MasterInterCompanyPricingLine).SingleOrDefault(p => p.MasterInterCompanyPricingId == verObject.MasterInterCompanyPricingId);


                    if (verObject != null && masterInterCompanyPricing != null)
                    {
                        masterInterCompanyPricing.FromCompanyId = verObject.FromCompanyId;
                        masterInterCompanyPricing.ToCompanyId = verObject.ToCompanyId;
                        masterInterCompanyPricing.ValidityFrom = verObject.ValidityFrom;
                        masterInterCompanyPricing.ValiditiyTo = verObject.ValidityTo;
                        masterInterCompanyPricing.InterCompanyMinMargin = verObject.InterCompanyMinMargin;
                        masterInterCompanyPricing.InterCompanyProfitSharing = verObject.InterCompanyProfitSharing;
                        masterInterCompanyPricing.MinBillingToCustomer = verObject.MinBillingToCustomer;
                        masterInterCompanyPricing.IsNeedVersionFunction = verObject.IsNeedVersionFunction;
                        masterInterCompanyPricing.CurrencyId = verObject.CurrencyId;
                        masterInterCompanyPricing.StatusCodeId = verObject.StatusCodeID.Value;
                        masterInterCompanyPricing.AddedByUserId = verObject.AddedByUserID;
                        masterInterCompanyPricing.AddedDate = verObject.AddedDate.Value;
                        masterInterCompanyPricing.VersionSessionId = verObject.SessionId;
                        if (masterInterCompanyPricing.MasterInterCompanyPricingLine != null && masterInterCompanyPricing.MasterInterCompanyPricingLine.Count > 0)
                        {
                            _context.MasterInterCompanyPricingLine.RemoveRange(masterInterCompanyPricing.MasterInterCompanyPricingLine);
                            _context.SaveChanges();
                        }
                        if (verObject.MasterInterCompanyPricingLineModels != null && verObject.MasterInterCompanyPricingLineModels.Count > 0)
                        {
                            verObject.MasterInterCompanyPricingLineModels.ForEach(s =>
                            {
                                MasterInterCompanyPricingLine masterInterCompanyPricingLineModel = new MasterInterCompanyPricingLine
                                {
                                    MasterInterCompanyPricingId = s.MasterInterCompanyPricingId,
                                    CurrencyId = s.CurrencyId,
                                    SellingToId = s.SellingToId,
                                    BillingPercentage = s.BillingPercentage,
                                    StatusCodeId = s.StatusCodeID.Value,
                                    AddedDate = s.AddedDate.Value,
                                    ModifiedDate = s.ModifiedDate,

                                };
                                _context.MasterInterCompanyPricingLine.Add(masterInterCompanyPricingLineModel);
                            });
                        }
                        _context.SaveChanges();
                    }

                    return verObject;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Undo Version failed.");
            }
            return new MasterInterCompanyPricingModel();
        }
        [HttpDelete]
        [Route("DeleteVersion")]
        public async Task<long> DeleteVersion(int Id)
        {

            var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

            if (tableData != null)
            {
                _context.TableDataVersionInfo.Remove(tableData);
                _context.SaveChanges();

            }
            return -1;
        }

    }
}

