﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.BussinessObject;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SobyCustomersController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly ApprovalService _approvalService;

        public SobyCustomersController(CRT_TMSContext context, IMapper mapper, GenerateDocumentNoSeries generate, ApprovalService approvalService)
        {
            _context = context;
            _mapper = mapper;
            _generateDocumentNoSeries = generate;
            _approvalService = approvalService;
        }
        [HttpGet]
        [Route("GetSobyCustomersAddressMigration")]
        public List<SobyCustomersAddressModel> GetSobyCustomersAddressMigration()
        {
            List<SobyCustomersAddressModel> SobyCustomersAddressModel = new List<SobyCustomersAddressModel>();

            var SobyCustomersMasterAddress = _context.SobyCustomersMasterAddress.Where(w => w.AddressTypeId == 1701).ToList();
            SobyCustomersMasterAddress.ForEach(value =>
             {
                 var SobyCustomersAddress = _context.SobyCustomersAddress.SingleOrDefault(s => s.SobyCustomersAddressId == value.RefKeyNo);
                 SobyCustomersAddress.SobyCustomersMasterAddressId = value.SobyCustomersMasterAddressId;

             });
            _context.SaveChanges();
            var sobycustomerssalesaddress = _context.SobyCustomersMasterAddress.Where(w => w.AddressTypeId == 1671).ToList();
            sobycustomerssalesaddress.ForEach(value =>
            {
                var SobyCustomersSalesAddress = _context.SobyCustomersSalesAddress.SingleOrDefault(s => s.SobyCustomersSalesAddressId == value.RefKeyNo);
                SobyCustomersSalesAddress.SobyCustomersMasterAddressId = value.SobyCustomersMasterAddressId;
            });
            _context.SaveChanges();
            return SobyCustomersAddressModel;
        }
        [HttpPost]
        [Route("GetSobyCustomersByRefNo")]
        public List<SobyCustomersModel> GetSobyCustomersByRefNo(RefSearchModel refSearchModel)
        {
            var sobyCustomers = _context.SobyCustomers
               .AsNoTracking().ToList();
            List<SobyCustomersModel> sobyCustomersModel = new List<SobyCustomersModel>();
            var sobycustomersIds = sobyCustomers.Select(s => s.SobyCustomersId).ToList();
            var userIds = sobyCustomers.Select(s => s.AddedByUserId.GetValueOrDefault(0)).ToList();
            userIds.AddRange(sobyCustomers.Select(s => s.ModifiedByUserId.GetValueOrDefault(0)).ToList());
            var codeIds = sobyCustomers.Select(s => s.StatusCodeId.GetValueOrDefault(0)).ToList();
            codeIds.AddRange(sobyCustomers.Select(s => s.BillingAddressStatusId.GetValueOrDefault(0)).ToList());
            var masterIds = sobyCustomers.Select(s => s.PaymentModeId.GetValueOrDefault(0)).ToList();
            masterIds.AddRange(sobyCustomers.Select(s => s.ActivationOfOrderId.GetValueOrDefault(0)).ToList());
            var sobyCustomersMasterAddressID = _context.SobyCustomersSalesAddress.Select(s => new
            {
                s.SobyCustomersId,
                s.SobyCustomersMasterAddressId,
                s.SobyCustomersSalesAddressId,
            }).Where(w => w.SobyCustomersMasterAddressId != null && sobycustomersIds.Contains(w.SobyCustomersId.Value)).ToList();
            var sobyCustomersMasterAddressIDs = sobyCustomersMasterAddressID.Select(s => s.SobyCustomersMasterAddressId).ToList();
            var SobyCustomersMasterAddress = _context.SobyCustomersMasterAddress.Select(s => new
            {
                s.SobyCustomersId,
                s.SobyCustomersMasterAddressId,
                s.AddressTypeId,
                s.AddressName,
                s.Address,
                s.Address2,
                s.PostalCode,
                s.ContactName,
                s.PhoneNo,
                s.EmailAddress,
                s.FaxNo,
                s.Uenno,
                s.Vatgstno,
                s.CompanyRegisterationNo,
                s.CountryId,
                s.StateId,
                s.CityId,
                s.DeliveryInformation,
            }).Where(w => sobyCustomersMasterAddressIDs.Contains(w.SobyCustomersMasterAddressId)).ToList();
            masterIds.AddRange(SobyCustomersMasterAddress.Select(s => s.CountryId.GetValueOrDefault(0)).ToList());
            masterIds.AddRange(SobyCustomersMasterAddress.Select(s => s.StateId.GetValueOrDefault(0)).ToList());
            masterIds.AddRange(SobyCustomersMasterAddress.Select(s => s.CityId.GetValueOrDefault(0)).ToList());
            codeIds.AddRange(SobyCustomersMasterAddress.Select(s => s.AddressTypeId.GetValueOrDefault(0)).ToList());
            var appUsers = _context.ApplicationUser.Select(s => new
            {
                s.UserName,
                s.UserId,
            }).Where(w => userIds.Contains(w.UserId)).AsNoTracking().ToList();
            var codeMasters = _context.CodeMaster.Select(s => new
            {
                s.CodeValue,
                s.CodeId,
            }).Where(w => codeIds.Contains(w.CodeId)).AsNoTracking().ToList();

            var masters = _context.ApplicationMasterDetail.Select(s => new
            {
                s.ApplicationMasterDetailId,
                s.Value,
            }).Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
            sobyCustomers.ForEach(s =>
            {
                SobyCustomersModel sobyCustomersModels = new SobyCustomersModel();
                sobyCustomersModels.SobyCustomersId = s.SobyCustomersId;
                sobyCustomersModels.StatusCodeID = s.StatusCodeId;
                sobyCustomersModels.AddedByUserID = s.AddedByUserId;
                sobyCustomersModels.ModifiedByUserID = s.ModifiedByUserId;
                sobyCustomersModels.AddedDate = s.AddedDate;
                sobyCustomersModels.ModifiedDate = s.ModifiedDate;
                sobyCustomersModels.AddedByUser = appUsers != null ? appUsers.FirstOrDefault(a => a.UserId == s.AddedByUserId)?.UserName : "";
                sobyCustomersModels.ModifiedByUser = appUsers != null ? appUsers.FirstOrDefault(a => a.UserId == s.ModifiedByUserId)?.UserName : "";
                sobyCustomersModels.StatusCode = codeMasters != null ? codeMasters.FirstOrDefault(a => a.CodeId == s.StatusCodeId)?.CodeValue : "";
                sobyCustomersModels.ProfileLinkReferenceNo = s.ProfileLinkReferenceNo;
                sobyCustomersModels.LinkProfileReferenceNo = s.LinkProfileReferenceNo;
                sobyCustomersModels.MasterProfileReferenceNo = s.MasterProfileReferenceNo;
                sobyCustomersModels.PaymentModeId = s.PaymentModeId;
                sobyCustomersModels.ActivationOfOrderId = s.ActivationOfOrderId;
                sobyCustomersModels.BillingAddressStatusId = s.BillingAddressStatusId;
                sobyCustomersModels.BillingAddressStatusName = codeMasters != null ? codeMasters.FirstOrDefault(a => a.CodeId == s.BillingAddressStatusId)?.CodeValue : "";
                sobyCustomersModels.PaymentModeName = masters != null ? masters.FirstOrDefault(a => a.ApplicationMasterDetailId == s.PaymentModeId)?.Value : "";
                sobyCustomersModels.ActivationOfOrderName = masters != null ? masters.FirstOrDefault(a => a.ApplicationMasterDetailId == s.ActivationOfOrderId)?.Value : "";
                SobyCustomersSalesAddressModel defaultAddress = new SobyCustomersSalesAddressModel();

                var defaultAddresItems = SobyCustomersMasterAddress.FirstOrDefault(w => w.SobyCustomersId == s.SobyCustomersId && w.AddressTypeId == 1671);
                defaultAddress.AddressTypeId = 1671;
                if (defaultAddresItems != null)
                {
                    defaultAddress.SobyCustomersSalesAddressId = (long)sobyCustomersMasterAddressID.FirstOrDefault(a => a.SobyCustomersMasterAddressId == defaultAddresItems.SobyCustomersMasterAddressId)?.SobyCustomersSalesAddressId;
                    defaultAddress.SobyCustomersMasterAddressId = defaultAddresItems?.SobyCustomersMasterAddressId;
                    defaultAddress.SobyCustomersId = defaultAddresItems?.SobyCustomersId;
                    defaultAddress.AddressTypeId = defaultAddresItems?.AddressTypeId;
                    defaultAddress.AddressName = defaultAddresItems?.AddressName;
                    defaultAddress.Address = defaultAddresItems?.Address;
                    defaultAddress.Address2 = defaultAddresItems?.Address2;
                    defaultAddress.State = masters != null && defaultAddresItems!=null ? masters.FirstOrDefault(a => a.ApplicationMasterDetailId == defaultAddresItems?.StateId.GetValueOrDefault(0))?.Value : "";
                    defaultAddress.PostalCode = defaultAddresItems?.PostalCode;
                    defaultAddress.City = masters != null && defaultAddresItems != null ? masters.FirstOrDefault(a => a.ApplicationMasterDetailId == defaultAddresItems?.CityId.GetValueOrDefault(0))?.Value : "";
                    defaultAddress.Country = masters != null && defaultAddresItems != null ? masters.FirstOrDefault(a => a.ApplicationMasterDetailId == defaultAddresItems?.CountryId.GetValueOrDefault(0))?.Value : "";
                    defaultAddress.ContactName = defaultAddresItems?.ContactName;
                    defaultAddress.PhoneNo = defaultAddresItems?.PhoneNo;
                    defaultAddress.EmailAddress = defaultAddresItems?.EmailAddress;
                    defaultAddress.FaxNo = defaultAddresItems?.FaxNo;
                    defaultAddress.Uenno = defaultAddresItems?.Uenno;
                    defaultAddress.Vatgstno = defaultAddresItems?.Vatgstno;
                    defaultAddress.CompanyRegisterationNo = defaultAddresItems?.CompanyRegisterationNo;
                    defaultAddress.CountryId = defaultAddresItems?.CountryId;
                    defaultAddress.StateId = defaultAddresItems?.StateId;
                    defaultAddress.CityId = defaultAddresItems?.CityId;
                    defaultAddress.DeliveryInformation = defaultAddresItems?.DeliveryInformation;
                }
                sobyCustomersModels.DefaultAddress = defaultAddress;
                SobyCustomersSalesAddressModel FinanceDepartment = new SobyCustomersSalesAddressModel();
                FinanceDepartment.AddressTypeId = 1674;
                var financeDepartmentItems = SobyCustomersMasterAddress.FirstOrDefault(w => w.SobyCustomersId == s.SobyCustomersId && w.AddressTypeId == 1674);
                if (financeDepartmentItems != null)
                {
                    FinanceDepartment.SobyCustomersSalesAddressId = (long)sobyCustomersMasterAddressID.FirstOrDefault(a => a.SobyCustomersMasterAddressId == financeDepartmentItems.SobyCustomersMasterAddressId)?.SobyCustomersSalesAddressId;
                    FinanceDepartment.SobyCustomersMasterAddressId = financeDepartmentItems.SobyCustomersMasterAddressId;
                    FinanceDepartment.SobyCustomersId = financeDepartmentItems.SobyCustomersId;
                    FinanceDepartment.AddressTypeId = financeDepartmentItems?.AddressTypeId;
                    FinanceDepartment.Address = financeDepartmentItems?.Address;
                    FinanceDepartment.Address2 = financeDepartmentItems?.Address2;
                    FinanceDepartment.PostalCode = financeDepartmentItems?.PostalCode;
                    FinanceDepartment.ContactName = financeDepartmentItems?.ContactName;
                    FinanceDepartment.PhoneNo = financeDepartmentItems?.PhoneNo;
                    FinanceDepartment.EmailAddress = financeDepartmentItems?.EmailAddress;
                    FinanceDepartment.State = masters != null && defaultAddresItems != null ? masters.FirstOrDefault(a => a.ApplicationMasterDetailId == financeDepartmentItems?.StateId.GetValueOrDefault(0))?.Value : "";
                    FinanceDepartment.City = masters != null && defaultAddresItems != null ? masters.FirstOrDefault(a => a.ApplicationMasterDetailId == financeDepartmentItems?.CityId.GetValueOrDefault(0))?.Value : "";
                    FinanceDepartment.Country = masters != null && defaultAddresItems != null ? masters.FirstOrDefault(a => a.ApplicationMasterDetailId == financeDepartmentItems?.CountryId.GetValueOrDefault(0))?.Value : ""; 
                    FinanceDepartment.FaxNo = financeDepartmentItems?.FaxNo;
                    FinanceDepartment.Uenno = financeDepartmentItems?.Uenno;
                    FinanceDepartment.Vatgstno = financeDepartmentItems?.Vatgstno;
                    FinanceDepartment.CompanyRegisterationNo = financeDepartmentItems?.CompanyRegisterationNo;
                    FinanceDepartment.AddressName = financeDepartmentItems?.AddressName;
                    FinanceDepartment.CountryId = financeDepartmentItems?.CountryId;
                    FinanceDepartment.StateId = financeDepartmentItems?.StateId;
                    FinanceDepartment.CityId = financeDepartmentItems?.CityId;
                }
                sobyCustomersModels.FinanceDepartment = FinanceDepartment;
                sobyCustomersModel.Add(sobyCustomersModels);
            });
            if (refSearchModel.IsHeader)
            {
                return sobyCustomersModel.Where(r => r.LinkProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.SobyCustomersId).ToList();
            }
            return sobyCustomersModel.Where(r => r.MasterProfileReferenceNo == refSearchModel.ProfileReferenceNo).OrderByDescending(o => o.SobyCustomersId).ToList();
        }
        [HttpGet]
        [Route("GetSobyCustomersMasterAddress")]
        public SobyCustomersMasterAddressModel GetSobyCustomersMasterAddress(string RefNo)
        {
            SobyCustomersMasterAddressModel defaultAddress = new SobyCustomersMasterAddressModel();
            var SobyCustomersId = _context.SobyCustomers.FirstOrDefault(s => s.LinkProfileReferenceNo == RefNo)?.SobyCustomersId;
            if (SobyCustomersId != null)
            {
                List<int?> ids = new List<int?>() { 1671, 1673 };
                var SobyCustomersMasterAddress = _context.SobyCustomersMasterAddress.Where(w => w.SobyCustomersId == SobyCustomersId && ids.Contains(w.AddressTypeId)).ToList();
                var defaultAddresItems = SobyCustomersMasterAddress.FirstOrDefault(w => w.SobyCustomersId == SobyCustomersId && w.AddressTypeId == 1671);
                var contactAddress =SobyCustomersMasterAddress.FirstOrDefault(w => w.SobyCustomersId == SobyCustomersId && w.AddressTypeId == 1673);
                var masterIds = SobyCustomersMasterAddress.Select(s => s.CityId.GetValueOrDefault(0)).ToList();
                masterIds.AddRange(SobyCustomersMasterAddress.Select(s => s.CountryId.GetValueOrDefault(0)).ToList());
                masterIds.AddRange(SobyCustomersMasterAddress.Select(s => s.StateId.GetValueOrDefault(0)).ToList());
                var masters = _context.ApplicationMasterDetail.Select(s => new
                {
                    s.ApplicationMasterDetailId,
                    s.Value,
                }).Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                if (defaultAddresItems != null)
                {
                    defaultAddress.SobyCustomersMasterAddressId = defaultAddresItems.SobyCustomersMasterAddressId;
                    defaultAddress.SobyCustomersId = defaultAddresItems.SobyCustomersId;
                    defaultAddress.AddressTypeId = defaultAddresItems.AddressTypeId.Value;
                    defaultAddress.AddressName = defaultAddresItems.AddressName;
                    defaultAddress.Address = defaultAddresItems.Address;
                    defaultAddress.Address2 = defaultAddresItems.Address2;
                    defaultAddress.PostalCode = defaultAddresItems.PostalCode;
                    defaultAddress.State = masters != null && defaultAddresItems != null ? masters.FirstOrDefault(a => a.ApplicationMasterDetailId == defaultAddresItems?.StateId.GetValueOrDefault(0))?.Value : "";
                    defaultAddress.City = masters != null && defaultAddresItems != null ? masters.FirstOrDefault(a => a.ApplicationMasterDetailId == defaultAddresItems?.CityId.GetValueOrDefault(0))?.Value : "";
                    defaultAddress.Country = masters != null && defaultAddresItems != null ? masters.FirstOrDefault(a => a.ApplicationMasterDetailId == defaultAddresItems?.CountryId.GetValueOrDefault(0))?.Value : "";
                    defaultAddress.ContactName = contactAddress?.Name;
                    defaultAddress.PhoneNo = contactAddress?.PhoneNo;
                    defaultAddress.EmailAddress = defaultAddresItems.EmailAddress;
                    defaultAddress.FaxNo = contactAddress?.FaxNo;
                    defaultAddress.Uenno = defaultAddresItems.Uenno;
                    defaultAddress.Vatgstno = defaultAddresItems.Vatgstno;
                    defaultAddress.CompanyRegisterationNo = defaultAddresItems.CompanyRegisterationNo;
                    defaultAddress.CountryId = defaultAddresItems.CountryId;
                    defaultAddress.StateId = defaultAddresItems.StateId;
                    defaultAddress.CityId = defaultAddresItems.CityId;
                    defaultAddress.DeliveryInformation = defaultAddresItems.DeliveryInformation;
                }
            }
            return defaultAddress;
        }
        [HttpGet]
        [Route("GetSobyCustomersAddress")]
        public List<SobyCustomersAddressModel> Get(int? id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();

            var sobyCustomersAddress = _context.SobyCustomersAddress.Include(m => m.SobyCustomersMasterAddress).Include(t => t.TypeOfAddress).Where(s => s.SobyCustomersId == id).AsNoTracking().ToList();
            List<SobyCustomersAddressModel> sobyCustomersAddressModels = new List<SobyCustomersAddressModel>();
            sobyCustomersAddress.ForEach(s =>
            {
                SobyCustomersAddressModel sobyCustomersAddressModel = new SobyCustomersAddressModel();
                sobyCustomersAddressModel.SobyCustomersAddressId = s.SobyCustomersAddressId;
                sobyCustomersAddressModel.SobyCustomersId = s.SobyCustomersId;
                sobyCustomersAddressModel.LocationNo = s.LocationNo;
                sobyCustomersAddressModel.LocationName = s.LocationName;
                sobyCustomersAddressModel.AddressTypeId = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.AddressTypeId : 1672;
                sobyCustomersAddressModel.Address = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.Address : null;
                sobyCustomersAddressModel.PostalCode = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.PostalCode : null;
                sobyCustomersAddressModel.CountryId = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.CountryId : null;
                sobyCustomersAddressModel.Country = s.SobyCustomersMasterAddress != null && s.SobyCustomersMasterAddress.CountryId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.SobyCustomersMasterAddress.CountryId).Select(m => m.Value).FirstOrDefault() : "";
                sobyCustomersAddressModel.InvoiceAddress = s.InvoiceAddress;
                sobyCustomersAddressModel.InvoiceAddressFlag = s.InvoiceAddress == true ? "Yes" : "No";
                sobyCustomersAddressModel.DeliverySchedule = s.DeliverySchedule;
                sobyCustomersAddressModel.Address2 = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.Address2 : null;
                sobyCustomersAddressModel.State = s.SobyCustomersMasterAddress != null && s.SobyCustomersMasterAddress.StateId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.SobyCustomersMasterAddress.StateId).Select(m => m.Value).FirstOrDefault() : "";
                sobyCustomersAddressModel.StateId = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.StateId : null;
                sobyCustomersAddressModel.PhoneNo = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.PhoneNo : null;
                sobyCustomersAddressModel.ContactName = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.ContactName : null;
                sobyCustomersAddressModel.City = s.SobyCustomersMasterAddress != null && s.SobyCustomersMasterAddress.CityId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.SobyCustomersMasterAddress.CityId).Select(m => m.Value).FirstOrDefault() : "";
                sobyCustomersAddressModel.CityId = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.CityId : null;
                sobyCustomersAddressModel.TypeOfAddressId = s.TypeOfAddressId;
                sobyCustomersAddressModel.NavisionNo = s.NavisionNo;
                sobyCustomersAddressModel.EmailAddress = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.EmailAddress : null;
                sobyCustomersAddressModel.CustomerCodeId = s.CustomerCodeId;
                sobyCustomersAddressModel.CustomerCode = s.CustomerCodeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CustomerCodeId).Select(m => m.Value).FirstOrDefault() : "";
                sobyCustomersAddressModel.TypeOfAddress = s.TypeOfAddress != null ? s.TypeOfAddress.CodeValue : null;
                sobyCustomersAddressModel.FaxNo = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.FaxNo : null;
                sobyCustomersAddressModel.Uenno = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.Uenno : null;
                sobyCustomersAddressModel.CompanyRegisterationNo = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.CompanyRegisterationNo : null;
                sobyCustomersAddressModel.Vatgstno = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.Vatgstno : null;
                sobyCustomersAddressModel.StatusCodeID = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.StatusCodeId : null;
                sobyCustomersAddressModel.StatusCode = s.SobyCustomersMasterAddress != null && s.SobyCustomersMasterAddress.StatusCode != null ? s.SobyCustomersMasterAddress.StatusCode.CodeValue : null;
                sobyCustomersAddressModel.SobyCustomersMasterAddressId = s.SobyCustomersMasterAddressId;
                sobyCustomersAddressModel.DeliveryInformation = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.DeliveryInformation : null;
                sobyCustomersAddressModels.Add(sobyCustomersAddressModel);
            });
            return sobyCustomersAddressModels.OrderByDescending(a => a.SobyCustomersAddressId).ToList();
        }
        [HttpGet]
        [Route("GetSobyCustomersContactInfo")]
        public List<SobyCustomerContactInfoModel> GetSobyCustomersContactInfo(int? id)
        {
            var sobyCustomersAddress = _context.SobyCustomerContactInfo.Include(t => t.SobyCustomersMasterAddress).Include(l => l.StationLocation).Include(a => a.SobyCustomersMasterAddress.StatusCode).Where(s => s.SobyCustomersId == id).AsNoTracking().ToList();
            List<SobyCustomerContactInfoModel> sobyCustomersAddressModels = new List<SobyCustomerContactInfoModel>();
            sobyCustomersAddress.ForEach(s =>
            {
                SobyCustomerContactInfoModel sobyCustomersAddressModel = new SobyCustomerContactInfoModel();
                sobyCustomersAddressModel.SobyCustomerContactInfoId = s.SobyCustomerContactInfoId;
                sobyCustomersAddressModel.SobyCustomersId = s.SobyCustomersId;
                sobyCustomersAddressModel.StationLocationId = s.StationLocationId;
                sobyCustomersAddressModel.StationLocationName = s.StationLocation != null ? s.StationLocation.AddressName : null;
                sobyCustomersAddressModel.SobyCustomersMasterAddressId = s.SobyCustomersMasterAddressId;
                sobyCustomersAddressModel.Department = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.Department : null;
                sobyCustomersAddressModel.Designation = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.Designation : null;
                sobyCustomersAddressModel.EmailAddress = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.EmailAddress : null;
                sobyCustomersAddressModel.Name = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.Name : null;
                sobyCustomersAddressModel.NickName = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.NickName : null;
                sobyCustomersAddressModel.PhoneNumber = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.PhoneNo : null;
                sobyCustomersAddressModel.FaxNumber = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.FaxNo : null;
                sobyCustomersAddressModel.StatusCodeID = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.StatusCodeId : null;
                sobyCustomersAddressModel.StatusCode = s.SobyCustomersMasterAddress != null && s.SobyCustomersMasterAddress.StatusCode != null ? s.SobyCustomersMasterAddress.StatusCode.CodeValue : null;
                sobyCustomersAddressModel.AddressTypeId = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.AddressTypeId : null;
                sobyCustomersAddressModels.Add(sobyCustomersAddressModel);
            });
            return sobyCustomersAddressModels.OrderByDescending(a => a.SobyCustomerContactInfoId).ToList();
        }
        [HttpGet]
        [Route("GetSoCustomersDelivery")]
        public List<SocustomersDeliveryModel> GetSoCustomersDelivery(int? id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var SocustomersDelivery = _context.SocustomersDelivery.Include(a => a.AddedByUser)
               .Include(m => m.ModifiedByUser).Include(mm => mm.SobyCustomerModeDelivery).Include(p => p.SobyCustomersTypeOfPermit)
               .Include(s => s.StatusCode).Where(s => s.SocustomersId == id).AsNoTracking().ToList();
            List<SocustomersDeliveryModel> SocustomersDeliveryModels = new List<SocustomersDeliveryModel>();
            SocustomersDelivery.ForEach(s =>
            {
                SocustomersDeliveryModel SocustomersDeliveryModel = new SocustomersDeliveryModel();
                SocustomersDeliveryModel.SocustomersDeliveryId = s.SocustomersDeliveryId;
                SocustomersDeliveryModel.SocustomersId = s.SocustomersId;
                SocustomersDeliveryModel.RequireSpecialPermit = s.RequireSpecialPermit;
                SocustomersDeliveryModel.RequireSpecialPermitFlag = s.RequireSpecialPermit == true ? "Yes" : "No";
                SocustomersDeliveryModel.StatusCodeID = s.StatusCodeId;
                SocustomersDeliveryModel.AddedByUserID = s.AddedByUserId;
                SocustomersDeliveryModel.ModifiedByUserID = s.ModifiedByUserId;
                SocustomersDeliveryModel.AddedDate = s.AddedDate;
                SocustomersDeliveryModel.ModifiedDate = s.ModifiedDate;
                SocustomersDeliveryModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                SocustomersDeliveryModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                SocustomersDeliveryModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                SocustomersDeliveryModel.ModeofDeliveryIds = s.SobyCustomerModeDelivery.Where(b => b.SocustomersDeliveryId == s.SocustomersDeliveryId).Select(b => b.ModeOfDeliveryId).ToList();
                SocustomersDeliveryModel.DeliveryTyperOfPermitIds = s.SobyCustomersTypeOfPermit.Where(b => b.SocustomersDeliveryId == s.SocustomersDeliveryId && b.PermitType == "Delivery").Select(b => b.TypeOfPermitId).ToList();
                SocustomersDeliveryModel.ModeofDeliveryItems = s.SobyCustomerModeDelivery != null && masterDetailList != null ? string.Join(",", s.SobyCustomerModeDelivery.Where(b => b.SocustomersDeliveryId == s.SocustomersDeliveryId).Select(b => masterDetailList.Where(m => m.ApplicationMasterDetailId == b.ModeOfDeliveryId).Select(m => m.Value).FirstOrDefault()).ToList()) : "";
                SocustomersDeliveryModel.DeliveryTyperOfPermitItems = s.SobyCustomersTypeOfPermit != null && masterDetailList != null ? string.Join(",", s.SobyCustomersTypeOfPermit.Where(b => b.SocustomersDeliveryId == s.SocustomersDeliveryId && b.PermitType == "Delivery").Select(b => masterDetailList.Where(m => m.ApplicationMasterDetailId == b.TypeOfPermitId).Select(m => m.Value).FirstOrDefault()).ToList()) : "";
                SocustomersDeliveryModels.Add(SocustomersDeliveryModel);
            });
            return SocustomersDeliveryModels.OrderByDescending(a => a.SocustomersDeliveryId).ToList();
        }
        [HttpGet]
        [Route("GetSocustomersItemCrossReference")]
        public List<SocustomersItemCrossReferenceModel> GetSocustomersItemCrossReference(int? id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var SocustomersItemCrossReference = _context.SocustomersItemCrossReference.Include(a => a.AddedByUser)
               .Include(m => m.ModifiedByUser).Include(p => p.SobyCustomersTypeOfPermit).Include(n => n.NavItem)
               .Include(s => s.StatusCode).Where(s => s.SobyCustomersId == id).AsNoTracking().ToList();
            List<SocustomersItemCrossReferenceModel> SocustomersItemCrossReferenceModels = new List<SocustomersItemCrossReferenceModel>();
            SocustomersItemCrossReference.ForEach(s =>
            {
                SocustomersItemCrossReferenceModel SocustomersItemCrossReferenceModel = new SocustomersItemCrossReferenceModel();
                SocustomersItemCrossReferenceModel.SocustomersItemCrossReferenceId = s.SocustomersItemCrossReferenceId;
                SocustomersItemCrossReferenceModel.SobyCustomersId = s.SobyCustomersId;
                SocustomersItemCrossReferenceModel.CustomerReferenceNo = s.CustomerReferenceNo;
                SocustomersItemCrossReferenceModel.CustomerReferenceNo2 = s.CustomerReferenceNo2;
                SocustomersItemCrossReferenceModel.NavItemId = s.NavItemId;
                SocustomersItemCrossReferenceModel.NavItemName = s.NavItemId != null ? s.NavItem.Code : null;
                SocustomersItemCrossReferenceModel.Description = s.Description;
                SocustomersItemCrossReferenceModel.CustomerPackingUomId = s.CustomerPackingUomId;
                SocustomersItemCrossReferenceModel.PerUomId = s.PerUomId;
                SocustomersItemCrossReferenceModel.PurchasePerUomId = s.PurchasePerUomId;
                SocustomersItemCrossReferenceModel.ShelfLifeDate = s.ShelfLifeDate;
                SocustomersItemCrossReferenceModel.MinShelfLife = s.MinShelfLife;
                SocustomersItemCrossReferenceModel.StatusCodeID = s.StatusCodeId;
                SocustomersItemCrossReferenceModel.AddedByUserID = s.AddedByUserId;
                SocustomersItemCrossReferenceModel.ModifiedByUserID = s.ModifiedByUserId;
                SocustomersItemCrossReferenceModel.AddedDate = s.AddedDate;
                SocustomersItemCrossReferenceModel.ModifiedDate = s.ModifiedDate;
                SocustomersItemCrossReferenceModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                SocustomersItemCrossReferenceModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                SocustomersItemCrossReferenceModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                SocustomersItemCrossReferenceModel.TypeOfPermitIds = s.SobyCustomersTypeOfPermit.Where(b => b.SocustomersItemCrossReferenceId == s.SocustomersItemCrossReferenceId).Select(b => b.TypeOfPermitId).ToList();
                SocustomersItemCrossReferenceModel.CustomerPackingUomName = s.CustomerPackingUomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CustomerPackingUomId).Select(m => m.Value).FirstOrDefault() : "";
                SocustomersItemCrossReferenceModel.PerUomName = s.PerUomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PerUomId).Select(m => m.Value).FirstOrDefault() : "";
                SocustomersItemCrossReferenceModel.PurchasePerUomName = s.PurchasePerUomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PurchasePerUomId).Select(m => m.Value).FirstOrDefault() : "";
                SocustomersItemCrossReferenceModel.IsSameAsSunward = s.IsSameAsSunward;
                SocustomersItemCrossReferenceModel.IsSameAsSunwardFlag = s.IsSameAsSunward == true ? "Yes" : "No";
                SocustomersItemCrossReferenceModels.Add(SocustomersItemCrossReferenceModel);
            });
            return SocustomersItemCrossReferenceModels.OrderByDescending(a => a.SocustomersItemCrossReferenceId).ToList();
        }

        [HttpGet]
        [Route("GetSocustomersItemCrossReferenceAll")]
        public List<SocustomersItemCrossReferenceModel> GetSocustomersItemCrossReferenceAll()
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var SocustomersItemCrossReference = _context.SocustomersItemCrossReference.Include(a => a.AddedByUser)
               .Include(m => m.ModifiedByUser).Include(p => p.SobyCustomersTypeOfPermit)
               .Include(s => s.StatusCode).AsNoTracking().ToList();
            List<SocustomersItemCrossReferenceModel> SocustomersItemCrossReferenceModels = new List<SocustomersItemCrossReferenceModel>();
            SocustomersItemCrossReference.ForEach(s =>
            {
                SocustomersItemCrossReferenceModel SocustomersItemCrossReferenceModel = new SocustomersItemCrossReferenceModel();
                SocustomersItemCrossReferenceModel.SocustomersItemCrossReferenceId = s.SocustomersItemCrossReferenceId;
                SocustomersItemCrossReferenceModel.SobyCustomersId = s.SobyCustomersId;
                SocustomersItemCrossReferenceModel.CustomerReferenceNo = s.CustomerReferenceNo;
                SocustomersItemCrossReferenceModel.CustomerReferenceNo2 = s.CustomerReferenceNo2;
                SocustomersItemCrossReferenceModel.NavItemId = s.NavItemId;
                SocustomersItemCrossReferenceModel.Description = s.Description;
                SocustomersItemCrossReferenceModel.CustomerPackingUomId = s.CustomerPackingUomId;
                SocustomersItemCrossReferenceModel.PerUomId = s.PerUomId;
                SocustomersItemCrossReferenceModel.PurchasePerUomId = s.PurchasePerUomId;
                SocustomersItemCrossReferenceModel.MinShelfLife = s.MinShelfLife;
                SocustomersItemCrossReferenceModel.ShelfLifeDate = s.ShelfLifeDate;
                SocustomersItemCrossReferenceModel.StatusCodeID = s.StatusCodeId;
                SocustomersItemCrossReferenceModel.AddedByUserID = s.AddedByUserId;
                SocustomersItemCrossReferenceModel.ModifiedByUserID = s.ModifiedByUserId;
                SocustomersItemCrossReferenceModel.AddedDate = s.AddedDate;
                SocustomersItemCrossReferenceModel.ModifiedDate = s.ModifiedDate;
                SocustomersItemCrossReferenceModel.No = s.CustomerReferenceNo + "|" + s.Description;
                SocustomersItemCrossReferenceModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                SocustomersItemCrossReferenceModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                SocustomersItemCrossReferenceModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                SocustomersItemCrossReferenceModel.TypeOfPermitIds = s.SobyCustomersTypeOfPermit.Where(b => b.SocustomersItemCrossReferenceId == s.SocustomersItemCrossReferenceId).Select(b => b.TypeOfPermitId).ToList();
                SocustomersItemCrossReferenceModel.CustomerPackingUomName = s.CustomerPackingUomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CustomerPackingUomId).Select(m => m.Value).FirstOrDefault() : "";
                SocustomersItemCrossReferenceModel.PerUomName = s.PerUomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PerUomId).Select(m => m.Value).FirstOrDefault() : "";
                SocustomersItemCrossReferenceModel.PurchasePerUomName = s.PurchasePerUomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PurchasePerUomId).Select(m => m.Value).FirstOrDefault() : "";
                SocustomersItemCrossReferenceModel.IsSameAsSunward = s.IsSameAsSunward;
                SocustomersItemCrossReferenceModel.IsSameAsSunwardFlag = s.IsSameAsSunward == true ? "Yes" : "No";
                SocustomersItemCrossReferenceModels.Add(SocustomersItemCrossReferenceModel);
            });
            return SocustomersItemCrossReferenceModels.OrderByDescending(a => a.SocustomersItemCrossReferenceId).ToList();
        }

        [HttpGet]
        [Route("GetItemCrossReferenceForSOWithoutBlanket")]
        public List<SocustomersItemCrossReferenceModel> GetItemCrossReferenceForSOWithoutBlanket()
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var SocustomersItemCrossReference = _context.SocustomersItemCrossReference.Include(p => p.SobyCustomersTypeOfPermit)
               .AsNoTracking().ToList();
            List<SocustomersItemCrossReferenceModel> SocustomersItemCrossReferenceModels = new List<SocustomersItemCrossReferenceModel>();
            SocustomersItemCrossReference.ForEach(s =>
            {
                SocustomersItemCrossReferenceModel SocustomersItemCrossReferenceModel = new SocustomersItemCrossReferenceModel();
                SocustomersItemCrossReferenceModel.SocustomersItemCrossReferenceId = s.SocustomersItemCrossReferenceId;
                SocustomersItemCrossReferenceModel.SobyCustomersId = s.SobyCustomersId;
                SocustomersItemCrossReferenceModel.CustomerReferenceNo = s.CustomerReferenceNo;
                SocustomersItemCrossReferenceModel.CustomerReferenceNo2 = s.CustomerReferenceNo2;
                SocustomersItemCrossReferenceModel.NavItemId = s.NavItemId;
                SocustomersItemCrossReferenceModel.Description = s.Description;
                SocustomersItemCrossReferenceModel.CustomerPackingUomId = s.CustomerPackingUomId;
                SocustomersItemCrossReferenceModel.PerUomId = s.PerUomId;
                SocustomersItemCrossReferenceModel.PurchasePerUomId = s.PurchasePerUomId;
                SocustomersItemCrossReferenceModel.No = s.CustomerReferenceNo + "|" + s.Description;
                SocustomersItemCrossReferenceModel.TypeOfPermitIds = s.SobyCustomersTypeOfPermit.Where(b => b.SocustomersItemCrossReferenceId == s.SocustomersItemCrossReferenceId).Select(b => b.TypeOfPermitId).ToList();
                SocustomersItemCrossReferenceModel.CustomerPackingUomName = s.CustomerPackingUomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CustomerPackingUomId).Select(m => m.Value).FirstOrDefault() : "";
                SocustomersItemCrossReferenceModel.PerUomName = s.PerUomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PerUomId).Select(m => m.Value).FirstOrDefault() : "";
                SocustomersItemCrossReferenceModel.PurchasePerUomName = s.PurchasePerUomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PurchasePerUomId).Select(m => m.Value).FirstOrDefault() : "";
                SocustomersItemCrossReferenceModel.IsSameAsSunward = s.IsSameAsSunward;
                SocustomersItemCrossReferenceModel.IsSameAsSunwardFlag = s.IsSameAsSunward == true ? "Yes" : "No";
                SocustomersItemCrossReferenceModels.Add(SocustomersItemCrossReferenceModel);
            });
            if (SocustomersItemCrossReferenceModels != null && SocustomersItemCrossReferenceModels.Count > 0)
            {
                SocustomersItemCrossReferenceModels.ForEach(s =>
                {
                    s.No = s.CustomerReferenceNo + " | " + s.Description + " | " + s.CustomerPackingUomName + " | " + s.PerUomName;

                });
            }
            return SocustomersItemCrossReferenceModels.OrderByDescending(a => a.SocustomersItemCrossReferenceId).ToList();
        }

        [HttpGet]
        [Route("GetSocustomersSalesInfomation")]
        public List<SocustomersSalesInfomationModel> GetSocustomersSalesInfomation(int? id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var SocustomersSalesInfomation = _context.SocustomersSalesInfomation.Include(a => a.AddedByUser)
               .Include(m => m.ModifiedByUser)
               .Include(s => s.StatusCode).Where(s => s.SobyCustomersId == id).AsNoTracking().ToList();
            List<SocustomersSalesInfomationModel> SocustomersSalesInfomationModels = new List<SocustomersSalesInfomationModel>();
            SocustomersSalesInfomation.ForEach(s =>
            {
                SocustomersSalesInfomationModel SocustomersItemCrossReferenceModel = new SocustomersSalesInfomationModel();
                SocustomersItemCrossReferenceModel.SocustomersSalesInfomationId = s.SocustomersSalesInfomationId;
                SocustomersItemCrossReferenceModel.SobyCustomersId = s.SobyCustomersId;
                SocustomersItemCrossReferenceModel.PaymentModeId = s.PaymentModeId;
                SocustomersItemCrossReferenceModel.ActivationOfOrderId = s.ActivationOfOrderId;
                SocustomersItemCrossReferenceModel.StatusCodeID = s.StatusCodeId;
                SocustomersItemCrossReferenceModel.AddedByUserID = s.AddedByUserId;
                SocustomersItemCrossReferenceModel.ModifiedByUserID = s.ModifiedByUserId;
                SocustomersItemCrossReferenceModel.AddedDate = s.AddedDate;
                SocustomersItemCrossReferenceModel.ModifiedDate = s.ModifiedDate;
                SocustomersItemCrossReferenceModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                SocustomersItemCrossReferenceModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                SocustomersItemCrossReferenceModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                SocustomersItemCrossReferenceModel.ActivationOfOrderName = s.ActivationOfOrderId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ActivationOfOrderId).Select(m => m.Value).FirstOrDefault() : "";
                SocustomersItemCrossReferenceModel.PaymentModeName = s.PaymentModeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PaymentModeId).Select(m => m.Value).FirstOrDefault() : "";
                SocustomersSalesInfomationModels.Add(SocustomersItemCrossReferenceModel);
            });
            return SocustomersSalesInfomationModels.OrderByDescending(a => a.SocustomersSalesInfomationId).ToList();
        }
        [HttpGet]
        [Route("GetSocustomersIssue")]
        public List<SocustomersIssueModel> GetSocustomersIssueModel(int? id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var SocustomersIssue = _context.SocustomersIssue.Include(a => a.AddedByUser)
               .Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Include(mm => mm.SobyCustomersManner).Include(p => p.SobyCustomersTenderAgency)
                    .Where(s => s.SocustomersId == id).Include(b => b.BuyingThrough).Include(c => c.TenderAgency).Include(d => d.CentralizedWith).AsNoTracking().ToList();
            List<SocustomersIssueModel> SocustomersIssueModels = new List<SocustomersIssueModel>();
            SocustomersIssue.ForEach(s =>
            {
                SocustomersIssueModel SocustomersIssueModel = new SocustomersIssueModel();
                SocustomersIssueModel.SocustomersIssueId = s.SocustomersIssueId;
                SocustomersIssueModel.SocustomersId = s.SocustomersId;
                SocustomersIssueModel.MustSupplyInFull = s.MustSupplyInFull;
                SocustomersIssueModel.MustSupplyInFullFlag = s.MustSupplyInFull == true ? "Yes" : "No";
                SocustomersIssueModel.MustFollowDeliveryDate = s.MustFollowDeliveryDate;
                SocustomersIssueModel.MustFollowDeliveryDateFlag = s.MustFollowDeliveryDate == true ? "Yes" : "No";
                SocustomersIssueModel.ShelfLifeRequirementDate = s.ShelfLifeRequirementDate;
                SocustomersIssueModel.StatusCodeID = s.StatusCodeId;
                SocustomersIssueModel.AddedByUserID = s.AddedByUserId;
                SocustomersIssueModel.ModifiedByUserID = s.ModifiedByUserId;
                SocustomersIssueModel.AddedDate = s.AddedDate;
                SocustomersIssueModel.ModifiedDate = s.ModifiedDate;
                SocustomersIssueModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                SocustomersIssueModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                SocustomersIssueModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                SocustomersIssueModel.SobyCustomersTenderAgencyIds = s.SobyCustomersTenderAgency.Where(b => b.SocustomersIssueId == s.SocustomersIssueId).Select(b => b.TenderAgencyId).ToList();
                SocustomersIssueModel.SobyCustomersMannerIds = s.SobyCustomersManner.Where(b => b.SocustomersIssueId == s.SocustomersIssueId).Select(b => b.MannersId).ToList();
                SocustomersIssueModel.SobyCustomersMannerList = s.SobyCustomersManner != null && masterDetailList != null ? string.Join(",", s.SobyCustomersManner.Where(b => b.SocustomersIssueId == s.SocustomersIssueId).Select(b => masterDetailList.Where(m => m.ApplicationMasterDetailId == b.MannersId).Select(m => m.Value).FirstOrDefault()).ToList()) : "";
                SocustomersIssueModel.SobyCustomersTenderAgencyList = s.SobyCustomersTenderAgency != null && masterDetailList != null ? string.Join(",", s.SobyCustomersTenderAgency.Where(b => b.SocustomersIssueId == s.SocustomersIssueId).Select(b => masterDetailList.Where(m => m.ApplicationMasterDetailId == b.TenderAgencyId).Select(m => m.Value).FirstOrDefault()).ToList()) : "";
                SocustomersIssueModel.BuyingThroughId = s.BuyingThroughId;
                SocustomersIssueModel.BuyingThrough = s.BuyingThrough != null ? s.BuyingThrough.CompanyName : null;
                SocustomersIssueModel.TenderAgencyId = s.TenderAgencyId;
                SocustomersIssueModel.TenderAgencyName = s.TenderAgency != null ? s.TenderAgency.CompanyName : null;
                SocustomersIssueModel.CentralizedWithId = s.CentralizedWithId;
                SocustomersIssueModel.CentralizedWithName = s.CentralizedWith != null ? s.CentralizedWith.CompanyName : null;
                SocustomersIssueModels.Add(SocustomersIssueModel);
            });
            return SocustomersIssueModels.OrderByDescending(a => a.SocustomersIssueId).ToList();
        }
        [HttpGet]
        [Route("GetSobyCustomersAddressDetails")]
        public List<SobyCustomersAddressModel> GetSobyCustomersAddressDetails()
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var sobyCustomersAddress = _context.SobyCustomersAddress.Include(s => s.SobyCustomersMasterAddress).Include(t => t.TypeOfAddress).AsNoTracking().ToList();
            List<SobyCustomersAddressModel> sobyCustomersAddressModels = new List<SobyCustomersAddressModel>();
            sobyCustomersAddress.ForEach(s =>
            {
                SobyCustomersAddressModel sobyCustomersAddressModel = new SobyCustomersAddressModel();
                sobyCustomersAddressModel.SobyCustomersAddressId = s.SobyCustomersAddressId;
                sobyCustomersAddressModel.SobyCustomersId = s.SobyCustomersId;
                sobyCustomersAddressModel.LocationNo = s.LocationNo;
                sobyCustomersAddressModel.LocationName = s.LocationName;
                sobyCustomersAddressModel.Address = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.Address : null;
                sobyCustomersAddressModel.PostalCode = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.PostalCode : null;
                sobyCustomersAddressModel.CountryId = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.CountryId : null;
                sobyCustomersAddressModel.Address2 = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.Address2 : null;
                sobyCustomersAddressModel.StateId = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.StateId : null;
                sobyCustomersAddressModel.PhoneNo = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.PhoneNo : null;
                sobyCustomersAddressModel.ContactName = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.ContactName : null;
                sobyCustomersAddressModel.CityId = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.CityId : null;
                sobyCustomersAddressModel.TypeOfAddressId = s.TypeOfAddressId;
                sobyCustomersAddressModel.NavisionNo = s.NavisionNo;
                sobyCustomersAddressModel.EmailAddress = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.EmailAddress : null;
                sobyCustomersAddressModel.TypeOfAddress = s.TypeOfAddress != null ? s.TypeOfAddress.CodeValue : null;
                sobyCustomersAddressModel.Country = s.SobyCustomersMasterAddress != null && s.SobyCustomersMasterAddress.CountryId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.SobyCustomersMasterAddress.CountryId).Select(m => m.Value).FirstOrDefault() : "";
                sobyCustomersAddressModel.State = s.SobyCustomersMasterAddress != null && s.SobyCustomersMasterAddress.StateId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.SobyCustomersMasterAddress.StateId).Select(m => m.Value).FirstOrDefault() : "";
                sobyCustomersAddressModel.City = s.SobyCustomersMasterAddress != null && s.SobyCustomersMasterAddress.CityId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.SobyCustomersMasterAddress.CityId).Select(m => m.Value).FirstOrDefault() : "";
                sobyCustomersAddressModel.FaxNo = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.FaxNo : null;
                sobyCustomersAddressModel.Uenno = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.Uenno : null;
                sobyCustomersAddressModel.CompanyRegisterationNo = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.CompanyRegisterationNo : null;
                sobyCustomersAddressModel.Vatgstno = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.Vatgstno : null;
                sobyCustomersAddressModels.Add(sobyCustomersAddressModel);
            });
            return sobyCustomersAddressModels.OrderByDescending(a => a.SobyCustomersAddressId).ToList();
        }
        [HttpGet]
        [Route("GetSobyCustomersAddressForSalesLine")]
        public List<SobyCustomersAddressModel> GetSobyCustomersAddressForSalesLine()
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var sobyCustomersAddress = _context.SobyCustomersAddress.Include(s => s.SobyCustomersMasterAddress).AsNoTracking().ToList();
            List<SobyCustomersAddressModel> sobyCustomersAddressModels = new List<SobyCustomersAddressModel>();
            sobyCustomersAddress.ForEach(s =>
            {
                SobyCustomersAddressModel sobyCustomersAddressModel = new SobyCustomersAddressModel();
                sobyCustomersAddressModel.SobyCustomersAddressId = s.SobyCustomersAddressId;
                sobyCustomersAddressModel.SobyCustomersId = s.SobyCustomersId;
                sobyCustomersAddressModel.LocationNo = s.LocationNo;
                sobyCustomersAddressModel.LocationName = s.LocationName;
                sobyCustomersAddressModel.Address = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.Address : null;
                sobyCustomersAddressModel.PostalCode = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.PostalCode : null;
                sobyCustomersAddressModel.CountryId = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.CountryId : null;
                sobyCustomersAddressModel.Country = s.SobyCustomersMasterAddress != null && s.SobyCustomersMasterAddress.CountryId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.SobyCustomersMasterAddress.CountryId).Select(m => m.Value).FirstOrDefault() : "";
                sobyCustomersAddressModel.DropDownDisplay = s.LocationNo + " | " + s.LocationName + " | " + sobyCustomersAddressModel.PostalCode + " | " + sobyCustomersAddressModel.Country;
                sobyCustomersAddressModel.Address2 = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.Address2 : null;
                sobyCustomersAddressModel.StateId = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.StateId : null;
                sobyCustomersAddressModel.PhoneNo = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.PhoneNo : null;
                //sobyCustomersAddressModel.PrimaryContactCode = s.PrimaryContactCode;
                sobyCustomersAddressModel.ContactName = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.ContactName : null;
                sobyCustomersAddressModel.CityId = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.CityId : null;
                sobyCustomersAddressModel.EmailAddress = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.EmailAddress : null;
                sobyCustomersAddressModel.State = s.SobyCustomersMasterAddress != null && s.SobyCustomersMasterAddress.StateId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.SobyCustomersMasterAddress.StateId).Select(m => m.Value).FirstOrDefault() : "";
                sobyCustomersAddressModel.City = s.SobyCustomersMasterAddress != null && s.SobyCustomersMasterAddress.CityId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.SobyCustomersMasterAddress.CityId).Select(m => m.Value).FirstOrDefault() : "";
                sobyCustomersAddressModel.FaxNo = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.FaxNo : null;
                sobyCustomersAddressModel.Uenno = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.Uenno : null;
                sobyCustomersAddressModel.CompanyRegisterationNo = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.CompanyRegisterationNo : null;
                sobyCustomersAddressModel.Vatgstno = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.Vatgstno : null;
                sobyCustomersAddressModels.Add(sobyCustomersAddressModel);
            });
            return sobyCustomersAddressModels.OrderByDescending(a => a.SobyCustomersAddressId).ToList();
        }

        [HttpGet]
        [Route("GetSobyCustomersAddressById")]
        public List<SobyCustomersAddressModel> GetSobyCustomersAddressById(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var sobyCustomersAddress = _context.SobyCustomersAddress.Include(s => s.SobyCustomersMasterAddress).AsNoTracking().Where(s => s.SobyCustomersId == id).ToList();
            List<SobyCustomersAddressModel> sobyCustomersAddressModels = new List<SobyCustomersAddressModel>();
            sobyCustomersAddress.ForEach(s =>
            {
                SobyCustomersAddressModel sobyCustomersAddressModel = new SobyCustomersAddressModel();
                sobyCustomersAddressModel.SobyCustomersAddressId = s.SobyCustomersAddressId;
                sobyCustomersAddressModel.SobyCustomersId = s.SobyCustomersId;
                sobyCustomersAddressModel.LocationNo = s.LocationNo;
                sobyCustomersAddressModel.LocationName = s.LocationName;
                sobyCustomersAddressModel.Address = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.Address : null;
                sobyCustomersAddressModel.PostalCode = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.PostalCode : null;
                sobyCustomersAddressModel.CountryId = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.CountryId : null;
                sobyCustomersAddressModel.Country = s.SobyCustomersMasterAddress != null && s.SobyCustomersMasterAddress.CountryId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.SobyCustomersMasterAddress.CountryId).Select(m => m.Value).FirstOrDefault() : "";
                sobyCustomersAddressModel.DropDownDisplay = s.LocationNo + " | " + s.LocationName + " | " + sobyCustomersAddressModel.PostalCode + " | " + sobyCustomersAddressModel.Country;
                sobyCustomersAddressModel.Address2 = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.Address2 : null;
                sobyCustomersAddressModel.StateId = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.StateId : null;
                sobyCustomersAddressModel.PhoneNo = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.PhoneNo : null;
                //sobyCustomersAddressModel.PrimaryContactCode = s.PrimaryContactCode;
                sobyCustomersAddressModel.ContactName = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.ContactName : null;
                sobyCustomersAddressModel.CityId = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.CityId : null;
                sobyCustomersAddressModel.EmailAddress = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.EmailAddress : null;
                sobyCustomersAddressModel.State = s.SobyCustomersMasterAddress != null && s.SobyCustomersMasterAddress.StateId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.SobyCustomersMasterAddress.StateId).Select(m => m.Value).FirstOrDefault() : "";
                sobyCustomersAddressModel.City = s.SobyCustomersMasterAddress != null && s.SobyCustomersMasterAddress.CityId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.SobyCustomersMasterAddress.CityId).Select(m => m.Value).FirstOrDefault() : "";
                sobyCustomersAddressModel.FaxNo = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.FaxNo : null;
                sobyCustomersAddressModel.Uenno = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.Uenno : null;
                sobyCustomersAddressModel.CompanyRegisterationNo = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.CompanyRegisterationNo : null;
                sobyCustomersAddressModel.Vatgstno = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.Vatgstno : null;
                sobyCustomersAddressModels.Add(sobyCustomersAddressModel);
            });
            return sobyCustomersAddressModels.OrderByDescending(a => a.SobyCustomersAddressId).ToList();
        }
        [HttpGet]
        [Route("GetSobyCustomersShippingAddress")]
        public List<ShipingCodeAddressModel> GetSobyCustomersShippingAddress(int id)
        {
            var ProfileReferenceNo = _context.CompanyListing.Where(w => w.CompanyListingId == id).Select(s => s.ProfileReferenceNo).FirstOrDefault();
            List<ShipingCodeAddressModel> ShipingCodeAddressModels = new List<ShipingCodeAddressModel>();
            if (ProfileReferenceNo != null)
            {
                var sobyCustomerId = _context.SobyCustomers.Where(w => w.LinkProfileReferenceNo == ProfileReferenceNo).Select(s => s.SobyCustomersId).FirstOrDefault();
                if (sobyCustomerId > 0)
                {
                    var masterDetailList = _context.ApplicationMasterDetail.ToList();
                    var SOByCustomersAddress = _context.SobyCustomersMasterAddress.Where(w => w.SobyCustomersId == sobyCustomerId).ToList();
                    if (SOByCustomersAddress != null)
                    {
                        SOByCustomersAddress.ForEach(h =>
                        {
                            ShipingCodeAddressModel ShipingCodeAddressModel = new ShipingCodeAddressModel();
                            ShipingCodeAddressModel.ShipingCodeAddressId = h.SobyCustomersMasterAddressId;
                            ShipingCodeAddressModel.Address = h.Address;
                            ShipingCodeAddressModel.Address2 = h.Address2;
                            ShipingCodeAddressModel.State = h.StateId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == h.StateId).Select(m => m.Value).FirstOrDefault() : "";
                            ShipingCodeAddressModel.City = h.CityId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == h.CityId).Select(m => m.Value).FirstOrDefault() : "";
                            ShipingCodeAddressModel.Country = h.CountryId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == h.CountryId).Select(m => m.Value).FirstOrDefault() : "";
                            ShipingCodeAddressModel.Country = h.CountryId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == h.CountryId).Select(m => m.Value).FirstOrDefault() : "";
                            //ShipingCodeAddressModel.LocationNo = h.LocationNo;
                            //ShipingCodeAddressModel.LocationName = h.LocationName;
                            ShipingCodeAddressModel.ShipingAddress = h.AddressName;
                            ShipingCodeAddressModels.Add(ShipingCodeAddressModel);
                        });
                    }
                }
            }
            return ShipingCodeAddressModels;
        }
        [HttpGet]
        [Route("GetSobyCustomersShippingAddressBySO")]
        public List<SobyCustomersMasterAddressModel> GetSobyCustomersShippingAddressBySO(int id)
        {
            int[] ids = { 1671, 1672 };
            List<SobyCustomersMasterAddressModel> ShipingCodeAddressModels = new List<SobyCustomersMasterAddressModel>();
            var SOByCustomersAddress = _context.SobyCustomersMasterAddress.Where(w => w.SobyCustomersId == id && w.AddressTypeId != null).ToList();
            if (SOByCustomersAddress != null)
            {
                var masterDetailList = _context.ApplicationMasterDetail.ToList();

                SOByCustomersAddress.ForEach(h =>
                {
                    SobyCustomersMasterAddressModel ShipingCodeAddressModel = new SobyCustomersMasterAddressModel();
                    ShipingCodeAddressModel.SobyCustomersMasterAddressId = h.SobyCustomersMasterAddressId;
                    ShipingCodeAddressModel.AddressTypeId = h.AddressTypeId.Value;
                    ShipingCodeAddressModel.AddressName = h.AddressName;
                    ShipingCodeAddressModels.Add(ShipingCodeAddressModel);
                });
            }
            return ShipingCodeAddressModels.Where(w => ids.Contains(w.AddressTypeId)).ToList();
        }
        [HttpGet]
        [Route("GetSobyCustomerSunwardEquivalent")]
        public List<SobyCustomerSunwardEquivalentModel> GetSobyCustomerSunwardEquivalent(int? id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();

            var sobyCustomerSunwardEquivalent = _context.SobyCustomerSunwardEquivalent.Include(o => o.OrderPlacetoCompany).Include(t => t.SupplyTo).Include(s => s.StatusCode).Include(n => n.NavItem).Where(s => s.SocustomersItemCrossReferenceId == id).AsNoTracking().ToList();
            List<SobyCustomerSunwardEquivalentModel> sobyCustomerSunwardEquivalentModels = new List<SobyCustomerSunwardEquivalentModel>();
            sobyCustomerSunwardEquivalent.ForEach(s =>
            {
                SobyCustomerSunwardEquivalentModel sobyCustomerSunwardEquivalentModel = new SobyCustomerSunwardEquivalentModel();
                sobyCustomerSunwardEquivalentModel.SobyCustomerSunwardEquivalentId = s.SobyCustomerSunwardEquivalentId;
                sobyCustomerSunwardEquivalentModel.SocustomersItemCrossReferenceId = s.SocustomersItemCrossReferenceId;
                sobyCustomerSunwardEquivalentModel.SobyCustomersId = s.SobyCustomersId;
                sobyCustomerSunwardEquivalentModel.OrderPlacetoCompanyId = s.OrderPlacetoCompanyId;
                sobyCustomerSunwardEquivalentModel.OrderPlacetoCompany = s.OrderPlacetoCompany != null ? s.OrderPlacetoCompany.Description : null;
                sobyCustomerSunwardEquivalentModel.NavItemId = s.NavItemId;
                sobyCustomerSunwardEquivalentModel.IsThisDefault = s.IsThisDefault;
                sobyCustomerSunwardEquivalentModel.IsThisDefaultFlag = s.IsThisDefault == true ? "Yes" : "No";
                sobyCustomerSunwardEquivalentModel.DefaultSupply = s.DefaultSupply;
                sobyCustomerSunwardEquivalentModel.StatusCodeID = s.StatusCodeId;
                sobyCustomerSunwardEquivalentModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : null;
                sobyCustomerSunwardEquivalentModel.FactorVsCustomerPacking = s.FactorVsCustomerPacking;
                sobyCustomerSunwardEquivalentModel.FactorVsPurchasePacking = s.FactorVsPurchasePacking;
                sobyCustomerSunwardEquivalentModel.NavItem = s.NavItem != null ? s.NavItem.Code : null;
                sobyCustomerSunwardEquivalentModel.SupplyToId = s.SupplyToId;
                sobyCustomerSunwardEquivalentModel.SupplyToName = s.SupplyTo != null ? s.SupplyTo.GenericCodeSupplyDescription : null;
                sobyCustomerSunwardEquivalentModel.ItemDescription = s.NavItem != null ? (s.NavItem.Code) : null;
                //sobyCustomerSunwardEquivalentModel.ManufacturingSite = s.NavItem != null ? s.NavItem.InternalRef : null;
                //sobyCustomerSunwardEquivalentModel.InterCompanyNo = s.NavItem != null ? s.NavItem.Company : null;
                sobyCustomerSunwardEquivalentModel.BUOM = s.NavItem != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.NavItem.Uom).Select(m => m.Value).FirstOrDefault() : null;

                sobyCustomerSunwardEquivalentModels.Add(sobyCustomerSunwardEquivalentModel);
            });
            return sobyCustomerSunwardEquivalentModels.OrderByDescending(a => a.SobyCustomerSunwardEquivalentId).ToList();
        }
        [HttpGet]
        [Route("GetSOCustomersAll")]
        public List<SobyCustomersModel> GetSOCustomersAll()
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var SobyCustomers = _context.SobyCustomers.AsNoTracking().ToList();
            List<SobyCustomersModel> sobyCustomersModels = new List<SobyCustomersModel>();
            SobyCustomers.ForEach(s =>
            {
                SobyCustomersModel sobyCustomersModel = new SobyCustomersModel();
                sobyCustomersModel.SobyCustomersId = s.SobyCustomersId;

                sobyCustomersModels.Add(sobyCustomersModel);
            });
            return sobyCustomersModels.OrderByDescending(a => a.SobyCustomersId).ToList();
        }

        [HttpGet]
        [Route("GetSOCustomerSalesAddres")]
        public List<SobyCustomersSalesAddressModel> GetSOCustomerSalesAddres()
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var sobyCustomersSalesAddress = _context.SobyCustomersSalesAddress.Include(t => t.SobyCustomersMasterAddress).AsNoTracking().ToList();
            List<SobyCustomersSalesAddressModel> sobyCustomersSalesAddressModels = new List<SobyCustomersSalesAddressModel>();
            sobyCustomersSalesAddress.ForEach(s =>
            {
                SobyCustomersSalesAddressModel sobyCustomersSalesAddressModel = new SobyCustomersSalesAddressModel();
                sobyCustomersSalesAddressModel.SobyCustomersId = s.SobyCustomersId;
                sobyCustomersSalesAddressModel.SobyCustomersSalesAddressId = s.SobyCustomersSalesAddressId;
                sobyCustomersSalesAddressModel.PostalCode = s.SobyCustomersMasterAddress != null ? s.SobyCustomersMasterAddress.PostalCode : null;
                sobyCustomersSalesAddressModels.Add(sobyCustomersSalesAddressModel);
            });
            return sobyCustomersSalesAddressModels.ToList();
        }
        [HttpPost]
        [Route("InsertSobyCustomersHeaders")]
        public SobyCustomersModel InsertSobyCustomersHeaders(SobyCustomersModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "SobyCustomersHeaders" });
            var sobyCustomers = new SobyCustomers
            {
                ProfileLinkReferenceNo = profileNo,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                PaymentModeId = value.PaymentModeId,
                ActivationOfOrderId = value.ActivationOfOrderId,
                BillingAddressStatusId = value.BillingAddressStatusId,
            };
            _context.SobyCustomers.Add(sobyCustomers);
            _context.SaveChanges();
            value.SobyCustomersId = sobyCustomers.SobyCustomersId;
            return value;
        }
        [HttpPost]
        [Route("InsertSobyCustomers")]
        public SobyCustomersModel Post(SobyCustomersModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileID, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = "SobyCustomers" });
            var sobyCustomers = new SobyCustomers
            {
                ProfileLinkReferenceNo = profileNo,
                LinkProfileReferenceNo = value.LinkProfileReferenceNo,
                MasterProfileReferenceNo = string.IsNullOrEmpty(value.MasterProfileReferenceNo) ? profileNo : value.MasterProfileReferenceNo,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                PaymentModeId = value.PaymentModeId,
                ActivationOfOrderId = value.ActivationOfOrderId,
                BillingAddressStatusId = value.BillingAddressStatusId,
            };
            _context.SobyCustomers.Add(sobyCustomers);
            _context.SaveChanges();
            value.SobyCustomersId = sobyCustomers.SobyCustomersId;
            SocustomersSalesAddress(value);
            value.MasterProfileReferenceNo = sobyCustomers.MasterProfileReferenceNo;
            value.ProfileLinkReferenceNo = sobyCustomers.ProfileLinkReferenceNo;
            return value;
        }
        [HttpPut]
        [Route("UpdateSobyCustomers")]
        public SobyCustomersModel Put(SobyCustomersModel value)
        {
            var sobyCustomers = _context.SobyCustomers.SingleOrDefault(p => p.SobyCustomersId == value.SobyCustomersId);
            sobyCustomers.LinkProfileReferenceNo = value.LinkProfileReferenceNo;
            sobyCustomers.ProfileLinkReferenceNo = value.ProfileLinkReferenceNo;
            sobyCustomers.StatusCodeId = value.StatusCodeID.Value;
            sobyCustomers.ModifiedByUserId = value.ModifiedByUserID;
            sobyCustomers.ModifiedDate = DateTime.Now;
            sobyCustomers.PaymentModeId = value.PaymentModeId;
            sobyCustomers.ActivationOfOrderId = value.ActivationOfOrderId;
            sobyCustomers.BillingAddressStatusId = value.BillingAddressStatusId;
            _context.SaveChanges();
            SocustomersSalesAddress(value);
            return value;
        }
        private void SocustomersSalesAddress(SobyCustomersModel value)
        {
            var defaultAddress = _context.SobyCustomersSalesAddress.SingleOrDefault(p => p.SobyCustomersSalesAddressId == value.DefaultAddress.SobyCustomersSalesAddressId);
            if (defaultAddress != null)
            {
                var sobyMasterAddress = _context.SobyCustomersMasterAddress.SingleOrDefault(s => s.SobyCustomersMasterAddressId == value.DefaultAddress.SobyCustomersMasterAddressId);
                if (sobyMasterAddress != null)
                {
                    sobyMasterAddress.SobyCustomersId = value.SobyCustomersId;
                    sobyMasterAddress.Address = value.DefaultAddress.Address;
                    sobyMasterAddress.Address2 = value.DefaultAddress.Address2;
                    sobyMasterAddress.PostalCode = value.DefaultAddress.PostalCode;
                    sobyMasterAddress.ContactName = value.DefaultAddress.ContactName;
                    sobyMasterAddress.PhoneNo = value.DefaultAddress.PhoneNo;
                    sobyMasterAddress.EmailAddress = value.DefaultAddress.EmailAddress;
                    sobyMasterAddress.CountryId = value.DefaultAddress.CountryId;
                    sobyMasterAddress.StateId = value.DefaultAddress.StateId;
                    sobyMasterAddress.CityId = value.DefaultAddress.CityId;
                    sobyMasterAddress.AddressName = value.DefaultAddress.AddressName;
                    sobyMasterAddress.AddressTypeId = value.DefaultAddress.AddressTypeId;
                    sobyMasterAddress.FaxNo = value.DefaultAddress.FaxNo;
                    sobyMasterAddress.Uenno = value.DefaultAddress.Uenno;
                    sobyMasterAddress.CompanyRegisterationNo = value.DefaultAddress.CompanyRegisterationNo;
                    sobyMasterAddress.Vatgstno = value.DefaultAddress.Vatgstno;
                    sobyMasterAddress.DeliveryInformation = value.DefaultAddress.DeliveryInformation;
                    _context.SaveChanges();
                    value.DefaultAddress.SobyCustomersMasterAddressId = sobyMasterAddress.SobyCustomersMasterAddressId;
                }
                defaultAddress.SobyCustomersId = value.SobyCustomersId;
                defaultAddress.SobyCustomersMasterAddressId = value.DefaultAddress.SobyCustomersMasterAddressId;
                _context.SaveChanges();
            }
            else
            {
                var sobyMasterAddress = new SobyCustomersMasterAddress
                {
                    SobyCustomersId = value.SobyCustomersId,
                    Address = value.DefaultAddress.Address,
                    Address2 = value.DefaultAddress.Address2,
                    PostalCode = value.DefaultAddress.PostalCode,
                    ContactName = value.DefaultAddress.ContactName,
                    PhoneNo = value.DefaultAddress.PhoneNo,
                    EmailAddress = value.DefaultAddress.EmailAddress,
                    CountryId = value.DefaultAddress.CountryId,
                    StateId = value.DefaultAddress.StateId,
                    CityId = value.DefaultAddress.CityId,
                    AddressName = value.DefaultAddress.AddressName,
                    AddressTypeId = value.DefaultAddress.AddressTypeId,
                    Uenno = value.DefaultAddress.Uenno,
                    CompanyRegisterationNo = value.DefaultAddress.CompanyRegisterationNo,
                    Vatgstno = value.DefaultAddress.Vatgstno,
                    DeliveryInformation = value.DefaultAddress.DeliveryInformation,
                };
                _context.SobyCustomersMasterAddress.Add(sobyMasterAddress);
                _context.SaveChanges();
                value.DefaultAddress.SobyCustomersMasterAddressId = sobyMasterAddress.SobyCustomersMasterAddressId;
                var defaultAddresss = new SobyCustomersSalesAddress
                {
                    SobyCustomersId = value.SobyCustomersId,
                    SobyCustomersMasterAddressId = value.DefaultAddress.SobyCustomersMasterAddressId,
                };
                _context.SobyCustomersSalesAddress.Add(defaultAddresss);
                _context.SaveChanges();
                value.DefaultAddress.SobyCustomersId = value.SobyCustomersId;
                value.DefaultAddress.SobyCustomersSalesAddressId = defaultAddresss.SobyCustomersSalesAddressId;
            }
            var FinanceDepartment = _context.SobyCustomersSalesAddress.SingleOrDefault(p => p.SobyCustomersSalesAddressId == value.FinanceDepartment.SobyCustomersSalesAddressId);
            if (FinanceDepartment != null)
            {
                var sobyMasterAddress = _context.SobyCustomersMasterAddress.SingleOrDefault(s => s.SobyCustomersMasterAddressId == value.FinanceDepartment.SobyCustomersMasterAddressId);
                if (sobyMasterAddress != null)
                {
                    sobyMasterAddress.SobyCustomersId = value.SobyCustomersId;
                    sobyMasterAddress.Address = value.FinanceDepartment.Address;
                    sobyMasterAddress.Address2 = value.FinanceDepartment.Address2;
                    sobyMasterAddress.PostalCode = value.FinanceDepartment.PostalCode;
                    sobyMasterAddress.ContactName = value.FinanceDepartment.ContactName;
                    sobyMasterAddress.PhoneNo = value.FinanceDepartment.PhoneNo;
                    sobyMasterAddress.EmailAddress = value.FinanceDepartment.EmailAddress;
                    sobyMasterAddress.CountryId = value.FinanceDepartment.CountryId;
                    sobyMasterAddress.StateId = value.FinanceDepartment.StateId;
                    sobyMasterAddress.CityId = value.FinanceDepartment.CityId;
                    sobyMasterAddress.AddressName = value.FinanceDepartment.AddressName;
                    sobyMasterAddress.Address = value.FinanceDepartment.Address;
                    sobyMasterAddress.Uenno = value.FinanceDepartment.Uenno;
                    sobyMasterAddress.CompanyRegisterationNo = value.FinanceDepartment.CompanyRegisterationNo;
                    sobyMasterAddress.Vatgstno = value.FinanceDepartment.Vatgstno;
                    sobyMasterAddress.AddressTypeId = value.FinanceDepartment.AddressTypeId;
                    _context.SaveChanges();
                    value.FinanceDepartment.SobyCustomersMasterAddressId = sobyMasterAddress.SobyCustomersMasterAddressId;
                }
                FinanceDepartment.SobyCustomersId = value.SobyCustomersId;
                FinanceDepartment.SobyCustomersMasterAddressId = value.FinanceDepartment.SobyCustomersMasterAddressId;

                _context.SaveChanges();
            }
            else
            {
                var sobyMasterAddress = new SobyCustomersMasterAddress
                {
                    SobyCustomersId = value.SobyCustomersId,
                    Address = value.FinanceDepartment.Address,
                    Address2 = value.FinanceDepartment.Address2,
                    PostalCode = value.FinanceDepartment.PostalCode,
                    ContactName = value.FinanceDepartment.ContactName,
                    PhoneNo = value.FinanceDepartment.PhoneNo,
                    EmailAddress = value.FinanceDepartment.EmailAddress,
                    CountryId = value.FinanceDepartment.CountryId,
                    StateId = value.FinanceDepartment.StateId,
                    CityId = value.FinanceDepartment.CityId,
                    AddressName = value.FinanceDepartment.AddressName,
                    AddressTypeId = value.FinanceDepartment.AddressTypeId,
                    Uenno = value.FinanceDepartment.Uenno,
                    CompanyRegisterationNo = value.FinanceDepartment.CompanyRegisterationNo,
                    Vatgstno = value.FinanceDepartment.Vatgstno,
                };
                _context.SobyCustomersMasterAddress.Add(sobyMasterAddress);
                _context.SaveChanges();
                value.FinanceDepartment.SobyCustomersMasterAddressId = sobyMasterAddress.SobyCustomersMasterAddressId;
                var FinanceDepartments = new SobyCustomersSalesAddress
                {
                    SobyCustomersId = value.SobyCustomersId,
                    SobyCustomersMasterAddressId = value.FinanceDepartment.SobyCustomersMasterAddressId,

                };
                _context.SobyCustomersSalesAddress.Add(FinanceDepartments);
                _context.SaveChanges();
                value.FinanceDepartment.SobyCustomersId = value.SobyCustomersId;
                value.FinanceDepartment.SobyCustomersSalesAddressId = FinanceDepartments.SobyCustomersSalesAddressId;
            }
        }
        [HttpPost]
        [Route("InsertSobyCustomerSunwardEquivalent")]
        public SobyCustomerSunwardEquivalentModel Post(SobyCustomerSunwardEquivalentModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var sobyCustomerSunwardEquivalent = new SobyCustomerSunwardEquivalent
            {
                SocustomersItemCrossReferenceId = value.SocustomersItemCrossReferenceId,
                OrderPlacetoCompanyId = value.OrderPlacetoCompanyId,
                NavItemId = value.NavItemId,
                FactorVsCustomerPacking = value.FactorVsCustomerPacking,
                FactorVsPurchasePacking = value.FactorVsPurchasePacking,
                IsThisDefault = value.IsThisDefaultFlag == "Yes" ? true : false,
                StatusCodeId = value.StatusCodeID,
                DefaultSupply = value.DefaultSupply,
                SupplyToId = value.SupplyToId,
                SessionId = value.SessionId
            };
            _context.SobyCustomerSunwardEquivalent.Add(sobyCustomerSunwardEquivalent);
            _context.SaveChanges();

            ApproveParam approveParam = new ApproveParam();
            approveParam.SessionId = value.SessionId;
            approveParam.ScreenId = "Company";
            if (!_approvalService.CheckIsApprovalWaiting(approveParam))
            {
                _approvalService.CreateApprovalDetails(approveParam);
            }
            value.SobyCustomerSunwardEquivalentId = sobyCustomerSunwardEquivalent.SobyCustomerSunwardEquivalentId;
            return value;
        }
        [HttpPut]
        [Route("UpdateSobyCustomerSunwardEquivalent")]
        public SobyCustomerSunwardEquivalentModel Put(SobyCustomerSunwardEquivalentModel s)
        {
            var sobyCustomerSunwardEquivalent = _context.SobyCustomerSunwardEquivalent.SingleOrDefault(p => p.SobyCustomerSunwardEquivalentId == s.SobyCustomerSunwardEquivalentId);
            sobyCustomerSunwardEquivalent.SessionId ??= Guid.NewGuid();
            sobyCustomerSunwardEquivalent.SocustomersItemCrossReferenceId = s.SocustomersItemCrossReferenceId;
            sobyCustomerSunwardEquivalent.OrderPlacetoCompanyId = s.OrderPlacetoCompanyId;
            sobyCustomerSunwardEquivalent.NavItemId = s.NavItemId;
            sobyCustomerSunwardEquivalent.FactorVsCustomerPacking = s.FactorVsCustomerPacking;
            sobyCustomerSunwardEquivalent.FactorVsPurchasePacking = s.FactorVsPurchasePacking;
            sobyCustomerSunwardEquivalent.IsThisDefault = s.IsThisDefaultFlag == "Yes" ? true : false;
            sobyCustomerSunwardEquivalent.StatusCodeId = s.StatusCodeID;
            sobyCustomerSunwardEquivalent.DefaultSupply = s.DefaultSupply;
            sobyCustomerSunwardEquivalent.SupplyToId = s.SupplyToId;
            _context.SaveChanges();

            ApproveParam approveParam = new ApproveParam();
            approveParam.SessionId = sobyCustomerSunwardEquivalent.SessionId;
            approveParam.ScreenId = "Company";
            if (!_approvalService.CheckIsApprovalWaiting(approveParam))
            {
                _approvalService.CreateApprovalDetails(approveParam);
            }
            return s;
        }

        [HttpPut]
        [Route("UpdateSobyCustomerSunwardEquivalentStatus")]
        public void UpdateSobyCustomerSunwardEquivalentStatus(SocustomersItemCrossReferenceReportModel reportModel)
        {
            var employee = _context.Employee.FirstOrDefault(e => e.UserId == reportModel.UserId);
            var isApprovalScreen = _context.ApproveMaster.Any(s => s.ScreenId == "Company" && s.UserId == employee.EmployeeId);
            if (isApprovalScreen)
            {
                var canApprove = _approvalService.Approve(new ApprovalDetailModel { SessionId = reportModel.SessionId, Remarks = reportModel.Remarks, StatusCodeID = reportModel.StatusCodeId, AddedByUserID = reportModel.UserId });
                if (canApprove)
                {
                    var sobyCustomerSunwardEquivalent = _context.SobyCustomerSunwardEquivalent.FirstOrDefault(p => p.SobyCustomerSunwardEquivalentId == reportModel.SobyCustomerSunwardEquivalentId);
                    sobyCustomerSunwardEquivalent.StatusCodeId = 1803;
                    _context.SaveChanges();
                }
            }
        }



        [HttpPost]
        [Route("InsertSocustomersIssue")]
        public SocustomersIssueModel InsertSocustomersIssue(SocustomersIssueModel value)
        {
            var socustomersIssue = new SocustomersIssue
            {
                SocustomersId = value.SobyCustomersId,
                MustSupplyInFull = value.MustSupplyInFullFlag == "Yes" ? true : false,
                MustFollowDeliveryDate = value.MustFollowDeliveryDateFlag == "Yes" ? true : false,
                ShelfLifeRequirementDate = value.ShelfLifeRequirementDate,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                BuyingThroughId = value.BuyingThroughId,
                TenderAgencyId = value.TenderAgencyId,
                CentralizedWithId = value.CentralizedWithId,
            };
            _context.SocustomersIssue.Add(socustomersIssue);
            _context.SaveChanges();
            socustomersIssue.SocustomersIssueId = socustomersIssue.SocustomersIssueId;
            value.SocustomersIssueId = socustomersIssue.SocustomersIssueId;
            if (value.SobyCustomersMannerIds != null && value.SobyCustomersMannerIds.Count > 0)
            {
                value.SobyCustomersMannerIds.ForEach(u =>
                {
                    var sobyCustomersMannerItems = new SobyCustomersManner()
                    {
                        SocustomersIssueId = socustomersIssue.SocustomersIssueId,
                        MannersId = u,
                    };
                    _context.SobyCustomersManner.Add(sobyCustomersMannerItems);
                    _context.SaveChanges();
                });
            }
            if (value.SobyCustomersTenderAgencyIds != null && value.SobyCustomersTenderAgencyIds.Count > 0)
            {
                value.SobyCustomersTenderAgencyIds.ForEach(t =>
                {
                    var sobyCustomersTenderItems = new SobyCustomersTenderAgency()
                    {
                        SocustomersIssueId = socustomersIssue.SocustomersIssueId,
                        TenderAgencyId = t,
                    };
                    _context.SobyCustomersTenderAgency.Add(sobyCustomersTenderItems);
                    _context.SaveChanges();
                });
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateSocustomersIssue")]
        public SocustomersIssueModel UpdateSocustomersIssue(SocustomersIssueModel value)
        {
            var socustomersIssue = _context.SocustomersIssue.SingleOrDefault(p => p.SocustomersIssueId == value.SocustomersIssueId);
            socustomersIssue.SocustomersId = value.SobyCustomersId;
            socustomersIssue.MustSupplyInFull = value.MustSupplyInFullFlag == "Yes" ? true : false;
            socustomersIssue.MustFollowDeliveryDate = value.MustFollowDeliveryDateFlag == "Yes" ? true : false;
            socustomersIssue.ShelfLifeRequirementDate = value.ShelfLifeRequirementDate;
            socustomersIssue.StatusCodeId = value.StatusCodeID;
            socustomersIssue.ModifiedByUserId = value.ModifiedByUserID;
            socustomersIssue.ModifiedDate = DateTime.Now;
            socustomersIssue.BuyingThroughId = value.BuyingThroughId;
            socustomersIssue.TenderAgencyId = value.TenderAgencyId;
            socustomersIssue.CentralizedWithId = value.CentralizedWithId;
            _context.SaveChanges();
            var sobyCustomersMannerItems = _context.SobyCustomersManner.Where(w => w.SocustomersIssueId == value.SocustomersIssueId).ToList();
            if (sobyCustomersMannerItems != null)
            {
                _context.SobyCustomersManner.RemoveRange(sobyCustomersMannerItems);
                _context.SaveChanges();
            }
            if (value.SobyCustomersMannerIds != null && value.SobyCustomersMannerIds.Count > 0)
            {
                value.SobyCustomersMannerIds.ForEach(u =>
                {
                    var sobyCustomersMannerItems = new SobyCustomersManner()
                    {
                        SocustomersIssueId = socustomersIssue.SocustomersIssueId,
                        MannersId = u,
                    };
                    _context.SobyCustomersManner.Add(sobyCustomersMannerItems);
                    _context.SaveChanges();
                });
            }
            var sobyCustomersTenderAgencyItems = _context.SobyCustomersTenderAgency.Where(w => w.SocustomersIssueId == value.SocustomersIssueId).ToList();
            if (sobyCustomersTenderAgencyItems != null)
            {
                _context.SobyCustomersTenderAgency.RemoveRange(sobyCustomersTenderAgencyItems);
                _context.SaveChanges();
            }
            if (value.SobyCustomersTenderAgencyIds != null && value.SobyCustomersTenderAgencyIds.Count > 0)
            {
                value.SobyCustomersTenderAgencyIds.ForEach(t =>
                {
                    var sobyCustomersTenderItems = new SobyCustomersTenderAgency()
                    {
                        SocustomersIssueId = socustomersIssue.SocustomersIssueId,
                        TenderAgencyId = t,
                    };
                    _context.SobyCustomersTenderAgency.Add(sobyCustomersTenderItems);
                    _context.SaveChanges();
                });
            }
            return value;
        }
        [HttpPost]
        [Route("InsertSocustomersItemCrossReference")]
        public SocustomersItemCrossReferenceModel InsertSocustomersItemCrossReference(SocustomersItemCrossReferenceModel value)
        {
            var SocustomersItemCrossReference = new SocustomersItemCrossReference
            {
                SobyCustomersId = value.SobyCustomersId,
                CustomerReferenceNo = value.CustomerReferenceNo,
                CustomerReferenceNo2 = value.CustomerReferenceNo2,
                NavItemId = value.NavItemId,
                Description = value.Description,
                CustomerPackingUomId = value.CustomerPackingUomId,
                PerUomId = value.PerUomId,
                PurchasePerUomId = value.PurchasePerUomId,
                ShelfLifeDate = value.ShelfLifeDate,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.ModifiedByUserID,
                AddedDate = DateTime.Now,
                MinShelfLife = value.MinShelfLife,
                IsSameAsSunward = value.IsSameAsSunwardFlag == "Yes" ? true : false,
            };
            _context.SocustomersItemCrossReference.Add(SocustomersItemCrossReference);
            _context.SaveChanges();
            SocustomersItemCrossReference.SocustomersItemCrossReferenceId = SocustomersItemCrossReference.SocustomersItemCrossReferenceId;
            value.SocustomersItemCrossReferenceId = SocustomersItemCrossReference.SocustomersItemCrossReferenceId;
            if (value.TypeOfPermitIds != null && value.TypeOfPermitIds.Count > 0)
            {
                value.TypeOfPermitIds.ForEach(u =>
                {
                    var TyperOfPermitItems = new SobyCustomersTypeOfPermit()
                    {
                        SocustomersItemCrossReferenceId = SocustomersItemCrossReference.SocustomersItemCrossReferenceId,
                        TypeOfPermitId = u,
                        PermitType = "Cross Reference",
                    };
                    _context.SobyCustomersTypeOfPermit.Add(TyperOfPermitItems);
                    _context.SaveChanges();
                });
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateSocustomersItemCrossReference")]
        public SocustomersItemCrossReferenceModel UpdateSocustomersItemCrossReference(SocustomersItemCrossReferenceModel value)
        {
            var socustomersItemCrossReference = _context.SocustomersItemCrossReference.SingleOrDefault(p => p.SocustomersItemCrossReferenceId == value.SocustomersItemCrossReferenceId);
            socustomersItemCrossReference.SobyCustomersId = value.SobyCustomersId;
            socustomersItemCrossReference.CustomerReferenceNo = value.CustomerReferenceNo;
            socustomersItemCrossReference.CustomerReferenceNo2 = value.CustomerReferenceNo2;
            socustomersItemCrossReference.NavItemId = value.NavItemId;
            socustomersItemCrossReference.Description = value.Description;
            socustomersItemCrossReference.CustomerPackingUomId = value.CustomerPackingUomId;
            socustomersItemCrossReference.PerUomId = value.PerUomId;
            socustomersItemCrossReference.PurchasePerUomId = value.PurchasePerUomId;
            socustomersItemCrossReference.ShelfLifeDate = value.ShelfLifeDate;
            socustomersItemCrossReference.StatusCodeId = value.StatusCodeID;
            socustomersItemCrossReference.ModifiedByUserId = value.ModifiedByUserID;
            socustomersItemCrossReference.MinShelfLife = value.MinShelfLife;
            socustomersItemCrossReference.ModifiedDate = DateTime.Now;
            socustomersItemCrossReference.IsSameAsSunward = value.IsSameAsSunwardFlag == "Yes" ? true : false;
            _context.SaveChanges();
            var permitRemove = _context.SobyCustomersTypeOfPermit.Where(w => w.SocustomersItemCrossReferenceId == socustomersItemCrossReference.SocustomersItemCrossReferenceId).ToList();
            if (permitRemove != null)
            {
                _context.SobyCustomersTypeOfPermit.RemoveRange(permitRemove);
                _context.SaveChanges();
            }
            if (value.TypeOfPermitIds != null && value.TypeOfPermitIds.Count > 0)
            {
                value.TypeOfPermitIds.ForEach(u =>
                {
                    var TyperOfPermitItems = new SobyCustomersTypeOfPermit()
                    {
                        SocustomersItemCrossReferenceId = socustomersItemCrossReference.SocustomersItemCrossReferenceId,
                        TypeOfPermitId = u,
                        PermitType = "Cross Reference",
                    };
                    _context.SobyCustomersTypeOfPermit.Add(TyperOfPermitItems);
                    _context.SaveChanges();
                });
            }
            var SobyCustomerSunwardEquivalent = _context.SobyCustomerSunwardEquivalent.Where(w => w.SocustomersItemCrossReferenceId == value.SocustomersItemCrossReferenceId).ToList();
            if (SobyCustomerSunwardEquivalent != null)
            {
                SobyCustomerSunwardEquivalent.ForEach(h =>
                {
                    var SobyCustomerSunwardEquivalentItem = _context.SobyCustomerSunwardEquivalent.SingleOrDefault(s => s.SobyCustomerSunwardEquivalentId == h.SobyCustomerSunwardEquivalentId);
                    SobyCustomerSunwardEquivalentItem.StatusCodeId = 1802;
                });
                _context.SaveChanges();
            }
            return value;
        }
        [HttpPost]
        [Route("InsertSocustomersDelivery")]
        public SocustomersDeliveryModel InsertSocustomersDelivery(SocustomersDeliveryModel value)
        {
            var socustomersDelivery = new SocustomersDelivery
            {
                SocustomersId = value.SocustomersId,
                RequireSpecialPermit = value.RequireSpecialPermitFlag == "Yes" ? true : false,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.ModifiedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.SocustomersDelivery.Add(socustomersDelivery);
            _context.SaveChanges();
            socustomersDelivery.SocustomersDeliveryId = socustomersDelivery.SocustomersDeliveryId;
            if (value.ModeofDeliveryIds != null && value.ModeofDeliveryIds.Count > 0)
            {
                value.ModeofDeliveryIds.ForEach(u =>
                {
                    var modeofDeliveryItems = new SobyCustomerModeDelivery()
                    {
                        SocustomersDeliveryId = socustomersDelivery.SocustomersDeliveryId,
                        ModeOfDeliveryId = u,
                    };
                    _context.SobyCustomerModeDelivery.Add(modeofDeliveryItems);
                    _context.SaveChanges();
                });
            }
            if (value.DeliveryTyperOfPermitIds != null && value.DeliveryTyperOfPermitIds.Count > 0)
            {
                value.DeliveryTyperOfPermitIds.ForEach(u =>
                {
                    var deliveryTyperOfPermitItems = new SobyCustomersTypeOfPermit()
                    {
                        SocustomersDeliveryId = socustomersDelivery.SocustomersDeliveryId,
                        TypeOfPermitId = u,
                        PermitType = "Delivery",
                    };
                    _context.SobyCustomersTypeOfPermit.Add(deliveryTyperOfPermitItems);
                    _context.SaveChanges();
                });
            }
            value.SocustomersDeliveryId = socustomersDelivery.SocustomersDeliveryId;
            return value;
        }
        [HttpPut]
        [Route("UpdateSocustomersDelivery")]
        public SocustomersDeliveryModel UpdateSocustomersDelivery(SocustomersDeliveryModel value)
        {
            var socustomersDelivery = _context.SocustomersDelivery.SingleOrDefault(p => p.SocustomersDeliveryId == value.SocustomersDeliveryId);
            socustomersDelivery.SocustomersId = value.SocustomersId;
            socustomersDelivery.RequireSpecialPermit = value.RequireSpecialPermitFlag == "Yes" ? true : false;
            socustomersDelivery.StatusCodeId = value.StatusCodeID;
            socustomersDelivery.AddedByUserId = value.AddedByUserID;
            socustomersDelivery.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            var modeofDeliveryRemove = _context.SobyCustomerModeDelivery.Where(w => w.SocustomersDeliveryId == socustomersDelivery.SocustomersDeliveryId).ToList();
            if (modeofDeliveryRemove != null)
            {
                _context.SobyCustomerModeDelivery.RemoveRange(modeofDeliveryRemove);
                _context.SaveChanges();
            }
            if (value.ModeofDeliveryIds != null && value.ModeofDeliveryIds.Count > 0)
            {
                value.ModeofDeliveryIds.ForEach(u =>
                {
                    var modeofDeliveryItems = new SobyCustomerModeDelivery()
                    {
                        SocustomersDeliveryId = socustomersDelivery.SocustomersDeliveryId,
                        ModeOfDeliveryId = u,
                    };
                    _context.SobyCustomerModeDelivery.Add(modeofDeliveryItems);
                    _context.SaveChanges();
                });
            }
            var permitRemove = _context.SobyCustomersTypeOfPermit.Where(w => w.SocustomersDeliveryId == socustomersDelivery.SocustomersDeliveryId).ToList();
            if (permitRemove != null)
            {
                _context.SobyCustomersTypeOfPermit.RemoveRange(permitRemove);
                _context.SaveChanges();
            }
            if (value.DeliveryTyperOfPermitIds != null && value.DeliveryTyperOfPermitIds.Count > 0)
            {
                value.DeliveryTyperOfPermitIds.ForEach(u =>
                {
                    var deliveryTyperOfPermitItems = new SobyCustomersTypeOfPermit()
                    {
                        SocustomersDeliveryId = socustomersDelivery.SocustomersDeliveryId,
                        TypeOfPermitId = u,
                        PermitType = "Delivery",
                    };
                    _context.SobyCustomersTypeOfPermit.Add(deliveryTyperOfPermitItems);
                    _context.SaveChanges();
                });
            }
            return value;
        }
        [HttpPost]
        [Route("InsertSobyCustomersAddress")]
        public SobyCustomersAddressModel InsertSobyCustomersAddress(SobyCustomersAddressModel value)
        {
            var profileNo = _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID, StatusCodeID = 710, Title = value.LocationName });
            var SobyCustomersMasterAddress = new SobyCustomersMasterAddress
            {
                SobyCustomersId = value.SobyCustomersId,
                Address = value.Address,
                AddressName = value.LocationName,
                PostalCode = value.PostalCode,
                CountryId = value.CountryId,
                Address2 = value.Address2,
                StateId = value.StateId,
                PhoneNo = value.PhoneNo,
                ContactName = value.ContactName,
                CityId = value.CityId,
                EmailAddress = value.EmailAddress,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                FaxNo = value.FaxNo,
                Uenno = value.Uenno,
                CompanyRegisterationNo = value.CompanyRegisterationNo,
                Vatgstno = value.Vatgstno,
                AddressTypeId = value.AddressTypeId,
                DeliveryInformation = value.DeliveryInformation,
            };
            _context.SobyCustomersMasterAddress.Add(SobyCustomersMasterAddress);
            _context.SaveChanges();
            value.SobyCustomersMasterAddressId = SobyCustomersMasterAddress.SobyCustomersMasterAddressId;
            var sobyCustomersAddress = new SobyCustomersAddress
            {
                SobyCustomersId = value.SobyCustomersId,
                LocationNo = profileNo,
                LocationName = value.LocationName,
                InvoiceAddress = value.InvoiceAddressFlag == "Yes" ? true : false,
                DeliverySchedule = value.DeliverySchedule,
                //PrimaryContactCode = value.PrimaryContactCode,
                TypeOfAddressId = value.TypeOfAddressId,
                NavisionNo = value.NavisionNo,
                CustomerCodeId = value.CustomerCodeId,
                SobyCustomersMasterAddressId = value.SobyCustomersMasterAddressId,
            };
            _context.SobyCustomersAddress.Add(sobyCustomersAddress);
            value.LocationNo = profileNo;
            _context.SaveChanges();
            value.SobyCustomersAddressId = sobyCustomersAddress.SobyCustomersAddressId;
            return value;
        }
        [HttpPut]
        [Route("UpdateSobyCustomersAddress")]
        public SobyCustomersAddressModel UpdateSobyCustomersAddress(SobyCustomersAddressModel value)
        {
            var sobyCustomersMasterAddress = _context.SobyCustomersMasterAddress.SingleOrDefault(p => p.SobyCustomersMasterAddressId == value.SobyCustomersMasterAddressId);
            sobyCustomersMasterAddress.SobyCustomersId = value.SobyCustomersId;
            sobyCustomersMasterAddress.AddressName = value.LocationName;
            sobyCustomersMasterAddress.Address = value.Address;
            sobyCustomersMasterAddress.PostalCode = value.PostalCode;
            sobyCustomersMasterAddress.CountryId = value.CountryId;
            sobyCustomersMasterAddress.Address2 = value.Address2;
            sobyCustomersMasterAddress.StateId = value.StateId;
            sobyCustomersMasterAddress.PhoneNo = value.PhoneNo;
            sobyCustomersMasterAddress.ContactName = value.ContactName;
            sobyCustomersMasterAddress.CityId = value.CityId;
            sobyCustomersMasterAddress.EmailAddress = value.EmailAddress;
            sobyCustomersMasterAddress.StatusCodeId = value.StatusCodeID;
            sobyCustomersMasterAddress.ModifiedByUserId = value.ModifiedByUserID;
            sobyCustomersMasterAddress.ModifiedDate = DateTime.Now;
            sobyCustomersMasterAddress.FaxNo = value.FaxNo;
            sobyCustomersMasterAddress.Uenno = value.Uenno;
            sobyCustomersMasterAddress.CompanyRegisterationNo = value.CompanyRegisterationNo;
            sobyCustomersMasterAddress.Vatgstno = value.Vatgstno;
            sobyCustomersMasterAddress.AddressTypeId = value.AddressTypeId;
            sobyCustomersMasterAddress.DeliveryInformation = value.DeliveryInformation;
            _context.SaveChanges();
            var sobyCustomersAddress = _context.SobyCustomersAddress.SingleOrDefault(p => p.SobyCustomersAddressId == value.SobyCustomersAddressId);
            sobyCustomersAddress.SobyCustomersId = value.SobyCustomersId;
            sobyCustomersAddress.LocationNo = value.LocationNo;
            sobyCustomersAddress.LocationName = value.LocationName;
            sobyCustomersAddress.InvoiceAddress = value.InvoiceAddressFlag == "Yes" ? true : false;
            sobyCustomersAddress.DeliverySchedule = value.DeliverySchedule;
            sobyCustomersAddress.TypeOfAddressId = value.TypeOfAddressId;
            sobyCustomersAddress.NavisionNo = value.NavisionNo;
            sobyCustomersAddress.CustomerCodeId = value.CustomerCodeId;
            _context.SaveChanges();
            return value;
        }
        [HttpPost]
        [Route("InsertSobyCustomersContactInfo")]
        public SobyCustomerContactInfoModel InsertSobyCustomersContactInfo(SobyCustomerContactInfoModel value)
        {
            var sobyCustomersMasterAddress = new SobyCustomersMasterAddress
            {
                SobyCustomersId = value.SobyCustomersId,
                Department = value.Department,
                Designation = value.Designation,
                Name = value.Name,
                NickName = value.NickName,
                PhoneNo = value.PhoneNumber,
                FaxNo = value.FaxNumber,
                EmailAddress = value.EmailAddress,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                AddressTypeId = value.AddressTypeId,
            };
            _context.SobyCustomersMasterAddress.Add(sobyCustomersMasterAddress);
            _context.SaveChanges();
            value.SobyCustomersMasterAddressId = sobyCustomersMasterAddress.SobyCustomersMasterAddressId;
            var sobyCustomersAddress = new SobyCustomerContactInfo
            {
                StationLocationId = value.StationLocationId,
                SobyCustomersId = value.SobyCustomersId,
                SobyCustomersMasterAddressId = value.SobyCustomersMasterAddressId,
            };
            _context.SobyCustomerContactInfo.Add(sobyCustomersAddress);
            _context.SaveChanges();
            value.SobyCustomerContactInfoId = sobyCustomersAddress.SobyCustomerContactInfoId;
            return value;
        }
        [HttpPut]
        [Route("UpdateSobyCustomersContactInfo")]
        public SobyCustomerContactInfoModel UpdateSobyCustomersContactInfo(SobyCustomerContactInfoModel value)
        {
            var sobyCustomersMasterAddress = _context.SobyCustomersMasterAddress.SingleOrDefault(p => p.SobyCustomersMasterAddressId == value.SobyCustomersMasterAddressId);
            sobyCustomersMasterAddress.SobyCustomersId = value.SobyCustomersId;
            sobyCustomersMasterAddress.Department = value.Department;
            sobyCustomersMasterAddress.Designation = value.Designation;
            sobyCustomersMasterAddress.Name = value.Name;
            sobyCustomersMasterAddress.NickName = value.NickName;
            sobyCustomersMasterAddress.PhoneNo = value.PhoneNumber;
            sobyCustomersMasterAddress.FaxNo = value.FaxNumber;
            sobyCustomersMasterAddress.EmailAddress = value.EmailAddress;
            sobyCustomersMasterAddress.StatusCodeId = value.StatusCodeID.Value;
            sobyCustomersMasterAddress.ModifiedByUserId = value.ModifiedByUserID;
            sobyCustomersMasterAddress.ModifiedDate = DateTime.Now;
            sobyCustomersMasterAddress.AddressTypeId = value.AddressTypeId;
            _context.SaveChanges();
            var sobyCustomersAddress = _context.SobyCustomerContactInfo.SingleOrDefault(p => p.SobyCustomerContactInfoId == value.SobyCustomerContactInfoId);
            sobyCustomersAddress.SobyCustomersId = value.SobyCustomersId;
            sobyCustomersAddress.StationLocationId = value.StationLocationId;
            sobyCustomersAddress.SobyCustomersMasterAddressId = value.SobyCustomersMasterAddressId;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteSobyCustomersContactInfo")]
        public ActionResult<string> DeleteSobyCustomersContactInfo(int id)
        {
            try
            {
                var sobyCustomersAddress = _context.SobyCustomerContactInfo.Where(p => p.SobyCustomerContactInfoId == id).FirstOrDefault();
                if (sobyCustomersAddress != null)
                {
                    _context.SobyCustomerContactInfo.RemoveRange(sobyCustomersAddress);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteSobyCustomersAddress")]
        public ActionResult<string> DeleteSobyCustomersAddress(int id)
        {
            try
            {
                var sobyCustomersAddress = _context.SobyCustomersAddress.Where(p => p.SobyCustomersAddressId == id).FirstOrDefault();
                if (sobyCustomersAddress != null)
                {
                    _context.SobyCustomersAddress.RemoveRange(sobyCustomersAddress);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteSobyCustomersIssue")]
        public ActionResult<string> DeleteSobyCustomersIssue(int id)
        {
            try
            {
                var socustomersIssue = _context.SocustomersIssue.Where(p => p.SocustomersIssueId == id).FirstOrDefault();
                if (socustomersIssue != null)
                {
                    var sobyCustomersManner = _context.SobyCustomersManner.Where(p => p.SocustomersIssueId == id).ToList();
                    if (socustomersIssue != null)
                    {
                        _context.SobyCustomersManner.RemoveRange(sobyCustomersManner);
                        _context.SaveChanges();
                    }
                    var sobyCustomersTenderAgency = _context.SobyCustomersTenderAgency.Where(p => p.SocustomersIssueId == id).ToList();
                    if (sobyCustomersTenderAgency != null)
                    {
                        _context.SobyCustomersTenderAgency.RemoveRange(sobyCustomersTenderAgency);
                        _context.SaveChanges();
                    }
                    _context.SocustomersIssue.Remove(socustomersIssue);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteSobyCustomersDelivery")]
        public ActionResult<string> DeleteSobyCustomersDelivery(int id)
        {
            try
            {
                var socustomersDelivery = _context.SocustomersDelivery.Where(p => p.SocustomersDeliveryId == id).FirstOrDefault();
                if (socustomersDelivery != null)
                {
                    var sobyCustomerModeDelivery = _context.SobyCustomerModeDelivery.Where(p => p.SocustomersDeliveryId == id).ToList();
                    if (socustomersDelivery != null)
                    {
                        _context.SobyCustomerModeDelivery.RemoveRange(sobyCustomerModeDelivery);
                        _context.SaveChanges();
                    }
                    var sobyCustomersTypeOfPermit = _context.SobyCustomersTypeOfPermit.Where(p => p.SocustomersDeliveryId == id).ToList();
                    if (sobyCustomersTypeOfPermit != null)
                    {
                        _context.SobyCustomersTypeOfPermit.RemoveRange(sobyCustomersTypeOfPermit);
                        _context.SaveChanges();
                    }
                    _context.SocustomersDelivery.Remove(socustomersDelivery);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteSocustomersItemCrossReference")]
        public ActionResult<string> DeleteSocustomersItemCrossReference(int id)
        {
            try
            {
                var socustomersItemCrossReference = _context.SocustomersItemCrossReference.Where(p => p.SocustomersItemCrossReferenceId == id).FirstOrDefault();
                if (socustomersItemCrossReference != null)
                {
                    var sobyCustomersTypeOfPermit = _context.SobyCustomersTypeOfPermit.Where(p => p.SocustomersItemCrossReferenceId == id).ToList();
                    if (sobyCustomersTypeOfPermit != null)
                    {
                        _context.SobyCustomersTypeOfPermit.RemoveRange(sobyCustomersTypeOfPermit);
                        _context.SaveChanges();
                    }
                    var sobyCustomerSunwardEquivalent = _context.SobyCustomerSunwardEquivalent.Where(p => p.SocustomersItemCrossReferenceId == id).ToList();
                    if (sobyCustomerSunwardEquivalent != null)
                    {
                        _context.SobyCustomerSunwardEquivalent.RemoveRange(sobyCustomerSunwardEquivalent);
                        _context.SaveChanges();
                    }
                    _context.SocustomersItemCrossReference.Remove(socustomersItemCrossReference);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteSobyCustomerSunwardEquivalent")]
        public ActionResult<string> DeleteSobyCustomerSunwardEquivalent(int id)
        {
            try
            {
                var sobyCustomerSunwardEquivalent = _context.SobyCustomerSunwardEquivalent.Where(p => p.SobyCustomerSunwardEquivalentId == id).FirstOrDefault();
                if (sobyCustomerSunwardEquivalent != null)
                {
                    _context.SobyCustomerSunwardEquivalent.Remove(sobyCustomerSunwardEquivalent);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteSocustomersSalesInfomation")]
        public ActionResult<string> DeleteSocustomersSalesInfomation(int id)
        {
            try
            {
                var socustomersSalesInfomation = _context.SocustomersSalesInfomation.Where(p => p.SocustomersSalesInfomationId == id).FirstOrDefault();
                if (socustomersSalesInfomation != null)
                {
                    _context.SocustomersSalesInfomation.RemoveRange(socustomersSalesInfomation);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteSobyCustomers")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var sobyCustomers = _context.SobyCustomers.Where(p => p.SobyCustomersId == id).FirstOrDefault();
                if (sobyCustomers != null)
                {
                    var SobyCustomerContactInfo = _context.SobyCustomerContactInfo.Where(p => p.SobyCustomersId == id).ToList();
                    if (SobyCustomerContactInfo != null)
                    {
                        _context.SobyCustomerContactInfo.RemoveRange(SobyCustomerContactInfo);
                        _context.SaveChanges();
                    }
                    var socustomersIssue = _context.SocustomersIssue.Where(p => p.SocustomersId == id).ToList();
                    if (socustomersIssue != null)
                    {
                        socustomersIssue.ForEach(h =>
                        {
                            var sobyCustomersManner = _context.SobyCustomersManner.Where(p => p.SocustomersIssueId == h.SocustomersIssueId).ToList();
                            if (socustomersIssue != null)
                            {
                                _context.SobyCustomersManner.RemoveRange(sobyCustomersManner);
                                _context.SaveChanges();
                            }
                            var sobyCustomersTenderAgency = _context.SobyCustomersTenderAgency.Where(p => p.SocustomersIssueId == h.SocustomersIssueId).ToList();
                            if (sobyCustomersTenderAgency != null)
                            {
                                _context.SobyCustomersTenderAgency.RemoveRange(sobyCustomersTenderAgency);
                                _context.SaveChanges();
                            }
                        });
                        _context.SocustomersIssue.RemoveRange(socustomersIssue);
                        _context.SaveChanges();
                    }
                    var socustomersDelivery = _context.SocustomersDelivery.Where(p => p.SocustomersId == id).ToList();
                    if (socustomersDelivery != null)
                    {
                        socustomersDelivery.ForEach(h =>
                        {
                            var sobyCustomerModeDelivery = _context.SobyCustomerModeDelivery.Where(p => p.SocustomersDeliveryId == h.SocustomersDeliveryId).ToList();
                            if (socustomersDelivery != null)
                            {
                                _context.SobyCustomerModeDelivery.RemoveRange(sobyCustomerModeDelivery);
                                _context.SaveChanges();
                            }
                            var sobyCustomersTypeOfPermit = _context.SobyCustomersTypeOfPermit.Where(p => p.SocustomersDeliveryId == h.SocustomersDeliveryId).ToList();
                            if (sobyCustomersTypeOfPermit != null)
                            {
                                _context.SobyCustomersTypeOfPermit.RemoveRange(sobyCustomersTypeOfPermit);
                                _context.SaveChanges();
                            }
                        });
                        _context.SocustomersDelivery.RemoveRange(socustomersDelivery);
                        _context.SaveChanges();
                    }
                    var socustomersItemCrossReference = _context.SocustomersItemCrossReference.Where(p => p.SobyCustomersId == id).ToList();
                    if (socustomersItemCrossReference != null)
                    {
                        socustomersItemCrossReference.ForEach(h =>
                        {
                            var sobyCustomersTypeOfPermit = _context.SobyCustomersTypeOfPermit.Where(p => p.SocustomersItemCrossReferenceId == h.SocustomersItemCrossReferenceId).ToList();
                            if (sobyCustomersTypeOfPermit != null)
                            {
                                _context.SobyCustomersTypeOfPermit.RemoveRange(sobyCustomersTypeOfPermit);
                                _context.SaveChanges();
                            }
                            var sobyCustomerSunwardEquivalent = _context.SobyCustomerSunwardEquivalent.Where(p => p.SocustomersItemCrossReferenceId == h.SocustomersItemCrossReferenceId).ToList();
                            if (sobyCustomerSunwardEquivalent != null)
                            {
                                _context.SobyCustomerSunwardEquivalent.RemoveRange(sobyCustomerSunwardEquivalent);
                                _context.SaveChanges();
                            }
                        });
                        _context.SocustomersItemCrossReference.RemoveRange(socustomersItemCrossReference);
                        _context.SaveChanges();
                    }
                    var sobyCustomersAddress = _context.SobyCustomersAddress.Where(p => p.SobyCustomersId == id).ToList();
                    if (sobyCustomersAddress != null)
                    {
                        _context.SobyCustomersAddress.RemoveRange(sobyCustomersAddress);
                        _context.SaveChanges();
                    }
                    var sobyCustomersSalesAddress = _context.SobyCustomersSalesAddress.Where(p => p.SobyCustomersId == id).ToList();
                    if (sobyCustomersSalesAddress != null)
                    {
                        _context.SobyCustomersSalesAddress.RemoveRange(sobyCustomersSalesAddress);
                        _context.SaveChanges();
                    }
                    var socustomersSalesInfomation = _context.SocustomersSalesInfomation.Where(p => p.SobyCustomersId == id).ToList();
                    if (socustomersSalesInfomation != null)
                    {
                        _context.SocustomersSalesInfomation.RemoveRange(socustomersSalesInfomation);
                        _context.SaveChanges();
                    }
                    _context.SobyCustomers.Remove(sobyCustomers);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet]
        [Route("GetSocustomersItemCrossReferenceReport")]
        public List<SocustomersItemCrossReferenceReportModel> GetSocustomersItemCrossReferenceReport(int id)
        {
            var approvalItems = _context.ApprovalDetails.Include(a => a.Approver).Where(d => d.AddedByUserId == id && d.Approver.ScreenId == "Company" && d.StatusCodeId == 2200).Select(s => s.SessionId).ToList();
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var companyListing = _context.CompanyListing.AsNoTracking().ToList();
            var socustomersItemCrossReferenceReport = _context.SobyCustomerSunwardEquivalent.Include(o => o.OrderPlacetoCompany).
                Include(s => s.SocustomersItemCrossReference).Include(ss => ss.SocustomersItemCrossReference.SobyCustomers).Include(c => c.SobyCustomers).Include(n => n.NavItem).Include(s => s.SupplyTo).Where(w => w.StatusCodeId != 1803).AsNoTracking().ToList();
            List<SocustomersItemCrossReferenceReportModel> socustomersItemCrossReferenceReportModels = new List<SocustomersItemCrossReferenceReportModel>();
            socustomersItemCrossReferenceReport.ForEach(s =>
            {
                SocustomersItemCrossReferenceReportModel socustomersItemCrossReferenceReportModel = new SocustomersItemCrossReferenceReportModel();
                socustomersItemCrossReferenceReportModel.CompanyListingID = s.SocustomersItemCrossReference != null && s.SocustomersItemCrossReference.SobyCustomers != null && companyListing.Count > 0 ? (companyListing.Where(w => w.ProfileReferenceNo == s.SocustomersItemCrossReference.SobyCustomers.LinkProfileReferenceNo).Select(w => w.CompanyListingId).FirstOrDefault()) : 0;
                socustomersItemCrossReferenceReportModel.SessionId = s.SessionId;
                socustomersItemCrossReferenceReportModel.CompanyName = s.SocustomersItemCrossReference != null && s.SocustomersItemCrossReference.SobyCustomers != null && companyListing.Count > 0 ? (companyListing.Where(w => w.ProfileReferenceNo == s.SocustomersItemCrossReference.SobyCustomers.LinkProfileReferenceNo).Select(w => w.CompanyName).FirstOrDefault()) : "";
                socustomersItemCrossReferenceReportModel.CompanyReferenceNo = s.SocustomersItemCrossReference != null ? s.SocustomersItemCrossReference.CustomerReferenceNo : "";
                socustomersItemCrossReferenceReportModel.CustomerReferenceNo2 = s.SocustomersItemCrossReference != null ? s.SocustomersItemCrossReference.CustomerReferenceNo2 : "";
                socustomersItemCrossReferenceReportModel.SupplyToDescription = s.SupplyTo != null ? s.SupplyTo.GenericCodeSupplyDescription : null;
                socustomersItemCrossReferenceReportModel.Description = s.SocustomersItemCrossReference != null ? s.SocustomersItemCrossReference.Description : "";
                socustomersItemCrossReferenceReportModel.CustomerPackingUomName = s.SocustomersItemCrossReference != null && s.SocustomersItemCrossReference.CustomerPackingUomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.SocustomersItemCrossReference.CustomerPackingUomId).Select(m => m.Value).FirstOrDefault() : "";
                socustomersItemCrossReferenceReportModel.PurchasePerUomName = s.SocustomersItemCrossReference != null && s.SocustomersItemCrossReference.PurchasePerUomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.SocustomersItemCrossReference.PurchasePerUomId).Select(m => m.Value).FirstOrDefault() : "";
                socustomersItemCrossReferenceReportModel.PerUomName = s.SocustomersItemCrossReference != null && s.SocustomersItemCrossReference.PerUomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.SocustomersItemCrossReference.PerUomId).Select(m => m.Value).FirstOrDefault() : "";
                socustomersItemCrossReferenceReportModel.OrderPlacetoCompany = s.OrderPlacetoCompany != null ? s.OrderPlacetoCompany.Description : "";
                socustomersItemCrossReferenceReportModel.NavItem = s.NavItem != null ? s.NavItem.Code : null;
                socustomersItemCrossReferenceReportModel.ItemDescription = s.NavItem != null ? (s.NavItem.Code) : "";
                //socustomersItemCrossReferenceReportModel.ManufacturingSite = s.NavItem != null ? s.NavItem.InternalRef : "";
                //socustomersItemCrossReferenceReportModel.InterCompanyNo = s.NavItem != null ? s.NavItem.Company : "";
                socustomersItemCrossReferenceReportModel.BUOM = s.NavItem != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.NavItem.Uom).Select(m => m.Value).FirstOrDefault() : null;
                socustomersItemCrossReferenceReportModel.FactorVsCustomerPacking = s.FactorVsCustomerPacking;
                socustomersItemCrossReferenceReportModel.FactorVsPurchasePacking = s.FactorVsPurchasePacking;
                socustomersItemCrossReferenceReportModel.IsThisDefaultFlag = s.IsThisDefault == true ? "Yes" : "No";
                socustomersItemCrossReferenceReportModel.DefaultSupply = s.DefaultSupply;
                socustomersItemCrossReferenceReportModel.CrossReferenceID = s.SocustomersItemCrossReferenceId;
                socustomersItemCrossReferenceReportModel.SobyCustomerSunwardEquivalentId = s.SobyCustomerSunwardEquivalentId;
                socustomersItemCrossReferenceReportModels.Add(socustomersItemCrossReferenceReportModel);
            });
            return socustomersItemCrossReferenceReportModels.Where(s => approvalItems.Contains(s.SessionId)).ToList();
        }

        [HttpGet]
        [Route("GetSocustomersItemCrossReferenceForPurchaseItem")]
        public List<SocustomersItemCrossReferenceModel> GetSocustomersItemCrossReferenceForPurchaseItem(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var SocustomersItemCrossReference = _context.SocustomersItemCrossReference.Include(a => a.AddedByUser)
               .Include(m => m.ModifiedByUser).Include(p => p.SobyCustomersTypeOfPermit)
               .Include(s => s.StatusCode).Where(s => s.SobyCustomersId == id).AsNoTracking().ToList();
            List<SocustomersItemCrossReferenceModel> SocustomersItemCrossReferenceModels = new List<SocustomersItemCrossReferenceModel>();
            SocustomersItemCrossReference.ForEach(s =>
            {
                SocustomersItemCrossReferenceModel SocustomersItemCrossReferenceModel = new SocustomersItemCrossReferenceModel();
                SocustomersItemCrossReferenceModel.SocustomersItemCrossReferenceId = s.SocustomersItemCrossReferenceId;
                SocustomersItemCrossReferenceModel.SobyCustomersId = s.SobyCustomersId;
                SocustomersItemCrossReferenceModel.CustomerReferenceNo = s.CustomerReferenceNo;
                SocustomersItemCrossReferenceModel.CustomerReferenceNo2 = s.CustomerReferenceNo2;
                SocustomersItemCrossReferenceModel.NavItemId = s.NavItemId;
                SocustomersItemCrossReferenceModel.Description = s.Description;
                SocustomersItemCrossReferenceModel.CustomerPackingUomId = s.CustomerPackingUomId;
                SocustomersItemCrossReferenceModel.PerUomId = s.PerUomId;
                SocustomersItemCrossReferenceModel.PurchasePerUomId = s.PurchasePerUomId;
                SocustomersItemCrossReferenceModel.ProductNo = s.CustomerReferenceNo + " | " + s.Description;
                SocustomersItemCrossReferenceModel.CustomerPackingUomName = s.CustomerPackingUomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CustomerPackingUomId).Select(m => m.Value).FirstOrDefault() : "";
                SocustomersItemCrossReferenceModel.PerUomName = s.PerUomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PerUomId).Select(m => m.Value).FirstOrDefault() : "";
                SocustomersItemCrossReferenceModel.PurchasePerUomName = s.PurchasePerUomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PurchasePerUomId).Select(m => m.Value).FirstOrDefault() : "";
                SocustomersItemCrossReferenceModels.Add(SocustomersItemCrossReferenceModel);
            });
            return SocustomersItemCrossReferenceModels.OrderByDescending(a => a.SocustomersItemCrossReferenceId).ToList();
        }

        [HttpGet]
        [Route("GetSocustomersItemCrossReferenceForSalesNovate")]
        public List<SocustomersItemCrossReferenceModel> GetSocustomersItemCrossReferenceForSalesNovate(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var sobycustomersIds = _context.PurchaseItemSalesEntryLine.Where(s => s.SalesOrderEntryId == id).Select(s => s.SobyCustomersId).ToList();
            var SocustomersItemCrossReference = _context.SocustomersItemCrossReference.Include(a => a.AddedByUser)
               .Include(m => m.ModifiedByUser).Include(p => p.SobyCustomersTypeOfPermit)
               .Include(s => s.StatusCode).AsNoTracking().ToList();
            if (SocustomersItemCrossReference != null && SocustomersItemCrossReference.Count > 0 && sobycustomersIds != null)
            {
                SocustomersItemCrossReference = SocustomersItemCrossReference.Where(s => sobycustomersIds.Contains(s.SocustomersItemCrossReferenceId)).ToList();
            }
            List<SocustomersItemCrossReferenceModel> SocustomersItemCrossReferenceModels = new List<SocustomersItemCrossReferenceModel>();
            SocustomersItemCrossReference.ForEach(s =>
            {
                SocustomersItemCrossReferenceModel SocustomersItemCrossReferenceModel = new SocustomersItemCrossReferenceModel();
                SocustomersItemCrossReferenceModel.SocustomersItemCrossReferenceId = s.SocustomersItemCrossReferenceId;
                SocustomersItemCrossReferenceModel.SobyCustomersId = s.SobyCustomersId;
                SocustomersItemCrossReferenceModel.CustomerReferenceNo = s.CustomerReferenceNo;
                SocustomersItemCrossReferenceModel.CustomerReferenceNo2 = s.CustomerReferenceNo2;
                SocustomersItemCrossReferenceModel.NavItemId = s.NavItemId;
                SocustomersItemCrossReferenceModel.Description = s.Description;
                SocustomersItemCrossReferenceModel.CustomerPackingUomId = s.CustomerPackingUomId;
                SocustomersItemCrossReferenceModel.PerUomId = s.PerUomId;
                SocustomersItemCrossReferenceModel.PurchasePerUomId = s.PurchasePerUomId;
                SocustomersItemCrossReferenceModel.ProductNo = s.CustomerReferenceNo + " | " + s.Description;
                SocustomersItemCrossReferenceModel.CustomerPackingUomName = s.CustomerPackingUomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.CustomerPackingUomId).Select(m => m.Value).FirstOrDefault() : "";
                SocustomersItemCrossReferenceModel.PerUomName = s.PerUomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PerUomId).Select(m => m.Value).FirstOrDefault() : "";
                SocustomersItemCrossReferenceModel.PurchasePerUomName = s.PurchasePerUomId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PurchasePerUomId).Select(m => m.Value).FirstOrDefault() : "";
                SocustomersItemCrossReferenceModels.Add(SocustomersItemCrossReferenceModel);
            });
            return SocustomersItemCrossReferenceModels.OrderByDescending(a => a.SocustomersItemCrossReferenceId).ToList();
        }
        [HttpGet]
        [Route("GetSoBycustomersTender")]
        public List<CompanyListingModel> GetSoBycustomersTender()
        {
            var CompanyListingItems = _context.CompanyListing.Include(c => c.CompanyListingCompanyType).ToList();
            List<CompanyListingModel> CompanyListingModels = new List<CompanyListingModel>();
            List<SocustomersIssueModel> SocustomersIssues = new List<SocustomersIssueModel>();
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var SOCustomersIssue = _context.SocustomersIssue.Include(s => s.Socustomers).Include(a => a.SobyCustomersManner).Include(b => b.SobyCustomersTenderAgency).ToList();
            if (SOCustomersIssue.Count > 0)
            {
                SOCustomersIssue.ForEach(s =>
                {
                    SocustomersIssueModel SocustomersIssue = new SocustomersIssueModel();
                    SocustomersIssue.LinkProfileReferenceNo = s.Socustomers != null ? s.Socustomers.LinkProfileReferenceNo : "";
                    SocustomersIssue.CompanyListingID = s.Socustomers != null && CompanyListingItems.Count > 0 ? CompanyListingItems.Where(w => w.ProfileReferenceNo == s.Socustomers.LinkProfileReferenceNo).Select(w => w.CompanyListingId).FirstOrDefault() : 0;
                    SocustomersIssue.CompanyListingName = s.Socustomers != null && CompanyListingItems.Count > 0 ? CompanyListingItems.Where(w => w.ProfileReferenceNo == s.Socustomers.LinkProfileReferenceNo).Select(w => w.CompanyName).FirstOrDefault() : "";
                    SocustomersIssue.SocustomersIssueId = s.SocustomersIssueId;
                    SocustomersIssue.SocustomersId = s.SocustomersId;
                    SocustomersIssue.SobyCustomersTenderAgencyList = (s.SobyCustomersTenderAgency != null && masterDetailList != null ? (string.Join(",", s.SobyCustomersTenderAgency.Where(b => b.SocustomersIssueId == s.SocustomersIssueId).Select(b => masterDetailList.Where(m => m.ApplicationMasterDetailId == b.TenderAgencyId && m.Value.ToLower() == "alps gpo").Select(m => m.Value).FirstOrDefault()).ToList())).TrimStart(',') : "").TrimEnd(',');
                    SocustomersIssue.SobyCustomersMannerList = (s.SobyCustomersManner != null && masterDetailList != null ? (string.Join(",", s.SobyCustomersManner.Where(b => b.SocustomersIssueId == s.SocustomersIssueId).Select(b => masterDetailList.Where(m => m.ApplicationMasterDetailId == b.MannersId && m.Value.ToLower() == "by tender agency").Select(m => m.Value).FirstOrDefault()).ToList())).TrimStart(',') : "").TrimEnd(',');
                    var companyListingCompanyType = CompanyListingItems.Where(c => c.CompanyListingId == SocustomersIssue.CompanyListingID).FirstOrDefault();
                    SocustomersIssue.CompanyListingTypeList = (companyListingCompanyType != null && companyListingCompanyType.CompanyListingCompanyType != null && masterDetailList != null ? (string.Join(",", companyListingCompanyType.CompanyListingCompanyType.Select(b => masterDetailList.Where(m => m.ApplicationMasterDetailId == b.CompanyTypeId && m.Value.ToLower() == "customer").Select(m => m.Value).FirstOrDefault()).ToList())).TrimStart(',') : "").TrimEnd(',');
                    SocustomersIssues.Add(SocustomersIssue);
                });
            };
            var SocustomersIssuess = SocustomersIssues.Where(w => w.SobyCustomersTenderAgencyList.ToLower() == "alps gpo" && w.SobyCustomersMannerList.ToLower() == "by tender agency" && w.CompanyListingTypeList.ToLower() == "customer").ToList();
            var companyListingItems = SocustomersIssuess.GroupBy(s => new { s.CompanyListingID, s.CompanyListingName }).Select(s => new { s.Key.CompanyListingID, s.Key.CompanyListingName }).ToList();
            companyListingItems.ForEach(h =>
            {
                CompanyListingModel CompanyListingModel = new CompanyListingModel();
                CompanyListingModel.CompanyListingID = h.CompanyListingID;
                CompanyListingModel.CompanyListingName = h.CompanyListingName;
                CompanyListingModels.Add(CompanyListingModel);
            });
            return CompanyListingModels;
        }

        [HttpGet]
        [Route("GetSoBycustomersTenderAgency")]
        public List<CompanyListingModel> GetSoBycustomersTenderAgency(int id)
        {
            var CompanyListingItems = _context.CompanyListing.Include(c => c.CompanyListingCompanyType).ToList();
            var CompanyListingCustomerCodeItems = _context.CompanyListingCustomerCode.ToList();
            List<CompanyListingModel> CompanyListingModels = new List<CompanyListingModel>();
            List<SocustomersIssueModel> SocustomersIssues = new List<SocustomersIssueModel>();
            var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var SOCustomersIssue = _context.SocustomersIssue.Include(s => s.Socustomers).Include(a => a.SobyCustomersManner).Include(b => b.SobyCustomersTenderAgency).Where(s => s.TenderAgencyId == id).ToList();
            if (SOCustomersIssue.Count > 0)
            {
                SOCustomersIssue.ForEach(s =>
                {
                    SocustomersIssueModel SocustomersIssue = new SocustomersIssueModel();
                    SocustomersIssue.LinkProfileReferenceNo = s.Socustomers != null ? s.Socustomers.LinkProfileReferenceNo : "";
                    SocustomersIssue.CompanyListingID = s.Socustomers != null && CompanyListingItems.Count > 0 ? CompanyListingItems.Where(w => w.ProfileReferenceNo == s.Socustomers.LinkProfileReferenceNo).Select(w => w.CompanyListingId).FirstOrDefault() : 0;
                    SocustomersIssue.CompanyListingName = s.Socustomers != null && CompanyListingItems.Count > 0 ? CompanyListingItems.Where(w => w.ProfileReferenceNo == s.Socustomers.LinkProfileReferenceNo).Select(w => w.CompanyName).FirstOrDefault() : "";
                    SocustomersIssue.CustomerCodeId = s.Socustomers != null && CompanyListingItems.Count > 0 ? CompanyListingItems.Where(w => w.ProfileReferenceNo == s.Socustomers.LinkProfileReferenceNo).Select(w => w.CustomerCodeId).FirstOrDefault() : 0;
                    SocustomersIssue.SocustomersIssueId = s.SocustomersIssueId;
                    SocustomersIssue.SocustomersId = s.SocustomersId;
                    SocustomersIssue.SobyCustomersTenderAgencyList = (s.SobyCustomersTenderAgency != null && masterDetailList != null ? (string.Join(",", s.SobyCustomersTenderAgency.Where(b => b.SocustomersIssueId == s.SocustomersIssueId).Select(b => masterDetailList.Where(m => m.ApplicationMasterDetailId == b.TenderAgencyId && m.Value.ToLower() == "alps gpo").Select(m => m.Value).FirstOrDefault()).ToList())).TrimStart(',') : "").TrimEnd(',');
                    SocustomersIssue.SobyCustomersMannerList = (s.SobyCustomersManner != null && masterDetailList != null ? (string.Join(",", s.SobyCustomersManner.Where(b => b.SocustomersIssueId == s.SocustomersIssueId).Select(b => masterDetailList.Where(m => m.ApplicationMasterDetailId == b.MannersId && m.Value.ToLower() == "by tender agency").Select(m => m.Value).FirstOrDefault()).ToList())).TrimStart(',') : "").TrimEnd(',');
                    var companyListingCompanyType = CompanyListingItems.Where(c => c.CompanyListingId == SocustomersIssue.CompanyListingID).FirstOrDefault();
                    SocustomersIssue.CompanyListingTypeList = (companyListingCompanyType != null && companyListingCompanyType.CompanyListingCompanyType != null && masterDetailList != null ? (string.Join(",", companyListingCompanyType.CompanyListingCompanyType.Select(b => masterDetailList.Where(m => m.ApplicationMasterDetailId == b.CompanyTypeId && m.Value.ToLower() == "customer").Select(m => m.Value).FirstOrDefault()).ToList())).TrimStart(',') : "").TrimEnd(',');
                    SocustomersIssues.Add(SocustomersIssue);
                });
            };
            var socustomersIssuess = SocustomersIssues.Where(w => w.SobyCustomersMannerList.ToLower() == "by tender agency" && w.CompanyListingTypeList.ToLower() == "customer").ToList();
            var companyListingItems = socustomersIssuess.GroupBy(s => new { s.CompanyListingID, s.CompanyListingName, s.CustomerCodeId, s.CompanyName, s.CustomerCode }).Select(s => new { s.Key.CompanyListingID, s.Key.CompanyListingName, s.Key.CustomerCodeId, s.Key.CompanyName, s.Key.CustomerCode }).ToList();
            companyListingItems.ForEach(h =>
            {
                CompanyListingModel CompanyListingModel = new CompanyListingModel();
                CompanyListingModel.CompanyListingID = h.CompanyListingID;
                CompanyListingModel.CompanyListingName = h.CompanyListingName;
                var customerCodes = CompanyListingCustomerCodeItems.Where(c => c.CompanyListingId == h.CompanyListingID).Select(s => s.CustomerCodeId).ToList();
                CompanyListingModel.CustomerCode = (string.Join(",", masterDetailList.Where(m => customerCodes.Contains(m.ApplicationMasterDetailId)).Select(m => m.Value).ToList()));
                CompanyListingModels.Add(CompanyListingModel);
            });
            if (CompanyListingModels != null && CompanyListingModels.Count > 0)
            {
                CompanyListingModels.ForEach(c =>
                {
                    c.CompanyName = c.CompanyListingName + " | " + c.CustomerCode;
                });
            }
            return CompanyListingModels;
        }
        [HttpGet]
        [Route("GetSoBycustomersForSalesNovate")]
        public List<CompanyListingModel> GetSoBycustomersForSalesNovate(int id)
        {
            List<CompanyListingModel> CompanyListingModels = new List<CompanyListingModel>();

            var purchaseItemIds = _context.PurchaseItemSalesEntryLine.Where(s => s.SalesOrderEntryId == id).Select(s => s.PurchaseItemSalesEntryLineId).ToList();
            if (purchaseItemIds != null && purchaseItemIds.Count > 0)
            {
                var socustomerIds = _context.ContractDistributionSalesEntryLine.Where(s => purchaseItemIds.Contains(s.PurchaseItemSalesEntryLineId.Value)).Select(c => c.SocustomerId).ToList();
                if (socustomerIds != null && socustomerIds.Count > 0)
                {
                    var CompanyListingItems = _context.CompanyListing.Where(s => socustomerIds.Contains(s.CompanyListingId)).ToList();

                    CompanyListingItems.ForEach(h =>
                    {
                        CompanyListingModel CompanyListingModel = new CompanyListingModel();
                        CompanyListingModel.CompanyListingID = h.CompanyListingId;
                        CompanyListingModel.CompanyListingName = h.CompanyName;
                        CompanyListingModels.Add(CompanyListingModel);
                    });
                }
            }
            return CompanyListingModels;
        }
        [HttpGet]
        [Route("GetNAVItemsByCompanies")]
        public List<NavItemModel> GetNAVItemsByCompanies()
        {
            List<NavItemModel> navItemModels = new List<NavItemModel>();
            var items = _context.Navitems.Select(s => s.VendorNo).Distinct().ToList();
            if (items != null && items.Count > 0)
            {
                items.ForEach(s =>
                {
                    NavItemModel navItemModel = new NavItemModel();
                    navItemModel.ReplenishmentMethod = s;
                    navItemModels.Add(navItemModel);
                });
            }
            return navItemModels.Where(w => w.ReplenishmentMethod != "" && w.ReplenishmentMethod != null).ToList();
        }
        [HttpGet]
        [Route("GetSocustomersItemCrossReferenceByCustomer")]
        public List<SocustomersItemCrossReferenceModel> GetSocustomersItemCrossReferenceByCustomer(long? id)
        {
            var CompanyListingItems = _context.CompanyListing.ToList();
            //var masterDetailList = _context.ApplicationMasterDetail.ToList();
            var SocustomersItemCrossReference = _context.SocustomersItemCrossReference.Include(s => s.SobyCustomers).Include(a => a.CustomerPackingUom).Include(a => a.PerUom).Include(a => a.PurchasePerUom).AsNoTracking().ToList();
            List<SocustomersItemCrossReferenceModel> SocustomersItemCrossReferenceModels = new List<SocustomersItemCrossReferenceModel>();
            SocustomersItemCrossReference.ForEach(s =>
            {
                SocustomersItemCrossReferenceModel SocustomersItemCrossReferenceModel = new SocustomersItemCrossReferenceModel();
                SocustomersItemCrossReferenceModel.CompanyListingID = s.SobyCustomers != null && CompanyListingItems != null ? CompanyListingItems.Where(w => w.ProfileReferenceNo == s.SobyCustomers.LinkProfileReferenceNo).Select(w => w.CompanyListingId).FirstOrDefault() : 0;
                SocustomersItemCrossReferenceModel.CompanyListingName = s.SobyCustomers != null && CompanyListingItems != null ? CompanyListingItems.Where(w => w.ProfileReferenceNo == s.SobyCustomers.LinkProfileReferenceNo).Select(w => w.CompanyName).FirstOrDefault() : "";
                SocustomersItemCrossReferenceModel.SocustomersItemCrossReferenceId = s.SocustomersItemCrossReferenceId;
                SocustomersItemCrossReferenceModel.Description = s.Description;
                SocustomersItemCrossReferenceModel.CustomerPackingUomName = s.CustomerPackingUom?.Value;
                SocustomersItemCrossReferenceModel.PerUomName = s.PerUom?.Value;

                SocustomersItemCrossReferenceModel.PurchasePerUomId = s.PurchasePerUomId;
                SocustomersItemCrossReferenceModel.PurchasePerUomName = s.PurchasePerUom?.Value;
                SocustomersItemCrossReferenceModel.CustomerReferenceNo = s.CustomerReferenceNo;
                SocustomersItemCrossReferenceModel.CustomerReferenceNo2 = s.CustomerReferenceNo2;
                SocustomersItemCrossReferenceModel.NavItemId = s.NavItemId;
                SocustomersItemCrossReferenceModel.No = s.CustomerReferenceNo + "|" + s.Description + "|" + SocustomersItemCrossReferenceModel.CustomerPackingUomName + "|" + SocustomersItemCrossReferenceModel.PerUomName;
                SocustomersItemCrossReferenceModels.Add(SocustomersItemCrossReferenceModel);
            });
            SocustomersItemCrossReferenceModels = SocustomersItemCrossReferenceModels.Where(o => o.CompanyListingID == id).OrderByDescending(a => a.SocustomersItemCrossReferenceId).ToList();
            return SocustomersItemCrossReferenceModels;
        }
    }
}