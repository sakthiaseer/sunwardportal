﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using APP.BussinessObject;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class SellingCatalogueController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly TableVersionRepository _repository;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly ApprovalService _approvalService;

        public SellingCatalogueController(CRT_TMSContext context, IMapper mapper, TableVersionRepository repository, GenerateDocumentNoSeries generateDocumentNoSeries, ApprovalService approvalService)
        {
            _context = context;
            _mapper = mapper;
            _repository = repository;
            _generateDocumentNoSeries = generateDocumentNoSeries;
            _approvalService = approvalService;
        }
        [HttpGet]
        [Route("GetSellingCatalogue")]
        public async Task<IEnumerable<TableDataVersionInfoModel<SellingCatalogueModel>>> GetExchangeVersion(string sessionID)
        {
            return await _repository.GetList<SellingCatalogueModel>(sessionID);
        }
        [HttpGet]
        [Route("GetSellingCatalogues")]
        public List<SellingCatalogueModel> Get()
        {
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();

            var sellingCatalogues = _context.SellingCatalogue
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(l => l.Company)
                .Include(c => c.Country)
                .Include(l => l.SellingPriceInformation)
                .AsNoTracking()
                .ToList();
            //if(sellingCatalogues!=null && sellingCatalogues.Count>0)
            //{
            //    var masterDetailIds = sellingCatalogues.Where(s => s.CountryId != null).Select(s => s.CountryId).ToList();
            //    if(masterDetailIds.Count>0)
            //    {
            //         masterDetailList = _context.ApplicationMasterDetail.Where(a=>masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().ToList();
            //    }

            //}
            List<SellingCatalogueModel> sellingCatalogueModels = new List<SellingCatalogueModel>();
            sellingCatalogues.ForEach(s =>
            {
                SellingCatalogueModel sellingCatalogueModel = new SellingCatalogueModel
                {
                    SellingCatalogueID = s.SellingCatalogueId,
                    CompanyID = s.CompanyId,
                    CountryID = s.CountryId,
                    FromValidity = s.FromValidity,
                    ToValidity = s.ToValidity,
                    NeedToKeepVersionInfo = s.NeedToKeepVersionInfo,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    Customer = s.Company?.CompanyName,
                    Country = s.Country?.Value,
                    VersionSessionId = s.VersionSessionId,
                    CustomerId = s.CustomerId,
                };
                sellingCatalogueModels.Add(sellingCatalogueModel);
            });
            return sellingCatalogueModels.OrderByDescending(a => a.SellingCatalogueID).ToList();
        }

        [HttpPost()]
        [Route("GetSellingCatalogueData")]
        public ActionResult<SellingCatalogueModel> GetQuotationHistoryData(SearchModel searchModel)
        {
            var SellingCatalogue = new SellingCatalogue();
            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
            List<long?> masterDetailIds = new List<long?>();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        SellingCatalogue = _context.SellingCatalogue.OrderByDescending(o => o.SellingCatalogueId).FirstOrDefault();
                        break;
                    case "Last":
                        SellingCatalogue = _context.SellingCatalogue.OrderByDescending(o => o.SellingCatalogueId).LastOrDefault();
                        break;
                    case "Next":
                        SellingCatalogue = _context.SellingCatalogue.OrderByDescending(o => o.SellingCatalogueId).LastOrDefault();
                        break;
                    case "Previous":
                        SellingCatalogue = _context.SellingCatalogue.OrderByDescending(o => o.SellingCatalogueId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        SellingCatalogue = _context.SellingCatalogue.OrderByDescending(o => o.SellingCatalogueId).FirstOrDefault();
                        break;
                    case "Last":
                        SellingCatalogue = _context.SellingCatalogue.OrderByDescending(o => o.SellingCatalogueId).LastOrDefault();
                        break;
                    case "Next":
                        SellingCatalogue = _context.SellingCatalogue.OrderBy(o => o.SellingCatalogueId).FirstOrDefault(s => s.SellingCatalogueId > searchModel.Id);
                        break;
                    case "Previous":
                        SellingCatalogue = _context.SellingCatalogue.OrderByDescending(o => o.SellingCatalogueId).FirstOrDefault(s => s.SellingCatalogueId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<SellingCatalogueModel>(SellingCatalogue);
            List<SellingPriceInformationModel> sellingPriceInformationModels = new List<SellingPriceInformationModel>();
            if (result != null)
            {
                if (result.SellingCatalogueID > 0)
                {
                    var sellingInformation = _context.SellingPriceInformation.Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode).Where(s => s.SellingCatalogueId == result.SellingCatalogueID).ToList();
                    if (sellingInformation != null && sellingInformation.Count > 0)
                    {
                        var bonuscurrencyIds = sellingInformation.Where(s => s.BonusCurrencyId != null).Select(s => s.BonusCurrencyId).ToList();
                        var mincurrencyIds = sellingInformation.Where(s => s.MinCurrencyId != null).Select(s => s.MinCurrencyId).ToList();
                        var quantityIds = sellingInformation.Where(s => s.Quantity != null).Select(s => s.Quantity).ToList();
                        var pricingMethodIds = sellingInformation.Where(s => s.PricingMethodId != null).Select(s => s.PricingMethodId).ToList();
                        if (bonuscurrencyIds.Count > 0)
                        {
                            masterDetailIds.AddRange(bonuscurrencyIds);
                        }
                        if (mincurrencyIds.Count > 0)
                        {
                            masterDetailIds.AddRange(mincurrencyIds);
                        }
                        if (quantityIds.Count > 0)
                        {
                            masterDetailIds.AddRange(quantityIds);
                        }
                        if (pricingMethodIds.Count > 0)
                        {
                            masterDetailIds.AddRange(pricingMethodIds);
                        }
                        if (masterDetailIds.Count > 0)
                        {
                            applicationmasterdetail = _context.ApplicationMasterDetail.Where(a => masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().Distinct().ToList();
                        }
                        sellingInformation.ForEach(s =>
                        {
                            SellingPriceInformationModel sellingPriceInformationModel = new SellingPriceInformationModel();

                            sellingPriceInformationModel.SellingPriceInformationID = s.SellingPriceInformationId;
                            sellingPriceInformationModel.SellingCatalogueID = s.SellingCatalogueId;
                            sellingPriceInformationModel.ManufacturingID = s.ManufacturingId;
                            sellingPriceInformationModel.ProductID = s.ProductId;
                            sellingPriceInformationModel.PricingMethodID = s.PricingMethodId;
                            sellingPriceInformationModel.BonusCurrencyID = s.BonusCurrencyId;
                            sellingPriceInformationModel.BonusSellingPrice = s.BonusSellingPrice;
                            sellingPriceInformationModel.Quantity = s.Quantity;
                            sellingPriceInformationModel.Bonus = s.Bonus;
                            sellingPriceInformationModel.MinCurrencyID = s.MinCurrencyId;
                            sellingPriceInformationModel.MinPrice = s.MinPrice;
                            sellingPriceInformationModel.MinQty = s.MinQty;
                            sellingPriceInformationModel.StatusCodeID = s.StatusCodeId;
                            sellingPriceInformationModel.AddedByUserID = s.AddedByUserId;
                            sellingPriceInformationModel.ModifiedByUserID = s.ModifiedByUserId;
                            sellingPriceInformationModel.AddedDate = s.AddedDate;
                            sellingPriceInformationModel.ModifiedDate = s.ModifiedDate;
                            if (s.BonusCurrencyId != null)
                            {
                                sellingPriceInformationModel.Currency = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.BonusCurrencyId).Select(a => a.Value).SingleOrDefault() : "";
                            }
                            if (s.MinCurrencyId != null)
                            {
                                sellingPriceInformationModel.MinCurrency = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.MinCurrencyId).Select(a => a.Value).SingleOrDefault() : "";
                            }
                            sellingPriceInformationModel.QuantityName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.Quantity).Select(a => a.Value).SingleOrDefault() : "";
                            sellingPriceInformationModel.PricingMethod = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PricingMethodId).Select(a => a.Value).SingleOrDefault() : "";
                            sellingPriceInformationModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                            sellingPriceInformationModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                            sellingPriceInformationModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";
                            sellingPriceInformationModels.Add(sellingPriceInformationModel);

                        });

                    }
                }
            }
            if (sellingPriceInformationModels != null && sellingPriceInformationModels.Count > 0)
            {
                result.sellingPriceInformationModels = sellingPriceInformationModels;
            }

            return result;
        }

        [HttpPost]
        [Route("InsertSellingCatalogue")]
        public SellingCatalogueModel Post(SellingCatalogueModel value)
        {
            var SessionId = Guid.NewGuid();

            var SellingCatalogue = new SellingCatalogue
            {
                CompanyId = value.CompanyID,
                CountryId = value.CountryID,
                FromValidity = value.FromValidity,
                ToValidity = value.ToValidity,
                NeedToKeepVersionInfo = value.NeedToKeepVersionInfo,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                VersionSessionId = SessionId,
                CustomerId = value.CustomerId,
            };
            _context.SellingCatalogue.Add(SellingCatalogue);
            _context.SaveChanges();
            value.VersionSessionId = SellingCatalogue.VersionSessionId;
            value.SellingCatalogueID = SellingCatalogue.SellingCatalogueId;
            if (value.CountryID > 0)
            {
                var masterDetailList = _context.ApplicationMasterDetail.Where(a => a.ApplicationMasterDetailId == value.CountryID).AsNoTracking().ToList();
                value.Country = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == value.CountryID).Select(a => a.Value).SingleOrDefault() : "";
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateSellingCatalogue")]
        public async Task<SellingCatalogueModel> Put(SellingCatalogueModel value)
        {
            value.VersionSessionId ??= Guid.NewGuid();
            var SellingCatalogue = _context.SellingCatalogue.SingleOrDefault(p => p.SellingCatalogueId == value.SellingCatalogueID);
            if (SellingCatalogue != null)
            {
                if (value.SaveVersionData)
                {
                    value.sellingPriceInformationModels = GetSellingPriceInformationsByID((int)value.SellingCatalogueID);
                    var versionInfo = new TableDataVersionInfoModel<SellingPriceInformationModel>
                    {
                        JsonData = JsonSerializer.Serialize(value),
                        AddedByUserId = value.ModifiedByUserID.Value,
                        AddedByUserID = value.ModifiedByUserID.Value,
                        StatusCodeID = value.StatusCodeID.Value,
                        AddedDate = DateTime.Now,
                        SessionId = value.VersionSessionId.Value,
                        ScreenId = value.ScreenID,
                        TableName = "SellingCatalogue",
                        PrimaryKey = value.SellingCatalogueID,
                    };
                    await _repository.Insert(versionInfo);
                }
                SellingCatalogue.CompanyId = value.CompanyID;
                SellingCatalogue.CountryId = value.CountryID;
                SellingCatalogue.FromValidity = value.FromValidity;
                SellingCatalogue.ToValidity = value.ToValidity;
                SellingCatalogue.NeedToKeepVersionInfo = value.NeedToKeepVersionInfo;
                SellingCatalogue.StatusCodeId = value.StatusCodeID.Value;
                SellingCatalogue.ModifiedByUserId = value.ModifiedByUserID;
                SellingCatalogue.ModifiedDate = DateTime.Now;
                SellingCatalogue.VersionSessionId = value.VersionSessionId;
                SellingCatalogue.CustomerId = value.CustomerId;
                _context.SaveChanges();
                if (value.CountryID > 0)
                {
                    var masterDetailList = _context.ApplicationMasterDetail.Where(a => a.ApplicationMasterDetailId == value.CountryID).AsNoTracking().ToList();
                    value.Country = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == value.CountryID).Select(a => a.Value).SingleOrDefault() : "";
                }
            }
            return value;
        }
        [HttpPut]
        [Route("UpdateSellingCatalogueVersion")]
        public SellingCatalogueModel UpdateSellingCatalogueVersion(SellingCatalogueModel value)
        {
            var sellingTires = _context.SellingPricingTiers.ToList();
            var SellingCatalogue = _context.SellingCatalogue.Include(i => i.SellingPriceInformation).SingleOrDefault(p => p.SellingCatalogueId == value.SellingCatalogueID);
            SellingCatalogue.CompanyId = value.CompanyID;
            SellingCatalogue.CountryId = value.CountryID;
            SellingCatalogue.FromValidity = value.FromValidity;
            SellingCatalogue.ToValidity = value.ToValidity;
            SellingCatalogue.NeedToKeepVersionInfo = value.NeedToKeepVersionInfo;
            SellingCatalogue.StatusCodeId = value.StatusCodeID.Value;
            SellingCatalogue.ModifiedByUserId = value.ModifiedByUserID;
            SellingCatalogue.ModifiedDate = DateTime.Now;
            SellingCatalogue.VersionSessionId = value.SessionId;
            SellingCatalogue.CustomerId = value.CustomerId;
            _context.SaveChanges();
            if (SellingCatalogue.SellingPriceInformation != null)
            {
                var SellingPriceInformationdetails = _context.SellingPriceInformation.Where(w => w.SellingCatalogueId == value.SellingCatalogueID).ToList();
                if (SellingPriceInformationdetails.Count > 0)
                {
                    SellingPriceInformationdetails.ForEach(h =>
                    {
                        var sellingTires = _context.SellingPricingTiers.Where(t => t.SellingPriceInfoId == h.SellingPriceInformationId).ToList();
                        if (sellingTires != null)
                        {
                            _context.SellingPricingTiers.RemoveRange(sellingTires);
                            _context.SaveChanges();
                        }
                    });
                }
                _context.SellingPriceInformation.RemoveRange(SellingCatalogue.SellingPriceInformation);
                _context.SaveChanges();
            }
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            value.sellingPriceInformationModels.ForEach(f =>
            {

                f.PricingMethod = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == f.PricingMethodID).Select(a => a.Value).SingleOrDefault() : "";
                if (f.PricingMethod.ToLower().Trim() == "bonus")
                {
                    f.MinCurrencyID = null;
                    f.MinPrice = null;
                    f.MinQty = null;
                }
                if (f.PricingMethod.ToLower().Trim() == "min qty")
                {
                    f.BonusSellingPrice = null;
                }
                var sellingPriceInformation = new SellingPriceInformation
                {
                    SellingCatalogueId = value.SellingCatalogueID,
                    ManufacturingId = f.ManufacturingID,
                    ProductId = f.ProductID,
                    PricingMethodId = f.PricingMethodID,
                    BonusCurrencyId = f.BonusCurrencyID,
                    BonusSellingPrice = f.BonusSellingPrice,
                    Quantity = f.Quantity,
                    Bonus = f.Bonus,
                    MinCurrencyId = f.MinCurrencyID,
                    MinPrice = f.MinPrice,
                    MinQty = f.MinQty,
                    StatusCodeId = f.StatusCodeID.Value,
                    AddedByUserId = f.AddedByUserID,
                    AddedDate = DateTime.Now,
                };
                _context.SellingPriceInformation.Add(sellingPriceInformation);
                _context.SaveChanges();
                var SellingPriceInfoID = sellingPriceInformation.SellingPriceInformationId;
                if (f.sellingPricingTiersList.Count > 0)
                {
                    f.sellingPricingTiersList.ForEach(h =>
                    {
                        var sellingPriceTiers = new SellingPricingTiers
                        {
                            Quantity = h.Quantity,
                            Bonus = h.Bonus,
                            SellingPriceInfoId = SellingPriceInfoID,
                            SellingCurrencyId = h.SellingCurrencyId,
                            PricingMethodId = h.PricingMethodId,
                            MinQty = h.MinQty,
                            SellingPrice = h.SellingPrice
                        };
                        _context.SellingPricingTiers.Add(sellingPriceTiers);
                        _context.SaveChanges();
                    });
                }
            });
            return value;
        }
        private List<SellingPriceInformationModel> GetSellingPriceInformationsByID(int id)
        {
            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();
            List<long?> masterDetailIds = new List<long?>();

            var sellingPriceInformations = _context.SellingPriceInformation
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(s => s.Product)
                .Include(s => s.SellingPricingTiers)
                .Where(s => s.SellingCatalogueId == id)
               .AsNoTracking()
                .ToList();
            SellingPriceInformationController sellingPriceInformationController = new SellingPriceInformationController(_context, _mapper);
            List<SellingPriceInformationModel> SellingPriceInformationModels = new List<SellingPriceInformationModel>();
            if (sellingPriceInformations != null && sellingPriceInformations.Count > 0)
            {
                var bonuscurrencyIds = sellingPriceInformations.Where(s => s.BonusCurrencyId != null).Select(s => s.BonusCurrencyId).ToList();
                var mincurrencyIds = sellingPriceInformations.Where(s => s.MinCurrencyId != null).Select(s => s.MinCurrencyId).ToList();
                var quantityIds = sellingPriceInformations.Where(s => s.Quantity != null).Select(s => s.Quantity).ToList();
                var pricingMethodIds = sellingPriceInformations.Where(s => s.PricingMethodId != null).Select(s => s.PricingMethodId).ToList();
                if (bonuscurrencyIds.Count > 0)
                {
                    masterDetailIds.AddRange(bonuscurrencyIds);
                }
                if (mincurrencyIds.Count > 0)
                {
                    masterDetailIds.AddRange(mincurrencyIds);
                }
                if (quantityIds.Count > 0)
                {
                    masterDetailIds.AddRange(quantityIds);
                }
                if (pricingMethodIds.Count > 0)
                {
                    masterDetailIds.AddRange(pricingMethodIds);
                }
                if (masterDetailIds.Count > 0)
                {
                    applicationmasterdetail = _context.ApplicationMasterDetail.Where(a => masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().Distinct().ToList();
                }
                sellingPriceInformations.ForEach(s =>
                {
                    SellingPriceInformationModel sellingPriceInformationModel = new SellingPriceInformationModel();

                    sellingPriceInformationModel.SellingPriceInformationID = s.SellingPriceInformationId;
                    sellingPriceInformationModel.SellingCatalogueID = s.SellingCatalogueId;
                    sellingPriceInformationModel.ManufacturingID = s.ManufacturingId;
                    sellingPriceInformationModel.ProductID = s.ProductId;
                    sellingPriceInformationModel.PricingMethodID = s.PricingMethodId;
                    sellingPriceInformationModel.BonusCurrencyID = s.BonusCurrencyId;
                    sellingPriceInformationModel.BonusSellingPrice = s.BonusSellingPrice;
                    sellingPriceInformationModel.Quantity = s.Quantity;
                    sellingPriceInformationModel.Bonus = s.Bonus;
                    sellingPriceInformationModel.MinCurrencyID = s.MinCurrencyId;
                    sellingPriceInformationModel.MinPrice = s.MinPrice;
                    sellingPriceInformationModel.MinQty = s.MinQty;
                    sellingPriceInformationModel.StatusCodeID = s.StatusCodeId;
                    sellingPriceInformationModel.AddedByUserID = s.AddedByUserId;
                    sellingPriceInformationModel.ModifiedByUserID = s.ModifiedByUserId;
                    sellingPriceInformationModel.AddedDate = s.AddedDate;
                    sellingPriceInformationModel.ModifiedDate = s.ModifiedDate;
                    sellingPriceInformationModel.ProductName = s.Product?.Code;
                    if (s.BonusCurrencyId != null)
                    {
                        sellingPriceInformationModel.Currency = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.BonusCurrencyId).Select(a => a.Value).SingleOrDefault() : "";
                        sellingPriceInformationModel.isBonus = true;
                    }
                    if (s.MinCurrencyId != null)
                    {
                        sellingPriceInformationModel.isMinQty = true;
                        sellingPriceInformationModel.Currency = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.MinCurrencyId).Select(a => a.Value).SingleOrDefault() : "";
                    }
                    sellingPriceInformationModel.QuantityName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.Quantity).Select(a => a.Value).SingleOrDefault() : "";
                    sellingPriceInformationModel.PricingMethod = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PricingMethodId).Select(a => a.Value).SingleOrDefault() : "";
                    sellingPriceInformationModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "";
                    sellingPriceInformationModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "";
                    sellingPriceInformationModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "";

                    if (s.SellingPricingTiers != null)
                    {
                        if (sellingPriceInformationModel.PricingMethod.ToLower().Contains("min"))
                        {
                            sellingPriceInformationModel.MinQty = s.SellingPricingTiers.Where(m => m.MinQty != null).Min(s => s.MinQty);
                        }
                        if (sellingPriceInformationModel.PricingMethod.ToLower().Contains("bonus"))
                        {
                            sellingPriceInformationModel.MinQty = s.SellingPricingTiers.Where(m => m.Quantity != null).Min(s => s.Quantity);
                            sellingPriceInformationModel.Bonus = s.SellingPricingTiers.Where(m => m.Bonus != null).Min(s => s.Bonus);
                        }
                    }

                    sellingPriceInformationModel.sellingPricingTiersList = sellingPriceInformationController.getSellingPriceTiersByID((int)s.SellingPriceInformationId);

                    SellingPriceInformationModels.Add(sellingPriceInformationModel);
                });
            }
            return SellingPriceInformationModels.OrderByDescending(a => a.SellingPriceInformationID).ToList();
        }
        [HttpDelete]
        [Route("DeleteSellingCatalogue")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var SellingCatalogue = _context.SellingCatalogue.Include(i => i.SellingPriceInformation).Where(p => p.SellingCatalogueId == id).FirstOrDefault();
                if (SellingCatalogue != null)
                {
                    if (SellingCatalogue.SellingPriceInformation != null)
                    {
                        var SellingPriceInformationdetails = _context.SellingPriceInformation.Where(w => w.SellingCatalogueId == id).ToList();
                        if (SellingPriceInformationdetails.Count > 0)
                        {
                            SellingPriceInformationdetails.ForEach(h =>
                            {
                                var sellingTires = _context.SellingPricingTiers.Where(t => t.SellingPriceInfoId == h.SellingPriceInformationId).ToList();
                                if (sellingTires != null)
                                {
                                    _context.SellingPricingTiers.RemoveRange(sellingTires);
                                    _context.SaveChanges();
                                }
                            });
                        }
                        _context.SellingPriceInformation.RemoveRange(SellingCatalogue.SellingPriceInformation);
                        _context.SaveChanges();
                    }
                    _context.SellingCatalogue.Remove(SellingCatalogue);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot Delete Refer to Others", ex);
            }
        }
        [HttpPost]
        [Route("GetSellingCatalogueReprt")]
        public List<SellingCatalogueReportModel> GetSellingCatalogueReprt(SellingCatalogueSearchModel searchModel)
        {
            List<SellingCatalogueReportModel> sellingCatalogueReportModels = new List<SellingCatalogueReportModel>();
            var sellingPriceInformation = _context.SellingPricingTiers.Include(p => p.SellingPriceInfo).Include(s => s.SellingPriceInfo.SellingCatalogue).Include(g => g.SellingPriceInfo.GenericCodeSupplyToMultiple).Include(b => b.SellingPriceInfo.SellingCatalogue.Company).Include(a => a.SellingPriceInfo.Product).Include(n => n.SellingPriceInfo.Product.Navitems).Where(s => s.SellingPricingTiersId > 0);
            if (searchModel.CountryId != null)
            {
                sellingPriceInformation = sellingPriceInformation.Where(s => s.SellingPriceInfo.SellingCatalogue.CountryId == searchModel.CountryId);
            }
            if (searchModel.CustomerId != null)
            {
                sellingPriceInformation = sellingPriceInformation.Where(s => s.SellingPriceInfo.SellingCatalogue.CompanyId == searchModel.CustomerId);
            }
            if (searchModel.FromValidity != null && searchModel.ToValidity != null)
            {
                sellingPriceInformation = sellingPriceInformation.Where(s => s.SellingPriceInfo.SellingCatalogue.FromValidity >= searchModel.FromValidity && s.SellingPriceInfo.SellingCatalogue.ToValidity <= searchModel.ToValidity);
            }
            else
            {
                if (searchModel.FromValidity != null)
                {
                    sellingPriceInformation = sellingPriceInformation.Where(s => s.SellingPriceInfo.SellingCatalogue.FromValidity >= searchModel.FromValidity);
                }
                if (searchModel.ToValidity != null)
                {
                    sellingPriceInformation = sellingPriceInformation.Where(s => s.SellingPriceInfo.SellingCatalogue.ToValidity >= searchModel.ToValidity);
                }
            }
            if (searchModel.ProductId != null)
            {
                sellingPriceInformation = sellingPriceInformation.Where(s => s.SellingPriceInfo.ProductId == searchModel.ProductId);
            }
            var sellingPriceInformations = sellingPriceInformation.OrderByDescending(o => o.SellingPricingTiersId).AsNoTracking().ToList();
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            if (sellingPriceInformations != null && sellingPriceInformations.Count > 0)
            {
                GenericCodesController genericCodes = new GenericCodesController(_context, _mapper, _generateDocumentNoSeries);
                CompanyListingController getompanyListings = new CompanyListingController(_context, _mapper, _generateDocumentNoSeries, _approvalService);
                sellingPriceInformations.ForEach(s =>
                {
                    SellingCatalogueReportModel sellingCatalogueReportModel = new SellingCatalogueReportModel();
                    sellingCatalogueReportModel.SellingPricingTiersId = s.SellingPricingTiersId;
                    sellingCatalogueReportModel.ProductName = s.SellingPriceInfo != null && s.SellingPriceInfo.Product != null ? s.SellingPriceInfo.Product.Code : null;
                    sellingCatalogueReportModel.UomName = s.SellingPriceInfo != null && s.SellingPriceInfo.Product != null && applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.SellingPriceInfo.Product.Uom).Select(a => a.Value).SingleOrDefault() : "";
                    sellingCatalogueReportModel.PricingMethodName = applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.PricingMethodId).Select(a => a.Value).SingleOrDefault() : "";
                    sellingCatalogueReportModel.CurrencyName = s.SellingPriceInfo != null && applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.SellingPriceInfo?.BonusCurrencyId).Select(a => a.Value).SingleOrDefault() : "";
                    sellingCatalogueReportModel.CountryName = s.SellingPriceInfo != null && s.SellingPriceInfo.SellingCatalogue != null && applicationmasterdetail != null ? applicationmasterdetail.Where(a => a.ApplicationMasterDetailId == s.SellingPriceInfo.SellingCatalogue.CountryId).Select(a => a.Value).SingleOrDefault() : "";
                    if (sellingCatalogueReportModel.PricingMethodName.ToLower().Trim() == "bonus")
                    {
                        sellingCatalogueReportModel.Bonus = s.Bonus;
                        sellingCatalogueReportModel.Qty = s.Quantity;
                    }
                    if (sellingCatalogueReportModel.PricingMethodName.ToLower().Trim() == "min qty")
                    {
                        sellingCatalogueReportModel.Qty = s.MinQty;
                    }
                    sellingCatalogueReportModel.SellingPrice = s.SellingPrice;
                    sellingCatalogueReportModel.NavisionList = s.SellingPriceInfo != null && s.SellingPriceInfo.Product != null && s.SellingPriceInfo.Product.Navitems.Count > 0 ? (s.SellingPriceInfo.Product.Navitems.Select(a => a.No).Aggregate((i, j) => i + "," + j)) : "";
                    sellingCatalogueReportModel.CustomerName = s.SellingPriceInfo?.SellingCatalogue?.Company?.CompanyName;
                    if (s.SellingPriceInfo != null && s.SellingPriceInfo.GenericCodeSupplyToMultiple != null)
                    {
                        var navList = genericCodes.GetGenericCodeSupplyToMultipleById((int)s.SellingPriceInfo?.GenericCodeSupplyToMultiple?.GenericCodeId).Where(w => w.GenericCodeSupplyToMultipleId == s.SellingPriceInfo?.GenericCodeSupplyToMultipleId).ToList();
                        var navItems = "";
                        var customerCompanyId = getompanyListings.GetCustomerCompanyId((int)s.SellingPriceInfo.SellingCatalogue.CompanyId);
                        customerCompanyId.ForEach(x =>
                        {
                            var lists = navList.SelectMany(n => n.NavItemsList).ToList();
                            navItems += lists.Where(cc => cc.CompanyId == x).Select(z => z.No).FirstOrDefault() + ",";
                        });
                        sellingCatalogueReportModel.NavisionList = navItems.TrimEnd(',').TrimStart(',');
                    }
                    sellingCatalogueReportModels.Add(sellingCatalogueReportModel);
                });
            }
            return sellingCatalogueReportModels;
        }


        [HttpPut]
        [Route("CreateVersion")]
        public async Task<SellingCatalogueModel> CreateVersion(SellingCatalogueModel value)
        {
            try
            {
                value.SessionId = value.VersionSessionId.Value;
                var versionData = _context.SellingCatalogue.Include("SellingPriceInformation.SellingPricingTiers").SingleOrDefault(p => p.SellingCatalogueId == value.SellingCatalogueID);

                if (versionData != null)
                {
                    var verObject = _mapper.Map<SellingCatalogueModel>(versionData);
                    verObject.sellingPriceInformationModels = new List<SellingPriceInformationModel>();
                    versionData.SellingPriceInformation.ToList().ForEach(f =>
                    {
                        var priceInfo = _mapper.Map<SellingPriceInformationModel>(f);
                        priceInfo.sellingPricingTiersList = new List<SellingPricingTiersModel>();
                        f.SellingPricingTiers.ToList().ForEach(t =>
                        {
                            var tiers = _mapper.Map<SellingPricingTiersModel>(t);
                            priceInfo.sellingPricingTiersList.Add(tiers);
                        });
                        verObject.sellingPriceInformationModels.Add(priceInfo);
                    });


                    var versionInfo = new TableDataVersionInfoModel<SellingCatalogueModel>
                    {
                        JsonData = JsonSerializer.Serialize(verObject),
                        AddedByUserId = value.ModifiedByUserID.Value,
                        AddedByUserID = value.ModifiedByUserID.Value,
                        AddedDate = DateTime.Now,
                        SessionId = value.SessionId.Value,
                        ScreenId = value.ScreenID,
                        TableName = "SellingCatalogue",
                        PrimaryKey = value.SellingCatalogueID,
                        ReferenceInfo = value.ReferenceInfo,
                        StatusCodeID = value.StatusCodeID.Value,
                    };
                    await _repository.Insert(versionInfo);
                }
                return value;
            }
            catch (Exception ex)
            {
                throw new Exception("create Version failed.");
            }
        }
        [HttpPut]
        [Route("CheckExitDataVersion")]
        public bool CheckExitDataVersion(SellingCatalogueModel value)
        {
            var sellingPriceInformationids = _context.SellingPriceInformation.Include(s => s.SellingPricingTiers).Where(p => p.SellingCatalogueId == value.SellingCatalogueID).Select(s => s.SellingPriceInformationId).ToList();
            if (sellingPriceInformationids != null)
            {
                var sellingPriceTiers = _context.SellingPricingTiers.Where(s => sellingPriceInformationids.Contains(s.SellingPriceInfoId.Value)).ToList();
                if (sellingPriceTiers != null && sellingPriceTiers.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }

        [HttpPut]
        [Route("ApplyVersion")]
        public async Task<SellingCatalogueModel> ApplyVersion(SellingCatalogueModel value)
        {
            try
            {
                var versionData = _context.SellingCatalogue.Include("SellingPriceInformation.SellingPricingTiers").SingleOrDefault(p => p.SellingCatalogueId == value.SellingCatalogueID);

                if (versionData != null)
                {
                    versionData.CompanyId = value.CompanyID;
                    versionData.CountryId = value.CountryID;
                    versionData.FromValidity = value.FromValidity;
                    versionData.ToValidity = value.ToValidity;
                    versionData.NeedToKeepVersionInfo = value.NeedToKeepVersionInfo;
                    versionData.StatusCodeId = value.StatusCodeID.Value;
                    versionData.ModifiedByUserId = value.ModifiedByUserID;
                    versionData.ModifiedDate = DateTime.Now;
                    versionData.VersionSessionId = value.VersionSessionId;

                    _context.SellingPriceInformation.RemoveRange(versionData.SellingPriceInformation);
                    _context.SaveChanges();
                    if (value.sellingPriceInformationModels != null)
                    {
                        value.sellingPriceInformationModels.ForEach(f =>
                        {
                            var sellingPrice = new SellingPriceInformation
                            {
                                SellingCatalogueId = versionData.SellingCatalogueId,
                                ManufacturingId = f.ManufacturingID,
                                ProductId = f.ProductID,
                                PricingMethodId = f.PricingMethodID,
                                BonusCurrencyId = f.BonusCurrencyID,
                                BonusSellingPrice = f.BonusSellingPrice,
                                MinCurrencyId = f.MinCurrencyID,
                                MinPrice = f.MinPrice,
                                MinQty = f.MinQty,
                                Quantity = f.Quantity,
                                Bonus = f.Bonus,
                                StatusCodeId = f.StatusCodeID.Value,
                                AddedByUserId = value.ModifiedByUserID,
                                AddedDate = DateTime.Now,
                                ModifiedByUserId = value.ModifiedByUserID,
                                ModifiedDate = DateTime.Now,
                                GenericCodeSupplyToMultipleId = f.GenericCodeSupplyToMultipleId
                            };
                            if (f.sellingPricingTiersList != null)
                            {
                                f.sellingPricingTiersList.ToList().ForEach(t =>
                                {
                                    var tiers = _mapper.Map<SellingPricingTiers>(t);
                                    tiers.SellingPricingTiersId = 0;
                                    tiers.SellingPriceInfoId = sellingPrice.SellingPriceInformationId;
                                    sellingPrice.SellingPricingTiers.Add(tiers);
                                });
                            }
                            _context.SellingPriceInformation.Add(sellingPrice);
                        });
                    }
                    _context.SaveChanges();
                }
                return value;
            }
            catch (Exception ex)
            {
                throw new Exception("Version update changes failed.");
            }
        }
        [HttpPut]
        [Route("TempVersion")]
        public async Task<long> TempVersion(SellingCatalogueModel value)
        {
            value.SessionId = value.VersionSessionId.Value;
            var versionData = _context.SellingCatalogue.Include("SellingPriceInformation.SellingPricingTiers").SingleOrDefault(p => p.SellingCatalogueId == value.SellingCatalogueID);
            if (versionData != null)
            {
                var verObject = _mapper.Map<SellingCatalogueModel>(versionData);
                verObject.sellingPriceInformationModels = new List<SellingPriceInformationModel>();
                versionData.SellingPriceInformation.ToList().ForEach(f =>
                {
                    var priceInfo = _mapper.Map<SellingPriceInformationModel>(f);
                    priceInfo.sellingPricingTiersList = new List<SellingPricingTiersModel>();
                    f.SellingPricingTiers.ToList().ForEach(t =>
                    {
                        var tiers = _mapper.Map<SellingPricingTiersModel>(t);
                        priceInfo.sellingPricingTiersList.Add(tiers);
                    });
                    verObject.sellingPriceInformationModels.Add(priceInfo);
                });
                var versionInfo = new TableDataVersionInfoModel<SellingCatalogueModel>
                {
                    JsonData = JsonSerializer.Serialize(verObject),
                    AddedByUserId = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedByUserID = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedDate = DateTime.Now,
                    SessionId = value.VersionSessionId.Value,
                    ScreenId = value.ScreenID,
                    TableName = "SellingCatalogue",
                    PrimaryKey = value.SellingCatalogueID,
                    ReferenceInfo = "Temp Version - undo",
                    StatusCodeID = value.StatusCodeID.Value,
                };
                var result = await _repository.Insert(versionInfo);
                return result.VersionTableId;
            }
            return -1;
        }

        [HttpGet]
        [Route("UndoVersion")]
        public async Task<SellingCatalogueModel> UndoVersion(int Id)
        {
            try
            {
                var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

                if (tableData != null)
                {
                    var verObject = JsonSerializer.Deserialize<SellingCatalogueModel>(tableData.JsonData);

                    var versionData = _context.SellingCatalogue.Include(m => m.SellingPriceInformation).Include("SellingPriceInformation.SellingPricingTiers").SingleOrDefault(p => p.SellingCatalogueId == verObject.SellingCatalogueID);


                    if (verObject != null && versionData != null)
                    {
                        versionData.CompanyId = verObject.CompanyID;
                        versionData.CountryId = verObject.CountryID;
                        versionData.FromValidity = verObject.FromValidity;
                        versionData.ToValidity = verObject.ToValidity;
                        versionData.NeedToKeepVersionInfo = verObject.NeedToKeepVersionInfo;
                        versionData.StatusCodeId = verObject.StatusCodeID.Value;
                        versionData.ModifiedByUserId = verObject.ModifiedByUserID;
                        versionData.ModifiedDate = DateTime.Now;
                        versionData.VersionSessionId = verObject.VersionSessionId;

                        _context.SellingPriceInformation.RemoveRange(versionData.SellingPriceInformation);
                        _context.SaveChanges();
                        if (verObject.sellingPriceInformationModels != null)
                        {
                            verObject.sellingPriceInformationModels.ForEach(f =>
                            {
                                var sellingPrice = new SellingPriceInformation
                                {
                                    SellingCatalogueId = versionData.SellingCatalogueId,
                                    ManufacturingId = f.ManufacturingID,
                                    ProductId = f.ProductID,
                                    PricingMethodId = f.PricingMethodID,
                                    BonusCurrencyId = f.BonusCurrencyID,
                                    BonusSellingPrice = f.BonusSellingPrice,
                                    MinCurrencyId = f.MinCurrencyID,
                                    MinPrice = f.MinPrice,
                                    MinQty = f.MinQty,
                                    Quantity = f.Quantity,
                                    Bonus = f.Bonus,
                                    StatusCodeId = f.StatusCodeID.Value,
                                    AddedByUserId = f.ModifiedByUserID,
                                    AddedDate = DateTime.Now,
                                    ModifiedByUserId = f.ModifiedByUserID,
                                    ModifiedDate = DateTime.Now,
                                    GenericCodeSupplyToMultipleId = f.GenericCodeSupplyToMultipleId
                                };
                                f.sellingPricingTiersList.ToList().ForEach(t =>
                                {
                                    var tiers = _mapper.Map<SellingPricingTiers>(t);
                                    tiers.SellingPricingTiersId = 0;
                                    tiers.SellingPriceInfoId = sellingPrice.SellingPriceInformationId;
                                    sellingPrice.SellingPricingTiers.Add(tiers);
                                });

                                _context.SellingPriceInformation.Add(sellingPrice);

                            });
                        }
                        _context.SaveChanges();
                    }
                    return verObject;
                }
                return new SellingCatalogueModel();
            }
            catch (Exception ex)
            {
                var errorMessage = "Undo Version Update Failed";
                throw new AppException(errorMessage, ex);
            }
        }
        [HttpDelete]
        [Route("DeleteVersion")]
        public async Task<long> DeleteVersion(int Id)
        {

            var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

            if (tableData != null)
            {
                var TempVersion = _context.TempVersion.Where(w => w.VersionId == tableData.VersionTableId).ToList();
                if (TempVersion != null)
                {
                    _context.TempVersion.RemoveRange(TempVersion);
                    _context.SaveChanges();
                }
                _context.TableDataVersionInfo.Remove(tableData);
                _context.SaveChanges();

            }
            return -1;
        }
    }
}