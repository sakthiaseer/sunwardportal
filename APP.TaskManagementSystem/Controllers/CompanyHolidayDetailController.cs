﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CompanyHolidayDetailController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public CompanyHolidayDetailController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetCompanyHolidayDetails")]
        public List<CompanyHolidayDetailModel> Get(int id)
        {
            var companyHolidayDetail = _context.CompanyHolidayDetail.
               Include(a => a.AllowEarlyRelease)
               .Include(m => m.Holiday)
               .Include(s => s.HolidayType).AsNoTracking().ToList();
            List<CompanyHolidayDetailModel> companyHolidayDetailModel = new List<CompanyHolidayDetailModel>();
            companyHolidayDetail.ForEach(s =>
            {
                CompanyHolidayDetailModel companyHolidayDetailModels = new CompanyHolidayDetailModel
                {
                    CompanyHolidayDetailID = s.CompanyHolidayDetailId,
                    CompanyHolidayID = s.CompanyHolidayId,
                    HolidayID = s.HolidayId,
                    HolidayTypeID = s.HolidayTypeId,
                    AllowEarlyReleaseID = s.AllowEarlyReleaseId,
                    EarlyRelease = s.EarlyRelease,
                    IsCompulsory = s.IsCompulsory,
                    IsCompanyChoose = s.IsCompanyChoose,
                    HolidayDate = s.HolidayDate,
                    HolidayName = s.Holiday != null ? s.Holiday.Name : "",
                    AllowEarlyRelease = s.AllowEarlyRelease != null ? s.AllowEarlyRelease.CodeValue : "",
                    HolidayType = s.HolidayType != null ? s.HolidayType.CodeValue : "",
                };
                companyHolidayDetailModel.Add(companyHolidayDetailModels);
            });
            return companyHolidayDetailModel.Where(i => i.CompanyHolidayID == id).OrderByDescending(a => a.CompanyHolidayDetailID).ToList();
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get CompanyHolidayDetails")]
        [HttpGet("GetCompanyHolidayDetails/{id:int}")]
        public ActionResult<CompanyHolidayDetailModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var companyHolidayDetail = _context.CompanyHolidayDetail.SingleOrDefault(p => p.CompanyHolidayDetailId == id.Value);
            var result = _mapper.Map<CompanyHolidayDetailModel>(companyHolidayDetail);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<CompanyHolidayDetailModel> GetData(SearchModel searchModel)
        {
            var companyHolidayDetail = new CompanyHolidayDetail();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        companyHolidayDetail = _context.CompanyHolidayDetail.OrderByDescending(o => o.CompanyHolidayDetailId).FirstOrDefault();
                        break;
                    case "Last":
                        companyHolidayDetail = _context.CompanyHolidayDetail.OrderByDescending(o => o.CompanyHolidayDetailId).LastOrDefault();
                        break;
                    case "Next":
                        companyHolidayDetail = _context.CompanyHolidayDetail.OrderByDescending(o => o.CompanyHolidayDetailId).LastOrDefault();
                        break;
                    case "Previous":
                        companyHolidayDetail = _context.CompanyHolidayDetail.OrderByDescending(o => o.CompanyHolidayDetailId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        companyHolidayDetail = _context.CompanyHolidayDetail.OrderByDescending(o => o.CompanyHolidayDetailId).FirstOrDefault();
                        break;
                    case "Last":
                        companyHolidayDetail = _context.CompanyHolidayDetail.OrderByDescending(o => o.CompanyHolidayDetailId).LastOrDefault();
                        break;
                    case "Next":
                        companyHolidayDetail = _context.CompanyHolidayDetail.OrderBy(o => o.CompanyHolidayDetailId).FirstOrDefault(s => s.CompanyHolidayDetailId > searchModel.Id);
                        break;
                    case "Previous":
                        companyHolidayDetail = _context.CompanyHolidayDetail.OrderByDescending(o => o.CompanyHolidayDetailId).FirstOrDefault(s => s.CompanyHolidayDetailId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<CompanyHolidayDetailModel>(companyHolidayDetail);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertCompanyHolidayDetails")]
        public CompanyHolidayDetailModel Post(CompanyHolidayDetailModel value)
        {
            var CompanyHolidayDetails = new CompanyHolidayDetail
            {
                //CompanyHolidayDetailId = value.CompanyHolidayDetailID,
                CompanyHolidayId = value.CompanyHolidayID,
                HolidayId = value.HolidayID,
                HolidayTypeId = value.HolidayTypeID,
                AllowEarlyReleaseId = value.AllowEarlyReleaseID,
                EarlyRelease = value.EarlyRelease,
                IsCompulsory = value.IsCompulsory,
                IsCompanyChoose = value.IsCompanyChoose
            };
            _context.CompanyHolidayDetail.Add(CompanyHolidayDetails);
            _context.SaveChanges();
            value.CompanyHolidayDetailID = CompanyHolidayDetails.CompanyHolidayDetailId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateCompanyHolidayDetails")]
        public CompanyHolidayDetailModel Put(CompanyHolidayDetailModel value)
        {
            var companyHolidayDetail = _context.CompanyHolidayDetail.SingleOrDefault(p => p.CompanyHolidayDetailId == value.CompanyHolidayDetailID);
            companyHolidayDetail.CompanyHolidayId = value.CompanyHolidayID;
            companyHolidayDetail.HolidayId = value.HolidayID;
            companyHolidayDetail.HolidayTypeId = value.HolidayTypeID;
            companyHolidayDetail.AllowEarlyReleaseId = value.AllowEarlyReleaseID;
            companyHolidayDetail.EarlyRelease = value.EarlyRelease;
            companyHolidayDetail.IsCompulsory = value.IsCompulsory;
            companyHolidayDetail.IsCompanyChoose = value.IsCompanyChoose;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteCompanyHolidayDetails")]
        public void Delete(int id)
        {
            var companyHolidayDetail = _context.CompanyHolidayDetail.SingleOrDefault(p => p.CompanyHolidayDetailId == id);
            if (companyHolidayDetail != null)
            {
                _context.CompanyHolidayDetail.Remove(companyHolidayDetail);
                _context.SaveChanges();
            }
        }
    }
}