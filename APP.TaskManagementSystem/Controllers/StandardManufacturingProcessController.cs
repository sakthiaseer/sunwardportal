﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class StandardManufacturingProcessController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public StandardManufacturingProcessController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetStandardManufacturingProcess")]
        public List<StandardManufacturingProcessModel> Get()
        {
            List<StandardManufacturingProcessModel> standardManufacturingProcessModels = new List<StandardManufacturingProcessModel>();
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
           
            var standardManufacturingProcess = _context.StandardManufacturingProcess
                .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode")
                .Include("Profile")
                .Include("MethodTempalte")
                .OrderByDescending(o => o.StandardManufacturingProcessId).AsNoTracking().ToList();
            if(standardManufacturingProcess!=null && standardManufacturingProcess.Count>0)
            {
                var masterDetailsIds = standardManufacturingProcess.Where(s => s.ManufacturingSiteId != null).Select(s => s.ManufacturingSiteId).ToList();
                if (masterDetailsIds.Count > 0)
                {
                    masterDetailList = _context.ApplicationMasterDetail.Where(a=>masterDetailsIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().ToList();
                }
            }
            standardManufacturingProcess.ForEach(s =>
            {
                StandardManufacturingProcessModel standardManufacturingProcessModel = new StandardManufacturingProcessModel
                {
                    StandardManufacturingProcessId = s.StandardManufacturingProcessId,
                    ManufacturingSiteId = s.ManufacturingSiteId,
                    ManufacturingSiteName = s.ManufacturingSiteId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingSiteId).Select(m => m.Value).FirstOrDefault() : "",
                    ProfileId = s.ProfileId,
                    ProfileName = s.Profile.Name,
                    MethodTempalteName = s.MethodTempalte.MethodTemplateNo,
                    MethodTempalteId = s.MethodTempalteId,
                    MethodTemplateName = s.MethodTemplateName,
                    TemplateName = s.TemplateName,
                    MachineGroupName = s.MachineGroupName,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser.UserName,
                    ModifiedByUser = s.ModifiedByUser.UserName,
                    StatusCode = s.StatusCode.CodeValue,
                };
                standardManufacturingProcessModels.Add(standardManufacturingProcessModel);
            });

            return standardManufacturingProcessModels;
        }
        [HttpGet]
        [Route("GetStandardManufacturingProcessLine")]
        public List<StandardManufacturingProcessLineModel> Get(int? id)
        {
            List<ApplicationMasterDetail> masterDetailList = new List<ApplicationMasterDetail>();
            List<long?> masterDetailIds = new List<long?>();
            List<StandardManufacturingProcessLineModel> standardManufacturingProcessLineModels = new List<StandardManufacturingProcessLineModel>();
          
            var icmasterOperation = _context.IcmasterOperation.AsNoTracking().ToList();
            var standardManufacturingProcessLine = _context.StandardManufacturingProcessLine
                                                    .Include("MachineGrouping")
                                                    .Include("MachineName")
                                                    .Where(l => l.StandardManufacturingProcessId == id)
                                                    .OrderByDescending(o => o.StandardManufacturingProcessLineId).AsNoTracking().ToList();
            if(standardManufacturingProcessLine!=null && standardManufacturingProcessLine.Count>0)
            {
                var MachineGroupingIds = standardManufacturingProcessLine.Where(s => s.MachineGrouping.MachineGroupingId != null).Select(s => s.MachineGrouping.MachineGroupingId).ToList();
                var determinfactorIds = standardManufacturingProcessLine.Where(s => s.DetermineFactorId != null).Select(s => s.DetermineFactorId).ToList();
                var ProcessStepIds = standardManufacturingProcessLine.Where(s => s.ProcesssStepId != null).Select(s => s.ProcesssStepId).ToList();
                var operationIds = standardManufacturingProcessLine.Where(s => s.OperationId != null).Select(s => s.OperationId).ToList();
                if(MachineGroupingIds.Count>0)
                {
                    masterDetailIds.AddRange(MachineGroupingIds);
                }
                if(determinfactorIds.Count>0)
                {
                    masterDetailIds.AddRange(determinfactorIds);
                }
                if(ProcessStepIds.Count>0)
                {
                    masterDetailIds.AddRange(ProcessStepIds);
                }
                if(operationIds.Count>0)
                {
                    masterDetailIds.AddRange(operationIds);
                }
                if(masterDetailIds.Count>0)
                {
                    masterDetailList = _context.ApplicationMasterDetail.Where(a=>masterDetailIds.Contains(a.ApplicationMasterDetailId)).AsNoTracking().Distinct().ToList();
                }
            }
            standardManufacturingProcessLine.ForEach(s =>
            {
                StandardManufacturingProcessLineModel standardManufacturingProcessLineModel = new StandardManufacturingProcessLineModel
                {
                    StandardManufacturingProcessLineId = s.StandardManufacturingProcessLineId,
                    StandardManufacturingProcessId = s.StandardManufacturingProcessId,
                    MethodTemplateRoutineLineId = s.MethodTemplateRoutineLineId,
                    DetermineFactorId = s.DetermineFactorId,
                    No = s.No,
                    OperationId = s.OperationId,
                    ProcesssStepId = s.ProcesssStepId,
                    MachineGroupingId = s.MachineGroupingId,
                    MachineGroupingName = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.MachineGrouping.MachineGroupingId).Select(a => a.Value).SingleOrDefault() : "",
                    MachineNameId = s.MachineNameId,
                    MachineName = s.MachineName.MachineNo + " | " + s.MachineName.NameOfTheMachine,
                    Location = s.Location,
                    TimeGapMin = s.TimeGapMin,
                    TimeRequire = s.TimeRequire,
                    DetermineFactorName = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.DetermineFactorId).Select(a => a.Value).SingleOrDefault() : "",
                    ProcesssStepName = masterDetailList != null ? masterDetailList.Where(a => a.ApplicationMasterDetailId == s.ProcesssStepId).Select(a => a.Value).SingleOrDefault() : "",
                    OperationName = icmasterOperation != null && s.OperationId != null ? icmasterOperation.Where(l => l.IcmasterOperationId == s.OperationId).Select(a => a.MasterOperation).SingleOrDefault() : "",

                };
                standardManufacturingProcessLineModels.Add(standardManufacturingProcessLineModel);
            });
            return standardManufacturingProcessLineModels;
        }
        [HttpPost]
        [Route("InsertStandardManufacturingProcess")]
        public StandardManufacturingProcessModel Post(StandardManufacturingProcessModel value)
        {

            var standardManufacturingProcess = new StandardManufacturingProcess
            {
                ManufacturingSiteId = value.ManufacturingSiteId,
                ProfileId = value.ProfileId,
                MethodTempalteId = value.MethodTempalteId,
                MethodTemplateName = value.MethodTemplateName,
                MachineGroupName = value.MachineGroupName,
                TemplateName = value.TemplateName,
                StatusCodeId = value.StatusCodeID,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.StandardManufacturingProcess.Add(standardManufacturingProcess);
            _context.SaveChanges();
            var methodTemplateRoutineLine = _context.MethodTemplateRoutineLine.Where(s => s.MethodTemplateRoutineId == value.MethodTempalteId).AsNoTracking().ToList();
            if (methodTemplateRoutineLine.Count > 0)
            {
                methodTemplateRoutineLine.ForEach(c =>
                {
                    var standardManufacturingProcessLine = new StandardManufacturingProcessLine
                    {
                        StandardManufacturingProcessId = standardManufacturingProcess.StandardManufacturingProcessId,
                        MethodTemplateRoutineLineId = c.MethodTemplateRoutineLineId,
                        No = c.No,
                        ProcesssStepId = c.ProcesssStepId,
                        OperationId = c.OperationId,
                        DetermineFactorId = c.DetermineFactorId,
                        StatusCodeId = value.StatusCodeID,
                        AddedByUserId = value.AddedByUserID,
                        AddedDate = DateTime.Now,
                    };
                    _context.StandardManufacturingProcessLine.Add(standardManufacturingProcessLine);
                    _context.SaveChanges();
                });
            }
            value.StandardManufacturingProcessId = standardManufacturingProcess.StandardManufacturingProcessId;
            return value;
        }
        [HttpPut]
        [Route("UpdateStandardManufacturingProcess")]
        public StandardManufacturingProcessModel Put(StandardManufacturingProcessModel value)
        {
            var standardManufacturingProcess = _context.StandardManufacturingProcess.SingleOrDefault(p => p.StandardManufacturingProcessId == value.StandardManufacturingProcessId);
            var MethodTempalteId = standardManufacturingProcess.MethodTempalteId;
            standardManufacturingProcess.ManufacturingSiteId = value.ManufacturingSiteId;
            standardManufacturingProcess.ProfileId = value.ProfileId;
            standardManufacturingProcess.MethodTempalteId = value.MethodTempalteId;
            standardManufacturingProcess.MethodTemplateName = value.MethodTemplateName;
            standardManufacturingProcess.MachineGroupName = value.MachineGroupName;
            standardManufacturingProcess.TemplateName = value.TemplateName;
            standardManufacturingProcess.StatusCodeId = value.StatusCodeID.Value;
            standardManufacturingProcess.ModifiedByUserId = value.ModifiedByUserID;
            standardManufacturingProcess.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            if (MethodTempalteId != value.MethodTempalteId)
            {
                var standardManufacturingProcessLines = _context.StandardManufacturingProcessLine.Where(l => l.StandardManufacturingProcessId == value.StandardManufacturingProcessId).AsNoTracking().ToList();
                if (standardManufacturingProcessLines != null)
                {
                    _context.StandardManufacturingProcessLine.RemoveRange(standardManufacturingProcessLines);
                    _context.SaveChanges();
                }
                var methodTemplateRoutineLine = _context.MethodTemplateRoutineLine.Where(s => s.MethodTemplateRoutineId == value.MethodTempalteId).AsNoTracking().ToList();
                if (methodTemplateRoutineLine.Count > 0)
                {
                    methodTemplateRoutineLine.ForEach(c =>
                    {
                        var standardManufacturingProcessLine = new StandardManufacturingProcessLine
                        {
                            StandardManufacturingProcessId = standardManufacturingProcess.StandardManufacturingProcessId,
                            MethodTemplateRoutineLineId = c.MethodTemplateRoutineLineId,
                            No = c.No,
                            ProcesssStepId = c.ProcesssStepId,
                            OperationId = c.OperationId,
                            DetermineFactorId = c.DetermineFactorId,
                            StatusCodeId = value.StatusCodeID,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                        };
                        _context.StandardManufacturingProcessLine.Add(standardManufacturingProcessLine);
                        _context.SaveChanges();
                    });
                }
            }

            return value;
        }
        [HttpPut]
        [Route("UpdateStandardManufacturingProcessLine")]
        public StandardManufacturingProcessLineModel Put(StandardManufacturingProcessLineModel value)
        {
            var standardManufacturingProcessLine = _context.StandardManufacturingProcessLine.SingleOrDefault(p => p.StandardManufacturingProcessLineId == value.StandardManufacturingProcessLineId);
            standardManufacturingProcessLine.DetermineFactorId = value.DetermineFactorId;
            standardManufacturingProcessLine.MethodTemplateRoutineLineId = value.MethodTemplateRoutineLineId;
            standardManufacturingProcessLine.No = value.No;
            standardManufacturingProcessLine.OperationId = value.OperationId;
            standardManufacturingProcessLine.ProcesssStepId = value.ProcesssStepId;
            standardManufacturingProcessLine.MachineGroupingId = value.MachineGroupingId;
            standardManufacturingProcessLine.MachineNameId = value.MachineNameId;
            standardManufacturingProcessLine.Location = value.Location;
            standardManufacturingProcessLine.TimeRequire = value.TimeRequire;
            standardManufacturingProcessLine.TimeGapMin = value.TimeGapMin;
            standardManufacturingProcessLine.StatusCodeId = value.StatusCodeID.Value;
            standardManufacturingProcessLine.ModifiedByUserId = value.ModifiedByUserID;
            standardManufacturingProcessLine.ModifiedDate = DateTime.Now;
            _context.SaveChanges();

            return value;
        }
        [HttpDelete]
        [Route("DeleteStandardManufacturingProcess")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var standardManufacturingProcess = _context.StandardManufacturingProcess.Where(p => p.StandardManufacturingProcessId == id).FirstOrDefault();
                if (standardManufacturingProcess != null)
                {
                    if (standardManufacturingProcess.StandardManufacturingProcessLine != null)
                    {
                        _context.StandardManufacturingProcessLine.RemoveRange(standardManufacturingProcess.StandardManufacturingProcessLine);
                        _context.SaveChanges();
                    }
                    _context.StandardManufacturingProcess.Remove(standardManufacturingProcess);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}