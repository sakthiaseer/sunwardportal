﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class AppmaterialReturnController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment _hostingEnvironment;
        public AppmaterialReturnController(CRT_TMSContext context, IMapper mapper, IHostingEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _hostingEnvironment = host;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetAppmaterialReturns")]
        public List<AppmaterialReturnModel> Get()
        {
            var appmaterialreturn = _context.AppmaterialReturn.Include("ModifiedByUser").Select(s => new AppmaterialReturnModel
            {

                MaterialReturnID = s.MaterialReturnId,
                ProdOrderNo = s.ProdOrderNo,
                ProdLineNo = s.ProdLineNo,
                ItemNo = s.ItemNo,
                Description = s.Description,
                DrumNo = s.DrumNo,
                LotNo = s.LotNo,
                QcrefNo = s.QcrefNo,
                BatchNo = s.BatchNo,
                ExpiryDate = s.ExpiryDate,
                Uom = s.Uom,
                BalanceWeight = s.BalanceWeight,
                PostedtoNav = s.PostedtoNav,
                Photo = s.Photo,

                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate

            }).OrderByDescending(o => o.MaterialReturnID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<AppmaterialReturnModel>>(AppmaterialReturn);
            return appmaterialreturn;
        }

        //Changes Done by Aravinth Start 18JUL2019

        [HttpPost]
        [Route("GetAppmaterialReturnFilter")]
        public List<AppmaterialReturnModel> GetFilter(AppmaterialReturnSearchModel value)
        {
            var appMaterialReturn = _context.AppmaterialReturn.Select(s => new AppmaterialReturnModel
            {
                MaterialReturnID = s.MaterialReturnId,
                ProdOrderNo = s.ProdOrderNo,
                ProdLineNo = s.ProdLineNo,
                ItemNo = s.ItemNo,
                Description = s.Description,
                DrumNo = s.DrumNo,
                LotNo = s.LotNo,
                QcrefNo = s.QcrefNo,
                BatchNo = s.BatchNo,
                Uom = s.Uom,

            }).OrderByDescending(o => o.MaterialReturnID).AsNoTracking().ToList();

            List<AppmaterialReturnModel> filteredItems = new List<AppmaterialReturnModel>();
            if (value.ProdOrderNo.Any())
            {
                var itemProdOrderNoFilters = appMaterialReturn.Where(p => p.ProdOrderNo != null ? (p.ProdOrderNo.ToLower() == (value.ProdOrderNo.ToLower())) : false).ToList();
                itemProdOrderNoFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.MaterialReturnID == i.MaterialReturnID))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.ProdLineNo.ToString().Any())
            {
                var itemProdLineNoFilters = appMaterialReturn.Where(p => p.ProdLineNo.ToString() != null ? (p.ProdLineNo.ToString().ToLower() == (value.ProdLineNo.ToString().ToLower())) : false).ToList();
                itemProdLineNoFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.MaterialReturnID == i.MaterialReturnID))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.ItemNo.Any())
            {
                var itemItemNoFilters = appMaterialReturn.Where(p => p.ItemNo != null ? (p.ItemNo.ToLower() == (value.ItemNo.ToLower())) : false).ToList();
                itemItemNoFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.MaterialReturnID == i.MaterialReturnID))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.Description.Any())
            {
                var itemDescriptionFilters = appMaterialReturn.Where(p => p.Description != null ? (p.Description.ToLower() == (value.Description.ToLower())) : false).ToList();
                itemDescriptionFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.MaterialReturnID == i.MaterialReturnID))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.DrumNo.Any())
            {
                var itemDrumNoFilters = appMaterialReturn.Where(p => p.DrumNo != null ? (p.DrumNo.ToLower() == (value.DrumNo.ToLower())) : false).ToList();
                itemDrumNoFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.MaterialReturnID == i.MaterialReturnID))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.LotNo.Any())
            {
                var itemLotNoFilters = appMaterialReturn.Where(p => p.LotNo != null ? (p.LotNo.ToLower() == (value.LotNo.ToLower())) : false).ToList();
                itemLotNoFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.MaterialReturnID == i.MaterialReturnID))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.QcrefNo.Any())
            {
                var itemQcrefNoFilters = appMaterialReturn.Where(p => p.QcrefNo != null ? (p.QcrefNo.ToLower() == (value.QcrefNo.ToLower())) : false).ToList();
                itemQcrefNoFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.MaterialReturnID == i.MaterialReturnID))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.BatchNo.Any())
            {
                var itemBatchNoFilters = appMaterialReturn.Where(p => p.BatchNo != null ? (p.BatchNo.ToLower() == (value.BatchNo.ToLower())) : false).ToList();
                itemBatchNoFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.MaterialReturnID == i.MaterialReturnID))
                    {
                        filteredItems.Add(i);
                    }

                });
            }
            if (value.Uom.Any())
            {
                var itemUomFilters = appMaterialReturn.Where(p => p.Uom != null ? (p.Uom.ToLower() == (value.Uom.ToLower())) : false).ToList();
                itemUomFilters.ForEach(i =>
                {
                    if (!filteredItems.Any(f => f.MaterialReturnID == i.MaterialReturnID))
                    {
                        filteredItems.Add(i);
                    }

                });
            }


            return filteredItems;
        }

        //Changes Done by Aravinth End 18JUL2019

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<AppmaterialReturnModel> GetData(SearchModel searchModel)
        {
            var appmaterialreturn = new AppmaterialReturn();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        appmaterialreturn = _context.AppmaterialReturn.OrderByDescending(o => o.MaterialReturnId).FirstOrDefault();
                        break;
                    case "Last":
                        appmaterialreturn = _context.AppmaterialReturn.OrderByDescending(o => o.MaterialReturnId).LastOrDefault();
                        break;
                    case "Next":
                        appmaterialreturn = _context.AppmaterialReturn.OrderByDescending(o => o.MaterialReturnId).LastOrDefault();
                        break;
                    case "Previous":
                        appmaterialreturn = _context.AppmaterialReturn.OrderByDescending(o => o.MaterialReturnId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        appmaterialreturn = _context.AppmaterialReturn.OrderByDescending(o => o.MaterialReturnId).FirstOrDefault();
                        break;
                    case "Last":
                        appmaterialreturn = _context.AppmaterialReturn.OrderByDescending(o => o.MaterialReturnId).LastOrDefault();
                        break;
                    case "Next":
                        appmaterialreturn = _context.AppmaterialReturn.OrderBy(o => o.MaterialReturnId).FirstOrDefault(s => s.MaterialReturnId > searchModel.Id);
                        break;
                    case "Previous":
                        appmaterialreturn = _context.AppmaterialReturn.OrderByDescending(o => o.MaterialReturnId).FirstOrDefault(s => s.MaterialReturnId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<AppmaterialReturnModel>(appmaterialreturn);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertAppmaterialReturn")]
        public AppmaterialReturnModel Post(AppmaterialReturnModel value)
        {
            var appmaterialreturn = new AppmaterialReturn
            {
                ProdOrderNo = value.ProdOrderNo,
                ProdLineNo = value.ProdLineNo,
                ItemNo = value.ItemNo,
                Description = value.Description,
                DrumNo = value.DrumNo,
                LotNo = value.LotNo,
                QcrefNo = value.QcrefNo,
                BatchNo = value.BatchNo,
                ExpiryDate = value.ExpiryDate,
                Uom = value.Uom,
                BalanceWeight = value.BalanceWeight,
                PostedtoNav = false,
                Photo = value.Photo,
                StatusCodeId = 1,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.AppmaterialReturn.Add(appmaterialreturn);

            if (value.FrontPhoto != null && value.FrontPhoto.Length > 0)
            {
                var serverPath = _hostingEnvironment.ContentRootPath + @"\AppUpload\" + value.Photo;
                System.IO.File.WriteAllBytes(serverPath, value.FrontPhoto);
            }

            _context.SaveChanges();

            value.MaterialReturnID = appmaterialreturn.MaterialReturnId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateAppmaterialReturn")]
        public AppmaterialReturnModel Put(AppmaterialReturnModel value)
        {
            var appmaterialreturn = _context.AppmaterialReturn.SingleOrDefault(p => p.MaterialReturnId == value.MaterialReturnID);

            appmaterialreturn.ProdOrderNo = value.ProdOrderNo;
            appmaterialreturn.ProdLineNo = value.ProdLineNo;
            appmaterialreturn.ItemNo = value.ItemNo;
            appmaterialreturn.Description = value.Description;
            appmaterialreturn.DrumNo = value.DrumNo;
            appmaterialreturn.LotNo = value.LotNo;
            appmaterialreturn.QcrefNo = value.QcrefNo;
            appmaterialreturn.BatchNo = value.BatchNo;
            appmaterialreturn.ExpiryDate = value.ExpiryDate;
            appmaterialreturn.Uom = value.Uom;
            appmaterialreturn.BalanceWeight = value.BalanceWeight;
            appmaterialreturn.PostedtoNav = value.PostedtoNav;
            appmaterialreturn.Photo = value.Photo;

            appmaterialreturn.ModifiedByUserId = value.ModifiedByUserID;
            appmaterialreturn.ModifiedDate = value.ModifiedDate;
            appmaterialreturn.StatusCodeId = value.StatusCodeID;
            _context.SaveChanges();



            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteAppmaterialReturn")]
        public void Delete(int id)
        {
            var appmaterialreturn = _context.AppmaterialReturn.SingleOrDefault(p => p.MaterialReturnId == id);
            var errorMessage = "";
            try
            {
                if (appmaterialreturn != null)
                {
                    _context.AppmaterialReturn.Remove(appmaterialreturn);
                    _context.SaveChanges();
                }
            }

            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "There is a transaction this record cannot be deleted!";
                }
                throw new AppException(errorMessage, ex);
            }
        }
    }
}