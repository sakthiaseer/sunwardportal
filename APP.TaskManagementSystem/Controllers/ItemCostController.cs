﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using APP.BussinessObject;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ItemCostController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly TableVersionRepository _repository;

        public ItemCostController(CRT_TMSContext context, IMapper mapper, TableVersionRepository repository)
        {
            _context = context;
            _mapper = mapper;
            _repository = repository;
        }

        #region ItemCost

        [HttpGet]
        [Route("GetItemCosts")]
        public List<ItemCostModel> Get()
        {
            List<ItemCostModel> itemCostModels = new List<ItemCostModel>();
            var applicationMasterDetatil = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var itemCosts = _context.ItemCost.Include("AddedByUser").Include(i => i.Item).Include(c => c.Company).Include("StatusCode").Include("ModifiedByUser").OrderByDescending(o => o.ItemCostId).AsNoTracking().ToList();
            if (itemCosts != null && itemCosts.Count > 0)
            {
                itemCosts.ForEach(s =>
                {
                    ItemCostModel itemCostModel = new ItemCostModel
                    {
                        ItemCostId = s.ItemCostId,
                        CostCurrency = s.CostCurrency,
                        CostMethod = s.CostMethod,
                        CompanyId = s.CompanyId,
                        CompanyName = s.Company?.PlantCode,
                        IsSyncWithNavision = s.IsSyncWithNavision,
                        PurposeOfCostingId = s.PurposeOfCostingId,
                        PurposeOfCosting = applicationMasterDetatil.FirstOrDefault(a => a.ApplicationMasterDetailId == s.PurposeOfCostingId) != null ? applicationMasterDetatil.FirstOrDefault(a => a.ApplicationMasterDetailId == s.PurposeOfCostingId).Value : string.Empty,
                        Uom = s.Uom,
                        ValidityFrom = s.ValidityFrom,
                        ValidityTo = s.ValidityTo,
                        Cost = s.Cost,
                        ItemId = s.ItemId,
                        ItemNo = s.Item?.No,
                        BaseUnitofMeasure = s.Item?.BaseUnitofMeasure,
                        InternalRef = s.Item?.InternalRef,
                        ItemCategoryCode = s.Item?.ItemCategoryCode.Replace("FP", ""),
                        Description = s.Item?.Description,
                        SessionId = s.SessionId,
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        VersionSessionId = s.VersionSessionId
                    };
                    itemCostModels.Add(itemCostModel);
                });
            }

            return itemCostModels;
        }

        [HttpGet]
        [Route("GetItemCostVersion")]
        public async Task<IEnumerable<TableDataVersionInfoModel<ItemCostModel>>> GetItemCostVersion(string sessionID)
        {
            return await _repository.GetList<ItemCostModel>(sessionID);
        }
        //[HttpGet("{id}", Name = "Get PlasticBag")]
        [HttpGet("GetItemCostById")]
        public ActionResult<ItemCostModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var itemCost = _context.ItemCost.SingleOrDefault(p => p.ItemCostId == id.Value);
            var result = _mapper.Map<ItemCostModel>(itemCost);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ItemCostModel> GetData(SearchModel searchModel)
        {
            var itemCost = new ItemCost();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        itemCost = _context.ItemCost.OrderByDescending(o => o.ItemCostId).FirstOrDefault();
                        break;
                    case "Last":
                        itemCost = _context.ItemCost.OrderByDescending(o => o.ItemCostId).LastOrDefault();
                        break;
                    case "Next":
                        itemCost = _context.ItemCost.OrderByDescending(o => o.ItemCostId).LastOrDefault();
                        break;
                    case "Previous":
                        itemCost = _context.ItemCost.OrderByDescending(o => o.ItemCostId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        itemCost = _context.ItemCost.OrderByDescending(o => o.ItemCostId).FirstOrDefault();
                        break;
                    case "Last":
                        itemCost = _context.ItemCost.OrderByDescending(o => o.ItemCostId).LastOrDefault();
                        break;
                    case "Next":
                        itemCost = _context.ItemCost.OrderBy(o => o.ItemCostId).FirstOrDefault(s => s.ItemCostId > searchModel.Id);
                        break;
                    case "Previous":
                        itemCost = _context.ItemCost.OrderByDescending(o => o.ItemCostId).FirstOrDefault(s => s.ItemCostId < searchModel.Id);
                        break;
                }
            }
           

            var result = _mapper.Map<ItemCostModel>(itemCost);
            if (result!=null )
            {
                if (result.ItemId > 0)
                {
                    var navItem = _context.Navitems.FirstOrDefault(n => n.ItemId == result.ItemId);
                    if (navItem != null)
                    {
                        result.ItemCategoryCode = navItem.ItemCategoryCode;
                        result.Description = navItem.Description;
                        result.InternalRef = navItem.InternalRef;
                        result.BaseUnitofMeasure = navItem.BaseUnitofMeasure;
                    }
                }
            }
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertItemCost")]
        public ItemCostModel Post(ItemCostModel value)
        {
            var SessionId = Guid.NewGuid();
            var versionSessionId = Guid.NewGuid();
            var applicationMasterDetatil = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var existingValidFrom = _context.ItemCost.Where(s => s.CompanyId == value.CompanyId && s.PurposeOfCostingId == value.PurposeOfCostingId &&((s.ValidityFrom <= value.ValidityFrom && s.ValidityTo>=value.ValidityFrom)||(s.ValidityTo>=value.ValidityTo && s.ValidityFrom<=value.ValidityTo))).FirstOrDefault();
            if (existingValidFrom == null)
            {
                var itemCost = new ItemCost
                {

                    CostCurrency = value.CostCurrency,
                    CostMethod = value.CostMethod,
                    CompanyId = value.CompanyId,
                    PurposeOfCostingId = value.PurposeOfCostingId,
                    IsSyncWithNavision = value.IsSyncWithNavision,
                    SessionId = SessionId,
                    Uom = value.Uom,
                    ValidityFrom = value.ValidityFrom,
                    ValidityTo = value.ValidityTo,
                    Cost = value.Cost,
                    ItemId = value.ItemId,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                    VersionSessionId = versionSessionId,

                };
                _context.ItemCost.Add(itemCost);
                _context.SaveChanges();
                value.ItemCostId = itemCost.ItemCostId;
                value.SessionId = SessionId;
                value.PurposeOfCosting = applicationMasterDetatil.FirstOrDefault(a => a.ApplicationMasterDetailId == value.PurposeOfCostingId) != null ? applicationMasterDetatil.FirstOrDefault(a => a.ApplicationMasterDetailId == value.PurposeOfCostingId).Value : string.Empty;
                var navItem = _context.Navitems.FirstOrDefault(n => n.ItemId == itemCost.ItemId);
                value.ItemCategoryCode = navItem?.ItemCategoryCode;
                value.Description = navItem?.Description;
                value.InternalRef = navItem?.InternalRef;
                value.BaseUnitofMeasure = navItem?.BaseUnitofMeasure;
                value.VersionSessionId = itemCost.VersionSessionId;
                if (value.CompanyId != null && value.IsSyncWithNavision == true)
                {
                    List<ItemCostLine> itemCostLines = new List<ItemCostLine>();
                    itemCostLines = SyncWithNavision(value.CompanyId);
                    if (itemCostLines != null && itemCostLines.Count > 0)
                    {
                        itemCostLines.ForEach(s =>
                        {
                            ItemCostLine itemCostLine = new ItemCostLine
                            {
                                ItemCostId = itemCost.ItemCostId,
                                ItemId = s.ItemId,
                                CostMethod = s.CostMethod,
                                CostCurrency = s.CostCurrency,
                                Cost = s.Cost,
                                Uom = s.Item?.PurchaseUom,
                                StatusCodeId = s.StatusCodeId,
                                AddedByUserId = s.AddedByUserId,
                                ModifiedByUserId = s.ModifiedByUserId,
                                AddedDate = s.AddedDate,
                                ModifiedDate = s.ModifiedDate,


                            };
                            _context.ItemCostLine.Add(itemCostLine);
                            _context.SaveChanges();
                        });
                    }

                }
            }
            else
            {
                throw new Exception("Item Cost Already Exist For Selected Compination");
            }
            return value;
        }

        [HttpGet]
        [Route("SyncWithNavision")]
        public List<ItemCostLine> SyncWithNavision(long? id)
        {
            List<ItemCostLine> itemCostLines = new List<ItemCostLine>();
           
            return itemCostLines;
        }
        [HttpPut]
        [Route("SyncWithNavisionUpdateCost")]
        public List<ItemCostLine> SyncWithNavisionUpdateCost(ItemCostModel itemCost)
        {
            List<ItemCostLine> itemCostLines = new List<ItemCostLine>();
            if (itemCost.CompanyId != null && itemCost.IsSyncWithNavision == true)
            {
                
                itemCostLines = SyncWithNavision(itemCost.CompanyId);
                if (itemCostLines != null && itemCostLines.Count > 0)
                {
                    itemCostLines.ForEach(s =>
                    {
                        ItemCostLine itemCostLine = new ItemCostLine
                        {
                            ItemCostId = itemCost.ItemCostId,
                            ItemId = s.ItemId,
                            CostMethod = s.CostMethod,
                            CostCurrency = s.CostCurrency,
                            Cost = s.Cost,
                            Uom = s.Item?.PurchaseUom,
                            StatusCodeId = s.StatusCodeId,
                            AddedByUserId = s.AddedByUserId,
                            ModifiedByUserId = s.ModifiedByUserId,
                            AddedDate = s.AddedDate,
                            ModifiedDate = s.ModifiedDate,


                        };
                        _context.ItemCostLine.Add(itemCostLine);
                        _context.SaveChanges();
                    });
                }

            }

            return itemCostLines;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateItemCost")]
        public async Task<ItemCostModel> Put(ItemCostModel value)
        {
            var itemCost = _context.ItemCost.SingleOrDefault(p => p.ItemCostId == value.ItemCostId);
            value.SessionId ??= Guid.NewGuid();
            value.VersionSessionId ??= Guid.NewGuid();
            if (value.SaveVersionData)
            {
                var versionInfo = new TableDataVersionInfoModel<ItemCostModel>
                {
                    JsonData = JsonSerializer.Serialize(value),
                    AddedByUserId = value.ModifiedByUserID.Value,
                    AddedByUserID = value.ModifiedByUserID.Value,
                    AddedDate = DateTime.Now,
                    SessionId = value.VersionSessionId.Value,
                    ScreenId = value.ScreenID,
                    TableName = "ItemCost",
                    PrimaryKey = value.ItemCostId,
                    ReferenceInfo = value.ReferenceInfo,
                };
                await _repository.Insert(versionInfo);
            }
            itemCost.ItemCostId = value.ItemCostId;
            itemCost.CostCurrency = value.CostCurrency;
            itemCost.CompanyId = value.CompanyId;
            itemCost.CostMethod = value.CostMethod;
            itemCost.PurposeOfCostingId = value.PurposeOfCostingId;
            itemCost.Uom = value.Uom;
            itemCost.ValidityFrom = value.ValidityFrom;
            itemCost.ValidityTo = value.ValidityTo;
            itemCost.SessionId = value.SessionId;
            itemCost.VersionSessionId = value.VersionSessionId;
            itemCost.Cost = value.Cost;
            itemCost.IsSyncWithNavision = value.IsSyncWithNavision;
            itemCost.ItemId = value.ItemId;
            itemCost.ModifiedByUserId = value.ModifiedByUserID;
            itemCost.ModifiedDate = DateTime.Now;
            itemCost.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            return value;
        }

        [HttpPut]
        [Route("UpdateItemCostVersion")]
        public ItemCostModel UpdateItemCostVersion(ItemCostModel value)
        {
            var itemCost = _context.ItemCost.Include(i => i.ItemCostLine).SingleOrDefault(p => p.ItemCostId == value.ItemCostId);
            itemCost.ItemCostId = value.ItemCostId;
            itemCost.CostCurrency = value.CostCurrency;
            itemCost.CompanyId = value.CompanyId;
            itemCost.CostMethod = value.CostMethod;
            itemCost.PurposeOfCostingId = value.PurposeOfCostingId;
            itemCost.Uom = value.Uom;
            itemCost.ValidityFrom = value.ValidityFrom;
            itemCost.ValidityTo = value.ValidityTo;
            itemCost.SessionId = value.SessionId;
            itemCost.VersionSessionId = value.VersionSessionId;
            itemCost.Cost = value.Cost;
            itemCost.IsSyncWithNavision = value.IsSyncWithNavision;
            itemCost.ItemId = value.ItemId;
            itemCost.ModifiedByUserId = value.ModifiedByUserID;
            itemCost.ModifiedDate = DateTime.Now;
            itemCost.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            if (itemCost.ItemCostLine != null)
            {
                _context.ItemCostLine.RemoveRange(itemCost.ItemCostLine);
                _context.SaveChanges();
            }
            value.ItemCostLineModels.ForEach(value =>
            {
                var itemCostLine = new ItemCostLine
                {
                    ItemCostId = value.ItemCostId,
                    ItemId = value.ItemId,
                    Cost = value.Cost,
                    CostCurrency = value.CostCurrency,
                    CostMethod = value.CostMethod,
                    ReasonForChange = value.ReasonForChange,
                    StatusCodeId = value.StatusCodeID.Value,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                };
                _context.ItemCostLine.Add(itemCostLine);
            });
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteItemCost")]
        public void Delete(int id)
        {
            var itemCost = _context.ItemCost.Include(c => c.ItemCostLine).SingleOrDefault(p => p.ItemCostId == id);
            if (itemCost != null)
            {
                if (itemCost.SessionId != null)
                {
                    var versions = _context.TableDataVersionInfo.Include(t => t.TempVersion).Where(f => f.SessionId == itemCost.VersionSessionId).ToList();
                    _context.TableDataVersionInfo.RemoveRange(versions);

                }
                _context.ItemCost.Remove(itemCost);
                _context.SaveChanges();
            }
        }
        [HttpPut]
        [Route("CreateVersion")]
        public async Task<ItemCostModel> CreateVersion(ItemCostModel value)
        {
            try
            {
                value.SessionId = value.VersionSessionId.Value;
                var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
                var versionData = _context.ItemCost.Include(s => s.ItemCostLine).SingleOrDefault(p => p.ItemCostId == value.ItemCostId);

                if (versionData != null)
                {
                    var verObject = _mapper.Map<ItemCostModel>(versionData);
                    verObject.ItemCostLineModels = new List<ItemCostLineModel>();
                    versionData.ItemCostLine.ToList().ForEach(s =>
                    {
                        var itemCostLineModel = new ItemCostLineModel
                        {
                            ItemCostId = s.ItemCostId,
                            ItemId = s.ItemId,
                            Cost = s.Cost,
                            CostCurrency = s.CostCurrency,
                            CostMethod = s.CostMethod,
                            ReasonForChange = s.ReasonForChange,
                            Uom = s.Uom,
                            StatusCodeID = s.StatusCodeId,
                            AddedByUserID = s.AddedByUserId,
                            AddedDate = s.AddedDate,
                        };
                        verObject.ItemCostLineModels.Add(itemCostLineModel);
                    });


                    var versionInfo = new TableDataVersionInfoModel<ItemCostModel>
                    {
                        JsonData = JsonSerializer.Serialize(verObject),
                        AddedByUserId = value.ModifiedByUserID.Value,
                        AddedByUserID = value.ModifiedByUserID.Value,
                        AddedDate = DateTime.Now,
                        SessionId = value.SessionId.Value,
                        ScreenId = value.ScreenID,
                        TableName = "ItemCost",
                        PrimaryKey = value.ItemCostId,
                        ReferenceInfo = value.ReferenceInfo,
                    };
                    await _repository.Insert(versionInfo);
                }
                return value;
            }
            catch (Exception ex)
            {
                throw new Exception("create Version failed.");
            }
        }

        [HttpPut]
        [Route("ApplyVersion")]
        public async Task<ItemCostModel> ApplyVersion(ItemCostModel value)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            try
            {
                var versionData = _context.ItemCost.Include(s => s.ItemCostLine).SingleOrDefault(p => p.ItemCostId == value.ItemCostId);

                if (versionData != null)
                {

                    versionData.StatusCodeId = value.StatusCodeID.Value;
                    versionData.ModifiedByUserId = value.ModifiedByUserID;
                    versionData.ModifiedDate = DateTime.Now;
                    versionData.VersionSessionId = value.VersionSessionId;

                    _context.ItemCostLine.RemoveRange(versionData.ItemCostLine);
                    _context.SaveChanges();
                    if (value.ItemCostLineModels != null && value.ItemCostLineModels.Count > 0)
                    {
                        value.ItemCostLineModels.ForEach(s =>
                        {
                            var itemCostLine = new ItemCostLine
                            {
                                ItemCostId = s.ItemCostId,
                                ItemId = s.ItemId,
                                Cost = s.Cost,
                                CostCurrency = s.CostCurrency,
                                CostMethod = s.CostMethod,
                                ReasonForChange = s.ReasonForChange,
                                Uom = s.Uom,
                                StatusCodeId = s.StatusCodeID.Value,
                                AddedByUserId = s.AddedByUserID,
                                AddedDate = DateTime.Now,
                            };
                            _context.ItemCostLine.Add(itemCostLine);
                        });
                    }

                    _context.SaveChanges();
                }
                return value;
            }
            catch (Exception ex)
            {
                throw new Exception("Version update changes failed.");
            }
        }
        #endregion
        [HttpPut]
        [Route("TempVersion")]
        public async Task<long> TempVersion(ItemCostModel value)
        {
            value.SessionId = value.VersionSessionId.Value;
            var itemCost = _context.ItemCost.SingleOrDefault(p => p.ItemCostId == value.ItemCostId);

            if (itemCost != null)
            {
                var verObject = _mapper.Map<ItemCostModel>(itemCost);
                if (verObject.ItemCostLineModels != null && verObject.ItemCostLineModels.Count > 0)
                {
                    verObject.ItemCostLineModels.ForEach(s =>
                    {
                        var itemCostLine = new ItemCostLine
                        {
                            ItemCostId = s.ItemCostId,
                            ItemId = s.ItemId,
                            Cost = s.Cost,
                            CostCurrency = s.CostCurrency,
                            CostMethod = s.CostMethod,
                            ReasonForChange = s.ReasonForChange,
                            Uom = s.Uom,
                            StatusCodeId = s.StatusCodeID.Value,
                            AddedByUserId = s.AddedByUserID,
                            AddedDate = DateTime.Now,
                        };
                        _context.ItemCostLine.Add(itemCostLine);
                    });
                }
                var versionInfo = new TableDataVersionInfoModel<ItemCostModel>
                {
                    JsonData = JsonSerializer.Serialize(verObject),
                    AddedByUserId = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedByUserID = value.ModifiedByUserID ?? value.AddedByUserID.Value,
                    AddedDate = DateTime.Now,
                    SessionId = value.SessionId.Value,
                    ScreenId = value.ScreenID,
                    TableName = "ItemCost",
                    PrimaryKey = value.ItemCostId,
                    ReferenceInfo = "Temp Version - undo",
                };
                var result = await _repository.Insert(versionInfo);
                return result.VersionTableId;
            }
            return -1;
        }
        [HttpGet]
        [Route("UndoVersion")]
        public async Task<ItemCostModel> UndoVersion(int Id)
        {
            try
            {
                var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

                if (tableData != null)
                {
                    var verObject = JsonSerializer.Deserialize<ItemCostModel>(tableData.JsonData);

                    var itemCost = _context.ItemCost.Include(s=>s.ItemCostLine).SingleOrDefault(p => p.ItemCostId == verObject.ItemCostId);


                    if (verObject != null && itemCost != null)
                    {
                        itemCost.CostCurrency = verObject.CostCurrency;
                        itemCost.CostMethod = verObject.CostMethod;
                        itemCost.CompanyId = verObject.CompanyId;
                        itemCost.PurposeOfCostingId = verObject.PurposeOfCostingId;
                        itemCost.IsSyncWithNavision = verObject.IsSyncWithNavision;
                        itemCost.SessionId = verObject.SessionId;
                        itemCost.Uom = verObject.Uom;
                        itemCost.ValidityFrom = verObject.ValidityFrom;
                        itemCost.ValidityTo = verObject.ValidityTo;
                        itemCost.Cost = verObject.Cost;
                        itemCost.ItemId = verObject.ItemId;                       
                        itemCost.StatusCodeId = verObject.StatusCodeID.Value;
                        itemCost.ModifiedByUserId = verObject.AddedByUserID;
                        itemCost.ModifiedDate = DateTime.Now;

                        if (itemCost.ItemCostLine != null && itemCost.ItemCostLine.Count > 0)
                        {
                            _context.ItemCostLine.RemoveRange(itemCost.ItemCostLine);
                            _context.SaveChanges();
                        }
                        if (verObject.ItemCostLineModels!=null && verObject.ItemCostLineModels.Count>0)
                            {
                            verObject.ItemCostLineModels.ForEach(s =>
                            {
                                var itemCostLine = new ItemCostLine
                                {
                                    ItemCostId = s.ItemCostId,
                                    ItemId = s.ItemId,
                                    Cost = s.Cost,
                                    CostCurrency = s.CostCurrency,
                                    CostMethod = s.CostMethod,
                                    ReasonForChange = s.ReasonForChange,
                                    Uom = s.Uom,
                                    StatusCodeId = s.StatusCodeID.Value,
                                    AddedByUserId = s.AddedByUserID,
                                    AddedDate = DateTime.Now,
                                };
                                _context.ItemCostLine.Add(itemCostLine);
                            });
                        }
                        _context.SaveChanges();
                    }

                    return verObject;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Undo Version failed.");
            }
            return new ItemCostModel();
        }
        [HttpDelete]
        [Route("DeleteVersion")]
        public async Task<long> DeleteVersion(int Id)
        {

            var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

            if (tableData != null)
            {
                _context.TableDataVersionInfo.Remove(tableData);
                _context.SaveChanges();

            }
            return -1;
        }
        #region ItemCostLine

        [HttpGet]
        [Route("GeItemCostLinesById")]
        public List<ItemCostLineModel> GeItemCostLinesById(int id)
        {
            var itemCostLines = _context.ItemCostLine
                .Include(a => a.AddedByUser)
                .Include(m => m.ModifiedByUser)
                .Include(s => s.StatusCode)
                .Include(i => i.Item)
                .Where(s => s.ItemCostId == id)
                .AsNoTracking()
                .ToList();
            List<ItemCostLineModel> itemCostLineModels = new List<ItemCostLineModel>();
            itemCostLines.ForEach(s =>
            {
                ItemCostLineModel itemCostLineModel = new ItemCostLineModel
                {
                    ItemCostId = s.ItemCostId,
                    ItemCostLineId = s.ItemCostLineId,
                    ItemId = s.ItemId,
                    Item = s.Item != null ? s.Item.No : "",
                    CostMethod = s.CostMethod,
                    CostCurrency = s.CostCurrency,
                    Cost = s.Cost,
                    Uom = s.Item?.PurchaseUom,
                    ReasonForChange = s.ReasonForChange,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",

                };
                itemCostLineModels.Add(itemCostLineModel);
            });
            return itemCostLineModels.OrderByDescending(a => a.ItemCostLineId).ToList();
        }
        [HttpPost]
        [Route("InsertItemCostLine")]
        public ItemCostLineModel Post(ItemCostLineModel value)
        {
            var itemCostLine = new ItemCostLine
            {
                ItemCostId = value.ItemCostId,
                ItemId = value.ItemId,
                Cost = value.Cost,
                CostCurrency = value.CostCurrency,
                CostMethod = value.CostMethod,
                ReasonForChange = value.ReasonForChange,
                Uom = value.Uom,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
            };
            _context.ItemCostLine.Add(itemCostLine);
            _context.SaveChanges();
            value.ItemCostLineId = itemCostLine.ItemCostLineId;
            return value;
        }
        [HttpPut]
        [Route("UpdateItemCostLine")]
        public ItemCostLineModel Put(ItemCostLineModel value)
        {
            var itemCostLine = _context.ItemCostLine.SingleOrDefault(p => p.ItemCostLineId == value.ItemCostLineId);
            itemCostLine.ItemCostId = value.ItemCostId;
            itemCostLine.ItemId = value.ItemId;
            itemCostLine.Cost = value.Cost;
            itemCostLine.CostCurrency = value.CostCurrency;
            itemCostLine.CostMethod = value.CostMethod;
            itemCostLine.ReasonForChange = value.ReasonForChange;
            itemCostLine.StatusCodeId = value.StatusCodeID.Value;
            itemCostLine.ModifiedByUserId = value.ModifiedByUserID;
            itemCostLine.ModifiedDate = DateTime.Now;
            _context.SaveChanges();

            return value;
        }
        [HttpDelete]
        [Route("DeleteItemCostLine")]
        public ActionResult<string> DeleteItemCostLine(int id)
        {
            try
            {
                var itemCostLine = _context.ItemCostLine.Where(p => p.ItemCostLineId == id).FirstOrDefault();
                if (itemCostLine != null)
                {

                    _context.ItemCostLine.Remove(itemCostLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #endregion
    }
}