﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NAV;
using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class BlisterScrapCodeController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        private readonly IConfiguration _config;
        public NAV.NAV Context { get; private set; }
        public BlisterScrapCodeController(CRT_TMSContext context, IMapper mapper, IConfiguration config)
        {
            _context = context;
            _mapper = mapper;
            _config = config;

            Context = new NAV.NAV(new Uri($"{_config["NAV:OdataUrl"]}/Company('{_config["NAV:Company"]}')"))
            {
                Credentials = new NetworkCredential(_config["NAV:UserName"], _config["NAV:Password"])
            };

        }

        // GET: api/Project
        [HttpGet]
        [Route("GetBlisterScrapCodes")]
        public List<BlisterScrapCodeModel> Get()
        {
            var blisterScrapCode = _context.BlisterScrapCode.Include("AddedByUser").Include("ModifiedByUser").Select(s => new BlisterScrapCodeModel
            {
                ScrapCodeID = s.ScrapCodeId,
                Name = s.Name,
                Description = s.Description,
                //StatusCode=s.StatusCode.CodeValue,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedByUser = s.AddedByUser.UserName,
                ModifiedByUser = s.ModifiedByUser.UserName,
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate

            }).OrderByDescending(o => o.ScrapCodeID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<BlisterScrapCodeModel>>(BlisterScrapCode);
            return blisterScrapCode;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get BlisterScrapCode")]
        [HttpGet("GetBlisterScrapCodes/{id:int}")]
        public ActionResult<BlisterScrapCodeModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var blisterScrapCode = _context.BlisterScrapCode.SingleOrDefault(p => p.ScrapCodeId == id.Value);
            var result = _mapper.Map<BlisterScrapCodeModel>(blisterScrapCode);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<BlisterScrapCodeModel> GetData(SearchModel searchModel)
        {
            var blisterScrapCode = new BlisterScrapCode();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        blisterScrapCode = _context.BlisterScrapCode.OrderByDescending(o => o.ScrapCodeId).FirstOrDefault();
                        break;
                    case "Last":
                        blisterScrapCode = _context.BlisterScrapCode.OrderByDescending(o => o.ScrapCodeId).LastOrDefault();
                        break;
                    case "Next":
                        blisterScrapCode = _context.BlisterScrapCode.OrderByDescending(o => o.ScrapCodeId).LastOrDefault();
                        break;
                    case "Previous":
                        blisterScrapCode = _context.BlisterScrapCode.OrderByDescending(o => o.ScrapCodeId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        blisterScrapCode = _context.BlisterScrapCode.OrderByDescending(o => o.ScrapCodeId).FirstOrDefault();
                        break;
                    case "Last":
                        blisterScrapCode = _context.BlisterScrapCode.OrderByDescending(o => o.ScrapCodeId).LastOrDefault();
                        break;
                    case "Next":
                        blisterScrapCode = _context.BlisterScrapCode.OrderBy(o => o.ScrapCodeId).FirstOrDefault(s => s.ScrapCodeId > searchModel.Id);
                        break;
                    case "Previous":
                        blisterScrapCode = _context.BlisterScrapCode.OrderByDescending(o => o.ScrapCodeId).FirstOrDefault(s => s.ScrapCodeId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<BlisterScrapCodeModel>(blisterScrapCode);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertBlisterScrapCode")]
        public async Task<BlisterScrapCodeModel> Post(BlisterScrapCodeModel value)
        {
            var blisterScrapCode = new BlisterScrapCode
            {
                Name = value.Name,
                Description = value.Description,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.BlisterScrapCode.Add(blisterScrapCode);
            _context.SaveChanges();
            value.ScrapCodeID = blisterScrapCode.ScrapCodeId;


            //var model = new CRTSHF_ScrapCodes
            //{
            //    Code = value.Name,
            //    Description = value.Description
            //};
            //Context.AddToCRTSHF_ScrapCodes(model);

            //TaskFactory<DataServiceResponse> taskFactory = new TaskFactory<DataServiceResponse>();
            //var response = await taskFactory.FromAsync(Context.BeginSaveChanges(null, null), iar => Context.EndSaveChanges(iar));

            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateBlisterScrapCode")]
        public async Task<BlisterScrapCodeModel> Put(BlisterScrapCodeModel value)
        {
            var blisterScrapCode = _context.BlisterScrapCode.SingleOrDefault(p => p.ScrapCodeId == value.ScrapCodeID);

            blisterScrapCode.ModifiedByUserId = value.ModifiedByUserID;
            blisterScrapCode.ModifiedDate = DateTime.Now;
            blisterScrapCode.Name = value.Name;
            blisterScrapCode.Description = value.Description;

            blisterScrapCode.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

          


            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteBlisterScrapCode")]
        public void Delete(int id)
        {
            var blisterScrapCode = _context.BlisterScrapCode.SingleOrDefault(p => p.ScrapCodeId == id);
            if (blisterScrapCode != null)
            {
                _context.BlisterScrapCode.Remove(blisterScrapCode);
                _context.SaveChanges();
            }
        }
    }
}