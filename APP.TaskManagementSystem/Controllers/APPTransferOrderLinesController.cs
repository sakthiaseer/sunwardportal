﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NAV;
using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Threading.Tasks;
using static ConsumptionService.SWD_PostConsumption_PortClient;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class APPTransferOrderLinesController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        private readonly IHostingEnvironment _hostingEnvironment;
        //public NAV.NAV Context { get; private set; }
        private readonly IConfiguration _config;

        public APPTransferOrderLinesController(CRT_TMSContext context, IMapper mapper, IHostingEnvironment host, IConfiguration config)
        {
            _context = context;
            _mapper = mapper;

            _hostingEnvironment = host;

            _config = config;
            //Context = new NAV.NAV(new Uri($"{_config["NAV:OdataUrl"]}/Company('{_config["NAV:Company"]}')"))
            //{
            //    Credentials = new NetworkCredential(_config["NAV:UserName"], _config["NAV:Password"])
            //};

        }

        // GET: api/Project
        [HttpGet]
        [Route("GetAPPTransferOrderLiness")]
        public List<APPTransferOrderLinesModel> Get()
        {
            var transferOrderLines = _context.AppconsumptionLines.Include(a => a.AddedUser).Select(s => new APPTransferOrderLinesModel
            {
                ConsumptionLineID = s.ConsumptionLineId,
                ConsumptionEntryID = s.ConsumptionEntryId,
                ItemNo = s.ItemNo,
                Description = s.Description,
                ProductionOrderNo = s.ProductionOrderNo,
                DrumNo = s.DrumNo,
                LotNo = s.LotNo,
                SubLotNo = s.SubLotNo,
                QCRefNo = s.QcrefNo,
                BatchNo = s.BatchNo,
                ExpiryDate = s.ExpiryDate,
                Quantity = s.Quantity,
                UOM = s.Uom,
                BaseQuantity = s.BaseQuantity,
                AddedUserId = s.AddedUserId,
                AddedDate = s.AddedDate,
                AddedByUser = s.AddedUser != null ? s.AddedUser.UserName : string.Empty


            }).OrderByDescending(o => o.ConsumptionLineID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<APPTransferOrderLinesModel>>(APPTransferOrderLines);
            return transferOrderLines;
        }



        [HttpGet]
        [Route("GetConsumptionLines")]
        public List<APPTransferOrderLinesModel> GetAPPTransferOrderLines(int id)
        {
            var APPTransferOrderLines = _context.AppconsumptionLines.Include(a => a.AddedUser).Select(s => new APPTransferOrderLinesModel
            {
                ConsumptionLineID = s.ConsumptionLineId,
                ConsumptionEntryID = s.ConsumptionEntryId,
                ProductionOrderNo = s.ProductionOrderNo,
                ItemNo = s.ItemNo,
                Description = s.Description,
                DrumNo = s.DrumNo,
                LotNo = s.LotNo,
                SubLotNo = s.SubLotNo,
                QCRefNo = s.QcrefNo,
                BatchNo = s.BatchNo,
                ExpiryDate = s.ExpiryDate,
                Quantity = s.Quantity,
                UOM = s.Uom,
                BaseQuantity = s.BaseQuantity,
                PostedtoNav = s.PostedtoNav,
                AddedUserId = s.AddedUserId,
                AddedDate = s.AddedDate,
                AddedByUser = s.AddedUser != null ? s.AddedUser.UserName : string.Empty

            }).OrderByDescending(o => o.ConsumptionLineID).Where(t => t.ConsumptionEntryID == id && t.PostedtoNav == false).AsNoTracking().ToList();

            return APPTransferOrderLines;
        }

        [HttpGet]
        [Route("GetConsumptionLinesById")]
        public List<APPTransferOrderLinesModel> GetConsumptionLinesById(int id, int userId)
        {
            List<APPTransferOrderLinesModel> aPPTransferOrderLinesModels = new List<APPTransferOrderLinesModel>();
            var APPTransferOrderLines = _context.AppconsumptionLines.Include(a => a.AddedUser).OrderByDescending(o => o.ConsumptionLineId).Where(t => t.ConsumptionEntryId == id).AsNoTracking().ToList();
            APPTransferOrderLines.ForEach(s =>
            {
                APPTransferOrderLinesModel aPPTransferOrderLinesModel = new APPTransferOrderLinesModel()
                {
                    ConsumptionLineID = s.ConsumptionLineId,
                    ConsumptionEntryID = s.ConsumptionEntryId,
                    ItemNo = s.ItemNo,
                    Description = s.Description,
                    LotNo = s.LotNo,
                    SubLotNo = s.SubLotNo,
                    QCRefNo = s.QcrefNo,
                    DrumNo = s.DrumNo,
                    ProdLineNo = s.ProdLineNo.Value,
                    ProductionOrderNo = s.ProductionOrderNo,
                    BatchNo = s.BatchNo,
                    ExpiryDate = s.ExpiryDate,
                    Quantity = s.Quantity,
                    UOM = s.Uom,
                    BaseQuantity = s.BaseQuantity,
                    PostedtoNav = s.PostedtoNav,
                    AddedUserId = s.AddedUserId,
                    AddedDate = s.AddedDate,
                    AddedByUser = s.AddedUser != null ? s.AddedUser.UserName : string.Empty,
                    LineNo = takeNDigits(s.ProdLineNo.Value, 1),
                };
                aPPTransferOrderLinesModels.Add(aPPTransferOrderLinesModel);
            });

            int lineNo = takeNDigits(userId, 1);
            return aPPTransferOrderLinesModels.Where(f => f.LineNo == lineNo).ToList();
        }
        [HttpGet]
        [Route("GetConsumptionLinesItemsById")]
        public List<APPTransferOrderLinesModel> GetConsumptionLinesItemsById(long? id)
        {
            List<APPTransferOrderLinesModel> aPPTransferOrderLinesModels = new List<APPTransferOrderLinesModel>();
            var APPTransferOrderLines = _context.AppconsumptionLines.Include(a => a.AddedUser).OrderByDescending(o => o.ConsumptionLineId).Where(t => t.ConsumptionEntryId == id).AsNoTracking().ToList();
            APPTransferOrderLines.ForEach(s =>
            {
                APPTransferOrderLinesModel aPPTransferOrderLinesModel = new APPTransferOrderLinesModel()
                {
                    ConsumptionLineID = s.ConsumptionLineId,
                    ConsumptionEntryID = s.ConsumptionEntryId,
                    ItemNo = s.ItemNo,
                    Description = s.Description,
                    LotNo = s.LotNo,
                    ProductionOrderNo = s.ProductionOrderNo,
                    SubLotNo = s.SubLotNo,
                    QCRefNo = s.QcrefNo,
                    DrumNo = s.DrumNo,
                    ProdLineNo = s.ProdLineNo.Value,
                    BatchNo = s.BatchNo,
                    ExpiryDate = s.ExpiryDate,
                    Quantity = s.Quantity,
                    UOM = s.Uom,
                    BaseQuantity = s.BaseQuantity,
                    PostedtoNav = s.PostedtoNav,
                    AddedUserId = s.AddedUserId,
                    AddedDate = s.AddedDate,
                    AddedByUser = s.AddedUser != null ? s.AddedUser.UserName : string.Empty,
                };
                aPPTransferOrderLinesModels.Add(aPPTransferOrderLinesModel);
            });

            return aPPTransferOrderLinesModels.ToList();
        }
        /// <summary>
        /// Returns first part of number.
        /// </summary>
        /// <param name="number">Initial number</param>
        /// <param name="N">Amount of digits required</param>
        /// <returns>First part of number</returns>
        public static int takeNDigits(int number, int N)
        {
            // this is for handling negative numbers, we are only insterested in postitve number
            number = Math.Abs(number);

            // special case for 0 as Log of 0 would be infinity
            if (number == 0)
                return number;

            // getting number of digits on this input number
            int numberOfDigits = (int)Math.Floor(Math.Log10(number) + 1);

            // check if input number has more digits than the required get first N digits
            if (numberOfDigits >= N)
                return (int)Math.Truncate((number / Math.Pow(10, numberOfDigits - N)));
            else
                return number;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<APPTransferOrderLinesModel> GetData(SearchModel searchModel)
        {
            var transferOrderLines = new AppconsumptionLines();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        transferOrderLines = _context.AppconsumptionLines.OrderByDescending(o => o.ConsumptionLineId).FirstOrDefault();
                        break;
                    case "Last":
                        transferOrderLines = _context.AppconsumptionLines.OrderByDescending(o => o.ConsumptionLineId).LastOrDefault();
                        break;
                    case "Next":
                        transferOrderLines = _context.AppconsumptionLines.OrderByDescending(o => o.ConsumptionLineId).LastOrDefault();
                        break;
                    case "Previous":
                        transferOrderLines = _context.AppconsumptionLines.OrderByDescending(o => o.ConsumptionLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        transferOrderLines = _context.AppconsumptionLines.OrderByDescending(o => o.ConsumptionLineId).FirstOrDefault();
                        break;
                    case "Last":
                        transferOrderLines = _context.AppconsumptionLines.OrderByDescending(o => o.ConsumptionLineId).LastOrDefault();
                        break;
                    case "Next":
                        transferOrderLines = _context.AppconsumptionLines.OrderBy(o => o.ConsumptionLineId).FirstOrDefault(s => s.ConsumptionLineId > searchModel.Id);
                        break;
                    case "Previous":
                        transferOrderLines = _context.AppconsumptionLines.OrderByDescending(o => o.ConsumptionLineId).FirstOrDefault(s => s.ConsumptionLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<APPTransferOrderLinesModel>(transferOrderLines);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertConsumptionLines")]
        public async Task<APPTransferOrderLinesModel> Post(APPTransferOrderLinesModel value)
        {
            bool isExist = false;
            bool isAlreadyPosted = false;
            var consumptionLines = _context.AppconsumptionLines.Where(t => t.ProdComLine == value.ProdComLineNo && t.ProdLineNo == value.ProdLineNo && t.ConsumptionEntryId == value.ConsumptionEntryID && t.ItemNo == value.ItemNo && t.LotNo == value.LotNo && t.QcrefNo == value.QCRefNo && t.SubLotNo == value.SubLotNo).ToList();


            if (consumptionLines.Any())
            {
                if (value.DrumNo.HasValue)
                {
                    var existItem = consumptionLines.FirstOrDefault(f => f.DrumNo == value.DrumNo);
                    if (existItem == null)
                    {
                        isExist = false;
                        isAlreadyPosted = false;
                    }
                    else
                    {
                        isAlreadyPosted = existItem.PostedtoNav == true;
                        isExist = true;
                    }
                }
                else
                {
                    isExist = consumptionLines.Any();
                }
            }

            if (!isExist)
            {
                if (value.AddedByUserID == null)
                {
                    var consumptionEntry = _context.AppconsumptionEntry.FirstOrDefault(c => c.ConsumptionEntryId == value.ConsumptionEntryID);
                    value.AddedByUserID = consumptionEntry.AddedByUserId;
                }

                long companyId = 1;
                if (value.CompanyName == "NAV_SG")
                {
                    companyId = 2;
                }

                if (value.Description == null || value.Description == string.Empty)
                {
                    var navItem = _context.Navitems.FirstOrDefault(f => f.No == value.ItemNo && f.CompanyId == companyId);
                    if (navItem != null)
                    {
                        value.Description = navItem.Description;
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(value.CompanyName))
                        {
                            var context = new DataService(_config, value.CompanyName);
                            var nquery = context.Context.ItemList.Where(f => f.No == value.ItemNo);
                            DataServiceQuery<ItemList> query = (DataServiceQuery<ItemList>)nquery;

                            TaskFactory<IEnumerable<ItemList>> taskFactory = new TaskFactory<IEnumerable<ItemList>>();
                            IEnumerable<ItemList> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

                            var itemResult = result.FirstOrDefault();

                            value.Description = itemResult.Description;
                        }
                    }
                }
                var transferOrder = new AppconsumptionLines
                {
                    ConsumptionEntryId = value.ConsumptionEntryID,
                    ItemNo = value.ItemNo,
                    Description = value.Description,
                    LotNo = value.LotNo,
                    QcrefNo = value.QCRefNo,
                    SubLotNo = value.SubLotNo,
                    BatchNo = value.BatchNo,
                    ProdLineNo = value.ProdLineNo,
                    ProdComLine = value.ProdComLineNo,
                    DrumNo = value.DrumNo,
                    ProductionOrderNo = value.ProductionOrderNo,
                    //ExpiryDate = value.ExpiryDate,
                    Quantity = value.Quantity,
                    Uom = value.UOM,
                    PostedtoNav = false,
                    BaseQuantity = value.BaseQuantity,
                    AddedUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,

                };
                _context.AppconsumptionLines.Add(transferOrder);
                _context.SaveChanges();
                value.ConsumptionLineID = transferOrder.ConsumptionLineId;
                return value;
            }
            else if (isAlreadyPosted)
            {
                value.IsError = true;
                value.Errormessage = "Production order for this item, lot no, qcrefno & sub-lot no.  already posted to server.";
                return value;
            }
            else
            {
                value.IsError = true;
                value.Errormessage = "Production order for this item, lot no, qcrefno & sub-lot no. already scanned.";
                return value;
            }
        }

        [HttpPost]
        [Route("GetConsumptionLineInfo")]
        public async Task<APPTransferOrderLinesModel> GetConsumptionLineInfo(APPTransferOrderLinesModel value)
        {
            var context = new DataService(_config, value.CompanyName);
            var nquery = context.Context.PKGQtyInProd.Where(r => r.Item_No == value.ItemNo && r.QC_Ref_No == value.QCRefNo);
            DataServiceQuery<NAV.PKGQtyInProd> query = (DataServiceQuery<NAV.PKGQtyInProd>)nquery;

            TaskFactory<IEnumerable<NAV.PKGQtyInProd>> taskFactory = new TaskFactory<IEnumerable<NAV.PKGQtyInProd>>();
            IEnumerable<NAV.PKGQtyInProd> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

            var pKGQtyInProd = result.FirstOrDefault();
            APPTransferOrderLinesModel aPPTransferOrderLinesModel = null;
            if (pKGQtyInProd != null)
            {
                long companyId = 1;
                if (value.CompanyName == "NAV_SG")
                {
                    companyId = 2;
                }
                var navItem = _context.Navitems.FirstOrDefault(f => f.No == value.ItemNo && f.CompanyId == companyId);
                if (navItem != null)
                {
                    value.Description = navItem.Description;
                }
                aPPTransferOrderLinesModel = new APPTransferOrderLinesModel
                {
                    UOM = pKGQtyInProd.Unit_of_Measure_Code,
                    BatchNo = pKGQtyInProd.Batch_No,
                    ConsumptionEntryID = value.ConsumptionEntryID,
                    ProductionOrderNo = value.ProductionOrderNo,
                    ItemNo = value.ItemNo,
                    LineNo = value.LineNo,
                    LotNo = pKGQtyInProd.Lot_No,
                    SubLotNo = pKGQtyInProd.Lot_No,
                    ProdLineNo = value.LineNo,
                    ProdComLineNo = value.LineNo,
                    QCRefNo = pKGQtyInProd.QC_Ref_No,
                    Description = value.Description,
                    DrumNo = value.DrumNo
                };
            }

            return aPPTransferOrderLinesModel;
        }

        [HttpPost]
        [Route("GetQCRefItems")]
        public async Task<List<APPTransferOrderLinesModel>> GetQCRefItems(APPTransferOrderLinesModel value)
        {
            var context = new DataService(_config, value.CompanyName);
            var nquery = context.Context.PKGQtyInProd.Where(r => r.Item_No == value.ItemNo && r.Lot_No == value.LotNo);
            DataServiceQuery<NAV.PKGQtyInProd> query = (DataServiceQuery<NAV.PKGQtyInProd>)nquery;
            List<APPTransferOrderLinesModel> aPPTransferOrderLinesModels = new List<APPTransferOrderLinesModel>();
            TaskFactory<IEnumerable<NAV.PKGQtyInProd>> taskFactory = new TaskFactory<IEnumerable<NAV.PKGQtyInProd>>();
            IEnumerable<NAV.PKGQtyInProd> result = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));

            var pKGQtyInProd = result.ToList();
            APPTransferOrderLinesModel aPPTransferOrderLinesModel = null;
            if (pKGQtyInProd != null && pKGQtyInProd.Count > 0)
            {
                long companyId = 1;
                if (value.CompanyName == "NAV_SG")
                {
                    companyId = 2;
                }
                var navItem = _context.Navitems.FirstOrDefault(f => f.No == value.ItemNo && f.CompanyId == companyId);
                if (navItem != null)
                {
                    value.Description = navItem.Description;
                }
                var qcRefGroupItems = pKGQtyInProd.GroupBy(s => s.QC_Ref_No).ToList();
                if (qcRefGroupItems.Any())
                {
                    qcRefGroupItems.ForEach(item =>
                    {
                        aPPTransferOrderLinesModel = new APPTransferOrderLinesModel
                        {
                            UOM = item.FirstOrDefault().Unit_of_Measure_Code,
                            BatchNo = item.FirstOrDefault().Batch_No,
                            ConsumptionEntryID = value.ConsumptionEntryID,
                            ProductionOrderNo = value.ProductionOrderNo,
                            ItemNo = item.FirstOrDefault().Item_No,
                            LineNo = value.LineNo,
                            LotNo = item.FirstOrDefault().Lot_No,
                            SubLotNo = item.FirstOrDefault().Lot_No,
                            ProdLineNo = value.LineNo,
                            ProdComLineNo = value.LineNo,
                            QCRefNo = item.Key,
                            Description = value.Description,
                            Quantity = item.ToList().Sum(q => q.Remaining_Quantity),
                        };
                        aPPTransferOrderLinesModels.Add(
                            aPPTransferOrderLinesModel
                        );
                    });
                }

            }

            return aPPTransferOrderLinesModels;
        }


        [HttpPost]
        [Route("InsertWithInputConsumptionLine")]
        public async Task<APPTransferOrderLinesModel> InsertWithInputConsumptionLine(APPTransferOrderLinesModel value)
        {
            var conusumptionlines = _context.AppconsumptionLines.Where(t => t.ProdLineNo == value.ProdLineNo && t.ConsumptionEntryId == value.ConsumptionEntryID && t.ItemNo == value.ItemNo && t.LotNo == value.LotNo && t.QcrefNo == value.QCRefNo && t.SubLotNo == value.SubLotNo);
            bool isLessQty = false;
            if (conusumptionlines.Any())
            {
                var quantity = conusumptionlines.Sum(q => q.Quantity);
                var diffQuantity = value.AvailableQuantity - quantity;
                isLessQty = diffQuantity > value.Quantity;
                if (diffQuantity != 0)
                {
                    if (isLessQty)
                    {
                        InsertLine(value);
                    }
                    else if (!isLessQty)
                    {
                        value.IsError = true;
                        value.Errormessage = "Production order for this item quantity greater than = " + value.AvailableQuantity;
                    }
                }
                else
                {
                    value.IsError = true;
                    value.Errormessage = "Production order for this item, qcrefno & prod line no. already scanned.";
                }
            }
            else
            {
                InsertLine(value);
            }
            return value;

        }

        private void InsertLine(APPTransferOrderLinesModel value)
        {
            if (value.AddedByUserID == null)
            {
                var consumptionEntry = _context.AppconsumptionEntry.FirstOrDefault(c => c.ConsumptionEntryId == value.ConsumptionEntryID);
                value.AddedByUserID = consumptionEntry.AddedByUserId;
            }

            long companyId = 1;
            if (value.CompanyName == "NAV_SG")
            {
                companyId = 2;
            }
            var navItem = _context.Navitems.FirstOrDefault(f => f.No == value.ItemNo && f.CompanyId == companyId);
            if (navItem != null)
            {
                value.Description = navItem.Description;
            }
            var transferOrder = new AppconsumptionLines
            {
                ConsumptionEntryId = value.ConsumptionEntryID,
                ItemNo = value.ItemNo,
                Description = value.Description,
                LotNo = value.LotNo,
                QcrefNo = value.QCRefNo,
                SubLotNo = value.SubLotNo,
                BatchNo = value.BatchNo,
                ProdLineNo = value.ProdLineNo,
                ProdComLine = value.ProdComLineNo,
                DrumNo = value.DrumNo,
                ProductionOrderNo = value.ProductionOrderNo,
                IsFullConsume = value.IsFullConsume,
                //ExpiryDate = value.ExpiryDate,
                Quantity = value.Quantity,
                Uom = value.UOM,
                PostedtoNav = false,
                BaseQuantity = value.BaseQuantity,
                AddedUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,

            };
            _context.AppconsumptionLines.Add(transferOrder);
            _context.SaveChanges();
            value.ConsumptionLineID = transferOrder.ConsumptionLineId;
        }

        [HttpPost]
        [Route("PostConsumption")]
        public async Task<APPTransferOrderModel> PostConsumption(APPTransferOrderModel value)
        {
            List<long> addedSuccess = new List<long>();
            List<AppconsumptionLines> transferOrderLines = null;
            try
            {
                var context = new DataService(_config, value.CompanyName);
                var guid = Guid.NewGuid();

                var appuser = _context.ApplicationUser.FirstOrDefault(u => u.UserId == value.AddedByUserID);
                if (appuser == null)
                {
                    value.IsError = true;
                    value.Errormessage = "APP user session expired. Please login again.";
                    return value;
                }
                value.AddedByUser = appuser.LoginId;


                transferOrderLines = _context.AppconsumptionLines.Where(t => t.ConsumptionEntryId == value.ConsumptionEntryID && t.PostedtoNav == false).ToList();
                foreach (var cons in transferOrderLines)
                {
                    int onlyThisAmount = 8;
                    string ticks = DateTime.Now.Ticks.ToString();
                    ticks = ticks.Substring(ticks.Length - onlyThisAmount);
                    int entryNumber = int.Parse(ticks);
                    var consumption = new SWD_ConsumptionEntries()
                    {
                        //Batch_No = cons.BatchNo,
                        Item_No = cons.ItemNo,
                        Line_No = cons.ProdLineNo,
                        Lot_No = cons.LotNo,
                        Posting_Date = DateTime.Now,
                        QC_Ref_No = cons.QcrefNo,
                        QC_Status = "Approved",
                        Unit_of_Measure_Code = cons.Uom,
                        Quantity = cons.Quantity,
                        Location_Code = value.TransferTo,
                        Production_Order_No = cons.ProductionOrderNo == string.Empty ? value.ProdOrderNo : cons.ProductionOrderNo,
                        Entry_No = entryNumber,
                        Session_User_ID = value.AddedByUser,
                        Session_Type = "Mobile",
                        Prod_Order_Comp_Line_No = cons.ProdComLine,
                    };
                    context.Context.AddToSWD_ConsumptionEntries(consumption);

                    var entity = context.Context.Entities;
                    TaskFactory<DataServiceResponse> taskFactory = new TaskFactory<DataServiceResponse>();
                    var response = await taskFactory.FromAsync(context.Context.BeginSaveChanges(null, null), iar => context.Context.EndSaveChanges(iar));

                    var post = new ConsumptionService.SWD_PostConsumption_PortClient();
                    //post.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    post.Endpoint.Address =
               new EndpointAddress(new Uri(_config[value.CompanyName + ":SoapUrl"] + "/" + _config[value.CompanyName + ":Company"] + "/Codeunit/SWD_PostConsumption"),
               new DnsEndpointIdentity(""));

                    post.ClientCredentials.UserName.UserName = _config[value.CompanyName + ":UserName"];
                    post.ClientCredentials.UserName.Password = _config[value.CompanyName + ":Password"];
                    post.ClientCredentials.Windows.ClientCredential.UserName = _config[value.CompanyName + ":UserName"]; ;
                    post.ClientCredentials.Windows.ClientCredential.Password = _config[value.CompanyName + ":Password"];
                    post.ClientCredentials.Windows.ClientCredential.Domain = _config[value.CompanyName + ":Domain"];
                    post.ClientCredentials.Windows.AllowedImpersonationLevel =
                  System.Security.Principal.TokenImpersonationLevel.Impersonation;


                    var fnobject = Guid.NewGuid().ToString();
                    await post.FnPostConsumptionAsync(entryNumber);

                    value.PostedtoNAV = true;
                    cons.PostedtoNav = true;
                    _context.SaveChanges();
                    addedSuccess.Add(cons.ConsumptionLineId);
                }
                return value;
            }
            catch (Exception ex)
            {
                var ids = transferOrderLines.Where(i => !addedSuccess.Contains(i.ConsumptionLineId)).Select(s => s.ConsumptionLineId).ToList();
                DeleteByIds(ids);
                value.PostedtoNAV = false;
                value.Errormessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                value.IsError = true;
                return value;
            }


        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateAPPTransferOrderLines")]
        public APPTransferOrderLinesModel Put(APPTransferOrderLinesModel value)
        {
            var transferOrder = _context.AppconsumptionLines.SingleOrDefault(p => p.ConsumptionLineId == value.ConsumptionLineID);

            _context.SaveChanges();
            transferOrder.ConsumptionEntryId = value.ConsumptionEntryID;
            transferOrder.ItemNo = value.ItemNo;
            transferOrder.Description = value.Description;
            transferOrder.LotNo = value.LotNo;
            transferOrder.SubLotNo = value.SubLotNo;
            transferOrder.QcrefNo = value.QCRefNo;
            transferOrder.BatchNo = value.BatchNo;
            transferOrder.ProductionOrderNo = value.ProductionOrderNo;
            transferOrder.ExpiryDate = value.ExpiryDate;
            transferOrder.Quantity = value.Quantity;
            transferOrder.Uom = value.UOM;
            transferOrder.BaseQuantity = value.BaseQuantity;
            transferOrder.BaseUom = value.BaseUOM;
            transferOrder.AddedDate = DateTime.Now;
            transferOrder.AddedUserId = value.AddedUserId;
            transferOrder.ProductionOrderNo = value.ProductionOrderNo;
            transferOrder.DrumNo = value.DrumNo;
            transferOrder.IsFullConsume = value.IsFullConsume;

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteConsumptionLines")]
        public void Delete(int id)
        {
            var transferOrder = _context.AppconsumptionLines.SingleOrDefault(p => p.ConsumptionLineId == id);
            var errorMessage = "";
            try
            {
                if (transferOrder != null)
                {
                    _context.AppconsumptionLines.Remove(transferOrder);
                    _context.SaveChanges();
                }
            }

            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "There is a transaction this record cannot be deleted!";
                }
                throw new AppException(errorMessage, ex);
            }
        }

        private void DeleteByIds(List<long> ids)
        {
            var transferOrder = _context.AppconsumptionLines.Where(p => ids.Contains(p.ConsumptionLineId));
            var errorMessage = "";
            try
            {
                if (transferOrder != null)
                {
                    _context.AppconsumptionLines.RemoveRange(transferOrder);
                    _context.SaveChanges();
                }
            }

            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "There is a transaction this record cannot be deleted!";
                }
                throw new AppException(errorMessage, ex);
            }
        }
    }
}