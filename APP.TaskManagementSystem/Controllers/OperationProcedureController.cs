﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class OperationProcedureController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public OperationProcedureController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;

        }
        [HttpGet]
        [Route("GetOperationProcedure")]
        public List<OperationProcedureModel> Get()
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var OperationProcedureDosagelist = _context.OperationProcedureDosage.AsNoTracking().ToList();
            List<OperationProcedureModel> operationProcedureModels = new List<OperationProcedureModel>();
            var operationProcedure = _context.OperationProcedure
                .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode").OrderByDescending(o => o.OperationProcedureId).AsNoTracking().ToList();
            if (operationProcedure != null && operationProcedure.Count > 0)
            {
                operationProcedure.ForEach(s =>
                {
                    OperationProcedureModel operationProcedureModel = new OperationProcedureModel
                    {
                        OperationProcedureId = s.OperationProcedureId,
                        DosageFormId = OperationProcedureDosagelist.Where(d => d.OperationProcedureId == s.OperationProcedureId).Select(d => d.DosageFormId).ToList(),
                        ManufacturingProcessId = s.ManufacturingProcessId,
                        // DosageFormName = s.DosageFormId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.DosageFormId).Select(m => m.Value).FirstOrDefault() : "",
                        ManufacturingProcessName = s.ManufacturingProcessId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.ManufacturingProcessId).Select(m => m.Value).FirstOrDefault() : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    };
                    operationProcedureModels.Add(operationProcedureModel);
                });

            }
            return operationProcedureModels;
        }

        [HttpGet]
        [Route("GetOperationProcedureItems")]
        public List<OperationProcedureItem> GetOperationProcedureItems()
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<OperationProcedureItem> operationProcedureItems = new List<OperationProcedureItem>();
            var operationProcedureLine = _context.OperationProcedureLine.Include(o => o.OperationProcedure).OrderByDescending(o => o.OperationProcedureLineId).AsNoTracking().ToList();
            if (operationProcedureLine != null && operationProcedureLine.Count > 0)
            {
                operationProcedureLine.ForEach(s =>
                {
                    var DosageFormName = s.OperationProcedure.DosageFormId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.OperationProcedure.DosageFormId).Select(m => m.Value).FirstOrDefault() : "";
                    var ManufacturingProcessName = s.OperationProcedure.ManufacturingProcessId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.OperationProcedure.ManufacturingProcessId).Select(m => m.Value).FirstOrDefault() : "";

                    OperationProcedureItem operationProcedureItem = new OperationProcedureItem
                    {
                        Id = s.OperationProcedureLineId,
                        ParentId = s.OperationProcedureId,
                        Name = DosageFormName + "|" + ManufacturingProcessName + "|" + (s.OprocedureId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.OprocedureId).Select(m => m.Value).FirstOrDefault() : ""),
                    };
                    operationProcedureItems.Add(operationProcedureItem);
                });
            }
            return operationProcedureItems;
        }

        [HttpGet]
        [Route("GetOperationProcedureLine")]
        public List<OperationProcedureLineModel> OperationProcedureLine(int id)
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            List<OperationProcedureLineModel> operationProcedureLineModels = new List<OperationProcedureLineModel>();
            var operationProcedureLine = _context.OperationProcedureLine.Where(w => w.OperationProcedureId == id).OrderByDescending(o => o.OperationProcedureLineId).AsNoTracking().ToList();
            if (operationProcedureLine != null && operationProcedureLine.Count > 0)
            {
                operationProcedureLine.ForEach(s =>
                {

                    OperationProcedureLineModel operationProcedureLineModel = new OperationProcedureLineModel
                    {
                        OperationProcedureLineId = s.OperationProcedureLineId,
                        OperationProcedureId = s.OperationProcedureId,
                        OprocedureId = s.OprocedureId,
                        Description = s.Description,
                        WILink = s.Wilink,
                        IsUpdateNavision = s.IsUpdateNavision,
                        SessionId=s.SessionId,
                        OprocedureName = s.OprocedureId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.OprocedureId).Select(m => m.Value).FirstOrDefault() : "",
                    };
                    operationProcedureLineModels.Add(operationProcedureLineModel);
                });
            }
            return operationProcedureLineModels;
        }
        [HttpPost]
        [Route("InsertOperationProcedure")]
        public OperationProcedureModel InsertOperationProcedure(OperationProcedureModel value)
        {
            var OperationProcedure = new OperationProcedure
            {
                ManufacturingProcessId = value.ManufacturingProcessId,
                AddedByUserId = value.AddedByUserID,
                StatusCodeId = value.StatusCodeID.Value,
                AddedDate = DateTime.Now,
            };
            _context.OperationProcedure.Add(OperationProcedure);
            if (value.DosageFormId != null)
            {
                value.DosageFormId.ForEach(c =>
                {
                    var DosageForm = new OperationProcedureDosage
                    {
                        DosageFormId = c,
                        OperationProcedureId = value.OperationProcedureId,
                    };
                    OperationProcedure.OperationProcedureDosage.Add(DosageForm);
                });
            }
            _context.SaveChanges();
            return value;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<OperationProcedureModel> GetData(SearchModel searchModel)
        {
            var OperationProcedure = new OperationProcedure();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        OperationProcedure = _context.OperationProcedure.OrderByDescending(o => o.OperationProcedureId).FirstOrDefault();
                        break;
                    case "Last":
                        OperationProcedure = _context.OperationProcedure.OrderByDescending(o => o.OperationProcedureId).LastOrDefault();
                        break;
                    case "Next":
                        OperationProcedure = _context.OperationProcedure.OrderByDescending(o => o.OperationProcedureId).LastOrDefault();
                        break;
                    case "Previous":
                        OperationProcedure = _context.OperationProcedure.OrderByDescending(o => o.OperationProcedureId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        OperationProcedure = _context.OperationProcedure.OrderByDescending(o => o.OperationProcedureId).FirstOrDefault();
                        break;
                    case "Last":
                        OperationProcedure = _context.OperationProcedure.OrderByDescending(o => o.OperationProcedureId).LastOrDefault();
                        break;
                    case "Next":
                        OperationProcedure = _context.OperationProcedure.OrderBy(o => o.OperationProcedureId).FirstOrDefault(s => s.OperationProcedureId > searchModel.Id);
                        break;
                    case "Previous":
                        OperationProcedure = _context.OperationProcedure.OrderByDescending(o => o.OperationProcedureId).FirstOrDefault(s => s.OperationProcedureId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<OperationProcedureModel>(OperationProcedure);
            return result;
        }
        [HttpPost]
        [Route("InsertOperationProcedureLine")]
        public OperationProcedureLineModel InsertOperationProcedureLine(OperationProcedureLineModel value)
        {
            var sessionId = Guid.NewGuid();
            var OperationProcedureLine = new OperationProcedureLine
            {
                OperationProcedureId = value.OperationProcedureId,
                OprocedureId = value.OprocedureId,
                Description = value.Description,
                Wilink = value.WILink,
                IsUpdateNavision = value.IsUpdateNavision,
                SessionId = sessionId,
            };
            _context.OperationProcedureLine.Add(OperationProcedureLine);
            _context.SaveChanges();
            value.OperationProcedureLineId = value.OperationProcedureLineId;
            value.SessionId = sessionId;
            return value;
        }
        [HttpPut]
        [Route("UpdateOperationProcedure")]
        public OperationProcedureModel Put(OperationProcedureModel value)
        {
            var OperationProcedure = _context.OperationProcedure.SingleOrDefault(p => p.OperationProcedureId == value.OperationProcedureId);
            OperationProcedure.ManufacturingProcessId = value.ManufacturingProcessId;
            OperationProcedure.ModifiedByUserId = value.ModifiedByUserID;
            OperationProcedure.StatusCodeId = value.StatusCodeID.Value;
            OperationProcedure.ModifiedDate = DateTime.Now;
            var OperationProcedureDosagedata = _context.OperationProcedureDosage.Where(l => l.OperationProcedureId == value.OperationProcedureId).ToList();
            if (OperationProcedureDosagedata.Count > 0)
            {
                _context.OperationProcedureDosage.RemoveRange(OperationProcedureDosagedata);
                _context.SaveChanges();
            }
            if (value.DosageFormId != null)
            {
                value.DosageFormId.ForEach(c =>
                {
                    var DosageForm = new OperationProcedureDosage
                    {
                        DosageFormId = c,
                        OperationProcedureId = value.OperationProcedureId
                    };
                    OperationProcedure.OperationProcedureDosage.Add(DosageForm);
                });
            }
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateOperationProcedureLine")]
        public OperationProcedureLineModel UpdateOperationProcedureLine(OperationProcedureLineModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            var OperationProcedureLine = _context.OperationProcedureLine.SingleOrDefault(p => p.OperationProcedureLineId == value.OperationProcedureLineId);
            OperationProcedureLine.OperationProcedureId = value.OperationProcedureId;
            OperationProcedureLine.OprocedureId = value.OprocedureId;
            OperationProcedureLine.Description = value.Description;
            OperationProcedureLine.Wilink = value.WILink;
            OperationProcedureLine.IsUpdateNavision = value.IsUpdateNavision;
            OperationProcedureLine.SessionId = value.SessionId;
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteOperationProcedure")]
        public ActionResult<string> DeleteOperationProcedure(int id)
        {
            try
            {
                var OperationProcedure = _context.OperationProcedure.Where(p => p.OperationProcedureId == id).FirstOrDefault();
                if (OperationProcedure != null)
                {
                    var OperationProcedureDosagedata = _context.OperationProcedureDosage.Where(l => l.OperationProcedureId == id).AsNoTracking().ToList();
                    if (OperationProcedureDosagedata.Count > 0)
                    {
                        _context.OperationProcedureDosage.RemoveRange(OperationProcedureDosagedata);
                        _context.SaveChanges();
                    }
                    var OperationProcedureLine = _context.OperationProcedureLine.Where(p => p.OperationProcedureId == id).AsNoTracking().ToList();
                    if (OperationProcedureLine != null)
                    {
                        _context.OperationProcedureLine.RemoveRange(OperationProcedureLine);
                        _context.SaveChanges();
                    }
                    _context.OperationProcedure.Remove(OperationProcedure);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("DeleteOperationProcedureLine")]
        public ActionResult<string> DeleteOperationProcedureLine(int id)
        {
            try
            {
                var OperationProcedureLine = _context.OperationProcedureLine.Where(p => p.OperationProcedureLineId == id).FirstOrDefault();
                if (OperationProcedureLine != null)
                {
                    _context.OperationProcedureLine.Remove(OperationProcedureLine);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}