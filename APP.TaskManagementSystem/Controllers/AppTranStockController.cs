﻿using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NAV;
using StockAdjustmentService;
using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class AppTranStockController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;
        public AppTranStockController(CRT_TMSContext context, IMapper mapper, IConfiguration config)
        {
            _context = context;
            _mapper = mapper;
            _config = config;
        }

        #region AppTranStock

        // GET: api/Project
        [HttpGet]
        [Route("GetAppTranStocks")]
        public List<AppTranStockModel> Get()
        {
            List<AppTranStockModel> appTranStockModels = new List<AppTranStockModel>();
            var appTranStocks = _context.AppTranStock.Include(s => s.AddedByUser).Include("ModifiedByUser").OrderByDescending(o => o.Id).AsNoTracking().ToList();

            appTranStocks.ForEach(s =>
            {
                AppTranStockModel appTranStockModel = new AppTranStockModel()
                {
                    Id = s.Id,
                    LocationFrom = s.LocationFrom,
                    LocationTo = s.LocationTo,
                    RequisitionNo = s.RequisitionNo,
                    TransferNo = s.TransferNo,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser?.UserName,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,

                };
                appTranStockModels.Add(appTranStockModel);
            });

            return appTranStockModels;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<AppTranStockModel> GetData(SearchModel searchModel)
        {
            var appTranStock = new AppTranStock();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        appTranStock = _context.AppTranStock.OrderByDescending(o => o.Id).FirstOrDefault();
                        break;
                    case "Last":
                        appTranStock = _context.AppTranStock.OrderByDescending(o => o.Id).LastOrDefault();
                        break;
                    case "Next":
                        appTranStock = _context.AppTranStock.OrderByDescending(o => o.Id).LastOrDefault();
                        break;
                    case "Previous":
                        appTranStock = _context.AppTranStock.OrderByDescending(o => o.Id).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        appTranStock = _context.AppTranStock.OrderByDescending(o => o.Id).FirstOrDefault();
                        break;
                    case "Last":
                        appTranStock = _context.AppTranStock.OrderByDescending(o => o.Id).LastOrDefault();
                        break;
                    case "Next":
                        appTranStock = _context.AppTranStock.OrderBy(o => o.Id).FirstOrDefault(s => s.Id > searchModel.Id);
                        break;
                    case "Previous":
                        appTranStock = _context.AppTranStock.OrderByDescending(o => o.Id).FirstOrDefault(s => s.Id < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<AppTranStockModel>(appTranStock);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertAppTranStock")]
        public async Task<AppTranStockModel> Post(AppTranStockModel value)
        {
            AppTranStock apptranstock = _context.AppTranStock.FirstOrDefault(p => p.LocationFrom == value.LocationFrom && p.LocationTo == value.LocationTo && p.RequisitionNo == value.RequisitionNo);
            if (apptranstock == null)
            {
                apptranstock = new AppTranStock
                {
                    LocationFrom = value.LocationFrom,
                    LocationTo = value.LocationTo,
                    RequisitionNo = value.RequisitionNo,
                    TransferNo=value.TransferNo,
                    StatusCodeId = 1,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                };
                _context.AppTranStock.Add(apptranstock);
            }
            else
            {
                apptranstock.LocationFrom = value.LocationFrom;
                apptranstock.LocationTo = value.LocationTo;
                apptranstock.TransferNo = value.TransferNo;
                apptranstock.RequisitionNo = value.RequisitionNo;
                apptranstock.ModifiedByUserId = value.AddedByUserID;
                apptranstock.ModifiedDate = DateTime.Now;
                apptranstock.StatusCodeId = 1;
            }
            _context.SaveChanges();

            value.Id = apptranstock.Id;
            return value;
        }

        [HttpPost]
        [Route("PostStockToNav")]
        public async Task<AppTranStockModel> PostStockToNav(AppTranStockModel value)
        {
            try
            {
                var locationItem = await _context.AppTranStock.Include(c => c.AppTranStockLine).FirstOrDefaultAsync(t => t.Id == value.Id);
                if (locationItem != null)
                {
                    var context = new DataService(_config, value.CompanyName);
                    var guid = Guid.NewGuid();
                    int onlyThisAmount = 8;
                    string ticks = DateTime.Now.Ticks.ToString();
                    ticks = ticks.Substring(ticks.Length - onlyThisAmount);
                    int entryNumber = int.Parse(ticks);
                    locationItem.AppTranStockLine.ToList().ForEach(cons =>
                    {

                        // SW Reference
                        var consumption = new SWDWebIntegrationEntry()
                        {
                            Entry_No = entryNumber,
                            Created_by = value.AddedByUser,
                            Entry_Type = "Create",
                            Item_No = cons.ItemNo,
                            Created_on = DateTime.Now,
                            Lot_No = cons.LotNo,
                            Posted_on = DateTime.Now,
                            Posting_Date = DateTime.Now,
                            // Posted_by = value.AddedByUser,
                            // Session_ID = guid,
                           // Transfer_From_Code = locationItem.LocationFrom,
                            //Transfer_To_Code = locationItem.LocationTo,
                            Quantity = cons.Qty,

                            // Session_User_ID = value.AddedByUser,
                        };
                        context.Context.AddToSWDWebIntegrationEntry(consumption);

                    });
                    TaskFactory<DataServiceResponse> taskFactory = new TaskFactory<DataServiceResponse>();
                    var response = await taskFactory.FromAsync(context.Context.BeginSaveChanges(null, null), iar => context.Context.EndSaveChanges(iar));

                    var post = new SWDWebIntegration_PortClient();

                    post.Endpoint.Address =
               new EndpointAddress(new Uri(_config[value.CompanyName + ":SoapUrl"] + "/" + _config[value.CompanyName + ":Company"] + "/Codeunit/SWDWebIntegration"),
               new DnsEndpointIdentity(""));

                    post.ClientCredentials.UserName.UserName = _config[value.CompanyName + ":UserName"];
                    post.ClientCredentials.UserName.Password = _config[value.CompanyName + ":Password"];
                    post.ClientCredentials.Windows.ClientCredential.UserName = _config[value.CompanyName + ":UserName"]; ;
                    post.ClientCredentials.Windows.ClientCredential.Password = _config[value.CompanyName + ":Password"];
                    post.ClientCredentials.Windows.ClientCredential.Domain = _config[value.CompanyName + ":Domain"];
                    post.ClientCredentials.Windows.AllowedImpersonationLevel =
                  System.Security.Principal.TokenImpersonationLevel.Impersonation;

                    var transferNo = await post.FnCreateTransOrderAsync(entryNumber);
                    locationItem.TransferNo = transferNo.return_value;
                    _context.SaveChanges();
                    value.TransferNo= transferNo.return_value;

                }
            }
            catch (Exception ex)
            {

                value.Errormessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                value.IsError = true;

            }
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateAppTranStock")]
        public AppTranStockModel Put(AppTranStockModel value)
        {
            var appTranStock = _context.AppTranStock.SingleOrDefault(p => p.Id == value.Id);
            appTranStock.LocationFrom = value.LocationFrom;
            appTranStock.LocationTo = value.LocationTo;
            appTranStock.RequisitionNo = value.RequisitionNo;
            appTranStock.ModifiedByUserId = value.ModifiedByUserID;
            appTranStock.ModifiedDate = DateTime.Now;
            appTranStock.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteAppTranStock")]
        public void Delete(int id)
        {
            var appTranStock = _context.AppTranStock.SingleOrDefault(p => p.Id == id);
            var errorMessage = "";
            try
            {
                if (appTranStock != null)
                {
                    _context.AppTranStock.Remove(appTranStock);
                    _context.SaveChanges();
                }
            }

            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "There is a transaction this record cannot be deleted!";
                }
                throw new AppException(errorMessage, ex);
            }
        }

        #endregion

        #region AppTranStockLine

        // GET: api/Project
        [HttpGet]
        [Route("GetAPPTranStockLines")]
        public List<AppTranStockLineModel> GetAPPTranStockLines()
        {
            var appTranStockLines = _context.AppTranStockLine.Include(a => a.AddedByUser).Select(s => new AppTranStockLineModel
            {
                Id = s.Id,
                AppTranStockId = s.AppTranStockId,
                ScanActionStatus = s.ScanActionStatus,
                ActionStatus = s.ActionStatus,
                DocRequisitionNo = s.DocRequisitionNo,
                LabelNo = s.LabelNo,
                ItemNo = s.ItemNo,
                LotNo = s.LotNo,
                StatusCodeID = s.StatusCodeId,
                AddedByUserID = s.AddedByUserId,
                AddedDate = s.AddedDate,
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : string.Empty
            }).OrderByDescending(o => o.Id).AsNoTracking().ToList();
            return appTranStockLines;
        }



        [HttpGet]
        [Route("GetAPPTranStockLinesById")]
        public List<AppTranStockLineModel> GetAPPTranStockLinesById(int id)
        {
            List<AppTranStockLineModel> stocklineModels = new List<AppTranStockLineModel>();
            var stocklines = _context.AppTranStockLine.Include(a => a.AddedByUser).AsNoTracking().Where(t => t.AppTranStockId == id).ToList();
            stocklines.ForEach(s =>
            {
                var stockLineModel = new AppTranStockLineModel
                {
                    Id = s.Id,
                    AppTranStockId = s.AppTranStockId,
                    ScanActionStatus = s.ScanActionStatus,
                    ActionStatus = s.ActionStatus,
                    DocRequisitionNo = s.DocRequisitionNo,
                    LabelNo = s.LabelNo,
                    ItemNo = s.ItemNo,
                    LotNo = s.LotNo,
                    Qty = s.Qty,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    AddedDate = s.AddedDate,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : string.Empty

                };
                stocklineModels.Add(stockLineModel);
            });


            return stocklineModels.OrderByDescending(o => o.Id).ToList();
        }


        [HttpPost()]
        [Route("GetDataLine")]
        public ActionResult<AppTranStockLineModel> GetDataLine(SearchModel searchModel)
        {
            var appTranStockLine = new AppTranStockLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        appTranStockLine = _context.AppTranStockLine.OrderByDescending(o => o.Id).FirstOrDefault();
                        break;
                    case "Last":
                        appTranStockLine = _context.AppTranStockLine.OrderByDescending(o => o.Id).LastOrDefault();
                        break;
                    case "Next":
                        appTranStockLine = _context.AppTranStockLine.OrderByDescending(o => o.Id).LastOrDefault();
                        break;
                    case "Previous":
                        appTranStockLine = _context.AppTranStockLine.OrderByDescending(o => o.Id).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        appTranStockLine = _context.AppTranStockLine.OrderByDescending(o => o.Id).FirstOrDefault();
                        break;
                    case "Last":
                        appTranStockLine = _context.AppTranStockLine.OrderByDescending(o => o.Id).LastOrDefault();
                        break;
                    case "Next":
                        appTranStockLine = _context.AppTranStockLine.OrderBy(o => o.Id).FirstOrDefault(s => s.Id > searchModel.Id);
                        break;
                    case "Previous":
                        appTranStockLine = _context.AppTranStockLine.OrderByDescending(o => o.Id).FirstOrDefault(s => s.Id < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<AppTranStockLineModel>(appTranStockLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertAppTranStockLine")]
        public async Task<AppTranStockLineModel> Post(AppTranStockLineModel value)
        {
            var locationItem = _context.AppTranLfromLto.FirstOrDefault(t => t.RequisitionNo == value.DocRequisitionNo);
            if (locationItem != null)
            {
                var locationLineItem = _context.AppTranLfromLtoLine.FirstOrDefault(t => t.ItemNo == value.ItemNo && t.AppTranLfromLtoId == locationItem.Id);
                var lineItemsCount = _context.AppTranStockLine.Count(t => t.DocRequisitionNo == value.DocRequisitionNo && t.ItemNo == value.ItemNo);
                if (locationLineItem != null)
                {
                    if (lineItemsCount <= locationLineItem.TotalLableQty)
                    {
                        var appTranStockLine = new AppTranStockLine
                        {
                            AppTranStockId = value.AppTranStockId,
                            ScanActionStatus = value.ScanActionStatus,
                            ActionStatus = value.ActionStatus,
                            DocRequisitionNo = value.DocRequisitionNo,
                            LabelNo = value.LabelNo,
                            ItemNo = value.ItemNo,
                            LotNo = value.LotNo,
                            Qty = value.Qty,
                            AddedByUserId = value.AddedByUserID,
                            StatusCodeId = 1,
                            AddedDate = DateTime.Now,

                        };
                        _context.AppTranStockLine.Add(appTranStockLine);
                        _context.SaveChanges();
                        value.Id = appTranStockLine.Id;
                        return value;
                    }
                    else
                    {
                        value.IsError = true;
                        value.Errormessage = "Item Label Qty should be less than Location TotalLabelQty!!!.";
                        return value;
                    }
                }
                else
                {
                    value.IsError = true;
                    value.Errormessage = "Requisition No not available!!!.";
                    return value;
                }

            }
            else
            {
                value.IsError = true;
                value.Errormessage = "Requisition No not available!!!.";
                return value;
            }
        }



        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTranStockLine")]
        public AppTranStockLineModel Put(AppTranStockLineModel value)
        {
            var apptramstockline = _context.AppTranStockLine.SingleOrDefault(p => p.Id == value.Id);
            apptramstockline.AppTranStockId = value.AppTranStockId;
            apptramstockline.ScanActionStatus = value.ScanActionStatus;
            apptramstockline.ActionStatus = value.ActionStatus;
            apptramstockline.DocRequisitionNo = value.DocRequisitionNo;
            apptramstockline.LabelNo = value.LabelNo;
            apptramstockline.ItemNo = value.ItemNo;
            apptramstockline.Qty = value.Qty;
            apptramstockline.LotNo = value.LotNo;
            apptramstockline.ModifiedDate = DateTime.Now;
            apptramstockline.ModifiedByUserId = value.ModifiedByUserID;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteTranStockLine")]
        public void DeleteTranStockLine(int id)
        {
            var appTranStockLine = _context.AppTranStockLine.SingleOrDefault(p => p.Id == id);
            var errorMessage = "";
            try
            {
                if (appTranStockLine != null)
                {
                    _context.AppTranStockLine.Remove(appTranStockLine);
                    _context.SaveChanges();
                }
            }

            catch (Exception ex)
            {
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "There is a transaction this record cannot be deleted!";
                }
                throw new AppException(errorMessage, ex);
            }
        }
        #endregion
    }
}
