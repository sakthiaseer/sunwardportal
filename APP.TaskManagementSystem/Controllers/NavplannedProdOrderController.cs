﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NavplannedProdOrderController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public NavplannedProdOrderController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetNavplannedProdOrder")]
        public List<NavplannedProdOrderModel> Get()
        {
            List<NavplannedProdOrderModel> navplannedProdOrderModels = new List<NavplannedProdOrderModel>();
            var navPlannedProdOrder = _context.NavplannedProdOrder.Include("Item").Include("Inpreport").OrderByDescending(o => o.PlannedProdOrderId).AsNoTracking().ToList();
            if(navPlannedProdOrder!=null && navPlannedProdOrder.Count>0)
            {
                navPlannedProdOrder.ForEach(s =>
                {
                    NavplannedProdOrderModel navplannedProdOrderModel = new NavplannedProdOrderModel
                    {
                        PlannedProdOrderID = s.PlannedProdOrderId,
                        ItemId = s.ItemId,
                        ItemNo = s.Item != null ? s.Item.No : string.Empty,
                        InpreportId = s.Inpreport.InpreportId,
                        Description = s.Description,
                        Rpono = s.Rpono,
                        StartDate = s.StartDate,
                        CompletionDate = s.CompletionDate,
                        Quantity = s.Quantity
                    };
                    navplannedProdOrderModels.Add(navplannedProdOrderModel);
                });
               

            }           

            return navplannedProdOrderModels;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetByINPReportID")]
        public List<NavplannedProdOrderModel> GetByINPReportID(int id)
        {
            var inpItems = _context.NavplannedProdOrder.Where(q => q.InpreportId == id).Select(s => new NavplannedProdOrderModel
            {
                //Changes Done by Aravinth Start
                InpreportId = s.InpreportId,
                //Changes Done by Aravinth End
                PlannedProdOrderID = s.PlannedProdOrderId,
                ItemId = s.ItemId,
                ItemNo = s.Item!=null?s.Item.No : string.Empty,
                Description = s.Description,
                Rpono = s.Rpono,
                StartDate = s.StartDate,
                CompletionDate = s.CompletionDate,
                Quantity = s.Quantity
            }).OrderByDescending(o => o.PlannedProdOrderID).AsNoTracking().ToList();
            return inpItems;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetNavINPItems")]
        public List<INPModel> GetByID(int id)
        {
            var inpItems = _context.NavinpitemReport.Where(q => q.InpreportId == id).Select(s => new INPModel
            {
                //Changes Done by Aravinth Start
                InpreportId = s.InpreportId,
                //Changes Done by Aravinth End
                ItemNo = s.ItemNo,

            }).OrderByDescending(o => o.InpreportId).AsNoTracking().ToList();
            return inpItems;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<NavplannedProdOrderModel> GetData(SearchModel searchModel)
        {
            var navPlannedProdOrder = new NavplannedProdOrder();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        navPlannedProdOrder = _context.NavplannedProdOrder.OrderByDescending(o => o.PlannedProdOrderId).FirstOrDefault();
                        break;
                    case "Last":
                        navPlannedProdOrder = _context.NavplannedProdOrder.OrderByDescending(o => o.PlannedProdOrderId).LastOrDefault();
                        break;
                    case "Next":
                        navPlannedProdOrder = _context.NavplannedProdOrder.OrderByDescending(o => o.PlannedProdOrderId).LastOrDefault();
                        break;
                    case "Previous":
                        navPlannedProdOrder = _context.NavplannedProdOrder.OrderByDescending(o => o.PlannedProdOrderId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        navPlannedProdOrder = _context.NavplannedProdOrder.OrderByDescending(o => o.PlannedProdOrderId).FirstOrDefault();
                        break;
                    case "Last":
                        navPlannedProdOrder = _context.NavplannedProdOrder.OrderByDescending(o => o.PlannedProdOrderId).LastOrDefault();
                        break;
                    case "Next":
                        navPlannedProdOrder = _context.NavplannedProdOrder.OrderBy(o => o.PlannedProdOrderId).FirstOrDefault(s => s.PlannedProdOrderId > searchModel.Id);
                        break;
                    case "Previous":
                        navPlannedProdOrder = _context.NavplannedProdOrder.OrderByDescending(o => o.PlannedProdOrderId).FirstOrDefault(s => s.PlannedProdOrderId < searchModel.Id);
                        break;
                }
            }

            var result = _mapper.Map<NavplannedProdOrderModel>(navPlannedProdOrder);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertNavplannedProdOrder")]
        public NavplannedProdOrderModel Post(NavplannedProdOrderModel value)
        {
            var itemNo = _context.Navitems.SingleOrDefault(p => p.ItemId == value.ItemId);

            var navPlannedProdOrder = new NavplannedProdOrder
            {
                InpreportId = value.InpreportId,
                ItemId = value.ItemId,
                ItemNo = itemNo.No,
                Description = value.Description,
                Rpono = value.Rpono,
                StartDate = value.StartDate,
                CompletionDate = value.CompletionDate,
                Quantity = value.Quantity,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.NavplannedProdOrder.Add(navPlannedProdOrder);
            _context.SaveChanges();
            value.PlannedProdOrderID = navPlannedProdOrder.PlannedProdOrderId;


            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateNavplannedProdOrder")]
        public NavplannedProdOrderModel Put(NavplannedProdOrderModel value)
        {
            var itemNo = _context.Navitems.SingleOrDefault(p => p.ItemId == value.ItemId);


            var navPlannedProdOrder = _context.NavplannedProdOrder.SingleOrDefault(p => p.PlannedProdOrderId == value.PlannedProdOrderID);
            if (navPlannedProdOrder != null)
            {
                navPlannedProdOrder.ItemId = value.ItemId;
                navPlannedProdOrder.ItemNo = itemNo.No;
                navPlannedProdOrder.InpreportId = value.InpreportId;
                navPlannedProdOrder.Description = value.Description;
                navPlannedProdOrder.Rpono = value.Rpono;
                navPlannedProdOrder.StartDate = value.StartDate;
                navPlannedProdOrder.CompletionDate = value.CompletionDate;
                navPlannedProdOrder.Quantity = value.Quantity;
                navPlannedProdOrder.ModifiedDate = DateTime.Now;
                navPlannedProdOrder.ModifiedByUserId = value.ModifiedByUserID;
                navPlannedProdOrder.StatusCodeId = value.StatusCodeID.Value;
            }

            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteNavplannedProdOrder")]
        public void Delete(int id)
        {
            var navPlannedProdOrder = _context.NavplannedProdOrder.SingleOrDefault(p => p.PlannedProdOrderId == id);
            if (navPlannedProdOrder != null)
            {
                _context.NavplannedProdOrder.Remove(navPlannedProdOrder);
                _context.SaveChanges();
            }
        }

    }
}