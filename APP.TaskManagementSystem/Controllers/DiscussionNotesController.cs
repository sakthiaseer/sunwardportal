﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class DiscussionNotesController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public DiscussionNotesController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetDiscussionNotes")]
        public ActionResult<DiscussionNotesModel> Get(int id, int userId)
        {

              var DiscussionNotes = _context.TaskDiscussion.Where(t => t.TaskId == id && t.UserId == userId).ToList();
            if (DiscussionNotes.Count!= 0)
            {
                var result = _mapper.Map<DiscussionNotesModel>(DiscussionNotes);
                return result;
            }
            else
            {
                return null;
            }


        }

        // GET: api/Project/2
        [HttpGet("GetdiscussionNotes/{id:int}")]
        //public ActionResult<TaskNotesModel> Get(int? id)
        //{
        //    //if (id == null)
        //    //{
        //    //    return NotFound();
        //    //}
        //    //var DiscussionNotes = _context.DiscussionNotes.SingleOrDefault(p => p.TaskNotesId == id.Value);
        //    //var result = _mapper.Map<TaskNotesModel>(DiscussionNotes);
        //    //if (result == null)
        //    //{
        //    //    return NotFound();
        //    //}
        //    return result;
        //}

      
        // POST: api/User
        [HttpPost]
        [Route("InsertDiscussionNotes")]
        public DiscussionNotes Post(DiscussionNotes value)
        {
            if (value.TaskId != 0)
            {
                var taskMaster = _context.TaskMaster.FirstOrDefault(d => d.TaskId == value.TaskId);
                if (taskMaster != null)
                {
                    taskMaster.DiscussionDate = value.DiscussionDate;
                    //_context.SaveChanges();              



                }
            }
            //DateTime remDate;
            //if (value.RemainderDate <= value.DueDate)
            //{
            //    remDate = value.RemainderDate;
            //}
            var DiscussionNotes = _context.TaskDiscussion.Where(p => p.TaskId == value.TaskId && p.UserId == value.TaskUserId).FirstOrDefault();
            if (DiscussionNotes == null)
            {
                 DiscussionNotes = new TaskDiscussion
                 {

                    TaskId = value.TaskId,
                    DiscussionNotes = value.DiscussionNotes1,
                    DiscussionDate = value.DiscussionDate,
                    UserId = value.TaskUserId,
                    

                };
                //    _context.DiscussionNotes.Add(DiscussionNotes);
                //    _context.SaveChanges();
                //    value.TaskNotesID = DiscussionNotes.TaskNotesId;
                }
                return value;

        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateDiscussionNotes")]
        public DiscussionNotes Put(DiscussionNotes value)
        {
            if (value.TaskId != 0)
            {
                var taskMaster = _context.TaskMaster.FirstOrDefault(d => d.TaskId == value.TaskId);
                if (taskMaster != null)
                {
                    taskMaster.DiscussionDate = value.DiscussionDate;
                    //_context.SaveChanges();              



                }
            }
            var DiscussionNotes = _context.TaskDiscussion.SingleOrDefault(p => p.DiscussionNotesId == value.DiscussionNotesId);
            //  folders.TaskMemberId = value.TaskMemberID;
            DiscussionNotes.TaskId = value.TaskId;
            DiscussionNotes.UserId = value.TaskUserId;
            DiscussionNotes.DiscussionNotes = value.DiscussionNotes1;
            DiscussionNotes.DiscussionDate = value.DiscussionDate;
            

            _context.SaveChanges();
            return value;
        }
    }
}