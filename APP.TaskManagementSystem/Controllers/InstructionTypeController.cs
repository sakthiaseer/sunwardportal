﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class InstructionTypeController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public InstructionTypeController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetInstructionTypes")]
        public List<InstructionTypeModel> Get()
        {
            List<InstructionTypeModel> instructionTypeModels = new List<InstructionTypeModel>();
            var instructionType = _context.InstructionType.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").OrderByDescending(o => o.InstructionTypeId).AsNoTracking().ToList();
            //var InstructionType = _context.InstructionType.Include("AddedByUser").Include("StatusCode").Include("ModifiedByUser").Select(s => new InstructionTypeModel
           
            if(instructionType!=null  && instructionType.Count>0)
            {
                instructionType.ForEach(s =>
                {
                    InstructionTypeModel instructionTypeModel = new InstructionTypeModel
                    {
                        InstructionTypeID = s.InstructionTypeId,
                        DocumentName = s.DocumentName,
                        TypeID = s.TypeId,
                        No = s.No,
                        InstructionNo = s.InstructionNo,
                        VersionNo = s.VersionNo,
                        EffectiveDate = s.EffectiveDate,
                        Link = s.Link,
                        DeviceCatalogMasterID = s.DeviceCatalogMasterId,
                        CalibrationServiceInformationID = s.CalibrationServiceInformationId,

                        ModifiedByUserID = s.AddedByUserId,
                        AddedByUserID = s.ModifiedByUserId,
                        StatusCodeID = s.StatusCodeId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                    };
                    instructionTypeModels.Add(instructionTypeModel);
                });

            }
            return instructionTypeModels;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Contact")]
        [HttpGet("GetInstructionType/{id:int}")]
        public ActionResult<InstructionTypeModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var instructionType = _context.InstructionType.SingleOrDefault(p => p.InstructionTypeId == id.Value);
            var result = _mapper.Map<InstructionTypeModel>(instructionType);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<InstructionTypeModel> GetData(SearchModel searchModel)
        {
            var instructionType = new InstructionType();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        instructionType = _context.InstructionType.OrderByDescending(o => o.InstructionTypeId).FirstOrDefault();
                        break;
                    case "Last":
                        instructionType = _context.InstructionType.OrderByDescending(o => o.InstructionTypeId).LastOrDefault();
                        break;
                    case "Next":
                        instructionType = _context.InstructionType.OrderByDescending(o => o.InstructionTypeId).LastOrDefault();
                        break;
                    case "Previous":
                        instructionType = _context.InstructionType.OrderByDescending(o => o.InstructionTypeId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        instructionType = _context.InstructionType.OrderByDescending(o => o.InstructionTypeId).FirstOrDefault();
                        break;
                    case "Last":
                        instructionType = _context.InstructionType.OrderByDescending(o => o.InstructionTypeId).LastOrDefault();
                        break;
                    case "Next":
                        instructionType = _context.InstructionType.OrderBy(o => o.InstructionTypeId).FirstOrDefault(s => s.InstructionTypeId > searchModel.Id);
                        break;
                    case "Previous":
                        instructionType = _context.InstructionType.OrderByDescending(o => o.InstructionTypeId).FirstOrDefault(s => s.InstructionTypeId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<InstructionTypeModel>(instructionType);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertInstructionType")]
        public InstructionTypeModel Post(InstructionTypeModel value)
        {
            var instructionType = new InstructionType
            {
                //InstructionTypeID = s.InstructionTypeId,
                DocumentName = value.DocumentName,
                TypeId = value.TypeID,
                No = value.No,
                InstructionNo = value.InstructionNo,
                VersionNo = value.VersionNo,
                EffectiveDate = value.EffectiveDate,
                Link = value.Link,
                DeviceCatalogMasterId = value.DeviceCatalogMasterID,
                CalibrationServiceInformationId = value.CalibrationServiceInformationID,

                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,

                //ContactActivities = new List<ContactActivities>(),

            };

            _context.InstructionType.Add(instructionType);
            _context.SaveChanges();
            value.InstructionTypeID = instructionType.InstructionTypeId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateInstructionType")]
        public InstructionTypeModel Put(InstructionTypeModel value)
        {
            var instructionType = _context.InstructionType.SingleOrDefault(p => p.InstructionTypeId == value.InstructionTypeID);

            instructionType.DocumentName = value.DocumentName;
            instructionType.TypeId = value.TypeID;
            instructionType.No = value.No;
            instructionType.InstructionNo = value.InstructionNo;
            instructionType.VersionNo = value.VersionNo;
            instructionType.EffectiveDate = value.EffectiveDate;
            instructionType.Link = value.Link;
            instructionType.DeviceCatalogMasterId = value.DeviceCatalogMasterID;
            instructionType.CalibrationServiceInformationId = value.CalibrationServiceInformationID;

            instructionType.ModifiedByUserId = value.ModifiedByUserID;
            instructionType.ModifiedDate = DateTime.Now;
            instructionType.StatusCodeId = value.StatusCodeID.Value;

            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteInstructionType")]
        public ActionResult<string> Delete(int id)
        {
            try
            {
                var instructionType = _context.InstructionType.SingleOrDefault(p => p.InstructionTypeId == id);
                if (instructionType != null)
                {
                    _context.InstructionType.Remove(instructionType);
                    _context.SaveChanges();
                }
                return Ok("Record deleted.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}