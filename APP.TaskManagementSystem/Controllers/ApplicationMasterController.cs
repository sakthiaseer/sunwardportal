﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ApplicationMasterController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public ApplicationMasterController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetApplicationMaster")]
        public List<ApplicationMaster> Get()
        {
            var applicationmaster = _context.ApplicationMaster.Select(s => new ApplicationMaster
            {
                ApplicationMasterId = s.ApplicationMasterId,
                ApplicationMasterName = s.ApplicationMasterName,
                ApplicationMasterDescription = s.ApplicationMasterDescription,
                IsApplyProfile = s.IsApplyProfile,
                IsApplyFileProfile = s.IsApplyFileProfile,

            }).OrderByDescending(o => o.ApplicationMasterId).AsNoTracking().ToList();
            return applicationmaster;
        }
        [HttpGet]
        [Route("GetApplicationMasterDetails")]
        public List<ApplicationMasterDetailModel> GetApplicationMasterDetails()
        {
            var applicationmaster = _context.ApplicationMasterDetail.Include(p => p.Profile).Include("ApplicationMaster").Select(s => new ApplicationMasterDetailModel
            {
                ApplicationMasterDetailId = s.ApplicationMasterDetailId,
                ApplicationMasterId = s.ApplicationMasterId,
                Description = s.Description,
                ApplicationMaster = s.ApplicationMaster.ApplicationMasterName,
                Value = s.Value,
                ProfileId = s.ProfileId,
                ProfileName = s.Profile != null ? s.Profile.Name : null,
                FileProfileTypeId = s.FileProfileTypeId,
                FileProfileTypeName = s.FileProfileType != null ? s.FileProfileType.Name : null,

            }).OrderByDescending(o => o.ApplicationMasterDetailId).AsNoTracking().ToList();
            return applicationmaster;
        }
        [HttpGet("GetApplicationMasterDetailById/{id:int}")]
        public List<ApplicationMasterDetailModel> GetApplicationMasterDetailById(int? id)
        {
            var applicationmaster = _context.ApplicationMasterDetail.Include(a => a.ApplicationMaster).Include(p => p.Profile).Include("ApplicationMaster").Select(s => new ApplicationMasterDetailModel
            {
                ApplicationMasterDetailId = s.ApplicationMasterDetailId,
                ApplicationMasterId = s.ApplicationMasterId,
                Description = s.Description,
                ApplicationMaster = s.ApplicationMaster.ApplicationMasterName,
                Value = s.Value,
                ProfileId = s.ProfileId,
                ProfileName = s.Profile != null ? s.Profile.Name : null,
                IsApplyProfile = s.ApplicationMaster != null ? s.ApplicationMaster.IsApplyProfile : null,
                IsApplyFileProfile = s.ApplicationMaster != null ? s.ApplicationMaster.IsApplyFileProfile : null,
                FileProfileTypeId = s.FileProfileTypeId,
                FileProfileTypeName = s.FileProfileType != null ? s.FileProfileType.Name : null,
            }).Where(s => s.ApplicationMasterId == id).OrderByDescending(o => o.ApplicationMasterDetailId).AsNoTracking().ToList();
            return applicationmaster;
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetApplicationMasterDetailByType")]
        public List<ApplicationMasterDetailModel> GetApplicationMasterDetailByType(int? type)
        {
            List<ApplicationMasterDetailModel> codeMasterDetails = new List<ApplicationMasterDetailModel>();
            var applicationMaster = _context.ApplicationMaster.FirstOrDefault(a => a.ApplicationMasterCodeId == type);
            if (applicationMaster != null)
            {
                var codeMasterDetail = _context.ApplicationMasterDetail.Include(p => p.Profile).Include(f=>f.FileProfileType).Where(d => d.ApplicationMasterId == applicationMaster.ApplicationMasterId).AsNoTracking().ToList();
                codeMasterDetail.ForEach(s =>
                {
                    ApplicationMasterDetailModel codeMasterDetailList = new ApplicationMasterDetailModel();
                    codeMasterDetailList.ApplicationMasterDetailId = s.ApplicationMasterDetailId;
                    codeMasterDetailList.ApplicationMasterId = s.ApplicationMasterId;
                    codeMasterDetailList.Description = s.Description;
                    codeMasterDetailList.Value = s.Value;
                    codeMasterDetailList.NameDescription = s.Value + " | " + s.Description;
                    codeMasterDetailList.ProfileId = s.ProfileId;
                    codeMasterDetailList.ProfileName = s.Profile != null ? s.Profile.Name : null;
                    codeMasterDetailList.IsApplyProfile = applicationMaster.IsApplyProfile;
                    codeMasterDetailList.FileProfileTypeId = s.FileProfileTypeId;
                    codeMasterDetailList.FileProfileTypeName = s.FileProfileType != null ? s.FileProfileType.Name : null;
                    codeMasterDetailList.Abbreviation1 = s.Profile != null && (!String.IsNullOrEmpty(s.Profile.Abbreviation1)) ? (JsonConvert.DeserializeObject<List<NumberSeriesCodeModel>>(s.Profile.Abbreviation1).ToList()) : null;
                    codeMasterDetails.Add(codeMasterDetailList);
                });
            }
            return codeMasterDetails.OrderBy(o => o.Value).ToList();

        }
        [HttpGet]
        [Route("GetApplicationMasterDetailByTypewithDesc")]
        public List<ApplicationMasterDetailModel> GetApplicationMasterDetailByTypewithDesc(int? type)
        {
            var codeMasterDetails = new List<ApplicationMasterDetailModel>();
            var applicationMaster = _context.ApplicationMaster.FirstOrDefault(a => a.ApplicationMasterCodeId == type);
            if (applicationMaster != null)
            {
                codeMasterDetails = _context.ApplicationMasterDetail.Include(p => p.Profile).Select(s => new ApplicationMasterDetailModel
                {
                    ApplicationMasterDetailId = s.ApplicationMasterDetailId,
                    ApplicationMasterId = s.ApplicationMasterId,
                    Description = s.Value + " | " + s.Description,
                    Value = s.Value,
                    ProfileId = s.ProfileId,
                    ProfileName = s.Profile != null ? s.Profile.Name : null,
                    FileProfileTypeId = s.FileProfileTypeId,
                    FileProfileTypeName = s.FileProfileType != null ? s.FileProfileType.Name : null,
                }).Where(d => d.ApplicationMasterId == applicationMaster.ApplicationMasterId).OrderBy(o => o.Value).AsNoTracking().ToList();
            }
            return codeMasterDetails;

        }
        // POST: api/User
        [HttpPost]
        [Route("InsertApplicationMasterDetail")]
        public ApplicationMasterDetailModel Post(ApplicationMasterDetailModel value)
        {
            var applicationmaster = new ApplicationMasterDetail
            {
                ApplicationMasterId = value.ApplicationMasterId,
                Value = value.Value,
                Description = value.Description,
                ProfileId = value.ProfileId,
                FileProfileTypeId = value.FileProfileTypeId
            };
            _context.ApplicationMasterDetail.Add(applicationmaster);
            _context.SaveChanges();
            value.ApplicationMasterDetailId = applicationmaster.ApplicationMasterDetailId;
            return value;
        }
        [HttpPost]
        [Route("InsertApplicationMasterDetailByCodeID")]
        public ApplicationMasterDetailModel InsertApplicationMasterDetailByCodeID(ApplicationMasterDetailModel value)
        {
            var applicationMaster = _context.ApplicationMaster.FirstOrDefault(f => f.ApplicationMasterCodeId == value.ApplicationMasterCodeId);
            var applicationmaster = new ApplicationMasterDetail
            {
                ApplicationMasterId = applicationMaster.ApplicationMasterId,
                Value = value.Value,
                Description = value.Description,
                ProfileId = value.ProfileId,
                FileProfileTypeId = value.FileProfileTypeId
            };
            _context.ApplicationMasterDetail.Add(applicationmaster);
            _context.SaveChanges();
            value.ApplicationMasterDetailId = applicationmaster.ApplicationMasterDetailId;
            return value;
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateApplicationMasterDetail")]
        public ApplicationMasterDetailModel Put(ApplicationMasterDetailModel value)
        {
            var applicationmaster = _context.ApplicationMasterDetail.SingleOrDefault(p => p.ApplicationMasterDetailId == value.ApplicationMasterDetailId);
            applicationmaster.ApplicationMasterId = value.ApplicationMasterId;
            applicationmaster.Value = value.Value;
            applicationmaster.Description = value.Description;
            applicationmaster.ProfileId = value.ProfileId;
            applicationmaster.FileProfileTypeId = value.FileProfileTypeId;
            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteApplicationMasterDetail")]
        public void Delete(int id)
        {
            var applicationmaster = _context.ApplicationMasterDetail.SingleOrDefault(p => p.ApplicationMasterDetailId == id);
            if (applicationmaster != null)
            {
                _context.ApplicationMasterDetail.Remove(applicationmaster);
                _context.SaveChanges();
            }
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ApplicationMasterDetailModel> GetData(SearchModel searchModel)
        {
            var applicationmaster = new ApplicationMasterDetail();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        applicationmaster = _context.ApplicationMasterDetail.Where(o => o.ApplicationMasterId == searchModel.MasterTypeID).OrderByDescending(o => o.ApplicationMasterDetailId).FirstOrDefault();
                        break;
                    case "Last":
                        applicationmaster = _context.ApplicationMasterDetail.Where(o => o.ApplicationMasterId == searchModel.MasterTypeID).OrderByDescending(o => o.ApplicationMasterDetailId).LastOrDefault();
                        break;
                    case "Next":
                        applicationmaster = _context.ApplicationMasterDetail.Where(o => o.ApplicationMasterId == searchModel.MasterTypeID).OrderByDescending(o => o.ApplicationMasterDetailId).LastOrDefault();
                        break;
                    case "Previous":
                        applicationmaster = _context.ApplicationMasterDetail.Where(o => o.ApplicationMasterId == searchModel.MasterTypeID).OrderByDescending(o => o.ApplicationMasterDetailId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        applicationmaster = _context.ApplicationMasterDetail.Where(o => o.ApplicationMasterId == searchModel.MasterTypeID).OrderByDescending(o => o.ApplicationMasterDetailId).FirstOrDefault();
                        break;
                    case "Last":
                        applicationmaster = _context.ApplicationMasterDetail.Where(o => o.ApplicationMasterId == searchModel.MasterTypeID).OrderByDescending(o => o.ApplicationMasterDetailId).LastOrDefault();
                        break;
                    case "Next":
                        applicationmaster = _context.ApplicationMasterDetail.Where(o => o.ApplicationMasterId == searchModel.MasterTypeID).OrderBy(o => o.ApplicationMasterDetailId).FirstOrDefault(s => s.ApplicationMasterDetailId > searchModel.Id);
                        break;
                    case "Previous":
                        applicationmaster = _context.ApplicationMasterDetail.Where(o => o.ApplicationMasterId == searchModel.MasterTypeID).OrderByDescending(o => o.ApplicationMasterDetailId).FirstOrDefault(s => s.ApplicationMasterDetailId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ApplicationMasterDetailModel>(applicationmaster);
            return result;
        }
        [HttpGet]
        [Route("GetCodeMasterDetailByName")]
        public List<ApplicationMasterDetailModel> GetCodeMasterDetailByName(string type)
        {
            var applicationmaster = _context.CodeMaster.Select(s => new ApplicationMasterDetailModel
            {
                ApplicationMasterDetailId = s.CodeMasterId,
                ApplicationMasterId = s.CodeId,
                Value = s.CodeValue,
                ApplicationMaster = s.CodeType,
                Description = s.CodeDescription,
            }).Where(s => s.ApplicationMaster == type).OrderByDescending(o => o.ApplicationMasterDetailId).AsNoTracking().ToList();
            return applicationmaster;
        }
        [HttpPost()]
        [Route("GetCodeMasterData")]
        public ActionResult<ApplicationMasterDetailModel> GetCodeMasterData(SearchModel searchModel)
        {
            var applicationmaster = new CodeMaster();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        applicationmaster = _context.CodeMaster.Where(o => o.CodeMasterId == searchModel.MasterTypeID).OrderByDescending(o => o.CodeMasterId).FirstOrDefault();
                        break;
                    case "Last":
                        applicationmaster = _context.CodeMaster.Where(o => o.CodeMasterId == searchModel.MasterTypeID).OrderByDescending(o => o.CodeMasterId).LastOrDefault();
                        break;
                    case "Next":
                        applicationmaster = _context.CodeMaster.Where(o => o.CodeMasterId == searchModel.MasterTypeID).OrderByDescending(o => o.CodeMasterId).LastOrDefault();
                        break;
                    case "Previous":
                        applicationmaster = _context.CodeMaster.Where(o => o.CodeMasterId == searchModel.MasterTypeID).OrderByDescending(o => o.CodeMasterId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        applicationmaster = _context.CodeMaster.Where(o => o.CodeMasterId == searchModel.MasterTypeID).OrderByDescending(o => o.CodeMasterId).FirstOrDefault();
                        break;
                    case "Last":
                        applicationmaster = _context.CodeMaster.Where(o => o.CodeMasterId == searchModel.MasterTypeID).OrderByDescending(o => o.CodeMasterId).LastOrDefault();
                        break;
                    case "Next":
                        applicationmaster = _context.CodeMaster.Where(o => o.CodeMasterId == searchModel.MasterTypeID).OrderBy(o => o.CodeMasterId).FirstOrDefault(s => s.CodeMasterId > searchModel.Id);
                        break;
                    case "Previous":
                        applicationmaster = _context.CodeMaster.Where(o => o.CodeMasterId == searchModel.MasterTypeID).OrderByDescending(o => o.CodeMasterId).FirstOrDefault(s => s.CodeMasterId < searchModel.Id);
                        break;
                }
            }
            var ApplicationMasterDetailItems = new ApplicationMasterDetailModel();
            if (applicationmaster != null)
            {
                ApplicationMasterDetailItems.ApplicationMasterDetailId = applicationmaster.CodeMasterId;
                ApplicationMasterDetailItems.ApplicationMasterId = applicationmaster.CodeId;
                ApplicationMasterDetailItems.Value = applicationmaster.CodeValue;
                ApplicationMasterDetailItems.ApplicationMaster = applicationmaster.CodeType;
                ApplicationMasterDetailItems.Description = applicationmaster.CodeDescription;
            }
            var result = _mapper.Map<ApplicationMasterDetailModel>(applicationmaster);
            return result;
        }
        [HttpPost]
        [Route("InsertCodeMsater")]
        public ApplicationMasterDetailModel InsertCodeMsater(ApplicationMasterDetailModel value)
        {
            int codeid = _context.CodeMaster.DefaultIfEmpty().Max(s => s.CodeId);
            var applicationmaster = new CodeMaster
            {
                CodeId = codeid + 1,
                CodeType = value.ApplicationMaster,
                CodeValue = value.Value,
                CodeDescription = value.Description,
            };
            _context.CodeMaster.Add(applicationmaster);
            _context.SaveChanges();
            value.ApplicationMasterDetailId = applicationmaster.CodeMasterId;
            return value;
        }
        [Route("UpdateCodeMaster")]
        public ApplicationMasterDetailModel UpdateCodeMaster(ApplicationMasterDetailModel value)
        {
            var applicationmaster = _context.CodeMaster.SingleOrDefault(p => p.CodeMasterId == value.ApplicationMasterDetailId);
            applicationmaster.CodeValue = value.Value;
            applicationmaster.CodeDescription = value.Description;
            _context.SaveChanges();
            return value;
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteCodeMaster")]
        public void DeleteCodeMaster(int id)
        {
            var applicationmaster = _context.CodeMaster.SingleOrDefault(p => p.CodeMasterId == id);
            if (applicationmaster != null)
            {
                _context.CodeMaster.Remove(applicationmaster);
                _context.SaveChanges();
            }
        }
    }
}