﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TempSalesPackInformationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public TempSalesPackInformationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpPost]
        [Route("InsertTempSalesPackInformation")]
        public TempSalesPackInformationModel InsertTempSalesPackInformation(TempSalesPackInformationModel value)
        {
            var finishProductGeneralInfolines = _context.FinishProdcutGeneralInfoLine.ToList();
            if (finishProductGeneralInfolines != null && finishProductGeneralInfolines.Count > 0)
            {
                var tempSalesPackInformations = _context.TempSalesPackInformation.ToList();
                finishProductGeneralInfolines.ForEach(f =>
                {
                    var existing = tempSalesPackInformations.FirstOrDefault(s => s.FinishProductGeneralInfoLineId == f.FinishProductGeneralInfoLineId);
                    if (existing == null)
                    {
                        TempSalesPackInformation tempSalesPackInformationModel = new TempSalesPackInformation
                        {
                            FinishproductGeneralInfoId = f.FinishProductGeneralInfoId,
                            FinishProductGeneralInfoLineId = f.FinishProductGeneralInfoLineId,
                            AddedByUserId = value.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = 5,
                        };
                        _context.TempSalesPackInformation.Add(tempSalesPackInformationModel);
                        _context.SaveChanges();
                    }

                });

            }
            return value;
        }
        [HttpPut]
        [Route("UpdateTempSalesPackInformation")]
        public TempSalesPackInformationModel Put(TempSalesPackInformationModel value)
        {
            var tempSalesPackInformation = _context.TempSalesPackInformation.SingleOrDefault(p => p.TempSalesPackInformationId == value.TempSalesPackInformationID);


            tempSalesPackInformation.StatusCodeId = value.StatusCodeID.Value;
            tempSalesPackInformation.ModifiedByUserId = value.ModifiedByUserID;
            tempSalesPackInformation.ModifiedDate = DateTime.Now;
            _context.SaveChanges();
            if (value.StatusCodeID == 2)
            {
                var tempsalesLine = _context.TempSalesPackInformationFactor.Where(t => t.TempSalesPackInformationId == tempSalesPackInformation.TempSalesPackInformationId).ToList();
                if (tempsalesLine != null && tempsalesLine.Count > 0)
                {
                    _context.TempSalesPackInformationFactor.RemoveRange(tempsalesLine);
                    _context.SaveChanges();
                }
            }
            return value;
        }
        [HttpGet]
        [Route("GetTempSalesPackInformationReport")]
        public List<TempSalesPackInformationReportModel> GetTempSalesPackInformationReport()
        {
            var masterDetailList = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var tempSalesitemInfo = _context.TempSalesPackInformation.Include(s => s.StatusCode).ToList();
            var tempSalesItemFinishProductLineIds = _context.TempSalesPackInformation.Select(t => t.FinishProductGeneralInfoLineId).ToList();
            var query = _context.FinishProdcutGeneralInfoLine
                .Include(r => r.FinishProductGeneralInfo.StatusCode)
                .Include(r => r.FinishProductGeneralInfo.FinishProduct)
                .Include(r => r.FinishProductGeneralInfo.ProductionRegistrationHolder)
                .Include(f => f.FinishProductGeneralInfo).Where(s => tempSalesItemFinishProductLineIds.Contains(s.FinishProductGeneralInfoLineId)).AsNoTracking().ToList();
            List<TempSalesPackInformationReportModel> tempSalesPackInformationReportModels = new List<TempSalesPackInformationReportModel>();
            query.ForEach(s =>
            {
                TempSalesPackInformationReportModel tempSalesPackInformationReportModel = new TempSalesPackInformationReportModel();

                tempSalesPackInformationReportModel.FinishProductGeneralInfoLineID = s.FinishProductGeneralInfoLineId;
                tempSalesPackInformationReportModel.FinishProductGeneralInfoID = s.FinishProductGeneralInfoId != null ? s.FinishProductGeneralInfoId.Value : 0;
                tempSalesPackInformationReportModel.ManufacturingSite = masterDetailList != null && s.FinishProductGeneralInfo != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.FinishProduct.ManufacturingSiteId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.FinishProduct.ManufacturingSiteId).Value : "";

                tempSalesPackInformationReportModel.ProductName = masterDetailList != null && s.FinishProductGeneralInfo != null && masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.FinishProduct.ProductId) != null ? masterDetailList.FirstOrDefault(a => a.ApplicationMasterDetailId == s.FinishProductGeneralInfo.FinishProduct.ProductId).Value : "";
                tempSalesPackInformationReportModel.PrhspecificProductName = s.FinishProductGeneralInfo?.PrhspecificProductId != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.FinishProductGeneralInfo.PrhspecificProductId).FirstOrDefault()?.Value : "";
                tempSalesPackInformationReportModel.ProductionRegistrationHolderName = s.FinishProductGeneralInfo?.ProductionRegistrationHolderId != null ? s.FinishProductGeneralInfo?.ProductionRegistrationHolder?.CompanyName : "";
                tempSalesPackInformationReportModel.RegisterCountry = s.FinishProductGeneralInfo?.RegisterCountry != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.FinishProductGeneralInfo.RegisterCountry).FirstOrDefault()?.Value : "";
                tempSalesPackInformationReportModel.ProductOwner = s.FinishProductGeneralInfo?.RegisterProductOwnerId != null && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.FinishProductGeneralInfo.RegisterProductOwnerId).FirstOrDefault()?.Value : "";
                tempSalesPackInformationReportModel.SmallestQtyUnit = s.PackQtyunitId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackQtyunitId).Select(m => m.Value).FirstOrDefault() : "";
                tempSalesPackInformationReportModel.RegistrationFactor = s.FactorOfSmallestProductionPack;
                tempSalesPackInformationReportModel.SmallestPerPack = s.PerPackId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PerPackId).Select(m => m.Value).FirstOrDefault() : "";
                tempSalesPackInformationReportModel.RegistrationPerPack = s.SalesPerPackId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.SalesPerPackId).Select(m => m.Value).FirstOrDefault() : "";
                tempSalesPackInformationReportModel.PackType = s.PackTypeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackTypeId).Select(m => m.Value).FirstOrDefault() : "";
                tempSalesPackInformationReportModel.PackagingType = s.PackagingTypeId != 0 && masterDetailList.Count > 0 ? masterDetailList.Where(m => m.ApplicationMasterDetailId == s.PackagingTypeId).Select(m => m.Value).FirstOrDefault() : "";
                tempSalesPackInformationReportModel.SmallestPackQty = s.PackQty;
                tempSalesPackInformationReportModel.StatusCodeID = tempSalesitemInfo?.Where(t => t.FinishProductGeneralInfoLineId == s.FinishProductGeneralInfoLineId).Select(t => t.StatusCodeId).FirstOrDefault();
                tempSalesPackInformationReportModel.StatusCode = tempSalesitemInfo?.Where(t => t.FinishProductGeneralInfoLineId == s.FinishProductGeneralInfoLineId).Select(t => t.StatusCodeId).FirstOrDefault() != null ? tempSalesitemInfo?.FirstOrDefault(t => t.FinishProductGeneralInfoLineId == s.FinishProductGeneralInfoLineId).StatusCode.CodeValue : "";
                tempSalesPackInformationReportModel.TempSalesPackInformationID = tempSalesitemInfo?.Where(t => t.FinishProductGeneralInfoLineId == s.FinishProductGeneralInfoLineId).Select(t => t.TempSalesPackInformationId).FirstOrDefault();
                tempSalesPackInformationReportModels.Add(tempSalesPackInformationReportModel);
            });
            return tempSalesPackInformationReportModels.OrderByDescending(f => f.FinishProductGeneralInfoID).ThenBy(l => l.FinishProductGeneralInfoLineID).ToList();

        }

        [HttpDelete]
        [Route("DeleteTempSalesPackInformation")]
        public void Delete(int id)
        {
            var tempSalesPackInformation = _context.TempSalesPackInformation.SingleOrDefault(p => p.TempSalesPackInformationId == id);
            if (tempSalesPackInformation != null)
            {
                _context.TempSalesPackInformation.Remove(tempSalesPackInformation);
                _context.SaveChanges();
            }
        }

    }
}