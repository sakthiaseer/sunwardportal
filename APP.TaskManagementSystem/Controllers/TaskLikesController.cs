﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class TaskLikesController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public TaskLikesController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetTaskLikes")]
        public List<TaskLikesModel> Get()
        {
            var taskLikes = _context.TaskLikes.Select(s=>new TaskLikesModel
            {
                TaskLikesID = s.TaskLikesId,
                TaskMasterID = s.TaskMasterId,
                LikedBy = s.LikedBy
            }
            ).OrderByDescending(o => o.TaskLikesID).AsNoTracking().ToList();
            //var result = _mapper.Map<List<TaskLikesModel>>(taskLikes);
            return taskLikes;
        }

        // GET: api/Project/2
        [HttpGet("GetTaskLikes/{id:int}")]
        public ActionResult<TaskLikesModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var taskLikes = _context.TaskLikes.SingleOrDefault(p => p.TaskLikesId == id.Value);
            var result = _mapper.Map<TaskLikesModel>(taskLikes);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<TaskLikesModel> GetData(SearchModel searchModel)
        {
            var taskLikes = new TaskLikes();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        taskLikes = _context.TaskLikes.OrderByDescending(o => o.TaskLikesId).FirstOrDefault();
                        break;
                    case "Last":
                        taskLikes = _context.TaskLikes.OrderByDescending(o => o.TaskLikesId).LastOrDefault();
                        break;
                    case "Next":
                        taskLikes = _context.TaskLikes.OrderByDescending(o => o.TaskLikesId).LastOrDefault();
                        break;
                    case "Previous":
                        taskLikes = _context.TaskLikes.OrderByDescending(o => o.TaskLikesId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        taskLikes = _context.TaskLikes.OrderByDescending(o => o.TaskLikesId).FirstOrDefault();
                        break;
                    case "Last":
                        taskLikes = _context.TaskLikes.OrderByDescending(o => o.TaskLikesId).LastOrDefault();
                        break;
                    case "Next":
                        taskLikes = _context.TaskLikes.OrderBy(o => o.TaskLikesId).FirstOrDefault(s => s.TaskLikesId > searchModel.Id);
                        break;
                    case "Previous":
                        taskLikes = _context.TaskLikes.OrderByDescending(o => o.TaskLikesId).FirstOrDefault(s => s.TaskLikesId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<TaskLikesModel>(taskLikes);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertTaskLikes")]
        public TaskLikesModel Post(TaskLikesModel value)
        {
            var taskLikes = new TaskLikes
            {
                TaskLikesId=value.TaskLikesID,
                TaskMasterId=value.TaskMasterID,
                LikedBy=value.LikedBy
                //AddedByUserId = value.AddedByUserID,
                //AddedDate = DateTime.Now,
                //StatusCodeId = value.StatusCodeID,
                //Name = value.Name,
                //Description = value.Description
            };
            _context.TaskLikes.Add(taskLikes);
            _context.SaveChanges();
            value.TaskLikesID = taskLikes.TaskLikesId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateTaskLikes")]
        public TaskLikesModel Put(TaskLikesModel value)
        {
            var project = _context.TaskLikes.SingleOrDefault(p => p.TaskLikesId == value.TaskLikesID);
            project.TaskMasterId = value.TaskMasterID;
            project.LikedBy = value.LikedBy;
            //project.ModifiedByUserId = value.ModifiedByUserID;
            //project.ModifiedDate = DateTime.Now;
            //project.Name = value.Name;
            //project.Description = value.Description;
            //project.StatusCodeId = value.StatusCodeID;
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteTaskLikes")]
        public void Delete(int id)
        {
            var taskLikes = _context.TaskLikes.SingleOrDefault(p => p.TaskLikesId == id);
            if (taskLikes != null)
            {
                _context.TaskLikes.Remove(taskLikes);
                _context.SaveChanges();
            }
        }
    }
}