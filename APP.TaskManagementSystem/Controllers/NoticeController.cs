﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using APP.TaskManagementSystem.Helper;
using Microsoft.Extensions.Configuration;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NoticeController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public NoticeController(CRT_TMSContext context, IMapper mapper, IConfiguration configuration)
        {
            _context = context;
            _mapper = mapper;
            _configuration = configuration;
        }
        /*[HttpGet]
        [Route("uploadDoc")]
        public void uploadDoc()
        {
            DocumentUploads DocumentUploads = new DocumentUploads(_configuration);
            DocumentUploads.UploadDocumentAll();
        }*/
        [HttpPost]
        [Route("UploadDocuments")]
        public IActionResult UploadDocuments(IFormCollection files)
        {
            var documentObjectId = string.Empty;
            files.Files.ToList().ForEach(f =>
            {
                DocumentUploads DocumentUploads = new DocumentUploads(_configuration);
                documentObjectId=DocumentUploads.UploadDocumentAll(f);
            });
            return Content(documentObjectId);
        }
            [HttpGet]
        [Route("GetNoticesAll")]
        [Authorize(Roles = "Admin")]

        public List<NoticeModel> GetNoticesAll()
        {
            var notice = _context.Notice.Include(a => a.AddedByUser).Include(s => s.Type).Include(m => m.ModifiedByUser).Include(s => s.Module).Include("StatusCode").AsNoTracking().ToList();
            List<NoticeModel> noticeModels = new List<NoticeModel>();
            if (notice != null)
            {
                var noticeIds = notice.Select(s => s.NoticeId).ToList();
                var noticeUserIds = _context.NoticeUser.Where(w => noticeIds.Contains(w.NoticeId.Value)).Select(s => new { s.UserId, s.NoticeId, s.UserGroupId }).ToList();
                var userIds = noticeUserIds.Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).Where(w => userIds.Contains(w.UserId)).ToList();
                notice.ForEach(s =>
                {
                    var userIds = new List<long?>();
                    var userGroupIds = new List<long?>();
                    userIds = noticeUserIds.Where(w => w.NoticeId == s.NoticeId && w.UserId != null).Select(u => u.UserId).ToList();
                    userGroupIds = noticeUserIds.Where(w => w.NoticeId == s.NoticeId && w.UserGroupId != null).Select(u => u.UserGroupId).Distinct().ToList();
                    var userTypes = userGroupIds != null && userGroupIds.Count > 0 ? "user-group" : "user";
                    NoticeModel noticeModel = new NoticeModel
                    {
                        NoticeId = s.NoticeId,
                        Name = s.Name,
                        Description = s.Description,
                        Link = s.Link,
                        StatusCodeID = s.StatusCodeId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        IsLogin = s.IsLogin,
                        ModuleId = s.ModuleId,
                        ModuleName = s.Module?.CodeValue,
                        UserIDs = userTypes == "user" ? userIds : new List<long?>(),
                        UserGroupIDs = userTypes == "user-group" ? userGroupIds : new List<long?>(),
                        TypeId = s.TypeId,
                        TypeName = s.Type?.Value,
                        UserTypes = userTypes,

                    };
                    noticeModels.Add(noticeModel);
                });
            }
            return noticeModels.OrderByDescending(a => a.NoticeId).ToList();
        }
        [HttpGet]
        [Route("GetNotices")]
        [Authorize(Roles = "Admin")]

        public List<NoticeModel> Get(long? id)
        {

            var notice = _context.Notice.Include(a => a.AddedByUser).Include(s => s.Type).Include(a => a.Module).Include(m => m.ModifiedByUser).Include("StatusCode").Where(w => w.ModuleId == id && w.StatusCodeId == 1).AsNoTracking().ToList();
            List<NoticeModel> noticeModels = new List<NoticeModel>();
            if (notice != null)
            {
                var noticeIds = notice.Select(s => s.NoticeId).ToList();
                var noticeUserIds = _context.NoticeUser.Where(w => noticeIds.Contains(w.NoticeId.Value)).Select(s => new { s.UserId, s.NoticeId, s.UserGroupId }).ToList();
                var userIds = noticeUserIds.Select(s => s.UserId.GetValueOrDefault(0)).ToList();
                var appUsers = _context.ApplicationUser.Select(s => new { s.UserId, s.UserName }).Where(w => userIds.Contains(w.UserId)).ToList();
                notice.ForEach(s =>
                {
                    var userIds = new List<long?>();
                    var userGroupIds = new List<long?>();
                    userIds = noticeUserIds.Where(w => w.NoticeId == s.NoticeId && w.UserId != null).Select(u => u.UserId).ToList();
                    userGroupIds = noticeUserIds.Where(w => w.NoticeId == s.NoticeId && w.UserGroupId != null).Select(u => u.UserGroupId).Distinct().ToList();
                    var userTypes = userGroupIds != null && userGroupIds.Count > 0 ? "user-group" : "user";
                    NoticeModel noticeModel = new NoticeModel
                    {
                        NoticeId = s.NoticeId,
                        Name = s.Name,
                        Description = s.Description,
                        Link = s.Link,
                        StatusCodeID = s.StatusCodeId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        IsLogin = s.IsLogin,
                        ModuleId = s.ModuleId,
                        ModuleName = s.Module?.CodeValue,
                        UserIDs = userTypes == "user" ? userIds : new List<long?>(),
                        UserGroupIDs = userTypes == "user-group" ? userGroupIds : new List<long?>(),
                        TypeId = s.TypeId,
                        TypeName = s.Type?.Value,
                        UserTypes = userGroupIds != null && userGroupIds.Count > 0 ? "user-group" : "user",

                    };
                    noticeModels.Add(noticeModel);
                });
            }
            return noticeModels.OrderByDescending(a => a.NoticeId).ToList();
        }
        [HttpPost]
        [Route("GetNoticeSearch")]
        [Authorize(Roles = "Admin")]
        public List<NoticeModel> GetNoticeSearch(NoticeModel searchData)
        {
            var notice = _context.NoticeLine.Include(a => a.AddedByUser).Include(a => a.Frequency).Include(a => a.NoticeLineWeekly).Include("NoticeLineWeekly.Weekly").Include(a => a.Notice).Include(a => a.Notice.Type).Include(a => a.Notice.Module).Include(s => s.Notice.NoticeUser).Include(t => t.Company).Include(m => m.ModifiedByUser).Include("StatusCode").Where(w => w.StatusCodeId == 1 && w.ModuleId == searchData.ModuleId);
            notice = notice.Where(w => ((DateTime.Now.Date >= w.StartDate.Value.Date && DateTime.Now.Date <= w.EndDate.Value.Date) || (DateTime.Now.Date >= w.StartDate.Value.Date && DateTime.Now.Date <= w.EndDate.Value.Date)) || w.EndDate == null);
            if (!string.IsNullOrWhiteSpace(searchData.Name))
            {
                notice = notice.Where(w => w.Notice.Name.Contains(searchData.Name) || w.Notice.Description.Contains(searchData.Name) || w.Company.PlantCode.Contains(searchData.Name) || w.Notice.Type.Value.Contains(searchData.Name));
            }
            List<NoticeModel> noticeModels = new List<NoticeModel>();
            var notices = notice.AsNoTracking().ToList();
            notices = notices.Where(w => w.StartDate == null || w.StartDate.Value.Date <= DateTime.Now.Date).ToList();
            notices.ForEach(s =>
            {
                var show = true;
                if (s.Notice != null && s.Notice.AddedByUserId != searchData.AddedByUserID)
                {
                    if (s.Notice.NoticeUser != null && s.Notice.NoticeUser.Count > 0)
                    {
                        var useIds = s.Notice.NoticeUser.Where(a => a.UserId == searchData.AddedByUserID).Count();
                        show = useIds > 0 ? true : false;
                    }
                }
                if (show == true)
                {
                    if (s.Frequency != null &&  s.Frequency.CodeValue == "Monthly" && s.MonthlyDay != null)
                    {
                        DateTime dateTime = DateTime.Now;
                        var date = new DateTime(dateTime.Year, dateTime.Month, s.MonthlyDay.Value);
                        if (date.Date == dateTime.Date)
                        {
                            if (s.EndDate == null || date.Date <= s.EndDate.Value.Date)
                            {
                                noticeModels.Add(GenerateNoticeModel(s));
                            }
                        }
                    }
                    else if (s.Frequency!=null && s.Frequency.CodeValue == "Weekly" && s.NoticeLineWeekly != null)
                    {
                        DateTime dateTime = DateTime.Now;
                        var weekList = s.NoticeLineWeekly.Where(w => w.CustomType == "Weekly" && w.Weekly?.CodeValue == dateTime.DayOfWeek.ToString()).Select(s => s.Weekly.CodeValue).ToList();
                        if (weekList.Count > 0)
                        {
                            if (s.EndDate == null || dateTime.Date <= s.EndDate.Value.Date)
                            {
                                noticeModels.Add(GenerateNoticeModel(s));
                            }
                        }
                    }
                    else if (s.Frequency != null && s.Frequency.CodeValue == "Yearly")
                    {
                        DateTime dateTime = DateTime.Now;
                        if (dateTime.Date == s.StartDate.Value.Date)
                        {
                            if (s.EndDate == null || dateTime.Date <= s.EndDate.Value.Date)
                            {
                                noticeModels.Add(GenerateNoticeModel(s));
                            }
                        }
                        else
                        {
                            DateTime addYear = s.StartDate.Value.AddYears(1);
                            if (dateTime.Date == addYear.Date)
                            {
                                if (s.DaysOfWeek == true)
                                {
                                    DateTime dateTimes = DateTime.Now;
                                    var weekList = s.NoticeLineWeekly.Where(w => w.CustomType == "Yearly" && w.Weekly?.CodeValue == dateTimes.DayOfWeek.ToString()).Select(s => s.Weekly.CodeValue).ToList();
                                    if (weekList.Count > 0)
                                    {
                                        if (s.EndDate == null || dateTimes.Date <= s.EndDate.Value.Date)
                                        {
                                            noticeModels.Add(GenerateNoticeModel(s));
                                        }
                                    }
                                }
                                else
                                {
                                    if (s.EndDate == null || dateTime.Date <= s.EndDate.Value.Date)
                                    {
                                        noticeModels.Add(GenerateNoticeModel(s));
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        noticeModels.Add(GenerateNoticeModel(s));
                    }

                }
            });
            return noticeModels.OrderByDescending(a => a.NoticeId).ToList();
        }
        private NoticeModel GenerateNoticeModel(NoticeLine s)
        {
            NoticeModel noticeModel = new NoticeModel
            {
                NoticeId = s.Notice.NoticeId,
                Name = s.Notice.Name,
                Description = s.Notice.Description,
                Link = s.Link,
                StatusCodeID = s.StatusCodeId,
                StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                AddedByUserID = s.AddedByUserId,
                ModifiedByUserID = s.ModifiedByUserId,
                AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                AddedDate = s.AddedDate,
                ModifiedDate = s.ModifiedDate,
                IsLogin = s.Notice?.IsLogin,
                ModuleId = s.ModuleId,
                Wilink = s.Wilink,
                ModuleName = s.Module?.CodeValue,
                TypeId = s.Notice?.TypeId,
                TypeName = s.Notice?.Type?.Value,
                CompanyName = s.Company?.PlantCode,
                UserIDs = s.Notice?.NoticeUser != null ? s.Notice?.NoticeUser?.Where(w => w.NoticeId == s.NoticeId).Select(s => s.UserId).ToList() : new List<long?>(),
            };
            return noticeModel;
        }
        private string GetNoticesLogInNameDes(string name, string desc, int i, int noticeCount)
        {
            var description = name + "-" + desc;
            if (i != noticeCount)
            {
                description += "&nbsp;&nbsp;&nbsp;&nbsp;";
            }
            return description;
        }
        [HttpGet]
        [Route("GetNoticesLogIn")]
        public NoticeModel GetNoticesLogIn()
        {
            var notices = _context.NoticeLine.Include(a => a.Frequency).Include(a => a.NoticeLineWeekly).Include("NoticeLineWeekly.Weekly").Include(s => s.Notice).Where(w => w.ModuleId == 3191 && w.Notice.StatusCodeId == 1);
            notices = notices.Where(w => ((DateTime.Now.Date >= w.StartDate.Value.Date && DateTime.Now.Date <= w.EndDate.Value.Date) || (DateTime.Now.Date >= w.StartDate.Value.Date && DateTime.Now.Date <= w.EndDate.Value.Date)) || w.EndDate == null);
            var notice = notices.AsNoTracking().ToList();
            NoticeModel noticeModels = new NoticeModel();
            int i = 1;
            notice = notice.Where(w => w.StartDate == null || w.StartDate.Value.Date <= DateTime.Now.Date).ToList();
            var noticeCount = notice.Count();
            notice.ForEach(s =>
            {
                if (s.Frequency != null && s.Frequency.CodeValue == "Monthly" && s.MonthlyDay != null)
                {
                    DateTime dateTime = DateTime.Now;
                    var date = new DateTime(dateTime.Year, dateTime.Month, s.MonthlyDay.Value);
                    if (date.Date == dateTime.Date)
                    {
                        if (s.EndDate == null || date.Date <= s.EndDate.Value.Date)
                        {
                            noticeModels.Description += GetNoticesLogInNameDes(s.Notice?.Name, s.Notice?.Description, i, noticeCount);
                        }
                    }
                }
                else if (s.Frequency != null &&  s.Frequency.CodeValue == "Weekly" && s.NoticeLineWeekly != null)
                {
                    DateTime dateTime = DateTime.Now;
                    var weekList = s.NoticeLineWeekly.Where(w => w.CustomType == "Weekly" && w.Weekly?.CodeValue == dateTime.DayOfWeek.ToString()).Select(s => s.Weekly.CodeValue).ToList();
                    if (weekList.Count > 0)
                    {
                        if (s.EndDate == null || dateTime.Date <= s.EndDate.Value.Date)
                        {
                            noticeModels.Description += GetNoticesLogInNameDes(s.Notice?.Name, s.Notice?.Description, i, noticeCount);
                        }
                    }
                }
                else if (s.Frequency != null && s.Frequency.CodeValue == "Yearly")
                {
                    DateTime dateTime = DateTime.Now;
                    if (dateTime.Date == s.StartDate.Value.Date)
                    {
                        if (s.EndDate == null || dateTime.Date <= s.EndDate.Value.Date)
                        {
                            noticeModels.Description += GetNoticesLogInNameDes(s.Notice?.Name, s.Notice?.Description, i, noticeCount);
                        }
                    }
                    else
                    {
                        DateTime addYear = s.StartDate.Value.AddYears(1);
                        if (dateTime.Date == addYear.Date)
                        {
                            if (s.DaysOfWeek == true)
                            {
                                DateTime dateTimes = DateTime.Now;
                                var weekList = s.NoticeLineWeekly.Where(w => w.CustomType == "Yearly" && w.Weekly?.CodeValue == dateTimes.DayOfWeek.ToString()).Select(s => s.Weekly.CodeValue).ToList();
                                if (weekList.Count > 0)
                                {
                                    if (s.EndDate == null || dateTimes.Date <= s.EndDate.Value.Date)
                                    {
                                        noticeModels.Description += GetNoticesLogInNameDes(s.Notice?.Name, s.Notice?.Description, i, noticeCount);
                                    }
                                }
                            }
                            else
                            {
                                if (s.EndDate == null || dateTime.Date <= s.EndDate.Value.Date)
                                {
                                    noticeModels.Description += GetNoticesLogInNameDes(s.Notice?.Name, s.Notice?.Description, i, noticeCount);
                                }
                            }
                        }
                    }
                }
                else
                {
                    noticeModels.Description += GetNoticesLogInNameDes(s.Notice?.Name, s.Notice?.Description, i, noticeCount);
                }
                i++;
            });
            return noticeModels;
        }
        [HttpPost()]
        [Route("GetData")]
        [Authorize(Roles = "Admin")]
        public ActionResult<NoticeModel> GetData(SearchModel searchModel)
        {
            var notice = new Notice();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        notice = _context.Notice.OrderByDescending(o => o.NoticeId).FirstOrDefault();
                        break;
                    case "Last":
                        notice = _context.Notice.OrderByDescending(o => o.NoticeId).LastOrDefault();
                        break;
                    case "Next":
                        notice = _context.Notice.OrderByDescending(o => o.NoticeId).LastOrDefault();
                        break;
                    case "Previous":
                        notice = _context.Notice.OrderByDescending(o => o.NoticeId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        notice = _context.Notice.OrderByDescending(o => o.NoticeId).FirstOrDefault();
                        break;
                    case "Last":
                        notice = _context.Notice.OrderByDescending(o => o.NoticeId).LastOrDefault();
                        break;
                    case "Next":
                        notice = _context.Notice.OrderBy(o => o.NoticeId).FirstOrDefault(s => s.NoticeId > searchModel.Id);
                        break;
                    case "Previous":
                        notice = _context.Notice.OrderByDescending(o => o.NoticeId).FirstOrDefault(s => s.NoticeId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<NoticeModel>(notice);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertNotice")]
        [Authorize(Roles = "Admin")]
        public NoticeModel Post(NoticeModel value)
        {
            var notice = new Notice
            {
                Name = value.Name,
                Link = value.Link,
                Description = value.Description,
                AddedByUserId = value.AddedByUserID.Value,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                IsLogin = value.IsLogin,
                ModuleId = value.ModuleId,
                TypeId = value.TypeId,

            };
            if (value.UserTypes == "user")
            {
                if (value.UserIDs != null && value.UserIDs.Count > 0)
                {
                    value.UserIDs.ForEach(s =>
                    {
                        var noticeUsers = new NoticeUser
                        {
                            UserId = s,
                        };
                        notice.NoticeUser.Add(noticeUsers);
                    });
                }
            }
            if (value.UserTypes == "user-group")
            {
                if (value.UserIDs != null && value.UserIDs.Count > 0)
                {
                    var userGroupUserList = _context.UserGroupUser.Where(u => value.UserGroupIDs.Contains(u.UserGroupId)).Distinct().ToList();
                    userGroupUserList.ForEach(s =>
                    {
                        var noticeUsers = new NoticeUser
                        {
                            UserGroupId = s.UserGroupId,
                            UserId = s.UserId,
                        };
                        notice.NoticeUser.Add(noticeUsers);
                    });
                }
            }
            _context.Notice.Add(notice);
            _context.SaveChanges();
            value.NoticeId = notice.NoticeId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateNotice")]
        [Authorize(Roles = "Admin")]

        public NoticeModel Put(NoticeModel value)
        {
            var notice = _context.Notice.SingleOrDefault(p => p.NoticeId == value.NoticeId);
            notice.Description = value.Description;
            notice.Name = value.Name;
            notice.Link = value.Link;
            notice.ModifiedByUserId = value.ModifiedByUserID;
            notice.ModifiedDate = DateTime.Now;
            notice.StatusCodeId = value.StatusCodeID.Value;
            notice.IsLogin = value.IsLogin;
            notice.ModuleId = value.ModuleId;
            notice.TypeId = value.TypeId;

            var noticeUserRemove = _context.NoticeUser.Where(w => w.NoticeId == value.NoticeId).ToList();
            if (noticeUserRemove != null)
            {
                _context.NoticeUser.RemoveRange(noticeUserRemove);
                _context.SaveChanges();
            }
            if (value.UserTypes == "user")
            {
                if (value.UserIDs.Count > 0)
                {
                    value.UserIDs.ForEach(s =>
                    {
                        var noticeUsers = new NoticeUser
                        {
                            NoticeId = value.NoticeId,
                            UserId = s,
                        };
                        _context.NoticeUser.Add(noticeUsers);
                    });
                }
            }
            if (value.UserTypes == "user-group")
            {
                if (value.UserGroupIDs.Count > 0)
                {
                    var userGroupUserList = _context.UserGroupUser.Where(u => value.UserGroupIDs.Contains(u.UserGroupId)).Distinct().ToList();
                    userGroupUserList.ForEach(s =>
                    {
                        var noticeUsers = new NoticeUser
                        {
                            NoticeId = value.NoticeId,
                            UserGroupId = s.UserGroupId,
                            UserId = s.UserId,
                        };
                        _context.NoticeUser.Add(noticeUsers);
                    });
                }
            }
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteNotice")]
        [Authorize(Roles = "Admin")]

        public void Delete(int id)
        {
            try
            {
                var division = _context.Notice.SingleOrDefault(p => p.NoticeId == id);
                if (division != null)
                {
                    var noticeUserRemove = _context.NoticeUser.Where(w => w.NoticeId == division.NoticeId).ToList();
                    if (noticeUserRemove != null)
                    {
                        _context.NoticeUser.RemoveRange(noticeUserRemove);
                        _context.SaveChanges();
                    }
                    var noticeLine = _context.NoticeLine.Where(W => W.NoticeId == id).ToList();
                    if (noticeLine != null)
                    {
                        noticeLine.ForEach(s =>
                        {
                            var noti = _context.NoticeLineWeekly.Where(w => w.NoticeLineId == s.NoticeLineId).ToList();
                            _context.NoticeLineWeekly.RemoveRange(noti);
                            _context.SaveChanges();
                        });

                        _context.NoticeLine.RemoveRange(noticeLine);
                        _context.SaveChanges();
                    }
                    _context.Notice.Remove(division);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("Notice Cannot be Delete! Reference to Others", ex);
            }
        }
    }
}
