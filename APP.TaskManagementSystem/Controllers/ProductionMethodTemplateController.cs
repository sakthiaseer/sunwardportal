﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ProductionMethodTemplateController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        public ProductionMethodTemplateController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetProductionMethodTemplates")]
        public List<ProductionMethodTemplateModel> Get()
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.AsNoTracking().ToList();
            var ictmasterlist = _context.Ictmaster.AsNoTracking().ToList();
            List<ProductionMethodTemplateModel> productionMethodTemplateModels = new List<ProductionMethodTemplateModel>();
            var productionMethodTemplates = _context.ProductionMethodTemplate.Include("AddedByUser").Include("ModifiedByUser").OrderByDescending(o => o.ProductionMethodTemplateId).AsNoTracking().ToList();
            if(productionMethodTemplates!=null && productionMethodTemplates.Count>0)
            {
                productionMethodTemplates.ForEach(s =>
                {
                    ProductionMethodTemplateModel productionMethodTemplateModel = new ProductionMethodTemplateModel
                    {
                        ProductionMethodTemplateID = s.ProductionMethodTemplateId,
                        ICMasterOperationID = s.IcmasterOperationId,
                        ProfileID = s.ProfileId,
                        RefNo = s.RefNo,
                        LocationID = s.LocationId,
                        MachineID = s.MachineId,
                        ManPower = s.ManPower,
                        HoursMinutes = s.HoursMinutes,
                        IsEachNewDay = s.IsEachNewDay,
                        IsEndOfTheDay = s.IsEndOfTheDay,
                        IsNewBatch = s.IsNewBatch,
                        IsNewSubLot = s.IsNewSubLot,
                        IdlingForXDays = s.IdlingForXdays,
                        IdlingForXhours = s.IdlingForXhours,
                        CampaignByXBatches = s.CampaignByXbatches,
                        ChangeShift = s.ChangeShift,
                        Location = s.Location!=null? s.Location.Name : "",                      
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate,
                        OperationName = s.IcmasterOperation!=null? s.IcmasterOperation.MasterOperation : "",

                        ManufacturingSiteIDs = s.ProductionMethodManufacturing.Where(t => t.ProductionMethodTemplateId == s.ProductionMethodTemplateId).Select(t => t.ManufacturingSiteId).ToList(),
                    };
                    productionMethodTemplateModels.Add(productionMethodTemplateModel);
                });
            }
           
            return productionMethodTemplateModels;
        }


        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ProductionMethodTemplateModel> GetData(SearchModel searchModel)
        {
            var productionMethodTemplates = new ProductionMethodTemplate();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionMethodTemplates = _context.ProductionMethodTemplate.OrderByDescending(o => o.ProductionMethodTemplateId).FirstOrDefault();
                        break;
                    case "Last":
                        productionMethodTemplates = _context.ProductionMethodTemplate.OrderByDescending(o => o.ProductionMethodTemplateId).LastOrDefault();
                        break;
                    case "Next":
                        productionMethodTemplates = _context.ProductionMethodTemplate.OrderByDescending(o => o.ProductionMethodTemplateId).LastOrDefault();
                        break;
                    case "Previous":
                        productionMethodTemplates = _context.ProductionMethodTemplate.OrderByDescending(o => o.ProductionMethodTemplateId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        productionMethodTemplates = _context.ProductionMethodTemplate.OrderByDescending(o => o.ProductionMethodTemplateId).FirstOrDefault();
                        break;
                    case "Last":
                        productionMethodTemplates = _context.ProductionMethodTemplate.OrderByDescending(o => o.ProductionMethodTemplateId).LastOrDefault();
                        break;
                    case "Next":
                        productionMethodTemplates = _context.ProductionMethodTemplate.OrderBy(o => o.ProductionMethodTemplateId).FirstOrDefault(s => s.ProductionMethodTemplateId > searchModel.Id);
                        break;
                    case "Previous":
                        productionMethodTemplates = _context.ProductionMethodTemplate.OrderByDescending(o => o.ProductionMethodTemplateId).FirstOrDefault(s => s.ProductionMethodTemplateId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ProductionMethodTemplateModel>(productionMethodTemplates);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertProductionMethodTemplate")]
        public ProductionMethodTemplateModel Post(ProductionMethodTemplateModel value)
        {
            var productionMethodTemplates = new ProductionMethodTemplate
            {

                IcmasterOperationId = value.ICMasterOperationID,
                ProfileId = value.ProfileID,
                RefNo = value.RefNo,
                LocationId = value.LocationID,
                MachineId = value.MachineID,
                ManPower = value.ManPower,
                 HoursMinutes = value.HoursMinutes,
                IsEachNewDay = value.IsEachNewDay,
                IsEndOfTheDay = value.IsEndOfTheDay,
                IsNewBatch = value.IsNewBatch,
                IsNewSubLot = value.IsNewSubLot,
                IdlingForXdays = value.IdlingForXDays,
                IdlingForXhours = value.IdlingForXhours,
                CampaignByXbatches = value.CampaignByXBatches,
                ChangeShift = value.ChangeShift,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,

            };
            _context.ProductionMethodTemplate.Add(productionMethodTemplates);
            _context.SaveChanges();
            value.ProductionMethodTemplateID = productionMethodTemplates.ProductionMethodTemplateId;
            if(value.ManufacturingSiteIDs!=null && value.ManufacturingSiteIDs.Count>0)
            {
                value.ManufacturingSiteIDs.ForEach(m => {
                    var existing = _context.ProductionMethodManufacturing.Where(p => p.ProductionMethodTemplateId == value.ProductionMethodTemplateID && p.ManufacturingSiteId == m).FirstOrDefault();
                    if (existing == null)
                    {
                        var productionMethodManufacturing = new ProductionMethodManufacturing
                        {
                            ProductionMethodTemplateId = value.ProductionMethodTemplateID,
                            ManufacturingSiteId = m,
                        };

                        _context.ProductionMethodManufacturing.Add(productionMethodManufacturing);
                        _context.SaveChanges();
                    }
                });
            }
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateProductionMethodTemplate")]
        public ProductionMethodTemplateModel Put(ProductionMethodTemplateModel value)
        {
            var productionMethodTemplates = _context.ProductionMethodTemplate.SingleOrDefault(p => p.ProductionMethodTemplateId == value.ProductionMethodTemplateID);

            productionMethodTemplates.ProfileId = value.ProfileID;
            productionMethodTemplates.RefNo = value.RefNo;
            productionMethodTemplates.IcmasterOperationId = value.ICMasterOperationID;
            productionMethodTemplates.LocationId = value.LocationID;
            productionMethodTemplates.MachineId = value.MachineID;
            productionMethodTemplates.ManPower = value.ManPower;
            productionMethodTemplates.HoursMinutes = value.HoursMinutes;
            productionMethodTemplates.IsEachNewDay = value.IsEachNewDay;
            productionMethodTemplates.IsEndOfTheDay = value.IsEndOfTheDay;
            productionMethodTemplates.IsNewBatch = value.IsNewBatch;
            productionMethodTemplates.IsNewSubLot = value.IsNewSubLot;
            productionMethodTemplates.IdlingForXdays = value.IdlingForXDays;
            productionMethodTemplates.IdlingForXhours = value.IdlingForXhours;
            productionMethodTemplates.CampaignByXbatches = value.CampaignByXBatches;
            productionMethodTemplates.ChangeShift = value.ChangeShift;
            productionMethodTemplates.ModifiedByUserId = value.ModifiedByUserID;
            productionMethodTemplates.ModifiedDate = DateTime.Now;
            productionMethodTemplates.StatusCodeId = value.StatusCodeID.Value;
            value.ProductionMethodTemplateID = productionMethodTemplates.ProductionMethodTemplateId;
            if (value.ManufacturingSiteIDs != null && value.ManufacturingSiteIDs.Count > 0)
            {
                value.ManufacturingSiteIDs.ForEach(m => {
                    var existing = _context.ProductionMethodManufacturing.Where(p => p.ProductionMethodTemplateId == value.ProductionMethodTemplateID && p.ManufacturingSiteId == m).FirstOrDefault();
                    if (existing == null)
                    {
                        var productionMethodManufacturing = new ProductionMethodManufacturing
                        {
                            ProductionMethodTemplateId = value.ProductionMethodTemplateID,
                            ManufacturingSiteId = m,
                        };

                        _context.ProductionMethodManufacturing.Add(productionMethodManufacturing);
                        _context.SaveChanges();
                    }
                });
            }
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteProductionMethodTemplate")]
        public void Delete(int id)
        {
            var productionMethodTemplates = _context.ProductionMethodTemplate.SingleOrDefault(p => p.ProductionMethodTemplateId == id);
            if (productionMethodTemplates != null)
            {
                var productionMethodManufacturinglist = _context.ProductionMethodManufacturing.Where(p => p.ProductionMethodTemplateId == id).AsNoTracking().ToList();
                if(productionMethodManufacturinglist !=null && productionMethodManufacturinglist.Count>0)
                {
                    _context.ProductionMethodManufacturing.RemoveRange(productionMethodManufacturinglist);
                    _context.SaveChanges();
                }
                _context.ProductionMethodTemplate.Remove(productionMethodTemplates);
                _context.SaveChanges();
            }
        }
    }
}