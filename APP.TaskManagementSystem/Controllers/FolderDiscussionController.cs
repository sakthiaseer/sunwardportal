﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class FolderDiscussionController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public FolderDiscussionController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Project
        [HttpGet]
        [Route("GetFolderDiscussions")]
        public List<FolderDiscussionModel> Get(long Id, long userId)
        {
            List<FolderDiscussionModel> folderDiscussionModels = new List<FolderDiscussionModel>();
            var documentIds = new List<long?>();
            var docuFolderId = _context.DocumentFolder.Where(i => i.DocumentId == Id).Select(s => s.PreviousDocumentId).FirstOrDefault();
            if (docuFolderId != null)
                documentIds = _context.DocumentFolder.Where(i => i.PreviousDocumentId == docuFolderId || i.DocumentFolderId == docuFolderId).AsNoTracking().Select(s => s.DocumentId).ToList();

            documentIds.Add(Id);
            var documents = _context.Documents.Select(s => new { s.DocumentId, s.SessionId, s.FileName, s.ContentType }).AsNoTracking().ToList();
            var folderDiscussion = _context.FolderDiscussion.Include(f=>f.DiscussedByNavigation).Include(f=>f.FolderDiscussionUser).Include(f=>f.FolderDiscussionAttachment).Include("FolderDiscussionAttachment.Document").Where(t => documentIds.Contains(t.DocumentId) && t.ParentDiscussionNotesId == null && t.IsDocument == true).AsNoTracking().OrderByDescending(o => o.DiscussionNotesId).ToList();
            folderDiscussion.ForEach(s =>
            {
                FolderDiscussionModel folderDiscussionModel = new FolderDiscussionModel
                {
                    DiscussionNotesID = s.DiscussionNotesId,
                    FolderID = s.FolderId,
                    DiscussionSubject = s.DiscussionSubject,
                    DiscussionNotes = s.DiscussionNotes,
                    DiscussedBy = s.DiscussedBy,
                    AddedByUser = s.DiscussedByNavigation?.UserName,
                    DiscussionDate = s.DiscussionDate.Value,
                    ParentDiscussionNotesID = s.ParentDiscussionNotesId,
                    IsEdited = s.IsEdited,
                    IsRead = s.FolderDiscussionUser?.Any(u => u.IsRead == false && u.UserId == userId),
                    EditedBy = s.EditedBy,
                    EditedDate = s.EditedDate,

                    StatusCodeID = s.StatusCodeId.Value,
                    FolderDiscussionModels = new List<FolderDiscussionModel>(),
                    FolderDiscussionAttachemntsModels = new FolderDiscussionAttachmentModel
                    {
                        DocumentId = s.FolderDiscussionAttachment.FirstOrDefault() != null ? s.FolderDiscussionAttachment.FirstOrDefault().DocumentId : 0,
                        FileName = s.FolderDiscussionAttachment.FirstOrDefault() != null ? s.FolderDiscussionAttachment.FirstOrDefault().FileName : string.Empty,
                        FileNames = s.FolderDiscussionAttachment?.Where(c => c.FolderDiscussionId == s.DiscussionNotesId).Select(c => c.FileName).ToList(),
                        DocumentIdList = s.FolderDiscussionAttachment?.Where(c => c.FolderDiscussionId == s.DiscussionNotesId).Select(c => c.DocumentId).ToList(),
                        
                        FileDocumentList = s.FolderDiscussionAttachment.Where(c => c.FolderDiscussionId == s.DiscussionNotesId).Select(t => new DocumentsModel
                        {
                            DocumentID = t.DocumentId.HasValue ? t.DocumentId.Value : 0,
                            FileName = documents.FirstOrDefault(d=>d.DocumentId ==t.DocumentId)?.FileName,
                            ContentType = documents.FirstOrDefault(d => d.DocumentId == t.DocumentId)?.ContentType,

                        }).ToList(),
                        ContentTypes = documents.Select(c => c.ContentType).ToList(),
                    },


                };
                folderDiscussionModels.Add(folderDiscussionModel);
            });

            folderDiscussionModels.ForEach(sc =>
            {
                List<FolderDiscussionModel> subDiscussionModels = new List<FolderDiscussionModel>();
                var subMessages = _context.FolderDiscussion.Include(f => f.DiscussedByNavigation).Include(f => f.FolderDiscussionUser).Include(f => f.FolderDiscussionAttachment).Include("FolderDiscussionAttachment.Document").Where(t => t.ParentDiscussionNotesId == sc.DiscussionNotesID).OrderBy(o => o.DiscussionNotesId).ToList();
                subMessages.ForEach(s =>
                {
                    FolderDiscussionModel folderDiscussionModel = new FolderDiscussionModel
                    {
                        DiscussionNotesID = s.DiscussionNotesId,
                        FolderID = s.FolderId,
                        DiscussionSubject = s.DiscussionSubject,
                        DiscussedBy = s.DiscussedBy,
                        AddedByUser = s.DiscussedByNavigation?.UserName,
                        DiscussionDate = s.DiscussionDate.Value,
                        DiscussionNotes = s.DiscussionNotes,
                        ParentDiscussionNotesID = s.ParentDiscussionNotesId,
                        IsEdited = s.IsEdited,
                        IsRead = s.FolderDiscussionUser.Any(u => u.IsRead == false && u.UserId == userId),
                        EditedBy = s.EditedBy,
                        EditedDate = s.EditedDate,
                        StatusCodeID = s.StatusCodeId.Value,
                        FolderDiscussionAttachemntsModels = new FolderDiscussionAttachmentModel
                        {
                            DocumentId = s.FolderDiscussionAttachment.FirstOrDefault() != null ? s.FolderDiscussionAttachment.FirstOrDefault().DocumentId : 0,
                            FileName = s.FolderDiscussionAttachment.FirstOrDefault() != null ? s.FolderDiscussionAttachment.FirstOrDefault().FileName : string.Empty,
                            FileNames = s.FolderDiscussionAttachment?.Where(c => c.FolderDiscussionId == s.DiscussionNotesId).Select(c => c.FileName).ToList(),
                            DocumentIdList = s.FolderDiscussionAttachment?.Where(c => c.FolderDiscussionId == s.DiscussionNotesId).Select(c => c.DocumentId).ToList(),
                            FileDocumentList = s.FolderDiscussionAttachment?.Where(c => c.FolderDiscussionId == s.DiscussionNotesId).Select(t => new DocumentsModel
                            {
                                DocumentID = t.DocumentId.HasValue ? t.DocumentId.Value : 0,
                                FileName = documents.FirstOrDefault(d => d.DocumentId == t.DocumentId)?.FileName,
                                ContentType = documents.FirstOrDefault(d => d.DocumentId == t.DocumentId)?.ContentType,

                            }).ToList(),
                            ContentTypes = documents.Select(c => c.ContentType).ToList(),
                        },
                    };
                    subDiscussionModels.Add(folderDiscussionModel);
                });


                sc.FolderDiscussionModels = subDiscussionModels;

            });

            return folderDiscussionModels;
        }

        [HttpGet]
        [Route("GetDocDiscussion")]
        public List<FolderDiscussionModel> GetDocDiscussion(long Id, long userId)
        {
            var documents = _context.Documents.Select(s => new { s.DocumentId, s.SessionId, s.FileName, s.ContentType }).AsNoTracking().ToList();

            List<FolderDiscussionModel> folderDiscussionModels = new List<FolderDiscussionModel>();
            var folderDiscussion = _context.FolderDiscussion.Include(f => f.DiscussedByNavigation).Include(f => f.FolderDiscussionUser).Include(f => f.FolderDiscussionAttachment)
                                    
                                    .Where(t => t.DocumentId == Id && t.ParentDiscussionNotesId == null && t.IsDocument == true)
                                    .OrderByDescending(o => o.DiscussionNotesId).AsNoTracking().ToList();
            folderDiscussion.ForEach(s =>
            {
                FolderDiscussionModel folderDiscussionModel = new FolderDiscussionModel
                {
                    DiscussionNotesID = s.DiscussionNotesId,
                    FolderID = s.FolderId,
                    DiscussionSubject = s.DiscussionSubject,
                    DiscussionNotes = s.DiscussionNotes,
                    DiscussedBy = s.DiscussedBy,
                    AddedByUser = s.DiscussedByNavigation?.UserName,
                    DiscussionDate = s.DiscussionDate.Value,
                    ParentDiscussionNotesID = s.ParentDiscussionNotesId,
                    IsEdited = s.IsEdited,
                    IsRead = s.FolderDiscussionUser?.Any(u => u.IsRead == false && u.UserId == userId),
                    EditedBy = s.EditedBy,
                    EditedDate = s.EditedDate,

                    StatusCodeID = s.StatusCodeId.Value,
                    FolderDiscussionModels = new List<FolderDiscussionModel>(),
                    FolderDiscussionAttachemntsModels = new FolderDiscussionAttachmentModel
                    {
                        DocumentId = s.FolderDiscussionAttachment.FirstOrDefault() != null ? s.FolderDiscussionAttachment.FirstOrDefault().DocumentId : 0,
                        FileName = s.FolderDiscussionAttachment.FirstOrDefault() != null ? s.FolderDiscussionAttachment.FirstOrDefault().FileName : string.Empty,
                        FileNames = s.FolderDiscussionAttachment?.Where(c => c.FolderDiscussionId == s.DiscussionNotesId).Select(c => c.FileName).ToList(),
                        DocumentIdList = s.FolderDiscussionAttachment?.Where(c => c.FolderDiscussionId == s.DiscussionNotesId).Select(c => c.DocumentId).ToList(),
                        FileDocumentList = s.FolderDiscussionAttachment?.Where(c => c.FolderDiscussionId == s.DiscussionNotesId).Select(t => new DocumentsModel
                        {
                            DocumentID = t.DocumentId.HasValue ? t.DocumentId.Value : 0,
                            FileName = documents.FirstOrDefault(d => d.DocumentId == t.DocumentId)?.FileName,
                            ContentType = documents.FirstOrDefault(d => d.DocumentId == t.DocumentId)?.ContentType,

                        }).ToList(),
                        ContentTypes = documents.Select(c => c.ContentType).ToList(),
                    },

                };
                folderDiscussionModels.Add(folderDiscussionModel);
            });


            folderDiscussionModels.ForEach(sc =>
            {
                List<FolderDiscussionModel> subFolderDiscussionModels = new List<FolderDiscussionModel>();
                var subMessages = _context.FolderDiscussion.Include(f => f.DiscussedByNavigation).Include(f => f.FolderDiscussionUser).Include(f => f.FolderDiscussionAttachment)
                                .Where(t => t.ParentDiscussionNotesId == sc.DiscussionNotesID)
                                .AsNoTracking().OrderBy(o => o.DiscussionNotesId).ToList();
                subMessages.ForEach(s =>
                {
                    FolderDiscussionModel folderDiscussionModel = new FolderDiscussionModel
                    {
                        DiscussionNotesID = s.DiscussionNotesId,
                        FolderID = s.FolderId,
                        DiscussionSubject = s.DiscussionSubject,
                        DiscussedBy = s.DiscussedBy,
                        AddedByUser = s.DiscussedByNavigation?.UserName,
                        DiscussionDate = s.DiscussionDate.Value,
                        DiscussionNotes = s.DiscussionNotes,
                        ParentDiscussionNotesID = s.ParentDiscussionNotesId,
                        IsEdited = s.IsEdited,
                        IsRead = s.FolderDiscussionUser?.Any(u => u.IsRead == false && u.UserId == userId),
                        EditedBy = s.EditedBy,
                        EditedDate = s.EditedDate,
                        StatusCodeID = s.StatusCodeId.Value
                    };
                    subFolderDiscussionModels.Add(folderDiscussionModel);
                });

                sc.FolderDiscussionModels = subFolderDiscussionModels;

            });

            return folderDiscussionModels;
        }

        //[HttpGet]
        //[Route("GetFolderDiscussionsByID")]
        //public ActionResult<FolderDiscussionModel> GetFolderDiscussion(int id, int userId)
        //{
        //    var folderDiscussion = _context.FolderDiscussion.SingleOrDefault(t => t.FolderId == id && t.DiscussedBy == userId);
        //    var result = _mapper.Map<FolderDiscussionModel>(folderDiscussion);
        //    return result;
        //}
        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get FolderDiscussion")]
        [HttpGet("GetFolderDiscussions/{id:int}")]
        public ActionResult<FolderDiscussionModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var folderDiscussion = _context.FolderDiscussion.SingleOrDefault(p => p.DiscussionNotesId == id.Value);
            var result = _mapper.Map<FolderDiscussionModel>(folderDiscussion);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<FolderDiscussionModel> GetData(SearchModel searchModel)
        {
            var folderDiscussion = new FolderDiscussion();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        folderDiscussion = _context.FolderDiscussion.OrderByDescending(o => o.DiscussionNotesId).FirstOrDefault();
                        break;
                    case "Last":
                        folderDiscussion = _context.FolderDiscussion.OrderByDescending(o => o.DiscussionNotesId).LastOrDefault();
                        break;
                    case "Next":
                        folderDiscussion = _context.FolderDiscussion.OrderByDescending(o => o.DiscussionNotesId).LastOrDefault();
                        break;
                    case "Previous":
                        folderDiscussion = _context.FolderDiscussion.OrderByDescending(o => o.DiscussionNotesId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        folderDiscussion = _context.FolderDiscussion.OrderByDescending(o => o.DiscussionNotesId).FirstOrDefault();
                        break;
                    case "Last":
                        folderDiscussion = _context.FolderDiscussion.OrderByDescending(o => o.DiscussionNotesId).LastOrDefault();
                        break;
                    case "Next":
                        folderDiscussion = _context.FolderDiscussion.OrderBy(o => o.DiscussionNotesId).FirstOrDefault(s => s.DiscussionNotesId > searchModel.Id);
                        break;
                    case "Previous":
                        folderDiscussion = _context.FolderDiscussion.OrderByDescending(o => o.DiscussionNotesId).FirstOrDefault(s => s.DiscussionNotesId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<FolderDiscussionModel>(folderDiscussion);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertFolderDiscussion")]
        public FolderDiscussionModel Post(FolderDiscussionModel value)
        {
            var folderDiscussion = new FolderDiscussion
            {
                FolderId = value.FolderID,
                DiscussionSubject = value.DiscussionSubject,
                DiscussionNotes = value.DiscussionNotes,
                DiscussedBy = value.DiscussedBy,
                ParentDiscussionNotesId = value.ParentDiscussionNotesID,
                DocumentId = value.DocumentID,
                IsDocument = value.DocumentID != null ? true : false,
                DiscussionDate = DateTime.Now,
                IsRead = false,
                IsEdited = false,
                StatusCodeId = value.StatusCodeID.Value,
            };
            _context.FolderDiscussion.Add(folderDiscussion);

            if (value.DocumentID > 0)
            {
                var folderattach = _context.DocumentFolder.FirstOrDefault(t => t.DocumentId == value.DocumentID && t.FolderId == value.FolderID);
                if (folderattach != null)
                {
                    folderattach.IsDiscussionNotes = true;
                }
            }

            _context.SaveChanges();
            value.DiscussionNotesID = folderDiscussion.DiscussionNotesId;
            value.FolderDiscussionModels = new List<FolderDiscussionModel>();

            var status = value.DocumentID != null ? 1 : 2;
            var users = new List<long?>();

            var documents = _context.Documents.Select(s => new { s.DocumentId, s.SessionId, s.FileName, s.ContentType }).Where(d => d.SessionId == value.SessionID).AsNoTracking().ToList();
            value.FolderDiscussionAttachemntsModels = new FolderDiscussionAttachmentModel();
            if (documents.Any())
            {
                var folderAttachement = new FolderDiscussionAttachment();
                documents.ForEach(d =>
                {
                    folderAttachement = new FolderDiscussionAttachment
                    {
                        FolderDiscussionId = folderDiscussion.DiscussionNotesId,
                        DocumentId = d.DocumentId,
                        UserId = folderDiscussion.DiscussedBy.Value,
                        FileName = d.FileName
                    };
                    _context.FolderDiscussionAttachment.Add(folderAttachement);
                });
                value.FolderDiscussionAttachemntsModels = new FolderDiscussionAttachmentModel
                {
                    DocumentId = folderAttachement.DocumentId,
                    FileName = folderAttachement.FileName,
                    FileNames = documents.Where(d => d.SessionId == value.SessionID).Select(d => d.FileName).ToList(),
                    DocumentIdList = _context.FolderDiscussionAttachment.Where(c => c.FolderDiscussionId == value.DiscussionNotesID).Select(c => c.DocumentId).ToList(),
                    FileDocumentList = _context.FolderDiscussionAttachment.Where(c => c.FolderDiscussionId == value.DiscussionNotesID).Select(t => new DocumentsModel
                    {
                        DocumentID = t.DocumentId.HasValue ? t.DocumentId.Value : 0,
                        FileName = documents.FirstOrDefault(d => d.DocumentId == t.DocumentId).FileName,
                        ContentType = documents.FirstOrDefault(d => d.DocumentId == t.DocumentId).ContentType,

                    }).ToList(),
                    ContentTypes = documents.Where(d => d.SessionId == value.SessionID).Select(d => d.ContentType).ToList(),
                };
            }
            var userDiscussion = new FolderDiscussionUser
            {
                IsRead = false,
                FolderDiscussionNotesId = folderDiscussion.DiscussionNotesId,
                FolderId = value.FolderID.Value,
                UserId = value.DiscussedBy.Value,
            };
            _context.FolderDiscussionUser.Add(userDiscussion);

            _context.SaveChanges();
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateFolderDiscussion")]
        public FolderDiscussionModel Put(FolderDiscussionModel value)
        {
            var folderDiscussion = _context.FolderDiscussion.SingleOrDefault(p => p.DiscussionNotesId == value.DiscussionNotesID);
            folderDiscussion.DiscussionNotes = value.DiscussionNotes;
            folderDiscussion.DiscussionSubject = value.DiscussionSubject;
            folderDiscussion.FolderId = value.FolderID;
            folderDiscussion.EditedBy = value.EditedBy;
            folderDiscussion.IsEdited = true;
            folderDiscussion.EditedDate = DateTime.Now;
            folderDiscussion.IsRead = false;
            // folderDiscussion.DiscussedBy = value.DiscussedBy;
            folderDiscussion.DiscussionDate = DateTime.Now;

            folderDiscussion.StatusCodeId = value.StatusCodeID;
            _context.SaveChanges();
            var documents = _context.Documents.Select(s => new { s.DocumentId, s.SessionId, s.FileName,s.ContentType }).Where(d => d.SessionId == value.SessionID).AsNoTracking().ToList();
            value.FolderDiscussionAttachemntsModels = new FolderDiscussionAttachmentModel();
            if (documents.Any())
            {
                var folderAttachement = new FolderDiscussionAttachment();
                documents.ForEach(d =>
                {
                    folderAttachement = new FolderDiscussionAttachment
                    {
                        FolderDiscussionId = folderDiscussion.DiscussionNotesId,
                        DocumentId = d.DocumentId,
                        UserId = folderDiscussion.DiscussedBy.Value,
                        FileName = d.FileName
                    };
                    _context.FolderDiscussionAttachment.Add(folderAttachement);
                });
                _context.SaveChanges();
                value.FolderDiscussionAttachemntsModels = new FolderDiscussionAttachmentModel
                {
                    DocumentId = folderAttachement.DocumentId,
                    FileName = folderAttachement.FileName,
                    FileNames = documents.Where(d => d.SessionId == value.SessionID).Select(d => d.FileName).ToList(),
                    DocumentIdList = _context.FolderDiscussionAttachment.Where(c => c.FolderDiscussionId == value.DiscussionNotesID).Select(c => c.DocumentId).ToList(),
                    FileDocumentList = _context.FolderDiscussionAttachment.Where(c => c.FolderDiscussionId == value.DiscussionNotesID).Select(t => new DocumentsModel
                    {
                        DocumentID = t.DocumentId.HasValue ? t.DocumentId.Value : 0,
                        FileName = documents.FirstOrDefault(d => d.DocumentId == t.DocumentId).FileName,
                        ContentType = documents.FirstOrDefault(d => d.DocumentId == t.DocumentId).ContentType,

                    }).ToList(),
                    ContentTypes = documents.Where(d => d.SessionId == value.SessionID).Select(d => d.ContentType).ToList(),
                };
            }
            return value;
        }

        [HttpPut]
        [Route("UpdateRead")]
        public FolderDiscussionModel UpdateRead(FolderDiscussionModel value)
        {
            var userDiscussion = _context.FolderDiscussionUser.Where(c => c.UserId == value.AddedByUserID && c.FolderId == value.FolderID).ToList();
            userDiscussion.ForEach(u =>
            {
                u.IsRead = true;
            });
            _context.SaveChanges();
            return value;
        }
        [HttpPut]
        [Route("UpdateDocRead")]
        public FolderDiscussionModel UpdateDocRead(FolderDiscussionModel value)
        {
            var folder = _context.FolderDiscussion.Where(p => p.FolderId == value.FolderID && p.DocumentId == value.DocumentID).Select(s => s.DiscussionNotesId).ToList();
            //project.IsEdited = true;

            var userDiscussion = _context.FolderDiscussionUser.Where(c => folder.Contains(c.FolderDiscussionNotesId) && c.UserId == value.AddedByUserID).ToList();
            userDiscussion.ForEach(u =>
            {
                u.IsRead = true;
            });
            _context.SaveChanges();
            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteFolderDiscussion")]
        public void Delete(int id)
        {
            try
            {
                var folderDiscussion = _context.FolderDiscussion.SingleOrDefault(p => p.DiscussionNotesId == id);
                if (folderDiscussion != null)
                {
                    var discussionUser = _context.FolderDiscussionUser.Where(c => c.FolderDiscussionNotesId == id).ToList();
                    _context.FolderDiscussionUser.RemoveRange(discussionUser);
                    var discussionattachemt = _context.FolderDiscussionAttachment.Where(c => c.FolderDiscussionId == id).ToList();
                    _context.FolderDiscussionAttachment.RemoveRange(discussionattachemt);
                    _context.FolderDiscussion.Remove(folderDiscussion);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new AppException("There are replies for this Discussion. Delete operation can not completed.!", ex);
            }
        }
        [HttpDelete]
        [Route("UpdateFolderAttachment")]
        public void UpdateFolderAttachment(int id)
        {
            try
            {
                var query = string.Format("delete from FolderDiscussionAttachment Where DocumentId={0}", id);
                var rowaffected = _context.Database.ExecuteSqlRaw(query);
                if (rowaffected <= 0)
                {
                    throw new Exception("Failed to delete document");
                }
                //var folderDiscussion = _context.FolderDiscussionAttachment.SingleOrDefault(p => p.DocumentId == id);
                //if (folderDiscussion != null)
                //{
                //    var documentAttach = _context.Documents.SingleOrDefault(c => c.DocumentId == id);
                //    _context.Documents.Remove(documentAttach);
                //    _context.FolderDiscussionAttachment.Remove(folderDiscussion);
                //    _context.SaveChanges();
                //}
            }
            catch (Exception ex)
            {
                throw new AppException("There are replies for this Discussion. Delete operation can not completed.!", ex);
            }
        }
    }
}