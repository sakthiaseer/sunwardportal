﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using APP.BussinessObject;
using APP.Common;
using APP.DataAccess.Models;
using APP.EntityModel;
using APP.TaskManagementSystem.Helper;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using System.IO;
using Microsoft.AspNetCore.Authorization;
namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class AppwikiController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        private readonly TableVersionRepository _repository;
        private readonly GenerateDocumentNoSeries _generateDocumentNoSeries;
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _hostingEnvironment;
        public AppwikiController(CRT_TMSContext context, IMapper mapper, TableVersionRepository repository, GenerateDocumentNoSeries generate, IConfiguration configuration, IWebHostEnvironment host)
        {
            _context = context;
            _mapper = mapper;
            _repository = repository;
            _generateDocumentNoSeries = generate;
            _configuration = configuration;
            _hostingEnvironment = host;
        }
        [HttpGet]
        [Route("GetApplicationWikiVersion")]
        public async Task<IEnumerable<TableDataVersionInfoModel<ApplicationWikiModel>>> GetApplicationWikiVersion(string sessionID)
        {
            return await _repository.GetList<ApplicationWikiModel>(sessionID);
        }
        [HttpGet]
        [Route("GetApplicationWikiLineVersion")]
        public async Task<IEnumerable<TableDataVersionInfoModel<ApplicationWikiLineModel>>> GetApplicationWikiLineVersion(string sessionID)
        {
            return await _repository.GetList<ApplicationWikiLineModel>(sessionID);
        }
        [HttpPost]
        [Route("GetExcel")]
        public IActionResult GetExcel(SearchModel searchModel)
        {
            WorkbookPart wBookPart = null;
            List<string> headers = new List<string>();
            headers.Add("Wiki Owner");
            headers.Add("Owner Dept");
            headers.Add("Wiki Type");
            headers.Add("Category");
            headers.Add("Topic");
            headers.Add("Profile ReferenceNo");
            headers.Add("Title");
            headers.Add("Version No");
            headers.Add("Status");
            headers.Add("Content");
            headers.Add("Translation");
            headers.Add("Effective Date");
            headers.Add("New Review Date");
            headers.Add("Preparation time/month");
            headers.Add("Modified By");
            headers.Add("Modified Date");
            string newFolderName = "WikiExcel";
            string folderName = _hostingEnvironment.ContentRootPath + @"\AppUpload";
            string serverPath = System.IO.Path.Combine(folderName, newFolderName);
            if (!System.IO.Directory.Exists(serverPath))
            {
                System.IO.Directory.CreateDirectory(serverPath);
            }
            var SessionId = Guid.NewGuid();
            string FromLocation = folderName + @"\" + newFolderName + @"\" + SessionId + ".xlsx";
            
            using (SpreadsheetDocument spreadsheetDoc = SpreadsheetDocument.Create(FromLocation, SpreadsheetDocumentType.Workbook))
            {
                wBookPart = spreadsheetDoc.AddWorkbookPart();
                wBookPart.Workbook = new Workbook();
                uint sheetId = 1;
                spreadsheetDoc.WorkbookPart.Workbook.Sheets = new Sheets();
                Sheets sheets = spreadsheetDoc.WorkbookPart.Workbook.GetFirstChild<Sheets>();
                WorksheetPart wSheetPart = wBookPart.AddNewPart<WorksheetPart>();
                Sheet sheet = new Sheet() { Id = spreadsheetDoc.WorkbookPart.GetIdOfPart(wSheetPart), SheetId = sheetId, Name = "Wiki Working" };
                sheets.Append(sheet);
                SheetData sheetData = new SheetData();
                wSheetPart.Worksheet = new Worksheet(sheetData);
                Row headerRow = new Row();
                foreach (string column in headers)
                {
                    /*Cell cell = new Cell();
                    cell.DataType = CellValues.String;
                    cell.CellValue = new CellValue(column);
                    headerRow.AppendChild(cell);*/
                    Cell cellHeader = new Cell();
                    cellHeader.DataType = CellValues.InlineString;
                    Run run1 = new Run();
                    run1.Append(new Text(column));

                    RunProperties run1Properties = new RunProperties();
                    run1Properties.Append(new Bold());
                    run1.RunProperties = run1Properties;
                    InlineString inlineString = new InlineString();
                    inlineString.Append(run1);
                    cellHeader.Append(inlineString);
                    headerRow.AppendChild(cellHeader);
                }
                sheetData.AppendChild(headerRow);
                var applicationWikiExcel = GetApplicationWikiExcel(searchModel);
                applicationWikiExcel.ForEach(d =>
                {
                    Row row = new Row();
                    row.AppendChild(AddCellColumn(d.WikiOwner));
                    row.AppendChild(AddCellColumn(d.WikiOwnerName));
                    row.AppendChild(AddCellColumn(d.WikiType));
                    row.AppendChild(AddCellColumn(d.WikiCategory));
                    row.AppendChild(AddCellColumn(d.WikiTopic));
                    row.AppendChild(AddCellColumn(d.ProfileReferenceNo));
                    row.AppendChild(AddCellColumn(d.Title));
                    row.AppendChild(AddCellColumn(d.VersionNo));
                    row.AppendChild(AddCellColumn(d.StatusCode));
                    row.AppendChild(AddCellColumn(d.ContentInfo));
                    row.AppendChild(AddCellColumn(d.TranslationRequired));
                    row.AppendChild(AddCellColumn(d.EffectiveDate !=null ? d.EffectiveDate?.ToString("dd-MMM-yyyy") : ""));
                    row.AppendChild(AddCellColumn(d.NewReviewDate != null ? d.NewReviewDate?.ToString("dd-MMM-yyyy") : ""));
                    row.AppendChild(AddCellColumn(d.EstimationPreparationTimeMonth!=null && d.EstimationPreparationTimeMonth>0?d.EstimationPreparationTimeMonth.ToString():""));
                    row.AppendChild(AddCellColumn(d.ModifiedByUser));
                    row.AppendChild(AddCellColumn(d.ModifiedDate != null ? d.ModifiedDate?.ToString("dd-MMM-yyyy hh:mm tt") : ""));
                    sheetData.AppendChild(row);
                });
            }
            FileStream stream = System.IO.File.OpenRead(FromLocation);
            var br = new BinaryReader(stream);
            Byte[] documentStream = br.ReadBytes((Int32)stream.Length);
            Stream streams = new MemoryStream(documentStream);
            stream.Close();
            System.IO.File.Delete((string)FromLocation);
            return Ok(streams);
        }
        private Cell AddCellColumn(string value)
        {
            Cell fileNamecell = new Cell();
            fileNamecell.DataType = CellValues.String;
            fileNamecell.CellValue = new CellValue(value);
            return fileNamecell;
        }
        [HttpGet]
        [Route("GetReleaseWikiList")]
        public List<ApplicationWikiModel> GetReleaseWikiList()
        {
            List<ApplicationWikiModel> applicationWikiModels = new List<ApplicationWikiModel>();
            var applicationwiki = _context.ApplicationWiki.Include(a => a.WikiOwner).Include(a => a.WikiType).Where(a => a.StatusCodeId == 841).AsNoTracking().ToList();
            if (applicationwiki != null && applicationwiki.Count > 0)
            {
                applicationwiki.ForEach(w =>
                {
                    ApplicationWikiModel applicationWikiModel = new ApplicationWikiModel();
                    applicationWikiModel.ApplicationWikiId = w.ApplicationWikiId;
                    applicationWikiModel.Title = w.Title;
                    applicationWikiModel.StatusCodeID = w.StatusCodeId;
                    applicationWikiModel.WikiOwnerId = w.WikiOwnerId;
                    applicationWikiModel.WikiTypeId = w.WikiTypeId;
                    applicationWikiModel.WikiOwnerName = w.WikiOwner?.Name;
                    applicationWikiModel.WikiOwnerTypeName = w.WikiType?.Value;
                    applicationWikiModel.WikiReference = w.WikiType?.Value + " | " + w.WikiOwner?.Name + " | " + w.Title;
                    applicationWikiModels.Add(applicationWikiModel);
                });
            }

            return applicationWikiModels;
        }
        [HttpPost]
        [Route("GetApplicationWikiExcel")]
        public List<ApplicationWikiModel> GetApplicationWikiExcel(SearchModel searchModel)
        {
            List<ApplicationWikiModel> applicationWikiModels = new List<ApplicationWikiModel>();
            string sqlQuery = string.Empty;
            string search = string.Empty;
            if (searchModel.ApplicationWikiSearch != null)
            {
                if (searchModel.ApplicationWikiSearch.WikiTypeId != null)
                {
                    search += (String.IsNullOrEmpty(search) ? "" : "AND ") + "WikiTypeID =" + searchModel.ApplicationWikiSearch.WikiTypeId;
                }
                if (searchModel.ApplicationWikiSearch.WikiOwnerId != null)
                {
                    search += (String.IsNullOrEmpty(search) ? "" : "AND ") + "WikiOwnerID =" + searchModel.ApplicationWikiSearch.WikiOwnerId;
                }
                if (!String.IsNullOrEmpty(searchModel.ApplicationWikiSearch.Title))
                {
                    search += (String.IsNullOrEmpty(search) ? "" : "AND ") + "Title like '%" + searchModel.ApplicationWikiSearch.Title + "%'";
                }
                var wikiIds = new List<long>();
                if (searchModel.ApplicationWikiSearch.WikiCategoryId != null)
                {
                    wikiIds.Add(-1);
                    wikiIds.AddRange(_context.AppWikiCategoryMultiple.Where(a => a.WikiCategoryId == searchModel.ApplicationWikiSearch.WikiCategoryId).AsNoTracking().Select(s => s.ApplicationWikiId.GetValueOrDefault(0)).ToList());
                }
                if (searchModel.ApplicationWikiSearch.WikiTopicId != null)
                {
                    wikiIds.Add(-1);
                    wikiIds.AddRange(_context.AppWikiTopicMultiple.Where(a => a.WikiTopicId == searchModel.ApplicationWikiSearch.WikiTopicId).AsNoTracking().Select(s => s.ApplicationWikiId.GetValueOrDefault(0)).ToList());
                }
                if (wikiIds != null && wikiIds.Count > 0)
                {
                    search += (String.IsNullOrEmpty(search) ? "" : "AND ") + "ApplicationWikiId in (" + string.Join(",", wikiIds.Distinct().ToList()) + ")";
                }
                if (searchModel.ApplicationWikiSearch.WikiOwnerTypeId != null)
                {
                    search += (String.IsNullOrEmpty(search) ? "" : "AND ") + "WikiOwnerTypeId =" + searchModel.ApplicationWikiSearch.WikiOwnerTypeId;
                    if (searchModel.ApplicationWikiSearch.PlantId != null)
                    {
                        search += (String.IsNullOrEmpty(search) ? "" : "AND ") + "PlantId =" + searchModel.ApplicationWikiSearch.PlantId;
                    }
                    if (searchModel.ApplicationWikiSearch.EmployeeId != null)
                    {
                        search += (String.IsNullOrEmpty(search) ? "" : "AND ") + "EmployeeId =" + searchModel.ApplicationWikiSearch.EmployeeId;
                    }
                }
                if (!String.IsNullOrEmpty(search))
                {
                    search = "WHERE " + search;
                }
            }
            sqlQuery = "Select  * from View_GetApplicationWiki" + " " + search;
            var applicationWikis = _context.Set<View_GetApplicationWiki>().FromSqlRaw(sqlQuery.Trim()).AsQueryable().ToList();
            var applicationWikiIds = applicationWikis.Select(s => s.ApplicationWikiId).ToList();
            var ApplicationWikiLineLists = _context.ApplicationWikiLine.Select(a => new { a.ApplicationWikiLineId, a.ApplicationWikiId }).Where(a => applicationWikiIds.Contains(a.ApplicationWikiId.Value)).ToList();
            var ApplicationWikiLineIds = ApplicationWikiLineLists.Select(s => s.ApplicationWikiLineId).ToList();
            var ApplicationWikiLineDutys = _context.ApplicationWikiLineDuty.Select(s => new { s.ApplicationWikiLineDutyId, s.ApplicationWikiLineId }).Where(w => ApplicationWikiLineIds.Contains(w.ApplicationWikiLineId.Value)).ToList();
            var ApplicationWikiLineDutyIds = ApplicationWikiLineDutys.Select(s => s.ApplicationWikiLineDutyId).ToList();
            var WikiResponsible = _context.WikiResponsible.Include(e => e.Employee).Select(s => new { s.ApplicationWikiLineDutyId, s.WikiResponsibilityId, s.EmployeeId, UserId = s.Employee.UserId.Value }).Where(w => ApplicationWikiLineDutyIds.Contains(w.ApplicationWikiLineDutyId.Value) && w.UserId != null).ToList();
            var draftApplicationWiki = _context.DraftApplicationWiki.Select(s => new { s.ApplicationWikiId, s.ReleaseApplicationWikiId, s.StatusCodeId }).Where(w => applicationWikiIds.Contains(w.ReleaseApplicationWikiId.Value) && w.StatusCodeId == 840).ToList();
            List<long?> applicationMasterCodeIds = new List<long?> { 101, 102 };
            var applicationMasterChild = _context.ApplicationMasterChild.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterParentId)).AsNoTracking().ToList();
            var appWikiTaskLink = _context.AppWikiTaskLink.Select(n => new AppWikiTaskLinkModel { ApplicationWikiId = n.ApplicationWikiId, AppWikiTaskLinkId = n.AppWikiTaskLinkId, TaskLink = n.TaskLink, Subject = n.Subject }).Where(w => applicationWikiIds.Contains(w.ApplicationWikiId.Value)).ToList();
            applicationWikis.ForEach(s =>
            {
                ApplicationWikiModel applicationWikiModel = new ApplicationWikiModel();
                applicationWikiModel.ApplicationWikiId = s.ApplicationWikiId;
                applicationWikiModel.WikiDate = s.WikiDate;
                applicationWikiModel.WikiEntryById = s.WikiEntryById;
                applicationWikiModel.EntryBy = s.WikiEntryBy;
                applicationWikiModel.WikiTypeId = s.WikiTypeId;
                applicationWikiModel.WikiType = s.WikiType;
                applicationWikiModel.WikiOwnerId = s.WikiOwnerId;
                applicationWikiModel.WikiOwner = s.WikiOwner;
                applicationWikiModel.WikiCategoryId = s.WikiCategoryId;
                applicationWikiModel.WikiTopicId = s.WikiTopicId;
                applicationWikiModel.Title = s.Title;
                applicationWikiModel.TranslationRequiredId = s.TranslationRequiredId;
                applicationWikiModel.TranslationRequired = s.TranslationRequired;
                applicationWikiModel.Objective = string.IsNullOrEmpty(s.Objective) ? "<div> &nbsp; </div>" : s.Objective; //s.Objective;
                applicationWikiModel.ScopeDesc = string.IsNullOrEmpty(s.Scope) ? "<div> &nbsp; </div>" : s.Scope; // s.Scope;
                applicationWikiModel.PreRequisition = string.IsNullOrEmpty(s.PreRequisition) ? "<div> &nbsp; </div>" : s.PreRequisition; //s.PreRequisition;
                applicationWikiModel.Content = string.IsNullOrEmpty(s.Content) ? "<div> &nbsp; </div>" : s.Content;
                applicationWikiModel.VersionNo = s.VersionNo;
                applicationWikiModel.StatusCodeID = s.StatusCodeId;
                applicationWikiModel.StatusCode = s.StatusCode;
                applicationWikiModel.AddedByUser = s.AddedByUser;
                applicationWikiModel.AddedByUserID = s.AddedByUserId;
                applicationWikiModel.ModifiedByUserID = s.ModifiedByUserId;
                applicationWikiModel.AddedDate = s.AddedDate;
                applicationWikiModel.ModifiedDate = s.ModifiedDate;
                applicationWikiModel.ModifiedByUser = s.ModifiedByUser;
                applicationWikiModel.IsMalay = s.IsMalay;
                applicationWikiModel.IsChinese = s.IsChinese;
                applicationWikiModel.ModifiedDate = s.ModifiedDate;
                applicationWikiModel.IsNoTranslation = !((s.IsMalay.HasValue && s.IsMalay.Value) || (s.IsChinese.HasValue && s.IsChinese.Value));
                applicationWikiModel.DepartmentId = s.DepartmentId;
                applicationWikiModel.ProfileNo = s.ProfileNo;
                applicationWikiModel.DepartmentName = s.DepartmentName;
                applicationWikiModel.Description = s.Description;
                applicationWikiModel.Remarks = s.Remarks;
                applicationWikiModel.EffectiveDate = s.EffectiveDate;
                applicationWikiModel.NewReviewDate = s.NewReviewDate;
                applicationWikiModel.SessionId = s.SessionId;
                applicationWikiModel.IsEdit = false;
                applicationWikiModel.ProfileReferenceNo = s.ProfileReferenceNo;
                applicationWikiModel.ProfileId = s.ProfileId;
                applicationWikiModel.ProfileName = s.ProfileName;
                applicationWikiModel.IsContentEntryFlag = s.IsContentEntry == true ? "Yes" : "No";
                applicationWikiModel.IsContentEntry = s.IsContentEntry;
                applicationWikiModel.TaskLink = s.TaskLink;
                applicationWikiModel.WikiCategoryIds = !String.IsNullOrEmpty(s.WikiCategoryIds) ? (s.WikiCategoryIds.Split(',').Select(long.Parse).ToList()) : new List<long>();
                applicationWikiModel.WikiTopicIds = !String.IsNullOrEmpty(s.WikiTopicIds) ? (s.WikiTopicIds.Split(',').Select(long.Parse).ToList()) : new List<long>();
                applicationWikiModel.WikiCategory = applicationWikiModel.WikiCategoryIds != null ? string.Join(",", applicationMasterChild.Where(w => applicationWikiModel.WikiCategoryIds.Contains(w.ApplicationMasterChildId)).Select(s => s.Value).ToList()) : "";
                applicationWikiModel.WikiTopic = applicationWikiModel.WikiTopicIds != null ? string.Join(",", applicationMasterChild.Where(w => applicationWikiModel.WikiTopicIds.Contains(w.ApplicationMasterChildId)).Select(s => s.Value).ToList()) : "";
                applicationWikiModel.ProfilePlantId = s.ProfilePlantId;
                applicationWikiModel.ProfileDepartmentId = s.ProfileDepartmentId;
                applicationWikiModel.ProfileSectionId = s.ProfileSectionId;
                applicationWikiModel.ProfileSubSectionId = s.ProfileSubSectionId;
                applicationWikiModel.ProfileDivisionId = s.ProfileDivisionId;
                applicationWikiModel.DraftStatus = false;
                applicationWikiModel.EstimationPreparationTimeMonth = s.EstimationPreparationTimeMonth;
                applicationWikiModel.WikiOwnerTypeId = s.WikiOwnerTypeId;
                applicationWikiModel.PlantId = s.PlantId;
                applicationWikiModel.EmployeeId = s.EmployeeId;
                applicationWikiModel.ContentInfo = s.ContentInfo;
                applicationWikiModel.WikiOwnerName = s.WikiOwnerTypeId == 2401 ? (s.PlantName) : (s.FirstName);
                var draft = draftApplicationWiki.Where(a => a.ReleaseApplicationWikiId == s.ApplicationWikiId && a.StatusCodeId == 840).Count();
                var ApplicationWikiLineList = ApplicationWikiLineLists.Where(a => a.ApplicationWikiId == s.ApplicationWikiId).Select(a => a.ApplicationWikiLineId).ToList();
                applicationWikiModel.IsEdit = true;
                if (draft > 0)
                {
                    applicationWikiModel.DraftStatus = true;
                }
                applicationWikiModel.AppWikiTaskLinkModel = appWikiTaskLink.Where(t => t.ApplicationWikiId == s.ApplicationWikiId).ToList();
                applicationWikiModels.Add(applicationWikiModel);
            });
            return applicationWikiModels;
        }
        [HttpGet]
        [Route("GetApplicationWikiPdf")]
        public ApplicationWikiModel GetApplicationWikiPdf(long? userId, long? id)
        {
            DocumentsController documentsController = new DocumentsController(_context, _mapper, _generateDocumentNoSeries, _hostingEnvironment);
            ApplicationWikiModel applicationWikiModel = new ApplicationWikiModel();
            string sqlQuery = string.Empty;
            sqlQuery = "Select  * from View_GetApplicationWiki where ApplicationWikiId=" + id;
            var applicationWikis = _context.Set<View_GetApplicationWiki>().FromSqlRaw(sqlQuery.Trim()).AsQueryable().ToList();
            var applicationWikiIds = applicationWikis.Select(s => s.ApplicationWikiId).ToList();
            var ApplicationWikiLineLists = _context.ApplicationWikiLine.Select(a => new { a.ApplicationWikiLineId, a.ApplicationWikiId }).Where(a => applicationWikiIds.Contains(a.ApplicationWikiId.Value)).ToList();
            var ApplicationWikiLineIds = ApplicationWikiLineLists.Select(s => s.ApplicationWikiLineId).ToList();
            var ApplicationWikiLineDutys = _context.ApplicationWikiLineDuty.Select(s => new { s.ApplicationWikiLineDutyId, s.ApplicationWikiLineId }).Where(w => ApplicationWikiLineIds.Contains(w.ApplicationWikiLineId.Value)).ToList();
            var ApplicationWikiLineDutyIds = ApplicationWikiLineDutys.Select(s => s.ApplicationWikiLineDutyId).ToList();
            var WikiResponsible = _context.WikiResponsible.Include(e => e.Employee).Select(s => new { s.ApplicationWikiLineDutyId, s.WikiResponsibilityId, s.EmployeeId, UserId = s.Employee.UserId.Value }).Where(w => ApplicationWikiLineDutyIds.Contains(w.ApplicationWikiLineDutyId.Value) && w.UserId != null).ToList();
            var draftApplicationWiki = _context.DraftApplicationWiki.Select(s => new { s.ApplicationWikiId, s.ReleaseApplicationWikiId, s.StatusCodeId }).Where(w => applicationWikiIds.Contains(w.ReleaseApplicationWikiId.Value) && w.StatusCodeId == 840).ToList();
            List<long?> applicationMasterCodeIds = new List<long?> { 101, 102 };
            var applicationMasterChild = _context.ApplicationMasterChild.Where(f => applicationMasterCodeIds.Contains(f.ApplicationMasterParentId)).AsNoTracking().ToList();
            var appWikiTaskLink = _context.AppWikiTaskLink.Select(n => new AppWikiTaskLinkModel { ApplicationWikiId = n.ApplicationWikiId, AppWikiTaskLinkId = n.AppWikiTaskLinkId, TaskLink = n.TaskLink, Subject = n.Subject }).Where(w => applicationWikiIds.Contains(w.ApplicationWikiId.Value)).ToList();
            var wikiOwnerTypeStatus = _context.CodeMaster.Where(w => w.CodeType.ToLower() == "WikiOwnerTypeStatus".ToLower()).ToList();
            applicationWikis.ForEach(s =>
            {
                applicationWikiModel.ApplicationWikiId = s.ApplicationWikiId;
                applicationWikiModel.WikiDate = s.WikiDate;
                applicationWikiModel.WikiEntryById = s.WikiEntryById;
                applicationWikiModel.EntryBy = s.WikiEntryBy;
                applicationWikiModel.WikiTypeId = s.WikiTypeId;
                applicationWikiModel.WikiType = s.WikiType;
                applicationWikiModel.WikiOwnerId = s.WikiOwnerId;
                applicationWikiModel.WikiOwner = s.WikiOwner;
                applicationWikiModel.WikiCategoryId = s.WikiCategoryId;
                applicationWikiModel.WikiTopicId = s.WikiTopicId;
                applicationWikiModel.Title = s.Title;
                applicationWikiModel.TranslationRequiredId = s.TranslationRequiredId;
                applicationWikiModel.TranslationRequired = s.TranslationRequired;
                applicationWikiModel.Objective = string.IsNullOrEmpty(s.Objective) ? "<div> &nbsp; </div>" : s.Objective; //s.Objective;
                applicationWikiModel.ScopeDesc = string.IsNullOrEmpty(s.Scope) ? "<div> &nbsp; </div>" : s.Scope; // s.Scope;
                applicationWikiModel.PreRequisition = string.IsNullOrEmpty(s.PreRequisition) ? "<div> &nbsp; </div>" : s.PreRequisition; //s.PreRequisition;
                applicationWikiModel.Content = string.IsNullOrEmpty(s.Content) ? "<div> &nbsp; </div>" : s.Content;
                applicationWikiModel.VersionNo = s.VersionNo;
                applicationWikiModel.StatusCodeID = s.StatusCodeId;
                applicationWikiModel.StatusCode = s.StatusCode;
                applicationWikiModel.AddedByUser = s.AddedByUser;
                applicationWikiModel.AddedByUserID = s.AddedByUserId;
                applicationWikiModel.ModifiedByUserID = s.ModifiedByUserId;
                applicationWikiModel.AddedDate = s.AddedDate;
                applicationWikiModel.ModifiedDate = s.ModifiedDate;
                applicationWikiModel.ModifiedByUser = s.ModifiedByUser;
                applicationWikiModel.IsMalay = s.IsMalay;
                applicationWikiModel.IsChinese = s.IsChinese;
                applicationWikiModel.ModifiedDate = s.ModifiedDate;
                applicationWikiModel.IsNoTranslation = !((s.IsMalay.HasValue && s.IsMalay.Value) || (s.IsChinese.HasValue && s.IsChinese.Value));
                applicationWikiModel.DepartmentId = s.DepartmentId;
                applicationWikiModel.ProfileNo = s.ProfileNo;
                applicationWikiModel.DepartmentName = s.DepartmentName;
                applicationWikiModel.Description = s.Description;
                applicationWikiModel.Remarks = s.Remarks;
                applicationWikiModel.EffectiveDate = s.EffectiveDate;
                applicationWikiModel.NewReviewDate = s.NewReviewDate;
                applicationWikiModel.SessionId = s.SessionId;
                applicationWikiModel.IsEdit = false;
                applicationWikiModel.ProfileReferenceNo = s.ProfileReferenceNo;
                applicationWikiModel.ProfileId = s.ProfileId;
                applicationWikiModel.ProfileName = s.ProfileName;
                applicationWikiModel.IsContentEntryFlag = s.IsContentEntry == true ? "Yes" : "No";
                applicationWikiModel.IsContentEntry = s.IsContentEntry;
                applicationWikiModel.TaskLink = s.TaskLink;
                applicationWikiModel.WikiCategoryIds = !String.IsNullOrEmpty(s.WikiCategoryIds) ? (s.WikiCategoryIds.Split(',').Select(long.Parse).ToList()) : new List<long>();
                applicationWikiModel.WikiTopicIds = !String.IsNullOrEmpty(s.WikiTopicIds) ? (s.WikiTopicIds.Split(',').Select(long.Parse).ToList()) : new List<long>();
                applicationWikiModel.WikiCategory = applicationWikiModel.WikiCategoryIds != null ? string.Join(",", applicationMasterChild.Where(w => applicationWikiModel.WikiCategoryIds.Contains(w.ApplicationMasterChildId)).Select(s => s.Value).ToList()) : "";
                applicationWikiModel.WikiTopic = applicationWikiModel.WikiTopicIds != null ? string.Join(",", applicationMasterChild.Where(w => applicationWikiModel.WikiTopicIds.Contains(w.ApplicationMasterChildId)).Select(s => s.Value).ToList()) : "";
                applicationWikiModel.ProfilePlantId = s.ProfilePlantId;
                applicationWikiModel.ProfileDepartmentId = s.ProfileDepartmentId;
                applicationWikiModel.ProfileSectionId = s.ProfileSectionId;
                applicationWikiModel.ProfileSubSectionId = s.ProfileSubSectionId;
                applicationWikiModel.ProfileDivisionId = s.ProfileDivisionId;
                applicationWikiModel.DraftStatus = false;
                applicationWikiModel.EstimationPreparationTimeMonth = s.EstimationPreparationTimeMonth;
                applicationWikiModel.WikiOwnerTypeId = s.WikiOwnerTypeId;
                applicationWikiModel.WikiOwnerTypeName = wikiOwnerTypeStatus != null ? wikiOwnerTypeStatus.FirstOrDefault(f => f.CodeId == s.WikiOwnerTypeId)?.CodeValue : "";
                applicationWikiModel.PlantId = s.PlantId;
                applicationWikiModel.EmployeeId = s.EmployeeId;
                applicationWikiModel.ContentInfo = s.ContentInfo;
                applicationWikiModel.WikiOwnerName = s.WikiOwnerTypeId == 2401 ? (s.PlantName) : (s.FirstName);
                var draft = draftApplicationWiki.Where(a => a.ReleaseApplicationWikiId == s.ApplicationWikiId && a.StatusCodeId == 840).Count();
                var ApplicationWikiLineList = ApplicationWikiLineLists.Where(a => a.ApplicationWikiId == s.ApplicationWikiId).Select(a => a.ApplicationWikiLineId).ToList();
                applicationWikiModel.IsEdit = true;
                if (draft > 0)
                {
                    applicationWikiModel.DraftStatus = true;
                }
                applicationWikiModel.AppWikiTaskLinkModel = appWikiTaskLink.Where(t => t.ApplicationWikiId == s.ApplicationWikiId).ToList();
                applicationWikiModel.documentsModels = documentsController.GetDocumentsBySessionID(s.SessionId, (int)userId, "");
                applicationWikiModel.applicationWikiLineModels = ApplicationWikiLine(s.ApplicationWikiId);
            });
            return applicationWikiModel;
        }
        private List<ApplicationWikiModel> GetApplicationWikiList(int? userId, int? superUser, long? id)
        {
            var ApplicationWikiLineDuty = _context.ApplicationWikiLineDuty.ToList();
            var WikiResponsible = _context.WikiResponsible.Include(w => w.Employee).ToList();
            List<ApplicationWikiModel> applicationWikiModels = new List<ApplicationWikiModel>();
            var applicationWiki = _context.ApplicationWiki
                                .Include(a => a.AddedByUser)
                                .Include(m => m.ModifiedByUser)
                                .Include(s => s.StatusCode)
                                .Include(t => t.TranslationRequired)
                                .Include(w => w.WikiEntryBy)
                                .Include(o => o.WikiOwner)
                                //.Include(w=>w.WikiType)
                                .Include(d => d.Department)
                                .Include(z => z.ApplicationWikiLine)
                                .Include(p => p.Profile)
                                .Include(c => c.AppWikiCategoryMultiple)
                                .Include(r => r.AppWikiTopicMultiple)
                                .Include("AppWikiCategoryMultiple.WikiCategory")
                                .Include("AppWikiTopicMultiple.WikiTopic")
                                .Include("ApplicationWikiLine.ApplicationWikiLineDuty")
                                .Include(i => i.DraftApplicationWiki)
                                 .Include("ApplicationWikiLine.ApplicationWikiLineDuty.WikiResponsible");
            if (id != null)
            {
                applicationWiki = applicationWiki.Where(w => w.ApplicationWikiId == id);
            }
            var applicationWikis = applicationWiki.OrderByDescending(o => o.ApplicationWikiId).AsNoTracking().ToList();
            if (applicationWikis != null)
            {
                List<long?> masterIds = applicationWikis.Where(w => w.WikiTypeId != null).Select(a => a.WikiTypeId).Distinct().ToList();
                masterIds.AddRange(applicationWikis.Where(w => w.WikiCategoryId != null).Select(a => a.WikiCategoryId).Distinct().ToList());
                masterIds.AddRange(applicationWikis.Where(w => w.WikiTopicId != null).Select(a => a.WikiTopicId).Distinct().ToList());
                var applicationmasterdetail = _context.ApplicationMasterDetail.Where(w => masterIds.Contains(w.ApplicationMasterDetailId)).AsNoTracking().ToList();
                applicationWikis.ForEach(s =>
                {
                    ApplicationWikiModel applicationWikiModel = new ApplicationWikiModel();
                    applicationWikiModel.ApplicationWikiId = s.ApplicationWikiId;
                    applicationWikiModel.WikiDate = s.WikiDate;
                    applicationWikiModel.WikiEntryById = s.WikiEntryById;
                    applicationWikiModel.EntryBy = s.WikiEntryBy != null ? s.WikiEntryBy.UserName : string.Empty;
                    applicationWikiModel.WikiTypeId = s.WikiTypeId;
                    // applicationWikiModel.WikiType = s.WikiType?.Value;
                    applicationWikiModel.WikiOwnerId = s.WikiOwnerId;
                    applicationWikiModel.WikiOwner = s.WikiOwner != null ? s.WikiOwner.Name : string.Empty;
                    applicationWikiModel.WikiCategoryId = s.WikiCategoryId;
                    //applicationWikiModel.WikiCategory = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiCategoryId) != null) ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiCategoryId).Value : string.Empty;
                    applicationWikiModel.WikiTopicId = s.WikiTopicId;
                    //applicationWikiModel.WikiTopic = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiTopicId) != null) ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiTopicId).Value : string.Empty;
                    applicationWikiModel.Title = s.Title;
                    applicationWikiModel.TranslationRequiredId = s.TranslationRequiredId;
                    applicationWikiModel.TranslationRequired = s.TranslationRequired != null ? s.TranslationRequired.CodeValue : string.Empty;
                    applicationWikiModel.Objective = s.Objective;
                    applicationWikiModel.ScopeDesc = s.Scope;
                    applicationWikiModel.PreRequisition = s.PreRequisition;
                    applicationWikiModel.Content = s.Content;
                    applicationWikiModel.VersionNo = s.VersionNo;
                    applicationWikiModel.StatusCodeID = s.StatusCodeId;
                    applicationWikiModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : string.Empty;
                    applicationWikiModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : string.Empty;
                    applicationWikiModel.AddedByUserID = s.AddedByUserId;
                    applicationWikiModel.ModifiedByUserID = s.ModifiedByUserId;
                    applicationWikiModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : string.Empty;
                    applicationWikiModel.IsMalay = s.IsMalay;
                    applicationWikiModel.IsChinese = s.IsChinese;
                    applicationWikiModel.ModifiedDate = s.ModifiedDate;
                    applicationWikiModel.IsNoTranslation = !((s.IsMalay.HasValue && s.IsMalay.Value) || (s.IsChinese.HasValue && s.IsChinese.Value));
                    applicationWikiModel.DepartmentId = s.DepartmentId;
                    applicationWikiModel.ProfileNo = s.ProfileNo;
                    applicationWikiModel.DepartmentName = s.Department?.Name;
                    applicationWikiModel.Description = s.Description;
                    applicationWikiModel.Remarks = s.Remarks;
                    applicationWikiModel.EffectiveDate = s.EffectiveDate;
                    applicationWikiModel.NewReviewDate = s.NewReviewDate;
                    applicationWikiModel.SessionId = s.SessionId;
                    applicationWikiModel.IsEdit = false;
                    applicationWikiModel.ProfileReferenceNo = s.ProfileReferenceNo;
                    applicationWikiModel.ProfileId = s.ProfileId;
                    applicationWikiModel.ProfileName = s.Profile != null ? s.Profile.Name : "";
                    applicationWikiModel.IsContentEntryFlag = s.IsContentEntry == true ? "Yes" : "No";
                    applicationWikiModel.IsContentEntry = s.IsContentEntry;
                    applicationWikiModel.TaskLink = s.TaskLink;
                    applicationWikiModel.WikiCategoryIds = s.AppWikiCategoryMultiple != null ? s.AppWikiCategoryMultiple.Where(w => w.ApplicationWikiId == s.ApplicationWikiId).Select(a => a.WikiCategoryId.Value).ToList() : new List<long>();
                    applicationWikiModel.WikiTopicIds = s.AppWikiTopicMultiple != null ? s.AppWikiTopicMultiple.Where(w => w.ApplicationWikiId == s.ApplicationWikiId).Select(a => a.WikiTopicId.Value).ToList() : new List<long>();
                    applicationWikiModel.WikiCategory = s.AppWikiCategoryMultiple != null ? string.Join(",", s.AppWikiCategoryMultiple.Where(w => w.ApplicationWikiId == s.ApplicationWikiId).Select(s => s.WikiCategory.Value).ToList()) : "";
                    applicationWikiModel.WikiTopic = s.AppWikiTopicMultiple != null ? string.Join(",", s.AppWikiTopicMultiple.Where(w => w.ApplicationWikiId == s.ApplicationWikiId).Select(s => s.WikiTopic.Value).ToList()) : "";
                    applicationWikiModel.ProfilePlantId = s.ProfilePlantId;
                    applicationWikiModel.ProfileDepartmentId = s.ProfileDepartmentId;
                    applicationWikiModel.ProfileDivisionId = s.ProfileDivisionId;
                    applicationWikiModel.ProfileSectionId = s.ProfileSectionId;
                    applicationWikiModel.ProfileSubSectionId = s.ProfileSubSectionId;
                    applicationWikiModel.ProfileDivisionId = s.ProfileDivisionId;
                    applicationWikiModel.DraftStatus = false;
                    applicationWikiModel.EstimationPreparationTimeMonth = s.EstimationPreparationTimeMonth;
                    applicationWikiModel.WikiOwnerTypeId = s.WikiOwnerTypeId;
                    applicationWikiModel.PlantId = s.PlantId;
                    applicationWikiModel.EmployeeId = s.EmployeeId;
                    applicationWikiModel.ContentInfo = s.ContentInfo;
                    if (s.DraftApplicationWiki != null)
                    {
                        var draft = s.DraftApplicationWiki.Where(a => a.ReleaseApplicationWikiId == s.ApplicationWikiId && a.StatusCodeId == 840).Count();
                        if (draft > 0)
                        {
                            applicationWikiModel.DraftStatus = true;
                        }
                    }
                    if (userId != superUser)
                    {
                        if (s.AddedByUserId == userId)
                        {
                            applicationWikiModel.IsEdit = true;
                        }
                        else
                        {
                            if (s.ApplicationWikiLine != null)
                            {
                                var ApplicationWikiLineList = s.ApplicationWikiLine.Where(a => a.ApplicationWikiId == s.ApplicationWikiId).Select(a => a.ApplicationWikiLineId).ToList();
                                var ApplicationWikiLineDutyList = ApplicationWikiLineDuty != null ? ApplicationWikiLineDuty.Where(w => ApplicationWikiLineList.Contains(w.ApplicationWikiLineId.Value)).Select(d => d.ApplicationWikiLineDutyId).ToList() : null;
                                var WikiResponsibleList = ApplicationWikiLineDutyList != null ? WikiResponsible.Where(w => ApplicationWikiLineDutyList.Contains(w.ApplicationWikiLineDutyId.Value) && w.Employee?.UserId == userId).ToList() : null;
                                applicationWikiModel.IsEdit = WikiResponsibleList != null && WikiResponsibleList.Count > 0 ? true : false;
                            }
                        }
                    }
                    else
                    {
                        applicationWikiModel.IsEdit = true;
                    }
                    if (s.StatusCodeId == 841)
                    {
                        //applicationWikiModel.IsEdit = false;
                    }
                    applicationWikiModels.Add(applicationWikiModel);
                });
            }
            return applicationWikiModels;
        }
        [HttpGet]
        [Route("GetApplicationWikiProlileList")]
        public List<DocumentProfileNoSeriesModel> GetApplicationWikiProlileList()
        {
            List<string> profileId = new List<string>(_configuration["WiProfileID"].Split(",")).ToList();
            var profileIds = profileId.Select(long.Parse).ToArray();
            var documentProfiles = _context.DocumentProfileNoSeries.Include(a => a.AddedByUser).Include(m => m.ModifiedByUser).Include(s => s.StatusCode).Where(w => profileIds.Contains(w.ProfileId)).AsNoTracking().ToList();
            List<DocumentProfileNoSeriesModel> documentProfileNoSeriesModel = new List<DocumentProfileNoSeriesModel>();
            documentProfiles.ForEach(s =>
            {
                DocumentProfileNoSeriesModel documentProfileNoSeriesModels = new DocumentProfileNoSeriesModel
                {
                    ProfileID = s.ProfileId,
                    Name = s.Name,
                    Description = s.Description,
                    Abbreviation = s.Abbreviation,
                    Abbreviation1 = s.Abbreviation1,
                    Abbreviation2 = s.Abbreviation2,
                    AbbreviationRequired = s.AbbreviationRequired,
                    SpecialWording = s.SpecialWording,
                    StartingNo = s.StartingNo,
                    StartWithYear = s.StartWithYear,
                    NoOfDigit = s.NoOfDigit,
                    IncrementalNo = s.IncrementalNo,
                    TranslationRequired = s.TranslationRequired,
                    LastCreatedDate = s.LastCreatedDate,
                    LastNoUsed = s.LastNoUsed,
                    Note = s.Note,
                    CategoryAbbreviation = s.CategoryAbbreviation,
                    CategoryId = s.CategoryId,
                    IsCategoryAbbreviation = s.IsCategoryAbbreviation,
                    CompanyId = s.CompanyId,
                    DepartmentId = s.DeparmentId,
                    GroupId = s.GroupId,
                    GroupAbbreviation = s.GroupAbbreviation,
                    IsGroupAbbreviation = s.IsGroupAbbreviation,
                    LinkId = s.LinkId,
                    ProfileTypeId = s.ProfileTypeId,
                    SeperatorToUse = s.SeperatorToUse,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    SampleDocumentNo = s.SampleDocumentNo,

                };
                documentProfileNoSeriesModel.Add(documentProfileNoSeriesModels);
            });
            documentProfileNoSeriesModel.OrderByDescending(a => a.ProfileID).ToList();
            return documentProfileNoSeriesModel;
        }
        [HttpGet]
        [Route("GetApplicationWiki")]
        public List<ApplicationWikiModel> Get(int? userId, int? superUser)
        {
            var getApplicationWikiLists = GetApplicationWikiList(userId, superUser, null);
            return getApplicationWikiLists;
        }
        [HttpGet]
        [Route("GetApplicationWikiByListing")]
        public ApplicationWikiModel GetApplicationWikiByListing(int? userId, int? superUser, long? id)
        {
            ApplicationWikiModel applicationWikiModels = new ApplicationWikiModel();
            var getApplicationWikiLists = GetApplicationWikiList(userId, superUser, id).FirstOrDefault();
            return getApplicationWikiLists;
        }
        [HttpGet]
        [Route("GetProfileAutonumber")]
        public DocumentNoSeriesModel GetProfileAutonumber(int? id)
        {
            DocumentNoSeriesModel documentNoSeriesModel = new DocumentNoSeriesModel();
            var documentProfile = _context.DocumentProfileNoSeries.Where(w => w.ProfileId == id).FirstOrDefault();
            if (documentProfile != null)
            {
                if (!string.IsNullOrEmpty(documentProfile.Abbreviation1))
                {
                    documentNoSeriesModel.Abbreviation1 = (Newtonsoft.Json.JsonConvert.DeserializeObject<List<NumberSeriesCodeModel>>(documentProfile.Abbreviation1).ToList());
                }
                documentNoSeriesModel.ProfileID = documentProfile.ProfileId;

            }

            return documentNoSeriesModel;

        }
        [HttpGet]
        [Route("GetApplicationWikiLine")]
        public List<ApplicationWikiLineModel> ApplicationWikiLine(long id)
        {
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var applicationWikis = _context.ApplicationWikiLine
                                .Include("AddedByUser")
                                .Include("ModifiedByUser")
                                .Include("StatusCode")
                                .Include("FunctionLinkNavigation")
                                .Include("PageLinkNavigation")
                                .Include(c => c.NotificationAdviceType)
                                .Include(a => a.ApplicationWikiWeekly)
                                .Include(b => b.ApplicationWikiLineNotify)
                                .Where(w => w.ApplicationWikiId == id).OrderByDescending(o => o.ApplicationWikiId).AsNoTracking().ToList();
            List<ApplicationWikiLineModel> applicationWikiLineModels = new List<ApplicationWikiLineModel>();
            applicationWikis.ForEach(s =>
            {
                ApplicationWikiLineModel applicationWikiLineModel = new ApplicationWikiLineModel
                {
                    ApplicationWikiLineId = s.ApplicationWikiLineId,
                    ApplicationWikiId = s.ApplicationWikiId,
                    DutyId = s.DutyId,
                    DutyName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.DutyId) != null) ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.DutyId).Value : string.Empty,
                    Responsibility = s.Responsibility,
                    PageLink = s.PageLink,
                    PageLinkName = s.PageLinkNavigation?.PermissionName,
                    FunctionLink = s.FunctionLink,
                    FunctionLinkName = s.FunctionLinkNavigation?.PermissionName,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCode = s.StatusCode.CodeValue,
                    NotificationAdvice = s.NotificationAdvice,
                    NotificationAdviceFlag = s.NotificationAdvice == true ? "Yes" : "No",
                    NotificationAdviceTypeId = s.NotificationAdviceTypeId,
                    CustomId = s.CustomId,
                    RepeatId = s.RepeatId,
                    DueDate = s.DueDate,
                    Monthly = s.Monthly,
                    Yearly = s.Yearly,
                    EventDescription = s.EventDescription,
                    DaysOfWeek = s.DaysOfWeek,
                    SessionId = s.SessionId,
                    NotificationStatusId = s.NotificationStatusId,
                    ScreenId = s.ScreenId,
                    Title = s.Title,
                    Message = s.Message,
                    NotifyEndDate = s.NotifyEndDate,
                    NotificationAdviceType = s.NotificationAdviceType?.CodeValue,
                    ApplicationWikiLineDutys = ApplicationWikiLineDuty(s.ApplicationWikiLineId),
                    ApplicationWikiRecurrences = ApplicationWikiRecurrence(s.ApplicationWikiLineId),
                    NotifyIds = s.ApplicationWikiLineNotify != null ? s.ApplicationWikiLineNotify.Where(c => c.ApplicationWikiLineId == s.ApplicationWikiLineId).Select(c => c.NotifyUserId).ToList() : new List<long?>(),
                    WeeklyIds = s.ApplicationWikiWeekly != null ? s.ApplicationWikiWeekly.Where(c => c.ApplicationWikiLineId == s.ApplicationWikiLineId && c.CustomType == "Weekly").Select(c => c.WeeklyId).ToList() : new List<int?>(),
                    DaysOfWeekIds = s.ApplicationWikiWeekly != null ? s.ApplicationWikiWeekly.Where(c => c.ApplicationWikiLineId == s.ApplicationWikiLineId && c.CustomType == "Yearly").Select(c => c.WeeklyId).ToList() : new List<int?>(),
                    IsAllowDocAccess = s.IsAllowDocAccess,
                };
                applicationWikiLineModels.Add(applicationWikiLineModel);
            });

            return applicationWikiLineModels;
        }
        [HttpGet]
        [Route("GetApplicationWikiRecurrence")]
        public List<ApplicationWikiRecurrenceModel> ApplicationWikiRecurrence(long id)
        {
            var ApplicationWikiRecurrence = _context.ApplicationWikiRecurrence
                 .Include("AddedByUser")
                .Include("ModifiedByUser")
                .Include("StatusCode")
                .Include("Type")
                .Include("OccurenceOption")
               .Select(s => new ApplicationWikiRecurrenceModel
               {
                   ApplicationWikiRecurrenceId = s.ApplicationWikiRecurrenceId,
                   ApplicationWikiLineId = s.ApplicationWikiLineId,
                   TypeId = s.TypeId,
                   RepeatNos = s.RepeatNos,
                   OccurenceOptionId = s.OccurenceOptionId,
                   NoOfOccurences = s.NoOfOccurences,
                   Sunday = s.Sunday,
                   Monday = s.Monday,
                   Tuesday = s.Tuesday,
                   Wednesday = s.Wednesday,
                   Thursday = s.Thursday,
                   Friday = s.Friday,
                   Saturyday = s.Saturyday,
                   StartDate = s.StartDate,
                   EndDate = s.EndDate,
                   StatusCodeID = s.StatusCodeId,
                   AddedByUserID = s.AddedByUserId,
                   ModifiedByUserID = s.ModifiedByUserId,
                   AddedDate = s.AddedDate,
                   ModifiedDate = s.ModifiedDate,
                   AddedByUser = s.AddedByUser.UserName,
                   ModifiedByUser = s.ModifiedByUser.UserName,
                   StatusCode = s.StatusCode.CodeValue,
                   TypeName = s.Type.CodeValue,
                   OccurenceOptionName = s.OccurenceOption.CodeValue,
               }).Where(w => w.ApplicationWikiLineId == id).OrderByDescending(o => o.ApplicationWikiRecurrenceId).AsNoTracking().ToList();
            return ApplicationWikiRecurrence;
        }
        [HttpGet]
        [Route("GetApplicationWikiLineDuty")]
        public List<ApplicationWikiLineDutyModel> ApplicationWikiLineDuty(long id)
        {
            List<ApplicationWikiLineDutyModel> applicationWikiLineDutyModels = new List<ApplicationWikiLineDutyModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var applicationWikis = _context.ApplicationWikiLineDuty
                                .Include("Departmant")
                                .Include("Designation")
                                .Include("Company")
                                .Include("AddedByUser")
                                .Include("ModifiedByUser")
                                .Include("StatusCode")
                                .Include("Employee")
                                .Where(w => w.ApplicationWikiLineId == id).OrderByDescending(o => o.ApplicationWikiLineDutyId).AsNoTracking().ToList();
            applicationWikis.ForEach(s =>
            {
                ApplicationWikiLineDutyModel applicationWikiLineDutyModel = new ApplicationWikiLineDutyModel
                {
                    ApplicationWikiLineId = s.ApplicationWikiLineId,
                    ApplicationWikiLineDutyId = s.ApplicationWikiLineDutyId,
                    DepartmentId = s.DepartmantId,
                    EmployeeId = s.EmployeeId,
                    EmployeeName = s.Employee?.FirstName,
                    DepartmentName = s.Departmant?.Name,
                    DesignationNumber = s.DesignationNumber,
                    CompanyId = s.CompanyId,
                    PlantCompanyName = s.Company?.Description,
                    DesignationId = s.DesignationId,
                    DesignationName = s.Designation?.Name,
                    DutyNo = s.DutyNo,
                    DutyNoName = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.DutyNo) != null) ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.DutyNo).Value : string.Empty,
                    StatusCodeID = s.StatusCodeId,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    AddedDate = s.AddedDate,
                    ModifiedDate = s.ModifiedDate,
                    AddedByUser = s.AddedByUser.UserName,
                    ModifiedByUser = s.ModifiedByUser?.UserName,
                    StatusCode = s.StatusCode.CodeValue,
                    Type = s.Type,
                    Description = s.Description,
                    wikiResponsibilityModels = GetWikiResponsibleByID((int)s.ApplicationWikiLineDutyId)
                    //EmployeeIDs = s.WikiResponsible?.Where(e => e.ApplicationWikiLineDutyId == s.ApplicationWikiLineDutyId).Select(e => e.EmployeeId).ToList(),
                    //UserGroupIDs = s.WikiResponsible?.Where(e => e.ApplicationWikiLineDutyId == s.ApplicationWikiLineDutyId).Select(e => e.UserGroupId).Distinct().ToList(),
                };
                applicationWikiLineDutyModels.Add(applicationWikiLineDutyModel);
            });

            return applicationWikiLineDutyModels;
        }
        // GET: api/Project/2
        [HttpGet]
        [Route("GetApplicationWikiByID")]
        public List<ApplicationWikiModel> Get(int? id)
        {
            List<ApplicationWikiModel> applicationWikiModels = new List<ApplicationWikiModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var applicationWikis = _context.ApplicationWiki
                                .Include(a => a.AddedByUser)
                                .Include(m => m.ModifiedByUser)
                                .Include(s => s.StatusCode)
                                .Include(t => t.TranslationRequired)
                                .Include(w => w.WikiEntryBy)
                                .Include(o => o.WikiOwner)
                                // .Include(w=>w.WikiType)
                                .Include(d => d.Department)
                                .Where(s => s.ApplicationWikiId == id.Value).AsNoTracking().ToList();
            applicationWikis.ForEach(s =>
            {
                ApplicationWikiModel applicationWikiModel = new ApplicationWikiModel
                {
                    ApplicationWikiId = s.ApplicationWikiId,
                    WikiDate = s.WikiDate,
                    WikiEntryById = s.WikiEntryById,
                    EntryBy = s.WikiEntryBy != null ? s.WikiEntryBy.UserName : string.Empty,
                    WikiTypeId = s.WikiTypeId,
                    // WikiType = s.WikiType?.Value,
                    WikiOwnerId = s.WikiOwnerId,
                    WikiOwner = s.WikiOwner != null ? s.WikiOwner.Name : string.Empty,
                    WikiCategoryId = s.WikiCategoryId,
                    WikiCategory = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiCategoryId) != null) ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiCategoryId).Value : string.Empty,
                    WikiTopicId = s.WikiTopicId,
                    WikiTopic = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiTopicId) != null) ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiTopicId).Value : string.Empty,
                    Title = s.Title,
                    TranslationRequiredId = s.TranslationRequiredId,
                    TranslationRequired = s.TranslationRequired != null ? s.TranslationRequired.CodeValue : string.Empty,
                    Objective = s.Objective,
                    ScopeDesc = s.Scope,
                    PreRequisition = s.PreRequisition,
                    Content = s.Content,
                    VersionNo = s.VersionNo,
                    StatusCodeID = s.StatusCodeId,
                    StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : string.Empty,
                    AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : string.Empty,
                    AddedByUserID = s.AddedByUserId,
                    ModifiedByUserID = s.ModifiedByUserId,
                    ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : string.Empty,
                    ModifiedDate = s.ModifiedDate,
                    IsNoTranslation = !((s.IsMalay.HasValue && s.IsMalay.Value) || (s.IsChinese.HasValue && s.IsChinese.Value)),
                    DepartmentId = s.DepartmentId,
                    ProfileNo = s.ProfileNo,
                    DepartmentName = s.Department.Name,
                    Description = s.Description,
                    Remarks = s.Remarks,
                    NewReviewDate = s.NewReviewDate,
                    EffectiveDate = s.EffectiveDate,
                    SessionId = s.SessionId,
                    ProfileReferenceNo = s.ProfileReferenceNo,
                    EstimationPreparationTimeMonth = s.EstimationPreparationTimeMonth,
                    WikiOwnerTypeId = s.WikiOwnerTypeId,
                    PlantId = s.PlantId,
                    EmployeeId = s.EmployeeId,
                    ContentInfo = s.ContentInfo,
                };
                applicationWikiModels.Add(applicationWikiModel);
            });

            return applicationWikiModels;
        }


        [HttpGet]
        [Route("GetWikiResponsibleByID")]
        public List<WikiResponsibilityModel> GetWikiResponsibleByID(int? id)
        {
            List<WikiResponsibilityModel> wikiResponsibilityModels = new List<WikiResponsibilityModel>();
            var applicationmasterdetail = _context.ApplicationMasterDetail.ToList();
            var usergroup = _context.UserGroupUser.Include(u => u.UserGroup).ToList();
            var wikiResponsible = _context.WikiResponsible

                                .Include(o => o.Employee)
                                .Include(d => d.Employee.Department)
                                .Include(e => e.Employee.Designation)
                                .Include(e => e.Employee.DepartmentNavigation)
                                .Include(e => e.Employee.User.UserGroupUser)
                                .Include(e => e.UserGroup)
                                .Where(s => s.ApplicationWikiLineDutyId == id.Value).AsNoTracking().ToList();
            wikiResponsible.ForEach(s =>
            {
                WikiResponsibilityModel wikiResponsibilityModel = new WikiResponsibilityModel
                {
                    WikiResponsibilityID = s.WikiResponsibilityId,
                    ApplicationWikiLineDutyID = s.ApplicationWikiLineDutyId,
                    EmployeeID = s.EmployeeId,
                    UserGroupID = s.UserGroupId,
                    UserID = s.UserId,
                    EmployeeName = s.Employee?.FirstName,
                    DepartmentName = s.Employee?.DepartmentNavigation?.Name,
                    DesignationName = s.Employee?.Designation?.Name,
                    HeadCount = s.Employee?.HeadCount,
                    UserGroupName = usergroup != null && s.Employee != null ? (usergroup.Where(w => w.UserId == s.Employee.UserId && w.UserGroupId == s.UserGroupId).Select(g => g.UserGroup?.Name).FirstOrDefault()) : "",
                };
                wikiResponsibilityModels.Add(wikiResponsibilityModel);
            });

            return wikiResponsibilityModels;
        }

        [HttpPost]
        [Route("IsWikiResponsibleByUser")]
        public async Task<bool> GetWikiResponsibleByUser(SearchModel searchModel)
        {
            WikiResponsibilityModel wikiResponsibilityModel = new WikiResponsibilityModel();
            var appWikiId = searchModel.Id;
            var userId = searchModel.UserID;

            var appWikiLineIds = await _context.ApplicationWikiLine.Where(s => s.ApplicationWikiId == appWikiId && s.IsAllowDocAccess == true).Select(a => a.ApplicationWikiLineId).ToListAsync();
            if (appWikiLineIds.Any())
            {
                var appWikiDutyIds = await _context.ApplicationWikiLineDuty.Where(w => appWikiLineIds.Contains(w.ApplicationWikiLineId.Value)).Select(s => s.ApplicationWikiLineDutyId).ToListAsync();
                var employeeIds = await _context.WikiResponsible.Where(r => appWikiDutyIds.Contains(r.ApplicationWikiLineDutyId.Value)).Select(g => g.EmployeeId).ToListAsync();
                var userIds = await _context.Employee.Where(e => employeeIds.Contains(e.EmployeeId)).Select(u => u.UserId).ToListAsync();
                return userIds.Contains(searchModel.UserID);
            }
            return false;
        }

        [HttpPost()]
        [Route("GetData")]
        public ActionResult<ApplicationWikiModel> GetData(SearchModel searchModel)
        {
            var applicationWiki = new ApplicationWiki();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        applicationWiki = _context.ApplicationWiki.Include(a => a.AppWikiCategoryMultiple).Include(b => b.AppWikiTopicMultiple).OrderByDescending(o => o.ApplicationWikiId).FirstOrDefault();
                        break;
                    case "Last":
                        applicationWiki = _context.ApplicationWiki.Include(a => a.AppWikiCategoryMultiple).Include(b => b.AppWikiTopicMultiple).OrderByDescending(o => o.ApplicationWikiId).LastOrDefault();
                        break;
                    case "Next":
                        applicationWiki = _context.ApplicationWiki.Include(a => a.AppWikiCategoryMultiple).Include(b => b.AppWikiTopicMultiple).OrderByDescending(o => o.ApplicationWikiId).LastOrDefault();
                        break;
                    case "Previous":
                        applicationWiki = _context.ApplicationWiki.Include(a => a.AppWikiCategoryMultiple).Include(b => b.AppWikiTopicMultiple).OrderByDescending(o => o.ApplicationWikiId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        applicationWiki = _context.ApplicationWiki.Include(a => a.AppWikiCategoryMultiple).Include(b => b.AppWikiTopicMultiple).OrderByDescending(o => o.ApplicationWikiId).FirstOrDefault();
                        break;
                    case "Last":
                        applicationWiki = _context.ApplicationWiki.Include(a => a.AppWikiCategoryMultiple).Include(b => b.AppWikiTopicMultiple).OrderByDescending(o => o.ApplicationWikiId).LastOrDefault();
                        break;
                    case "Next":
                        applicationWiki = _context.ApplicationWiki.Include(a => a.AppWikiCategoryMultiple).Include(b => b.AppWikiTopicMultiple).OrderBy(o => o.ApplicationWikiId).FirstOrDefault(s => s.ApplicationWikiId > searchModel.Id);
                        break;
                    case "Previous":
                        applicationWiki = _context.ApplicationWiki.Include(a => a.AppWikiCategoryMultiple).Include(b => b.AppWikiTopicMultiple).OrderByDescending(o => o.ApplicationWikiId).FirstOrDefault(s => s.ApplicationWikiId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<ApplicationWikiModel>(applicationWiki);
            if (applicationWiki != null)
            {
                var appWikiTaskLink = _context.AppWikiTaskLink.Select(n => new AppWikiTaskLinkModel { ApplicationWikiId = n.ApplicationWikiId, AppWikiTaskLinkId = n.AppWikiTaskLinkId, TaskLink = n.TaskLink, Subject = n.Subject }).Where(w => w.ApplicationWikiId == result.ApplicationWikiId).ToList();

                result.WikiCategoryIds = applicationWiki.AppWikiCategoryMultiple != null ? applicationWiki.AppWikiCategoryMultiple.Where(w => w.ApplicationWikiId == applicationWiki.ApplicationWikiId).Select(a => a.WikiCategoryId.Value).ToList() : new List<long>();
                result.WikiTopicIds = applicationWiki.AppWikiTopicMultiple != null ? applicationWiki.AppWikiTopicMultiple.Where(w => w.ApplicationWikiId == applicationWiki.ApplicationWikiId).Select(a => a.WikiTopicId.Value).ToList() : new List<long>();
                result.AppWikiTaskLinkModel = appWikiTaskLink.Where(t => t.ApplicationWikiId == result.ApplicationWikiId).ToList();
                result.IsEdits = true;
                var ApplicationWikiLineLists = _context.ApplicationWikiLine.Select(a => new { a.ApplicationWikiLineId, a.ApplicationWikiId, a.IsAllowDocAccess }).Where(a => a.ApplicationWikiId == result.ApplicationWikiId).ToList();
                var ApplicationWikiLineIds = ApplicationWikiLineLists.Select(s => s.ApplicationWikiLineId).ToList();
                var ApplicationWikiLineDutys = _context.ApplicationWikiLineDuty.Select(s => new { s.ApplicationWikiLineDutyId, s.ApplicationWikiLineId }).Where(w => ApplicationWikiLineIds.Contains(w.ApplicationWikiLineId.Value)).ToList();
                var ApplicationWikiLineDutyIds = ApplicationWikiLineDutys.Select(s => s.ApplicationWikiLineDutyId).ToList();
                var WikiResponsible = _context.WikiResponsible.Include(e => e.Employee).Select(s => new { s.ApplicationWikiLineDutyId, s.WikiResponsibilityId, s.EmployeeId, UserId = s.Employee.UserId.Value }).Where(w => ApplicationWikiLineDutyIds.Contains(w.ApplicationWikiLineDutyId.Value) && w.UserId != null).ToList();

                if (result.AddedByUserID == searchModel.UserID)
                {
                    result.IsEdits = true;
                }
                else
                {
                    if (ApplicationWikiLineLists != null && ApplicationWikiLineLists.Count > 0)
                    {
                        var ApplicationWikiLineDoc = ApplicationWikiLineLists.Where(a => a.ApplicationWikiId == result.ApplicationWikiId && a.IsAllowDocAccess == true).Select(a => a.IsAllowDocAccess).Distinct().Count();
                        if (ApplicationWikiLineDoc > 0)
                        {
                            result.IsEdits = true;
                        }
                        else
                        {
                            var ApplicationWikiLineDutyList = ApplicationWikiLineDutys != null ? ApplicationWikiLineDutys.Where(w => ApplicationWikiLineIds.Contains(w.ApplicationWikiLineId.Value)).Select(d => d.ApplicationWikiLineDutyId).ToList() : null;
                            var WikiResponsibleCount = WikiResponsible.Where(w => ApplicationWikiLineDutyList.Contains(w.ApplicationWikiLineDutyId.Value)).Count();
                            if (WikiResponsibleCount > 0)
                            {
                                var WikiResponsibleList = ApplicationWikiLineDutyList != null ? WikiResponsible.Where(w => ApplicationWikiLineDutyList.Contains(w.ApplicationWikiLineDutyId.Value) && w.UserId == searchModel.UserID).ToList() : null;
                                result.IsEdits = WikiResponsibleList != null && WikiResponsibleList.Count > 0 ? true : false;
                            }
                        }
                    }
                }
            }
            return result;
        }

        [HttpPost()]
        [Route("GetApplicationWikiBySearch")]
        public List<ApplicationWikiModel> GetApplicationWikiBySearch(ApplicationWikiSearchModel appwikisearchmodel)
        {
            var ApplicationWikiLineDuty = _context.ApplicationWikiLineDuty.ToList();
            var WikiResponsible = _context.WikiResponsible.Include(w => w.Employee).ToList();
            List<long> appwikiids = new List<long>();
            List<ApplicationWikiModel> applicationWikiModels = new List<ApplicationWikiModel>();
            if (appwikisearchmodel.WikiTypeId != null)
            {
                var appwikiid = _context.ApplicationWiki.Where(a => a.WikiTypeId == appwikisearchmodel.WikiTypeId).AsNoTracking().Select(s => s.ApplicationWikiId).ToList();
                appwikiids.AddRange(appwikiid);
            }
            if (appwikisearchmodel.WikiOwnerId != null)
            {
                var appwikiId = _context.ApplicationWiki.Where(a => a.WikiOwnerId == appwikisearchmodel.WikiOwnerId).AsNoTracking().Select(s => s.ApplicationWikiId).ToList();
                appwikiids.AddRange(appwikiId);
            }
            if (appwikisearchmodel.Title != null)
            {
                var appwikiId = _context.ApplicationWiki.Where(a => a.Title == appwikisearchmodel.Title).AsNoTracking().Select(s => s.ApplicationWikiId).ToList();
                appwikiids.AddRange(appwikiId);
            }
            var applicationWikis = _context.ApplicationWiki
            .Include(a => a.AddedByUser)
            .Include(m => m.ModifiedByUser)
            .Include(s => s.StatusCode)
            .Include(t => t.TranslationRequired)
            .Include(w => w.WikiEntryBy)
            .Include(o => o.WikiOwner)
            .Include(d => d.Department)
            .Include(p => p.Profile)
            .Include(c => c.AppWikiCategoryMultiple)
            .Include(r => r.AppWikiTopicMultiple)
            .Include(c => c.Plant)
            .Include(e => e.Employee)
            .Include(w => w.WikiType)
            .Include("AppWikiCategoryMultiple.WikiCategory")
            .Include("AppWikiTopicMultiple.WikiTopic")
            .Include(i => i.DraftApplicationWiki)
            .Where(s => appwikiids.Contains(s.ApplicationWikiId))
             .ToList();
            List<long?> applicationMasterCodeIds = new List<long?> { 147, 101 };

            List<ApplicationMasterDetail> applicationmasterdetail = new List<ApplicationMasterDetail>();

            applicationWikis.ForEach(s =>
            {
                ApplicationWikiModel applicationWikiModel = new ApplicationWikiModel();
                applicationWikiModel.ApplicationWikiId = s.ApplicationWikiId;
                applicationWikiModel.WikiDate = s.WikiDate;
                applicationWikiModel.WikiEntryById = s.WikiEntryById;
                applicationWikiModel.EntryBy = s.WikiEntryBy != null ? s.WikiEntryBy.UserName : string.Empty;
                applicationWikiModel.WikiTypeId = s.WikiTypeId;
                applicationWikiModel.WikiType = s.WikiType?.Value;
                applicationWikiModel.WikiOwnerId = s.WikiOwnerId;
                applicationWikiModel.WikiOwner = s.WikiOwner != null ? s.WikiOwner.Name : string.Empty;
                applicationWikiModel.WikiCategoryId = s.WikiCategoryId;
                //applicationWikiModel.WikiCategory = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiCategoryId) != null) ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiCategoryId).Value : string.Empty;
                applicationWikiModel.WikiTopicId = s.WikiTopicId;
                //applicationWikiModel.WikiTopic = (applicationmasterdetail.Count >= 0 && applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiTopicId) != null) ? applicationmasterdetail.FirstOrDefault(a => a.ApplicationMasterDetailId == s.WikiTopicId).Value : string.Empty;
                applicationWikiModel.Title = s.Title;
                applicationWikiModel.TranslationRequiredId = s.TranslationRequiredId;
                applicationWikiModel.TranslationRequired = s.TranslationRequired != null ? s.TranslationRequired.CodeValue : string.Empty;
                applicationWikiModel.Objective = s.Objective;
                applicationWikiModel.ScopeDesc = s.Scope;
                applicationWikiModel.PreRequisition = s.PreRequisition;
                applicationWikiModel.Content = s.Content;
                applicationWikiModel.VersionNo = s.VersionNo;
                applicationWikiModel.StatusCodeID = s.StatusCodeId;
                applicationWikiModel.StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : string.Empty;
                applicationWikiModel.AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : null;
                applicationWikiModel.AddedByUserID = s.AddedByUserId;
                applicationWikiModel.ModifiedByUserID = s.ModifiedByUserId;
                applicationWikiModel.AddedDate = s.AddedDate;
                applicationWikiModel.ModifiedDate = s.ModifiedDate;
                applicationWikiModel.ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : null;
                applicationWikiModel.IsMalay = s.IsMalay;
                applicationWikiModel.IsChinese = s.IsChinese;
                applicationWikiModel.ModifiedDate = s.ModifiedDate;
                applicationWikiModel.IsNoTranslation = !((s.IsMalay.HasValue && s.IsMalay.Value) || (s.IsChinese.HasValue && s.IsChinese.Value));
                applicationWikiModel.DepartmentId = s.DepartmentId;
                applicationWikiModel.ProfileNo = s.ProfileNo;
                applicationWikiModel.DepartmentName = s.Department?.Name;
                applicationWikiModel.Description = s.Description;
                applicationWikiModel.Remarks = s.Remarks;
                applicationWikiModel.EffectiveDate = s.EffectiveDate;
                applicationWikiModel.NewReviewDate = s.NewReviewDate;
                applicationWikiModel.SessionId = s.SessionId;
                applicationWikiModel.IsEdit = false;
                applicationWikiModel.ProfileReferenceNo = s.ProfileReferenceNo;
                applicationWikiModel.ProfileId = s.ProfileId;
                applicationWikiModel.ProfileName = s.Profile != null ? s.Profile.Name : "";
                applicationWikiModel.IsContentEntryFlag = s.IsContentEntry == true ? "Yes" : "No";
                applicationWikiModel.IsContentEntry = s.IsContentEntry;
                applicationWikiModel.TaskLink = s.TaskLink;
                applicationWikiModel.WikiCategoryIds = s.AppWikiCategoryMultiple != null ? s.AppWikiCategoryMultiple.Where(w => w.ApplicationWikiId == s.ApplicationWikiId).Select(a => a.WikiCategoryId.Value).ToList() : new List<long>();
                applicationWikiModel.WikiTopicIds = s.AppWikiTopicMultiple != null ? s.AppWikiTopicMultiple.Where(w => w.ApplicationWikiId == s.ApplicationWikiId).Select(a => a.WikiTopicId.Value).ToList() : new List<long>();
                applicationWikiModel.WikiCategory = s.AppWikiCategoryMultiple != null ? string.Join(",", s.AppWikiCategoryMultiple.Where(w => w.ApplicationWikiId == s.ApplicationWikiId).Select(s => s.WikiCategory.Value).ToList()) : "";
                applicationWikiModel.WikiTopic = s.AppWikiTopicMultiple != null ? string.Join(",", s.AppWikiTopicMultiple.Where(w => w.ApplicationWikiId == s.ApplicationWikiId).Select(s => s.WikiTopic.Value).ToList()) : "";
                applicationWikiModel.ProfilePlantId = s.ProfilePlantId;
                applicationWikiModel.ProfileDepartmentId = s.ProfileDepartmentId;
                applicationWikiModel.ProfileSectionId = s.ProfileSectionId;
                applicationWikiModel.ProfileSubSectionId = s.ProfileSubSectionId;
                applicationWikiModel.ProfileDivisionId = s.ProfileDivisionId;
                applicationWikiModel.DraftStatus = false;
                applicationWikiModel.EstimationPreparationTimeMonth = s.EstimationPreparationTimeMonth;
                applicationWikiModel.WikiOwnerTypeId = s.WikiOwnerTypeId;
                applicationWikiModel.PlantId = s.PlantId;
                applicationWikiModel.EmployeeId = s.EmployeeId;
                applicationWikiModel.ContentInfo = s.ContentInfo;
                applicationWikiModel.WikiOwnerName = s.WikiOwnerTypeId == 2401 ? (s.Plant?.Description) : (s.Employee?.FirstName);
                if (s.DraftApplicationWiki != null)
                {
                    var draft = s.DraftApplicationWiki.Where(a => a.ReleaseApplicationWikiId == s.ApplicationWikiId && a.StatusCodeId == 840).Count();
                    if (draft > 0)
                    {
                        applicationWikiModel.DraftStatus = true;
                    }
                }

                applicationWikiModels.Add(applicationWikiModel);
            });
            return applicationWikiModels;
        }

        // POST: api/User
        [HttpPost]
        [Route("InsertApplicationWiki")]
        public ApplicationWikiModel Post(ApplicationWikiModel value)
        {
            var SessionId = Guid.NewGuid();
            var profileNo = _generateDocumentNoSeries.GenerateDocumentProfileAutoNumber(new DocumentNoSeriesModel
            {
                ProfileID = value.ProfileId,
                AddedByUserID = value.AddedByUserID,
                StatusCodeID = 710,
                DepartmentName = value.ProfileNoModel?.DepartmentName,
                CompanyCode = value.ProfileNoModel?.CompanyCode,
                SectionName = value.ProfileNoModel?.SectionName,
                SubSectionName = value.ProfileNoModel?.SubSectionName,
                DepartmentId = value.ProfileNoModel?.DepartmentId,
                PlantID = value.ProfileNoModel?.PlantID,
                SectionId = value.ProfileNoModel?.SectionId,
                SubSectionId = value.ProfileNoModel?.SubSectionId,
                ScreenID = value.ScreenID,
                ScreenAutoNumberId = value.ApplicationWikiId,
            });
            var applicationWiki = new ApplicationWiki
            {
                WikiDate = value.WikiDate,
                WikiEntryById = value.WikiEntryById,
                WikiTypeId = value.WikiTypeId,
                WikiOwnerId = value.WikiOwnerId,
                WikiCategoryId = value.WikiCategoryId,
                WikiTopicId = value.WikiTopicId,
                Title = value.Title,
                TranslationRequiredId = value.TranslationRequiredId,
                Objective = value.Objective,
                Scope = value.ScopeDesc,
                PreRequisition = value.PreRequisition,
                Content = value.Content,
                StatusCodeId = value.StatusCodeID.Value,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                DepartmentId = value.DepartmentId,
                Description = value.Description,
                Remarks = value.Remarks,
                EffectiveDate = value.EffectiveDate,
                NewReviewDate = value.NewReviewDate,
                VersionNo = value.VersionNo,
                SessionId = SessionId,
                ProfileReferenceNo = profileNo,
                ProfileId = value.ProfileId,
                TaskLink = value.TaskLink,
                IsContentEntry = value.IsContentEntryFlag == "Yes" ? true : false,
                ProfilePlantId = value.ProfilePlantId,
                ProfileDepartmentId = value.ProfileDepartmentId,
                ProfileDivisionId = value.ProfileDivisionId,
                ProfileSectionId = value.ProfileSectionId,
                ProfileSubSectionId = value.ProfileSubSectionId,
                EstimationPreparationTimeMonth = value.EstimationPreparationTimeMonth,
                WikiOwnerTypeId = value.WikiOwnerTypeId,
                PlantId = value.PlantId,
                EmployeeId = value.EmployeeId,
                ContentInfo = value.ContentInfo,
            };
            if (!(value.IsNoTranslation.HasValue && value.IsNoTranslation.Value))
            {
                applicationWiki.IsMalay = value.IsMalay;
                applicationWiki.IsChinese = value.IsChinese;
            }
            if (value.WikiCategoryIds.Count > 0)
            {
                value.WikiCategoryIds.ForEach(h =>
                {
                    AppWikiCategoryMultiple AppWikiCategoryMultiple = new AppWikiCategoryMultiple
                    {
                        WikiCategoryId = h,
                    };
                    applicationWiki.AppWikiCategoryMultiple.Add(AppWikiCategoryMultiple);
                });
            }
            if (value.WikiTopicIds.Count > 0)
            {
                value.WikiTopicIds.ForEach(h =>
                {
                    AppWikiTopicMultiple appWikiTopicMultiple = new AppWikiTopicMultiple
                    {
                        WikiTopicId = h,
                    };
                    applicationWiki.AppWikiTopicMultiple.Add(appWikiTopicMultiple);
                });
            }
            if (value.AppWikiTaskLinkModel != null && value.AppWikiTaskLinkModel.Count > 0)
            {
                value.AppWikiTaskLinkModel.ForEach(h =>
                {
                    AppWikiTaskLink appWikiTaskLink = new AppWikiTaskLink
                    {
                        TaskLink = h.TaskLink,
                        Subject = h.Subject,
                    };
                    applicationWiki.AppWikiTaskLink.Add(appWikiTaskLink);
                });
            }
            _context.ApplicationWiki.Add(applicationWiki);
            _context.SaveChanges();
            value.ApplicationWikiId = applicationWiki.ApplicationWikiId;
            value.SessionId = SessionId;
            value.ProfileReferenceNo = profileNo;
            return value;
        }
        [HttpPost]
        [Route("InsertApplicationWikiLine")]
        public ApplicationWikiLineModel InsertApplicationWikiLine(ApplicationWikiLineModel value)
        {
            var SessionId = Guid.NewGuid();
            var ApplicationWikiLine = new ApplicationWikiLine
            {
                ApplicationWikiId = value.ApplicationWikiId,
                DutyId = value.DutyId,
                Responsibility = value.Responsibility,
                FunctionLink = value.FunctionLink,
                PageLink = value.PageLink,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                NotificationAdvice = value.NotificationAdviceFlag == "Yes" ? true : false,
                NotificationAdviceTypeId = value.NotificationAdviceTypeId,
                RepeatId = value.RepeatId,
                CustomId = value.CustomId,
                DueDate = value.DueDate == null ? DateTime.Now : value.DueDate,
                Monthly = value.Monthly,
                Yearly = value.Yearly,
                EventDescription = value.EventDescription,
                DaysOfWeek = value.DaysOfWeek,
                SessionId = SessionId,
                NotificationStatusId = value.NotificationStatusId,
                Title = value.Title,
                Message = value.Message,
                ScreenId = value.ScreenId,
                NotifyEndDate = value.NotifyEndDate,
                IsAllowDocAccess = value.IsAllowDocAccess,
            };
            _context.ApplicationWikiLine.Add(ApplicationWikiLine);
            _context.SaveChanges();
            value.ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId;
            if (value.WeeklyIds != null && value.WeeklyIds.Count > 0)
            {
                value.WeeklyIds.ForEach(u =>
                {
                    var applicationWikiWeekly = new ApplicationWikiWeekly()
                    {
                        ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId,
                        WeeklyId = u,
                        CustomType = "Weekly",
                    };
                    _context.ApplicationWikiWeekly.Add(applicationWikiWeekly);
                    _context.SaveChanges();
                });
            }
            if (value.DaysOfWeek == true && value.DaysOfWeekIds != null && value.DaysOfWeekIds.Count > 0)
            {
                value.DaysOfWeekIds.ForEach(u =>
                {
                    var applicationWikiWeekly = new ApplicationWikiWeekly()
                    {
                        ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId,
                        WeeklyId = u,
                        CustomType = "Yearly",
                    };
                    _context.ApplicationWikiWeekly.Add(applicationWikiWeekly);
                    _context.SaveChanges();
                });
            }
            if (value.NotifyIds != null && value.NotifyIds.Count > 0)
            {
                value.NotifyIds.ForEach(u =>
                {
                    var applicationWikiLineNotify = new ApplicationWikiLineNotify()
                    {
                        ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId,
                        NotifyUserId = u,
                    };
                    _context.ApplicationWikiLineNotify.Add(applicationWikiLineNotify);
                    _context.SaveChanges();
                });
            }
            value.SessionId = SessionId;
            value.NotificationAdvice = ApplicationWikiLine.NotificationAdvice;
            NotificationHandeler(value);
            return value;
        }
        private void NotificationHandeler(ApplicationWikiLineModel value)
        {
            var notificationHandlerItems = _context.NotificationHandler.Where(w => w.SessionId == value.SessionId).ToList();
            List<NotificationHandler> NotificationHandlers = new List<NotificationHandler>();
            NotificationHandler NotificationHandler = new NotificationHandler();
            if (notificationHandlerItems != null)
            {
                notificationHandlerItems.ForEach(h =>
                {
                    var notificationHandler = _context.NotificationHandler.SingleOrDefault(w => w.NotificationHandlerId == h.NotificationHandlerId);
                    if (notificationHandler != null)
                    {
                        notificationHandler.NotifyStatusId = 2;
                    }
                    _context.SaveChanges();
                });
            }
            if (value.NotificationAdvice == true && value.DueDate != null)
            {
                var DueDate = value.DueDate;
                value.DueDate = value.DueDate == null ? DateTime.Now : value.DueDate;
                if (value.RepeatId != null && value.RepeatId != 1881)
                {
                    if (value.RepeatId == 1882)
                    {
                        NotificationHandler.NextNotifyDate = DueDate == null ? DateTime.Now.AddDays(1) : DueDate;
                        NotificationHandler.AddedDays = 1;
                    }
                    else if (value.RepeatId == 1883)
                    {
                        DateTime Monday = value.DueDate.Value.Date;
                        int daysUntilMonday = ((int)DayOfWeek.Monday - (int)Monday.DayOfWeek + 7) % 7;
                        NotificationHandler.NextNotifyDate = Monday.AddDays(daysUntilMonday);
                        NotificationHandler.AddedDays = 7;
                    }
                    else if (value.RepeatId == 1884)
                    {
                        DateTime Monday = value.DueDate.Value.Date;
                        int daysUntilMonday = ((int)DayOfWeek.Monday - (int)Monday.DayOfWeek + 7) % 7;
                        DateTime NextMonday = Monday.AddDays(daysUntilMonday);
                        NotificationHandler.NextNotifyDate = NextMonday.AddDays(7);
                        NotificationHandler.AddedDays = 14;
                    }
                    else if (value.RepeatId == 1885)
                    {
                        NotificationHandler.NextNotifyDate = new DateTime(value.DueDate.Value.Year, value.DueDate.Value.Month, 1).AddMonths(+1);
                        var totalDays = (NotificationHandler.NextNotifyDate.Value.AddMonths(1) - NotificationHandler.NextNotifyDate.Value).TotalDays;
                        NotificationHandler.AddedDays = 0;
                    }
                    else if (value.RepeatId == 1886)
                    {
                        NotificationHandler.NextNotifyDate = new DateTime(value.DueDate.Value.Year, value.DueDate.Value.Month, 1).AddYears(+1);
                        NotificationHandler.AddedDays = 0;
                    }
                }
                if (value.CustomId != null)
                {
                    if (value.CustomId == 2131)
                    {
                        NotificationHandler.NextNotifyDate = DueDate == null ? DateTime.Now.AddDays(1) : DueDate;
                        NotificationHandler.AddedDays = 1;
                    }
                    if (value.CustomId == 2132)
                    {
                        if (value.WeeklyIds.Count > 0)
                        {

                            value.WeeklyIds.ForEach(h =>
                            {
                                if (h == 1445)
                                {
                                    NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                    DateTime Monday = value.DueDate.Value.Date;
                                    int daysUntilMonday = ((int)DayOfWeek.Monday - (int)Monday.DayOfWeek + 7) % 7;
                                    DateTime NextMonday = Monday.AddDays(daysUntilMonday);
                                    NotificationWeeklyHandler.NextNotifyDate = NextMonday;
                                    NotificationWeeklyHandler.AddedDays = 7;
                                    NotificationHandlers.Add(NotificationWeeklyHandler);
                                }
                                else if (h == 1446)
                                {
                                    NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                    DateTime Tuesday = value.DueDate.Value.Date;
                                    int daysUntilTuesday = ((int)DayOfWeek.Tuesday - (int)Tuesday.DayOfWeek + 7) % 7;
                                    DateTime NextTuesday = Tuesday.AddDays(daysUntilTuesday);
                                    NotificationWeeklyHandler.NextNotifyDate = NextTuesday;
                                    NotificationWeeklyHandler.AddedDays = 7;
                                    NotificationHandlers.Add(NotificationWeeklyHandler);
                                }
                                else if (h == 1447)
                                {
                                    NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                    DateTime Wednesday = value.DueDate.Value.Date;
                                    int daysUntilWednesday = ((int)DayOfWeek.Wednesday - (int)Wednesday.DayOfWeek + 7) % 7;
                                    DateTime NextWednesday = Wednesday.AddDays(daysUntilWednesday);
                                    NotificationWeeklyHandler.NextNotifyDate = NextWednesday;
                                    NotificationWeeklyHandler.AddedDays = 7;
                                    NotificationHandlers.Add(NotificationWeeklyHandler);
                                }
                                else if (h == 1448)
                                {
                                    NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                    DateTime Thursday = value.DueDate.Value.Date;
                                    int daysUntilThursday = ((int)DayOfWeek.Thursday - (int)Thursday.DayOfWeek + 7) % 7;
                                    DateTime NextThursday = Thursday.AddDays(daysUntilThursday);
                                    NotificationWeeklyHandler.NextNotifyDate = NextThursday;
                                    NotificationWeeklyHandler.AddedDays = 7;
                                    NotificationHandlers.Add(NotificationWeeklyHandler);
                                }
                                else if (h == 1449)
                                {
                                    NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                    DateTime Friday = value.DueDate.Value.Date;
                                    int daysUntilFriday = ((int)DayOfWeek.Friday - (int)Friday.DayOfWeek + 7) % 7;
                                    DateTime NextFriday = Friday.AddDays(daysUntilFriday);
                                    NotificationWeeklyHandler.NextNotifyDate = NextFriday;
                                    NotificationWeeklyHandler.AddedDays = 7;
                                    NotificationHandlers.Add(NotificationWeeklyHandler);
                                }
                                else if (h == 1450)
                                {
                                    NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                    DateTime Saturday = value.DueDate.Value.Date;
                                    int daysUntilSaturday = ((int)DayOfWeek.Saturday - (int)Saturday.DayOfWeek + 7) % 7;
                                    DateTime NextSaturday = Saturday.AddDays(daysUntilSaturday);
                                    NotificationWeeklyHandler.NextNotifyDate = NextSaturday;
                                    NotificationWeeklyHandler.AddedDays = 7;
                                    NotificationHandlers.Add(NotificationWeeklyHandler);
                                }
                                else if (h == 1451)
                                {
                                    NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                    DateTime Sunday = value.DueDate.Value.Date;
                                    int daysUntilSunday = ((int)DayOfWeek.Sunday - (int)Sunday.DayOfWeek + 7) % 7;
                                    DateTime NextSunday = Sunday.AddDays(daysUntilSunday);
                                    NotificationWeeklyHandler.NextNotifyDate = NextSunday;
                                    NotificationWeeklyHandler.AddedDays = 7;
                                    NotificationHandlers.Add(NotificationWeeklyHandler);
                                }
                            });
                        }
                    }
                    if (value.CustomId == 2133)
                    {
                        var date = new DateTime(value.DueDate.Value.Year, value.DueDate.Value.Month, value.Monthly.Value);
                        if (DueDate == null)
                        {
                            date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, value.Monthly.Value);

                            if (DateTime.Now.Day >= value.Monthly)
                            {
                                var currentday = DateTime.Now.AddMonths(1);
                                date = new DateTime(currentday.Year, currentday.Month, value.Monthly.Value);
                            }
                        }
                        NotificationHandler.NextNotifyDate = date;
                        NotificationHandler.AddedDays = 0;
                    }
                    if (value.CustomId == 2134)
                    {
                        var dates = (new DateTime(DateTime.Now.Year, value.Yearly.Value, DateTime.Now.Day)).AddYears(1);
                        var dt = new DateTime(dates.Date.Year, dates.Date.Month, 1);
                        if (value.DaysOfWeek == true)
                        {
                            if (value.DaysOfWeekIds.Count > 0)
                            {
                                value.DaysOfWeekIds.ForEach(h =>
                                {
                                    if (h == 1445)
                                    {
                                        NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                        var t = dt.AddDays((7 - dt.DayOfWeek.GetHashCode() + DayOfWeek.Monday.GetHashCode()) % 7);
                                        NotificationWeeklyHandler.NextNotifyDate = t;
                                        var nextDate = t.Date.AddDays(7);
                                        NotificationWeeklyHandler.AddedDays = (t.Date.Month == nextDate.Month) ? 7 : (DateTime.IsLeapYear(t.Year) ? 366 : 365);
                                        NotificationHandlers.Add(NotificationWeeklyHandler);
                                    }
                                    else if (h == 1446)
                                    {
                                        NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                        var t = dt.AddDays((7 - dt.DayOfWeek.GetHashCode() + DayOfWeek.Tuesday.GetHashCode()) % 7);
                                        NotificationWeeklyHandler.NextNotifyDate = t;
                                        var nextDate = t.Date.AddDays(7);
                                        NotificationWeeklyHandler.AddedDays = (t.Date.Month == nextDate.Month) ? 7 : (DateTime.IsLeapYear(t.Year) ? 366 : 365);
                                        NotificationHandlers.Add(NotificationWeeklyHandler);
                                    }
                                    else if (h == 1447)
                                    {
                                        NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                        var t = dt.AddDays((7 - dt.DayOfWeek.GetHashCode() + DayOfWeek.Wednesday.GetHashCode()) % 7);
                                        NotificationWeeklyHandler.NextNotifyDate = t;
                                        var nextDate = t.Date.AddDays(7);
                                        NotificationWeeklyHandler.AddedDays = (t.Date.Month == nextDate.Month) ? 7 : (DateTime.IsLeapYear(t.Year) ? 366 : 365);
                                        NotificationHandlers.Add(NotificationWeeklyHandler);
                                    }
                                    else if (h == 1448)
                                    {
                                        NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                        var t = dt.AddDays((7 - dt.DayOfWeek.GetHashCode() + DayOfWeek.Thursday.GetHashCode()) % 7);
                                        NotificationWeeklyHandler.NextNotifyDate = t;
                                        var nextDate = t.Date.AddDays(7);
                                        NotificationWeeklyHandler.AddedDays = (t.Date.Month == nextDate.Month) ? 7 : (DateTime.IsLeapYear(t.Year) ? 366 : 365);
                                        NotificationHandlers.Add(NotificationWeeklyHandler);
                                    }
                                    else if (h == 1449)
                                    {
                                        NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                        var t = dt.AddDays((7 - dt.DayOfWeek.GetHashCode() + DayOfWeek.Friday.GetHashCode()) % 7);
                                        NotificationWeeklyHandler.NextNotifyDate = t;
                                        var nextDate = t.Date.AddDays(7);
                                        NotificationWeeklyHandler.AddedDays = (t.Date.Month == nextDate.Month) ? 7 : (DateTime.IsLeapYear(t.Year) ? 366 : 365);
                                        NotificationHandlers.Add(NotificationWeeklyHandler);
                                    }
                                    else if (h == 1450)
                                    {
                                        NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                        var t = dt.AddDays((7 - dt.DayOfWeek.GetHashCode() + DayOfWeek.Saturday.GetHashCode()) % 7);
                                        NotificationWeeklyHandler.NextNotifyDate = t;
                                        var nextDate = t.Date.AddDays(7);
                                        NotificationWeeklyHandler.AddedDays = (t.Date.Month == nextDate.Month) ? 7 : (DateTime.IsLeapYear(t.Year) ? 366 : 365);
                                        NotificationHandlers.Add(NotificationWeeklyHandler);
                                    }
                                    else if (h == 1451)
                                    {
                                        NotificationHandler NotificationWeeklyHandler = new NotificationHandler();
                                        var t = dt.AddDays((7 - dt.DayOfWeek.GetHashCode() + DayOfWeek.Sunday.GetHashCode()) % 7);
                                        NotificationWeeklyHandler.NextNotifyDate = t;
                                        var nextDate = t.Date.AddDays(7);
                                        NotificationWeeklyHandler.AddedDays = (t.Date.Month == nextDate.Month) ? 7 : (DateTime.IsLeapYear(t.Year) ? 366 : 365);
                                        NotificationHandlers.Add(NotificationWeeklyHandler);
                                    }
                                });
                            }
                        }
                        else
                        {
                            NotificationHandler.NextNotifyDate = dates;
                            NotificationHandler.AddedDays = 0;
                        }
                    }
                }
                NotificationHandlers.Add(NotificationHandler);
                if (NotificationHandlers.Count > 0)
                {
                    NotificationHandlers.ForEach(h =>
                    {
                        if (h.NextNotifyDate != null)
                        {
                            if (h.NextNotifyDate.Value.Date <= value.NotifyEndDate || value.NotifyEndDate == null)
                            {
                                // if (value.NotifyIds.Count > 0)
                                //{
                                //value.NotifyIds.ForEach(u =>
                                // {
                                NotificationHandler NotificationHandlerinsert = new NotificationHandler();
                                NotificationHandlerinsert.SessionId = value.SessionId;
                                NotificationHandlerinsert.NotifyStatusId = value.NotificationStatusId;
                                NotificationHandlerinsert.Title = value.Title;
                                NotificationHandlerinsert.Message = value.Message;
                                NotificationHandlerinsert.NextNotifyDate = h.NextNotifyDate;
                                NotificationHandlerinsert.AddedDays = h.AddedDays;
                                NotificationHandlerinsert.ScreenId = value.ScreenId;
                                // NotificationHandlerinsert.NotifyTo = u;
                                _context.NotificationHandler.Add(NotificationHandlerinsert);
                                //});
                                //}
                            }

                        }
                    });
                }
                _context.SaveChanges();
            }
        }

        [HttpPost]
        [Route("InsertApplicationWikiRecurrence")]
        public ApplicationWikiRecurrenceModel InsertApplicationWikiRecurrence(ApplicationWikiRecurrenceModel value)
        {
            var ApplicationWikiRecurrenceData = _context.ApplicationWikiRecurrence.Where(p => p.ApplicationWikiLineId == value.ApplicationWikiLineId).ToList();
            if (ApplicationWikiRecurrenceData.Count > 0)
            {
                var ApplicationWikiRecurrences = _context.ApplicationWikiRecurrence.SingleOrDefault(p => p.ApplicationWikiLineId == value.ApplicationWikiLineId);
                ApplicationWikiRecurrences.ApplicationWikiLineId = value.ApplicationWikiLineId;
                ApplicationWikiRecurrences.TypeId = value.TypeId;
                ApplicationWikiRecurrences.RepeatNos = value.RepeatNos;
                ApplicationWikiRecurrences.OccurenceOptionId = value.OccurenceOptionId;
                ApplicationWikiRecurrences.StatusCodeId = value.StatusCodeID;
                ApplicationWikiRecurrences.ModifiedByUserId = value.ModifiedByUserID;
                ApplicationWikiRecurrences.ModifiedDate = value.ModifiedDate;
                ApplicationWikiRecurrences.Sunday = value.SelectedDay.Contains(0) ? true : false;
                ApplicationWikiRecurrences.Monday = value.SelectedDay.Contains(1) ? true : false;
                ApplicationWikiRecurrences.Tuesday = value.SelectedDay.Contains(2) ? true : false;
                ApplicationWikiRecurrences.Wednesday = value.SelectedDay.Contains(3) ? true : false;
                ApplicationWikiRecurrences.Thursday = value.SelectedDay.Contains(4) ? true : false;
                ApplicationWikiRecurrences.Friday = value.SelectedDay.Contains(5) ? true : false;
                ApplicationWikiRecurrences.Saturyday = value.SelectedDay.Contains(6) ? true : false;
                ApplicationWikiRecurrences.StartDate = value.StartDate;
                ApplicationWikiRecurrences.EndDate = value.EndDate;
                _context.SaveChanges();
                return value;
            }
            else
            {

                var ApplicationWikiRecurrence = new ApplicationWikiRecurrence
                {
                    ApplicationWikiLineId = value.ApplicationWikiLineId,
                    TypeId = value.TypeId,
                    RepeatNos = value.RepeatNos,
                    OccurenceOptionId = value.OccurenceOptionId,
                    NoOfOccurences = value.NoOfOccurences,
                    AddedByUserId = value.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = value.StatusCodeID.Value,
                    Sunday = value.SelectedDay.Contains(0) ? true : false,
                    Monday = value.SelectedDay.Contains(1) ? true : false,
                    Tuesday = value.SelectedDay.Contains(2) ? true : false,
                    Wednesday = value.SelectedDay.Contains(3) ? true : false,
                    Thursday = value.SelectedDay.Contains(4) ? true : false,
                    Friday = value.SelectedDay.Contains(5) ? true : false,
                    Saturyday = value.SelectedDay.Contains(6) ? true : false,
                    StartDate = value.StartDate,
                    EndDate = value.EndDate,
                };
                _context.ApplicationWikiRecurrence.Add(ApplicationWikiRecurrence);
                _context.SaveChanges();
                value.ApplicationWikiRecurrenceId = ApplicationWikiRecurrence.ApplicationWikiRecurrenceId;
                return value;
            }
        }
        [HttpPost]
        [Route("InsertApplicationWikiLineDuty")]
        public ApplicationWikiLineDutyModel InsertApplicationWikiLineDuty(ApplicationWikiLineDutyModel value)
        {

            var ApplicationWikiLineDuty = new ApplicationWikiLineDuty
            {
                ApplicationWikiLineId = value.ApplicationWikiLineId,
                DepartmantId = value.DepartmentId,
                DesignationId = value.DesignationId,
                EmployeeId = value.EmployeeId,
                CompanyId = value.CompanyId,
                DesignationNumber = value.DesignationNumber,
                DutyNo = value.DutyNo,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,
                Type = value.Type,
                Description = value.Description,
            };
            _context.ApplicationWikiLineDuty.Add(ApplicationWikiLineDuty);
            _context.SaveChanges();
            value.ApplicationWikiLineDutyId = ApplicationWikiLineDuty.ApplicationWikiLineDutyId;
            var userGroupList = _context.UserGroupUser.ToList();
            var employeeList = _context.Employee.ToList();
            if (value.EmployeeIDs != null && value.EmployeeIDs.Count > 0)
            {
                value.EmployeeIDs.ForEach(s =>
                {
                    //var userid = employeeList.Where(e => e.EmployeeId == s).Select(u => u.UserId).FirstOrDefault();
                    var existing = _context.WikiResponsible.Where(e => e.EmployeeId == s && e.ApplicationWikiLineDutyId == value.ApplicationWikiLineDutyId).FirstOrDefault();
                    if (existing == null)
                    {
                        var wikiResponsible = new WikiResponsible
                        {
                            EmployeeId = s,
                            //UserGroupId = userid != null && userGroupList != null ? userGroupList.Where(w => w.UserId == userid).Select(i=>i.UserGroupId).FirstOrDefault() : null,
                            ApplicationWikiLineDutyId = value.ApplicationWikiLineDutyId,
                        };
                        _context.WikiResponsible.Add(wikiResponsible);
                        _context.SaveChanges();
                    }
                });
            }
            else if (value.UserGroupIDs != null && value.UserGroupIDs.Count > 0)
            {
                value.UserGroupIDs.ForEach(s =>
                {
                    var userGroupuserIds = userGroupList?.Where(u => u.UserGroupId == s).Select(s => s.UserId).ToList();
                    if (userGroupuserIds != null && userGroupuserIds.Count > 0)
                    {
                        var userGroupEmpIds = employeeList?.Where(e => userGroupuserIds.Contains(e.UserId)).Select(e => e.EmployeeId).ToList();
                        if (userGroupEmpIds != null && userGroupEmpIds.Count > 0)
                        {
                            userGroupEmpIds.ForEach(emp =>
                            {
                                var existing = _context.WikiResponsible.Where(e => e.EmployeeId == emp && e.ApplicationWikiLineDutyId == value.ApplicationWikiLineDutyId).FirstOrDefault();
                                if (existing == null)
                                {
                                    var wikiResponsible = new WikiResponsible
                                    {
                                        EmployeeId = emp,
                                        UserGroupId = s,
                                        ApplicationWikiLineDutyId = value.ApplicationWikiLineDutyId,
                                    };
                                    _context.WikiResponsible.Add(wikiResponsible);
                                    _context.SaveChanges();
                                }
                            });
                        }
                    }
                });
            }

            return value;
        }
        [HttpPut]
        [Route("CreateVersion")]
        public async Task<ApplicationWikiModel> CreateVersion(ApplicationWikiModel value)
        {
            value.SessionId = value.SessionId.Value;
            var applicationWiki = _context.ApplicationWiki.SingleOrDefault(p => p.ApplicationWikiId == value.ApplicationWikiId);
            if (applicationWiki != null)
            {
                var verObject = _mapper.Map<ApplicationWikiModel>(applicationWiki);
                verObject.WikiCategoryIds = _context.AppWikiCategoryMultiple.AsNoTracking().Where(w => w.ApplicationWikiId == applicationWiki.ApplicationWikiId).Select(a => a.WikiCategoryId.Value).ToList();
                verObject.WikiTopicIds = _context.AppWikiTopicMultiple.AsNoTracking().Where(w => w.ApplicationWikiId == applicationWiki.ApplicationWikiId).Select(a => a.WikiTopicId.Value).ToList();
                verObject.ApplicationWikiLines = ApplicationWikiLine(value.ApplicationWikiId);
                var AppWikiTaskLink = _context.AppWikiTaskLink.Select(n => new AppWikiTaskLinkModel { ApplicationWikiId = n.ApplicationWikiId, AppWikiTaskLinkId = n.AppWikiTaskLinkId, TaskLink = n.TaskLink, Subject = n.Subject }).Where(w => w.ApplicationWikiId == value.ApplicationWikiId).ToList();
                verObject.AppWikiTaskLinkModel = AppWikiTaskLink;
                var versionInfo = new TableDataVersionInfoModel<ApplicationWikiModel>
                {
                    JsonData = JsonSerializer.Serialize(verObject),
                    AddedByUserId = value.AddedByUserID.Value,
                    AddedByUserID = value.AddedByUserID.Value,
                    StatusCodeID = value.StatusCodeID.Value,
                    AddedDate = DateTime.Now,
                    SessionId = value.SessionId.Value,
                    ScreenId = value.ScreenID,
                    TableName = "ApplicationWiki",
                    PrimaryKey = value.ApplicationWikiId,
                    ReferenceInfo = value.ReferenceInfo
                };
                await _repository.Insert(versionInfo);
            }
            return value;
        }
        [HttpPut]
        [Route("TempVersion")]
        public async Task<long> TempVersion(ApplicationWikiModel value)
        {
            value.SessionId = value.SessionId.Value;
            var ProductionCycle = _context.ApplicationWiki.SingleOrDefault(p => p.ApplicationWikiId == value.ApplicationWikiId);

            if (ProductionCycle != null)
            {
                var verObject = _mapper.Map<ApplicationWikiModel>(ProductionCycle);
                verObject.WikiCategoryIds = _context.AppWikiCategoryMultiple.AsNoTracking().Where(w => w.ApplicationWikiId == ProductionCycle.ApplicationWikiId).Select(a => a.WikiCategoryId.Value).ToList();
                verObject.WikiTopicIds = _context.AppWikiTopicMultiple.AsNoTracking().Where(w => w.ApplicationWikiId == ProductionCycle.ApplicationWikiId).Select(a => a.WikiTopicId.Value).ToList();
                verObject.ApplicationWikiLines = ApplicationWikiLine(value.ApplicationWikiId);
                var AppWikiTaskLink = _context.AppWikiTaskLink.Select(n => new AppWikiTaskLinkModel { ApplicationWikiId = n.ApplicationWikiId, AppWikiTaskLinkId = n.AppWikiTaskLinkId, TaskLink = n.TaskLink, Subject = n.Subject }).Where(w => w.ApplicationWikiId == value.ApplicationWikiId).ToList();
                verObject.AppWikiTaskLinkModel = AppWikiTaskLink;
                var versionInfo = new TableDataVersionInfoModel<ApplicationWikiModel>
                {
                    JsonData = JsonSerializer.Serialize(verObject),
                    AddedByUserId = value.AddedByUserID.Value,
                    AddedByUserID = value.AddedByUserID.Value,
                    StatusCodeID = value.StatusCodeID.Value,
                    AddedDate = DateTime.Now,
                    SessionId = value.SessionId.Value,
                    ScreenId = value.ScreenID,
                    TableName = "ApplicationWiki",
                    PrimaryKey = value.ApplicationWikiId,
                    ReferenceInfo = "Temp Version - undo",
                };
                var result = await _repository.Insert(versionInfo);
                return result.VersionTableId;
            }
            return -1;
        }
        [HttpGet]
        [Route("UndoVersion")]
        public async Task<ApplicationWikiModel> UndoVersion(int Id)
        {
            // try
            // {
            var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

            if (tableData != null)
            {
                var verObject = JsonSerializer.Deserialize<ApplicationWikiModel>(tableData.JsonData);

                var applicationWiki = _context.ApplicationWiki.SingleOrDefault(p => p.ApplicationWikiId == verObject.ApplicationWikiId);


                if (verObject != null && applicationWiki != null)
                {
                    applicationWiki.WikiDate = verObject.WikiDate;
                    applicationWiki.WikiEntryById = verObject.WikiEntryById;
                    applicationWiki.WikiTypeId = verObject.WikiTypeId;
                    applicationWiki.WikiOwnerId = verObject.WikiOwnerId;
                    applicationWiki.WikiCategoryId = verObject.WikiCategoryId;
                    applicationWiki.WikiTopicId = verObject.WikiTopicId;
                    applicationWiki.Title = verObject.Title;
                    applicationWiki.TranslationRequiredId = verObject.TranslationRequiredId;
                    applicationWiki.Objective = verObject.Objective;
                    applicationWiki.Scope = verObject.ScopeDesc;
                    applicationWiki.PreRequisition = verObject.PreRequisition;
                    applicationWiki.Content = verObject.Content;
                    applicationWiki.IsContentEntry = verObject.IsContentEntry;
                    applicationWiki.TaskLink = verObject.TaskLink;
                    applicationWiki.EstimationPreparationTimeMonth = verObject.EstimationPreparationTimeMonth;
                    applicationWiki.WikiOwnerTypeId = verObject.WikiOwnerTypeId;
                    applicationWiki.PlantId = verObject.PlantId;
                    applicationWiki.EmployeeId = verObject.EmployeeId;
                    applicationWiki.ContentInfo = verObject.ContentInfo;
                    if (string.IsNullOrEmpty(verObject.VersionNo))
                    {
                        applicationWiki.VersionNo = "0";
                    }
                    verObject.VersionNo = (Convert.ToDouble(verObject.VersionNo) + 1).ToString();
                    if (!(verObject.IsNoTranslation.HasValue && verObject.IsNoTranslation.Value))
                    {
                        applicationWiki.IsMalay = verObject.IsMalay;
                        applicationWiki.IsChinese = verObject.IsChinese;
                    }
                    applicationWiki.VersionNo = verObject.VersionNo;
                    applicationWiki.StatusCodeId = verObject.StatusCodeID.Value;
                    applicationWiki.ModifiedByUserId = verObject.ModifiedByUserID;
                    applicationWiki.ModifiedDate = DateTime.Now;
                    applicationWiki.DepartmentId = verObject.DepartmentId;
                    applicationWiki.Description = verObject.Description;
                    applicationWiki.Remarks = verObject.Remarks;
                    applicationWiki.EffectiveDate = verObject.EffectiveDate;
                    applicationWiki.NewReviewDate = verObject.NewReviewDate;
                    applicationWiki.ProfilePlantId = verObject.ProfilePlantId;
                    applicationWiki.ProfileDepartmentId = verObject.ProfileDepartmentId;
                    applicationWiki.ProfileDivisionId = verObject.ProfileDivisionId;
                    applicationWiki.ProfileSectionId = verObject.ProfileSectionId;
                    applicationWiki.ProfileSubSectionId = verObject.ProfileSubSectionId;
                    _context.SaveChanges();
                    var AppWikiCategoryMultipleRemove = _context.AppWikiCategoryMultiple.Where(w => w.ApplicationWikiId == applicationWiki.ApplicationWikiId).ToList();
                    if (AppWikiCategoryMultipleRemove.Count > 0)
                    {
                        _context.AppWikiCategoryMultiple.RemoveRange(AppWikiCategoryMultipleRemove);
                        _context.SaveChanges();
                    }
                    var AppWikiTopicMultipleRemove = _context.AppWikiTopicMultiple.Where(w => w.ApplicationWikiId == applicationWiki.ApplicationWikiId).ToList();
                    if (AppWikiTopicMultipleRemove.Count > 0)
                    {
                        _context.AppWikiTopicMultiple.RemoveRange(AppWikiTopicMultipleRemove);
                        _context.SaveChanges();
                    }
                    var appWikiTaskLink = _context.AppWikiTaskLink.Where(w => w.ApplicationWikiId == applicationWiki.ApplicationWikiId).ToList();
                    if (appWikiTaskLink.Count > 0)
                    {
                        _context.AppWikiTaskLink.RemoveRange(appWikiTaskLink);
                        _context.SaveChanges();
                    }
                    if (verObject.WikiCategoryIds.Count > 0)
                    {
                        verObject.WikiCategoryIds.ForEach(h =>
                        {
                            AppWikiCategoryMultiple AppWikiCategoryMultiple = new AppWikiCategoryMultiple
                            {
                                ApplicationWikiId = applicationWiki.ApplicationWikiId,
                                WikiCategoryId = h,
                            };
                            _context.AppWikiCategoryMultiple.Add(AppWikiCategoryMultiple);
                        });
                        _context.SaveChanges();
                    }
                    if (verObject.AppWikiTaskLinkModel != null && verObject.AppWikiTaskLinkModel.Count > 0)
                    {
                        verObject.AppWikiTaskLinkModel.ForEach(h =>
                        {
                            AppWikiTaskLink appWikiTaskLink = new AppWikiTaskLink
                            {
                                ApplicationWikiId = applicationWiki.ApplicationWikiId,
                                TaskLink = h.TaskLink,
                                Subject = h.Subject
                            };
                            _context.AppWikiTaskLink.Add(appWikiTaskLink);
                        });
                        _context.SaveChanges();
                    }
                    if (verObject.WikiTopicIds.Count > 0)
                    {
                        verObject.WikiTopicIds.ForEach(h =>
                        {
                            AppWikiTopicMultiple AppWikiTopicMultiple = new AppWikiTopicMultiple
                            {
                                ApplicationWikiId = applicationWiki.ApplicationWikiId,
                                WikiTopicId = h,
                            };
                            _context.AppWikiTopicMultiple.Add(AppWikiTopicMultiple);
                        });
                        _context.SaveChanges();
                    }
                    var appline = _context.ApplicationWikiLine.Where(prc => prc.ApplicationWikiId == applicationWiki.ApplicationWikiId).ToList();
                    if (appline != null)
                    {
                        if (appline.Count > 0)
                        {
                            appline.ForEach(s =>
                            {
                                var weekly = _context.ApplicationWikiWeekly.Where(w => w.ApplicationWikiLineId == s.ApplicationWikiLineId).ToList();
                                if (weekly != null)
                                {
                                    _context.ApplicationWikiWeekly.RemoveRange(weekly);
                                    _context.SaveChanges();
                                }
                                var ApplicationWikiRecurrence = _context.ApplicationWikiRecurrence.Where(w => w.ApplicationWikiLineId == s.ApplicationWikiLineId).ToList();
                                if (ApplicationWikiRecurrence != null)
                                {
                                    _context.ApplicationWikiRecurrence.RemoveRange(ApplicationWikiRecurrence);
                                    _context.SaveChanges();
                                }
                                var applineduty = _context.ApplicationWikiLineDuty.Where(prc => prc.ApplicationWikiLineId == s.ApplicationWikiLineId).ToList();
                                if (applineduty != null)
                                {
                                    applineduty.ForEach(h =>
                                    {
                                        var wikiresponsible = _context.WikiResponsible.Where(p => p.ApplicationWikiLineDutyId == h.ApplicationWikiLineDutyId).ToList();
                                        if (wikiresponsible != null)
                                        {
                                            _context.WikiResponsible.RemoveRange(wikiresponsible);
                                            _context.SaveChanges();
                                        }
                                    });

                                    _context.ApplicationWikiLineDuty.RemoveRange(applineduty);
                                    _context.SaveChanges();
                                }
                            });
                        }
                        _context.ApplicationWikiLine.RemoveRange(appline);
                        _context.SaveChanges();
                    }
                    verObject.ApplicationWikiLines.ForEach(ec =>
                    {
                        var ApplicationWikiLine = new ApplicationWikiLine
                        {
                            ApplicationWikiId = verObject.ApplicationWikiId,
                            DutyId = ec.DutyId,
                            Responsibility = ec.Responsibility,
                            FunctionLink = ec.FunctionLink,
                            PageLink = ec.PageLink,
                            AddedByUserId = ec.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = ec.StatusCodeID.Value,
                            NotificationAdvice = ec.NotificationAdviceFlag == "Yes" ? true : false,
                            NotificationAdviceTypeId = ec.NotificationAdviceTypeId,
                            RepeatId = ec.RepeatId,
                            CustomId = ec.CustomId,
                            DueDate = ec.DueDate,
                            Monthly = ec.Monthly,
                            Yearly = ec.Yearly,
                            EventDescription = ec.EventDescription,
                            DaysOfWeek = ec.DaysOfWeek,
                            SessionId = ec.SessionId,
                            NotificationStatusId = ec.NotificationStatusId,
                            Title = ec.Title,
                            Message = ec.Message,
                            ScreenId = ec.ScreenId,
                            IsAllowDocAccess = ec.IsAllowDocAccess,
                        };
                        _context.ApplicationWikiLine.Add(ApplicationWikiLine);
                        _context.SaveChanges();
                        if (ec.WeeklyIds != null && ec.WeeklyIds.Count > 0)
                        {
                            ec.WeeklyIds.ForEach(u =>
                            {
                                var applicationWikiWeekly = new ApplicationWikiWeekly()
                                {
                                    ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId,
                                    WeeklyId = u,
                                };
                                _context.ApplicationWikiWeekly.Add(applicationWikiWeekly);
                                _context.SaveChanges();
                            });
                        }
                        if (ec.ApplicationWikiRecurrences != null)
                        {
                            ec.ApplicationWikiRecurrences.ForEach(r =>
                            {
                                var ApplicationWikiRecurrence = new ApplicationWikiRecurrence
                                {
                                    ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId,
                                    TypeId = r.TypeId,
                                    RepeatNos = r.RepeatNos,
                                    OccurenceOptionId = r.OccurenceOptionId,
                                    NoOfOccurences = r.NoOfOccurences,
                                    AddedByUserId = ec.AddedByUserID,
                                    AddedDate = DateTime.Now,
                                    StatusCodeId = r.StatusCodeID.Value,
                                    Sunday = r.Sunday,
                                    Monday = r.Monday,
                                    Tuesday = r.Monday,
                                    Wednesday = r.Wednesday,
                                    Thursday = r.Thursday,
                                    Friday = r.Friday,
                                    Saturyday = r.Saturyday,
                                    StartDate = r.StartDate,
                                    EndDate = r.EndDate,
                                };
                                _context.ApplicationWikiRecurrence.Add(ApplicationWikiRecurrence);
                                _context.SaveChanges();
                            });
                        }
                        var userGroupList = _context.UserGroupUser.ToList();
                        var employeeList = _context.Employee.ToList();
                        if (ec.ApplicationWikiLineDutys != null && ec.ApplicationWikiLineDutys.Count > 0)
                        {
                            ec.ApplicationWikiLineDutys.ForEach(duty =>
                            {
                                var ApplicationWikiLineDuty = new ApplicationWikiLineDuty
                                {
                                    ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId,
                                    DepartmantId = duty.DepartmentId,
                                    DesignationId = duty.DesignationId,
                                    EmployeeId = duty.EmployeeId,
                                    CompanyId = duty.CompanyId,
                                    DesignationNumber = duty.DesignationNumber,
                                    DutyNo = duty.DutyNo,
                                    AddedByUserId = duty.AddedByUserID,
                                    AddedDate = DateTime.Now,
                                    StatusCodeId = duty.StatusCodeID.Value,
                                    Type = duty.Type,
                                    Description = duty.Description,
                                };
                                _context.ApplicationWikiLineDuty.Add(ApplicationWikiLineDuty);
                                _context.SaveChanges();
                                if (duty.EmployeeIDs != null && duty.EmployeeIDs.Count > 0)
                                {
                                    duty.EmployeeIDs.ForEach(s =>
                                    {
                                        var wikiResponsible = new WikiResponsible
                                        {
                                            EmployeeId = s,
                                            ApplicationWikiLineDutyId = ApplicationWikiLineDuty.ApplicationWikiLineDutyId,
                                        };
                                        _context.WikiResponsible.Add(wikiResponsible);
                                        _context.SaveChanges();

                                    });
                                }
                                else if (duty.UserGroupIDs != null && duty.UserGroupIDs.Count > 0)
                                {
                                    duty.UserGroupIDs.ForEach(s =>
                                    {
                                        if (s != null)
                                        {
                                            var userGroupuserIds = userGroupList?.Where(u => u.UserGroupId == s).Select(s => s.UserId).ToList();
                                            if (userGroupuserIds != null && userGroupuserIds.Count > 0)
                                            {
                                                var userGroupEmpIds = employeeList?.Where(e => userGroupuserIds.Contains(e.UserId)).Select(e => e.EmployeeId).ToList();
                                                if (userGroupEmpIds != null && userGroupEmpIds.Count > 0)
                                                {
                                                    userGroupEmpIds.ForEach(emp =>
                                                    {
                                                        var wikiResponsible = new WikiResponsible
                                                        {
                                                            EmployeeId = emp,
                                                            UserGroupId = s,
                                                            ApplicationWikiLineDutyId = duty.ApplicationWikiLineDutyId,
                                                        };
                                                        _context.WikiResponsible.Add(wikiResponsible);
                                                        _context.SaveChanges();
                                                    });
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    });
                    _context.SaveChanges();
                }
                return verObject;
            }
            return new ApplicationWikiModel();
            /* }
             catch (Exception ex)
             {
                 var errorMessage = "Undo Version Update Failed";
                 throw new AppException(errorMessage, ex);
             }*/
        }
        // PUT: api/User/5
        [HttpPut]
        [Route("UpdateApplicationWiki")]
        public async Task<ApplicationWikiModel> Put(ApplicationWikiModel value)
        {
            var applicationWiki = _context.ApplicationWiki.SingleOrDefault(p => p.ApplicationWikiId == value.ApplicationWikiId);
            value.SessionId ??= Guid.NewGuid();
            value.ProfileReferenceNo ??= _generateDocumentNoSeries.GenerateDocumentNo(new DocumentNoSeriesModel { ProfileID = value.ProfileId, AddedByUserID = value.AddedByUserID });

            if (applicationWiki != null)
            {
                if (value.SaveVersionData)
                {
                    var versionInfo = new TableDataVersionInfoModel<ApplicationWikiModel>
                    {
                        JsonData = JsonSerializer.Serialize(value),
                        AddedByUserId = value.ModifiedByUserID.Value,
                        AddedDate = DateTime.Now,
                        SessionId = value.SessionId.Value,
                        ScreenId = value.ScreenID,
                        TableName = "ApplicationWiki",
                        PrimaryKey = value.ApplicationWikiId,
                        ReferenceInfo = value.ReferenceInfo,

                    };
                    await _repository.Insert(versionInfo);
                }
                applicationWiki.WikiDate = value.WikiDate;
                applicationWiki.WikiEntryById = value.WikiEntryById;
                applicationWiki.WikiTypeId = value.WikiTypeId;
                applicationWiki.WikiOwnerId = value.WikiOwnerId;
                applicationWiki.WikiCategoryId = value.WikiCategoryId;
                applicationWiki.WikiTopicId = value.WikiTopicId;
                applicationWiki.Title = value.Title;
                applicationWiki.TranslationRequiredId = value.TranslationRequiredId;
                applicationWiki.Objective = value.Objective;
                applicationWiki.Scope = value.ScopeDesc;
                applicationWiki.PreRequisition = value.PreRequisition;
                applicationWiki.Content = value.Content;
                applicationWiki.ProfileReferenceNo = value.ProfileReferenceNo;
                applicationWiki.VersionNo = value.VersionNo;
                applicationWiki.EstimationPreparationTimeMonth = value.EstimationPreparationTimeMonth;
                applicationWiki.WikiOwnerTypeId = value.WikiOwnerTypeId;
                applicationWiki.PlantId = value.PlantId;
                applicationWiki.EmployeeId = value.EmployeeId;
                applicationWiki.ContentInfo = value.ContentInfo;
                if (!(value.IsNoTranslation.HasValue && value.IsNoTranslation.Value))
                {
                    applicationWiki.IsMalay = value.IsMalay;
                    applicationWiki.IsChinese = value.IsChinese;
                }
                applicationWiki.VersionNo = value.VersionNo;
                applicationWiki.SessionId = value.SessionId;
                applicationWiki.StatusCodeId = value.StatusCodeID.Value;
                applicationWiki.ModifiedByUserId = value.ModifiedByUserID;
                applicationWiki.ModifiedDate = DateTime.Now;
                applicationWiki.DepartmentId = value.DepartmentId;
                applicationWiki.Description = value.Description;
                applicationWiki.Remarks = value.Remarks;
                applicationWiki.EffectiveDate = value.EffectiveDate;
                applicationWiki.NewReviewDate = value.NewReviewDate;
                applicationWiki.ProfileId = value.ProfileId;
                applicationWiki.TaskLink = value.TaskLink;
                applicationWiki.IsContentEntry = value.IsContentEntryFlag == "Yes" ? true : false;
                applicationWiki.ProfilePlantId = value.ProfilePlantId;
                applicationWiki.ProfileDepartmentId = value.ProfileDepartmentId;
                applicationWiki.ProfileDivisionId = value.ProfileDivisionId;
                applicationWiki.ProfileSectionId = value.ProfileSectionId;
                applicationWiki.ProfileSubSectionId = value.ProfileSubSectionId;
                _context.SaveChanges();
                if (value.StatusCodeID == 841)
                {
                    DocumentsController itemClassificationController = new DocumentsController(_context, _mapper, _generateDocumentNoSeries, _hostingEnvironment);
                    var documentsBySession = itemClassificationController.GetDocumentsBySessionID(value.SessionId, (int)value.AddedByUserID, "Wiki");
                    if (documentsBySession != null && documentsBySession.Count > 0)
                    {
                        documentsBySession.ForEach(d =>
                        {
                            var docus = new AppWikiReleaseDoc
                            {
                                DocumentId = d.DocumentID,
                                ApplicationWikiId = value.ApplicationWikiId,
                            };
                            _context.AppWikiReleaseDoc.Add(docus);
                            _context.SaveChanges();
                        });
                    }
                }
                var AppWikiCategoryMultipleRemove = _context.AppWikiCategoryMultiple.Where(w => w.ApplicationWikiId == value.ApplicationWikiId).ToList();
                if (AppWikiCategoryMultipleRemove.Count > 0)
                {
                    _context.AppWikiCategoryMultiple.RemoveRange(AppWikiCategoryMultipleRemove);
                    _context.SaveChanges();
                }
                var AppWikiTopicMultipleRemove = _context.AppWikiTopicMultiple.Where(w => w.ApplicationWikiId == value.ApplicationWikiId).ToList();
                if (AppWikiTopicMultipleRemove.Count > 0)
                {
                    _context.AppWikiTopicMultiple.RemoveRange(AppWikiTopicMultipleRemove);
                    _context.SaveChanges();
                }
                var appWikiTaskLinkRemove = _context.AppWikiTaskLink.Where(w => w.ApplicationWikiId == value.ApplicationWikiId).ToList();
                if (appWikiTaskLinkRemove.Count > 0)
                {
                    _context.AppWikiTaskLink.RemoveRange(appWikiTaskLinkRemove);
                    _context.SaveChanges();
                }
                if (value.WikiCategoryIds != null && value.WikiCategoryIds.Count > 0)
                {
                    value.WikiCategoryIds.ForEach(h =>
                    {
                        AppWikiCategoryMultiple AppWikiCategoryMultiple = new AppWikiCategoryMultiple
                        {
                            ApplicationWikiId = applicationWiki.ApplicationWikiId,
                            WikiCategoryId = h,
                        };
                        _context.AppWikiCategoryMultiple.Add(AppWikiCategoryMultiple);
                    });
                    _context.SaveChanges();
                }
                if (value.WikiTopicIds != null && value.WikiTopicIds.Count > 0)
                {
                    value.WikiTopicIds.ForEach(h =>
                    {
                        AppWikiTopicMultiple appWikiTopicMultiple = new AppWikiTopicMultiple
                        {
                            ApplicationWikiId = applicationWiki.ApplicationWikiId,
                            WikiTopicId = h,
                        };
                        applicationWiki.AppWikiTopicMultiple.Add(appWikiTopicMultiple);
                    });
                    _context.SaveChanges();
                }
                if (value.AppWikiTaskLinkModel != null && value.AppWikiTaskLinkModel.Count > 0)
                {
                    value.AppWikiTaskLinkModel.ForEach(h =>
                    {
                        AppWikiTaskLink appWikiTaskLink = new AppWikiTaskLink
                        {
                            ApplicationWikiId = applicationWiki.ApplicationWikiId,
                            TaskLink = h.TaskLink,
                            Subject = h.Subject,
                        };
                        applicationWiki.AppWikiTaskLink.Add(appWikiTaskLink);
                    });
                }
                _context.SaveChanges();
            }
            return value;

        }
        [HttpPost]
        [Route("InsertDraftApplicationWiki")]
        public async Task<ApplicationWikiModel> InsertDraftApplicationWiki(ApplicationWikiModel value)
        {
            var applicationWikis = _context.ApplicationWiki.Include(a => a.AppWikiTaskLink).Include(a => a.AppWikiCategoryMultiple).Include(b => b.AppWikiTopicMultiple).SingleOrDefault(p => p.ApplicationWikiId == value.ApplicationWikiId);
            if (applicationWikis != null)
            {
                var applicationWiki = new DraftApplicationWiki
                {
                    WikiDate = applicationWikis.WikiDate,
                    WikiEntryById = applicationWikis.WikiEntryById,
                    WikiTypeId = applicationWikis.WikiTypeId,
                    WikiOwnerId = applicationWikis.WikiOwnerId,
                    WikiCategoryId = applicationWikis.WikiCategoryId,
                    WikiTopicId = applicationWikis.WikiTopicId,
                    Title = applicationWikis.Title,
                    TranslationRequiredId = applicationWikis.TranslationRequiredId,
                    Objective = applicationWikis.Objective,
                    Scope = applicationWikis.Scope,
                    PreRequisition = applicationWikis.PreRequisition,
                    Content = applicationWikis.Content,
                    StatusCodeId = 840,
                    AddedByUserId = applicationWikis.AddedByUserId,
                    AddedDate = DateTime.Now,
                    DepartmentId = applicationWikis.DepartmentId,
                    Description = applicationWikis.Description,
                    Remarks = applicationWikis.Remarks,
                    EffectiveDate = applicationWikis.EffectiveDate,
                    NewReviewDate = applicationWikis.NewReviewDate,
                    VersionNo = applicationWikis.VersionNo,
                    SessionId = applicationWikis.SessionId,
                    ProfileReferenceNo = applicationWikis.ProfileReferenceNo,
                    ProfileId = applicationWikis.ProfileId,
                    TaskLink = applicationWikis.TaskLink,
                    IsContentEntry = applicationWikis.IsContentEntry,
                    ReleaseApplicationWikiId = applicationWikis.ApplicationWikiId,
                    IsMalay = applicationWikis.IsMalay,
                    IsChinese = applicationWikis.IsChinese,
                    ProfilePlantId = applicationWikis.ProfilePlantId,
                    ProfileDepartmentId = applicationWikis.ProfileDepartmentId,
                    ProfileDivisionId = applicationWikis.ProfileDivisionId,
                    ProfileSectionId = applicationWikis.ProfileSectionId,
                    ProfileSubSectionId = applicationWikis.ProfileSubSectionId,
                    EstimationPreparationTimeMonth = applicationWikis.EstimationPreparationTimeMonth,
                    WikiOwnerTypeId = applicationWikis.WikiOwnerTypeId,
                    PlantId = applicationWikis.PlantId,
                    EmployeeId = applicationWikis.EmployeeId,
                    ContentInfo = applicationWikis.ContentInfo,
                };
                if (applicationWikis.AppWikiCategoryMultiple != null)
                {
                    applicationWikis.AppWikiCategoryMultiple.ToList().ForEach(h =>
                    {
                        DraftAppWikiCategoryMultiple AppWikiCategoryMultiple = new DraftAppWikiCategoryMultiple
                        {
                            WikiCategoryId = h.WikiCategoryId,
                        };
                        applicationWiki.DraftAppWikiCategoryMultiple.Add(AppWikiCategoryMultiple);
                    });
                }
                if (applicationWikis.AppWikiTopicMultiple != null)
                {
                    applicationWikis.AppWikiTopicMultiple.ToList().ForEach(h =>
                    {
                        DraftAppWikiTopicMultiple appWikiTopicMultiple = new DraftAppWikiTopicMultiple
                        {
                            WikiTopicId = h.WikiTopicId,
                        };
                        applicationWiki.DraftAppWikiTopicMultiple.Add(appWikiTopicMultiple);
                    });
                }
                if (applicationWikis.AppWikiTaskLink != null)
                {
                    applicationWikis.AppWikiTaskLink.ToList().ForEach(h =>
                    {
                        DraftAppWikiTaskLink taskLink = new DraftAppWikiTaskLink
                        {
                            TaskLink = h.TaskLink,
                            Subject = h.Subject,
                        };
                        applicationWiki.DraftAppWikiTaskLink.Add(taskLink);
                    });
                }
                _context.DraftApplicationWiki.Add(applicationWiki);
                var ApplicationWikiLines = _context.ApplicationWikiLine.Include(a => a.ApplicationWikiRecurrence).
                    Include(b => b.ApplicationWikiWeekly).
                    Include(c => c.ApplicationWikiLineDuty).
                    Include(d => d.ApplicationWikiLineNotify).
                    Where(w => w.ApplicationWikiId == applicationWikis.ApplicationWikiId).ToList();
                if (ApplicationWikiLines != null)
                {
                    ApplicationWikiLines.ForEach(ec =>
                    {
                        var ApplicationWikiLine = new DraftApplicationWikiLine
                        {
                            DutyId = ec.DutyId,
                            Responsibility = ec.Responsibility,
                            FunctionLink = ec.FunctionLink,
                            PageLink = ec.PageLink,
                            AddedByUserId = ec.AddedByUserId,
                            AddedDate = DateTime.Now,
                            StatusCodeId = ec.StatusCodeId.Value,
                            NotificationAdviceTypeId = ec.NotificationAdviceTypeId,
                            RepeatId = ec.RepeatId,
                            CustomId = ec.CustomId,
                            DueDate = ec.DueDate,
                            Monthly = ec.Monthly,
                            Yearly = ec.Yearly,
                            EventDescription = ec.EventDescription,
                            DaysOfWeek = ec.DaysOfWeek,
                            SessionId = ec.SessionId,
                            NotificationStatusId = ec.NotificationStatusId,
                            Title = ec.Title,
                            Message = ec.Message,
                            ScreenId = ec.ScreenId,
                            IsAllowDocAccess = ec.IsAllowDocAccess,

                        };
                        applicationWiki.DraftApplicationWikiLine.Add(ApplicationWikiLine);
                        if (ec.ApplicationWikiRecurrence != null)
                        {
                            ec.ApplicationWikiRecurrence.ToList().ForEach(r =>
                            {
                                var ApplicationWikiRecurrence = new DraftApplicationWikiRecurrence
                                {
                                    TypeId = r.TypeId,
                                    RepeatNos = r.RepeatNos,
                                    OccurenceOptionId = r.OccurenceOptionId,
                                    NoOfOccurences = r.NoOfOccurences,
                                    AddedByUserId = r.AddedByUserId,
                                    AddedDate = DateTime.Now,
                                    StatusCodeId = r.StatusCodeId.Value,
                                    Sunday = r.Sunday,
                                    Monday = r.Monday,
                                    Tuesday = r.Monday,
                                    Wednesday = r.Wednesday,
                                    Thursday = r.Thursday,
                                    Friday = r.Friday,
                                    Saturyday = r.Saturyday,
                                    StartDate = r.StartDate,
                                    EndDate = r.EndDate,
                                };
                                ApplicationWikiLine.DraftApplicationWikiRecurrence.Add(ApplicationWikiRecurrence);
                            });
                        }
                        if (ec.ApplicationWikiWeekly != null)
                        {
                            ec.ApplicationWikiWeekly.ToList().ForEach(u =>
                            {
                                var applicationWikiWeekly = new DraftApplicationWikiWeekly()
                                {
                                    WeeklyId = u.WeeklyId,
                                };
                                ApplicationWikiLine.DraftApplicationWikiWeekly.Add(applicationWikiWeekly);

                            });
                        }
                        if (ec.ApplicationWikiLineNotify != null)
                        {
                            ec.ApplicationWikiLineNotify.ToList().ForEach(u =>
                            {
                                var applicationWikiLineNotify = new DraftApplicationWikiLineNotify()
                                {
                                    NotifyUserId = u.NotifyUserId,
                                };
                                ApplicationWikiLine.DraftApplicationWikiLineNotify.Add(applicationWikiLineNotify);
                            });
                        }
                        if (ec.ApplicationWikiLineDuty != null)
                        {
                            ec.ApplicationWikiLineDuty.ToList().ForEach(duty =>
                            {
                                var ApplicationWikiLineDuty = new DraftApplicationWikiLineDuty
                                {
                                    ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId,
                                    DepartmantId = duty.DepartmantId,
                                    DesignationId = duty.DesignationId,
                                    EmployeeId = duty.EmployeeId,
                                    CompanyId = duty.CompanyId,
                                    DesignationNumber = duty.DesignationNumber,
                                    DutyNo = duty.DutyNo,
                                    AddedByUserId = duty.AddedByUserId,
                                    AddedDate = DateTime.Now,
                                    StatusCodeId = duty.StatusCodeId,
                                    Type = duty.Type,
                                    Description = duty.Description,
                                };
                                ApplicationWikiLine.DraftApplicationWikiLineDuty.Add(ApplicationWikiLineDuty);
                                var WikiResponsible = _context.WikiResponsible.Where(w => w.ApplicationWikiLineDutyId == duty.ApplicationWikiLineDutyId).ToList();
                                if (WikiResponsible != null && duty.WikiResponsible.Count > 0)
                                {
                                    WikiResponsible.ForEach(s =>
                                    {
                                        var wikiResponsible = new DraftWikiResponsible
                                        {
                                            EmployeeId = s.EmployeeId,
                                            UserId = s.UserId,
                                            UserGroupId = s.UserGroupId,
                                        };
                                        ApplicationWikiLineDuty.DraftWikiResponsible.Add(wikiResponsible);
                                    });
                                }
                            });
                        }
                    });
                }
                _context.SaveChanges();
                DocumentsController itemClassificationController = new DocumentsController(_context, _mapper, _generateDocumentNoSeries, _hostingEnvironment);
                var documentsBySession = itemClassificationController.GetDocumentsBySessionID(value.SessionId, (int)value.AddedByUserID, "Wiki");
                if (documentsBySession != null && documentsBySession.Count > 0)
                {
                    documentsBySession.ForEach(d =>
                    {
                        var docus = new AppWikiDraftDoc
                        {
                            DocumentId = d.DocumentID,
                            ApplicationWikiId = applicationWiki.ApplicationWikiId,
                        };
                        _context.AppWikiDraftDoc.Add(docus);
                        _context.SaveChanges();
                    });
                }
            }
            return value;
        }
        [HttpPut]
        [Route("ApplyVersion")]
        public ApplicationWikiModel PutVersion(ApplicationWikiModel value)
        {
            var applicationWiki = _context.ApplicationWiki.SingleOrDefault(p => p.ApplicationWikiId == value.ApplicationWikiId);
            value.SessionId ??= Guid.NewGuid();
            if (applicationWiki != null)
            {
                applicationWiki.WikiDate = value.WikiDate;
                applicationWiki.WikiEntryById = value.WikiEntryById;
                applicationWiki.WikiTypeId = value.WikiTypeId;
                applicationWiki.WikiOwnerId = value.WikiOwnerId;
                applicationWiki.WikiCategoryId = value.WikiCategoryId;
                applicationWiki.WikiTopicId = value.WikiTopicId;
                applicationWiki.Title = value.Title;
                applicationWiki.TranslationRequiredId = value.TranslationRequiredId;
                applicationWiki.Objective = value.Objective;
                applicationWiki.Scope = value.ScopeDesc;
                applicationWiki.PreRequisition = value.PreRequisition;
                applicationWiki.Content = value.Content;
                applicationWiki.ContentInfo = value.ContentInfo;
                if (string.IsNullOrEmpty(value.VersionNo))
                {
                    applicationWiki.VersionNo = "0";
                }
                value.VersionNo = (Convert.ToDouble(value.VersionNo) + 1).ToString();
                if (!(value.IsNoTranslation.HasValue && value.IsNoTranslation.Value))
                {
                    applicationWiki.IsMalay = value.IsMalay;
                    applicationWiki.IsChinese = value.IsChinese;
                }
                applicationWiki.VersionNo = value.VersionNo;
                applicationWiki.StatusCodeId = value.StatusCodeID.Value;
                applicationWiki.ModifiedByUserId = value.ModifiedByUserID;
                applicationWiki.ModifiedDate = DateTime.Now;
                applicationWiki.DepartmentId = value.DepartmentId;
                applicationWiki.Description = value.Description;
                applicationWiki.Remarks = value.Remarks;
                applicationWiki.EffectiveDate = value.EffectiveDate;
                applicationWiki.NewReviewDate = value.NewReviewDate;
                _context.SaveChanges();
            }

            var appline = _context.ApplicationWikiLine.Where(prc => prc.ApplicationWikiId == applicationWiki.ApplicationWikiId).ToList();
            if (appline != null)
            {
                if (appline.Count > 0)
                {
                    appline.ForEach(s =>
                    {
                        var weekly = _context.ApplicationWikiWeekly.Where(w => w.ApplicationWikiLineId == s.ApplicationWikiLineId).ToList();
                        if (weekly != null)
                        {
                            _context.ApplicationWikiWeekly.RemoveRange(weekly);
                            _context.SaveChanges();
                        }
                        var ApplicationWikiRecurrence = _context.ApplicationWikiRecurrence.Where(w => w.ApplicationWikiLineId == s.ApplicationWikiLineId).ToList();
                        if (ApplicationWikiRecurrence != null)
                        {
                            _context.ApplicationWikiRecurrence.RemoveRange(ApplicationWikiRecurrence);
                            _context.SaveChanges();
                        }
                        var applineduty = _context.ApplicationWikiLineDuty.Where(prc => prc.ApplicationWikiLineId == s.ApplicationWikiLineId).ToList();
                        if (applineduty != null)
                        {
                            applineduty.ForEach(h =>
                            {
                                var wikiresponsible = _context.WikiResponsible.Where(p => p.ApplicationWikiLineDutyId == h.ApplicationWikiLineDutyId).ToList();
                                if (wikiresponsible != null)
                                {
                                    _context.WikiResponsible.RemoveRange(wikiresponsible);
                                    _context.SaveChanges();
                                }
                            });

                            _context.ApplicationWikiLineDuty.RemoveRange(applineduty);
                            _context.SaveChanges();
                        }
                    });
                }
                _context.ApplicationWikiLine.RemoveRange(appline);
                _context.SaveChanges();
            }

            value.ApplicationWikiLines.ForEach(ec =>
            {
                var ApplicationWikiLine = new ApplicationWikiLine
                {
                    ApplicationWikiId = value.ApplicationWikiId,
                    DutyId = ec.DutyId,
                    Responsibility = ec.Responsibility,
                    FunctionLink = ec.FunctionLink,
                    PageLink = ec.PageLink,
                    AddedByUserId = ec.AddedByUserID,
                    AddedDate = DateTime.Now,
                    StatusCodeId = ec.StatusCodeID.Value,
                    NotificationAdvice = ec.NotificationAdviceFlag == "Yes" ? true : false,
                    NotificationAdviceTypeId = ec.NotificationAdviceTypeId,
                    RepeatId = ec.RepeatId,
                    CustomId = ec.CustomId,
                    DueDate = ec.DueDate,
                    Monthly = ec.Monthly,
                    Yearly = ec.Yearly,
                    EventDescription = ec.EventDescription,
                    DaysOfWeek = ec.DaysOfWeek,
                    SessionId = ec.SessionId,
                    NotificationStatusId = ec.NotificationStatusId,
                    Title = ec.Title,
                    Message = ec.Message,
                    IsAllowDocAccess = ec.IsAllowDocAccess,
                    ScreenId = ec.ScreenId,
                };
                _context.ApplicationWikiLine.Add(ApplicationWikiLine);
                _context.SaveChanges();
                if (ec.ApplicationWikiRecurrences != null)
                {
                    ec.ApplicationWikiRecurrences.ForEach(r =>
                    {
                        var ApplicationWikiRecurrence = new ApplicationWikiRecurrence
                        {
                            ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId,
                            TypeId = r.TypeId,
                            RepeatNos = r.RepeatNos,
                            OccurenceOptionId = r.OccurenceOptionId,
                            NoOfOccurences = r.NoOfOccurences,
                            AddedByUserId = r.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = r.StatusCodeID.Value,
                            Sunday = r.Sunday,
                            Monday = r.Monday,
                            Tuesday = r.Monday,
                            Wednesday = r.Wednesday,
                            Thursday = r.Thursday,
                            Friday = r.Friday,
                            Saturyday = r.Saturyday,
                            StartDate = r.StartDate,
                            EndDate = r.EndDate,
                        };
                        _context.ApplicationWikiRecurrence.Add(ApplicationWikiRecurrence);
                        _context.SaveChanges();
                    });
                }
                if (ec.WeeklyIds != null && ec.WeeklyIds.Count > 0)
                {
                    ec.WeeklyIds.ForEach(u =>
                    {
                        var applicationWikiWeekly = new ApplicationWikiWeekly()
                        {
                            ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId,
                            WeeklyId = u,
                        };
                        _context.ApplicationWikiWeekly.Add(applicationWikiWeekly);
                        _context.SaveChanges();
                    });
                }
                var userGroupList = _context.UserGroupUser.ToList();
                var employeeList = _context.Employee.ToList();
                if (ec.ApplicationWikiLineDutys != null && ec.ApplicationWikiLineDutys.Count > 0)
                {
                    ec.ApplicationWikiLineDutys.ForEach(duty =>
                    {
                        var ApplicationWikiLineDuty = new ApplicationWikiLineDuty
                        {
                            ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId,
                            DepartmantId = duty.DepartmentId,
                            DesignationId = duty.DesignationId,
                            EmployeeId = duty.EmployeeId,
                            CompanyId = duty.CompanyId,
                            DesignationNumber = duty.DesignationNumber,
                            DutyNo = duty.DutyNo,
                            AddedByUserId = duty.AddedByUserID,
                            AddedDate = DateTime.Now,
                            StatusCodeId = duty.StatusCodeID.Value,
                            Type = duty.Type,
                            Description = duty.Description,
                        };
                        _context.ApplicationWikiLineDuty.Add(ApplicationWikiLineDuty);
                        _context.SaveChanges();
                        if (duty.EmployeeIDs != null && duty.EmployeeIDs.Count > 0)
                        {
                            duty.EmployeeIDs.ForEach(s =>
                            {
                                //var userid = employeeList.Where(e => e.EmployeeId == s).Select(u => u.UserId).FirstOrDefault();
                                var wikiResponsible = new WikiResponsible
                                {
                                    EmployeeId = s,
                                    //UserGroupId = userid != null && userGroupList != null ? userGroupList.Where(w => w.UserId == userid).Select(i => i.UserGroupId).FirstOrDefault() : null,
                                    ApplicationWikiLineDutyId = ApplicationWikiLineDuty.ApplicationWikiLineDutyId,
                                };
                                _context.WikiResponsible.Add(wikiResponsible);
                                _context.SaveChanges();

                            });
                        }
                        else if (duty.UserGroupIDs != null && duty.UserGroupIDs.Count > 0)
                        {
                            duty.UserGroupIDs.ForEach(s =>
                            {
                                if (s != null)
                                {
                                    var userGroupuserIds = userGroupList?.Where(u => u.UserGroupId == s).Select(s => s.UserId).ToList();
                                    if (userGroupuserIds != null && userGroupuserIds.Count > 0)
                                    {
                                        var userGroupEmpIds = employeeList?.Where(e => userGroupuserIds.Contains(e.UserId)).Select(e => e.EmployeeId).ToList();
                                        if (userGroupEmpIds != null && userGroupEmpIds.Count > 0)
                                        {
                                            userGroupEmpIds.ForEach(emp =>
                                            {
                                                var wikiResponsible = new WikiResponsible
                                                {
                                                    EmployeeId = emp,
                                                    UserGroupId = s,
                                                    ApplicationWikiLineDutyId = duty.ApplicationWikiLineDutyId,
                                                };
                                                _context.WikiResponsible.Add(wikiResponsible);
                                                _context.SaveChanges();
                                            });
                                        }
                                    }
                                }
                            });
                        }
                    });
                }
            });
            _context.SaveChanges();
            return value;
        }
        [HttpDelete]
        [Route("DeleteVersion")]
        public async Task<long> DeleteVersion(int Id)
        {

            var tableData = await _context.TableDataVersionInfo.SingleOrDefaultAsync(p => p.VersionTableId == Id);

            if (tableData != null)
            {
                _context.TableDataVersionInfo.Remove(tableData);
                _context.SaveChanges();

            }
            return -1;
        }
        [HttpPut]
        [Route("UpdateApplicationWikiLine")]
        public ApplicationWikiLineModel UpdateApplicationWikiLine(ApplicationWikiLineModel value)
        {
            value.SessionId ??= Guid.NewGuid();
            value.DueDate ??= DateTime.Now;
            var ApplicationWikiLine = _context.ApplicationWikiLine.SingleOrDefault(p => p.ApplicationWikiLineId == value.ApplicationWikiLineId);
            ApplicationWikiLine.ApplicationWikiId = value.ApplicationWikiId;
            ApplicationWikiLine.DutyId = value.DutyId;
            ApplicationWikiLine.Responsibility = value.Responsibility;
            ApplicationWikiLine.FunctionLink = value.FunctionLink;
            ApplicationWikiLine.PageLink = value.PageLink;
            ApplicationWikiLine.StatusCodeId = value.StatusCodeID.Value;
            ApplicationWikiLine.ModifiedByUserId = value.ModifiedByUserID;
            ApplicationWikiLine.ModifiedDate = DateTime.Now;
            ApplicationWikiLine.NotificationAdvice = value.NotificationAdviceFlag == "Yes" ? true : false;
            ApplicationWikiLine.NotificationAdviceTypeId = value.NotificationAdviceTypeId;
            ApplicationWikiLine.RepeatId = value.RepeatId;
            ApplicationWikiLine.CustomId = value.CustomId;
            ApplicationWikiLine.DueDate = value.DueDate;
            ApplicationWikiLine.Monthly = value.Monthly;
            ApplicationWikiLine.Yearly = value.Yearly;
            ApplicationWikiLine.EventDescription = value.EventDescription;
            ApplicationWikiLine.DaysOfWeek = value.DaysOfWeek;
            ApplicationWikiLine.SessionId = value.SessionId;
            ApplicationWikiLine.NotificationStatusId = value.NotificationStatusId;
            ApplicationWikiLine.Title = value.Title;
            ApplicationWikiLine.Message = value.Message;
            ApplicationWikiLine.ScreenId = value.ScreenId;
            ApplicationWikiLine.NotifyEndDate = value.NotifyEndDate;
            ApplicationWikiLine.IsAllowDocAccess = value.IsAllowDocAccess;
            _context.SaveChanges();
            var ApplicationWikiWeeklyRemove = _context.ApplicationWikiWeekly.Where(w => w.ApplicationWikiLineId == value.ApplicationWikiLineId).ToList();
            if (ApplicationWikiWeeklyRemove != null)
            {
                _context.ApplicationWikiWeekly.RemoveRange(ApplicationWikiWeeklyRemove);
                _context.SaveChanges();
            }
            if (value.WeeklyIds != null && value.WeeklyIds.Count > 0)
            {
                value.WeeklyIds.ForEach(u =>
                {
                    var applicationWikiWeekly = new ApplicationWikiWeekly()
                    {
                        ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId,
                        WeeklyId = u,
                        CustomType = "Weekly"
                    };
                    _context.ApplicationWikiWeekly.Add(applicationWikiWeekly);
                    _context.SaveChanges();
                });
            }
            if (value.DaysOfWeek == true && value.DaysOfWeekIds != null && value.DaysOfWeekIds.Count > 0)
            {
                value.DaysOfWeekIds.ForEach(u =>
                {
                    var applicationWikiWeekly = new ApplicationWikiWeekly()
                    {
                        ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId,
                        WeeklyId = u,
                        CustomType = "Yearly"
                    };
                    _context.ApplicationWikiWeekly.Add(applicationWikiWeekly);
                    _context.SaveChanges();
                });
            }
            var notifyUserRemove = _context.ApplicationWikiLineNotify.Where(w => w.ApplicationWikiLineId == value.ApplicationWikiLineId).ToList();
            if (notifyUserRemove != null)
            {
                _context.ApplicationWikiLineNotify.RemoveRange(notifyUserRemove);
                _context.SaveChanges();
            }
            if (value.NotifyIds != null && value.NotifyIds.Count > 0)
            {
                value.NotifyIds.ForEach(u =>
                {
                    var applicationWikiLineNotify = new ApplicationWikiLineNotify()
                    {
                        ApplicationWikiLineId = ApplicationWikiLine.ApplicationWikiLineId,
                        NotifyUserId = u,
                    };
                    _context.ApplicationWikiLineNotify.Add(applicationWikiLineNotify);
                    _context.SaveChanges();
                });
            }
            value.NotificationAdvice = ApplicationWikiLine.NotificationAdvice;
            NotificationHandeler(value);
            return value;

        }
        [HttpPut]
        [Route("UpdateApplicationWikiLineDuty")]
        public ApplicationWikiLineDutyModel UpdateApplicationWikiLineDuty(ApplicationWikiLineDutyModel value)
        {
            var ApplicationWikiLineDuty = _context.ApplicationWikiLineDuty.SingleOrDefault(p => p.ApplicationWikiLineDutyId == value.ApplicationWikiLineDutyId);
            ApplicationWikiLineDuty.ApplicationWikiLineId = value.ApplicationWikiLineId;
            ApplicationWikiLineDuty.DepartmantId = value.DepartmentId;
            ApplicationWikiLineDuty.DesignationId = value.DesignationId;
            ApplicationWikiLineDuty.CompanyId = value.CompanyId;
            ApplicationWikiLineDuty.EmployeeId = value.EmployeeId;
            ApplicationWikiLineDuty.DesignationNumber = value.DesignationNumber;
            ApplicationWikiLineDuty.DutyNo = value.DutyNo;
            ApplicationWikiLineDuty.StatusCodeId = value.StatusCodeID.Value;
            ApplicationWikiLineDuty.ModifiedByUserId = value.ModifiedByUserID;
            ApplicationWikiLineDuty.ModifiedDate = DateTime.Now;
            ApplicationWikiLineDuty.Type = value.Type;
            ApplicationWikiLineDuty.Description = value.Description;
            var userGroupList = _context.UserGroupUser.ToList();
            var employeeList = _context.Employee.ToList();
            if (value.EmployeeIDs != null && value.EmployeeIDs.Count > 0)
            {
                value.EmployeeIDs.ForEach(s =>
                {

                    //var userid = employeeList.Where(e => e.EmployeeId == s).Select(u => u.UserId).FirstOrDefault();
                    var existing = _context.WikiResponsible.Where(e => e.EmployeeId == s && e.ApplicationWikiLineDutyId == value.ApplicationWikiLineDutyId).FirstOrDefault();
                    if (existing == null)
                    {
                        var wikiResponsible = new WikiResponsible
                        {
                            EmployeeId = s,
                            //UserGroupId = userid != null && userGroupList != null ? userGroupList.Where(w => w.UserId == userid).Select(i => i.UserGroupId).FirstOrDefault() : null,
                            ApplicationWikiLineDutyId = value.ApplicationWikiLineDutyId,
                        };
                        _context.WikiResponsible.Add(wikiResponsible);
                        _context.SaveChanges();
                    }
                });
            }
            else if (value.UserGroupIDs != null && value.UserGroupIDs.Count > 0)
            {
                value.UserGroupIDs.ForEach(s =>
                {
                    if (s != null)
                    {
                        var userGroupuserIds = userGroupList?.Where(u => u.UserGroupId == s).Select(s => s.UserId).ToList();
                        if (userGroupuserIds != null && userGroupuserIds.Count > 0)
                        {
                            var userGroupEmpIds = employeeList?.Where(e => userGroupuserIds.Contains(e.UserId)).Select(e => e.EmployeeId).ToList();
                            if (userGroupEmpIds != null && userGroupEmpIds.Count > 0)
                            {
                                userGroupEmpIds.ForEach(emp =>
                                {
                                    var existing = _context.WikiResponsible.Where(e => e.EmployeeId == emp && e.ApplicationWikiLineDutyId == value.ApplicationWikiLineDutyId).FirstOrDefault();
                                    if (existing == null)
                                    {
                                        var wikiResponsible = new WikiResponsible
                                        {
                                            EmployeeId = emp,
                                            UserGroupId = s,
                                            ApplicationWikiLineDutyId = value.ApplicationWikiLineDutyId,
                                        };
                                        _context.WikiResponsible.Add(wikiResponsible);
                                        _context.SaveChanges();
                                    }
                                });
                            }
                        }
                    }
                });
            }
            _context.SaveChanges();

            return value;

        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeleteApplicationWiki")]
        public void Delete(int id)
        {
            var applicationWiki = _context.ApplicationWiki.SingleOrDefault(p => p.ApplicationWikiId == id);
            if (applicationWiki != null)
            {
                var AppWikiCategoryMultipleRemove = _context.AppWikiCategoryMultiple.Where(w => w.ApplicationWikiId == applicationWiki.ApplicationWikiId).ToList();
                if (AppWikiCategoryMultipleRemove.Count > 0)
                {
                    _context.AppWikiCategoryMultiple.RemoveRange(AppWikiCategoryMultipleRemove);
                    _context.SaveChanges();
                }
                var appWikiReleaseDoc = _context.AppWikiReleaseDoc.Where(w => w.ApplicationWikiId == applicationWiki.ApplicationWikiId).ToList();
                if (appWikiReleaseDoc.Count > 0)
                {
                    _context.AppWikiReleaseDoc.RemoveRange(appWikiReleaseDoc);
                    _context.SaveChanges();
                }
                var appWikiTaskLinkRemove = _context.AppWikiTaskLink.Where(w => w.ApplicationWikiId == applicationWiki.ApplicationWikiId).ToList();
                if (appWikiTaskLinkRemove.Count > 0)
                {
                    _context.AppWikiTaskLink.RemoveRange(appWikiTaskLinkRemove);
                    _context.SaveChanges();
                }
                var AppWikiTopicMultipleRemove = _context.AppWikiTopicMultiple.Where(w => w.ApplicationWikiId == applicationWiki.ApplicationWikiId).ToList();
                if (AppWikiTopicMultipleRemove.Count > 0)
                {
                    _context.AppWikiTopicMultiple.RemoveRange(AppWikiTopicMultipleRemove);
                    _context.SaveChanges();
                }
                var ApplicationWikiLine = _context.ApplicationWikiLine.Where(p => p.ApplicationWikiId == id).ToList();
                if (ApplicationWikiLine != null)
                {
                    ApplicationWikiLine.ForEach(c =>
                    {
                        var ApplicationWikiRecurrence = _context.ApplicationWikiRecurrence.Where(p => p.ApplicationWikiLineId == c.ApplicationWikiLineId).ToList();
                        if (ApplicationWikiRecurrence != null)
                        {
                            _context.ApplicationWikiRecurrence.RemoveRange(ApplicationWikiRecurrence);
                            _context.SaveChanges();
                        }
                        var ApplicationWikiLineDuty = _context.ApplicationWikiLineDuty.Where(p => p.ApplicationWikiLineId == c.ApplicationWikiLineId).ToList();
                        if (ApplicationWikiLineDuty != null)
                        {
                            ApplicationWikiLineDuty.ForEach(d =>
                            {
                                var wikiresponsible = _context.WikiResponsible.Where(p => p.ApplicationWikiLineDutyId == d.ApplicationWikiLineDutyId).ToList();
                                if (wikiresponsible != null)
                                {
                                    _context.WikiResponsible.RemoveRange(wikiresponsible);
                                    _context.SaveChanges();
                                }
                            });
                            _context.ApplicationWikiLineDuty.RemoveRange(ApplicationWikiLineDuty);
                            _context.SaveChanges();
                        }
                        var ApplicationWikiWeekly = _context.ApplicationWikiWeekly.Where(p => p.ApplicationWikiLineId == c.ApplicationWikiLineId).ToList();
                        if (ApplicationWikiWeekly != null)
                        {
                            _context.ApplicationWikiWeekly.RemoveRange(ApplicationWikiWeekly);
                            _context.SaveChanges();
                        }
                        var notificationHandler = _context.NotificationHandler.Where(w => w.SessionId == c.SessionId).ToList();
                        if (notificationHandler != null)
                        {
                            _context.NotificationHandler.RemoveRange(notificationHandler);
                            _context.SaveChanges();
                        }
                        var ApplicationWikiLineNotify = _context.ApplicationWikiLineNotify.Where(p => p.ApplicationWikiLineId == c.ApplicationWikiLineId).ToList();
                        if (ApplicationWikiLineNotify != null)
                        {
                            _context.ApplicationWikiLineNotify.RemoveRange(ApplicationWikiLineNotify);
                            _context.SaveChanges();
                        }
                    });
                    _context.ApplicationWikiLine.RemoveRange(ApplicationWikiLine);
                    _context.SaveChanges();
                }
            }
            _context.ApplicationWiki.Remove(applicationWiki);
            _context.SaveChanges();
        }
        [HttpDelete]
        [Route("DeleteApplicationWikiLine")]
        public void DeleteApplicationWikiLine(int id)
        {
            var ApplicationWikiLine = _context.ApplicationWikiLine.SingleOrDefault(p => p.ApplicationWikiLineId == id);
            var ApplicationWikiLineDuty = _context.ApplicationWikiLineDuty.Where(p => p.ApplicationWikiLineId == id).ToList();
            var ApplicationWikiRecurrence = _context.ApplicationWikiRecurrence.SingleOrDefault(p => p.ApplicationWikiLineId == id);
            if (ApplicationWikiRecurrence != null)
            {
                _context.ApplicationWikiRecurrence.Remove(ApplicationWikiRecurrence);
                _context.SaveChanges();
            }
            var notificationHandler = _context.NotificationHandler.Where(w => w.SessionId == ApplicationWikiLine.SessionId).ToList();
            if (notificationHandler != null)
            {
                _context.NotificationHandler.RemoveRange(notificationHandler);
                _context.SaveChanges();
            }
            var ApplicationWikiWeekly = _context.ApplicationWikiWeekly.Where(p => p.ApplicationWikiLineId == id).ToList();
            if (ApplicationWikiWeekly != null)
            {
                _context.ApplicationWikiWeekly.RemoveRange(ApplicationWikiWeekly);
                _context.SaveChanges();
            }
            if (ApplicationWikiLineDuty != null)
            {
                ApplicationWikiLineDuty.ForEach(h =>
                {
                    var wikiresponsible = _context.WikiResponsible.Where(p => p.ApplicationWikiLineDutyId == h.ApplicationWikiLineDutyId).ToList();
                    if (wikiresponsible != null)
                    {
                        _context.WikiResponsible.RemoveRange(wikiresponsible);
                        _context.SaveChanges();
                    }
                });
                _context.ApplicationWikiLineDuty.RemoveRange(ApplicationWikiLineDuty);
                _context.SaveChanges();
            }
            _context.ApplicationWikiLine.Remove(ApplicationWikiLine);
            _context.SaveChanges();
        }
        [HttpDelete]
        [Route("DeleteApplicationWikiLineDuty")]
        public void DeleteApplicationWikiLineDuty(int id)
        {
            var ApplicationWikiLineDuty = _context.ApplicationWikiLineDuty.SingleOrDefault(p => p.ApplicationWikiLineDutyId == id);
            var wikiresponsible = _context.WikiResponsible.Where(p => p.ApplicationWikiLineDutyId == id).ToList();
            if (wikiresponsible != null)
            {
                _context.WikiResponsible.RemoveRange(wikiresponsible);
                _context.SaveChanges();
            }
            _context.ApplicationWikiLineDuty.Remove(ApplicationWikiLineDuty);
            _context.SaveChanges();
        }

        [HttpPost]
        [Route("InsertWikiResponsible")]
        public WikiResponsibilityModel InsertWikiResponsible(WikiResponsibilityModel value)
        {
            var userGroupList = _context.UserGroupUser.ToList();
            var employeeList = _context.Employee.ToList();
            if (value.EmployeeIDs != null && value.EmployeeIDs.Count > 0)
            {
                value.EmployeeIDs.ForEach(s =>
                {

                    var existing = _context.WikiResponsible.Where(e => e.EmployeeId == s && e.ApplicationWikiLineDutyId == value.ApplicationWikiLineDutyID).FirstOrDefault();
                    if (existing == null)
                    {
                        var wikiResponsible = new WikiResponsible
                        {
                            EmployeeId = s,
                            UserGroupId = value.UserGroupID,
                            UserId = value.UserID,
                            ApplicationWikiLineDutyId = value.ApplicationWikiLineDutyID,
                        };
                        _context.WikiResponsible.Add(wikiResponsible);
                        _context.SaveChanges();
                    }
                });
            }
            else if (value.UserGroupIDs != null && value.UserGroupIDs.Count > 0)
            {
                value.UserGroupIDs.ForEach(s =>
                {
                    if (s != null)
                    {
                        var userGroupuserIds = userGroupList?.Where(u => u.UserGroupId == s).Select(s => s.UserId).ToList();
                        if (userGroupuserIds != null && userGroupuserIds.Count > 0)
                        {
                            var userGroupEmpIds = employeeList?.Where(e => userGroupuserIds.Contains(e.UserId)).Select(e => e.EmployeeId).ToList();
                            if (userGroupEmpIds != null && userGroupEmpIds.Count > 0)
                            {
                                userGroupEmpIds.ForEach(emp =>
                                {
                                    var existing = _context.WikiResponsible.Where(e => e.EmployeeId == emp && e.ApplicationWikiLineDutyId == value.ApplicationWikiLineDutyID).FirstOrDefault();
                                    if (existing == null)
                                    {
                                        var wikiResponsible = new WikiResponsible
                                        {
                                            EmployeeId = emp,
                                            UserGroupId = s,
                                            UserId = value.UserID,
                                            ApplicationWikiLineDutyId = value.ApplicationWikiLineDutyID,
                                        };
                                        _context.WikiResponsible.Add(wikiResponsible);
                                        _context.SaveChanges();
                                    }
                                });
                            }
                        }
                    }
                });
            }
            return value;
        }
        [HttpDelete]
        [Route("DeleteWikiResponsible")]
        public void DeleteWikiResponsible(int id)
        {
            var wikiresponsible = _context.WikiResponsible.SingleOrDefault(p => p.WikiResponsibilityId == id);
            if (wikiresponsible != null)
            {
                _context.WikiResponsible.Remove(wikiresponsible);
                _context.SaveChanges();
            }
        }
        [HttpPut]
        [Route("DeleteWikiResponsibles")]
        public WikiResponsibilityModel DeleteWikiResponsibles(WikiResponsibilityModel value)
        {

            var selectDocumentRoleIds = value.WikiResponsibilityIDs;
            if (value.WikiResponsibilityIDs.Count > 0)
            {
                value.WikiResponsibilityIDs.ForEach(sel =>
                {
                    var wikiresponsible = _context.WikiResponsible.Where(p => p.WikiResponsibilityId == sel).FirstOrDefault();
                    if (wikiresponsible != null)
                    {


                        _context.WikiResponsible.Remove(wikiresponsible);
                        _context.SaveChanges();

                    }
                });

                _context.SaveChanges();
            }


            return value;
        }
        [HttpGet]
        [Route("UpdateAppWikiReleaseDoc")]
        public void UpdateAppWikiReleaseDoc(long? id)
        {
            var appWikiReleaseDoc = _context.AppWikiReleaseDoc.Where(w => w.ApplicationWikiId == id).ToList();
            if (appWikiReleaseDoc != null && appWikiReleaseDoc.Count > 0)
            {
                var docs = _context.Documents.Select(d => new { d.DocumentId, d.DocumentParentId, d.IsLatest }).ToList();
                var docIds = appWikiReleaseDoc.Select(s => s.DocumentId).ToList();
                appWikiReleaseDoc.ForEach(s =>
                {
                    var docCount = docs.Where(w => w.DocumentId == s.DocumentId && w.IsLatest == true).Count();
                    if (docCount > 0)
                    {

                    }
                    else
                    {
                        var isLatest = docs.Where(w => w.DocumentParentId == s.DocumentId && w.IsLatest == true).FirstOrDefault();
                        if (isLatest != null)
                        {
                            var query = string.Format("Update AppWikiReleaseDoc Set DocumentId='{1}' Where AppWikiReleaseDocId= {0}", s.AppWikiReleaseDocId, isLatest.DocumentId);
                            _context.Database.ExecuteSqlRaw(query);
                        }
                    }

                });
            }
        }

        [HttpGet]
        [Route("UpdateAppWikiReleaseDocAll")]
        public void UpdateAppWikiReleaseDocAll()
        {
            var appWikiReleaseDoc = _context.AppWikiReleaseDoc.Select(s => s.ApplicationWikiId).Distinct().ToList();
            var applicationwiki = _context.ApplicationWiki.Select(s => new { s.SessionId, s.ApplicationWikiId, s.AddedByUserId }).Where(w => !appWikiReleaseDoc.Contains(w.ApplicationWikiId)).ToList();
            if (applicationwiki != null && applicationwiki.Count > 0)
            {
                DocumentsController itemClassificationController = new DocumentsController(_context, _mapper, _generateDocumentNoSeries, _hostingEnvironment);
                applicationwiki.ForEach(a =>
                {
                    var documentsBySession = itemClassificationController.GetDocumentsBySessionID(a.SessionId, (int)a.AddedByUserId.Value, "Wiki").Select(s => s.DocumentID).ToList();
                    if (documentsBySession != null && documentsBySession.Count > 0)
                    {
                        documentsBySession.ForEach(d =>
                        {
                            var docus = new AppWikiReleaseDoc
                            {
                                DocumentId = d,
                                ApplicationWikiId = a.ApplicationWikiId,
                            };
                            _context.AppWikiReleaseDoc.Add(docus);
                            _context.SaveChanges();
                        });
                    }
                });
            }
        }
    }
}