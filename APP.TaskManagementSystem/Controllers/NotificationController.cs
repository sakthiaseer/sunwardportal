﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class NotificationController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;

        public NotificationController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        [HttpGet]
        [Route("GetNotifications")]
        public List<NotificationModel> GetNotifications(long id)
        {
            var HolidayMaster = _context.Notification.Include("Module").Include("NotifiedUser").Where(w=>w.NotifiedUserId == id && w.IsRead == false).Select(s => new NotificationModel
            {
                Notification = s.Notification1,
                IsRead = s.IsRead,
                ModuleId = s.ModuleId,
                NotificationId = s.NotificationId,
                NotificationType = s.Module!=null? s.Module.CodeValue : string.Empty,
                NotifiedUserId = s.NotifiedUserId,
                UpdatedDate = s.UpdatedDate,
                NotifiedUser = s.NotifiedUser!=null? s.NotifiedUser.UserName : string.Empty,
                NotificationTime = s.UpdatedDate.Value.ToString("dd-MMM-yyyy")
            })

                .OrderByDescending(o => o.NotificationId).AsNoTracking().ToList();
            return HolidayMaster;
        }
        [HttpPut]
        [Route("UpdateNotification")]
        public NotificationModel Put(NotificationModel value)
        {
            var taskNotes = _context.Notification.SingleOrDefault(p => p.NotificationId == value.NotificationId);
            taskNotes.IsRead = true;
            _context.SaveChanges();
            return value;
        }

    }
}