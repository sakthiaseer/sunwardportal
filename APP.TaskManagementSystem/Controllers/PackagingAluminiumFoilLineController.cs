﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace APP.TaskManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class PackagingAluminiumFoilLineController : ControllerBase
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        public PackagingAluminiumFoilLineController(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        // GET: api/Project
        [HttpGet]
        [Route("GetPackagingAluminiumFoilLinesById")]
        public List<PackageAluminiumFoilLineModel> Get(int id)
        {
            List<PackageAluminiumFoilLineModel> packageAluminiumFoilLineModels = new List<PackageAluminiumFoilLineModel>();
            var packagingAluminiumFoilLine = _context.PackagingAluminiumFoilLine.Include("AddedByUser").Include("ModifiedByUser").Where(t => t.PackagingAluminiumFoilId == id).OrderByDescending(o => o.PackagingAluminiumFoilLineId).AsNoTracking().ToList();
            if (packagingAluminiumFoilLine != null && packagingAluminiumFoilLine.Count > 0)
            {
                packagingAluminiumFoilLine.ForEach(s =>
                {
                    PackageAluminiumFoilLineModel packageAluminiumFoilLineModel = new PackageAluminiumFoilLineModel
                    {
                        BlisterAndFoilGap = s.BlisterAndFoilGap,
                        BlisterGap = s.BlisterGap,
                        BlisterLength = s.BlisterLength,
                        BlisterWidth = s.BlisterWidth,
                        RepeatedLength = s.RepeatedLength,
                        PackagingAluminiumFoilId = s.PackagingAluminiumFoilId,
                        PackagingAluminiumFoilLineId = s.PackagingAluminiumFoilLineId,
                        StatusCode = s.StatusCode != null ? s.StatusCode.CodeValue : "",
                        AddedByUser = s.AddedByUser != null ? s.AddedByUser.UserName : "",
                        ModifiedByUser = s.ModifiedByUser != null ? s.ModifiedByUser.UserName : "",
                        StatusCodeID = s.StatusCodeId,
                        AddedByUserID = s.AddedByUserId,
                        ModifiedByUserID = s.ModifiedByUserId,                        
                        AddedDate = s.AddedDate,
                        ModifiedDate = s.ModifiedDate

                    };
                    packageAluminiumFoilLineModels.Add(packageAluminiumFoilLineModel);

                });
            }
            return packageAluminiumFoilLineModels;
        }

        // GET: api/Project/2
        //[HttpGet("{id}", Name = "Get Country")]
        [HttpGet("GetPackagingAluminiumFoilLine/{id:int}")]
        public ActionResult<PackageAluminiumFoilLineModel> Get(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var packagingAluminiumFoilLine = _context.PackagingAluminiumFoilLine.SingleOrDefault(p => p.PackagingAluminiumFoilLineId == id.Value);
            var result = _mapper.Map<PackageAluminiumFoilLineModel>(packagingAluminiumFoilLine);
            if (result == null)
            {
                return NotFound();
            }
            return result;
        }
        [HttpPost()]
        [Route("GetData")]
        public ActionResult<PackageAluminiumFoilLineModel> GetData(SearchModel searchModel)
        {
            var packagingAluminiumFoilLine = new PackagingAluminiumFoilLine();

            if (searchModel.Id == 0)
            {
                switch (searchModel.Action)
                {
                    case "First":
                        packagingAluminiumFoilLine = _context.PackagingAluminiumFoilLine.OrderByDescending(o => o.PackagingAluminiumFoilLineId).FirstOrDefault();
                        break;
                    case "Last":
                        packagingAluminiumFoilLine = _context.PackagingAluminiumFoilLine.OrderByDescending(o => o.PackagingAluminiumFoilLineId).LastOrDefault();
                        break;
                    case "Next":
                        packagingAluminiumFoilLine = _context.PackagingAluminiumFoilLine.OrderByDescending(o => o.PackagingAluminiumFoilLineId).LastOrDefault();
                        break;
                    case "Previous":
                        packagingAluminiumFoilLine = _context.PackagingAluminiumFoilLine.OrderByDescending(o => o.PackagingAluminiumFoilLineId).FirstOrDefault();
                        break;
                }
            }
            else
            {
                switch (searchModel.Action)
                {
                    case "First":
                        packagingAluminiumFoilLine = _context.PackagingAluminiumFoilLine.OrderByDescending(o => o.PackagingAluminiumFoilLineId).FirstOrDefault();
                        break;
                    case "Last":
                        packagingAluminiumFoilLine = _context.PackagingAluminiumFoilLine.OrderByDescending(o => o.PackagingAluminiumFoilLineId).LastOrDefault();
                        break;
                    case "Next":
                        packagingAluminiumFoilLine = _context.PackagingAluminiumFoilLine.OrderBy(o => o.PackagingAluminiumFoilLineId).FirstOrDefault(s => s.PackagingAluminiumFoilLineId > searchModel.Id);
                        break;
                    case "Previous":
                        packagingAluminiumFoilLine = _context.PackagingAluminiumFoilLine.OrderByDescending(o => o.PackagingAluminiumFoilLineId).FirstOrDefault(s => s.PackagingAluminiumFoilLineId < searchModel.Id);
                        break;
                }
            }
            var result = _mapper.Map<PackageAluminiumFoilLineModel>(packagingAluminiumFoilLine);
            return result;
        }
        // POST: api/User
        [HttpPost]
        [Route("InsertPackagingAluminiumFoilLine")]
        public PackageAluminiumFoilLineModel Post(PackageAluminiumFoilLineModel value)
        {
            var packagingAluminiumFoilLine = new PackagingAluminiumFoilLine
            {
                RepeatedLength = value.RepeatedLength,
                BlisterAndFoilGap = value.BlisterAndFoilGap,
                BlisterGap = value.BlisterGap,
                BlisterLength = value.BlisterLength,
                BlisterWidth = value.BlisterWidth,
                PackagingAluminiumFoilId = value.PackagingAluminiumFoilId,
                AddedByUserId = value.AddedByUserID,
                AddedDate = DateTime.Now,
                StatusCodeId = value.StatusCodeID.Value,

            };
            _context.PackagingAluminiumFoilLine.Add(packagingAluminiumFoilLine);
            _context.SaveChanges();
            value.PackagingAluminiumFoilLineId = packagingAluminiumFoilLine.PackagingAluminiumFoilLineId;
            return value;
        }

        // PUT: api/User/5
        [HttpPut]
        [Route("UpdatePackagingAluminiumFoilLine")]
        public PackageAluminiumFoilLineModel Put(PackageAluminiumFoilLineModel value)
        {
            var packagingAluminiumFoilLine = _context.PackagingAluminiumFoilLine.SingleOrDefault(p => p.PackagingAluminiumFoilLineId == value.PackagingAluminiumFoilLineId);
            packagingAluminiumFoilLine.BlisterAndFoilGap = value.BlisterAndFoilGap;
            packagingAluminiumFoilLine.BlisterGap = value.BlisterGap;
            packagingAluminiumFoilLine.RepeatedLength = value.RepeatedLength;
            packagingAluminiumFoilLine.BlisterLength = value.BlisterLength;
            packagingAluminiumFoilLine.BlisterWidth = value.BlisterWidth;
            packagingAluminiumFoilLine.PackagingAluminiumFoilId = value.PackagingAluminiumFoilId;
            packagingAluminiumFoilLine.ModifiedByUserId = value.ModifiedByUserID;
            packagingAluminiumFoilLine.ModifiedDate = DateTime.Now;
            packagingAluminiumFoilLine.StatusCodeId = value.StatusCodeID.Value;
            _context.SaveChanges();

            return value;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("DeletePackagingAluminiumFoilLine")]
        public void Delete(int id)
        {
            var packagingAluminiumFoilLine = _context.PackagingAluminiumFoilLine.SingleOrDefault(p => p.PackagingAluminiumFoilLineId == id);
            if (packagingAluminiumFoilLine != null)
            {
                _context.PackagingAluminiumFoilLine.Remove(packagingAluminiumFoilLine);
                _context.SaveChanges();
            }
        }
    }
}