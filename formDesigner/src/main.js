import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
import moment from "moment";
import 'font-awesome/css/font-awesome.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css' // Ensure you are using css-loader
import VueHotkey from 'v-hotkey'
Vue.use(VueHotkey)
import VueClipboard from 'vue-clipboard2';
Vue.use(VueClipboard);

Vue.use(moment);

Vue.use(vuetify);

Vue.filter('formatDate', function (value) {
  if (value) {
    return moment(value).format('DD-MMM-YYYY');
  }
});
Vue.filter('formatDateTime', function (value) {
  if (value) {
    return moment(value).format('DD-MMM-YYYY hh:mm A');
  }
});
import GlobalComponents from './globalComponents';
Vue.use(GlobalComponents);

Vue.prototype.$eventHub = new Vue(); // Global event bus
import store from './store';
Vue.config.productionTip = false

new Vue({
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
