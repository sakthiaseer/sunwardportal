import myApi from '@/util/api';

export function getMasterForms() {
    return myApi.getAll("MasterForm/GetMasterForms")
}

export function createMasterForm(data) {
    return myApi.create(data, "MasterForm/InsertMasterForm")
}

export function updateMasterForm(data) {

    return myApi.update(data.masterFormId, data, "MasterForm/UpdateMasterForm")
}
export function deleteMasterForm(data) {

    return myApi.delete(data.masterFormId, 'MasterForm/DeleteMasterForm')
}

export function getMasterFormBySessionId(sessionID) {
    return myApi.getBySessionID(sessionID, "MasterForm/GetMasterFormBySessionId")
}
export function getMasterFormLineItem(sessionID, userId) {
    return myApi.getBySession(sessionID, userId, "MasterForm/GetMasterFormLineItem")
}
export function getMasterFormLines(id) {
    return myApi.getByID(id, "MasterForm/GetMasterFormLines")
}
export function createMasterFormLine(data) {
    return myApi.create(data, "MasterForm/InsertMasterFormLine")
}

export function updateMasterFormLine(data) {
    return myApi.update(data.masterFormId, data, "MasterForm/UpdateMasterFormLine")
}
export function deleteMasterFormLine(data) {
    return myApi.delete(data.masterFormDetailId, 'MasterForm/DeleteMasterFormLine')
}