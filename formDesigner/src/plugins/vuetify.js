
import Vue from 'vue';
import Vuetify, {
    VRow,
    VCol,
    VTextField,
    VTooltip,
    VCheckbox,
    VSelect,
    VTextarea,
    VRadio,
    VDatePicker,
    VRadioGroup
} from 'vuetify/lib';
import { Ripple, Intersect, Touch, Resize } from 'vuetify/lib/directives';

Vue.use(Vuetify, {
    components: { VRow, VTooltip, VCol, VTextField, VCheckbox, VSelect, VTextarea, VRadio, VDatePicker, VRadioGroup },
    directives: { Ripple, Intersect, Touch, Resize },
});

export default new Vuetify({});
