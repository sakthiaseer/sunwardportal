import {
    AUTH_REQUEST,
    AUTH_ERROR,
    AUTH_SUCCESS,
    AUTH_LOGOUT,
    AUTH_ASSIGN,
} from '../actions/auth';
import {
    authenticate
} from '@/api/login';
import {
    setToken,
    removeToken,
    getToken,
    setuserName,
    setuserID,
    setTheme,
    getuserID,
    getuserName,
    setPage,
    getPage,
    getTheme,
} from '@/util/auth';

const state = {
    token: getToken() || '',
    status: '',
    hasLoadedOnce: false
};

const getters = {
    isAuthenticated: state => !!state.token,
    authStatus: state => state.status,
};

const actions = {
    [AUTH_REQUEST]: ({
        commit,
        dispatch
    }, user) => {
        return new Promise((resolve, reject) => {
            commit(AUTH_REQUEST);
            authenticate(user)
                .then(resp => {
                    console.log(resp.data);
                    resp.data.formId = user.formId;
                    console.log('Response', resp);
                    if (resp.data.userId != null) {
                        setToken(resp.data.token);
                        setuserName(resp.data.userName);
                        setuserID(resp.data.userId);
                        setTheme(resp.data.userTheme);
                        setPage(resp.data.pageSize);
                        commit(AUTH_SUCCESS, resp);
                        //dispatch(USER_REQUEST)
                        resolve(resp);
                    } else {
                        commit(AUTH_ERROR, 'Incorrect username or password ');
                        removeToken();
                        reject(err);
                    }
                })
                .catch(err => {
                    commit(AUTH_ERROR, err);
                    removeToken();
                    reject(err);
                });
        });
    },
    [AUTH_LOGOUT]: ({
        commit,
        dispatch
    }) => {
        return new Promise((resolve, reject) => {
            commit(AUTH_LOGOUT);
            removeToken();
            resolve();
        });
    },

    [AUTH_ASSIGN]: ({
        commit,
        dispatch
    }) => {
        return new Promise((resolve, reject) => {
            commit(AUTH_ASSIGN);
            resolve();
        });
    }

};

const mutations = {
    [AUTH_REQUEST]: (state) => {
        state.status = 'loading';
    },
    [AUTH_SUCCESS]: (state, resp) => {
        state.status = 'success';
        state.token = resp.data.token;
        state.userId = resp.data.userId;
        state.formId = resp.data.formId;
        state.userName = resp.data.userName;
        state.pageSize = resp.data.pageSize;
        state.userTheme = resp.data.userTheme;

        state.hasLoadedOnce = true;
    },
    [AUTH_ERROR]: (state) => {
        state.status = 'error';
        state.hasLoadedOnce = true;
    },
    [AUTH_LOGOUT]: (state) => {
        state.token = '';
        state.status = '';
        state.userId = '';
        state.userName = '';

        setToken('');
        setuserName(null);
        setuserID(-1);
    },
    [AUTH_ASSIGN]: (state) => {
        state.status = 'success';
        state.token = getToken();
        state.userId = getuserID();
        state.userName = getuserName();
        state.pageSize = getPage();
        state.userTheme = getTheme();
        state.hasLoadedOnce = true;
        state.authMenu = [];
        state.classificationPermission = [];
    },
};

export default {
    state,
    getters,
    actions,
    mutations,
};