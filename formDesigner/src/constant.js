export default Object.freeze({
    SAVE: "Record Saved Successfully",
    UPDATE: "Record Updated Successfully",
    DELETE: "Record Deleted Successfully",
    VALIDATE: "validation error occured.Please check the required field values!.",
    DATEFORMAT: "DD-MMM-YYYY",
    YEARMONTHDAYFORMAT: "YYYY-MMM-DD",
    MONTHYEARFORMAT: "MMM-YYYY",
    DATETIMEFORMAT: "DD-MMM-YYYY hh:mm A",
    VERSION: "Version Saved Successfully",

    // BASEURL: "https://portal.sunwardpharma.com:2025/#/tasktreeview?",
    // DOWNURL: "https://portal.sunwardpharma.com:2025/#/downloadFile?",
    // APPUpload: "https://portal.sunwardpharma.com:2025/AppUpload/",
    // GLOSSARYURL: "https://portal.sunwardpharma.com:2025/#/Glossaryview?",
    // NotificationHub: "https://portal.sunwardpharma.com:2025/tmsapiDev/chatHub",
    // FOLDERURL: "https://portal.sunwardpharma.com:2025/#/publicfoldertreeview?", 
    // ATTACH_DOWNURL:"https://portal.sunwardpharma.com:2025/#/AttachmentDownload?",

    ITEMCLASSIFICATIONPROFILEERROR: "Profile Not Set In Application Form",



    NotificationHub: "https://portal.sunwardpharma.com/tmsapi/chatHub",
    BASEURL: "https://portal.sunwardpharma.com/#/tasktreeview?",
    FOLDERURL: "https://portal.sunwardpharma.com/#/publicfoldertreeview?",
    DOWNURL: "https://portal.sunwardpharma.com/#/downloadFile?",
    APPUpload: "https://portal.sunwardpharma.com/AppUpload/",
    GLOSSARYURL: "https://portal.sunwardpharma.com/#/Glossaryview?",
    ATTACH_DOWNURL: "https://portal.sunwardpharma.com/#/AttachmentDownload?",
    SURVEYURL: "https://portal.sunwardpharma.com/#/surveyform?",

    SuperUser: "47",
});
