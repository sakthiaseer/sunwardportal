﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using XF.Material.Forms.UI;

namespace APP.SWAM.Mobile.Behaviors
{
    public sealed class TextboxFocusedEvent
    {
        public static readonly BindableProperty FocusedProperty =
           BindableProperty.CreateAttached(
               "FocusCommand",
               typeof(ICommand),
               typeof(TextboxFocusedEvent),
               default(ICommand),
               BindingMode.OneWay,
               null,
               PropertyChanged);

        private static void PropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var textfield = bindable as MaterialTextField;

            if (textfield != null)
            {
                textfield.PropertyChanged -= OnFocusChanged;
                textfield.PropertyChanged += OnFocusChanged;
            }
        }
        private static void OnFocusChanged(object sender, PropertyChangedEventArgs e)
        {
            var list = sender as MaterialTextField;

            if (list != null && list.IsEnabled )
            { 
                var command = GetFocusedCommand(list);
                if (command != null && command.CanExecute(e.PropertyName))
                {
                    command.Execute(e.PropertyName);
                }
            }
        }

        public static ICommand GetFocusedCommand(BindableObject bindableObject)
        {
            return (ICommand)bindableObject.GetValue(FocusedProperty);
        }

        public static void SetFocusedCommand(BindableObject bindableObject, object value)
        {
            bindableObject.SetValue(FocusedProperty, value);
        }
    }
}
