﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.DataGrid;


namespace APP.SWAM.Mobile.Behaviors
{
    public sealed class ItemSelectedCommandDataGrid
    {
        public static readonly BindableProperty ItemSelectedCommandProperty =
            BindableProperty.CreateAttached(
                "ItemSelectedCommand",
                typeof(ICommand),
                typeof(ItemSelectedCommandDataGrid),
                default(ICommand),
                BindingMode.OneWay,
                null,
                PropertyChanged);

        private static void PropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var dataGrid = bindable as DataGrid;

            if (dataGrid != null)
            {
                dataGrid.ItemSelected -= DataGridOnItemSelected;
                dataGrid.ItemSelected += DataGridOnItemSelected;
            }
        }

        private static void DataGridOnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var dataGrid = sender as DataGrid;

            if (dataGrid != null && dataGrid.IsEnabled && !dataGrid.IsRefreshing)
            {
                var command = GetItemSelectedCommand(dataGrid);
                if (command != null && e.SelectedItem != null && command.CanExecute(e.SelectedItem))
                {
                    command.Execute(e.SelectedItem);
                }
            }
        }

        public static ICommand GetItemSelectedCommand(BindableObject bindableObject)
        {
            return (ICommand)bindableObject.GetValue(ItemSelectedCommandProperty);
        }

        public static void SetItemSelectedCommand(BindableObject bindableObject, object value)
        {
            bindableObject.SetValue(ItemSelectedCommandProperty, value);
        }
    }
}
