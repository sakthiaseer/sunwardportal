﻿using Plugin.InputKit.Shared.Controls;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.Behaviors
{

    public sealed class ItemSelectionChangedDropDown
    {
        public static readonly BindableProperty ItemSelectionChangedProperty =
            BindableProperty.CreateAttached(
                "ItemSelectionChangedCommand",
                typeof(ICommand),
                typeof(ItemSelectionChangedDropDown),
                default(ICommand),
                BindingMode.OneWay,
                null,
                PropertyChanged);

        private static void PropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var dropdown = bindable as Dropdown;

            if (dropdown != null)
            {
                dropdown.SelectedItemChanged -= OnDropDownSelectionChanged;
                dropdown.SelectedItemChanged += OnDropDownSelectionChanged;
            }
        }

        private static void OnDropDownSelectionChanged(object sender, Plugin.InputKit.Shared.Utils.SelectedItemChangedArgs e)
        {
            var dropdown = sender as Dropdown;

            if (dropdown != null)
            {
                var command = GetItemSelectionChanged(dropdown);
                if (command != null && command.CanExecute(e.NewItem))
                {
                    command.Execute(e.NewItem);
                }
            }
        }


        public static ICommand GetItemSelectionChanged(BindableObject bindableObject)
        {
            return (ICommand)bindableObject.GetValue(ItemSelectionChangedProperty);
        }

        public static void SetItemSelectionChanged(BindableObject bindableObject, object value)
        {
            bindableObject.SetValue(ItemSelectionChangedProperty, value);
        }
    }
}
