﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class APPTransferReClassModel : BaseViewModel
    {
        private long _transferReclassID;
        public long TransferReclassID { get { return _transferReclassID; } set { SetProperty(ref _transferReclassID, value); } }

        private string _loginUser;
        public string LoginUser { get { return _loginUser; } set { SetProperty(ref _loginUser, value); } }
        private DateTime _startDate;
        public DateTime StartDate { get { return _startDate; } set { SetProperty(ref _startDate, value); } }

        private string _fromLocation;
        public string FromLocation { get { return _fromLocation; } set { SetProperty(ref _fromLocation, value); ValidateForm(); } }

        private string _transferOrderNo;
        public string TransferOrderNo { get { return _transferOrderNo; } set { SetProperty(ref _transferOrderNo, value); ValidateTransferOrder(); } }

        private string _receiveLocation;
        public string ReceiveLocation { get { return _receiveLocation; } set { SetProperty(ref _receiveLocation, value); ValidateReceiveLocation(); } }

        private string _toLocation;
        public string ToLocation { get { return _toLocation; } set { SetProperty(ref _toLocation, value); ValidateTo();  } }

        private bool _isfromLocation;
        public bool IsFromLocationNoError { get { return _isfromLocation; } set { SetProperty(ref _isfromLocation, value); } }

        private bool _isToLocation;
        public bool IsToLocationError { get { return _isToLocation; } set { SetProperty(ref _isToLocation, value); IsValid(); } }

        private bool _isTransferOrderError;
        public bool IsTransferOrderError { get { return _isTransferOrderError; } set { SetProperty(ref _isTransferOrderError, value); } }

        private bool _isReceiveLocationError;
        public bool IsReceiveLocationError { get { return _isReceiveLocationError; } set { SetProperty(ref _isReceiveLocationError, value); } }

        private void ValidateForm()
        {
            IsFromLocationNoError = string.IsNullOrEmpty(FromLocation);
        }
        private void ValidateTo()
        {
            IsToLocationError = string.IsNullOrEmpty(ToLocation);
        }
        private void ValidateTransferOrder()
        {
            IsTransferOrderError = string.IsNullOrEmpty(TransferOrderNo);
        }

        private void ValidateReceiveLocation()
        {
            IsReceiveLocationError = string.IsNullOrEmpty(ReceiveLocation);
        }

        public bool IsValidFrom()
        {
            return !IsFromLocationNoError;
        }
        public bool IsValidTo()
        {
            return !IsToLocationError;
        }
        public bool IsValidTransferOrder()
        {
            return !IsTransferOrderError;
        }
        public bool IsValidReceiveLocation()
        {
            return !IsReceiveLocationError;
        }
        public bool IsValid()
        {
            return !IsFromLocationNoError && !IsToLocationError;
        }

    }
}
