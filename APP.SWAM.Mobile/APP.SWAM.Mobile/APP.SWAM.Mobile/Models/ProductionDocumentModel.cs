﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class ProductionDocumentModel : BaseViewModel
    {
        private long _productionDocumentId;
        public long ProductionDocumentId { get { return _productionDocumentId; } set { SetProperty(ref _productionDocumentId, value); } }
        private long? _fileProfileTypeId;
        public long? FileProfileTypeId { get { return _fileProfileTypeId; } set { SetProperty(ref _fileProfileTypeId, value); } }
        private string _documentName;
        public string DocumentName { get { return _documentName; } set { SetProperty(ref _documentName, value); } }
        private int? _statusCodeID;
        public int? StatusCodeID { get { return _statusCodeID; } set { SetProperty(ref _statusCodeID, value); } }
        private string _statusCode;
        public string StatusCode { get { return _statusCode; } set { SetProperty(ref _statusCode, value); } }
        private Guid? _sessionId;
        public Guid? SessionId { get { return _sessionId; } set { SetProperty(ref _sessionId, value); } }
    }
}
