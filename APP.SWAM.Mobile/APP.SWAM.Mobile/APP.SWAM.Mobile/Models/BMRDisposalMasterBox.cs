﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class BMRDisposalMasterBox
    {
        public long BmrdisposalMasterBoxId { get; set; }
        public long? CompanyId { get; set; }
        public long? TypeOfDisposalBoxId { get; set; }
        public string BoxNo { get; set; }
        public DateTime? DisposalDate { get; set; }
        public long? EstimateNumberOfBmr { get; set; }
        public long? LocationId { get; set; }
        public string TypeOfDisposalBoxName { get; set; }
        public string CompanyDatabaseName { get; set; }
        public string LocationName { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public long? ProfileID { get; set; }
        public string Area { get; set; }
        public string SpecificAreaName { get; set; }
    }
}
