﻿using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class ApplicationPermissionModel : ViewModelBase
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long PermissionID { get; set; }
        public string PermissionName { get; set; }
        public string PermissionCode { get; set; }
        public long? ParentID { get; set; }
        public byte? PermissionLevel { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string Icon { get; set; }
        public string MenuId { get; set; }
        public string PermissionURL { get; set; }
        public string PermissionGroup { get; set; }
        public string PermissionOrder { get; set; }
        public bool IsDisplay { get; set; }
        public long IsSelected { get; set; }
        public long? AppplicationPermissionID { get; set; }
    }
}
