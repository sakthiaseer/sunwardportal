﻿using APP.SWAM.Mobile.ViewModels;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.Models
{
    public class ProductionEntryModel : BaseViewModel
    {
        private long _productionEntryID;
        public long ProductionEntryID { get { return _productionEntryID; } set { SetProperty(ref _productionEntryID, value); } }

        private string _locationName;
        public string LocationName { get { return _locationName; } set { SetProperty(ref _locationName, value); ValidateLogin(); } }

        private string _productionOrderNo;
        public string ProductionOrderNo { get { return _productionOrderNo; } set { SetProperty(ref _productionOrderNo, value); ValidateLogin(); } }

        private string _itemName;
        public string ItemName { get { return _itemName; } set { SetProperty(ref _itemName, value); } }

        private string _rePlanRefNo;
        public string RePlanRefNo { get { return _rePlanRefNo; } set { SetProperty(ref _rePlanRefNo, value); } }

        private string _processNo;
        public string ProcessNo { get { return _processNo; } set { SetProperty(ref _processNo, value); } }

        private string _roomStatus;
        public string RoomStatus { get { return _roomStatus; } set { SetProperty(ref _roomStatus, value); } }

        private string _roomStatus1;
        public string RoomStatus1 { get { return _roomStatus1; } set { SetProperty(ref _roomStatus1, value); } }

        private string _actionName;
        public string ActionName { get { return _actionName; } set { SetProperty(ref _actionName, value); ValidateLogin(); } }

        private List<string> _actionNames;
        public List<string> ActionNames { get { return _actionNames; } set { SetProperty(ref _actionNames, value); } }

        private long? _productionActionID;
        public long? ProductionActionID { get { return _productionActionID; } set { SetProperty(ref _productionActionID, value); } }
        private List<long> _prodTaskIds = new List<long>();
        public List<long> ProdTaskIds { get { return _prodTaskIds; } set { SetProperty(ref _prodTaskIds, value); ValidateLogin(); } }

        private string _prodTask;
        public string ProdTask { get { return _prodTask; } set { SetProperty(ref _prodTask, value);} }
        private string _numberOfWorker;
        public string NumberOfWorker { get { return _numberOfWorker; } set { SetProperty(ref _numberOfWorker, value); ValidateLogin(); } }

        private string _loginUser;
        public string LoginUser { get { return _loginUser; } set { SetProperty(ref _loginUser, value); } }
        private DateTime _startDate;
        public DateTime StartDate { get { return _startDate; } set { SetProperty(ref _startDate, value); } }

        private string _productionLineNo;
        public string ProductionLineNo { get { return _productionLineNo; } set { SetProperty(ref _productionLineNo, value); } }

        private byte[] _frontPhoto;
        public byte[] FrontPhoto { get { return _frontPhoto; } set { SetProperty(ref _frontPhoto, value); } }

        private byte[] _backPhoto;
        public byte[] BackPhoto { get { return _backPhoto; } set { SetProperty(ref _backPhoto, value); } }

        #region Validation 
        private bool _islocationName;
        public bool IsLocationNameError { get { return _islocationName; } set { SetProperty(ref _islocationName, value); } }

        private bool _isproductionOrderNo;
        public bool IsProductionOrderNoError { get { return _isproductionOrderNo; } set { SetProperty(ref _isproductionOrderNo, value); } }

        private bool _isnumberOfWorker;
        public bool IsNumberOfWorkerError { get { return _isnumberOfWorker; } set { SetProperty(ref _isnumberOfWorker, value); } }

        private bool _IsactionName;
        public bool IsActionNameError { get { return _IsactionName; } set { SetProperty(ref _IsactionName, value); } }

      

        private string _batchNo;
        public string BatchNumber { get { return _batchNo; } set { SetProperty(ref _batchNo, value); } }

        private Guid? _sessionId;
        public Guid? SessionId { get { return _sessionId; } set { SetProperty(ref _sessionId, value); } }

        private void ValidateLogin()
        {
            IsLocationNameError = string.IsNullOrEmpty(LocationName);
            IsProductionOrderNoError = string.IsNullOrEmpty(ProductionOrderNo);
            IsNumberOfWorkerError = string.IsNullOrEmpty(NumberOfWorker);
            IsActionNameError = string.IsNullOrEmpty(ActionName);
        }

        public bool IsValid()
        {
            if (IsActionNameError || IsLocationNameError || IsProductionOrderNoError || IsNumberOfWorkerError)
                return false;
            return true;
        }
        #endregion
    }
}
