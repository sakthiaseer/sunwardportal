﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class APPTransferReClassLineModel : BaseViewModel
    {
        private long _apptransferRelcassLineID;
        public long ApptransferRelcassLineID { get { return _apptransferRelcassLineID; } set { SetProperty(ref _apptransferRelcassLineID, value); } }

        private long _transferReclassID;
        public long TransferRelcassId { get { return _transferReclassID; } set { SetProperty(ref _transferReclassID, value); } }



        private string _fromLocation;
        public string FromLocation { get { return _fromLocation; } set { SetProperty(ref _fromLocation, value); } }

        private string _toLocation;
        public string ToLocation { get { return _toLocation; } set { SetProperty(ref _toLocation, value); } }

        private string _productionOrderNo;
        public string ProdOrderNo { get { return _productionOrderNo; } set { SetProperty(ref _productionOrderNo, value); } }

        private string _replanRefNo;
        public string ReplanRefNo { get { return _replanRefNo; } set { SetProperty(ref _replanRefNo, value); } }
        private string _description;
        public string Description { get { return _description; } set { SetProperty(ref _description, value); } }
        private string _sublotNo;
        public string LotNo { get { return _sublotNo; } set { SetProperty(ref _sublotNo, value); ValidateDrum(); } }

        private string _drumNo;
        public string DrumNo { get { return _drumNo; } set { SetProperty(ref _drumNo, value); ValidateDrum(); } }

        private string _qcRefNo;
        public string QCRefNo { get { return _qcRefNo; } set { SetProperty(ref _qcRefNo, value); } }

        private string _itemNo;
        public string ItemNo { get { return _itemNo; } set { SetProperty(ref _itemNo, value); } }

        private string _batchNo;
        public string BatchNo { get { return _batchNo; } set { SetProperty(ref _batchNo, value); } }

        private string _UOM;
        public string UOM { get { return _UOM; } set { SetProperty(ref _UOM, value); } }

        private string _quantity;
        public string Quantity { get { return _quantity; } set { SetProperty(ref _quantity, value); ValidateDrum(); } }

        private string _prodLineNo;
        public string ProdLineNo { get { return _prodLineNo; } set { SetProperty(ref _prodLineNo, value); } }

        private string _receiveQuantity ;
        public string ReceiveQuantity
        {
            get
            {
                return _receiveQuantity;
            }
            set
            {
                SetProperty(ref _receiveQuantity, value);
                ValidateReceive();
            }
        }

        private bool _isdrumNo;
        public bool IsDrumNoError { get { return _isdrumNo; } set { SetProperty(ref _isdrumNo, value); } }

        private bool _postedtoNAV;
        public bool PostedtoNAV { get { return _postedtoNAV; } set { SetProperty(ref _postedtoNAV, value); } }

        private bool _isquantity;
        public bool IsQuantity { get { return _isquantity; } set { SetProperty(ref _isquantity, value); } }

        private bool _isLotNo;
        public bool IsLotNoError { get { return _isLotNo; } set { SetProperty(ref _isLotNo, value); } }

        private bool _isReceivequantity;
        public bool IsReceiveQuantity { get { return _isReceivequantity; } set { SetProperty(ref _isReceivequantity, value); } }

        private void ValidateDrum()
        {
            IsDrumNoError = string.IsNullOrEmpty(DrumNo);
            IsQuantity = string.IsNullOrEmpty(Quantity);
            IsLotNoError = string.IsNullOrEmpty(LotNo);
        }
        private void ValidateReceive()
        {
            IsReceiveQuantity = string.IsNullOrEmpty(ReceiveQuantity);
        }

        public bool IsValidReceive()
        {
            return !IsReceiveQuantity;
        }


        public bool IsValid()
        {
            return !IsDrumNoError && !IsLotNoError && !IsQuantity;
        }
    }
}
