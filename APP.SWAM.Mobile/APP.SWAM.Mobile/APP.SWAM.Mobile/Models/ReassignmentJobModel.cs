﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class ReassignmentJobModel : BaseViewModel
    {
        private string _loginUser;
        public string LoginUser { get { return _loginUser; } set { SetProperty(ref _loginUser, value); } }
        private DateTime _startDate;
        public DateTime StartDate { get { return _startDate; } set { SetProperty(ref _startDate, value); } }

        private DateTime _startDateTime;
        public DateTime StartDateTime { get { return _startDateTime; } set { SetProperty(ref _startDateTime, value); } }
        private DateTime _endDateTime;
        public DateTime EndDateTime { get { return _endDateTime; } set { SetProperty(ref _endDateTime, value); } }

        private TimeSpan _startTime;
        public TimeSpan StartTime { get { return _startTime; } set { SetProperty(ref _startTime, value); } }
        private TimeSpan _endTime;
        public TimeSpan EndTime { get { return _endTime; } set { SetProperty(ref _endTime, value); } }
        private long? _prodTaskld;
        public long? ProdTaskId { get { return _prodTaskld; } set { SetProperty(ref _prodTaskld, value); ValidateLogin(); } }
        private string _prodTask;
        public string ProdTask { get { return _prodTask; } set { SetProperty(ref _prodTask, value); } }
        private long _reassignmentJobId;
        public long ReassignmentJobId { get { return _reassignmentJobId; } set { SetProperty(ref _reassignmentJobId, value); } }
        private long? _specificProcedureld;
        public long? SpecificProcedureId { get { return _specificProcedureld; } set { SetProperty(ref _specificProcedureld, value); ValidateLogin(); } }
        private string _specificProcedure;
        public string SpecificProcedure { get { return _specificProcedure; } set { SetProperty(ref _specificProcedure, value); ValidateLogin(); } }
        private long? _companyDbld;
        public long? CompanyDbld { get { return _companyDbld; } set { SetProperty(ref _companyDbld, value); } }
        private string _companyDb;
        public string CompanyDb { get { return _companyDb; } set { SetProperty(ref _companyDb, value); } }
        private long? _shiftCodeld;
        public long? MobileShiftId { get { return _shiftCodeld; } set { SetProperty(ref _shiftCodeld, value); ValidateLogin(); } }
        private string _shiftCode;
        public string ShiftCode { get { return _shiftCode; } set { SetProperty(ref _shiftCode, value); } }
        private List<long?> _manpowerlds = new List<long?>();
        public List<long?> ReAssignementManpowerMultipleIds { get { return _manpowerlds; } set { SetProperty(ref _manpowerlds, value); ValidateLogin(); } }

        private string _manPower;
        public string ManPower { get { return _manPower; } set { SetProperty(ref _manPower, value); } }

        #region Valldation 
        private bool _isShiftCode;
        public bool IsShiftCode { get { return _isShiftCode; } set { SetProperty(ref _isShiftCode, value); } }

        private bool _isSpecificProcedure;
        public bool IsSpecificProcedure { get { return _isSpecificProcedure; } set { SetProperty(ref _isSpecificProcedure, value); } }

        private bool _isprodTask;
        public bool IsProdTask { get { return _isprodTask; } set { SetProperty(ref _isprodTask, value); } }

        private bool _isManpowerAssign;
        public bool IsManpowerAssign { get { return _isManpowerAssign; } set { SetProperty(ref _isManpowerAssign, value); } }

        private void ValidateLogin()
        {
            IsShiftCode = MobileShiftId == null || MobileShiftId == 0;
            IsProdTask = ProdTaskId == null || ProdTaskId == 0;
            IsSpecificProcedure = SpecificProcedureId == null || SpecificProcedureId == 0;
            IsManpowerAssign = ReAssignementManpowerMultipleIds.Count == 0;
        }

        public bool IsValid()
        {
            if (IsShiftCode || IsProdTask || IsManpowerAssign || IsSpecificProcedure)
                return false;
            return true;
        }
        #endregion

    }

    public class ReAssignementManpowerMultipleModel : BaseViewModel
    {
        private long _reassignmentManpowerld;
        public long ReassignmentManpowerld { get { return _reassignmentManpowerld; } set { SetProperty(ref _reassignmentManpowerld, value); } }
        private long? _manPowerld;
        public long? ManPowerld { get { return _manPowerld; } set { SetProperty(ref _manPowerld, value); } }
        private long? _reassignmentld;
        public long? Reassignmentld { get { return _reassignmentld; } set { SetProperty(ref _reassignmentld, value); } }
    }
}
