﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class IPIRModel : BaseViewModel
    {
        private string _loginUser;
        public string LoginUser { get { return _loginUser; } set { SetProperty(ref _loginUser, value); } }
        private DateTime _startDate;
        public DateTime StartDate { get { return _startDate; } set { SetProperty(ref _startDate, value); } }
        private long _ipirmobileId;
        public long IpirmobileId { get { return _ipirmobileId; } set { SetProperty(ref _ipirmobileId, value); } }
        private long? _companyId;
        public long? CompanyId { get { return _companyId; } set { SetProperty(ref _companyId, value); } }

        private int? _intendedActionId;
        public int? IntendedActionId { get { return _intendedActionId; } set { SetProperty(ref _intendedActionId, value); ValidateLogin(); } }

        private string _intendedAction;
        public string IntendedAction { get { return _intendedAction; } set { SetProperty(ref _intendedAction, value); ValidateLogin(); } }

        private string _ipirreportNo;
        public string IpirreportNo { get { return _ipirreportNo; } set { SetProperty(ref _ipirreportNo, value); } }

        private string _location;
        public string Location { get { return _location; } set { SetProperty(ref _location, value); ValidateLogin(); } }

        private string _productionOrderNo;
        public string ProductionOrderNo { get { return _productionOrderNo; } set { SetProperty(ref _productionOrderNo, value); ValidateLogin(); } }

        private string _productName;
        public string ProductName { get { return _productName; } set { SetProperty(ref _productName, value); } }

        private string _batchNo;
        public string BatchNo { get { return _batchNo; } set { SetProperty(ref _batchNo, value); } }

        private string _issueDescription;
        public string IssueDescription { get { return _issueDescription; } set { SetProperty(ref _issueDescription, value); } }

        private string _issueDescription1;
        public string IssueDescription1 { get { return _issueDescription1; } set { SetProperty(ref _issueDescription1, value); } }

        private string _issueDescription2;
        public string IssueDescription2 { get { return _issueDescription2; } set { SetProperty(ref _issueDescription2, value); } }

        private string _ipirIsssueTitle;
        public string IPIRIssueTitle { get { return _ipirIsssueTitle; } set { SetProperty(ref _ipirIsssueTitle, value); } }

        private string _ipirIssue;
        public string IPIRIssue { get { return _ipirIssue; } set { SetProperty(ref _ipirIssue, value); } }

        private List<long?> _ipirIssueTitleIds = new List<long?>();
        public List<long?> IpirIssueTitleIds { get { return _ipirIssueTitleIds; } set { SetProperty(ref _ipirIssueTitleIds, value); } }

        private List<long?> _ipirIsssueIds = new List<long?>();
        public List<long?> IpirIssueIds { get { return _ipirIsssueIds; } set { SetProperty(ref _ipirIsssueIds, value); } }


        private string _ipirIsssue1Title;
        public string IPIRIssue1Title { get { return _ipirIsssue1Title; } set { SetProperty(ref _ipirIsssue1Title, value); } }

        private string _ipir1Isssue;
        public string IPIR1Issue { get { return _ipir1Isssue; } set { SetProperty(ref _ipir1Isssue, value); } }

        private string _applicationUser;
        public string ApplicationUser { get { return _applicationUser; } set { SetProperty(ref _applicationUser, value); } }

        private long? _applicationUserId;
        public long? ApplicationUserId { get { return _applicationUserId; } set { SetProperty(ref _applicationUserId, value); } }

        private string _applicationUser1;
        public string ApplicationUser1 { get { return _applicationUser1; } set { SetProperty(ref _applicationUser1, value); } }

        private long? _applicationUserId1;
        public long? ApplicationUserId1 { get { return _applicationUserId1; } set { SetProperty(ref _applicationUserId1, value); } }

        private string _statusAssignmentName;
        public string StatusAssignmentName { get { return _statusAssignmentName; } set { SetProperty(ref _statusAssignmentName, value); } }

        private int? _statusAssignmentId;
        public int? StatusAssignmentId { get { return _statusAssignmentId; } set { SetProperty(ref _statusAssignmentId, value); } }

        private string _statusCode;
        public string StatusCode { get { return _statusCode; } set { SetProperty(ref _statusCode, value); } }

        private List<long?> _ipirIssue1TitleIds = new List<long?>();
        public List<long?> IpirIssue1TitleIds { get { return _ipirIssue1TitleIds; } set { SetProperty(ref _ipirIssue1TitleIds, value); } }

        private List<long?> _ipirIssue1Ids = new List<long?>();
        public List<long?> IpirIssue1Ids { get { return _ipirIssue1Ids; } set { SetProperty(ref _ipirIssue1Ids, value); } }

        private string _photo;
        public string Photo { get { return _photo; } set { SetProperty(ref _photo, value); } }
        private string _photo1;
        public string Photo1 { get { return _photo1; } set { SetProperty(ref _photo1, value); } }
        private string _photo2;
        public string Photo2 { get { return _photo2; } set { SetProperty(ref _photo2, value); } }

        private bool? _isConfirmIPIR;
        public bool? IsConfirmIPIR { get { return _isConfirmIPIR; } set { SetProperty(ref _isConfirmIPIR, value); } }

        private byte[] _photoSource;
        public byte[] PhotoSource { get { return _photoSource; } set { SetProperty(ref _photoSource, value); } }
        private byte[] _photoSource1;
        public byte[] PhotoSource1 { get { return _photoSource1; } set { SetProperty(ref _photoSource1, value); } }
        private byte[] _photoSource2;
        public byte[] PhotoSource2 { get { return _photoSource2; } set { SetProperty(ref _photoSource2, value); } }


        #region Valldation 
        private bool _isIntendedAction;
        public bool IsIntendedAction { get { return _isIntendedAction; } set { SetProperty(ref _isIntendedAction, value); } }

        private bool _isLocation;
        public bool IsLocation { get { return _isLocation; } set { SetProperty(ref _isLocation, value); } }

        private bool _isProductionOrder;
        public bool IsProductionOrder { get { return _isProductionOrder; } set { SetProperty(ref _isProductionOrder, value); } }


        private void ValidateLogin()
        {
            IsIntendedAction = string.IsNullOrEmpty(IntendedAction);
            IsLocation = string.IsNullOrEmpty(Location);
            IsProductionOrder = string.IsNullOrEmpty(ProductionOrderNo);
        }

        public bool IsValid()
        {
            if (IsIntendedAction || IsLocation || IsProductionOrder)
                return false;
            return true;
        }

        #endregion

    }

    public class IPIRMobilePost : BaseViewModel
    {
        public long? IpirmobileId { get; set; }
        public int? IntendedActionId { get; set; }
        public List<IPIRMobileActionModel> MobileActionItems { get; set; } = new List<IPIRMobileActionModel>();
    }

    public class IPIRMobileActionModel
    {
        public long IpirmobileActionId { get; set; }
        public long? IpirmobileId { get; set; }
        public int? IntendedActionId { get; set; }
        public string IntendedAction { get; set; }
        public bool? IsConfirmIpir { get; set; }
        public string IssueDescription { get; set; }
        public string Photo { get; set; }
        public byte[] PhotoSource { get; set; }
        public long? AssignmentTo { get; set; }
        public string AssignmentToName { get; set; }
        public int? AssignmentStatusId { get; set; }
        public string AssignmentStatus { get; set; }
        public List<long?> IssueRelatedIds { get; set; }
        public List<long?> IssueTitleIds { get; set; }
    }
}