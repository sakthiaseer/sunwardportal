﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.Models
{
    public class ScanDocumentModel : BaseViewModel
    {
        private long _documentID;
        public long DocumentID   { get { return _documentID; } set { SetProperty(ref _documentID, value); } }

        private string _documentName;
        public string DocumentName { get { return _documentName; } set { SetProperty(ref _documentName, value); ValidateLogin(); } }

        private long _fileProfileTypeID;
        public long FileProfileTypeID { get { return _fileProfileTypeID; } set { SetProperty(ref _fileProfileTypeID, value); } }

        private string _profileTypeName;
        public string ProfileTypeName { get { return _profileTypeName; } set { SetProperty(ref _profileTypeName, value); } }

        private string _documentPath;
        public string DocumentPath { get { return _documentPath; } set { SetProperty(ref _documentPath, value); } }
        private bool _isProductionDocument;
        public bool IsProductionDocument { get { return _isProductionDocument; } set { SetProperty(ref _isProductionDocument, value); } }

        private Guid _sessionId;
        public Guid SessionId { get { return _sessionId; } set { SetProperty(ref _sessionId, value); } }

        private List<ItemModel> _items = new List<ItemModel>();
        public List<ItemModel> Items
        {
            get { return _items; }
            set { _items = value; OnPropertyChanged("Items"); }
        }

        #region Validation 
        private bool _isDocumentNameError;
        public bool IsDocumentNameError { get { return _isDocumentNameError; } set { SetProperty(ref _isDocumentNameError, value); } }

        private void ValidateLogin()
        {
            IsDocumentNameError = string.IsNullOrEmpty(DocumentName);
        }

        public bool IsValid()
        {
            if (IsDocumentNameError)
                return false;
            return true;
        }
        #endregion
    }

    public class ItemModel : BaseViewModel
    {

        byte[] imageByte;
        public byte[] ImageByte
        {
            get { return imageByte; }
            set { imageByte = value; OnPropertyChanged("ImageByte"); }
        }

        ImageSource image;
        public ImageSource Image
        {
            get { return image; }
            set { image = value; OnPropertyChanged("Image"); }
        }

        string fileName;
        public string FileName
        {
            get { return fileName; }
            set { fileName = value; OnPropertyChanged("FileName"); }
        }
        int id;
        public int Id { get { return id; } set { id = value; OnPropertyChanged("Id"); } }
    }
}
