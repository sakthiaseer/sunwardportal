﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class StartOfDayModel : BaseViewModel
    {
        private string _loginUser;
        public string LoginUser { get { return _loginUser; } set { SetProperty(ref _loginUser, value); } }
        private DateTime _startDate;
        public DateTime StartDate { get { return _startDate; } set { SetProperty(ref _startDate, value); } }
        private long _startOfDayId;
        public long StartOfDayId { get { return _startOfDayId; } set { SetProperty(ref _startOfDayId, value); } }
        private long? _companyDbid;
        public long? CompanyDbid { get { return _companyDbid; } set { SetProperty(ref _companyDbid, value); } }
        private string _companyDb;
        public string CompanyDb { get { return _companyDb; } set { SetProperty(ref _companyDb, value); } }
        private long? _workingBlockId;
        public long? WorkingBlockId { get { return _workingBlockId; } set { SetProperty(ref _workingBlockId, value); ValidateLogin(); } }
        private string _workingBlock;
        public string WorkingBlock { get { return _workingBlock; } set { SetProperty(ref _workingBlock, value); } }
        private List<long> _manpowerIds = new List<long>();
        public List<long> ManPowerIds { get { return _manpowerIds; } set { SetProperty(ref _manpowerIds, value); ValidateLogin(); } }
        private List<long> _prodTaskIds = new List<long>();
        public List<long> ProdTaskIds { get { return _prodTaskIds; } set { SetProperty(ref _prodTaskIds, value); ValidateLogin(); } }

        private string _manPower;
        public string ManPower { get { return _manPower; } set { SetProperty(ref _manPower, value); } }
        private string _prodTask;
        public string ProdTask { get { return _prodTask; } set { SetProperty(ref _prodTask, value); } }

        #region Validation 
        private bool _isWorkingBlock;
        public bool IsWorkingBlock { get { return _isWorkingBlock; } set { SetProperty(ref _isWorkingBlock, value); } }

        private bool _isprodTask;
        public bool IsProdTask { get { return _isprodTask; } set { SetProperty(ref _isprodTask, value); } }

        private bool _isManpowerAssign;
        public bool IsManpowerAssign { get { return _isManpowerAssign; } set { SetProperty(ref _isManpowerAssign, value); } }

        private void ValidateLogin()
        {
            IsWorkingBlock = WorkingBlockId == null || WorkingBlockId == 0;
            IsProdTask = ProdTaskIds.Count == 0;
            IsManpowerAssign = ManPowerIds.Count == 0;
        }

        public bool IsValid()
        {
            if (IsWorkingBlock || IsProdTask || IsManpowerAssign)
                return false;
            return true;
        }
        #endregion
    }
    public class StartOfDayManpowerMultipleModel : BaseViewModel
    {
        private long _startOfDayManpowerMultipleId;
        public long StartOfDayManpowerMultipleId { get { return _startOfDayManpowerMultipleId; } set { SetProperty(ref _startOfDayManpowerMultipleId, value); } }
        private long? _startOfDayManpowerId;
        public long? StartOfDayManpowerId { get { return _startOfDayManpowerId; } set { SetProperty(ref _startOfDayManpowerId, value); } }
        private long? _startOfDayId;
        public long? StartOfDayId { get { return _startOfDayId; } set { SetProperty(ref _startOfDayId, value); } }
    }
    public class StartOfDayProdTaskMultipleModel : BaseViewModel
    {
        private long _startOfDayProdTaskMultipleId;
        public long StartOfDayProdTaskMultipleId { get { return _startOfDayProdTaskMultipleId; } set { SetProperty(ref _startOfDayProdTaskMultipleId, value); } }
        private long? _prodTaskPerformId;
        public long? ProdTaskPerformId { get { return _prodTaskPerformId; } set { SetProperty(ref _prodTaskPerformId, value); } }
        private long? _startOfDayId;
        public long? StartOfDayId { get { return _startOfDayId; } set { SetProperty(ref _startOfDayId, value); } }
    }
}
