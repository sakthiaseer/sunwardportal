﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.Models
{
    public class MenuItem : BaseViewModel
    {
        private string _title;
        private MenuItemType _menuItemType;
        private Type _viewModelType;
        private bool _isEnabled;
        private bool _isAlwaysAvailable;

        public string Title
        {
            get
            {
                return _title;
            }

            set
            {
                SetProperty(ref _title, value);
            }
        }

        public MenuItemType MenuItemType
        {
            get
            {
                return _menuItemType;
            }

            set
            {
                SetProperty(ref _menuItemType, value);
            }
        }

        public Type ViewModelType
        {
            get
            {
                return _viewModelType;
            }

            set
            {
                SetProperty(ref _viewModelType, value);
            }
        }

        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }

            set
            {
                SetProperty(ref _isEnabled, value);
            }
        }

        public bool IsAlwaysAvailable
        {
            get
            {
                return _isAlwaysAvailable;
            }

            set
            {
                SetProperty(ref _isAlwaysAvailable, value);
            }
        }

        public Func<Task> AfterNavigationAction { get; set; }
    }
    public enum MenuItemType
    {
        Home = 0,
        Menu = 1,
        Location = 2,
        ProductionEntry = 3,
        AssetItems = 4,
        Chat = 5,
        Ticket = 6,
        Transfer = 7,
        SDispensing = 8,
        DDispensing = 9,
        About = 10,
        Logout = 11,
        MaterialReclass = 12,
        TransferReClass = 13,
        TransferProduction = 14,
        TransferReceive = 15,
        ScannedImage = 16,
        ProductionScan = 17,
        IPIR = 18,
        CreateIPIR = 19,
        ProductionOutput = 20,
        QCApproval = 21,
        StartOfDay = 22,
        ReAssignmentJob = 23,
        BMRToCarton = 24,
        ProcessTransfer=25,
    }
}