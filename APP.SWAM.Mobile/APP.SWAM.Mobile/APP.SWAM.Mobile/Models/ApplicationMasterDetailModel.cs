﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class ApplicationMasterDetailModel : BaseViewModel
    {
        public long ApplicationMasterDetailId { get; set; }
        public long ApplicationMasterId { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string ApplicationMaster { get; set; }
        public string NameDescription { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileName { get; set; }
        public bool? IsApplyProfile { get; set; }
    }
}
