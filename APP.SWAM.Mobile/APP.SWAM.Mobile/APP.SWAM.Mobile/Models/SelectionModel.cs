﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class SelectionModel:BaseViewModel
    {

        private long _id;
        private string _name;
        public long Id { get { return _id; } set { SetProperty(ref _id, value); } }
        public string Name { get { return _name; } set { SetProperty(ref _name, value); } }
        public override string ToString() => Name;
    }
}
