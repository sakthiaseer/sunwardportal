﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class DispensorDrumModel : BaseViewModel
    {


        private long _dispenserDispensingDrumDetailsId;
        public long DispenserDispensingDrumDetailsId { get { return _dispenserDispensingDrumDetailsId; } set { SetProperty(ref _dispenserDispensingDrumDetailsId, value); } }
        private long _dispenserDispensingLineId;
        public long DispenserDispensingLineId { get { return _dispenserDispensingLineId; } set { SetProperty(ref _dispenserDispensingLineId, value); } }
        private string _materialNo;
        public string MaterialNo { get { return _materialNo; } set { SetProperty(ref _materialNo, value); } }
        private string _lotNo;
        public string LotNo { get { return _lotNo; } set { SetProperty(ref _lotNo, value); } }
        private string _qcrefNo;
        public string QcrefNo { get { return _qcrefNo; } set { SetProperty(ref _qcrefNo, value); } }
        private decimal? _weight;
        public decimal? Weight { get { return _weight; } set { SetProperty(ref _weight, value); } }
        private string _weighingPhoto;
        public string WeighingPhoto { get { return _weighingPhoto; } set { SetProperty(ref _weighingPhoto, value); } }
        private byte[] _weighingPhotoStream;
        public byte[] WeighingPhotoStream { get { return _weighingPhotoStream; } set { SetProperty(ref _weighingPhotoStream, value); } }
    }
}
