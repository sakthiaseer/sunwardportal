﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class LastChatMetaData
    {
        public long LastChatMetaDataID { get; set; }
        public string LastChatDatetime { get; set; }
        public bool MessageIsFromMe { get; set; }
        public string MessageContent { get; set; }
    }
}
