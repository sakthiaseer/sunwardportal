﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class MaterialReturnModel : BaseViewModel
    {
        private long _materialReturnID;
        public long MaterialReturnID { get { return _materialReturnID; } set { SetProperty(ref _materialReturnID, value); } }

        private string _loginUser;
        public string LoginUser { get { return _loginUser; } set { SetProperty(ref _loginUser, value); } }
        private DateTime _startDate;
        public DateTime StartDate { get { return _startDate; } set { SetProperty(ref _startDate, value); } }

        private string _productionOrderNo;
        public string ProdOrderNo { get { return _productionOrderNo; } set { SetProperty(ref _productionOrderNo, value); ValidateLogin(); } }

        private string _replanRefNo;
        public string ReplanRefNo { get { return _replanRefNo; } set { SetProperty(ref _replanRefNo, value); } }
        private string _description;
        public string Description { get { return _description; } set { SetProperty(ref _description, value); } }
        private string _sublotNo;
        public string LotNo { get { return _sublotNo; } set { SetProperty(ref _sublotNo, value); } }

        private string _drumNo;
        public string DrumNo { get { return _drumNo; } set { SetProperty(ref _drumNo, value); } }

        private string _qcRefNo;
        public string QCRefNo { get { return _qcRefNo; } set { SetProperty(ref _qcRefNo, value); } }

        private string _itemNo;
        public string ItemNo { get { return _itemNo; } set { SetProperty(ref _itemNo, value); } }

        private string _batchNo;
        public string BatchNo { get { return _batchNo; } set { SetProperty(ref _batchNo, value); } }
        private string _UOM;
        public string UOM { get { return _UOM; } set { SetProperty(ref _UOM, value); } }
        private string _quantity;
        public string BalanceWeight { get { return _quantity; } set { SetProperty(ref _quantity, value); } }

        private string _prodLineNo;
        public string ProdLineNo { get { return _prodLineNo; } set { SetProperty(ref _prodLineNo, value); } }

        private string _photo;
        public string Photo { get { return _photo; } set { SetProperty(ref _photo, value); } }

        private byte[] _frontPhoto;
        public byte[] FrontPhoto { get { return _frontPhoto; } set { SetProperty(ref _frontPhoto, value); } }

        private bool _postedtoNAV;
        public bool PostedtoNAV { get { return _postedtoNAV; } set { SetProperty(ref _postedtoNAV, value); } }

        private bool _isproductionOrderNo;
        public bool IsProductionOrderNoError { get { return _isproductionOrderNo; } set { SetProperty(ref _isproductionOrderNo, value); } }

        private bool _isdrumNo;
        public bool IsDrumNoError { get { return _isdrumNo; } set { SetProperty(ref _isdrumNo, value); } }

        private void ValidateLogin()
        {
            IsDrumNoError = string.IsNullOrEmpty(DrumNo);
        }

        public bool IsValid()
        {
            return !IsDrumNoError;
        }

    }
}
