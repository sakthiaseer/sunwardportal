﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class FileProfileTypeMobile : BaseViewModel
    {
        private long _fileProfileTypeId;
        public long FileProfileTypeId { get { return _fileProfileTypeId; } set { SetProperty(ref _fileProfileTypeId, value); } }
        private long _profileId;
        public long ProfileId { get { return _profileId; } set { SetProperty(ref _profileId, value); } }
        private string _name;
        public string Name { get { return _name; } set { SetProperty(ref _name, value); } }
        private string _description;
        public string Description { get { return _description; } set { SetProperty(ref _description, value); } }
        private long? _parentId;
        public long? ParentId { get { return _parentId; } set { SetProperty(ref _parentId, value); } }
        private bool? _isExpiryDate;
        public bool? IsExpiryDate { get { return _isExpiryDate; } set { SetProperty(ref _isExpiryDate, value); } }
        private string _profileName;
        public string ProfileName { get { return _profileName; } set { SetProperty(ref _profileName, value); } }
        private string _hint;
        public string Hint { get { return _hint; } set { SetProperty(ref _hint, value); } }
        private long _totalDocuments;
        public long TotalDocuments { get { return _totalDocuments; } set { SetProperty(ref _totalDocuments, value); } }
    }

    public class FPModel : BaseViewModel
    {
        private long _id;
        public long Id { get { return _id; } set { SetProperty(ref _id, value); } }
        private string _description;
        public string Description { get { return _description; } set { SetProperty(ref _description, value); } }
        private string _hint;
        public string Hint { get { return _hint; } set { SetProperty(ref _hint, value); } }
    }
}
