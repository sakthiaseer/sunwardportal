﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class MobileShiftModel : BaseViewModel
    {
        public long MobileShiftId { get; set; }
        public long? PlantId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
