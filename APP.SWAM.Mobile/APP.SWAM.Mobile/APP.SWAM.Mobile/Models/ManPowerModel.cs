﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class ManPowerModel : BaseViewModel
    {
        public long? EmployeeID { get; set; }
        public string Name { get; set; }
    }
}
