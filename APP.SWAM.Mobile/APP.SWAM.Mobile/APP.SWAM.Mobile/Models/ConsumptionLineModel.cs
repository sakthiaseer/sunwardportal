﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
   public class ConsumptionLineModel : BaseViewModel
    {
        private long _consumptionEntryID;
        public long ConsumptionEntryID { get { return _consumptionEntryID; } set { SetProperty(ref _consumptionEntryID, value); } }
        private long _consumptionLineID;
        public long ConsumptionLineID { get { return _consumptionLineID; } set { SetProperty(ref _consumptionLineID, value); } }

        
        private string _loginUser;
        public string LoginUser { get { return _loginUser; } set { SetProperty(ref _loginUser, value); } }
        private DateTime _startDate;
        public DateTime StartDate { get { return _startDate; } set { SetProperty(ref _startDate, value); } }

        private string _productionOrderNo;
        public string ProdOrderNo { get { return _productionOrderNo; } set { SetProperty(ref _productionOrderNo, value); } }

        private string _transferFrom;
        public string TransferFrom { get { return _transferFrom; } set { SetProperty(ref _transferFrom, value); } }
        private string _transferTo;
        public string TransferTo { get { return _transferTo; } set { SetProperty(ref _transferTo, value); } }

        private string _lotNo;
        public string LotNo { get { return _lotNo; } set { SetProperty(ref _lotNo, value); } }

        private string _qcRefNo;
        public string QCRefNo { get { return _qcRefNo; } set { SetProperty(ref _qcRefNo, value); } }

        private string _itemNo;
        public string ItemNo { get { return _itemNo; } set { SetProperty(ref _itemNo, value); } }

        private string _description;
        public string Description { get { return _description; } set { SetProperty(ref _description, value); } }

        private string _batchNo;
        public string BatchNo { get { return _batchNo; } set { SetProperty(ref _batchNo, value); } }
        private string _UOM;
        public string UOM { get { return _UOM; } set { SetProperty(ref _UOM, value); } }
        private string _quantity;
        public string Quantity { get { return _quantity; } set { SetProperty(ref _quantity, value); } }

        private string _baseUOM;
        public string BaseUOM { get { return _baseUOM; } set { SetProperty(ref _baseUOM, value); } }
        private string _basequantity;
        public string BaseQuantity { get { return _basequantity; } set { SetProperty(ref _basequantity, value); } }

        private string _prodLineNo;
        public string ProdLineNo { get { return _prodLineNo; } set { SetProperty(ref _prodLineNo, value); } }

        private bool _postedtoNAV;
        public bool PostedtoNAV { get { return _postedtoNAV; } set { SetProperty(ref _postedtoNAV, value); } }

        private string _prodComLineNo;
        public string ProdComLineNo { get { return _prodComLineNo; } set { SetProperty(ref _prodComLineNo, value); } }

        private long? _addedUserId;
        public long? AddedUserId { get { return _addedUserId; } set { SetProperty(ref _addedUserId, value); } }



    }
}
