﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class BaseModel
    {
        public int? StatusCodeID { get; set; }
        [DisplayName("Status")]
        public string StatusCode { get; set; }
        public Nullable<System.DateTime> LastAccessDate { get; set; }
        public Nullable<long> AddedByUserID { get; set; }
        [DisplayName("Created By")]
        public string AddedByUser { get; set; }
        [DisplayName("Created Date")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MMM-yyyy HH:mm:ss tt}")]
        public System.DateTime? AddedDate { get; set; }
        [DisplayName("Modified By")]
        public string ModifiedByUser { get; set; }
        public long? ModifiedByUserID { get; set; }
        [DisplayName("Modified Date")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MMM-yyyy HH:mm:ss tt}")]
        public DateTime? ModifiedDate { get; set; }

        public bool IsRecordExist { get; set; }
    }
}
