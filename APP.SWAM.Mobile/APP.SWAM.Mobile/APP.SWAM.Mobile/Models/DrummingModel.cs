﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class DrummingModel : BaseViewModel
    {
        private long _drummingId;
        public long DrummingId { get { return _drummingId; } set { SetProperty(ref _drummingId, value); } }

        private string _loginUser;
        public string LoginUser { get { return _loginUser; } set { SetProperty(ref _loginUser, value); } }
        private DateTime _startDate;
        public DateTime StartDate { get { return _startDate; } set { SetProperty(ref _startDate, value); } }

        private string _productionOrderNo;
        public string ProdOrderNo { get { return _productionOrderNo; } set { SetProperty(ref _productionOrderNo, value); } }

        private string _wrokOrderNo;
        public string WrokOrderNo { get { return _wrokOrderNo; } set { SetProperty(ref _wrokOrderNo, value); ValidateLogin(); } }
        private string _itemNo;
        public string ItemNo { get { return _itemNo; } set { SetProperty(ref _itemNo, value); } }
        private string _description;
        public string Description { get { return _description; } set { SetProperty(ref _description, value); } }
        private string _sublotNo;
        public string SublotNo { get { return _sublotNo; } set { SetProperty(ref _sublotNo, value); } }
        private string _drumNo;
        public string DrumNo { get { return _drumNo; } set { SetProperty(ref _drumNo, value); ValidateLogin(); } }
        private string _drumWeight;
        public string DrumWeight { get { return _drumWeight; } set { SetProperty(ref _drumWeight, value); } }
        private string _bagNo;
        public string BagNo { get { return _bagNo; } set { SetProperty(ref _bagNo, value); } }
        private string _bagWeight;
        public string BagWeight { get { return _bagWeight; } set { SetProperty(ref _bagWeight, value); } }
        private string _totalWeight;
        public string TotalWeight { get { return _totalWeight; } set { SetProperty(ref _totalWeight, value); } }
        private bool _isproductionOrderNo;
        public bool IsWorkOrderNoError { get { return _isproductionOrderNo; } set { SetProperty(ref _isproductionOrderNo, value); } }
        private bool _drumNoError;
        public bool IsDrumNoError { get { return _drumNoError; } set { SetProperty(ref _drumNoError, value); } }


        private void ValidateLogin()
        {
            IsWorkOrderNoError = string.IsNullOrEmpty(WrokOrderNo);
            IsDrumNoError = string.IsNullOrEmpty(DrumNo);
        }

        public bool IsValid()
        {
            return !IsWorkOrderNoError || !IsDrumNoError;
        }

    }
}
