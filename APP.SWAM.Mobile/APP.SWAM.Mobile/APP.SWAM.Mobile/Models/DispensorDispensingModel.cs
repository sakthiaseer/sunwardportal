﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class DispensorDispensingModel : BaseViewModel
    {
        public long _dispenserDispensingId;
        public long DispenserDispensingId { get { return _dispenserDispensingId; } set { SetProperty(ref _dispenserDispensingId, value); } }
        private string _loginUser;
        public string LoginUser { get { return _loginUser; } set { SetProperty(ref _loginUser, value); } }
        private DateTime _startDate;
        public DateTime StartDate { get { return _startDate; } set { SetProperty(ref _startDate, value); } }


        private string _wrokOrderNo;
        public string WorkOrderNo { get { return _wrokOrderNo; } set { SetProperty(ref _wrokOrderNo, value); IsValidWork(); } }


        private string _itemNo;
        public string ItemNo { get { return _itemNo; } set { SetProperty(ref _itemNo, value); } }

        private string _description;
        public string Description { get { return _description; } set { SetProperty(ref _description, value); } }

        private int? _totalItem;
        public int? TotalItem { get { return _totalItem; } set { SetProperty(ref _totalItem, value); } }

        private int? _weighingMaterial;
        public int? WeighingMaterial { get { return _weighingMaterial; } set { SetProperty(ref _weighingMaterial, value); } }


        private bool _isproductionOrderNo;
        public bool IsWorkOrderNoError { get { return _isproductionOrderNo; } set { SetProperty(ref _isproductionOrderNo, value); } }

        public bool IsValidWork()
        {
            return !IsWorkOrderNoError;
        }
    }
}
