﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
   public class ConsumptionModel : BaseViewModel
    {
        private long _consumptionEntryID;
        public long ConsumptionEntryID { get { return _consumptionEntryID; } set { SetProperty(ref _consumptionEntryID, value); } }

        private string _loginUser;
        public string LoginUser { get { return _loginUser; } set { SetProperty(ref _loginUser, value);   } }
        private DateTime _startDate;
        public DateTime StartDate { get { return _startDate; } set { SetProperty(ref _startDate, value);   } }

        private string _productionOrderNo;
        public string ProdOrderNo { get { return _productionOrderNo; } set { SetProperty(ref _productionOrderNo, value); ValidateLogin(); } }

        private string _replanRefNo;
        public string ReplanRefNo { get { return _replanRefNo; } set { SetProperty(ref _replanRefNo, value);   } }
        private string _description;
        public string Description { get { return _description; } set { SetProperty(ref _description, value);   } }
        private string _sublotNo;
        public string SublotNo { get { return _sublotNo; } set { SetProperty(ref _sublotNo, value);   } }

        private string _lineNo;
        public string LineNo { get { return _lineNo; } set { SetProperty(ref _lineNo, value); } }
        private string _transferFrom;
        public string TransferFrom { get { return _transferFrom; } set { SetProperty(ref _transferFrom, value); } }
        private string _transferTo;
        public string TransferTo { get { return _transferTo; } set { SetProperty(ref _transferTo, value); } }

        private bool _isproductionOrderNo;
        public bool IsProductionOrderNoError { get { return _isproductionOrderNo; } set { SetProperty(ref _isproductionOrderNo, value); } }

        private void ValidateLogin()
        {            
            IsProductionOrderNoError = string.IsNullOrEmpty(ProdOrderNo);            
        }

        public bool IsValid()
        { 
            return !IsProductionOrderNoError;
        }

    }
}
