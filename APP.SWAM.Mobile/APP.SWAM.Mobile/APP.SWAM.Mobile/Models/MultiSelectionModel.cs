﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.MultiSelectListView;

namespace APP.SWAM.Mobile.Models
{
    public class MultiSelectionModel: BaseModel
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string MultiSelectionName { get; set; }

        public MultiSelectObservableCollection<MultiSelectionModel> MultiselectionItems { get; set; }
    }
}
