﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class CodeMasterModel
    {
        public int CodeID { get; set; }
        public string CodeValue { get; set; }
        public string CodeType { get; set; }

        public string CodeDescription { get; set; }

    }
}
