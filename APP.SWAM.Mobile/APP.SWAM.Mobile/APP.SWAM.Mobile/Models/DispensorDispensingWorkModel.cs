﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class DispensorDispensingWorkModel : BaseViewModel
    {
        private long _dispenserDispensingLineId;
        public long DispenserDispensingLineId { get { return _dispenserDispensingLineId; } set { SetProperty(ref _dispenserDispensingLineId, value); } }
        private long? _dispenserDispensingId;
        public long? DispenserDispensingId { get { return _dispenserDispensingId; } set { SetProperty(ref _dispenserDispensingId, value); } }
        private string _prodOrderNo;
        public string ProdOrderNo { get { return _prodOrderNo; } set { SetProperty(ref _prodOrderNo, value); } }
        private string _workOrderNo;
        public string WorkOrderNo { get { return _workOrderNo; } set { SetProperty(ref _workOrderNo, value); } }
        private int? _prodLineNo;
        public int? ProdLineNo { get { return _prodLineNo; } set { SetProperty(ref _prodLineNo, value); } }
        private string _subLotNo;
        public string SubLotNo { get { return _subLotNo; } set { SetProperty(ref _subLotNo, value); } }
        private string _jobNo;
        public string JobNo { get { return _jobNo; } set { SetProperty(ref _jobNo, value); } }
        private string _bagNo;
        public string BagNo { get { return _bagNo; } set { SetProperty(ref _bagNo, value); } }
        private decimal? beforeTareWeight;
        public decimal? BeforeTareWeight { get { return beforeTareWeight; } set { SetProperty(ref beforeTareWeight, value); } }
        private string _imageBeforeTare;
        public string ImageBeforeTare { get { return _imageBeforeTare; } set { SetProperty(ref _imageBeforeTare, value); } }
        private byte[] _beforeTarePhoto;
        public byte[] BeforeTarePhoto { get { return _beforeTarePhoto; } set { SetProperty(ref _beforeTarePhoto, value); } }
        private decimal? _afterTareWeight;
        public decimal? AfterTareWeight { get { return _afterTareWeight; } set { SetProperty(ref _afterTareWeight, value); } }
        private byte[] _afterTarePhoto;
        public byte[] AfterTarePhoto { get { return _afterTarePhoto; } set { SetProperty(ref _afterTarePhoto, value); } }
        private string _imageAfterTare;
        public string ImageAfterTare { get { return _imageAfterTare; } set { SetProperty(ref _imageAfterTare, value); } }
        private string _itemNo;
        public string ItemNo { get { return _itemNo; } set { SetProperty(ref _itemNo, value); } }
        private string _lotNo;
        public string LotNo { get { return _lotNo; } set { SetProperty(ref _lotNo, value); } }
        private string _qcrefNo;
        public string QcrefNo { get { return _qcrefNo; } set { SetProperty(ref _qcrefNo, value); } }
        private decimal? _weight;
        public decimal? Weight { get { return _weight; } set { SetProperty(ref _weight, value); } }
        private string _weighingPhoto;
        public string WeighingPhoto { get { return _weighingPhoto; } set { SetProperty(ref _weighingPhoto, value); } }
        private byte[] _weighingPhotoStream;
        public byte[] WeighingPhotoStream { get { return _weighingPhotoStream; } set { SetProperty(ref _weighingPhotoStream, value); } }
        private bool? _printLabel;
        public bool? PrintLabel { get { return _printLabel; } set { SetProperty(ref _printLabel, value); } }
        public bool? PostedtoNAV { get; set; }

        private List<AppDispenserDispensingDrumDetailModel> _dispenserDispensingDrumDetailModels = new List<AppDispenserDispensingDrumDetailModel>();
        public List<AppDispenserDispensingDrumDetailModel> DispenserDispensingDrumDetailModels { get { return _dispenserDispensingDrumDetailModels; } set { SetProperty(ref _dispenserDispensingDrumDetailModels, value); } }

    }
}
