﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace APP.SWAM.Mobile.Models
{
    public class HomeMenuModel : BaseViewModel
    {
        string imageSource;
        public string ImageSource
        {
            get { return imageSource; }
            set { SetProperty(ref imageSource, value); }
        }

        string menuName;
        public string MenuName
        {
            get { return menuName; }
            set { SetProperty(ref menuName, value); }
        }

        MenuItemType menuItem;
        public MenuItemType MenuItem
        {
            get { return menuItem; }
            set { SetProperty(ref menuItem, value); }
        }

        bool isVisible;
        public bool IsVisible
        {
            get { return IsVisible; }
            set { SetProperty(ref isVisible, value); }
        }

        public ICommand MenuCommand { get; set; }

    }
}
