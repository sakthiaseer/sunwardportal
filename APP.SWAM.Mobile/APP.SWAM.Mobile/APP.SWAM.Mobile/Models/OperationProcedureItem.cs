﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class OperationProcedureItem
    {
        public string Name { get; set; }
        public long Id { get; set; }
        public long? ParentId { get; set; }

    }
}
