﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
   public class SupervisorDispensingModel : BaseViewModel
    {
        private long _supervisorDispensingId;
        public long SupervisorDispensingId { get { return _supervisorDispensingId; } set { SetProperty(ref _supervisorDispensingId, value); } }

        private string _loginUser;
        public string LoginUser { get { return _loginUser; } set { SetProperty(ref _loginUser, value); } }
        private DateTime _startDate;
        public DateTime StartDate { get { return _startDate; } set { SetProperty(ref _startDate, value); } }

        private string _productionOrderNo;
        public string ProdOrderNo { get { return _productionOrderNo; } set { SetProperty(ref _productionOrderNo, value);  } }
        private string _wrokOrderNo;
        public string WrokOrderNo { get { return _wrokOrderNo; } set { SetProperty(ref _wrokOrderNo, value); ValidateLogin(); } }
        private string _jobNo;
        public string JobNo { get { return _jobNo; } set { SetProperty(ref _jobNo, value); ValidateLogin(); } }
        private bool _preLineClearance;
        public bool PreLineClearance { get { return _preLineClearance; } set { SetProperty(ref _preLineClearance, value);   } }
        private string _itemNo;
        public string ItemName { get { return _itemNo; } set { SetProperty(ref _itemNo, value);   } }
        private string _description;
        public string Description { get { return _description; } set { SetProperty(ref _description, value); } }
        private string _weighingMachine;
        public string WeighingMachine { get { return _weighingMachine; } set { SetProperty(ref _weighingMachine, value);  } }
        private string _lotnoo;
        public string LotNo { get { return _lotnoo; } set { SetProperty(ref _lotnoo, value); ValidateLogin(); } }
        private string _sublotnoo;
        public string SubLotNo { get { return _sublotnoo; } set { SetProperty(ref _sublotnoo, value); ValidateLogin(); } }
        private string _qcrefNo;
        public string QcrefNo { get { return _qcrefNo; } set { SetProperty(ref _qcrefNo, value);   } }
        private string _uom;
        public string UOM { get { return _uom; } set { SetProperty(ref _uom, value); } }
        private string _drumNo;
        public string DrumNo { get { return _drumNo; } set { SetProperty(ref _drumNo, value); ValidateLogin(); } }
        private string _drumWeight;
        public string DrumWeight { get { return _drumWeight; } set { SetProperty(ref _drumWeight, value); ValidateLogin();  } }

        private int _totalMatWeight;
        public int TotalMatWeight { get { return _totalMatWeight; } set { SetProperty(ref _totalMatWeight, value); } }

        private bool _isproductionOrderNo;
        public bool IsWorkOrderNoError { get { return _isproductionOrderNo; } set { SetProperty(ref _isproductionOrderNo, value); } }
        private bool _drumNoError;
        public bool IsDrumNoError { get { return _drumNoError; } set { SetProperty(ref _drumNoError, value); } }

        private bool _drumWeightError;
        public bool IsDrumWeight { get { return _drumWeightError; } set { SetProperty(ref _drumWeightError, value); } }

        

        private bool _jobNoError;
        public bool IsJobNoError { get { return _jobNoError; } set { SetProperty(ref _jobNoError, value); } }

        private void ValidateLogin()
        {
            IsWorkOrderNoError = string.IsNullOrEmpty(WrokOrderNo);
            IsDrumNoError = string.IsNullOrEmpty(DrumNo);
            IsJobNoError = string.IsNullOrEmpty(JobNo);
            IsDrumWeight = string.IsNullOrEmpty(DrumWeight);
        }

        public bool IsValid()
        {
            return !IsWorkOrderNoError || !IsDrumNoError || !IsJobNoError || !IsDrumWeight;
        }
        public bool IsValidWork()
        {
            return !IsWorkOrderNoError ;
        }
    }
}
