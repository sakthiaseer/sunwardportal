﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class SourceListMobile
    {
        public long SourceListID { get; set; }
        public string Name { get; set; }
    }
}
