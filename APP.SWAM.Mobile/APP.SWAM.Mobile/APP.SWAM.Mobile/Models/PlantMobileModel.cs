﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class PlantMobileModel : BaseViewModel
    {
        private long _plantID;

        private string _plantCode;

        private string _description;
        public long PlantID { get { return _plantID; } set { SetProperty(ref _plantID, value); } }
        public string PlantCode { get { return _plantCode; } set { SetProperty(ref _plantCode, value); } }
        public string Description { get { return _description; } set { SetProperty(ref _description, value); } }
    }
}
