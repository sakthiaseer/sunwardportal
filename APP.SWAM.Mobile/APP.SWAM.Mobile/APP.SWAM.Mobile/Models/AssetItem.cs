﻿using APP.SWAM.Mobile.ViewModels;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.Models
{
    public class PlantMaintenanceEntryLineModel : BaseViewModel
    {
        private long _plantEntryLineID;
        public long PlantEntryLineID { get { return _plantEntryLineID; } set { SetProperty(ref _plantEntryLineID, value); } }

        private long? _plantEntryId;
        public long? PlantEntryId { get { return _plantEntryId; } set { SetProperty(ref _plantEntryId, value); } }

        private long? _itemGroupId;
        public long? ItemGroupId  { get { return _itemGroupId; } set { SetProperty(ref _itemGroupId, value); } }

        private string _entryNumber;
        public string EntryNumber { get { return _entryNumber; } set { SetProperty(ref _entryNumber, value); } }

        private string _manufacture;
        public string Manufacture { get { return _manufacture; } set { SetProperty(ref _manufacture, value); } }

        private string _brand;
        public string Brand { get { return _brand; } set { SetProperty(ref _brand, value); } }

        private string _modelNumber;
        public string ModelNumber { get { return _modelNumber; } set { SetProperty(ref _modelNumber, value); } }

        private string _manufactureIdNo;
        public string ManufactureIdNo { get { return _manufactureIdNo; } set { SetProperty(ref _manufactureIdNo, value); } }

        private string _manufactureSerialNo;
        public string ManufactureSerialNo { get { return _manufactureIdNo; } set { SetProperty(ref _manufactureIdNo, value); } }

        private string _sWExistingId;
        public string SWExistingId { get { return _sWExistingId; } set { SetProperty(ref _sWExistingId, value); } }

        private byte[] _frontPhoto;            
        public byte[] FrontPhoto { get { return _frontPhoto; } set { SetProperty(ref _frontPhoto, value); } }

        private byte[] _backPhoto;
        public byte[] BackPhoto { get { return _backPhoto; } set { SetProperty(ref _backPhoto, value); } }

        private ImageSource _frontPhotoSource;
        public ImageSource FrontPhotoSource { get { return _frontPhotoSource; } set { SetProperty(ref _frontPhotoSource, value); } }

        private ImageSource _backPhotoSource;
        public ImageSource BackPhotoSource { get { return _backPhotoSource; } set { SetProperty(ref _backPhotoSource, value); } }

        private byte[] _locationVideo;
        public byte[] LocationVideo { get { return _locationVideo; } set { SetProperty(ref _locationVideo, value); } }


        private MediaFile _locationVideoSource;
        public MediaFile LocationVideoSource { get { return _locationVideoSource; } set { SetProperty(ref _locationVideoSource, value); } }

        private string _condition;
        public string Condition { get { return _condition; } set { SetProperty(ref _condition, value); } }

        private string _recommendation;
        public string Recommendation { get { return _recommendation; } set { SetProperty(ref _recommendation, value); } }

        private string _videoFileName;
        public string VideoFileName { get { return _videoFileName; } set { SetProperty(ref _videoFileName, value); } }

        private string _videourl;
        public string VideoURL { get { return _videourl; } set { SetProperty(ref _videourl, value); } }

        private string _frontPhotoName;
        public string FrontPhotoName { get { return _frontPhotoName; } set { SetProperty(ref _frontPhotoName, value); } }

        private string _backPhotoName;
        public string BackPhotoName { get { return _backPhotoName; } set { SetProperty(ref _backPhotoName, value); } }
     
        

    }
}
