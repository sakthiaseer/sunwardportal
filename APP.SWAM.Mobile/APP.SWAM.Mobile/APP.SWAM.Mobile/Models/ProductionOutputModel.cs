﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class ProductionOutputModel : BaseViewModel
    {
        private long _productionOutputId;
        public long ProductionOutputId { get { return _productionOutputId; } set { SetProperty(ref _productionOutputId, value); } }
        private string _locationName;
        public string LocationName { get { return _locationName; } set { SetProperty(ref _locationName, value); ValidateLogin(); } }
        private long? _productionEntryId;
        public long? ProductionEntryId { get { return _productionEntryId; } set { SetProperty(ref _productionEntryId, value); } }
        private string _productionOrderNo;
        public string ProductionOrderNo { get { return _productionOrderNo; } set { SetProperty(ref _productionOrderNo, value); ValidateLogin(); } }
        public string _subLotNo;
        public string SubLotNo { get { return _subLotNo; } set { SetProperty(ref _subLotNo, value); } }
        private string _drumNo;
        public string DrumNo { get { return _drumNo; } set { SetProperty(ref _drumNo, value); } }
        private string _description;
        public string Description { get { return _description; } set { SetProperty(ref _description, value); } }
        private string _batchNo;
        public string BatchNo { get { return _batchNo; } set { SetProperty(ref _batchNo, value); ValidateLogin(); } }
        private decimal? _outputQty = 0;
        public decimal? OutputQty { get { return _outputQty; } set { SetProperty(ref _outputQty, value); } }
        private string _stroutputQty = "0";
        public string StrOutputQty { get { return _stroutputQty; } set { SetProperty(ref _stroutputQty, value); } }
        private string _buom;
        public string Buom { get { return _buom; } set { SetProperty(ref _buom, value); } }
        private bool? _isLotComplete;
        public bool? IsLotComplete { get { return _isLotComplete; } set { SetProperty(ref _isLotComplete, value); } }
        public bool? _isProdutionOrderComplete;
        public bool? IsProdutionOrderComplete { get { return _isProdutionOrderComplete; } set { SetProperty(ref _isProdutionOrderComplete, value); } }
        private long? _itemId;
        public long? ItemId { get { return _itemId; } set { SetProperty(ref _itemId, value); } }
        private string _itemNo;
        public string ItemNo { get { return _itemNo; } set { SetProperty(ref _itemNo, value); } }
        private string _loginUser;
        public string LoginUser { get { return _loginUser; } set { SetProperty(ref _loginUser, value); } }
        private DateTime _startDate;
        public DateTime StartDate { get { return _startDate; } set { SetProperty(ref _startDate, value); } }

        #region Validation 
        private bool _islocationName;
        public bool IsLocationNameError { get { return _islocationName; } set { SetProperty(ref _islocationName, value); } }

        private bool _isproductionOrderNo;
        public bool IsProductionOrderNoError { get { return _isproductionOrderNo; } set { SetProperty(ref _isproductionOrderNo, value); } }

        private bool _isBatchNoError;
        public bool IsBatchNoError { get { return _isBatchNoError; } set { SetProperty(ref _isBatchNoError, value); } }

        private void ValidateLogin()
        {
            IsLocationNameError = string.IsNullOrEmpty(LocationName);
            IsProductionOrderNoError = string.IsNullOrEmpty(ProductionOrderNo);
            IsBatchNoError = string.IsNullOrEmpty(BatchNo);
        }

        public bool IsValid()
        {
            if (IsLocationNameError || IsProductionOrderNoError || IsBatchNoError )
                return false;
            return true;
        }
        #endregion
    }
}
