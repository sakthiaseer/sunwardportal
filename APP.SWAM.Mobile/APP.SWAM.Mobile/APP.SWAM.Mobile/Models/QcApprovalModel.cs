﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class QcApprovalModel : BaseViewModel
    {
        private long _qcapprovalId;
        public long QcapprovalId { get { return _qcapprovalId; } set { SetProperty(ref _qcapprovalId, value); } }
        private string _inspectionNo;
        public string InspectionNo { get { return _inspectionNo; } set { SetProperty(ref _inspectionNo, value); } }
        private bool? _isRetest;
        public bool? IsRetest { get { return _isRetest; } set { SetProperty(ref _isRetest, value); } }

        private bool? _isSkipValidation;
        public bool? IsSkipValidation { get { return _isSkipValidation; } set { SetProperty(ref _isSkipValidation, value); } }

        private long? _testNameId;
        public long? TestNameId { get { return _testNameId; } set { SetProperty(ref _testNameId, value); ValidateLogin(); } }
        private string _testName;
        public string TestName { get { return _testName; } set { SetProperty(ref _testName, value); } }
        private string _productionOrderNo;
        public string ProductionOrderNo { get { return _productionOrderNo; } set { SetProperty(ref _productionOrderNo, value); } }
        private string _rePlanRefNo;
        public string RePlanRefNo { get { return _rePlanRefNo; } set { SetProperty(ref _rePlanRefNo, value); } }
        private int? _statusCodeID;
        public int? StatusCodeID { get { return _statusCodeID; } set { SetProperty(ref _statusCodeID, value); ValidateLogin(); } }
        private string _statusCode;
        public string StatusCode { get { return _statusCode; } set { SetProperty(ref _statusCode, value); } }
        private string _loginUser;
        public string LoginUser { get { return _loginUser; } set { SetProperty(ref _loginUser, value); } }
        private DateTime _startDate;
        public DateTime StartDate { get { return _startDate; } set { SetProperty(ref _startDate, value); } }
        private Guid? _sessionId;
        public Guid? SessionId { get { return _sessionId; } set { SetProperty(ref _sessionId, value); } }
        private List<QcApprovalLineModel> _qcApprovalLines;
        public List<QcApprovalLineModel> QcApprovalLines { get { return _qcApprovalLines; } set { SetProperty(ref _qcApprovalLines, value); } }

        #region Valldation 
        private bool _isTestCode;
        public bool IsTestCode { get { return _isTestCode; } set { SetProperty(ref _isTestCode, value); } }

        private bool _isStatusCode;
        public bool IsStatusCode { get { return _isStatusCode; } set { SetProperty(ref _isStatusCode, value); } }


        private void ValidateLogin()
        {
            IsTestCode = TestNameId == null || TestNameId == 0;
            IsStatusCode = StatusCodeID == null || StatusCodeID == 0;
        }

        public bool IsValid()
        {
            if (IsTestCode || IsStatusCode)
                return false;
            return true;
        }
        #endregion

    }
    public class QcApprovalLineModel : BaseViewModel
    {
        private long _qcapprovalLineId;
        public long QcapprovalLineId { get { return _qcapprovalLineId; } set { SetProperty(ref _qcapprovalLineId, value); } }
        private long? _qcapprovalId;
        public long? QcapprovalId { get { return _qcapprovalId; } set { SetProperty(ref _qcapprovalId, value); } }
        private string _productionOrderNo;
        public string ProductionOrderNo { get { return _productionOrderNo; } set { SetProperty(ref _productionOrderNo, value); } }
        private string _subLotNo;
        public string SubLotNo { get { return _subLotNo; } set { SetProperty(ref _subLotNo, value); } }
        private string _drumNo;
        public string DrumNo { get { return _drumNo; } set { SetProperty(ref _drumNo, value); } }
        private int? _moisturePercent;
        public int? MoisturePercent { get { return _moisturePercent; } set { SetProperty(ref _moisturePercent, value); } }

        private int _percent;
        public int Percent { get { return _percent; } set { SetProperty(ref _percent, value); } }

        private string _moistureStatus;
        public string MoistureStatus { get { return _moistureStatus; } set { SetProperty(ref _moistureStatus, value); } }
    }
}
