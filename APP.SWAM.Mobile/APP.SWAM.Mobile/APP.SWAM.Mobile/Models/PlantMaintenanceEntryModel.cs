﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class PlantMaintenanceEntryModel : BaseModel
    {
        public long PlantEntryId { get; set; }
        public long? CompanyId { get; set; }
        public long? SiteId { get; set; }
        public long? ZoneId { get; set; }
        public long? LocationId { get; set; }
        public long? AreaId { get; set; }
        public long? SpecificAreaId { get; set; }
    }
}
