﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class ICTMobileMaster
    {
        public long ICTMasterID { get; set; }
        public long? CompanyID { get; set; }
        public long? ParentICTID { get; set; }
        public int MasterType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
