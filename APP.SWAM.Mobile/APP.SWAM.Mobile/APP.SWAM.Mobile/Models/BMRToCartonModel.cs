﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class BMRToCartonModel : BaseViewModel
    {
        private string _loginUser;
        public string LoginUser { get { return _loginUser; } set { SetProperty(ref _loginUser, value); } }
        private DateTime _startDate;
        public DateTime StartDate { get { return _startDate; } set { SetProperty(ref _startDate, value); } }
        private long _bmrtoCartonId;
        public long BmrtoCartonId { get { return _bmrtoCartonId; } set { SetProperty(ref _bmrtoCartonId, value); } }
        private string _displaybox;
        public string Displaybox { get { return _displaybox; } set { SetProperty(ref _displaybox, value); ValidateLogin(); } }
        private long? _estimatedNoOfBMR;
        public long? EstimatedNoOfBMR { get { return _estimatedNoOfBMR; } set { SetProperty(ref _estimatedNoOfBMR, value); } }
        public long? LoginUserId { get; set; }
        #region Validation 
        private bool _isDisplaybox;
        public bool IsDisplaybox { get { return _isDisplaybox; } set { SetProperty(ref _isDisplaybox, value); } }

        private void ValidateLogin()
        {
            IsDisplaybox = string.IsNullOrEmpty(Displaybox);
        }

        public bool IsValid()
        {
            if (IsDisplaybox)
                return false;
            return true;
        }
        #endregion
    }

    public class BMRToCartonLineModel : BaseViewModel
    {
        private string _loginUser;
        public string LoginUser { get { return _loginUser; } set { SetProperty(ref _loginUser, value); } }
        private DateTime _startDate;
        public DateTime StartDate { get { return _startDate; } set { SetProperty(ref _startDate, value); } }
        private long _bmrtoCartonLineId;
        public long BmrtoCartonLineId { get { return _bmrtoCartonLineId; } set { SetProperty(ref _bmrtoCartonLineId, value); } }
        private long _bmrtoCartonId;
        public long BmrtoCartonId { get { return _bmrtoCartonId; } set { SetProperty(ref _bmrtoCartonId, value); } }
        private string _bmrno;
        public string Bmrno { get { return _bmrno; } set { SetProperty(ref _bmrno, value); ValidateLogin(); } }
        public long? LoginUserId { get; set; }
        private long? _estimatedNoOfBMR;
        public long? EstimatedNoOfBMR { get { return _estimatedNoOfBMR; } set { SetProperty(ref _estimatedNoOfBMR, value); } }
        #region Validation 
        private bool _isBmrNo;
        public bool IsBmrNo { get { return _isBmrNo; } set { SetProperty(ref _isBmrNo, value); } }

        private void ValidateLogin()
        {
            IsBmrNo = string.IsNullOrEmpty(Bmrno);
        }

        public bool IsValid()
        {
            if (IsBmrNo)
                return false;
            return true;
        }
        #endregion
    }
}
