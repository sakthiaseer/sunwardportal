﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Models
{
    public class ProcessTransferModel : BaseViewModel
    {
        private string _loginUser;
        public string LoginUser { get { return _loginUser; } set { SetProperty(ref _loginUser, value); } }
        private DateTime _startDate;
        public DateTime StartDate { get { return _startDate; } set { SetProperty(ref _startDate, value); } }
        private long _processTransferId;
        public long ProcessTransferId { get { return _processTransferId; } set { SetProperty(ref _processTransferId, value); } }
        private string _locationFrom;
        public string LocationFrom { get { return _locationFrom; } set { SetProperty(ref _locationFrom, value); ValidateLogin(); } }
        private string _locationTo;
        public string LocationTo { get { return _locationTo; } set { SetProperty(ref _locationTo, value); ValidateLogin(); } }
        private string _trolleyPalletNo;
        public string TrolleyPalletNo { get { return _trolleyPalletNo; } set { SetProperty(ref _trolleyPalletNo, value); ValidateLogin(); } }

        #region Validation 
        private bool _isLocationFrom;
        public bool IsLocationFrom { get { return _isLocationFrom; } set { SetProperty(ref _isLocationFrom, value); } }

        private bool _isTrolleyPalletNo;
        public bool IsTrolleyPalletNo { get { return _isTrolleyPalletNo; } set { SetProperty(ref _isTrolleyPalletNo, value); } }

        private bool _isLocationTo;
        public bool IsLocationTo { get { return _isLocationTo; } set { SetProperty(ref _isLocationTo, value); } }

        private void ValidateLogin()
        {
            IsLocationFrom = string.IsNullOrEmpty(LocationFrom);
            IsLocationTo = string.IsNullOrEmpty(LocationTo);
            IsTrolleyPalletNo = string.IsNullOrEmpty(TrolleyPalletNo);
        }

        public bool IsValid()
        {
            if (IsLocationFrom || IsTrolleyPalletNo)
                return false;
            return true;
        }
        #endregion
    }

    public class ProcessTransferLineModel : BaseViewModel
    {
        private string _loginUser;
        public string LoginUser { get { return _loginUser; } set { SetProperty(ref _loginUser, value); } }
        private DateTime _startDate;
        public DateTime StartDate { get { return _startDate; } set { SetProperty(ref _startDate, value); } }
        private long _processTransferLineId;
        public long ProcessTransferLineId { get { return _processTransferLineId; } set { SetProperty(ref _processTransferLineId, value); } }
        private long _processTransferId;
        public long ProcessTransferId { get { return _processTransferId; } set { SetProperty(ref _processTransferId, value); } }
        private string _drumInfo;
        public string DrumInfo { get { return _drumInfo; } set { SetProperty(ref _drumInfo, value); ValidateLogin(); } }
        private string _processInfo;
        public string ProcessInfo { get { return _processInfo; } set { SetProperty(ref _processInfo, value); } }
        private string _weight;
        public string Weight { get { return _weight; } set { SetProperty(ref _weight, value); } }

        #region Validation 
        private bool _isDrumInfo;
        public bool IsDrumInfo { get { return _isDrumInfo; } set { SetProperty(ref _isDrumInfo, value); } }

        private void ValidateLogin()
        {
            IsDrumInfo = string.IsNullOrEmpty(DrumInfo);
        }

        public bool IsValid()
        {
            if (IsDrumInfo || IsDrumInfo)
                return false;
            return true;
        }
        #endregion
    }
}
