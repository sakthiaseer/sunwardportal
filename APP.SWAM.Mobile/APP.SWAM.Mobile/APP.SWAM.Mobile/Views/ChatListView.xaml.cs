﻿using APP.SWAM.Mobile.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace APP.SWAM.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChatListView : ContentPage
    {
        public ChatListView()
        {
            InitializeComponent();
        }

        protected override bool OnBackButtonPressed()
        {
            return base.OnBackButtonPressed();
        }

        protected override void OnAppearing()
        {
            Settings.InSideChatUser = true;
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            Settings.InSideChatUser = false;
            Settings.CurrentChatUserId = 0;
            base.OnDisappearing();
        }
    }
}