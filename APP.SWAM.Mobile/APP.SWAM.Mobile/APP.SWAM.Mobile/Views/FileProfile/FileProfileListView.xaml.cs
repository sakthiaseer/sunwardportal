﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace APP.SWAM.Mobile.Views.FileProfile
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FileProfileListView : ContentPage
    {
        public FileProfileListView()
        {
            InitializeComponent();
            NavigationService = Locator.Instance.Resolve<INavigationService>();
            FillData();
        }
        protected readonly INavigationService NavigationService;
        private List<FPModel> OriginalProfiles = new List<FPModel>();
        private ObservableCollection<FPModel> _profiles = new ObservableCollection<FPModel>();
        public ObservableCollection<FPModel> Profiles { get { return _profiles; } set { _profiles = value; OnPropertyChanged(); } }

        public ICommand BackCommand => new Command(OnBackPressed);

        private string _searchText;
        public string SearchText { get { return _searchText; } set { _searchText = value; OnPropertyChanged(); } }

        async void FillData()
        {
            UserDialogs.Instance.ShowLoading();
            var restclient = new RestClient();
            var fileProfileTypeMobiles = await restclient.Get<FPModel>("FileProfileType/GetFileProfileTypeDetails");
            Profiles = new ObservableCollection<FPModel>(fileProfileTypeMobiles);
            OriginalProfiles = fileProfileTypeMobiles;
            applicationUserList.ItemsSource = Profiles;
            UserDialogs.Instance.HideLoading();
        }

        private async void OnBackPressed()
        {
            await NavigationService.NavigateBackAsync();
        }

        private void SearchBar_SearchButtonPressed(object sender, EventArgs e)
        {
            UsersSearch();
        }

        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            UsersSearch();
        }

        private void UsersSearch()
        {
            UserDialogs.Instance.ShowLoading();
            var restclient = new RestClient();

            var users = new List<FPModel>();
            if (!String.IsNullOrEmpty(userSearch.Text))
            {
                OriginalProfiles.ForEach(c =>
                {
                    if (c.Description.ToLower().Contains(userSearch.Text))
                    {
                        users.Add(c);
                    }
                });
                Profiles = new ObservableCollection<FPModel>(users);
            }
            else
            {
                OriginalProfiles.ForEach(c =>
                {
                    users.Add(c);
                });
                Profiles = new ObservableCollection<FPModel>(OriginalProfiles);
            }

            UserDialogs.Instance.HideLoading();

     
            applicationUserList.ItemsSource = Profiles;
        }

        private void applicationUserList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            MessagingCenter.Send((this.BindingContext as FileProfileListViewModel).SelectedProfile, "AddSelectedProfile");
        }
    }
}