﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using System.Net.Http;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Helpers;

namespace APP.SWAM.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PDFView : ContentPage
    {
        string url = "http://media.wuerth.com/stmedia/shop/catalogpages/LANG_it/1637048.pdf";
        public PDFView()
        {
            InitializeComponent();

            MessagingCenter.Subscribe<ScanDocumentModel>(this, "LoadPDF", jobModel =>
            {
                if (Settings.DBName == "LIVE")
                {
                    Device.OpenUri(new Uri(Constant.LIVEURL + jobModel.DocumentName));
                }
                else
                {
                    Device.OpenUri(new Uri(Constant.DEVURL + jobModel.DocumentName));
                }
            });
           
        }
    }
}