﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.InfiniteScroll;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace APP.SWAM.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserListView : ContentPage
    {
        private const int PageSize = 20;
        public UserListView()
        {
            InitializeComponent();
            NavigationService = Locator.Instance.Resolve<INavigationService>();
            FillData();

        }
        protected readonly INavigationService NavigationService;
        private List<User> OriginalUsers = new List<User>();
        private ObservableCollection<User> _users = new ObservableCollection<User>();
        public ObservableCollection<User> Users { get { return _users; } set { _users = value; OnPropertyChanged(); } }

        public ICommand BackCommand => new Command(OnBackPressed);

        private string _searchText;
        public string SearchText { get { return _searchText; } set { _searchText = value; OnPropertyChanged(); } }

        private User _selectedUser;
        public User SelectedUser { get { return _selectedUser; } set { _selectedUser = value; OnPropertyChanged(); } }

        async void FillData()
        {
            UserDialogs.Instance.ShowLoading();
            var restclient = new RestClient();
            var batchUsers = await restclient.Get<User>("ChatMessage/GetUsers", Settings.UserId);
            batchUsers.ForEach(c =>
            {
                User newUser = new User
                {
                    ChatUserId = c.ChatUserId,
                    UserId = c.UserId,
                    Name = c.Name,
                    ImageURL = "SWLogo.png",
                    AvatarUrl = c.ImageURL
                };
                Users.Add(newUser);
                OriginalUsers.Add(newUser);
            });

            userList.ItemsSource = Users;
            UserDialogs.Instance.HideLoading();
        }

        private async void OnBackPressed()
        {
            await NavigationService.NavigateBackAsync();
        }

        private async void UserList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            await NavigationService.NavigateToAsync(typeof(ChatListViewModel), e.Item);
        }

        private void SearchBar_SearchButtonPressed(object sender, EventArgs e)
        {
            UsersSearch();
        }

        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            UsersSearch();
        }

        private void UsersSearch()
        {
            UserDialogs.Instance.ShowLoading();
            var restclient = new RestClient();

            var users = new ObservableCollection<User>();
            if (!String.IsNullOrEmpty(userSearch.Text))
            {
                OriginalUsers.ForEach(c =>
                {
                    if (c.Name.ToLower().Contains(userSearch.Text))
                    {
                        User newUser = new User
                        {
                            ChatUserId = c.ChatUserId,
                            UserId = c.UserId,
                            Name = c.Name,
                            ImageURL = "SWLogo.png",
                            AvatarUrl = c.ImageURL
                        };
                        users.Add(newUser);
                    }
                });
            }
            else
            {
                OriginalUsers.ForEach(c =>
                {
                    User newUser = new User
                    {
                        ChatUserId = c.ChatUserId,
                        UserId = c.UserId,
                        Name = c.Name,
                        ImageURL = "SWLogo.png",
                        AvatarUrl = c.ImageURL
                    };
                    users.Add(newUser);
                });
                Users = new ObservableCollection<User>(OriginalUsers);
            }

            UserDialogs.Instance.HideLoading();

            Users = users;
            userList.ItemsSource = Users;
        }


    }
}