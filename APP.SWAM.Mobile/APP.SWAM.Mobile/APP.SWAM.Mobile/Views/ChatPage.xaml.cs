﻿using APP.SWAM.Mobile.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace APP.SWAM.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChatPage : ContentPage
    {
        public ChatPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            Settings.InSideChat = true;
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            Settings.InSideChat = false;
            base.OnDisappearing();
        }
    }
}