﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace APP.SWAM.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ApplicationUserList : ContentPage
    {
        public ApplicationUserList()
        {
            InitializeComponent();
            NavigationService = Locator.Instance.Resolve<INavigationService>();
            FillData();
        }
        protected readonly INavigationService NavigationService;
        private List<ApplicationUserModel> OriginalUsers = new List<ApplicationUserModel>();
        private ObservableCollection<ApplicationUserModel> _users = new ObservableCollection<ApplicationUserModel>();
        public ObservableCollection<ApplicationUserModel> Users { get { return _users; } set { _users = value; OnPropertyChanged(); } }

        public ICommand BackCommand => new Command(OnBackPressed);

        private string _searchText;
        public string SearchText { get { return _searchText; } set { _searchText = value; OnPropertyChanged(); } }

        async void FillData()
        {
            UserDialogs.Instance.ShowLoading();
            var restclient = new RestClient();
            var applicationUserItems = await restclient.Get<ApplicationUserModel>("ApplicationUser/GetApplicationUsers");
            Users = new ObservableCollection<ApplicationUserModel>(applicationUserItems);
            OriginalUsers = applicationUserItems;
            applicationUserList.ItemsSource = Users;
            UserDialogs.Instance.HideLoading();
        }

        private async void OnBackPressed()
        {
            await NavigationService.NavigateBackAsync();
        }

        private void SearchBar_SearchButtonPressed(object sender, EventArgs e)
        {
            UsersSearch();
        }

        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            UsersSearch();
        }

        private void UsersSearch()
        {
            UserDialogs.Instance.ShowLoading();
            var restclient = new RestClient();

            var users = new ObservableCollection<ApplicationUserModel>();
            if (!String.IsNullOrEmpty(userSearch.Text))
            {
                OriginalUsers.ForEach(c =>
                {
                    if (c.Name.ToLower().Contains(userSearch.Text))
                    {
                        users.Add(c);
                    }
                });
                Users = new ObservableCollection<ApplicationUserModel>(users);
            }
            else
            {
                OriginalUsers.ForEach(c =>
                {
                    users.Add(c);
                });
                Users = new ObservableCollection<ApplicationUserModel>(OriginalUsers);
            }

            UserDialogs.Instance.HideLoading();
            applicationUserList.ItemsSource = Users;
        }

        private void applicationUserList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            MessagingCenter.Send((this.BindingContext as ApplicationUserListViewModel).SelectedUser, "AddSelectedUser");
        }
    }
}