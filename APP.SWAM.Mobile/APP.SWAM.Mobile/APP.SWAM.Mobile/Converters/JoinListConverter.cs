﻿using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.Converters
{
    public class JoinListConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var modelList = value as IEnumerable<BaseViewModel>;
            if (modelList != null)
            {
                return string.Join(", ", modelList.Select(m => m.Title));
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
