﻿using APP.SWAM.Mobile.Models;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.Converters
{
    public class MenuItemTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var menuItemType = (MenuItemType)value;

            var platform = Device.OS == TargetPlatform.Windows;

            switch(menuItemType)
            {
                case MenuItemType.Logout:
                    return platform ? "Assets/uwp_ic_user.png" : "Logout.png";
                case MenuItemType.Menu:
                    return platform ? "Assets/uwp_ic_home.png" : "menu.png";
                case MenuItemType.Home:
                    return platform ? "Assets/uwp_ic_home.png" : "Home.png";
                case MenuItemType.Location:
                    return platform ? "Assets/uwp_ic_hotel.png" : "locationsmall.png";
                case MenuItemType.AssetItems:
                    return platform ? "Assets/uwp_ic_hotel.png" : "manufacturesmall.png";
                case MenuItemType.Ticket:
                    return platform ? "Assets/uwp_ic_hotel.png" : "ticket.png";
                case MenuItemType.DDispensing:
                    return platform ? "Assets/uwp_ic_hotel.png" : "dispensersmall.png";
                case MenuItemType.SDispensing:
                    return platform ? "Assets/uwp_ic_hotel.png" : "supervisorsmall.png";
                case MenuItemType.Transfer:
                case MenuItemType.TransferProduction:
                case MenuItemType.TransferReceive:
                    return platform ? "Assets/uwp_ic_hotel.png" : "transfersmall.png";
                case MenuItemType.Chat:
                    return platform ? "Assets/uwp_ic_hotel.png" : "Chat.png";
                case MenuItemType.About:
                    return platform ? "Assets/uwp_ic_tour.png" : "About.png";
                case MenuItemType.TransferReClass:
                    return platform ? "Assets/uwp_ic_tour.png" : "About.png";
                case MenuItemType.MaterialReclass:
                    return platform ? "Assets/uwp_ic_tour.png" : "About.png";
                default:
                    return string.Empty;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
