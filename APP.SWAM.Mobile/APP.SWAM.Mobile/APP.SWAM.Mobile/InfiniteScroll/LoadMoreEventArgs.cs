﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.InfiniteScroll
{
    public class LoadingMoreEventArgs : EventArgs
    {
        public LoadingMoreEventArgs(bool isLoadingMore)
        {
            IsLoadingMore = isLoadingMore;
        }

        public bool IsLoadingMore { get; }
    }
}