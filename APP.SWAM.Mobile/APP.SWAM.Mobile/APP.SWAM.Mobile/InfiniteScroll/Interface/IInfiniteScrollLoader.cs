﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace APP.SWAM.Mobile.InfiniteScroll
{
    public interface IInfiniteScrollLoader
    {
        bool CanLoadMore { get; }

        Task LoadMoreAsync();
    }
}
