﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.InfiniteScroll
{
    public interface IInfiniteScrollLoading
    {
        bool IsLoadingMore { get; }

        event EventHandler<LoadingMoreEventArgs> LoadingMore;
    }
}
