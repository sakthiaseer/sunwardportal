﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.InfiniteScroll
{
    public interface IInfiniteScrollDetector
    {
        bool ShouldLoadMore(object currentItem);
    }
}
