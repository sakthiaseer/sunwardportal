﻿using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Templates;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.TemplateSelector
{
    public class ChatDataTemplateSelector : DataTemplateSelector
    {
        public ChatDataTemplateSelector()
        {
            // Retain instances!
            this.incomingDataTemplate = new DataTemplate(typeof(IncomingViewCell));
            this.outgoingDataTemplate = new DataTemplate(typeof(OutgoingViewCell));
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            var messageVm = item as ChatMessageModel;
            if (messageVm == null)
                return null;
            return messageVm.IsIncoming ? this.incomingDataTemplate : this.outgoingDataTemplate;
        }

        private readonly DataTemplate incomingDataTemplate;
        private readonly DataTemplate outgoingDataTemplate;
    }
}
