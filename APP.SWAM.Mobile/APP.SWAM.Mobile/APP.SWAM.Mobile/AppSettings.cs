﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile
{
    public static class AppSettings
    {
        // Endpoints
        private const string DefaultImagesBaseUri = "YOUR_IMAGE_BASE_URI";
       
        private static ISettings Settings => CrossSettings.Current;

        public static string ImagesBaseUri
        {
            get => Settings.GetValueOrDefault(nameof(ImagesBaseUri), DefaultImagesBaseUri);

            set => Settings.AddOrUpdateValue(nameof(ImagesBaseUri), value);
        }

        public static User User
        {
            get => Settings.GetValueOrDefault(nameof(User), default(User));

            set => Settings.AddOrUpdateValue(nameof(User), value);
        }

        public static void RemoveUserData()
        {
            Settings.Remove(nameof(User));
        }
    }
}
