﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Enum
{
    public enum ConnectionState
    {
        Disconnected,
        Connecting,
        Connected,
        Disconnecting
    }
}
