﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.ViewModels;
using Matcha.BackgroundService;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace APP.SWAM.Mobile.Services.BackgroundService
{
    public class RefreshOnlineUserPeriodically : IPeriodicTask
    {

        public RefreshOnlineUserPeriodically(int minutes, ChatViewModel chatViewModel)
        {
            Interval = TimeSpan.FromMinutes(minutes);
            ChatViewModel = chatViewModel;
        }

        public TimeSpan Interval { get; set; }

        public ChatViewModel ChatViewModel { get; set; }

        public async Task<bool> StartJob()
        {
            try
            {
                await ChatViewModel.RefreshChatUsers();
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.Alert("Exception Message" + ex.Message, "Exception", "Ok");
                return false;
            }
            return true;

        }
    }
}
