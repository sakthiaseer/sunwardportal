﻿using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using Matcha.BackgroundService;
using Plugin.LocalNotifications;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace APP.SWAM.Mobile.Services.BackgroundService
{
    public abstract class BaseBackgroundService : IPeriodicTask
    {
        public BaseBackgroundService(int minutes, ChatMessageModel message)
        {
            Interval = TimeSpan.FromSeconds(minutes);
            Message = message;
        }

        public TimeSpan Interval { get; set; }

        public ChatMessageModel Message { get; set; }

        public async Task<bool> StartJob()
        {
            try
            {
                if (Settings.IsLoggedIn)
                {
                    long activeChatUserId = 0;
                    if (Settings.InSideChatUser)
                    {
                        activeChatUserId = Settings.CurrentChatUserId;
                    }
                    RestClient restClient = new RestClient();
                    var chatMessageModels = await restClient.GetByIds<ChatMessageModel>("ChatMessage/GetUserNotificationMessages", Settings.UserId, activeChatUserId);
                    chatMessageModels.ForEach(m =>
                    {
                        CrossLocalNotifications.Current.Show(m.SenderName, m.Message, (int)m.SentUserId, DateTime.Now.AddSeconds(5));
                    });
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

            return true;
        }
    }
}
