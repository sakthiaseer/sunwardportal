﻿using APP.SWAM.Mobile.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Services.BackgroundService
{
    public class PushNotificationPeriodicService : BaseBackgroundService
    {
        public PushNotificationPeriodicService(int minutes, ChatMessageModel message) : base(minutes, message)
        {

        }
    }
}
