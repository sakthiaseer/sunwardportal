﻿using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.ViewModel;
using APP.SWAM.Mobile.ViewModel.Base;
using APP.SWAM.Mobile.ViewModels;
using APP.SWAM.Mobile.Views;
using APP.SWAM.Mobile.Views.FileProfile;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.Services
{
    public partial class NavigationService : INavigationService
    {
        // private readonly IAuthenticationService _authenticationService;
        protected readonly Dictionary<Type, Type> _mappings;

        protected Application CurrentApplication
        {
            get { return Application.Current; }
        }

        public NavigationService()
        {
            // _authenticationService = authenticationService;
            _mappings = new Dictionary<Type, Type>();

            CreatePageViewModelMappings();
        }

        public async Task InitializeAsync()
        {
            if (string.IsNullOrEmpty(Settings.RestServiceURL))
            {
                await NavigateToAsync<SetupViewModel>();
            }
            //else if (!Settings.IsLoggedIn)
            //{
            //    await NavigateToAsync<LoginViewModel>();
            //}
            else
            {
                Constant.RestUrl = Settings.RestServiceURL;
                await NavigateToAsync<LoginViewModel>();
            }
        }

        public Task NavigateToAsync<TViewModel>() where TViewModel : ViewModelBase
        {
            return InternalNavigateToAsync(typeof(TViewModel), null);
        }

        public Task NavigateToAsync<TViewModel>(object parameter) where TViewModel : ViewModelBase
        {
            return InternalNavigateToAsync(typeof(TViewModel), parameter);
        }

        public Task NavigateToAsync(Type viewModelType)
        {
            return InternalNavigateToAsync(viewModelType, null);
        }

        public Task NavigateToAsync(Type viewModelType, object parameter)
        {
            return InternalNavigateToAsync(viewModelType, parameter);
        }

        public async Task NavigateBackAsync()
        {
            if (CurrentApplication.MainPage is MainView)
            {
                var mainPage = CurrentApplication.MainPage as MainView;
                await mainPage.Detail.Navigation.PopAsync();
            }
            else if (CurrentApplication.MainPage != null)
            {
                await CurrentApplication.MainPage.Navigation.PopAsync();
            }
        }

        public virtual Task RemoveLastFromBackStackAsync()
        {
            var mainPage = CurrentApplication.MainPage as MainView;

            if (mainPage != null)
            {
                mainPage.Detail.Navigation.RemovePage(
                    mainPage.Detail.Navigation.NavigationStack[mainPage.Detail.Navigation.NavigationStack.Count - 2]);
            }

            return Task.FromResult(true);
        }

        protected virtual async Task InternalNavigateToAsync(Type viewModelType, object parameter)
        {
            Page page = CreateAndBindPage(viewModelType, parameter);

            if (page is MainView)
            {
                CurrentApplication.MainPage = page;
            }
            else if (page is LoginView)
            {
                CurrentApplication.MainPage = new CustomNavigationPage(page);
            }
            else if (CurrentApplication.MainPage is MainView)
            {

                var mainPage = CurrentApplication.MainPage as MainView;
                var navigationPage = mainPage.Detail as CustomNavigationPage;
                if (parameter is Models.MenuItem)
                {
                    navigationPage = null;
                }
                if (navigationPage != null && parameter == null)
                {
                    navigationPage = new CustomNavigationPage(page);
                    mainPage.Detail = navigationPage;
                }
                if (navigationPage != null)
                {
                    var currentPage = navigationPage.CurrentPage;

                    if (currentPage.GetType() != page.GetType())
                    {
                        await navigationPage.PushAsync(page);
                    }
                }
                else
                {
                    navigationPage = new CustomNavigationPage(page);
                    mainPage.Detail = navigationPage;
                }

                mainPage.IsPresented = false;
            }
            else
            {
                var navigationPage = CurrentApplication.MainPage as CustomNavigationPage;

                if (navigationPage != null)
                {
                    await navigationPage.PushAsync(page);
                }
                else
                {
                    CurrentApplication.MainPage = new CustomNavigationPage(page);
                }
            }

            await (page.BindingContext as ViewModelBase).InitializeAsync(parameter);
        }

        protected Type GetPageTypeForViewModel(Type viewModelType)
        {
            if (!_mappings.ContainsKey(viewModelType))
            {
                throw new KeyNotFoundException($"No map for ${viewModelType} was found on navigation mappings");
            }

            return _mappings[viewModelType];
        }

        protected Page CreateAndBindPage(Type viewModelType, object parameter)
        {
            Type pageType = GetPageTypeForViewModel(viewModelType);

            if (pageType == null)
            {
                throw new Exception($"Mapping type for {viewModelType} is not a page");
            }

            Page page = Activator.CreateInstance(pageType) as Page;
            ViewModelBase viewModel = Locator.Instance.Resolve(viewModelType) as ViewModelBase;
            page.BindingContext = viewModel;

            return page;
        }

        private void CreatePageViewModelMappings()
        {
            _mappings.Add(typeof(LoginViewModel), typeof(LoginView));
            _mappings.Add(typeof(ExtendedSplashViewModel), typeof(ExtendedSplashView));
            _mappings.Add(typeof(MainViewModel), typeof(MainView));
            _mappings.Add(typeof(HomeViewModel), typeof(HomeView));
            _mappings.Add(typeof(AssetLocationViewModel), typeof(AssetLocationView));
            _mappings.Add(typeof(AboutViewModel), typeof(AboutPage));
            _mappings.Add(typeof(AssetItemsViewModel), typeof(AssetItems));
            _mappings.Add(typeof(AssetItemViewModel), typeof(AssetItem));
            _mappings.Add(typeof(PlaywebVideoViewModel), typeof(PlayWebVideoPage));
            _mappings.Add(typeof(ProductionEntryViewModel), typeof(ProductionEntry));
            _mappings.Add(typeof(ProductionOutputViewmodel), typeof(ProductionOutput));
            _mappings.Add(typeof(QcApprovalViewmodel), typeof(QcApproval));
            _mappings.Add(typeof(QcApprovalLineItemViewModel), typeof(QcApprovalLineItem));
            _mappings.Add(typeof(StartOfDayViewModel), typeof(StartOfDay));
            _mappings.Add(typeof(ReassignmentJobViewModel), typeof(ReassignmentJobView));
            _mappings.Add(typeof(ProcessTransferViewModel), typeof(ProcessTransfer));
            _mappings.Add(typeof(ProcessTransferLineViewModel), typeof(ProcessTransferLines));
            _mappings.Add(typeof(IPIRListItemViewModel), typeof(IPIRItemView));
            _mappings.Add(typeof(IPIRListViewModel), typeof(IPIRListView));
            _mappings.Add(typeof(CreateIPIRListViewModel), typeof(CreateIPIRListView));
            _mappings.Add(typeof(CreateIPIRItemViewModel), typeof(CreateIPIRItemView));
            _mappings.Add(typeof(MultiselectionViewModel), typeof(MultiSelectList));
            _mappings.Add(typeof(ApplicationUserListViewModel), typeof(ApplicationUserList));
            _mappings.Add(typeof(ProcessTransferItemViewModel), typeof(ProcessTransferItem));
            _mappings.Add(typeof(BMRToCartonViewModel), typeof(BMRToCartonPage));
            _mappings.Add(typeof(BMRToCartonLineViewModel), typeof(BMRToCartonLinePage));
            _mappings.Add(typeof(BMRToCartonItemViewModel), typeof(BMRToCartonItemPage));
            _mappings.Add(typeof(ConsumptionEntryViewModel), typeof(ConsumptionEntryPage));
            _mappings.Add(typeof(ConsumptionItemViewModel), typeof(ConsumptionItemsPage));
            _mappings.Add(typeof(DrummingViewModel), typeof(DrummingPage));
            _mappings.Add(typeof(SupervisorDispensingViewModel), typeof(SupervisorDispensingPage));
            _mappings.Add(typeof(SupervisorDispensingItemViewModel), typeof(SupervisorDispensingItem));
            _mappings.Add(typeof(SupervisorDispensingLineViewModel), typeof(SupervisorDispensingLines));
            _mappings.Add(typeof(DispensorDispensingViewModel), typeof(DispensorDispensingPage));
            _mappings.Add(typeof(DispensorDispensingLinesViewModel), typeof(DispensorDispensingLines));
            _mappings.Add(typeof(DispensorDispensingItemViewModel), typeof(DispensorDispensingItem));
            _mappings.Add(typeof(ChatViewModel), typeof(ChatPage));
            _mappings.Add(typeof(ChatListViewModel), typeof(ChatListView));
            //_mappings.Add(typeof(ChatMainViewModel), typeof(ChatMainView));
            _mappings.Add(typeof(CameraViewModel), typeof(Camera));
            _mappings.Add(typeof(UserListViewModel), typeof(UserListView));
            _mappings.Add(typeof(SetupViewModel), typeof(SetUpPage));
            _mappings.Add(typeof(ScanListViewModel), typeof(ScanListView));
            _mappings.Add(typeof(ScannedImagesViewModel), typeof(ScannedImagesView));
            _mappings.Add(typeof(ProductionScanDocumentViewModel), typeof(ProductionScanDocumentView));
            _mappings.Add(typeof(FileProfileListViewModel), typeof(FileProfileListView));
            _mappings.Add(typeof(ProductionDocumentListViewModel), typeof(ProductionDocumentList));
            _mappings.Add(typeof(PDFViewerViewModel), typeof(PDFView));
            _mappings.Add(typeof(TransferReclassEntryViewModel), typeof(TransferReclassEntryPage));
            _mappings.Add(typeof(TransferReclassLineViewModel), typeof(TransferReclassLinePage));
            _mappings.Add(typeof(TransferReclassItemViewModel), typeof(TransferReclassItemPage));
            _mappings.Add(typeof(MaterialReturnViewModel), typeof(MaterialRetunPage));
            _mappings.Add(typeof(DispensorScanViewModel), typeof(DispensorScanDrumPage));
            _mappings.Add(typeof(TransferProductionViewModel), typeof(TransferProduction));
            _mappings.Add(typeof(TransferProductionItemViewModel), typeof(TransferProductionItemView));
            _mappings.Add(typeof(TransferProductionLineViewModel), typeof(TransferProductionLineView));
            _mappings.Add(typeof(TransferReceiveViewModel), typeof(TransferReceive));
            _mappings.Add(typeof(TransferReceiveItemViewModel), typeof(TransferReceiveItem));
            _mappings.Add(typeof(TransferReceiveLineViewModel), typeof(TransferReceiveLine));

        }
    }
}
