﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.ViewModel.Base;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace APP.SWAM.Mobile.Services
{
    public class RestClient
    {
        protected readonly IDialogService DialogService;
        public RestClient()
        {
            DialogService = Locator.Instance.Resolve<IDialogService>();
        }

        private HttpClient InitClientHandler()
        {
            var httpHandler = new HttpClientHandler
            {
                AutomaticDecompression = System.Net.DecompressionMethods.GZip | System.Net.DecompressionMethods.Deflate
            };

            return InitHttpClient(new HttpClient(httpHandler));

        }

        private HttpClient InitHttpClient(HttpClient client)
        {
            client.DefaultRequestHeaders.Remove("Accept-Encoding");
            client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip");
            return client;
        }

        public async Task<List<T>> Get<T>(string method)
        {
            try
            {
                using (var client = InitClientHandler())
                {
                    client.BaseAddress = new Uri(Constant.RestUrl);
                    var response = await client.GetAsync(method);
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (result != "")
                    {
                        var Items = JsonConvert.DeserializeObject<List<T>>(result);
                        return Items;
                    }
                }


                return null;

            }
            catch (Exception e)
            {
                UserDialogs.Instance.Toast("Connectivity Error", new TimeSpan(5));
                Debug.WriteLine("Method Name :" + method + "Exception Details :" + e.Message);
                return null;
            }
        }
        public async Task<List<T>> Get<T>(string method, long Id)
        {
            try
            {
                using (var client = InitClientHandler())
                {
                    client.Timeout = TimeSpan.FromMinutes(Constant.Timeout);
                    client.BaseAddress = new Uri(Constant.RestUrl);
                    var response = await client.GetAsync(method + "?Id=" + Id);
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (result != "")
                    {
                        var Items = JsonConvert.DeserializeObject<List<T>>(result);
                        return Items;
                    }

                }

                return null;
            }
            catch (Exception e)
            {
                UserDialogs.Instance.Toast("Connectivity Error", new TimeSpan(5));
                Debug.WriteLine("Method Name :" + method + "Exception Details :" + e.Message);
                return null;
            }
        }
        public async Task<List<T>> Get<T>(string method, string param)
        {
            try
            {
                using (var client = InitClientHandler())
                {
                    client.Timeout = TimeSpan.FromMinutes(Constant.Timeout);
                    client.BaseAddress = new Uri(Constant.RestUrl);
                    var response = await client.GetAsync(method + "?Id=" + param);
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (result != "")
                    {
                        var Items = JsonConvert.DeserializeObject<List<T>>(result);
                        return Items;
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                UserDialogs.Instance.Toast("Connectivity Error", new TimeSpan(5));
                Debug.WriteLine("Method Name :" + method + "Exception Details :" + e.Message);
                return null;
            }
        }

        public async Task<List<T>> GetByIds<T>(string method, long Id, long itemId)
        {
            try
            {
                using (var client = InitClientHandler())
                {
                    client.Timeout = TimeSpan.FromMinutes(Constant.Timeout);
                    client.BaseAddress = new Uri(Constant.RestUrl);
                    var response = await client.GetAsync(method + "?Id=" + Id + "&&itemId=" + itemId);
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (result != "")
                    {
                        var Items = JsonConvert.DeserializeObject<List<T>>(result);
                        return Items;
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                UserDialogs.Instance.Toast("Connectivity Error", new TimeSpan(5));
                Debug.WriteLine("Method Name :" + method + "Exception Details :" + e.Message);
                return null;
            }
        }

        public async Task<List<T>> GetApplicationMasterByType<T>(string method, long Id)
        {
            try
            {
                using (var client = InitClientHandler())
                {
                    client.Timeout = TimeSpan.FromMinutes(Constant.Timeout);
                    client.BaseAddress = new Uri(Constant.RestUrl);
                    var response = await client.GetAsync(method + "?type=" + Id);
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (result != "")
                    {
                        var Items = JsonConvert.DeserializeObject<List<T>>(result);
                        return Items;
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                UserDialogs.Instance.Toast("Connectivity Error", new TimeSpan(5));
                Debug.WriteLine("Method Name :" + method + "Exception Details :" + e.Message);
                return null;
            }
        }

        public async Task<List<T>> GetCodeMasterByType<T>(string method, string type)
        {
            try
            {
                using (var client = InitClientHandler())
                {
                    client.Timeout = TimeSpan.FromMinutes(Constant.Timeout);
                    client.BaseAddress = new Uri(Constant.RestUrl);
                    var response = await client.GetAsync(method + "?type=" + type);
                    var result = response.Content.ReadAsStringAsync().Result;
                    if (result != "")
                    {
                        var Items = JsonConvert.DeserializeObject<List<T>>(result);
                        return Items;
                    }

                    return null;
                }
            }
            catch (Exception e)
            {
                UserDialogs.Instance.Toast("Connectivity Error", new TimeSpan(5));
                Debug.WriteLine("Method Name :" + method + "Exception Details :" + e.Message);
                return null;
            }
        }

        public async Task<T> GetItem<T>(string method, long Id)
        {
            try
            {
                using (var client = InitClientHandler())
                {
                    client.BaseAddress = new Uri(Constant.RestUrl);
                    var response = await client.GetAsync(method + "?Id=" + Id);
                    var result = response.Content.ReadAsStringAsync().Result;
                    var Item = default(T);
                    if (result != "")
                    {
                        Item = JsonConvert.DeserializeObject<T>(result);
                        return Item;
                    }
                    return Item;
                }
            }
            catch (Exception e)
            {
                UserDialogs.Instance.Toast("Connectivity Error", new TimeSpan(5));
                Debug.WriteLine("Method Name :" + method + "Exception Details :" + e.Message);
                return default(T);
            }
        }

        public async Task<T> GetItemByNo<T>(string method, string no)
        {
            try
            {
                using (var client = InitClientHandler())
                {
                    client.BaseAddress = new Uri(Constant.RestUrl);
                    var response = await client.GetAsync(method + "?No=" + no);
                    var result = response.Content.ReadAsStringAsync().Result;
                    var Item = default(T);
                    if (result != "")
                    {
                        Item = JsonConvert.DeserializeObject<T>(result);
                        return Item;
                    }
                    return Item;
                }
            }
            catch (Exception e)
            {
                UserDialogs.Instance.Toast("Connectivity Error", new TimeSpan(5));
                Debug.WriteLine("Method Name :" + method + "Exception Details :" + e.Message);
                return default(T);
            }
        }

        public async Task<T> PostAsync<T>(string method, T item)
        {
            try
            {
                using (var client = InitClientHandler())
                {
                    client.Timeout = TimeSpan.FromMinutes(Constant.Timeout);
                    client.BaseAddress = new Uri(Constant.RestUrl);
                    var json = JsonConvert.SerializeObject(item);
                    var content = new StringContent(json, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = null;
                    response = await client.PostAsync(method, content);

                    var Item = default(T);
                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        if (result != "")
                        {
                            Item = JsonConvert.DeserializeObject<T>(result);
                            return Item;
                        }
                    }
                    return Item;
                }
            }
            catch (Exception e)
            {
                await DialogService.ShowAlertAsync(e.Message, "Exception", "Ok");
                return default(T);
            }
        }
        public async Task<List<T>> PostAllAsync<T>(string method, List<T> item)
        {
            try
            {

                using (var client = InitClientHandler())
                {
                    client.Timeout = TimeSpan.FromMinutes(Constant.Timeout);
                    client.BaseAddress = new Uri(Constant.RestUrl);
                    var json = JsonConvert.SerializeObject(item);
                    var content = new StringContent(json, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = null;
                    response = await client.PostAsync(method, content);

                    var Item = default(List<T>);
                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        if (result != "")
                        {
                            Item = JsonConvert.DeserializeObject<List<T>>(result);
                            return Item;
                        }
                    }
                    return Item;
                }
            }
            catch (Exception e)
            {
                UserDialogs.Instance.Toast("Connectivity Error", new TimeSpan(5));
                Debug.WriteLine("Method Name :" + method + "Exception Details :" + e.Message);
                return default(List<T>);
            }
        }

        public async Task<ApplicationUserModel> Login(string method, ApplicationUserModel item)
        {
            try
            {
                using (var client = InitClientHandler())
                {
                    client.BaseAddress = new Uri(Constant.RestUrl);
                    var json = JsonConvert.SerializeObject(item);
                    var content = new StringContent(json, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = null;
                    response = await client.PostAsync(method, content);
                    var result = response.Content.ReadAsStringAsync().Result;
                    var Item = default(ApplicationUserModel);
                    if (result != "")
                    {
                        UserModel userModel = JsonConvert.DeserializeObject<UserModel>(result);
                        Item = new ApplicationUserModel { UserID = userModel.userId, UserName = userModel.userName, LoginID = userModel.LoginID, LoginPassword = userModel.LoginPassword };
                        return Item;
                    }
                    return item;
                }
            }
            catch (Exception e)
            {
                UserDialogs.Instance.Toast("Connectivity Error", new TimeSpan(5));
                Debug.WriteLine("Method Name :" + method + "Exception Details :" + e.Message);
                return null;
            }
        }
        public async Task<bool> PostMediaAsync<T>(string method, T item)
        {
            using (var stream = new MemoryStream())
            using (var bson = new BsonWriter(stream))
            {
                var jsonSerializer = new JsonSerializer();



                jsonSerializer.Serialize(bson, item);

                using (var client = new HttpClient
                {
                    BaseAddress = new Uri(Constant.RestUrl)
                })
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                            new MediaTypeWithQualityHeaderValue("application/bson"));

                    var byteArrayContent = new ByteArrayContent(stream.ToArray());
                    byteArrayContent.Headers.ContentType = new MediaTypeHeaderValue("application/bson");

                    var result = await client.PostAsync(
                            method, byteArrayContent);

                    return result.EnsureSuccessStatusCode().IsSuccessStatusCode;
                }
            }
        }
        public async Task<T> PutAsync<T>(string method, T item)
        {
            try
            {
                using (var client = InitClientHandler())
                {
                    client.Timeout = TimeSpan.FromMinutes(Constant.Timeout);
                    client.BaseAddress = new Uri(Constant.RestUrl);
                    //var json = JsonConvert.SerializeObject(item);
                    //var content = new StringContent(json, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = null;
                    response = await client.PutAsync(method, new JsonContent(item));

                    var Item = default(T);
                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        if (result != "")
                        {
                            Item = JsonConvert.DeserializeObject<T>(result);
                            return Item;
                        }
                    }
                    return Item;
                }
            }
            catch (Exception e)
            {
                UserDialogs.Instance.Toast("Connectivity Error", new TimeSpan(5));
                Debug.WriteLine("Method Name :" + method + "Exception Details :" + e.Message);
                return default(T);
            }
        }
        public async Task<bool> DeleteAsync<T>(string method, long Id)
        {
            try
            {
                using (var client = InitClientHandler())
                {
                    client.BaseAddress = new Uri(Constant.RestUrl);
                    HttpResponseMessage response = null;
                    response = await client.DeleteAsync(method + "?Id=" + Id);

                    return response.IsSuccessStatusCode;
                }
            }
            catch (Exception e)
            {
                UserDialogs.Instance.Toast("Connectivity Error", new TimeSpan(5));
                Debug.WriteLine("Method Name :" + method + "Exception Details :" + e.Message);
                return false;
            }
        }

    }


    public class UserModel
    {
        public int userId { get; set; }
        public string userName { get; set; }
        public string LoginID { get; set; }
        public string LoginPassword { get; set; }
    }
}
