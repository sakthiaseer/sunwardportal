﻿using APP.SWAM.Mobile.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace APP.SWAM.Mobile.Services.ChatService
{
    public interface IChatService
    {
        Task Connect();
        Task Send(ChatMessageModel message);
        Task JoinRoom(string roomName);
        Task Disconnect();
    }
}
