﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.ViewModels.Base;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.Services.ChatService
{
    public class ChatService : IChatService, INotifyPropertyChanged
    {
        private HubConnection Connection;

        private ChatHubSingleton chatHubSingleton;

        public ChatService()
        {
            Connection = ChatHubSingleton.GetChatHubInstance().Connection;
        }


        public async Task Connect()
        {
            try
            {
                bool isConnectionFailed = false;

                await Connection.StartAsync().ContinueWith(task =>
                {
                    if (task.IsFaulted)
                    {
                        isConnectionFailed = true;
                        UserDialogs.Instance.Toast("Connectivity Error", TimeSpan.FromSeconds(5));
                    }
                });

                if (!isConnectionFailed)
                {
                    string connectionGuid = string.Empty;
                    Connection.On<string>("UserConnected", async (connectionId) =>
                    {
                        ChatHubSingleton.GetChatHubInstance().ConnectionID = connectionId;
                        await InsertOnlineUser();
                    });

                    Connection.On("ReceiveMessage", (ChatMessageModel message) =>
                    {
                        MessagingCenter.Send(this, "AddMessages", message);
                    });
                }
            }

            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task InsertOnlineUser()
        {
            await Connection.SendAsync("InsertOnlineUserAsync", new User { UserId = Settings.UserId, Name = Constant.Username, ConnectionId = ChatHubSingleton.GetChatHubInstance().ConnectionID });
        }

        public string ConnectionGUID { get; set; }

        public List<User> ActiveUsers { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public async Task Send(ChatMessageModel message)
        {
            try
            {
                await Connection.SendAsync("SendMessageToUser", message);
            }
            catch (Exception ex)
            {
                if (Connection.State == HubConnectionState.Disconnected)
                {
                    UserDialogs.Instance.Toast("Connectivity Error", TimeSpan.FromSeconds(5));
                    return;
                }
            }
        }

        public async Task JoinRoom(string roomName)
        {
            await Connection.SendAsync("JoinGroup", roomName);
        }

        public async Task Disconnect()
        {
            await Connection.StopAsync();
        }
    }
}
