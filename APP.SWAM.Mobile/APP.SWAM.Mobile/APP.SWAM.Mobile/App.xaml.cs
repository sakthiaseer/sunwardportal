﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using APP.SWAM.Mobile.Views;
using System.Threading.Tasks;
using APP.SWAM.Mobile.ViewModel.Base;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel;
using APP.SWAM.Mobile.Services.BackgroundService;
using APP.SWAM.Mobile.Models;
using Matcha.BackgroundService;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace APP.SWAM.Mobile
{
    public partial class App : Application
    {

        static App()
        {
            BuildDependencies();
        }

        public App()
        {
            InitializeComponent();
            XF.Material.Forms.Material.Init(this);
            InitNavigation();
        }


        public static void BuildDependencies()
        {
            Locator.Instance.Build();
        }

        private Task InitNavigation()
        {
            var navigationService = Locator.Instance.Resolve<INavigationService>();
            return navigationService.NavigateToAsync<ExtendedSplashViewModel>();
        }

        protected override void OnStart()
        {
            StartBackgroundService();
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        private static void StartBackgroundService()
        {
            //Rss gets updated every 3 minutes
          //  BackgroundAggregatorService.Add(() => new PushNotificationPeriodicService(30, new ChatMessageModel()));

            //you now running the periodic task in the background
           // BackgroundAggregatorService.StartBackgroundService();
        }
    }
}
