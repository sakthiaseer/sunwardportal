﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.InfiniteScroll;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.Services.BackgroundService;
using APP.SWAM.Mobile.ViewModel.Base;
using APP.SWAM.Mobile.Views;
using Matcha.BackgroundService;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class ChatViewModel : ViewModelBase
    {
        private const int PageSize = 20;
        private List<User> OriginalUsers = new List<User>();
        private ObservableCollection<User> _users = new ObservableCollection<User>();
        public ObservableCollection<User> Users { get { return _users; } set { _users = value; OnPropertyChanged(); } }

        public Command LoadItemsCommand { get; set; }

        public ICommand ChatItemSelectedCommand => new Command<Models.User>(OnChatItemSelected);

        private ICommand openUserListCommand;
        public ICommand OpenUserListCommand
        {
            get
            {
                return openUserListCommand ??
                (openUserListCommand = new Command(OnOpenUserList));
            }
        }

        private ICommand userSearchCommand;
        public ICommand UserSearchCommand
        {
            get
            {
                return userSearchCommand ??
                (userSearchCommand = new Command(OnUserSearch));
            }
        }

        private ICommand clearCommand;
        public ICommand ClearCommand
        {
            get
            {
                return clearCommand ??
                (clearCommand = new Command(OnClearSearch));
            }
        }


        public static readonly BindableProperty IsWorkingProperty =
          BindableProperty.Create(nameof(IsWorking), typeof(bool), typeof(ChatViewModel), default(bool));

        public bool IsWorking
        {
            get { return (bool)GetValue(IsWorkingProperty); }
            set { SetValue(IsWorkingProperty, value); }
        }



        private string _searchText;
        public string SearchText { get { return _searchText; } set { _searchText = value; OnPropertyChanged(); } }

        private int _itemsCount;
        public int ItemsCount { get { return _itemsCount; } set { _itemsCount = value; OnPropertyChanged(); } }


        public ChatViewModel()
        {
            Device.StartTimer(TimeSpan.FromSeconds(30), RefreshOnlineUsersByTimer);
        }


        public async Task RefreshChatUsers()
        {
            UserDialogs.Instance.ShowLoading();
            var restclient = new RestClient();
            var users = await restclient.Get<User>("ChatMessage/GetChatUsers");
            users.ForEach(u =>
            {
                u.ImageURL = "SWLogo.png";
            });
            Users = new ObservableCollection<User>(users.Where(u => u.UserId != Settings.UserId).ToList());
            UserDialogs.Instance.HideLoading();
        }

        public async Task RefreshBackgroundChatUsers()
        {
            var restclient = new RestClient();
            var users = await restclient.Get<User>("ChatMessage/GetChatUsers");
            users.ForEach(u =>
            {
                u.ImageURL = "SWLogo.png";
            });
            Users = new ObservableCollection<User>(users.Where(u => u.UserId != Settings.UserId).ToList());
        }

        private bool RefreshOnlineUsersByTimer()
        {
            Device.BeginInvokeOnMainThread(async () => { await RefreshBackgroundChatUsers(); });
            return true;
        }

        public override async Task InitializeAsync(object navigationData)
        {
            await RefreshChatUsers();
        }

        private async void OnChatItemSelected(Models.User item)
        {
            if (IsConnected)
            {
                await NavigationService.NavigateToAsync(typeof(ChatListViewModel), item);
            }
            else
            {
                ShowConnectivityError();
            }
        }

        private async void OnOpenUserList()
        {
            if (IsConnected)
            {
                await NavigationService.NavigateToAsync(typeof(UserListViewModel), this);
            }
            else
            {
                ShowConnectivityError();
            }
        }
            
        private void OnUserSearch()
        {
            Users = new InfiniteScrollCollection<User>(Users.Where(u => u.Name.ToLower().Contains(SearchText.ToLower())).ToList());
        }

        private void OnClearSearch()
        {
            if (String.IsNullOrEmpty(SearchText))
            {
                Users = new InfiniteScrollCollection<User>(OriginalUsers);
            }
            else
            {
                Users = new InfiniteScrollCollection<User>(OriginalUsers);
                Users = new InfiniteScrollCollection<User>(Users.Where(u => u.Name.ToLower().Contains(SearchText.ToLower())).ToList());
            }
        }
    }
}
