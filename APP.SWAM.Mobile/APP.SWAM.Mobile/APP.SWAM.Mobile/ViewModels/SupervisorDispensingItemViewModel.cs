﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class SupervisorDispensingItemViewModel : ViewModelBase
    {
        #region fields
       

        private SupervisorDispensingLineViewModel _parentViewModel;
        private SupervisorDispensingModel _model;
        
        private bool isRefreshing;
        private long _drummingId;
        #endregion

        #region Properties

        public SupervisorDispensingLineViewModel ParentViewModel
        {
            get { return _parentViewModel; }
            set { _parentViewModel = value; OnPropertyChanged(nameof(ParentViewModel)); }
        }

        public SupervisorDispensingModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }

        public long SupervisorDispensingId
        {
            get { return _drummingId; }
            set { _drummingId = value; OnPropertyChanged(nameof(SupervisorDispensingId)); }
        }


        public Command SaveCommand { get; set; }
        public Command ClearCommand { get; set; }
        public ICommand FocusedCommand { get; set; }

        #endregion

        public SupervisorDispensingItemViewModel()
        {
            SaveCommand = new Command(OnSave);
            ClearCommand = new Command(OnExit);
            FocusedCommand = new Command(OnProdScan);
            Model = new SupervisorDispensingModel
            {
                LoginUser = Constant.Username,
                StartDate = DateTime.Now,
            };
        }

        public override Task InitializeAsync(object navigationData)
        {
            ParentViewModel = (navigationData as SupervisorDispensingLineViewModel);
            Model = ParentViewModel.Model;
            return base.InitializeAsync(navigationData);
        }

        void OnProdScan()
        {
#if DEBUG
            string qrCode = "JOB-0001~RM19-M0258~MS18-1020~QCREF001~25~DRUM001";

            var barcode = qrCode.Split('~');
            if (barcode.Length > 3)
            {
                Model.JobNo = barcode[0].Trim();
                Model.SubLotNo = barcode[1].Trim();
                Model.ProdOrderNo = barcode[2].Trim();
                Model.QcrefNo = barcode[3].Trim();
                Model.DrumWeight = barcode[4].Trim();
                Model.DrumNo = barcode[5].Trim();
                Model.UOM = "KG";
            }
#endif
        }
        async void OnSave()
        {
            if (Model.IsValid())
            {
                IsBusy = true;
                IsEnabled = false;

                UserDialogs.Instance.ShowLoading("Validating Dispensing...");
                Model.AddedByUserId = Settings.UserId;
                var restclient = new RestClient();
                Model = await restclient.PostAsync<SupervisorDispensingModel>("APPSupervisorDispensingEntry/InsertSupervisorDispensing", Model);

                IsBusy = false;
                IsEnabled = true;
                UserDialogs.Instance.HideLoading();
                if (Model.SupervisorDispensingId > 0)
                {
                    await DialogService.ShowConfirmAsync("Record validated successfully.", "Validation", "OK", "Cancel");
                    ParentViewModel.FillData();
                    await NavigationService.NavigateBackAsync();
                }
                else
                {
                    await DialogService.ShowConfirmAsync("Validation failed.Please select correct drum", "Validation", "OK", "Cancel");
                }
               
            }
            else
            {
                await DialogService.ShowConfirmAsync("Validation Failed. Please scan work order.", "Next", "OK", "Cancel");
            }
        }
        void OnExit()
        {
            Model = new SupervisorDispensingModel
            {
                LoginUser = Constant.Username,
                StartDate = DateTime.Now,

                LotNo = string.Empty,
                ProdOrderNo = string.Empty,
                SubLotNo = string.Empty,
                UOM = string.Empty,
                JobNo = string.Empty,
                DrumWeight = string.Empty,
                DrumNo = string.Empty,
            };
        }
    }
}
