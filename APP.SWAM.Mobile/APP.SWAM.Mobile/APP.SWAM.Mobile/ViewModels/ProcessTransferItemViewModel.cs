﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using ZXing.Mobile;

namespace APP.SWAM.Mobile.ViewModels
{
    public class ProcessTransferItemViewModel : ViewModelBase
    {
        #region fields
        private ProcessTransferLineModel _model;

        private bool isRefreshing;
        #endregion

        #region Properties

        public ProcessTransferLineModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }


        public Command SaveCommand { get; set; }
        public Command ClearCommand { get; set; }
        public ICommand FocusedCommand { get; set; }

        #endregion

        public ProcessTransferItemViewModel()
        {
            SaveCommand = new Command(OnSave);
            ClearCommand = new Command(OnExit);
            FocusedCommand = new Command(OnProdScan);
            Title = "Process Transfer Item " + "(" + Settings.DBName + ")";
            Model = new ProcessTransferLineModel
            {
                LoginUser = Constant.Username,
                StartDate = DateTime.Now,
            };
        }

        public override Task InitializeAsync(object navigationData)
        {
            Model = (navigationData as ProcessTransferLineModel);
            return base.InitializeAsync(navigationData);
        }

        void OnProdScan()
        {
#if DEBUG
            string qrCode = "JOB-0001~RM19-M0258~MS18-1020~QCREF001~25~DRUM001";

            var barcode = qrCode.Split('~');
            if (barcode.Length > 3)
            {
                Model.DrumInfo = barcode[0].Trim();
                Model.ProcessInfo = barcode[1].Trim();
                Model.Weight = barcode[2].Trim();
            }

#else
            if (string.IsNullOrEmpty(Model.DrumInfo))
            {
                var scanner = new MobileBarcodeScanner();
                scanner.Torch(true);
                scanner.AutoFocus();

                scanner.ScanContinuously(s =>
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        if (Model == null)
                            Model = new ProcessTransferLineModel();
                        var barcode = s.Text.Split('~');
                        if (barcode.Length > 3)
                        {
                            Model.DrumInfo = barcode[0].Trim();
                            Model.ProcessInfo = barcode[1].Trim();
                            Model.Weight = barcode[2].Trim();
                        }
                        else
                        {
                            await DialogService.ShowConfirmAsync("In-Valid QR Code.QR Code must contains 4 segment(ex:Drum Info,Process Info,Weight).Please scan valid QR Code", "QR Code", "OK", "Cancel");
 
                        }
                    });
                    scanner.Cancel();
                    scanner.Torch(false);
                });


            }
#endif
        }

        async void OnSave()
        {
            if (Model.IsValid())
            {
                IsBusy = true;
                IsEnabled = false;

                UserDialogs.Instance.ShowLoading("Validating Process Transefer...");
                Model.AddedByUserId = Settings.UserId;
                var restclient = new RestClient();
                Model = await restclient.PostAsync<ProcessTransferLineModel>("ProcessTransfer/InsertProcessTransferLine", Model);

                IsBusy = false;
                IsEnabled = true;
                UserDialogs.Instance.HideLoading();
                if (Model.ProcessTransferLineId > 0)
                {
                    await DialogService.ShowConfirmAsync("Record validated successfully.", "Validation", "OK", "Cancel");
                    MessagingCenter.Send(Model, "AddProcessTransfer");
                    await NavigationService.NavigateBackAsync();
                }
                else
                {
                    await DialogService.ShowConfirmAsync("Validation failed.Please select correct drum", "Validation", "OK", "Cancel");
                }

            }
            else
            {
                await DialogService.ShowConfirmAsync("Validation Failed. Please scan work order.", "Next", "OK", "Cancel");
            }
        }
        void OnExit()
        {
            Model = new ProcessTransferLineModel
            {
                LoginUser = Constant.Username,
                StartDate = DateTime.Now,
                DrumInfo = string.Empty,
                ProcessInfo = string.Empty,
                Weight = string.Empty,
            };
        }
    }
}