﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.MultiSelectListView;

namespace APP.SWAM.Mobile.ViewModels
{
    public class ReassignmentJobViewModel : ViewModelBase
    {
        #region fields
        private ObservableCollection<ReassignmentJobModel> reassignmentJobModels = new ObservableCollection<ReassignmentJobModel>();
        private ReassignmentJobModel _model;
        private bool isRefreshing;
        private long _reassignmentJobId;
        #endregion

        #region Properties
        public ObservableCollection<ReassignmentJobModel> ReassignmentJobModels
        {
            get { return reassignmentJobModels; }
            set { reassignmentJobModels = value; OnPropertyChanged(nameof(reassignmentJobModels)); }
        }

        public ReassignmentJobModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }

        public long ReassignmentJobId
        {
            get { return _reassignmentJobId; }
            set { _reassignmentJobId = value; OnPropertyChanged(nameof(ReassignmentJobId)); }
        }

        private List<string> _workingBlocks = new List<string>();
        public List<string> WorkingBlocks
        {
            get { return _workingBlocks; }
            set { _workingBlocks = value; OnPropertyChanged(nameof(WorkingBlocks)); }

        }
        List<MobileShiftModel> workingBlockItems = new List<MobileShiftModel>();
        List<ApplicationMasterDetailModel> specificProcedures = new List<ApplicationMasterDetailModel>();
        private List<string> _specificProcedures = new List<string>();
        public List<string> SpecificProcedures
        {
            get { return _specificProcedures; }
            set { _specificProcedures = value; OnPropertyChanged(nameof(SpecificProcedures)); }

        }

        private List<string> _prodTasks = new List<string>();
        public List<string> ProductionTasks
        {
            get { return _prodTasks; }
            set { _prodTasks = value; OnPropertyChanged(nameof(ProductionTasks)); }

        }
        List<OperationProcedureItem> prodTaskItems = new List<OperationProcedureItem>();


        private List<string> _manPowers = new List<string>();
        public List<string> ManPowers
        {
            get { return _manPowers; }
            set { _manPowers = value; OnPropertyChanged(nameof(ManPowers)); }

        }
        List<ManPowerModel> manPowerItems = new List<ManPowerModel>();
        public Command ProdTaskCommand { get; set; }
        public Command ManPowerCommand { get; set; }
        public Command SaveReAssignJobCommand { get; set; }
        public Command ExitCommand { get; set; }
        #endregion

        public override Task InitializeAsync(object navigationData)
        {
            return base.InitializeAsync(navigationData);
        }

        public ReassignmentJobViewModel()
        {
            SaveReAssignJobCommand = new Command(Save);
            ExitCommand = new Command(OnExit);
            ManPowerCommand = new Command(OnManPower);
            Model = new ReassignmentJobModel
            {
                LoginUser = Settings.UserName,
                StartDate = DateTime.Now,
            };
            Title = "Re-Assignment Job" + "(" + Settings.DBName + ")";
            MessagingCenter.Subscribe<MultiSelectionModel>(this, "AddManPowerSelectionItems", mSelection =>
            {
                foreach (var item in mSelection.MultiselectionItems)
                {
                    if (item.IsSelected)
                    {
                        Model.ReAssignementManpowerMultipleIds.Add(item.Data.Id);
                        Model.ManPower += item.Data.Title + ",";
                    }
                }
            });
            LoadChoices();
        }

        private async void LoadChoices()
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                workingBlockItems = await restClient.Get<MobileShiftModel>("MobileShift/GetMobileShiftByPlants", Settings.NAVName);
                WorkingBlocks = workingBlockItems.Select(q => q.Code).ToList();
                prodTaskItems = await restClient.Get<OperationProcedureItem>("OperationProcedure/GetOperationProcedureItems");
                ProductionTasks = prodTaskItems.Select(q => q.Name).ToList();
                specificProcedures = await restClient.GetApplicationMasterByType<ApplicationMasterDetailModel>("ApplicationMaster/GetApplicationMasterDetailByType", 301);
                SpecificProcedures = specificProcedures.Select(q => q.Value).ToList();
                manPowerItems = await restClient.Get<ManPowerModel>("HumanMovement/GetManPowerBySage");
                ManPowers = manPowerItems.Select(q => q.Name).ToList();

            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            PageDialog.HideLoading();
        }

        private async void OnManPower()
        {
            MultiSelectObservableCollection<MultiSelectionModel> items = new MultiSelectObservableCollection<MultiSelectionModel>();
            manPowerItems.ForEach(p =>
            {
                items.Add(new MultiSelectionModel
                {
                    Id = p.EmployeeID.Value,
                    Title = p.Name,
                });
            });
            MultiSelectionModel multiSelectionModel = new MultiSelectionModel
            {
                MultiSelectionName = "ManPower",
                MultiselectionItems = items
            };
            await NavigationService.NavigateToAsync<MultiselectionViewModel>(multiSelectionModel);
        }
        async void Save()
        {
            try
            {
                Model.MobileShiftId = workingBlockItems.FirstOrDefault(m => m.Code == Model.ShiftCode) != null ? workingBlockItems.FirstOrDefault(m => m.Code == Model.ShiftCode).MobileShiftId as long? : null;
                Model.ProdTaskId = prodTaskItems.FirstOrDefault(m => m.Name == Model.ProdTask) != null ? prodTaskItems.FirstOrDefault(m => m.Name == Model.ProdTask).Id as long? : null;
                Model.SpecificProcedureId = specificProcedures.FirstOrDefault(m => m.Value == Model.SpecificProcedure) != null ? specificProcedures.FirstOrDefault(m => m.Value == Model.SpecificProcedure).ApplicationMasterDetailId as long? : null;
                if (Model.IsValid())
                {
                    IsBusy = true;
                    IsEnabled = false;
                    UserDialogs.Instance.ShowLoading("Saving...");
                    Model.CompanyName = Settings.NAVName;
                    Model.AddedByUserId = Settings.UserId;
                    Model.PostedUserID = Settings.UserId;
                    Model.StartDateTime = DateTime.Now.Date.Add(Model.StartTime);
                    Model.EndDateTime = DateTime.Now.Date.Add(Model.EndTime);

                    var restclient = new RestClient();
                    var result = await restclient.PostAsync<ReassignmentJobModel>("ReAssignmentJob/InsertReAssignmentJob", Model);
                    if (result != null && !result.IsError)
                    {

                        await DialogService.ShowConfirmAsync("Record saved successfully.", "SAVE", "OK", "Cancel");
                        OnExit();
                    }
                    else
                    {
                        IsBusy = false;
                        IsEnabled = true;
                        await DialogService.ShowConfirmAsync("Record insertion failed." + result.Errormessage, "Error", "OK", "Cancel");
                    }
                    IsBusy = false;
                    IsEnabled = true;
                    UserDialogs.Instance.HideLoading();
                }
                else
                {
                    await DialogService.ShowConfirmAsync("Please key-in all the required fields", "Validation", "OK", "Cancel");
                }
            }
            catch (Exception ex)
            {
                IsBusy = false;
                IsEnabled = true;
                await DialogService.ShowConfirmAsync(ex.Message, "ERROR", "OK", "Cancel");
            }
        }

        void OnExit()
        {
            Model.LoginUser = Settings.UserName;
            Model.StartDate = DateTime.Now;
            Model.ReAssignementManpowerMultipleIds = new List<long?>();
            Model.MobileShiftId = 0;
            Model.ReassignmentJobId = -1;
            Model.StartTime = new TimeSpan();
            Model.EndTime = new TimeSpan();
            Model.SpecificProcedure = string.Empty;
            Model.SpecificProcedureId = 0;
            Model.ProdTaskId = 0;
            Model.ShiftCode = string.Empty;
            Model.ProdTask = string.Empty;
            Model.ManPower = string.Empty;
        }

    }
}
