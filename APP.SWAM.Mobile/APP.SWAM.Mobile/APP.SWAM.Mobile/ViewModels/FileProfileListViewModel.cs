﻿using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
namespace APP.SWAM.Mobile.ViewModels
{
    public class FileProfileListViewModel : ViewModelBase
    {
        public FileProfileListViewModel()
        {
            ExitCommand = new Command(OnExit);
        }

        public string ScreenName { get; set; }

        public ICommand ExitCommand { get; set; }

        private FPModel _selectedProfile = new FPModel();
        public FPModel SelectedProfile { get { return _selectedProfile; } set { _selectedProfile = value; OnPropertyChanged(); } }

        public override Task InitializeAsync(object navigationData)
        {
            this.ScreenName = (navigationData as FileProfileListViewModel).ScreenName;
            return base.InitializeAsync(navigationData);
        }

        private async void OnExit()
        {
            if (SelectedProfile != null)
            {
                if (ScreenName == "FileProfile")
                {
                    MessagingCenter.Send(SelectedProfile, "AddSelectedProfile");
                }
            }
            await NavigationService.NavigateBackAsync();
        }
    }
}
