﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModel
{
    public class LoginViewModel : ViewModelBase
    {
        MenuViewModel _menuViewModel;
        public LoginViewModel(MenuViewModel menuViewModel)
        {
            _menuViewModel = menuViewModel;
#if DEBUG
            LoginID = "crt";
            LoginPassword = "1234";
#endif
        }

        private bool _isLoading;
        public bool IsLoading
        {
            get
            {
                return _isLoading;
            }
            set
            {
                _isLoading = value;
                OnPropertyChanged("IsLoading");
            }
        }

        private string _loginId;
        public string LoginID
        {
            get
            {
                return _loginId;
            }
            set
            {
                _loginId = value;
                OnPropertyChanged("LoginID");
                ValidateLogin();
            }
        }

        private string _loginPassword;
        public string LoginPassword
        {
            get
            {
                return _loginPassword;
            }
            set
            {
                _loginPassword = value;
                OnPropertyChanged("LoginPassword");
                ValidateLogin();
            }
        }


        private bool _isLoginIDError;

        public bool IsLoginIDError
        {
            get
            {
                return _isLoginIDError;
            }

            set
            {
                _isLoginIDError = value;
                OnPropertyChanged();
            }
        }

        private bool _isLoginPasswordError;

        public bool IsLoginPasswordError
        {
            get
            {
                return _isLoginPasswordError;
            }

            set
            {
                _isLoginPasswordError = value;
                OnPropertyChanged();
            }
        }


        /// <summary>
        ///  Gets and sets login command
        ///  Command that logins to service
        /// </summary>


        public ICommand LoginCommand => new Command(LoginAsync);

        public override Task InitializeAsync(object navigationData)
        {
            return base.InitializeAsync(navigationData);
        }

        private async void LoginAsync()
        {
            IsBusy = true;
            IsEnabled = false;
            InitializeValidation();
            UserDialogs.Instance.ShowLoading("Login Progress...");
            try
            {
                if (IsConnected)
                {
                    if (!String.IsNullOrEmpty(LoginID) && !String.IsNullOrEmpty(LoginPassword))
                    {
                        var restclient = new RestClient();

                        ApplicationUserModel userModel = new ApplicationUserModel
                        {
                            LoginID = LoginID,
                            LoginPassword = LoginPassword
                        };

                        ApplicationUserModel applicationUserModel = await restclient.Login("UserAuth/Login", userModel);

                        if (applicationUserModel.UserID > 0)
                        {
                            Constant.AppUserId = applicationUserModel.UserID;
                            Constant.Username = applicationUserModel.UserName;
                            Constant.LoginDate = DateTime.Now.ToString();

                            Settings.UserId = applicationUserModel.UserID;
                            Settings.LoginName = applicationUserModel.LoginID;
                            Settings.UserName = applicationUserModel.UserName;
                            Settings.LoginPassword = applicationUserModel.LoginPassword;
                            Settings.IsLoggedIn = true;
                            await NavigationService.NavigateToAsync<MainViewModel>();
                        }
                        else
                        {
                            await Application.Current.MainPage.DisplayAlert("Login Failure", "Please Enter Valid Details!", "OK");
                            IsBusy = false;
                            IsEnabled = true;
                            UserDialogs.Instance.HideLoading();
                            userModel = new ApplicationUserModel();
                            return;
                        }
                    }

                    else
                    {
                        ValidateLogin();
                        await Application.Current.MainPage.DisplayAlert("Login Failure", "Please Enter Valid Details!", "OK");
                        IsBusy = false;
                        IsEnabled = true;
                        UserDialogs.Instance.HideLoading();
                        return;
                    }
                }
                else
                {
                    ShowConnectivityError();
                }

            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Login Failure", "Unexpected error!.Check the connectivity", "OK");
                IsBusy = false;
                IsEnabled = true;
                UserDialogs.Instance.HideLoading();
            }


            IsBusy = false;
            IsEnabled = true;
            UserDialogs.Instance.HideLoading();
        }

        private void InitializeValidation()
        {
            IsLoginIDError = false;
            IsLoginPasswordError = false;
        }

        private void ValidateLogin()
        {
            IsLoginIDError = string.IsNullOrEmpty(LoginID);
            IsLoginPasswordError = string.IsNullOrEmpty(LoginPassword);
        }
    }
}
