﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class ProcessTransferLineViewModel : ViewModelBase
    {
        #region fields

        private ProcessTransferLineModel _model;
        private ObservableCollection<ProcessTransferLineModel> _modelEntries = new ObservableCollection<ProcessTransferLineModel>();
        private bool isRefreshing;
        private long _processTransferId;
        private ProcessTransferLineModel _selectedItem = new ProcessTransferLineModel();
        #endregion

        #region Properties

        public ProcessTransferLineModel SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value; OnPropertyChanged(nameof(SelectedItem));
            }
        }

        private ProcessTransferModel _parentModel;
        public ProcessTransferModel ParentModel
        {
            get { return _parentModel; }
            set
            {
                _parentModel = value; OnPropertyChanged(nameof(ParentModel));
            }
        }

        public ObservableCollection<ProcessTransferLineModel> ProcessTransferLines
        {
            get { return _modelEntries; }
            set { _modelEntries = value; OnPropertyChanged(nameof(ProcessTransferLines)); }
        }

        public ProcessTransferLineModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }

        public long ProcessTransferId
        {
            get { return _processTransferId; }
            set { _processTransferId = value; OnPropertyChanged(nameof(ProcessTransferId)); }
        }


        public Command AddLineCommand { get; set; }
        public Command DeleteCommand { get; set; }
        public ICommand SelectedItemLine { get; set; }
        #endregion

        public ProcessTransferLineViewModel()
        {
            AddLineCommand = new Command(OnAdd);
            DeleteCommand = new Command(OnDelete);
            SelectedItemLine = new Command(OnSelected);
            Model = new ProcessTransferLineModel
            {
                LoginUser = Constant.Username,
                StartDate = DateTime.Now,
            };

            MessagingCenter.Subscribe<ProcessTransferLineModel>(this, "AddProcessTransfer", jobModel =>
            {
                OnReloadJob(jobModel);
            });
        }

        public override Task InitializeAsync(object navigationData)
        {
            ParentModel = (navigationData as ProcessTransferModel);
            Model.ProcessTransferId = ParentModel.ProcessTransferId;
            return base.InitializeAsync(navigationData);
        }
        async void OnDelete()
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                await restClient.DeleteAsync<bool>("ProcessTransfer/DeleteProcessTransferLine", SelectedItem.ProcessTransferLineId);
            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            ProcessTransferLines.Remove(SelectedItem);
            PageDialog.HideLoading();
        }
        async void OnSelected()
        {
            if (SelectedItem.ProcessTransferId > 0)
            {
                await NavigationService.NavigateToAsync<ProcessTransferItemViewModel>(SelectedItem);
            }
        }

        private async void OnReloadJob(ProcessTransferLineModel jobModel)
        {
            await LoadData(jobModel.ProcessTransferId);
        }
        async void OnAdd()
        {
            Model = new ProcessTransferLineModel();
            Model.ProcessTransferId = ParentModel.ProcessTransferId;
            await NavigationService.NavigateToAsync<ProcessTransferItemViewModel>(Model);
        }
        private async Task LoadData(long? processTransferId)
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                var processTransfers = await restClient.Get<ProcessTransferLineModel>("ProcessTransfer/GetProcessTransferLines", processTransferId.Value);
                ProcessTransferLines = new ObservableCollection<ProcessTransferLineModel>(processTransfers);
            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            PageDialog.HideLoading();
        }

    }
}
