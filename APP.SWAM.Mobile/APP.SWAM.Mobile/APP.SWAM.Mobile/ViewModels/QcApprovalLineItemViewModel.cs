﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class QcApprovalLineItemViewModel : ViewModelBase
    {
        public Command UpdateCommand { get; set; }
        public Command ExitCommand { get; set; }

        private QcApprovalLineModel _model = new QcApprovalLineModel();


        private List<string> _moistureStaus = new List<string>();
        public List<string> MoistureStaus
        {
            get { return _moistureStaus; }
            set { _moistureStaus = value; OnPropertyChanged(nameof(MoistureStaus)); }

        }
        public QcApprovalLineModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        private int _selectedIndex = 0;
        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set
            {
                _selectedIndex = value; OnPropertyChanged(nameof(SelectedIndex));
            }
        }
        public QcApprovalLineItemViewModel()
        {
            UpdateCommand = new Command(OnUpdate);
            ExitCommand = new Command(OnExit);
            Title = "Qc Approval Item " + "(" + Settings.DBName + ")";
            MoistureStaus.Add("Pass");
            MoistureStaus.Add("Fail");
        }

        public override Task InitializeAsync(object navigationData)
        {
            Model = (navigationData as QcApprovalLineModel);
            return base.InitializeAsync(navigationData);
        }

        private async void OnUpdate()
        {
            IsBusy = true;
            IsEnabled = false;

            UserDialogs.Instance.ShowLoading("Validating Qc Approval Line...");
            Model.AddedByUserId = Settings.UserId;
            Model.MoisturePercent = Model.Percent;
            Model.MoistureStatus = MoistureStaus[SelectedIndex];
            var restclient = new RestClient();
            Model = await restclient.PostAsync<QcApprovalLineModel>("QCApproval/UpdateQcApprovalLine", Model);

            IsBusy = false;
            IsEnabled = true;
            UserDialogs.Instance.HideLoading();
            if (Model.QcapprovalLineId > 0)
            {
                await DialogService.ShowConfirmAsync("Record updated successfully.", "Validation", "OK", "Cancel");
                await NavigationService.NavigateBackAsync();
            }

        }

        private async void OnExit()
        {
            await NavigationService.NavigateBackAsync();
        }
    }
}
