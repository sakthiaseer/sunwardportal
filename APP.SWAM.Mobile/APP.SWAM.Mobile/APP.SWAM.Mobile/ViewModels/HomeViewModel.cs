﻿using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.ViewModel.Base;
using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModel
{
    public class HomeViewModel : ViewModelBase
    {
        public HomeViewModel()
        {
            LocationCommand = new Command(OnLocation);
            ProductionStartCommand = new Command(OnProductionStart);
            ProductionOutputCommand = new Command(OnProductionOutput);
            QcApprovalCommand = new Command(OnQcApproval);
            StartOfDayCommand = new Command(OnStartOfDay);
            ReassignmentJobCommand = new Command(OnReassignmentJob);
            ProcessTransferCommand = new Command(OnProcessTransfer);
            IPIRCommand = new Command(OnIPIR);
            CreateIPIRCommand = new Command(OnCreateIPIR);
            BMRToCartonCommand = new Command(OnBMRToCarton);
            ChatCommand = new Command(OnChat);
            ConsumptionCommand = new Command(OnConsumption);
            ScanImageCommand = new Command(OnScanImage);
            ProdScanCommand = new Command(OnProdScan);
            DrummingCommand = new Command(OnDrum);
            SupDispensingCommand = new Command(OnSupDispense);
            DisDispensingCommand = new Command(OnDisDispense);
        }

        public override Task InitializeAsync(object navigationData)
        {
            InitiaizeMenuItems();
            HomeMenuItems = new ObservableCollection<HomeMenuModel>(HomeMenuItems.Where(m => !Settings.ExcludedMenus.Contains(m.MenuItem.ToString())));
            return base.InitializeAsync(navigationData);
        }

        public Command LocationCommand { get; set; }
        public Command ProductionStartCommand { get; set; }
        public Command ProductionOutputCommand { get; set; }
        public Command QcApprovalCommand { get; set; }
        public Command StartOfDayCommand { get; set; }

        public Command ReassignmentJobCommand { get; set; }
        public Command ProcessTransferCommand { get; set; }
        public Command IPIRCommand { get; set; }
        public Command CreateIPIRCommand { get; set; }
        public Command BMRToCartonCommand { get; set; }
        public Command ChatCommand { get; set; }
        public Command ConsumptionCommand { get; set; }
        public Command DrummingCommand { get; set; }
        public Command SupDispensingCommand { get; set; }
        public Command DisDispensingCommand { get; set; }
        public Command ScanImageCommand { get; set; }
        public Command ProdScanCommand { get; set; }

        ObservableCollection<HomeMenuModel> homeMenuItems = new ObservableCollection<HomeMenuModel>();
        public ObservableCollection<HomeMenuModel> HomeMenuItems
        {
            get
            {
                return homeMenuItems;
            }
            set
            {
                homeMenuItems = value;
                OnPropertyChanged();
            }
        }



        private void InitiaizeMenuItems()
        {
            HomeMenuItems.Add(new HomeMenuModel
            {
                MenuName = "Location",
                Title = "Maintaining the Asset Location",
                MenuItem = MenuItemType.Location,
                IsVisible = true,
                ImageSource = "location.png",
                MenuCommand = new Command(OnLocation)
            });
            HomeMenuItems.Add(new HomeMenuModel
            {
                MenuName = "Production Entry Start",
                Title = "Production Start process",
                MenuItem = MenuItemType.AssetItems,
                IsVisible = true,
                ImageSource = "manufacture.png",
                MenuCommand = new Command(OnProductionStart)
            });
            HomeMenuItems.Add(new HomeMenuModel
            {
                MenuName = "Production Output",
                Title = "Production Output process",
                MenuItem = MenuItemType.ProductionOutput,
                IsVisible = true,
                ImageSource = "output.png",
                MenuCommand = new Command(OnProductionOutput)
            });
            HomeMenuItems.Add(new HomeMenuModel
            {
                MenuName = "Qc Approval",
                Title = "Maintaining the QC Approval",
                MenuItem = MenuItemType.QCApproval,
                IsVisible = true,
                ImageSource = "approve.png",
                MenuCommand = new Command(OnQcApproval)
            });
            HomeMenuItems.Add(new HomeMenuModel
            {
                MenuName = "Start Of Day",
                Title = "Maintaining the Manpower",
                MenuItem = MenuItemType.StartOfDay,
                IsVisible = true,
                ImageSource = "manpower.png",
                MenuCommand = new Command(OnStartOfDay)
            });
            HomeMenuItems.Add(new HomeMenuModel
            {
                MenuName = "Re-assignment Job",
                Title = "Maintaining Re-assignment Job",
                MenuItem = MenuItemType.ReAssignmentJob,
                IsVisible = true,
                ImageSource = "manpower.png",
                MenuCommand = new Command(OnReassignmentJob)
            });
            HomeMenuItems.Add(new HomeMenuModel
            {
                MenuName = "Process Transfer",
                Title = "Maintaining the process transfer",
                MenuItem = MenuItemType.ProcessTransfer,
                IsVisible = true,
                ImageSource = "ptransfer.png",
                MenuCommand = new Command(OnProcessTransfer)
            });
            HomeMenuItems.Add(new HomeMenuModel
            {
                MenuName = "BMR To Carton",
                Title = "BMR To Carton Process",
                MenuItem = MenuItemType.BMRToCarton,
                IsVisible = true,
                ImageSource = "box.png",
                MenuCommand = new Command(OnBMRToCarton)
            });
            HomeMenuItems.Add(new HomeMenuModel
            {
                MenuName = "Chat",
                Title = "Chat application",
                MenuItem = MenuItemType.Chat,
                IsVisible = true,
                ImageSource = "notes.png",
                MenuCommand = new Command(OnChat)
            });

            HomeMenuItems.Add(new HomeMenuModel
            {
                MenuName = "Consumption",
                Title = "Maintaining the consumption",
                MenuItem = MenuItemType.Ticket,
                IsVisible = true,
                ImageSource = "consumption.png",
                MenuCommand = new Command(OnConsumption)
            });

            HomeMenuItems.Add(new HomeMenuModel
            {
                MenuName = "Drumming",
                Title = "Maintaining the drumming process",
                MenuItem = MenuItemType.Transfer,
                IsVisible = true,
                ImageSource = "drumming.png",
                MenuCommand = new Command(OnDrum)
            });

            HomeMenuItems.Add(new HomeMenuModel
            {
                MenuName = "Supervisor Dispensing",
                Title = "Maintaining the Supervisor Dispensing",
                MenuItem = MenuItemType.SDispensing,
                IsVisible = true,
                ImageSource = "supervisond.png",
                MenuCommand = new Command(OnSupDispense)
            });

            HomeMenuItems.Add(new HomeMenuModel
            {
                MenuName = "Dispensor Dispensing",
                Title = "Maintaining the Dispensor Dispensing",
                MenuItem = MenuItemType.DDispensing,
                IsVisible = true,
                ImageSource = "dispensord.png",
                MenuCommand = new Command(OnDisDispense)
            });

            HomeMenuItems.Add(new HomeMenuModel
            {
                MenuName = "IPIR Issue",
                Title = "Maintaining IPIR Issue",
                MenuItem = MenuItemType.IPIR,
                IsVisible = true,
                ImageSource = "notes.png",
                MenuCommand = new Command(OnIPIR)
            });
            HomeMenuItems.Add(new HomeMenuModel
            {
                MenuName = "Create IPIR",
                Title = "Maintaining Create IPIR",
                MenuItem = MenuItemType.CreateIPIR,
                IsVisible = true,
                ImageSource = "notes.png",
                MenuCommand = new Command(OnCreateIPIR)
            });

            HomeMenuItems.Add(new HomeMenuModel
            {
                MenuName = "Production Scan",
                Title = "Maintaining Production Document",
                MenuItem = MenuItemType.ProductionScan,
                IsVisible = true,
                ImageSource = "notes.png",
                MenuCommand = new Command(OnProdScan)
            });
        }

        private async void OnLocation()
        {
            await NavigationService.NavigateToAsync<AssetLocationViewModel>();
        }
        private async void OnConsumption()
        {
            await NavigationService.NavigateToAsync<ConsumptionEntryViewModel>();
        }
        private async void OnDrum()
        {
            await NavigationService.NavigateToAsync<DrummingViewModel>();
        }
        private async void OnSupDispense()
        {
            await NavigationService.NavigateToAsync<SupervisorDispensingViewModel>();
        }
        private async void OnDisDispense()
        {
            await NavigationService.NavigateToAsync<DispensorDispensingViewModel>();
        }
        private async void OnProductionStart()
        {
            await NavigationService.NavigateToAsync<ProductionEntryViewModel>();
        }

        private async void OnProductionOutput()
        {
            await NavigationService.NavigateToAsync<ProductionOutputViewmodel>();
        }

        private async void OnQcApproval()
        {
            await NavigationService.NavigateToAsync<QcApprovalViewmodel>();
        }

        private async void OnStartOfDay()
        {
            await NavigationService.NavigateToAsync<StartOfDayViewModel>();
        }

        private async void OnReassignmentJob()
        {
            await NavigationService.NavigateToAsync<ReassignmentJobViewModel>();
        }

        private async void OnProcessTransfer()
        {
            await NavigationService.NavigateToAsync<ProcessTransferViewModel>();
        }

        private async void OnIPIR()
        {
            await NavigationService.NavigateToAsync<IPIRListViewModel>();
        }

        private async void OnCreateIPIR()
        {
            await NavigationService.NavigateToAsync<CreateIPIRListViewModel>();
        }

        private async void OnBMRToCarton()
        {
            await NavigationService.NavigateToAsync<BMRToCartonViewModel>();
        }


        private async void OnChat()
        {
            await NavigationService.NavigateToAsync<ChatViewModel>();
        }

        private async void OnScanImage()
        {
            await NavigationService.NavigateToAsync<ScanListViewModel>();
        }

        private async void OnProdScan()
        {
            await NavigationService.NavigateToAsync<ProductionDocumentListViewModel>();
        }

    }



}
