﻿using APP.SWAM.Mobile.ViewModel.Base;
using FormsVideoLibrary;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class PlaywebVideoViewModel : ViewModelBase
    {

        public Command ExitCommand { get; set; }
        private VideoSource _videoURL = VideoSource.FromUri("https://archive.org/download/ElephantsDream/ed_hd_512kb.mp4");
        public VideoSource VideoUrl { get => _videoURL; set { _videoURL = value; OnPropertyChanged(); } }
        public PlaywebVideoViewModel()
        {
            ExitCommand = new Command(OnExit);

        }
        public override Task InitializeAsync(object urls)
        {
            VideoUrl = VideoSource.FromUri(urls.ToString());

            return base.InitializeAsync(urls);
        }


        async void OnExit()
        {
            await PopupNavigation.Instance.PopAsync();
        }
    }
}
