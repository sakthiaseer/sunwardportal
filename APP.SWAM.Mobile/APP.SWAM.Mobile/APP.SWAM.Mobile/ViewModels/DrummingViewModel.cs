﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class DrummingViewModel : ViewModelBase
    {

        #region fields

        private DrummingModel _model;
        private bool isRefreshing;
        private long _drummingId;
        #endregion

        #region Properties


        public DrummingModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }

        public long DrummingId
        {
            get { return _drummingId; }
            set { _drummingId = value; OnPropertyChanged(nameof(DrummingId)); }
        }


        public Command SaveCommand { get; set; }
        public Command ClearCommand { get; set; }
        public ICommand FocusedCommand { get; set; }
        public ICommand DrumFocusedCommand { get; set; }
        #endregion
        public DrummingViewModel()
        {
            SaveCommand = new Command(OnSave);
            ClearCommand = new Command(OnExit);
            FocusedCommand = new Command(OnProdScan);
            DrumFocusedCommand = new Command(OnDrumScan);
            Model = new DrummingModel
            {
                LoginUser = Constant.Username,
                StartDate = DateTime.Now,
            };
        }

        public override Task InitializeAsync(object navigationData)
        {
            return base.InitializeAsync(navigationData);
        }

        void OnDrumScan()
        {
#if DEBUG
            string qrCode = "DRUM-0001~25~BAG-0001~10~35";

            var barcode = qrCode.Split('~');
            if (barcode.Length > 3)
            {
                Model.DrumNo = barcode[0].Trim();
                Model.DrumWeight = barcode[1].Trim();
                Model.BagNo = barcode[2].Trim();
                Model.BagWeight = barcode[3].Trim();
                Model.TotalWeight = barcode[3].Trim();
            }
#endif
        }

        void OnProdScan()
        {
#if DEBUG
            string qrCode = "WO19-M0258~RM19-M0258~ITEM-001~Prozine Syrup 50X120ML~8~1";

            var barcode = qrCode.Split('~');
            if (barcode.Length > 3)
            {
                Model.WrokOrderNo = barcode[0].Trim();
                Model.ProdOrderNo = barcode[1].Trim();
                Model.ItemNo = barcode[1].Trim();
                Model.Description = barcode[2].Trim();
                Model.Description = barcode[3].Trim();
            }
#endif
        }
        async void OnSave()
        {
            if (Model.IsValid())
            {
                IsBusy = true;
                IsEnabled = false;

                UserDialogs.Instance.ShowLoading("Validating Drum...");
                Model.AddedByUserId = Settings.UserId;
                var restclient = new RestClient();
                Model = await restclient.PostAsync<DrummingModel>("APPDrumming/InsertAPPDrumming", Model);

                IsBusy = false;
                IsEnabled = true;
                UserDialogs.Instance.HideLoading();
                if (Model.DrummingId > 0)
                {
                    await DialogService.ShowConfirmAsync("Record validated successfully.", "Validation", "OK", "Cancel");
                }
                else
                {
                    await DialogService.ShowConfirmAsync("Validation failed.Please select correct drum", "Validation", "OK", "Cancel");
                }               
                OnExit();
            }
            else
            {
                await DialogService.ShowConfirmAsync("Validation Failed. Please scan production order.", "Next", "OK", "Cancel");
            }
        }
        void OnExit()
        {
            Model = new DrummingModel
            {
                LoginUser = Constant.Username,
                StartDate = DateTime.Now,
                AddedByUserId = Settings.UserId,
                BagNo = string.Empty,
                BagWeight = string.Empty,
                Description = string.Empty,
                DrumNo = string.Empty,
                DrumWeight = string.Empty,
                ItemNo = string.Empty,
                ProdOrderNo = string.Empty,
                SublotNo = string.Empty,
                Title = string.Empty,
                TotalWeight = string.Empty,
                WrokOrderNo = string.Empty,
            }; 
        }
    }
}
