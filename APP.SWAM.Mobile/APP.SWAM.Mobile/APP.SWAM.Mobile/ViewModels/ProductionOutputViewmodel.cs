﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;

using Plugin.Media;
using Plugin.Media.Abstractions;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using ZXing.Mobile;

namespace APP.SWAM.Mobile.ViewModels
{
    public class ProductionOutputViewmodel : ViewModelBase
    {
        #region fields
        private ObservableCollection<ProductionOutputModel> productionOutputModels = new ObservableCollection<ProductionOutputModel>();
        private ProductionOutputModel selectedProductionOutputModel;
        private bool isRefreshing;
        private long productionOutputId;
        #endregion

        #region Properties
        public ObservableCollection<ProductionOutputModel> ProductionOutputModels
        {
            get { return productionOutputModels; }
            set { productionOutputModels = value; OnPropertyChanged(nameof(ProductionOutputModels)); }
        }

        public ProductionOutputModel SelectedProductionOutputModel
        {
            get { return selectedProductionOutputModel; }
            set
            {
                selectedProductionOutputModel = value; OnPropertyChanged(nameof(SelectedProductionOutputModel));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }

        public long ProductionOutputID
        {
            get { return productionOutputId; }
            set { productionOutputId = value; OnPropertyChanged(nameof(ProductionOutputID)); }
        }

        public Command SaveProductionOutputCommand { get; set; }
        public Command ExitCommand { get; set; }
        public ICommand TextboxFocusedEvent { get; set; }
        public ICommand LocationScanCommand { get; set; }
        #endregion

        public override Task InitializeAsync(object navigationData)
        {
            return base.InitializeAsync(navigationData);
        }



        public ProductionOutputViewmodel()
        {
            SaveProductionOutputCommand = new Command(Save);
            ExitCommand = new Command(OnExit);
            TextboxFocusedEvent = new Command(OnProdScan);
            LocationScanCommand = new Command(OnLocationScan);
            SelectedProductionOutputModel = new ProductionOutputModel
            {
                LoginUser = Settings.UserName,
                StartDate = DateTime.Now,
            };
            Title = "Production Output" + "(" + Settings.DBName + ")";
        }
        void OnLocationScan()
        {
#if DEBUG

            string qrCode = "SWMY";

            selectedProductionOutputModel.LocationName = qrCode;
#else
            if (string.IsNullOrEmpty(selectedProductionOutputModel.LocationName))
            {
                var scanner = new MobileBarcodeScanner();
                scanner.Torch(true);
                scanner.AutoFocus();

                scanner.ScanContinuously(s =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (selectedProductionOutputModel == null)
                            selectedProductionOutputModel = new ProductionOutputModel();
                        selectedProductionOutputModel.LocationName = s.Text;
                    });
                    scanner.Cancel();
                    scanner.Torch(false);
                });
            }
#endif
        }

        async void OnProdScan()
        {
            //#if DEBUG

            //            string qrCode = "MT20-0013-5-GR1~10000~Metformin 500mg Tablet Granules~643-20026~1~PR-GR-006";

            //            var barcode = qrCode.Split('~');
            //            if (barcode.Length == 6)
            //            {
            //                selectedProductionOutputModel.ProductionOrderNo = barcode[0].Trim();
            //                selectedProductionOutputModel.SubLotNo = barcode[1].Trim();
            //                selectedProductionOutputModel.Description = barcode[2].Trim();
            //                selectedProductionOutputModel.BatchNo = barcode[3].Trim();
            //                selectedProductionOutputModel.DrumNo = barcode[4].Trim();
            //                selectedProductionOutputModel.ItemNo = barcode[5].Trim();
            //                selectedProductionOutputModel.CompanyName = Settings.NAVName;
            //                selectedProductionOutputModel.AddedByUserId = Settings.UserId;
            //                selectedProductionOutputModel.PostedUserID = Settings.UserId;

            //                var restclient = new RestClient();
            //                var result = await restclient.PostAsync<ProductionOutputModel>("ProductionOutput/GetProductionOrderInformation", selectedProductionOutputModel);
            //                if (result.IsError)
            //                {
            //                    await DialogService.ShowConfirmAsync(result.Errormessage, "Error", "OK", "Cancel");
            //                    OnExit();
            //                    return;
            //                }

            //                selectedProductionOutputModel.Buom = result.Buom;
            //                selectedProductionOutputModel.Description = result.Description;
            //                selectedProductionOutputModel.BatchNo = result.BatchNo;
            //                selectedProductionOutputModel.ItemNo = result.ItemNo;

            //                if (string.IsNullOrEmpty(selectedProductionOutputModel.BatchNo))
            //                {
            //                    await DialogService.ShowConfirmAsync("Batch No should not be blank.Please scan valid QR Code", "QR Code", "OK", "Cancel");
            //                    OnExit();
            //                    return;
            //                }
            //                if (string.IsNullOrEmpty(selectedProductionOutputModel.ItemNo))
            //                {
            //                    await DialogService.ShowConfirmAsync("Item No should not be blank.Please scan valid QR Code", "QR Code", "OK", "Cancel");
            //                    OnExit();
            //                    return;
            //                }
            //            }

            //#else
            if (string.IsNullOrEmpty(selectedProductionOutputModel.ProductionOrderNo))
            {
                var scanner = new MobileBarcodeScanner();
                scanner.Torch(true);
                scanner.AutoFocus();
                string[] barcode = new string[0];
                scanner.ScanContinuously(async s =>
               {
                   Device.BeginInvokeOnMainThread(async () =>
                   {
                       if (selectedProductionOutputModel == null)
                           selectedProductionOutputModel = new ProductionOutputModel();

                       barcode = s.Text.Split('~');
                       if (barcode.Length == 6)
                       {
                           selectedProductionOutputModel.ProductionOrderNo = barcode[0].Trim();
                           selectedProductionOutputModel.SubLotNo = barcode[1].Trim();
                           selectedProductionOutputModel.Description = barcode[2].Trim();
                           selectedProductionOutputModel.BatchNo = barcode[3].Trim();
                           selectedProductionOutputModel.DrumNo = barcode[4].Trim();
                           selectedProductionOutputModel.ItemNo = barcode[5].Trim();
                           selectedProductionOutputModel.CompanyName = Settings.NAVName;
                           selectedProductionOutputModel.AddedByUserId = Settings.UserId;
                           selectedProductionOutputModel.PostedUserID = Settings.UserId;
                           var restclient = new RestClient();
                           var result = await restclient.PostAsync<ProductionOutputModel>("ProductionOutput/GetProductionOrderInformation", selectedProductionOutputModel);
                           if (result != null && result.IsError)
                           {
                               await DialogService.ShowConfirmAsync(result.Errormessage, "Error", "OK", "Cancel");
                               OnExit();
                               return;
                           }
                           selectedProductionOutputModel.Buom = result.Buom;
                           selectedProductionOutputModel.Description = result.Description;
                           selectedProductionOutputModel.BatchNo = result.BatchNo;
                           selectedProductionOutputModel.ItemNo = result.ItemNo;
                           if (string.IsNullOrEmpty(selectedProductionOutputModel.BatchNo))
                           {
                               await DialogService.ShowConfirmAsync("Batch No should not be blank.Please scan valid QR Code", "QR Code", "OK", "Cancel");
                               OnExit();
                               return;
                           }
                           if (string.IsNullOrEmpty(selectedProductionOutputModel.ItemNo))
                           {
                               await DialogService.ShowConfirmAsync("Item No should not be blank.Please scan valid QR Code", "QR Code", "OK", "Cancel");
                               OnExit();
                               return;
                           }
                       }
                   });
                   scanner.Cancel();
                   scanner.Torch(false);

                   if (barcode.Length < 6)
                   {
                       await DialogService.ShowConfirmAsync("In-Valid QR Code.QR Code must contains 6 segment(ex:Prod.No,Sub Lot No, Desc, BatchNo,DrumNo,ItemNo).Please scan valid QR Code", "QR Code", "OK", "Cancel");
                       OnExit();
                       return;
                   }

               });
            }
//#endif
        }

        async void Save()
        {
            try
            {
                if (selectedProductionOutputModel.IsValid())
                {
                    IsBusy = true;
                    IsEnabled = false;
                    UserDialogs.Instance.ShowLoading("Saving...");
                    selectedProductionOutputModel.CompanyName = Settings.NAVName;
                    selectedProductionOutputModel.AddedByUserId = Settings.UserId;
                    selectedProductionOutputModel.PostedUserID = Settings.UserId;
                    selectedProductionOutputModel.OutputQty = decimal.Parse(selectedProductionOutputModel.StrOutputQty);
                    var restclient = new RestClient();
                    var result = await restclient.PostAsync<ProductionOutputModel>("ProductionOutput/InsertProductionOutput", selectedProductionOutputModel);
                    if (result != null && !result.IsError)
                    {

                        await DialogService.ShowConfirmAsync("Record saved successfully.", "SAVE", "OK", "Cancel");
                        OnExit();
                    }
                    else
                    {
                        await DialogService.ShowConfirmAsync("Record insertion failed." + result.Errormessage, "Error", "OK", "Cancel");
                    }
                    IsBusy = false;
                    IsEnabled = true;
                    UserDialogs.Instance.HideLoading();
                }
                else
                {
                    await DialogService.ShowConfirmAsync("Please key-in all the required fields", "Validation", "OK", "Cancel");
                }
            }
            catch (Exception ex)
            {
                await DialogService.ShowConfirmAsync(ex.Message, "ERROR", "OK", "Cancel");
                IsBusy = false;
                IsEnabled = true;
                UserDialogs.Instance.HideLoading();
            }
        }

        void OnExit()
        {

            selectedProductionOutputModel.LoginUser = Settings.UserName;
            selectedProductionOutputModel.StartDate = DateTime.Now;
            selectedProductionOutputModel.ItemNo = string.Empty;
            selectedProductionOutputModel.LocationName = string.Empty;
            selectedProductionOutputModel.ProductionOrderNo = string.Empty;
            selectedProductionOutputModel.BatchNo = string.Empty;
            selectedProductionOutputModel.Buom = string.Empty;
            selectedProductionOutputModel.DrumNo = string.Empty;
            selectedProductionOutputModel.SubLotNo = string.Empty;
            selectedProductionOutputModel.Description = string.Empty;
            selectedProductionOutputModel.ProductionOutputId = -1;
            selectedProductionOutputModel.OutputQty = 0;
            selectedProductionOutputModel.StrOutputQty = string.Empty;
            selectedProductionOutputModel.IsProdutionOrderComplete = false;
            selectedProductionOutputModel.IsLotComplete = false;
        }

    }
}
