﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModel.Base
{
    public class MenuViewModel : ViewModelBase
    {

        private ObservableCollection<Models.MenuItem> _menuItems;
        List<string> excludeMenus = new List<string>();
        public MenuViewModel()
        {
            MenuItems = new ObservableCollection<Models.MenuItem>();
        }

        public string UserName => AppSettings.User?.Name;

        public string UserAvatar => AppSettings.User?.AvatarUrl;

        public ObservableCollection<Models.MenuItem> MenuItems
        {
            get
            {
                return _menuItems;
            }
            set
            {
                _menuItems = value;
                OnPropertyChanged();
            }
        }

        public List<string> ExcludedMenus
        {
            get
            {
                return excludeMenus;
            }
            set
            {
                excludeMenus = value;
                OnPropertyChanged();
            }
        }

        public ICommand MenuItemSelectedCommand => new Command<Models.MenuItem>(OnSelectMenuItem);

        public async override Task InitializeAsync(object navigationData)
        {
            UserDialogs.Instance.ShowLoading();
            InitMenuItems();
            if (Settings.ExcludedMenus.Any())
            {
                ExcludedMenus = Settings.ExcludedMenus;
            }
            else
            {
                RestClient restClient = new RestClient();
                List<ApplicationPermissionModel> applicationPermissions = await restClient.GetByIds<ApplicationPermissionModel>("ApplicationUser/GetUserPermission", Settings.UserId, 1);
                foreach (var menuItem in MenuItems.Where(m => !m.IsAlwaysAvailable))
                {
                    var applicationPermission = applicationPermissions.FirstOrDefault(a => a.MenuId == menuItem.MenuItemType.ToString());
                    if (applicationPermission == null)
                    {
                        ExcludedMenus.Add(menuItem.MenuItemType.ToString());
                    }
                }
                Settings.ExcludedMenus = ExcludedMenus;
            }
            MenuItems = new ObservableCollection<Models.MenuItem>(MenuItems.Where(m => !ExcludedMenus.Contains(m.MenuItemType.ToString())));
            UserDialogs.Instance.HideLoading();
        }

        public Task OnViewAppearingAsync(VisualElement view)
        {
            return Task.FromResult(true);
        }

        public Task OnViewDisappearingAsync(VisualElement view)
        {
            return Task.FromResult(true);
        }

        private void InitMenuItems()
        {
            MenuItems.Add(new Models.MenuItem
            {
                Title = "Menu",
                MenuItemType = MenuItemType.Menu,
                ViewModelType = typeof(HomeViewModel),
                IsEnabled = true,
                IsAlwaysAvailable = true,

            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "Asset Mgmt",
                MenuItemType = MenuItemType.Location,
                ViewModelType = typeof(AssetLocationViewModel),
                IsEnabled = true,

            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "Production Start",
                MenuItemType = MenuItemType.AssetItems,
                ViewModelType = typeof(ProductionEntryViewModel),
                IsEnabled = true,
            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "Production Output",
                MenuItemType = MenuItemType.ProductionOutput,
                ViewModelType = typeof(ProductionOutputViewmodel),
                IsEnabled = true,
            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "QC Approval",
                MenuItemType = MenuItemType.QCApproval,
                ViewModelType = typeof(QcApprovalViewmodel),
                IsEnabled = true,
            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "Start Of Day",
                MenuItemType = MenuItemType.StartOfDay,
                ViewModelType = typeof(StartOfDayViewModel),
                IsEnabled = true,
            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "Re-assignment Job",
                MenuItemType = MenuItemType.ReAssignmentJob,
                ViewModelType = typeof(ReassignmentJobViewModel),
                IsEnabled = true,
            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "Process Transfer",
                MenuItemType = MenuItemType.ProcessTransfer,
                ViewModelType = typeof(ProcessTransferViewModel),
                IsEnabled = true,
            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "BMR To Carton",
                MenuItemType = MenuItemType.BMRToCarton,
                ViewModelType = typeof(BMRToCartonViewModel),
                IsEnabled = true,
            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "Ticket Consumption",
                MenuItemType = MenuItemType.Ticket,
                ViewModelType = typeof(ConsumptionEntryViewModel),
                IsEnabled = true,
            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "Transfer Drum",
                MenuItemType = MenuItemType.Transfer,
                ViewModelType = typeof(DrummingViewModel),
                IsEnabled = true,
            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "Supervisor Dispensing",
                MenuItemType = MenuItemType.SDispensing,
                ViewModelType = typeof(SupervisorDispensingViewModel),
                IsEnabled = true,
            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "Dispensor Dispensing",
                MenuItemType = MenuItemType.DDispensing,
                ViewModelType = typeof(DispensorDispensingViewModel),
                IsEnabled = true,
            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "Chat",
                MenuItemType = MenuItemType.Chat,
                ViewModelType = typeof(ChatViewModel),
                IsEnabled = true,

            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "Material Return",
                MenuItemType = MenuItemType.MaterialReclass,
                ViewModelType = typeof(MaterialReturnViewModel),
                IsEnabled = true,                
            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "Transfer/Reclass",
                MenuItemType = MenuItemType.TransferReClass,
                ViewModelType = typeof(TransferReclassEntryViewModel),
                IsEnabled = true,               
            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "Transfer Production",
                MenuItemType = MenuItemType.TransferProduction,
                ViewModelType = typeof(TransferProductionViewModel),
                IsEnabled = true,
            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "Transfer Receive",
                MenuItemType = MenuItemType.TransferReceive,
                ViewModelType = typeof(TransferReceiveViewModel),
                IsEnabled = true,
            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "IPIR Issue",
                MenuItemType = MenuItemType.IPIR,
                ViewModelType = typeof(IPIRListViewModel),
                IsEnabled = true,
            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "Create IPIR",
                MenuItemType = MenuItemType.CreateIPIR,
                ViewModelType = typeof(CreateIPIRListViewModel),
                IsEnabled = true,
            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "Prod Doc",
                MenuItemType = MenuItemType.ProductionScan,
                ViewModelType = typeof(ProductionDocumentListViewModel),
                IsEnabled = true,
            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "Change Instance",
                MenuItemType = MenuItemType.Logout,
                ViewModelType = typeof(SetupViewModel),
                IsEnabled = true,
                IsAlwaysAvailable = true,
            });
            MenuItems.Add(new Models.MenuItem
            {
                Title = "Logout",
                MenuItemType = MenuItemType.Logout,
                ViewModelType = typeof(LoginViewModel),
                IsEnabled = true,
                IsAlwaysAvailable = true,
            });

        }

        private async void OnSelectMenuItem(Models.MenuItem item)
        {
            if (item.IsEnabled && item.ViewModelType != null)
            {
                if (item.MenuItemType == MenuItemType.Logout)
                {
                    Settings.RemoveLoginName();
                    Settings.RemoveLoginPassword();
                    Settings.RemoveUserId();
                    Settings.RemoveIsLoggedIn();
                    Settings.ExcludedMenus = new List<string>();
                    await ChatService.Disconnect();
                }
                item.AfterNavigationAction?.Invoke();
                await NavigationService.NavigateToAsync(item.ViewModelType, item);
            }
        }

        private void SetMenuItemStatus(MenuItemType type, bool enabled)
        {
            Models.MenuItem menuItem = MenuItems.FirstOrDefault(m => m.MenuItemType == type);

            if (menuItem != null)
            {
                menuItem.IsEnabled = enabled;
            }
        }

    }
}
