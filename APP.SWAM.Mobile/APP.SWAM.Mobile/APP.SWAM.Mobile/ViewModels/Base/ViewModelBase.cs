﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.Services.ChatService;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModel.Base
{
    public abstract class ViewModelBase : BindableObject
    {
        private bool _isBusy;

        protected readonly IDialogService DialogService;
        protected readonly INavigationService NavigationService;
        protected readonly IChatService ChatService;
        private IConnectivity connectivity = CrossConnectivity.Current;

        public ViewModelBase()
        {
            PageDialog = UserDialogs.Instance;
            DialogService = Locator.Instance.Resolve<IDialogService>();
            NavigationService = Locator.Instance.Resolve<INavigationService>();
            ChatService = Locator.Instance.Resolve<IChatService>();
            connectivity.ConnectivityChanged += OnConnectivityChanged;
            InitialConnectivity();
            
        }

        private void InitialConnectivity()
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                ShowConnectivityError();
            }
            else
            {
                IsConnected = true;
            }
        }

        private void OnConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            IsConnected = e.IsConnected;

            if (!IsConnected)
            {
                ShowConnectivityError();
            }
        }

        public void ShowConnectivityError()
        {
            var stringResponse = "A problem has occurred with your network connection.";
            this.PageDialog.Toast(stringResponse, TimeSpan.FromSeconds(5));
        }

        public bool IsBusy
        {
            get
            {
                return _isBusy;
            }

            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }

        private bool _isEnabled = true;

        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }

            set
            {
                _isEnabled = value;
                OnPropertyChanged();
            }
        }

        string title = string.Empty;
        public string Title
        {
            get { return title; }
            set { title = value; OnPropertyChanged(); }
        }

        public IUserDialogs PageDialog { get; }

        private bool _isConnected;

        public bool IsConnected
        {
            get { return _isConnected; }
            set { _isConnected = value; OnPropertyChanged(); }
        }

        public virtual Task InitializeAsync(object navigationData)
        {
            return Task.FromResult(false);
        }
    }
}
