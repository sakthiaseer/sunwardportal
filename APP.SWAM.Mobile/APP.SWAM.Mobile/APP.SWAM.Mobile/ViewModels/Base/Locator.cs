﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Text;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModels;
using APP.SWAM.Mobile.Services.ChatService;

namespace APP.SWAM.Mobile.ViewModel.Base
{
    public class Locator
    {
        private IContainer _container;
        private ContainerBuilder _containerBuilder;

        private static readonly Locator _instance = new Locator();

        public static Locator Instance
        {
            get
            {
                return _instance;
            }
        }

        public Locator()
        {
            _containerBuilder = new ContainerBuilder();

            _containerBuilder.RegisterType<DialogService>().As<IDialogService>();
            _containerBuilder.RegisterType<NavigationService>().As<INavigationService>();
            _containerBuilder.RegisterType<ChatService>().As<IChatService>();


            _containerBuilder.RegisterType<LoginViewModel>();
            _containerBuilder.RegisterType<MainViewModel>();
            _containerBuilder.RegisterType<MenuViewModel>();
            _containerBuilder.RegisterType<ExtendedSplashViewModel>();
            _containerBuilder.RegisterType<HomeViewModel>();
            _containerBuilder.RegisterType<AboutViewModel>();
            _containerBuilder.RegisterType<AssetLocationViewModel>();
            _containerBuilder.RegisterType<AssetItemViewModel>();
            _containerBuilder.RegisterType<AssetItemsViewModel>();
            _containerBuilder.RegisterType<PlaywebVideoViewModel>();
            _containerBuilder.RegisterType<ProductionEntryViewModel>();
            _containerBuilder.RegisterType<ProductionOutputViewmodel>();
            _containerBuilder.RegisterType<QcApprovalViewmodel>();
            _containerBuilder.RegisterType<QcApprovalLineItemViewModel>();
            _containerBuilder.RegisterType<StartOfDayViewModel>();
            _containerBuilder.RegisterType<ReassignmentJobViewModel>();
            _containerBuilder.RegisterType<ProcessTransferViewModel>();
            _containerBuilder.RegisterType<ProcessTransferItemViewModel>();
            _containerBuilder.RegisterType<IPIRListItemViewModel>();
            _containerBuilder.RegisterType<IPIRListViewModel>();
            _containerBuilder.RegisterType<CreateIPIRListViewModel>();
            _containerBuilder.RegisterType<CreateIPIRItemViewModel>();
            _containerBuilder.RegisterType<ProcessTransferLineViewModel>();
            _containerBuilder.RegisterType<MultiselectionViewModel>();
            _containerBuilder.RegisterType<ApplicationUserListViewModel>();
            _containerBuilder.RegisterType<BMRToCartonViewModel>();
            _containerBuilder.RegisterType<BMRToCartonLineViewModel>();
            _containerBuilder.RegisterType<BMRToCartonItemViewModel>();
            _containerBuilder.RegisterType<ConsumptionEntryViewModel>();
            _containerBuilder.RegisterType<ConsumptionItemViewModel>();
            _containerBuilder.RegisterType<DrummingViewModel>();
            _containerBuilder.RegisterType<SupervisorDispensingViewModel>();
            _containerBuilder.RegisterType<SupervisorDispensingLineViewModel>();
            _containerBuilder.RegisterType<SupervisorDispensingItemViewModel>();
            _containerBuilder.RegisterType<DispensorDispensingViewModel>();
            _containerBuilder.RegisterType<DispensorDispensingLinesViewModel>();
            _containerBuilder.RegisterType<DispensorDispensingItemViewModel>();
            _containerBuilder.RegisterType<SetupViewModel>();
            _containerBuilder.RegisterType<ScanListViewModel>();
            _containerBuilder.RegisterType<ScannedImagesViewModel>();
            _containerBuilder.RegisterType<ProductionScanDocumentViewModel>();
            _containerBuilder.RegisterType<FileProfileListViewModel>();
            _containerBuilder.RegisterType<ProductionDocumentListViewModel>();
            _containerBuilder.RegisterType<PDFViewerViewModel>();
            _containerBuilder.RegisterType<TransferReclassEntryViewModel>();
            _containerBuilder.RegisterType<TransferReclassLineViewModel>();
            _containerBuilder.RegisterType<MaterialReturnViewModel>();
            _containerBuilder.RegisterType<TransferReclassItemViewModel>();
            _containerBuilder.RegisterType<ChatViewModel>();
            _containerBuilder.RegisterType<ChatListViewModel>();
            _containerBuilder.RegisterType<AboutViewModel>();
            _containerBuilder.RegisterType<CameraViewModel>();
            _containerBuilder.RegisterType<ChatMainViewModel>();
            _containerBuilder.RegisterType<UserListViewModel>();
            _containerBuilder.RegisterType<DispensorScanViewModel>();
            _containerBuilder.RegisterType<TransferProductionItemViewModel>();
            _containerBuilder.RegisterType<TransferProductionLineViewModel>();
            _containerBuilder.RegisterType<TransferProductionViewModel>();
            _containerBuilder.RegisterType<TransferReceiveViewModel>();
            _containerBuilder.RegisterType<TransferReceiveItemViewModel>();
            _containerBuilder.RegisterType<TransferReceiveLineViewModel>();

        }

        public T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        public object Resolve(Type type)
        {
            return _container.Resolve(type);
        }

        public void Register<TInterface, TImplementation>() where TImplementation : TInterface
        {
            _containerBuilder.RegisterType<TImplementation>().As<TInterface>();
        }

        public void Register<T>() where T : class
        {
            _containerBuilder.RegisterType<T>();
        }

        public void Build()
        {
            _container = _containerBuilder.Build();
        }
    }
}
