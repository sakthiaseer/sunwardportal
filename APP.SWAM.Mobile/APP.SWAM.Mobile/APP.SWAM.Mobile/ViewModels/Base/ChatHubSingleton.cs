﻿using APP.SWAM.Mobile.Helpers;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.ViewModels.Base
{
    public sealed class ChatHubSingleton
    {
        private static readonly ChatHubSingleton _instance = new ChatHubSingleton();

        private ChatHubSingleton()
        {
            Connection = new HubConnectionBuilder()
                      .WithUrl(Constant.ChatHubUrl)
                      .Build();
        }

        public static ChatHubSingleton GetChatHubInstance()
        {
            return _instance;
        }

        public HubConnection Connection { get; }

        public String ConnectionID { get; set; }

    }
}
