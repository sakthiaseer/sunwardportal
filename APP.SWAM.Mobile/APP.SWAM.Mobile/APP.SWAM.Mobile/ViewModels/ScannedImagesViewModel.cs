﻿using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class ScannedImagesViewModel : ViewModelBase
    {
        QcApprovalModel qcApprovalModel = new QcApprovalModel();
        ProductionEntryModel productionEntryModel = new ProductionEntryModel();
        public ScannedImagesViewModel()
        {
            ScanCommand = new Command(OnScanItem);
            PostCommand = new Command(OnPost);
            FileProfileTypeCommand = new Command(OnFileProfileTypes);
            Model = new ScanDocumentModel
            {
                AddedByUserId = Settings.UserId,
            };
            LoadChoices();
            //MessagingCenter.Subscribe<FileProfileTypeMobile>(this, "AddSelectedProfile", mSelection =>
            //{
            //    Model.ProfileTypeName = mSelection.Name;
            //    Model.FileProfileTypeID = mSelection.FileProfileTypeId;
            //});
        }

        public override Task InitializeAsync(object navigationData)
        {
            if (navigationData is QcApprovalViewmodel)
            {
                qcApprovalModel = (navigationData as QcApprovalViewmodel).Model;
                Model.SessionId = qcApprovalModel.SessionId.Value;
            }
            else if (navigationData is ProductionEntryViewModel)
            {
                productionEntryModel = (navigationData as ProductionEntryViewModel).SelectedProductionEntry;
                Model.SessionId = productionEntryModel.SessionId.Value;
            }
            return base.InitializeAsync(navigationData);


        }

        private async void LoadChoices()
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                fileProfileTypes = await restClient.Get<FileProfileTypeMobile>("FileProfileType/GetFileProfileTypeMobileItems");
                FileProfileTypes = fileProfileTypes.Select(q => q.Name).ToList();
            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            PageDialog.HideLoading();
        }

        private async void OnFileProfileTypes()
        {
            FileProfileListViewModel fileProfileListViewModel = new FileProfileListViewModel();
            fileProfileListViewModel.ScreenName = "FileProfile";
            await NavigationService.NavigateToAsync<FileProfileListViewModel>(fileProfileListViewModel);
        }

        private List<string> _fileProfileTypes = new List<string>();
        public List<string> FileProfileTypes
        {
            get { return _fileProfileTypes; }
            set { _fileProfileTypes = value; OnPropertyChanged(nameof(FileProfileTypes)); }

        }

        List<FileProfileTypeMobile> fileProfileTypes = new List<FileProfileTypeMobile>();

        private ScanDocumentModel _model = new ScanDocumentModel();

        public ScanDocumentModel Model
        {
            get { return _model ?? (_model = new ScanDocumentModel()); }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        private ObservableCollection<ItemModel> _items = new ObservableCollection<ItemModel>();
        public ObservableCollection<ItemModel> Items
        {
            get { return _items; }
            set { _items = value; OnPropertyChanged("Items"); }
        }

        public Command ScanCommand { get; set; }
        public Command PostCommand { get; set; }
        public Command FileProfileTypeCommand { get; set; }

        private async void OnScanItem()
        {
            Image image = new Image();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DialogService.ShowConfirmAsync("No Camera", ":( No camera available.", "OK", "Cancel");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 30,
                //CustomPhotoSize = 50,
                PhotoSize = PhotoSize.Medium,
                //MaxWidthHeight = 2000,
                DefaultCamera = CameraDevice.Rear
            });

            if (file == null)
                return;

            ItemModel itemModel = new ItemModel();
            ItemModel item = new ItemModel();
            itemModel.Id = Items.Count + 1;
            item.Id = Items.Count + 1;
            itemModel.ImageByte = ReadFully(file.GetStream());
            item.ImageByte = ReadFully(file.GetStream());
            itemModel.FileName = Guid.NewGuid().ToString() + ".png";
            item.FileName = Guid.NewGuid().ToString() + ".png";
            ImageSource PhotoSource = ImageSource.FromStream(() =>
            {
                return new MemoryStream(itemModel.ImageByte);
            });
            itemModel.Image = PhotoSource;
            item.Image = PhotoSource; ;
            Model.Items.Add(itemModel);
            Items.Add(itemModel);
        }

        private async void OnPost()
        {
            PageDialog.ShowLoading();
            try
            {
                if (Model.IsValid())
                {

                    RestClient restClient = new RestClient();
                    foreach (var item in Model.Items)
                    {
                        item.Image = null;
                    }
                    Model.FileProfileTypeID = fileProfileTypes.FirstOrDefault(m => m.Name.Trim().ToLower() == Model.ProfileTypeName.Trim().ToLower()) != null ? fileProfileTypes.FirstOrDefault(m => m.Name.Trim().ToLower() == Model.ProfileTypeName.Trim().ToLower()).FileProfileTypeId : 0;
                    if (Model.Items.Count > 0)
                    {
                        var scanDocumentModel = await restClient.PostAsync<ScanDocumentModel>("Documents/ScanByteToPDF", Model);

                        if (scanDocumentModel.DocumentID > 0)
                        {
                            MessagingCenter.Send(Model, "ReloadScanItems");
                            await NavigationService.NavigateBackAsync();
                        }
                    }
                    else
                    {
                        await PageDialog.AlertAsync("Error", "Please Scan atleast one Image", "OK");
                        PageDialog.HideLoading();
                    }
                }
                else
                {
                    await DialogService.ShowConfirmAsync("Please key-in all the required fields", "Validation", "OK", "Cancel");
                }

            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            PageDialog.HideLoading();

        }

        public byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }


    }
}
