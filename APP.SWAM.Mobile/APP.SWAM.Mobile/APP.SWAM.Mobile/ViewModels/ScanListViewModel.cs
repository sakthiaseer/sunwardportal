﻿using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class ScanListViewModel : ViewModelBase
    {
        public ICommand ScanCommand { get; set; }

        public ICommand SelectedItemLine { get; set; }

        ObservableCollection<ScanDocumentModel> _modelEntries = new ObservableCollection<ScanDocumentModel>();
        public ObservableCollection<ScanDocumentModel> ModelEntries
        {
            get { return _modelEntries; }
            set { _modelEntries = value; OnPropertyChanged(nameof(ModelEntries)); }
        }

        ScanDocumentModel _modelEntry = new ScanDocumentModel();
        public ScanDocumentModel SelectedModel
        {
            get { return _modelEntry; }
            set
            {
                _modelEntry = value;
                OnPropertyChanged(nameof(SelectedModel));
            }
        }
        public ScanListViewModel()
        {
            ScanCommand = new Command(OnScanNewItem);
            SelectedItemLine = new Command(OnSelected);
            SelectedModel = new ScanDocumentModel
            {
                AddedByUserId = Settings.UserId,
            };

            MessagingCenter.Subscribe<ScanDocumentModel>(this, "ReloadScanItems", jobModel =>
            {
                LoadScanItems();
            });
            LoadScanItems();
        }
        public override Task InitializeAsync(object navigationData)
        {
            return base.InitializeAsync(navigationData);
        }

        async void OnScanNewItem()
        {
            await NavigationService.NavigateToAsync<ScannedImagesViewModel>(this);
        }

        async void OnSelected()
        {
            if (SelectedModel.DocumentID > 0)
            {
                //await NavigationService.NavigateToAsync<PDFViewerViewModel>(SelectedModel);
                if (SelectedModel.DocumentID > 0)
                {
                    //MessagingCenter.Send(SelectedModel, "LoadPDF");
                    //MessagingCenter.Subscribe<ScanDocumentModel>(this, "LoadPDF", jobModel =>
                    //{
                    
                    if (Settings.DBName == "LIVE")
                    {
                        Device.OpenUri(new Uri(Constant.LIVEURL + SelectedModel.DocumentName));
                    }
                    else
                    {
                        Device.OpenUri(new Uri(Constant.DEVURL + SelectedModel.DocumentName));
                    }
                    //});
                }
            }
        }

        private async void LoadScanItems()
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                var scanDocuments = await restClient.Get<ScanDocumentModel>("Documents/GetMobileDocuments", SelectedModel.AddedByUserId.Value);
                scanDocuments.ForEach(s =>
                {
                    ModelEntries.Add(s);
                });
            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
            }
            PageDialog.HideLoading();
        }
    }
}
