﻿using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.ViewModel;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class SetupViewModel : ViewModelBase
    {
        private Dictionary<string, string> dbDictionary = new Dictionary<string, string>();
        public Command SaveCommand { get; set; }
        private string _dbName;
        private string _NavName;
        private List<string> _dbNames;
        private List<string> _NavNames;

        public List<string> NAVNames
        {
            get { return _NavNames; }
            set { _NavNames = value; OnPropertyChanged(nameof(NAVNames)); }

        }
        public string NAVName
        {
            get { return _NavName; }
            set { _NavName = value; OnPropertyChanged(nameof(NAVName)); }

        }

        public List<string> DBNames
        {
            get { return _dbNames; }
            set { _dbNames = value; OnPropertyChanged(nameof(DBNames)); }

        }

        public string DBName
        {
            get { return _dbName; }
            set { _dbName = value; OnPropertyChanged(nameof(DBName)); }

        }
        public Dictionary<string, string> DBDictionary
        {
            get { return dbDictionary; }
            set { dbDictionary = value; OnPropertyChanged(nameof(DBDictionary)); }

        }
        public override Task InitializeAsync(object navigationData)
        {
            return base.InitializeAsync(navigationData);
        }
        public SetupViewModel()
        {
            SaveCommand = new Command(OnSave);
            DBNames = new List<string> { "DEV", "UAT", "LIVE" };
            NAVNames = new List<string> { "NAV_SG", "NAV_JB" };
            NAVName = string.IsNullOrEmpty(Settings.NAVName) ? "NAV_SG" : Settings.NAVName;
            DBName = string.IsNullOrEmpty(Settings.DBName) ? "LIVE" : Settings.DBName;
#if DEBUG
            DBDictionary = new Dictionary<string, string>
            {
                { "DEV", "http://192.168.1.9/swapi/api/" },
                { "UAT", "http://192.168.1.9/swapi/api/" },
                { "LIVE", "https://portal.sunwardpharma.com/tmsapi/api/" }
            };

#else
            DBDictionary = new Dictionary<string, string>
            {
                { "DEV", "https://portal.sunwardpharma.com/tmsapidev/api/" },
                { "UAT", "https://portal.sunwardpharma.com/tmsapidev/api/" },
                { "LIVE", "https://portal.sunwardpharma.com/tmsapi/api/" }
            };
          
#endif
        }
        async void OnSave()
        {
            if (!string.IsNullOrEmpty(DBName) && !string.IsNullOrEmpty(NAVName))
            {
                Settings.DBName = DBName;
                Settings.NAVName = NAVName;
                Settings.RestServiceURL = dbDictionary[DBName];
                Constant.RestUrl = dbDictionary[DBName];
                await NavigationService.NavigateToAsync<LoginViewModel>();
            }
        }
    }
}
