﻿using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace APP.SWAM.Mobile.ViewModel
{
    public class ExtendedSplashViewModel : ViewModelBase
    {
        public override async Task InitializeAsync(object navigationData)
        {
            IsBusy = true;
            await Task.Delay(3000);
            await NavigationService.InitializeAsync();

            IsBusy = false;
        }
    }
}
