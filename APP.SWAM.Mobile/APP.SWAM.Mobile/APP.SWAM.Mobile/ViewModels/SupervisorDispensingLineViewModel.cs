﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class SupervisorDispensingLineViewModel : ViewModelBase
    {
        #region fields

        private SupervisorDispensingModel _model;
        private ObservableCollection<SupervisorDispensingModel> _modelEntries = new ObservableCollection<SupervisorDispensingModel>();
        private bool isRefreshing;
        private long _drummingId;
        private SupervisorDispensingModel _selectedItem;
        #endregion

        #region Properties

        public SupervisorDispensingModel SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value; OnPropertyChanged(nameof(SelectedItem));
            }
        }

        public ObservableCollection<SupervisorDispensingModel> DispensingEntries
        {
            get { return _modelEntries; }
            set { _modelEntries = value; OnPropertyChanged(nameof(DispensingEntries)); }
        }

        public SupervisorDispensingModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }

        public long SupervisorDispensingId
        {
            get { return _drummingId; }
            set { _drummingId = value; OnPropertyChanged(nameof(SupervisorDispensingId)); }
        }


        public Command AddLineCommand { get; set; }
        public Command DeleteCommand { get; set; }
        public ICommand PostCommand { get; set; }
        public ICommand SelectedItemLine { get; set; }
        #endregion

        public SupervisorDispensingLineViewModel()
        {
            AddLineCommand = new Command(OnAdd);
            PostCommand = new Command(OnPost);
            DeleteCommand = new Command(OnDelete);
            SelectedItemLine = new Command(OnSelected);
            Model = new SupervisorDispensingModel
            {
                LoginUser = Constant.Username,
                StartDate = DateTime.Now,
            };
        }

        public override Task InitializeAsync(object navigationData)
        {
            Model = (navigationData as SupervisorDispensingModel);
            if (!string.IsNullOrEmpty(Model.WrokOrderNo))
            {
                SupervisorDispensingId = Model.SupervisorDispensingId;
                FillData();
            }
            return base.InitializeAsync(navigationData);
        }
        public async void FillData()
        {
            UserDialogs.Instance.ShowLoading("Loading...");
            var restclient = new RestClient();
            var entries = await restclient.Get<SupervisorDispensingModel>("APPSupervisorDispensingEntry/GetDispensingEntryByID", Model.WrokOrderNo);

            DispensingEntries = new ObservableCollection<SupervisorDispensingModel>(entries);
            UserDialogs.Instance.HideLoading();
        }
        async void OnDelete()
        {
            if (SelectedItem != null && SelectedItem.SupervisorDispensingId > 0)
            {
                IsBusy = true;
                IsEnabled = false;
                UserDialogs.Instance.ShowLoading("Deleting Dispensing entry...");
                var restclient = new RestClient();
                var result = await restclient.DeleteAsync<bool>("APPSupervisorDispensingEntry/DeleteDispensingEntry", SelectedItem.SupervisorDispensingId);
                if (result)
                {
                    FillData();
                }
                else
                {
                    await DialogService.ShowConfirmAsync("Delete operation failed.!", "DELETE", "OK", "Cancel");
                }
                IsBusy = false;
                IsEnabled = true;
                UserDialogs.Instance.HideLoading();
            }
            else
            {
                await DialogService.ShowConfirmAsync("Select record to delete.", "DELETE", "OK", "Cancel");
            }
        }
        async void OnSelected()
        {

        }
        async void OnPost()
        {
            IsBusy = true;
            IsEnabled = false;

            UserDialogs.Instance.ShowLoading("Posting Consumption into NAV...");
            Model.AddedByUserId = Settings.UserId;
            Model.SupervisorDispensingId = SupervisorDispensingId;
            var restclient = new RestClient();
            var result = await restclient.PostAsync<SupervisorDispensingModel>("APPSupervisorDispensingEntry/PostDispensing", Model);
            if (result.SupervisorDispensingId > 0)
                await DialogService.ShowConfirmAsync("Record posted successfully.", "POST", "OK", "Cancel");
            else
                await DialogService.ShowConfirmAsync("Posting failed.", "POST", "OK", "Cancel");
            IsBusy = false;
            IsEnabled = true;
            UserDialogs.Instance.HideLoading();
        }
        async void OnAdd()
        {
            await NavigationService.NavigateToAsync<SupervisorDispensingItemViewModel>(this);
        }

    }
}
