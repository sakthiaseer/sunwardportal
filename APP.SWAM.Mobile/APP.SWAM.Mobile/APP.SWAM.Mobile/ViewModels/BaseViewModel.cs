﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;

namespace APP.SWAM.Mobile.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public IDataStore<Item> DataStore => DependencyService.Get<IDataStore<Item>>() ?? new MockDataStore();

        bool isBusy = false;
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

        string _errormessage = string.Empty;
        public string Errormessage
        {
            get { return _errormessage; }
            set { SetProperty(ref _errormessage, value); }
        }
        bool _IsError = false;
        public bool IsError
        {
            get { return _IsError; }
            set { SetProperty(ref _IsError, value); }
        }

        private long? _loginUserId;
        public long? AddedByUserId { get { return _loginUserId; } set { SetProperty(ref _loginUserId, value); } }

        private long? _postedUserID;
        public long? PostedUserID { get { return _postedUserID; } set { SetProperty(ref _postedUserID, value); } }

        string title = string.Empty;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }
        string companyName = string.Empty;
        public string CompanyName
        {
            get { return companyName; }
            set { SetProperty(ref companyName, value); }
        }

        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName]string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
