﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.MultiSelectListView;

namespace APP.SWAM.Mobile.ViewModels
{
    public class StartOfDayViewModel : ViewModelBase
    {
        #region fields
        private ObservableCollection<StartOfDayModel> startOfDayModels = new ObservableCollection<StartOfDayModel>();
        private StartOfDayModel _model;
        private bool isRefreshing;
        private long startOfDayId;
        #endregion

        #region Properties
        public ObservableCollection<StartOfDayModel> StartOfDayModels
        {
            get { return startOfDayModels; }
            set { startOfDayModels = value; OnPropertyChanged(nameof(startOfDayModels)); }
        }

        public StartOfDayModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }

        public long StartOfDayId
        {
            get { return startOfDayId; }
            set { startOfDayId = value; OnPropertyChanged(nameof(StartOfDayId)); }
        }

        private List<string> _workingBlocks = new List<string>();
        public List<string> WorkingBlocks
        {
            get { return _workingBlocks; }
            set { _workingBlocks = value; OnPropertyChanged(nameof(WorkingBlocks)); }

        }
        List<ApplicationMasterDetailModel> workingBlockItems = new List<ApplicationMasterDetailModel>();

        private List<string> _prodTasks = new List<string>();
        public List<string> ProductionTasks
        {
            get { return _prodTasks; }
            set { _prodTasks = value; OnPropertyChanged(nameof(ProductionTasks)); }

        }
        List<OperationProcedureItem> prodTaskItems = new List<OperationProcedureItem>();

        private List<string> _manPowers = new List<string>();
        public List<string> ManPowers
        {
            get { return _manPowers; }
            set { _manPowers = value; OnPropertyChanged(nameof(ManPowers)); }

        }
        List<ManPowerModel> manPowerItems = new List<ManPowerModel>();
        public Command ProdTaskCommand { get; set; }
        public Command ManPowerCommand { get; set; }
        public Command SaveStartOfDayCommand { get; set; }
        public Command ExitCommand { get; set; }
        #endregion

        public override Task InitializeAsync(object navigationData)
        {
            return base.InitializeAsync(navigationData);
        }

        public StartOfDayViewModel()
        {
            SaveStartOfDayCommand = new Command(Save);
            ExitCommand = new Command(OnExit);
            ProdTaskCommand = new Command(OnProdTask);
            ManPowerCommand = new Command(OnManPower);
            Model = new StartOfDayModel
            {
                LoginUser = Settings.UserName,
                StartDate = DateTime.Now,
            };
            Title = "Start of Day" + "(" + Settings.DBName + ")";
            MessagingCenter.Subscribe<MultiSelectionModel>(this, "AddProdTaskSelectionItems", mSelection =>
            {
                foreach (var item in mSelection.MultiselectionItems)
                {
                    if (item.IsSelected)
                    {
                        Model.ProdTaskIds.Add(item.Data.Id);
                        Model.ProdTask += item.Data.Title + ",";
                    }
                }
            });
            MessagingCenter.Subscribe<MultiSelectionModel>(this, "AddManPowerSelectionItems", mSelection =>
            {
                foreach (var item in mSelection.MultiselectionItems)
                {
                    if (item.IsSelected)
                    {
                        Model.ManPowerIds.Add(item.Data.Id);
                        Model.ManPower += item.Data.Title + ",";
                    }
                }
            });
            LoadChoices();
        }

        private async void LoadChoices()
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                workingBlockItems = await restClient.GetApplicationMasterByType<ApplicationMasterDetailModel>("ApplicationMaster/GetApplicationMasterDetailByType", 283);
                WorkingBlocks = workingBlockItems.Select(q => q.Value).ToList();
                prodTaskItems = await restClient.Get<OperationProcedureItem>("OperationProcedure/GetOperationProcedureItems");
                ProductionTasks = prodTaskItems.Select(q => q.Name).ToList();
                manPowerItems = await restClient.Get<ManPowerModel>("HumanMovement/GetManPowerBySage");
                ManPowers = manPowerItems.Select(q => q.Name).ToList();

            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            PageDialog.HideLoading();
        }
        private async void OnProdTask()
        {
            MultiSelectObservableCollection<MultiSelectionModel> items = new MultiSelectObservableCollection<MultiSelectionModel>();
            prodTaskItems.ForEach(p =>
            {
                items.Add(new MultiSelectionModel
                {
                    Id = p.Id,
                    Title = p.Name,
                });
            });
            MultiSelectionModel multiSelectionModel = new MultiSelectionModel
            {
                MultiSelectionName = "ProdTask",
                MultiselectionItems = items
            };
            await NavigationService.NavigateToAsync<MultiselectionViewModel>(multiSelectionModel);
        }

        private async void OnManPower()
        {
            MultiSelectObservableCollection<MultiSelectionModel> items = new MultiSelectObservableCollection<MultiSelectionModel>();
            manPowerItems.ForEach(p =>
            {
                items.Add(new MultiSelectionModel
                {
                    Id = p.EmployeeID.Value,
                    Title = p.Name,
                });
            });
            MultiSelectionModel multiSelectionModel = new MultiSelectionModel
            {
                MultiSelectionName = "ManPower",
                MultiselectionItems = items
            };
            await NavigationService.NavigateToAsync<MultiselectionViewModel>(multiSelectionModel);
        }
        async void Save()
        {
            try
            {
                if (Model.IsValid())
                {
                    IsBusy = true;
                    IsEnabled = false;
                    UserDialogs.Instance.ShowLoading("Saving...");
                    Model.CompanyName = Settings.NAVName;
                    Model.AddedByUserId = Settings.UserId;
                    Model.PostedUserID = Settings.UserId;
                    Model.WorkingBlockId = workingBlockItems.FirstOrDefault(m => m.Value == Model.WorkingBlock) != null ? workingBlockItems.FirstOrDefault(m => m.Value == Model.WorkingBlock).ApplicationMasterDetailId as long? : null;
                    

                    var restclient = new RestClient();
                    var result = await restclient.PostAsync<StartOfDayModel>("StartOfDay/InsertStartOfDay", Model);
                    if (result != null && !result.IsError)
                    {

                        await DialogService.ShowConfirmAsync("Record saved successfully.", "SAVE", "OK", "Cancel");
                        OnExit();
                    }
                    else
                    {
                        await DialogService.ShowConfirmAsync("Record insertion failed." + result.Errormessage, "Error", "OK", "Cancel");
                    }
                    IsBusy = false;
                    IsEnabled = true;
                    UserDialogs.Instance.HideLoading();
                }
                else
                {
                    await DialogService.ShowConfirmAsync("Please key-in all the required fields", "Validation", "OK", "Cancel");
                }
            }
            catch (Exception ex)
            {
                await DialogService.ShowConfirmAsync(ex.Message, "ERROR", "OK", "Cancel");
            }
        }

        void OnExit()
        {
            Model.LoginUser = Settings.UserName;
            Model.StartDate = DateTime.Now;
            Model.ManPowerIds = new List<long>();
            Model.WorkingBlockId = 0;
            Model.StartOfDayId = -1;
            Model.ProdTaskIds = new List<long>();
            Model.WorkingBlock = string.Empty;
            Model.ProdTask = string.Empty;
            Model.ManPower = string.Empty;
        }

    }
}
