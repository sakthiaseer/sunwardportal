﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class CreateIPIRListViewModel : ViewModelBase
    {
        #region fields
        private ObservableCollection<IPIRModel> _modelEntries = new ObservableCollection<IPIRModel>();
        private IPIRModel _modelEntry = new IPIRModel();
        private bool isRefreshing;
        private long _ipirMobileId;
        #endregion

        #region Properties
        public ObservableCollection<IPIRModel> ModelEntries
        {
            get { return _modelEntries; }
            set { _modelEntries = value; OnPropertyChanged(nameof(ModelEntries)); }
        }

        public IPIRModel SelectedModel
        {
            get { return _modelEntry; }
            set
            {
                _modelEntry = value;
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }

        public long IpirMobileId
        {
            get { return _ipirMobileId; }
            set { _ipirMobileId = value; OnPropertyChanged(nameof(IpirMobileId)); }
        }
        public ICommand OpenCommand { get; set; }

        public ICommand AddNewCommand { get; set; }
        public ICommand TextboxFocusedEvent { get; set; }

        #endregion

        public override Task InitializeAsync(object navigationData)
        {
            FillData();
            return base.InitializeAsync(navigationData);
        }
        public async void FillData()
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...");
                var restclient = new RestClient();
                var entries = await restclient.Get<IPIRModel>("IPIRMobile/GetIPIRMobileItems");

                ModelEntries = new ObservableCollection<IPIRModel>(entries);
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
                await DialogService.ShowConfirmAsync(ex.Message, "Load", "OK", "Cancel");
            }
        }
        public CreateIPIRListViewModel()
        {
            OpenCommand = new Command(OnOpenItem);
        }
        async void OnOpenItem()
        {
            SelectedModel.LoginUser = Constant.Username;
            SelectedModel.StartDate = DateTime.Now;
            SelectedModel.CompanyName = Settings.NAVName;
            await NavigationService.NavigateToAsync<CreateIPIRItemViewModel>(SelectedModel);
        }
    }
}