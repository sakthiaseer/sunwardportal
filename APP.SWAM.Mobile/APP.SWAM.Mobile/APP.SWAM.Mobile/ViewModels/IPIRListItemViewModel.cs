﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using ZXing.Mobile;

namespace APP.SWAM.Mobile.ViewModels
{
    public class IPIRListItemViewModel : ViewModelBase
    {
        #region fields
        private IPIRModel _model;

        private bool isRefreshing;
        #endregion

        #region Properties

        public IPIRModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }


        public Command SaveCommand { get; set; }
        public Command ClearCommand { get; set; }
        public Command ProdCommand { get; set; }
        public Command LocationScanCommand { get; set; }

        private ImageSource _frontPhotoSource;
        public ImageSource FrontPhotoSource { get { return _frontPhotoSource; } set { _frontPhotoSource = value; OnPropertyChanged(nameof(FrontPhotoSource)); } }
        public Command TakeFrontPhotoCommand { get; set; }

        private List<string> _intendedItems = new List<string>();
        public List<string> IntendedItems
        {
            get { return _intendedItems; }
            set { _intendedItems = value; OnPropertyChanged(nameof(IntendedItems)); }
        }

        List<CodeMasterModel> codeMasters = new List<CodeMasterModel>();

        #endregion

        public IPIRListItemViewModel()
        {
            SaveCommand = new Command(OnSave);
            ClearCommand = new Command(OnExit);
            ProdCommand = new Command(OnProdScan);
            LocationScanCommand = new Command(OnLocationScan);
            TakeFrontPhotoCommand = new Command(OnTakeFrontPhoto);
            Title = "IPIR Item " + "(" + Settings.DBName + ")";
            Model = new IPIRModel
            {
                LoginUser = Constant.Username,
                StartDate = DateTime.Now,
                CompanyName = Settings.NAVName,
            };
            
            if (Settings.DBName == "LIVE")
            {
                FrontPhotoSource = ImageSource.FromUri(new Uri(Constant.LIVEURL + "/Noimages.png"));
            }
            else
            {
                FrontPhotoSource = ImageSource.FromUri(new Uri(Constant.DEVURL + "/Noimages.png"));
            }
            LoadChoices();
        }

        public override Task InitializeAsync(object navigationData)
        {
            Model = (navigationData as IPIRModel);
            return base.InitializeAsync(navigationData);
        }

        private async void LoadChoices()
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                codeMasters = await restClient.GetCodeMasterByType<CodeMasterModel>("ApplicationUser/GetCodeMasterByType", "IntendedAction");
                IntendedItems = codeMasters.Select(q => q.CodeValue).ToList();
            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            PageDialog.HideLoading();
        }
        void OnProdScan()
        {
#if DEBUG
            string qrCode = "MT18-1945-4-GR1~10000~Fenadium F Tablet Granules~704-19005~1~MT18-1945";

            var barcode = qrCode.Split('~');
            if (barcode.Length > 4)
            {
                Model.ProductionOrderNo = barcode[0].Trim();
                Model.ProductName = barcode[2].Trim();
                Model.BatchNo = barcode[3].Trim();
            }

#else
            if (string.IsNullOrEmpty(Model.Location))
            {
                var scanner = new MobileBarcodeScanner();
                scanner.Torch(true);
                scanner.AutoFocus();

                scanner.ScanContinuously(s =>
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        if (Model == null)
                            Model = new IPIRModel();
                        var barcode = s.Text.Split('~');
                        if (barcode.Length > 3)
                        {
                            Model.ProductionOrderNo = barcode[0].Trim();
                            Model.ProductName = barcode[2].Trim();
                            Model.BatchNo = barcode[3].Trim();
                        }
                        else
                        {
                            await DialogService.ShowConfirmAsync("In-Valid QR Code.QR Code must contains 4 segment(ex:Drum Info,Process Info,Weight).Please scan valid QR Code", "QR Code", "OK", "Cancel");
 
                        }
                    });
                    scanner.Cancel();
                    scanner.Torch(false);
                });


            }
#endif
        }

        void OnLocationScan()
        {
#if DEBUG
            Model.Location = "SWMY";
#else
            if (string.IsNullOrEmpty(Model.Location))
            {
                var scanner = new MobileBarcodeScanner();
                scanner.Torch(true);
                scanner.AutoFocus();

                scanner.ScanContinuously(s =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (Model == null)
                            Model = new IPIRModel();
                        Model.Location = s.Text;
                    });
                    scanner.Cancel();
                    scanner.Torch(false);
                });
            }
#endif
        }
        async void OnSave()
        {
            if (Model.IsValid())
            {
                IsBusy = true;
                IsEnabled = false;

                UserDialogs.Instance.ShowLoading("Validating IPIR...");
                Model.AddedByUserId = Settings.UserId;
                Model.IntendedActionId = codeMasters.FirstOrDefault(m => m.CodeValue == Model.IntendedAction) != null ? codeMasters.FirstOrDefault(m => m.CodeValue == Model.IntendedAction).CodeID as int? : null;
                Model.CompanyName = Settings.NAVName;
                var restclient = new RestClient();
                Model = await restclient.PostAsync<IPIRModel>("IPIRMobile/InsertIpirMobile", Model);

                IsBusy = false;
                IsEnabled = true;
                UserDialogs.Instance.HideLoading();
                if (Model.IpirmobileId > 0)
                {
                    await DialogService.ShowConfirmAsync("Record validated successfully.", "Validation", "OK", "Cancel");
                    MessagingCenter.Send(Model, "ReloadIPIRItems");
                    await NavigationService.NavigateBackAsync();
                }
                else
                {
                    await DialogService.ShowConfirmAsync(Model.Errormessage, "Validation", "OK", "Cancel");
                }

            }
            else
            {
                await DialogService.ShowConfirmAsync("Validation Failed.", "Next", "OK", "Cancel");
            }
        }
        async void OnTakeFrontPhoto()
        {
            Image image = new Image();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DialogService.ShowConfirmAsync("No Camera", ":( No camera available.", "OK", "Cancel");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 30,
                //CustomPhotoSize = 50,
                PhotoSize = PhotoSize.Medium,
                //MaxWidthHeight = 2000,
                DefaultCamera = CameraDevice.Rear
            });

            if (file == null)
                return;
            Model.PhotoSource = ReadFully(file.GetStream());
            Model.Photo = Guid.NewGuid().ToString() + ".png";
            FrontPhotoSource = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                file.Dispose();
                return stream;
            });
        }
        public byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        void OnExit()
        {
            Model = new IPIRModel
            {
                LoginUser = Constant.Username,
                StartDate = DateTime.Now,
                BatchNo = string.Empty,
                CompanyId = null,
                CompanyName = string.Empty,
                IntendedAction = string.Empty,
                IssueDescription = string.Empty,
                IssueDescription1 = string.Empty,
                IssueDescription2 = string.Empty,
                IntendedActionId = null,
                IpirmobileId = 0,
                IpirreportNo = string.Empty,
                Location = string.Empty,
                Photo = string.Empty,
                PhotoSource = null,
                ProductionOrderNo = string.Empty,
                ProductName = string.Empty
            };
        }
    }
}