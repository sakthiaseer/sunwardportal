﻿using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class SupervisorDispensingViewModel : ViewModelBase
    {
        #region fields

        private SupervisorDispensingModel _model;
        private bool isRefreshing;
        private long _drummingId;
        #endregion

        #region Properties


        public SupervisorDispensingModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }

        public long SupervisorDispensingId
        {
            get { return _drummingId; }
            set { _drummingId = value; OnPropertyChanged(nameof(SupervisorDispensingId)); }
        }


        public Command SaveCommand { get; set; }
        public Command ClearCommand { get; set; }
        public ICommand FocusedCommand { get; set; }

        #endregion

        public SupervisorDispensingViewModel()
        {
            SaveCommand = new Command(OnSave);
            ClearCommand = new Command(OnExit);
            FocusedCommand = new Command(OnProdScan);
            Model = new SupervisorDispensingModel
            {
                LoginUser = Settings.UserName,
                StartDate = DateTime.Now,
            };
        }

        public override Task InitializeAsync(object navigationData)
        {
            return base.InitializeAsync(navigationData);
        }

        void OnProdScan()
        {
#if DEBUG
            string qrCode = "WO19-M0258~RM19-M0258~Prozine Syrup 50X120ML~3";

            var barcode = qrCode.Split('~');
            if (barcode.Length > 3)
            {
                Model.WrokOrderNo = barcode[0].Trim();
                Model.ItemName = barcode[1].Trim();
                Model.Description = barcode[2].Trim();
                Model.TotalMatWeight = int.Parse(barcode[3].Trim());

            }
#endif
        }
        async void OnSave()
        {
            if (Model.IsValidWork())
            {
                await NavigationService.NavigateToAsync<SupervisorDispensingLineViewModel >(Model);
            }
            else
            {
                await DialogService.ShowConfirmAsync("Validation Failed. Please scan work order.", "Next", "OK", "Cancel");
            }
        }
        void OnExit()
        {
            Model = new SupervisorDispensingModel
            {
                LoginUser = Settings.UserName,
                StartDate = DateTime.Now,
                PreLineClearance = false,
                WrokOrderNo = string.Empty,
                ItemName = string.Empty,
                Description = string.Empty,
            };
        }
    }
}
