﻿using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using ZXing.Mobile;

namespace APP.SWAM.Mobile.ViewModels
{
    public class ProcessTransferViewModel : ViewModelBase
    {
        #region fields

        private ProcessTransferModel _model;
        private bool isRefreshing;
        private long _processTransferId;
        #endregion

        #region Properties


        public ProcessTransferModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }

        public long ProcessTransferId
        {
            get { return _processTransferId; }
            set { _processTransferId = value; OnPropertyChanged(nameof(ProcessTransferId)); }
        }


        public Command SaveCommand { get; set; }
        public Command ClearCommand { get; set; }
        public Command FocusedCommand { get; set; }
        public Command LocationToCommand { get; set; }

        #endregion

        public ProcessTransferViewModel()
        {
            SaveCommand = new Command(OnSave);
            ClearCommand = new Command(OnExit);
            FocusedCommand = new Command(OnLocationScan);
            LocationToCommand = new Command(OnLocationTo);
            Model = new ProcessTransferModel
            {
                LoginUser = Settings.UserName,
                StartDate = DateTime.Now,
                AddedByUserId = Settings.UserId,
            };
            Title = "Process Transfer " + "(" + Settings.DBName + ")";
        }

        public override Task InitializeAsync(object navigationData)
        {
            return base.InitializeAsync(navigationData);
        }

        void OnLocationScan()
        {
#if DEBUG
            string qrCode = "SWMY";
            Model.LocationFrom = qrCode;
#else
            if (string.IsNullOrEmpty(Model.LocationFrom))
            {
                var scanner = new MobileBarcodeScanner();
                scanner.Torch(true);
                scanner.AutoFocus();

                scanner.ScanContinuously(s =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (Model == null)
                            Model = new ProcessTransferModel();
                        Model.LocationFrom = s.Text;
                    });
                    scanner.Cancel();
                    scanner.Torch(false);
                });
            }
#endif
        }

        void OnLocationTo()
        {
#if DEBUG
            string qrCode = "SWMY";
            Model.LocationTo = qrCode;
#else
            if (string.IsNullOrEmpty(Model.LocationTo))
            {
                var scanner = new MobileBarcodeScanner();
                scanner.Torch(true);
                scanner.AutoFocus();

                scanner.ScanContinuously(s =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (Model == null)
                            Model = new ProcessTransferModel();
                        Model.LocationTo = s.Text;
                    });
                    scanner.Cancel();
                    scanner.Torch(false);
                });
            }
#endif
        }
        async void OnSave()
        {
            if (Model.IsValid())
            {
                await CreateProcessTransfer();
                await NavigationService.NavigateToAsync<ProcessTransferLineViewModel>(Model);
            }
            else
            {
                await DialogService.ShowConfirmAsync("Validation Failed. Please scan work order.", "Next", "OK", "Cancel");
            }
        }

        private async Task CreateProcessTransfer()
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                Model = await restClient.PostAsync("ProcessTransfer/InsertProcessTransfer", Model);
            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            PageDialog.HideLoading();
        }

        void OnExit()
        {
            Model = new ProcessTransferModel
            {
                LoginUser = Settings.UserName,
                StartDate = DateTime.Now,
                LocationFrom = string.Empty,
                TrolleyPalletNo = string.Empty
            };
        }
    }
}
