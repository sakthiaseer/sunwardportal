﻿using APP.SWAM.Mobile.ViewModel.Base;
using APP.SWAM.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace APP.SWAM.Mobile.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private MenuViewModel _menuViewModel;

        public MainViewModel(MenuViewModel menuViewModel)
        {
            _menuViewModel = menuViewModel;
        }

        public MenuViewModel MenuViewModel
        {
            get
            {
                return _menuViewModel;
            }

            set
            {
                _menuViewModel = value;
                OnPropertyChanged();
            }
        }

        public async override Task InitializeAsync(object navigationData)
        {
            await _menuViewModel.InitializeAsync(navigationData);
            await NavigationService.NavigateToAsync<HomeViewModel>();
            await InitializeChatService();
        }

        private async Task InitializeChatService()
        {
            try
            {
                await ChatService.Disconnect();
                await ChatService.Connect();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
