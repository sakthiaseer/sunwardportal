﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using ZXing.Mobile;

namespace APP.SWAM.Mobile.ViewModels
{
    class BMRToCartonItemViewModel : ViewModelBase
    {
        #region fields
        private BMRToCartonLineModel _model;

        private bool isRefreshing;
        #endregion

        #region Properties

        public BMRToCartonLineModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }


        public Command SaveCommand { get; set; }
        public Command ClearCommand { get; set; }
        public ICommand FocusedCommand { get; set; }
        public ICommand LocationCommand { get; set; }

        #endregion

        public BMRToCartonItemViewModel()
        {
            SaveCommand = new Command(OnSave);
            ClearCommand = new Command(OnExit);
            FocusedCommand = new Command(OnBMRScan);
            Title = "BMR To Carton Item " + "(" + Settings.DBName + ")";
            Model = new BMRToCartonLineModel
            {
                LoginUser = Constant.Username,
                StartDate = DateTime.Now,
                LoginUserId = Settings.UserId,
            };
        }

        public override Task InitializeAsync(object navigationData)
        {
            Model = (navigationData as BMRToCartonLineModel);
            return base.InitializeAsync(navigationData);
        }

        void OnBMRScan()
        {
#if DEBUG
            string qrCode = "MT20-0198";
            Model.Bmrno = qrCode.Trim();

#else
            if (string.IsNullOrEmpty(Model.Bmrno))
            {
                var scanner = new MobileBarcodeScanner();
                scanner.Torch(true);
                scanner.AutoFocus();

                scanner.ScanContinuously(s =>
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        if (Model == null)
                            Model = new BMRToCartonLineModel();
                        var bmrNo = s.Text;
                        if (string.IsNullOrEmpty(bmrNo))
                        {
                            Model.Bmrno = bmrNo.Trim();
                        }
                        else
                        {
                            await DialogService.ShowConfirmAsync("In-Valid QR Code.QR Code must contains No(ex:BMR No).Please scan valid QR Code", "QR Code", "OK", "Cancel");

                        }
                    });
                    scanner.Cancel();
                    scanner.Torch(false);
                });


            }
#endif
        }

        async void OnSave()
        {
            if (Model.IsValid())
            {
                IsBusy = true;
                IsEnabled = false;

                UserDialogs.Instance.ShowLoading("Validating BMR To Carton...");
                Model.AddedByUserId = Settings.UserId;
                Model.LoginUserId = Settings.UserId;
                var restclient = new RestClient();
                Model = await restclient.PostAsync<BMRToCartonLineModel>("BMRToCarton/InsertBMRToCartonLine", Model);

                IsBusy = false;
                IsEnabled = true;
                UserDialogs.Instance.HideLoading();
                if (Model.BmrtoCartonLineId > 0)
                {
                    await DialogService.ShowConfirmAsync("Record validated successfully.", "Validation", "OK", "Cancel");
                    MessagingCenter.Send(Model, "AddBMRToCarton");
                    await NavigationService.NavigateBackAsync();
                }
                else
                {
                    await DialogService.ShowConfirmAsync("Validation failed.Please select correct bmr", "Validation", "OK", "Cancel");
                }

            }
            else
            {
                await DialogService.ShowConfirmAsync("Validation Failed. Please scan bmr.", "Next", "OK", "Cancel");
            }
        }
        void OnExit()
        {
            Model = new BMRToCartonLineModel
            {
                LoginUser = Constant.Username,
                StartDate = DateTime.Now,
                Bmrno = string.Empty,
                LoginUserId = Settings.UserId,
            };
        }
    }
}