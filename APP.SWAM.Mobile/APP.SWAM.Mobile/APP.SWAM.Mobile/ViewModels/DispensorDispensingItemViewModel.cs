﻿using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class DispensorDispensingItemViewModel : ViewModelBase
    {
        #region fields


        private ObservableCollection<AppDispenserDispensingDrumDetailModel> _dispensorDrums = new ObservableCollection<AppDispenserDispensingDrumDetailModel>();
        private DispensorDispensingWorkModel _model = new DispensorDispensingWorkModel();

        public DispensorDispensingWorkModel Model
        {
            get { return _model ?? (_model = new DispensorDispensingWorkModel()); }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }


        private long? _dispenserDispensingId;

        public long? DispenserDispensingId
        {
            get { return _dispenserDispensingId; }
            set
            {
                _dispenserDispensingId = value; OnPropertyChanged(nameof(DispenserDispensingId));
            }
        }



        #endregion



        public Command SaveCommand { get; set; }
        public ICommand ScanDrumCommand { get; set; }
        public ICommand FocusedCommand { get; set; }
        public ICommand JobSelectedCommand { get; set; }
        public ICommand TakePhotoCommand { get; set; }

        public ObservableCollection<AppDispenserDispensingDrumDetailModel> DispensorDrums
        {
            get { return _dispensorDrums; }
            set { _dispensorDrums = value; OnPropertyChanged(nameof(DispensorDrums)); }
        }

        private ImageSource _beforeTarePhotoSource;
        public ImageSource BeforeTarePhotoSource { get { return _beforeTarePhotoSource; } set { _beforeTarePhotoSource = value; OnPropertyChanged(nameof(BeforeTarePhotoSource)); } }

        private ImageSource _afterTarePhotoSource;
        public ImageSource AfterTarePhotoSource { get { return _afterTarePhotoSource; } set { _afterTarePhotoSource = value; OnPropertyChanged(nameof(AfterTarePhotoSource)); } }

        private ImageSource _weighingPhotoSource;
        public ImageSource WeighingPhotoSource { get { return _weighingPhotoSource; } set { _weighingPhotoSource = value; OnPropertyChanged(nameof(WeighingPhotoSource)); } }

        void OnJobScan()
        {
#if DEBUG
            CreateTestJob();
#endif
        }

        public DispensorDispensingItemViewModel()
        {
            FocusedCommand = new Command(OnJobScan);
            ScanDrumCommand = new Command(OnAddDrum);
            SaveCommand = new Command(OnSave);
            TakePhotoCommand = new Command<string>(OnTakePhoto);

            MessagingCenter.Subscribe<AppDispenserDispensingDrumDetailModel>(this, "AddDrums", drummingModel =>
            {
                OnReLoadDrum(drummingModel);
            });
        }
        public async override Task InitializeAsync(object navigationData)
        {
            Model = (navigationData as DispensorDispensingWorkModel);
            if (Model.DispenserDispensingLineId > 0)
            {
                await LoadLineData(Model.DispenserDispensingLineId);
                DispensorDrums = new ObservableCollection<AppDispenserDispensingDrumDetailModel>(Model.DispenserDispensingDrumDetailModels);
            }
            DispenserDispensingId = Model.DispenserDispensingId;
        }

        private async Task LoadLineData(long dispenserDispensingLineId)
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                Model = await restClient.GetItem<DispensorDispensingWorkModel>("APPDispenserDispensingLine/GetAPPDispenserDispensingLineItemByID", dispenserDispensingLineId);
            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            PageDialog.HideLoading();
        }

        async void OnAddDrum()
        {
            await NavigationService.NavigateToAsync<DispensorScanViewModel>(Model);
        }

        private async void OnSave()
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                Model.DispenserDispensingId = DispenserDispensingId;
                Model = await restClient.PostAsync("APPDispenserDispensingLine/InsertAPPDispenserDispensingLine", Model);
                MessagingCenter.Send(Model, "AddJobs");
                await NavigationService.NavigateBackAsync();
            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            PageDialog.HideLoading();
        }

        private void CreateTestJob()
        {
            string qrCode = "WO19-M0258~RM19-M0258~123~321~Prozine Syrup 50X120ML~3";

            var barcode = qrCode.Split('~');
            if (barcode.Length > 3)
            {
                Model.JobNo = barcode[0].Trim();
                Model.ProdOrderNo = barcode[3].Trim();
                Model.SubLotNo = barcode[2].Trim();
                Model.ProdLineNo = 1;
                Model.BagNo = "1234";
                Model.BeforeTareWeight = 20;
                Model.ImageBeforeTare = String.IsNullOrEmpty(Model.ImageBeforeTare) ? "Noimages.png" : Model.ImageBeforeTare;
                Model.AfterTareWeight = 21;
                Model.ImageAfterTare = String.IsNullOrEmpty(Model.ImageAfterTare) ? "Noimages.png" : Model.ImageAfterTare;
                Model.ItemNo = "item no";
                Model.LotNo = "Lot no";
                Model.QcrefNo = "QC ref No";
                Model.Weight = 20;
                Model.WeighingPhoto = String.IsNullOrEmpty(Model.WeighingPhoto) ? "Noimages.png" : Model.WeighingPhoto;
            }
        }

        private void OnReLoadDrum(AppDispenserDispensingDrumDetailModel drumModel)
        {
            Model.DispenserDispensingDrumDetailModels.Add(drumModel);
            DispensorDrums.Add(drumModel);
        }

        private async void OnTakePhoto(string photoType)
        {
            Image image = new Image();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DialogService.ShowConfirmAsync("No Camera", ":( No camera available.", "OK", "Cancel");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 30,
                //CustomPhotoSize = 50,
                PhotoSize = PhotoSize.Medium,
                //MaxWidthHeight = 2000,
                DefaultCamera = CameraDevice.Rear
            });

            if (file == null)
                return;
            if (photoType == "BeforeTare")
            {
                Model.BeforeTarePhoto = ReadFully(file.GetStream());
                Model.ImageBeforeTare = Guid.NewGuid().ToString() + ".png";
                BeforeTarePhotoSource = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    file.Dispose();
                    return stream;
                });
            }
            if (photoType == "AfterTare")
            {
                Model.AfterTarePhoto = ReadFully(file.GetStream());
                Model.ImageAfterTare = Guid.NewGuid().ToString() + ".png";
                AfterTarePhotoSource = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    file.Dispose();
                    return stream;
                });
            }
            if (photoType == "Weighing")
            {
                Model.WeighingPhotoStream = ReadFully(file.GetStream());
                Model.WeighingPhoto = Guid.NewGuid().ToString() + ".png";
                WeighingPhotoSource = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    file.Dispose();
                    return stream;
                });
            }
        }

        public byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

    }
}
