﻿using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using Plugin.Media;
using Plugin.Media.Abstractions;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class ProductionScanDocumentViewModel : ViewModelBase
    {
        ProductionDocumentModel productionDocumentModel = new ProductionDocumentModel();
        public Command FileProfileTypeCommand { get; set; }

        public ProductionScanDocumentViewModel()
        {
            ScanCommand = new Command(OnScanItem);
            PostCommand = new Command(OnPost);
            FileProfileTypeCommand = new Command(OnFileProfileTypes);
            Model = new ScanDocumentModel
            {
                AddedByUserId = Settings.UserId,
            };
            MessagingCenter.Subscribe<FPModel>(this, "AddSelectedProfile", mSelection =>
            {
                Model.ProfileTypeName = mSelection.Description;
                Model.FileProfileTypeID = mSelection.Id;
            });
            //LoadChoices();
        }

        public override Task InitializeAsync(object navigationData)
        {

            //CrossMedia.Current.Initialize();
            return base.InitializeAsync(navigationData);
        }

        private async void LoadChoices()
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                fileProfileTypes = await restClient.Get<FPModel>("FileProfileType/GetFileProfileTypeDetails");
                FileProfileTypes = fileProfileTypes.Select(q => q.Description).ToList();
            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            PageDialog.HideLoading();
        }

        private async void OnFileProfileTypes()
        {
            FileProfileListViewModel fileProfileListViewModel = new FileProfileListViewModel();
            fileProfileListViewModel.ScreenName = "FileProfile";
            await NavigationService.NavigateToAsync<FileProfileListViewModel>(fileProfileListViewModel);
        }

        private List<string> _fileProfileTypes = new List<string>();
        public List<string> FileProfileTypes
        {
            get { return _fileProfileTypes; }
            set { _fileProfileTypes = value; OnPropertyChanged(nameof(FileProfileTypes)); }

        }

        List<FPModel> fileProfileTypes = new List<FPModel>();

        private ScanDocumentModel _model = new ScanDocumentModel();

        public ScanDocumentModel Model
        {
            get { return _model ?? (_model = new ScanDocumentModel()); }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        private ObservableCollection<ItemModel> _items = new ObservableCollection<ItemModel>();
        public ObservableCollection<ItemModel> Items
        {
            get { return _items; }
            set { _items = value; OnPropertyChanged("Items"); }
        }

        public Command ScanCommand { get; set; }
        public Command PostCommand { get; set; }

        private async void OnScanItem()
        {
            Image image = new Image();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DialogService.ShowConfirmAsync("No Camera", ":( No camera available.", "OK", "Cancel");
                return;
            }

            //  new ImageCropper()
            //{
            //    Success = (imageFile) =>
            //    {
            //        Device.BeginInvokeOnMainThread(() =>
            //        {
            //            image.Source = ImageSource.FromFile(imageFile);
            //        });
            //    }
            //}.Show(_page);

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "SW Scanned",
                SaveToAlbum = true,
                CustomPhotoSize = 50,
                PhotoSize = PhotoSize.Medium,
                //MaxWidthHeight = 2000,
                DefaultCamera = CameraDevice.Rear,
                AllowCropping = true,
                CompressionQuality = 90
            });

            if (file == null)
                return;

            ItemModel itemModel = new ItemModel();
            ItemModel item = new ItemModel();
            itemModel.Id = Items.Count + 1;
            item.Id = Items.Count + 1;
            itemModel.ImageByte = ReadFully(file.GetStream());
            item.ImageByte = ReadFully(file.GetStream());
            itemModel.FileName = Guid.NewGuid().ToString() + ".png";
            item.FileName = Guid.NewGuid().ToString() + ".png";
            ImageSource PhotoSource = ImageSource.FromStream(() =>
            {
                return new MemoryStream(itemModel.ImageByte);
            });


            itemModel.Image = PhotoSource;
            item.Image = PhotoSource; ;
            Model.Items.Add(itemModel);
            Items.Add(itemModel);
        }

        private async void OnPost()
        {
            PageDialog.ShowLoading();
            try
            {
                if (Model.IsValid())
                {

                    RestClient restClient = new RestClient();
                    foreach (var item in Model.Items)
                    {
                        item.Image = null;
                    }
                    Model.SessionId = Guid.NewGuid();
                    Model.IsProductionDocument = true;
                    if (Model.Items.Count > 0)
                    {
                        var scanDocumentModel = await restClient.PostAsync<ScanDocumentModel>("Documents/ScanByteToPDF", Model);

                        if (scanDocumentModel.DocumentID > 0)
                        {
                            MessagingCenter.Send(Model, "ReloadScanItems");
                            await NavigationService.NavigateBackAsync();
                        }
                    }
                    else
                    {
                        await PageDialog.AlertAsync("Error", "Please Scan atleast one Image", "OK");
                        PageDialog.HideLoading();
                    }
                }
                else
                {
                    await DialogService.ShowConfirmAsync("Please key-in all the required fields", "Validation", "OK", "Cancel");
                }

            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            PageDialog.HideLoading();

        }

        public byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }


    }
}
