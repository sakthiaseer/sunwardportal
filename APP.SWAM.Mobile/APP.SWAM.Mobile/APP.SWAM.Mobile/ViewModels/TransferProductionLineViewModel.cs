﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using ZXing.Mobile;

namespace APP.SWAM.Mobile.ViewModels
{
    public class TransferProductionLineViewModel : ViewModelBase
    {
        #region fields
        private ObservableCollection<APPTransferReClassLineModel> _modelEntries = new ObservableCollection<APPTransferReClassLineModel>();
        private APPTransferReClassLineModel _modelEntry = new APPTransferReClassLineModel();
        private APPTransferReClassModel _remodelEntry = new APPTransferReClassModel();
        private bool isRefreshing;
        private long _transferReclassID;
        #endregion

        #region Properties
        public ObservableCollection<APPTransferReClassLineModel> ModelEntries
        {
            get { return _modelEntries; }
            set { _modelEntries = value; OnPropertyChanged(nameof(ModelEntries)); }
        }

        public APPTransferReClassLineModel SelectedModel
        {
            get { return _modelEntry; }
            set
            {
                _modelEntry = value;
            }
        }
        public APPTransferReClassModel RelcassEntryModel
        {
            get { return _remodelEntry; }
            set
            {
                _remodelEntry = value; OnPropertyChanged(nameof(RelcassEntryModel));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }

        public long TransferReclassID
        {
            get { return _transferReclassID; }
            set { _transferReclassID = value; OnPropertyChanged(nameof(TransferReclassID)); }
        }

        public ICommand SaveCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand OpenCommand { get; set; }

        public ICommand AddNewCommand { get; set; }
        public ICommand TextboxFocusedEvent { get; set; }


        #endregion

        public override Task InitializeAsync(object navigationData)
        {

            RelcassEntryModel = (navigationData as APPTransferReClassModel);
            TransferReclassID = RelcassEntryModel.TransferReclassID;
            FillData();
            return base.InitializeAsync(navigationData);
        }
        public async void FillData()
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...");
                var restclient = new RestClient();
                var entries = await restclient.Get<APPTransferReClassLineModel>("ApptransferRelcassLines/GetApptransferReclassEntrysByID", TransferReclassID);
                entries = entries == null ? new List<APPTransferReClassLineModel>() : entries;
                ModelEntries = new ObservableCollection<APPTransferReClassLineModel>(entries);
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
                await DialogService.ShowConfirmAsync(ex.Message, "Load", "OK", "Cancel");
            }
        }
        public TransferProductionLineViewModel()
        {
            DeleteCommand = new Command(OnDelete);
            SaveCommand = new Command(OnPost);
            OpenCommand = new Command(OnOpenItem);
            AddNewCommand = new Command(OnAddNewItem);
            TextboxFocusedEvent = new Command(OnProdScan);
        }
        async void OnDelete()
        {
            if (SelectedModel != null && SelectedModel.ApptransferRelcassLineID > 0)
            {
                IsBusy = true;
                IsEnabled = false;
                UserDialogs.Instance.ShowLoading("Deleting Line entry...");
                var restclient = new RestClient();
                var result = await restclient.DeleteAsync<bool>("ApptransferRelcassLines/DeleteApptransferReclassEntry", SelectedModel.ApptransferRelcassLineID);
                if (result)
                {
                    FillData();
                }
                else
                {
                    await DialogService.ShowAlertAsync("Delete operation failed.!", "DELETE", "OK");
                }
                IsBusy = false;
                IsEnabled = true;
                UserDialogs.Instance.HideLoading();
            }
            else
            {
                await DialogService.ShowAlertAsync("Select record to delete.", "DELETE", "OK");
            }
        }
        void OnProdScan()
        {
#if DEBUG
            string qrCode = "QUARANTINE~GRANU";

            var barcode = qrCode.Split('~');
            if (barcode.Length > 1)
            {
                RelcassEntryModel.ToLocation = barcode[0].Trim();

            }
#else
            if (string.IsNullOrEmpty( RelcassEntryModel.ToLocation))
            {
                var scanner = new MobileBarcodeScanner();
                scanner.Torch(true);
                scanner.AutoFocus();

                scanner.ScanContinuously(s =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    { 
                         RelcassEntryModel.ToLocation = s.Text;
                    });
                    scanner.Cancel();
                    scanner.Torch(false);
                });
            }
#endif
        }
        async void OnPost()
        {
            try
            {
                if (RelcassEntryModel.IsValidTo())
                {
                    IsBusy = true;
                    IsEnabled = false;

                    UserDialogs.Instance.ShowLoading("Posting Consumption into NAV...");
                    RelcassEntryModel.AddedByUserId = Settings.UserId;
                    RelcassEntryModel.TransferReclassID = TransferReclassID;
                    RelcassEntryModel.CompanyName = Settings.NAVName;
                    var restclient = new RestClient();
                    var result = await restclient.PostAsync<APPTransferReClassModel>("ApptransferRelcassLines/PostTransferReclass", RelcassEntryModel);
                    if (!result.IsError)
                    {
                        await DialogService.ShowAlertAsync("Record posted successfully.", "POST", "OK");
                        MessagingCenter.Send(new APPTransferReClassModel(), "InitProdTrans");
                        await NavigationService.NavigateBackAsync();
                    }
                    else
                        await DialogService.ShowConfirmAsync("Posting failed." + result.Errormessage, "POST", "OK", "Cancel");
                    IsBusy = false;
                    IsEnabled = true;
                    UserDialogs.Instance.HideLoading();


                }
                else
                {
                    await DialogService.ShowConfirmAsync("Posting failed. Validation error.", "POST", "OK", "Cancel");
                }
            }
            catch (Exception ex)
            {
                IsBusy = false;
                IsEnabled = true;
                await DialogService.ShowConfirmAsync(ex.Message, "ERROR", "OK", "Cancel");
            }
        }
        async void OnOpenItem()
        {
            await NavigationService.NavigateToAsync<TransferProductionItemViewModel>(this);
        }
        async void OnAddNewItem()
        {
            SelectedModel = new APPTransferReClassLineModel();
            await NavigationService.NavigateToAsync<TransferProductionItemViewModel>(this);
        }
    }
}
