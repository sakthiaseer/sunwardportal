﻿using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace APP.SWAM.Mobile.ViewModels
{
    public class PDFViewerViewModel : ViewModelBase
    {

        private ScanDocumentModel _model = new ScanDocumentModel();
        public ScanDocumentModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        public PDFViewerViewModel()
        {

        }



        public override Task InitializeAsync(object navigationData)
        {
            Model = navigationData as ScanDocumentModel;
            return base.InitializeAsync(navigationData);

        }
    }
}
