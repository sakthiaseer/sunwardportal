﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using ZXing.Mobile;

namespace APP.SWAM.Mobile.ViewModels
{
    public class TransferReclassEntryViewModel : ViewModelBase
    {
        #region fields

        private APPTransferReClassModel _model;
        private bool isRefreshing;
        private long _transferReclassID;
        #endregion

        #region Properties


        public APPTransferReClassModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }

        public long TransferReclassID
        {
            get { return _transferReclassID; }
            set { _transferReclassID = value; OnPropertyChanged(nameof(TransferReclassID)); }
        }


        public Command SaveCommand { get; set; }
        public Command ExitCommand { get; set; }
        public ICommand TextboxFocusedEvent { get; set; }

        #endregion

        public override Task InitializeAsync(object navigationData)
        {
            return base.InitializeAsync(navigationData);
        }

        public TransferReclassEntryViewModel()
        {
            SaveCommand = new Command(OnSave);
            ExitCommand = new Command(OnExit);
            TextboxFocusedEvent = new Command(OnProdScan);
            Model = new APPTransferReClassModel
            {
                LoginUser = Settings.UserName,
                StartDate = DateTime.Now,
            };
            Title = "Transfer/ReClass" + "(" + Settings.DBName + ")";
            MessagingCenter.Subscribe<APPTransferReClassModel>(this, "InitProdTransRec", drummingModel =>
            {
                Model = new APPTransferReClassModel
                {
                    LoginUser = Settings.UserName,
                    StartDate = DateTime.Now,
                };
            });

        }
        void OnProdScan()
        {
#if DEBUG
            string qrCode = "STORE 1~GRANU";

            var barcode = qrCode.Split('~');
            if (barcode.Length > 1)
            {
                Model.FromLocation = barcode[0].Trim();

            }
#else
            if (string.IsNullOrEmpty(Model.FromLocation))
            {
                var scanner = new MobileBarcodeScanner();
                scanner.Torch(true);
                scanner.AutoFocus();

                scanner.ScanContinuously(s =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Model.FromLocation = s.Text;
                    });
                    scanner.Cancel();
                    scanner.Torch(false);
                });
            }
#endif
        }
        async void OnSave()
        {
            try
            {
                if (Model.IsValidFrom())
                {
                    IsBusy = true;
                    IsEnabled = false;

                    UserDialogs.Instance.ShowLoading("Validating Transfer/Reclass...");
                    Model.AddedByUserId = Settings.UserId;
                    var restclient = new RestClient();
                    Model = await restclient.PostAsync<APPTransferReClassModel>("ApptransferRelcassEntry/InsertApptransferReclassEntry", Model);

                    IsBusy = false;
                    IsEnabled = true;
                    UserDialogs.Instance.HideLoading();

                    if (!Model.IsError)
                    {
                        TransferReclassID = Model.TransferReclassID;
                        await NavigationService.NavigateToAsync<TransferReclassLineViewModel>(Model);
                    }
                    else
                    {
                        await DialogService.ShowConfirmAsync(Model.Errormessage, "Next", "OK", "Cancel");
                    }
                }
                else
                {

                    await DialogService.ShowConfirmAsync("Validation Failed. Please scan from location.", "Next", "OK", "Cancel");
                }


            }
            catch (Exception ex)
            {
                IsBusy = false;
                IsEnabled = true;
                UserDialogs.Instance.HideLoading();
                await DialogService.ShowConfirmAsync(ex.Message, "ERROR", "OK", "Cancel");
            }
        }
        void OnExit()
        {
            Model = new APPTransferReClassModel
            {
                LoginUser = Settings.UserName,
                StartDate = DateTime.Now,

            };
        }

    }
}
