﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class AssetLocationViewModel : ViewModelBase
    {
        public AssetLocationViewModel()
        {
            FillData();
            SubmitCommand = new Command(Submit);
            CompanySelectionChangedCommand = new Command(OnCompanySelectionChanged);
            SiteSelectionChangedCommand = new Command(OnSiteSelectionChanged);
            ZonesSelectionChangedCommand = new Command(OnZonesSelectionChanged);
            LocationsSelectionChangedCommand = new Command(OnLocationSelectionChanged);
            AreaSelectionChangedCommand = new Command(OnAreaSelectionChanged);
            SpecificAreaSelectionChangedCommand = new Command(OnSpecificAreasChanged);

            Title = "Asset Mgmt" + "(" + Settings.DBName + ")";
        }

        private string _userName = Constant.Username;
        private string _loginDateTime = Constant.LoginDate;

        public Command SubmitCommand { get; set; }

        public Command CompanySelectionChangedCommand { get; set; }

        public Command SiteSelectionChangedCommand { get; set; }
        public Command ZonesSelectionChangedCommand { get; set; }
        public Command LocationsSelectionChangedCommand { get; set; }
        public Command AreaSelectionChangedCommand { get; set; }
        public Command SpecificAreaSelectionChangedCommand { get; set; }
        public bool IsValidated { get; set; }
        public string UserName { get => _userName; set { _userName = value; OnPropertyChanged(); } }
        public string LoginDateTime { get => _loginDateTime; set { _loginDateTime = value; OnPropertyChanged(); } }

        private SelectionModel _selectedCompany;
        public SelectionModel SelectedCompany { get => _selectedCompany; set { _selectedCompany = value; OnPropertyChanged(); } }

        private SelectionModel _selectedSite;
        public SelectionModel SelectedSite { get => _selectedSite; set { _selectedSite = value; OnPropertyChanged(); } }

        private SelectionModel _selectedZone;
        public SelectionModel SelectedZone { get => _selectedZone; set { _selectedZone = value; OnPropertyChanged(); } }

        private SelectionModel _selectedLocation;
        public SelectionModel SelectedLocation { get => _selectedLocation; set { _selectedLocation = value; OnPropertyChanged(); } }

        private SelectionModel _selectedArea;
        public SelectionModel SelectedArea { get => _selectedArea; set { _selectedArea = value; OnPropertyChanged(); } }

        private SelectionModel _selectedSpecificArea;
        public SelectionModel SelectedSpecificArea { get => _selectedSpecificArea; set { _selectedSpecificArea = value; OnPropertyChanged(); } }

        public IList<PlantMobileModel> Plants { get; set; } = new ObservableCollection<PlantMobileModel>();
        public IList<ICTMobileMaster> ICTMobileMasters { get; set; } = new ObservableCollection<ICTMobileMaster>();
        public IList<SelectionModel> Companies { get; set; } = new ObservableCollection<SelectionModel>();

        public IList<SelectionModel> Sites { get; set; } = new ObservableCollection<SelectionModel>();
        public IList<SelectionModel> Zones { get; set; } = new ObservableCollection<SelectionModel>();
        public IList<SelectionModel> Locations { get; set; } = new ObservableCollection<SelectionModel>();
        public IList<SelectionModel> Areas { get; set; } = new ObservableCollection<SelectionModel>();
        public IList<SelectionModel> SpecificAreas { get; set; } = new ObservableCollection<SelectionModel>();


        async void OnCompanySelectionChanged()
        {
            var restclient = new RestClient();
            ICTMobileMasters = await restclient.Get<ICTMobileMaster>("ICTMaster/GetICTMobileMasters", SelectedCompany.Id);
            Sites.Clear();
            SelectedSite = null;
            foreach (var ictMobileMaster in ICTMobileMasters.Where(i => i.MasterType == 570))
            {
                Sites.Add(new SelectionModel { Id = ictMobileMaster.ICTMasterID, Name = ictMobileMaster.Name });
            }

        }

        void OnSiteSelectionChanged()
        {
            Zones.Clear();
            SelectedZone = null;
            if (SelectedSite != null)
            {
                foreach (var ictMobileMaster in ICTMobileMasters.Where(i => i.MasterType == 571 && i.ParentICTID == SelectedSite.Id))
                {
                    Zones.Add(new SelectionModel { Id = ictMobileMaster.ICTMasterID, Name = ictMobileMaster.Name });
                }
            }
        }

        void OnZonesSelectionChanged()
        {
            Locations.Clear();
            SelectedLocation = null;
            if (SelectedZone != null)
            {
                foreach (var ictMobileMaster in ICTMobileMasters.Where(i => i.MasterType == 572 && i.ParentICTID == SelectedZone.Id))
                {
                    Locations.Add(new SelectionModel { Id = ictMobileMaster.ICTMasterID, Name = ictMobileMaster.Name });
                }
            }
        }

        void OnLocationSelectionChanged()
        {
            Areas.Clear();
            SelectedArea = null;
            if (SelectedLocation != null)
            {
                foreach (var ictMobileMaster in ICTMobileMasters.Where(i => i.MasterType == 573 && i.ParentICTID == SelectedLocation.Id))
                {
                    Areas.Add(new SelectionModel { Id = ictMobileMaster.ICTMasterID, Name = ictMobileMaster.Name });
                }
            }
        }

        void OnAreaSelectionChanged()
        {
            SpecificAreas.Clear();
            SelectedSpecificArea = null;
            if (SelectedArea != null)
            {
                foreach (var ictMobileMaster in ICTMobileMasters.Where(i => i.MasterType == 574 && i.ParentICTID == SelectedArea.Id))
                {
                    SpecificAreas.Add(new SelectionModel { Id = ictMobileMaster.ICTMasterID, Name = ictMobileMaster.Name });
                }
            }
        }

        void OnSpecificAreasChanged()
        {

        }

        async void FillData()
        {

            var restclient = new RestClient();
            var companies = await restclient.Get<PlantMobileModel>("Plant/GetMobilePlants");

            companies.ForEach(c =>
            {
                Companies.Add(new SelectionModel { Id = c.PlantID, Name = c.PlantCode });
            });
        }

        public override Task InitializeAsync(object navigationData)
        {
            return base.InitializeAsync(navigationData);
        }

        async void Submit()
        {
            if (!IsValidated)
            {
                await Application.Current.MainPage.DisplayAlert("", "You must fill all the fields correctly!", "OK");
                return;
            }

            IsBusy = true;
            IsEnabled = false;


            UserDialogs.Instance.ShowLoading("Save Location...");

            //DO SOME STUFFS HERE

            PlantMaintenanceEntryModel planMaintenanceViewMdel = new PlantMaintenanceEntryModel();
            planMaintenanceViewMdel.CompanyId = SelectedCompany?.Id;
            planMaintenanceViewMdel.SiteId = SelectedSite?.Id;
            planMaintenanceViewMdel.ZoneId = SelectedZone?.Id;
            planMaintenanceViewMdel.LocationId = SelectedLocation?.Id;
            planMaintenanceViewMdel.AreaId = SelectedArea?.Id;
            planMaintenanceViewMdel.SpecificAreaId = SelectedSpecificArea?.Id;
            planMaintenanceViewMdel.AddedByUserID = 1;
            planMaintenanceViewMdel.AddedDate = DateTime.Now;



            var restclient = new RestClient();
            planMaintenanceViewMdel = await restclient.PostAsync<PlantMaintenanceEntryModel>("PlantMaintenanceEntry/InsertPlantMaintenanceEntrys", planMaintenanceViewMdel);
            IsBusy = false;
            IsEnabled = true;
            UserDialogs.Instance.HideLoading();
            await NavigationService.NavigateToAsync<AssetItemsViewModel>(planMaintenanceViewMdel);
        }

    }


}
