﻿using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.MultiSelectListView;

namespace APP.SWAM.Mobile.ViewModels
{
    public class MultiselectionViewModel : ViewModelBase
    {
        public MultiselectionViewModel()
        {
            ExitCommand = new Command(OnExit);
        }

        public MultiSelectionModel MultiSelection { get; set; }

        public override Task InitializeAsync(object navigationData)
        {
            MultiSelection = navigationData as MultiSelectionModel;
            MultiselectionItems = MultiSelection.MultiselectionItems;
            return base.InitializeAsync(navigationData);
        }

        public ICommand ExitCommand { get; set; }

        private MultiSelectObservableCollection<MultiSelectionModel> _multiselectionItems = new MultiSelectObservableCollection<MultiSelectionModel>();
        public MultiSelectObservableCollection<MultiSelectionModel> MultiselectionItems
        {
            get { return _multiselectionItems; }
            set { _multiselectionItems = value; OnPropertyChanged(); }
        }

        private async void OnExit()
        {
            if (MultiSelection.MultiSelectionName == "ProdTask")
            {
                MessagingCenter.Send(MultiSelection, "AddProdTaskSelectionItems");
            }
            if (MultiSelection.MultiSelectionName == "ManPower")
            {
                MessagingCenter.Send(MultiSelection, "AddManPowerSelectionItems");
            }
            if (MultiSelection.MultiSelectionName == "IPIR Issue")
            {
                MessagingCenter.Send(MultiSelection, "AddIPIRIssue");
            }
            if (MultiSelection.MultiSelectionName == "IPIR Issue Title")
            {
                MessagingCenter.Send(MultiSelection, "AddIPIRIssueTitle");
            }
            if (MultiSelection.MultiSelectionName == "IPIR Issue 1")
            {
                MessagingCenter.Send(MultiSelection, "AddIPIRIssue1");
            }
            if (MultiSelection.MultiSelectionName == "IPIR Issue Title 1")
            {
                MessagingCenter.Send(MultiSelection, "AddIPIRIssueTitle1");
            }
            await NavigationService.NavigateBackAsync();
        }
    }
}
