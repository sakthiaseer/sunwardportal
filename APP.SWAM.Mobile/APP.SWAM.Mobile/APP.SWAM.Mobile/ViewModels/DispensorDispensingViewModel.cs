﻿using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class DispensorDispensingViewModel : ViewModelBase
    {
        #region fields

        private DispensorDispensingModel _model;

        public DispensorDispensingModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        #endregion

        public Command ScanJobCommand { get; set; }
        public Command ClearCommand { get; set; }
        public ICommand FocusedCommand { get; set; }

        public DispensorDispensingViewModel()
        {
            Model = new DispensorDispensingModel
            {
                LoginUser = Settings.UserName,
                StartDate = DateTime.Now,
            };
            ScanJobCommand = new Command(OnScanJob);
            ClearCommand = new Command(OnClear);
            FocusedCommand = new Command(OnWorkOrderScan);
        }
        public override Task InitializeAsync(object navigationData)
        {
            return base.InitializeAsync(navigationData);
        }
        async void OnScanJob()
        {
            if (Model.IsValidWork())
            {
                await CreateDispensingItem();
                await NavigationService.NavigateToAsync<DispensorDispensingLinesViewModel>(Model);
            }
            else
            {
                await DialogService.ShowConfirmAsync("Validation Failed. Please scan work order.", "Next", "OK", "Cancel");
            }
        }

        private async Task CreateDispensingItem()
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                Model = await restClient.PostAsync("AppDispenserDispensing/InsertAPPDispenserDispensing", Model);
            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            PageDialog.HideLoading();
        }

        void OnWorkOrderScan()
        {
#if DEBUG
            string qrCode = "WO19-M0258~RM19-M0258~Prozine Syrup 50X120ML~3";

            var barcode = qrCode.Split('~');
            if (barcode.Length > 3)
            {
                Model.WorkOrderNo = barcode[0].Trim();
                Model.ItemNo = barcode[1].Trim();
                Model.Description = barcode[2].Trim();
                Model.TotalItem = 3;
                Model.WeighingMaterial = 1;

            }
#endif
        }

        void OnClear()
        {
            Model.WorkOrderNo = string.Empty;
            Model.ItemNo = string.Empty;
            Model.Description = string.Empty;
        }
    }
}
