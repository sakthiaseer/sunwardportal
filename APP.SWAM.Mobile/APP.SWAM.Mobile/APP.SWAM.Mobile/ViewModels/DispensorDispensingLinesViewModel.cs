﻿using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class DispensorDispensingLinesViewModel : ViewModelBase
    {
        #region fields

        private DispensorDispensingWorkModel _model = new DispensorDispensingWorkModel();
        private ObservableCollection<DispensorDispensingWorkModel> _modelEntries = new ObservableCollection<DispensorDispensingWorkModel>();
        private bool isRefreshing;
        private long _drummingId;
        private DispensorDispensingWorkModel _selectedItem = new DispensorDispensingWorkModel();
        #endregion

        #region Properties

        public DispensorDispensingWorkModel SelectedItem
        {
            get { return _selectedItem ?? new DispensorDispensingWorkModel(); }
            set
            {
                _selectedItem = value; OnPropertyChanged(nameof(SelectedItem));
            }
        }

        public ObservableCollection<DispensorDispensingWorkModel> DispensorDispensingEntries
        {
            get { return _modelEntries; }
            set { _modelEntries = value; OnPropertyChanged(nameof(DispensorDispensingEntries)); }
        }

        public DispensorDispensingWorkModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        private DispensorDispensingModel _parentModel;
        public DispensorDispensingModel ParentModel
        {
            get { return _parentModel; }
            set
            {
                _parentModel = value; OnPropertyChanged(nameof(ParentModel));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }

        public long SupervisorDispensingId
        {
            get { return _drummingId; }
            set { _drummingId = value; OnPropertyChanged(nameof(SupervisorDispensingId)); }
        }


        public Command NewCommand { get; set; }
        public Command DeleteCommand { get; set; }
        public ICommand JobSelectedCommand { get; set; }

        #endregion

        #region Methods

        public DispensorDispensingLinesViewModel()
        {
            NewCommand = new Command(OnNewJob);
            DeleteCommand = new Command(OnDelete);
            JobSelectedCommand = new Command(OnSelected);

            MessagingCenter.Subscribe<DispensorDispensingWorkModel>(this, "AddJobs", jobModel =>
            {
                OnReloadJob(jobModel);
            });
        }
        public override Task InitializeAsync(object navigationData)
        {
            ParentModel = navigationData as DispensorDispensingModel;
            Model.DispenserDispensingId = ParentModel.DispenserDispensingId;
            return base.InitializeAsync(navigationData);
        }

        public async void OnNewJob()
        {
            Model = new DispensorDispensingWorkModel();
            Model.DispenserDispensingId = ParentModel.DispenserDispensingId;
            await NavigationService.NavigateToAsync<DispensorDispensingItemViewModel>(Model);
        }

        public async void OnDelete()
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                await restClient.DeleteAsync<bool>("APPDispenserDispensingLine/DeleteDispensingEntryLine", SelectedItem.DispenserDispensingLineId);
            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            DispensorDispensingEntries.Remove(SelectedItem);
            PageDialog.HideLoading();
        }


        public async void OnSelected()
        {
            if (SelectedItem.DispenserDispensingId != null)
            {
                await NavigationService.NavigateToAsync<DispensorDispensingItemViewModel>(SelectedItem);
            }
        }

        private async void OnReloadJob(DispensorDispensingWorkModel jobModel)
        {
            //DispensorDispensingEntries.Add(jobModel);
            await LoadData(jobModel.DispenserDispensingId);
        }


        private async Task LoadData(long? dispenserDispensingId)
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                var dispensorDispensingEntries = await restClient.Get<DispensorDispensingWorkModel>("APPDispenserDispensingLine/GetAPPDispenserDispensingLineByID", dispenserDispensingId.Value);
                DispensorDispensingEntries = new ObservableCollection<DispensorDispensingWorkModel>(dispensorDispensingEntries);
            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            PageDialog.HideLoading();
        }

        #endregion
    }
}
