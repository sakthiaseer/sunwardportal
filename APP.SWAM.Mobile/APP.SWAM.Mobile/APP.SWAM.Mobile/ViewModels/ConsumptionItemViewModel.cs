﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using ZXing.Mobile;

namespace APP.SWAM.Mobile.ViewModels
{
    public class ConsumptionItemViewModel : ViewModelBase
    {
        #region fields

        private ConsumptionLineModel _model;
        private ConsumptionLineModel _selectedItem;
        private ObservableCollection<ConsumptionLineModel> _consumptionEntries = new ObservableCollection<ConsumptionLineModel>();
        private List<ConsumptionLineModel> _entries = new List<ConsumptionLineModel>();
        private bool isRefreshing;
        private long consumptionnEntryID;
        private string _prodOrderNo;
        #endregion

        #region Properties

        public ObservableCollection<ConsumptionLineModel> ConsumptionEntries
        {
            get { return _consumptionEntries; }
            set { _consumptionEntries = value; OnPropertyChanged(nameof(ConsumptionEntries)); }
        }
        public List<ConsumptionLineModel> Entries
        {
            get { return _entries; }
            set { _entries = value; OnPropertyChanged(nameof(Entries)); }
        }

        public ConsumptionLineModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(_model));
            }
        }
        public ConsumptionLineModel SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value; OnPropertyChanged(nameof(SelectedItem));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }

        public long ConsumptionEntryID
        {
            get { return consumptionnEntryID; }
            set { consumptionnEntryID = value; OnPropertyChanged(nameof(ConsumptionEntryID)); }
        }
        public string ProdOrderNo
        {
            get { return _prodOrderNo; }
            set { _prodOrderNo = value; OnPropertyChanged(nameof(ProdOrderNo)); }
        }


        public Command SaveCommand { get; set; }
        public Command DeleteCommand { get; set; }
        public Command ExitCommand { get; set; }
        public ICommand ScanLineCommand { get; set; }
        public ICommand SelectedItemLine { get; set; }
        #endregion

        public override Task InitializeAsync(object navigationData)
        {
            var ConsumptionEntry = (navigationData as ConsumptionModel);

            consumptionnEntryID = ConsumptionEntry.ConsumptionEntryID;
            ProdOrderNo = ConsumptionEntry.ProdOrderNo;
            Model.ProdOrderNo = ConsumptionEntry.ProdOrderNo;
            Model.TransferFrom = ConsumptionEntry.TransferFrom;
            Model.TransferTo = ConsumptionEntry.TransferTo;

            FillData();
            return base.InitializeAsync(navigationData);
        }
        public async void FillData()
        {
            UserDialogs.Instance.ShowLoading("Loading...");
            var restclient = new RestClient();
            var entries = await restclient.Get<ConsumptionLineModel>("APPTransferOrderLines/GetConsumptionLines", ConsumptionEntryID);

            ConsumptionEntries = new ObservableCollection<ConsumptionLineModel>(entries);
            UserDialogs.Instance.HideLoading();
        }
        public ConsumptionItemViewModel()
        {
            SaveCommand = new Command(OnPost);
            DeleteCommand = new Command(OnDelete);
            ExitCommand = new Command(OnExit);
            ScanLineCommand = new Command(OnProdScan);
            SelectedItemLine = new Command(OnSelected);
            Model = new ConsumptionLineModel
            {
                LoginUser = Constant.Username,
                StartDate = DateTime.Now,
            };
        }
        async void OnDelete()
        {
            if (SelectedItem != null && SelectedItem.ConsumptionLineID > 0)
            {
                IsBusy = true;
                IsEnabled = false;
                UserDialogs.Instance.ShowLoading("Deleting Consumption entry...");
                var restclient = new RestClient();
                var result = await restclient.DeleteAsync<bool>("APPTransferOrderLines/DeleteConsumptionLines", SelectedItem.ConsumptionLineID);
                if (result)
                {
                    FillData();
                }
                else
                {
                    await DialogService.ShowAlertAsync("Delete operation failed.!", "DELETE", "OK");
                }
                IsBusy = false;
                IsEnabled = true;
                UserDialogs.Instance.HideLoading();
            }
            else
            {
                await DialogService.ShowAlertAsync("Select record to delete.", "DELETE", "OK");
            }
        }
        async void OnSelected()
        {
            //SelectedItem = this as ConsumptionLineModel;
        }
        async void OnPost()
        {
            try
            {
                IsBusy = true;
                IsEnabled = false;

                UserDialogs.Instance.ShowLoading("Posting Consumption into NAV...");
                Model.CompanyName = Settings.NAVName;
                Model.AddedByUserId = Settings.UserId;
                Model.ConsumptionEntryID = ConsumptionEntryID;
                var restclient = new RestClient();
                var result = await restclient.PostAsync<ConsumptionLineModel>("APPTransferOrderLines/PostConsumption", Model);
                if (!result.IsError)
                {
                    await DialogService.ShowAlertAsync("Record posted successfully.", "POST", "OK");
                    MessagingCenter.Send(new ConsumptionModel(), "InitConsump");
                    await NavigationService.NavigateBackAsync();
                }
                else
                    await DialogService.ShowAlertAsync("Posting failed." + result.Errormessage, "POST", "OK");
                IsBusy = false;
                IsEnabled = true;

                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {
                IsBusy = false;
                IsEnabled = true;
                UserDialogs.Instance.HideLoading();
                await DialogService.ShowAlertAsync(ex.Message, "ERROR", "OK");
            }
        }
        async void OnProdScan()
        {
            try
            {

#if DEBUG
                string qrCode = "MAT-NP-0092~10000~RM19-M0157~RMJ1903027~187.20~KG~1234";

                var barcode = qrCode.Split('~');
                if (barcode.Length > 5)
                {
                    Model.ItemNo = barcode[0].Trim();
                    Model.BatchNo = barcode[1].Trim();
                    Model.ProdLineNo = barcode[1].Trim();
                    Model.LotNo = barcode[2].Trim();
                    Model.QCRefNo = barcode[3].Trim();
                    Model.Quantity = barcode[4].Trim();
                    Model.UOM = barcode[5].Trim();
                    Model.ProdComLineNo = barcode[6].Trim();
                    //Model.Description = barcode[6].Trim();
                    Model.ConsumptionEntryID = ConsumptionEntryID;
                    Model.AddedByUserId = Settings.UserId;
                    Model.ProdOrderNo = ProdOrderNo;
                    OnSave(Model);
                }
                else
                {
                    await DialogService.ShowAlertAsync("In-Valid QR Code.QR Code must contains 6 segment(ex:ItemNo,LineNo,LotNo,BatchNo).Please scan valid QR Code", "QR Code", "OK");
                }

#else

                var scanner = new MobileBarcodeScanner
                {
                    UseCustomOverlay = false,
                    CameraUnsupportedMessage = "Camera not supporting.",
                };
                var result = await scanner.Scan();
                var barcode = result.Text.Split('~');

                if (barcode.Length > 5)
                {
                    Model.ItemNo = barcode[0].Trim();
                    Model.BatchNo = barcode[1].Trim();
                    Model.ProdLineNo = barcode[1].Trim();
                    Model.LotNo = barcode[2].Trim();
                    Model.QCRefNo = barcode[3].Trim();
                    Model.Quantity = barcode[4].Trim();
                    Model.UOM = barcode[5].Trim();
                    Model.ProdComLineNo = barcode[6].Trim();
                    //Model.Description = barcode[6].Trim();
                    Model.ConsumptionEntryID = ConsumptionEntryID;
                    Model.AddedByUserId = Settings.UserId;
                    Model.ProdOrderNo = ProdOrderNo;
                    OnSave(Model);
                }
                else
                {
                    await DialogService.ShowAlertAsync("In-Valid QR Code.QR Code must contains 6 segment(ex:ItemNo,LineNo,LotNo,BatchNo).Please scan valid QR Code", "QR Code", "OK");
                }

#endif
            }
            catch (Exception ex)
            {
                await DialogService.ShowAlertAsync(ex.Message, "QR Code", "OK");
            }

        }

        async void OnSave(ConsumptionLineModel value)
        {
            try
            {

                IsBusy = true;
                IsEnabled = false;

                UserDialogs.Instance.ShowLoading("Validate Ticket Consumption...");
                value.AddedUserId = Settings.UserId;
                value.AddedByUserId = Settings.UserId;
                value.CompanyName = Settings.NAVName;
                var restclient = new RestClient();
                var result = await restclient.PostAsync<ConsumptionLineModel>("APPTransferOrderLines/InsertConsumptionLines", value);
                if (result.IsError)
                {
                    await DialogService.ShowAlertAsync("In-Valid QR Code.Please scan valid QR Code OR Item already scanned/posted to NAV", "QR Code", "OK");

                }
                FillData();
                IsBusy = false;
                IsEnabled = true;
                UserDialogs.Instance.HideLoading();

            }
            catch (Exception ex)
            {
                IsBusy = false;
                IsEnabled = true;
                UserDialogs.Instance.HideLoading();
                await DialogService.ShowAlertAsync(ex.Message, "ERROR", "OK");

            }
        }
        void OnExit()
        {
            Model = new ConsumptionLineModel
            {
                ProdOrderNo = string.Empty
            };
        }
    }
}
