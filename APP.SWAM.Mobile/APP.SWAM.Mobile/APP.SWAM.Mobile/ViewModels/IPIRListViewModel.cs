﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class IPIRListViewModel : ViewModelBase
    {
        #region fields
        private ObservableCollection<IPIRModel> _modelEntries = new ObservableCollection<IPIRModel>();
        private IPIRModel _modelEntry = new IPIRModel();
        private bool isRefreshing;
        private long _ipirMobileId;
        #endregion

        #region Properties
        public ObservableCollection<IPIRModel> ModelEntries
        {
            get { return _modelEntries; }
            set { _modelEntries = value; OnPropertyChanged(nameof(ModelEntries)); }
        }

        public IPIRModel SelectedModel
        {
            get { return _modelEntry; }
            set
            {
                _modelEntry = value;
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }

        public long IpirMobileId
        {
            get { return _ipirMobileId; }
            set { _ipirMobileId = value; OnPropertyChanged(nameof(IpirMobileId)); }
        }

        public ICommand DeleteCommand { get; set; }
        public ICommand OpenCommand { get; set; }

        public ICommand AddNewCommand { get; set; }
        public ICommand TextboxFocusedEvent { get; set; }

        #endregion

        public override Task InitializeAsync(object navigationData)
        {
            MessagingCenter.Subscribe<IPIRModel>(this, "ReloadIPIRItems", jobModel =>
            {
                FillData();
            });
            FillData();
            return base.InitializeAsync(navigationData);
        }
        public async void FillData()
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...");
                var restclient = new RestClient();
                var entries = await restclient.Get<IPIRModel>("IPIRMobile/GetIPIRMobileItems");

                ModelEntries = new ObservableCollection<IPIRModel>(entries);
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
                await DialogService.ShowConfirmAsync(ex.Message, "Load", "OK", "Cancel");
            }
        }
        public IPIRListViewModel()
        {
            DeleteCommand = new Command(OnDelete);
            OpenCommand = new Command(OnOpenItem);
            AddNewCommand = new Command(OnAddNewItem);
        }
        async void OnDelete()
        {
            if (SelectedModel != null && SelectedModel.IpirmobileId > 0)
            {
                IsBusy = true;
                IsEnabled = false;
                UserDialogs.Instance.ShowLoading("Deleting Line entry...");
                var restclient = new RestClient();
                var result = await restclient.DeleteAsync<bool>("IPIRMobile/DeleteIpirMobile", SelectedModel.IpirmobileId);
                if (result)
                {
                    FillData();
                }
                else
                {
                    await DialogService.ShowAlertAsync("Delete operation failed.!", "DELETE", "OK");
                }
                IsBusy = false;
                IsEnabled = true;
                UserDialogs.Instance.HideLoading();
            }
            else
            {
                await DialogService.ShowAlertAsync("Select record to delete.", "DELETE", "OK");
            }
        }
        async void OnOpenItem()
        {
            await NavigationService.NavigateToAsync<IPIRListItemViewModel>(SelectedModel);
        }
        async void OnAddNewItem()
        {
            SelectedModel = new IPIRModel();
            SelectedModel.LoginUser = Constant.Username;
            SelectedModel.StartDate = DateTime.Now;
            SelectedModel.CompanyName = Settings.NAVName;
            await NavigationService.NavigateToAsync<IPIRListItemViewModel>(SelectedModel);
        }
    }
}