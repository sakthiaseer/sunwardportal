﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class TransferReceiveItemViewModel : ViewModelBase
    {
        public Command SaveCommand { get; set; }
        public Command ExitCommand { get; set; }
        public APPTransferReClassLineModel Model
        {
            get { return _model; }
            set { _model = value; OnPropertyChanged(nameof(Model)); }
        }
        private APPTransferReClassLineModel _model = new APPTransferReClassLineModel();

        public TransferReceiveLineViewModel ParentViewModel
        {
            get { return _parentViewModel; }
            set { _parentViewModel = value; OnPropertyChanged(nameof(ParentViewModel)); }
        }
        private TransferReceiveLineViewModel _parentViewModel;

        public override Task InitializeAsync(object navigationData)
        {
            ParentViewModel = (navigationData as TransferReceiveLineViewModel);
            Model = ParentViewModel.SelectedModel;
            Model.TransferRelcassId = ParentViewModel.TransferReclassID;
            Model.ReceiveQuantity = Model.Quantity;
            return base.InitializeAsync(navigationData);
        }

        public TransferReceiveItemViewModel()
        {
            SaveCommand = new Command(OnSave);
            ExitCommand = new Command(OnExit);
        }
        async void OnSave()
        {
            try
            {
                if (Model.IsValidReceive())
                {
                    IsBusy = true;
                    IsEnabled = false;
                    UserDialogs.Instance.ShowLoading("Saving...");

                    var restclient = new RestClient();

                    if (Model.ApptransferRelcassLineID > 0)
                    {
                        var item = await restclient.PutAsync<APPTransferReClassLineModel>("ApptransferRelcassLines/UpdateApptransferReclassLine", Model);
                    }
                    IsBusy = false;
                    IsEnabled = true;
                    UserDialogs.Instance.HideLoading();
                    await DialogService.ShowConfirmAsync("Record saved successfully.", "SAVE", "OK", "Cancel");

                    await Task.Delay(500);

                    OnExit();
                }
                else
                {
                    await DialogService.ShowConfirmAsync("Validation error occured!.", "Validation", "OK", "Cancel");
                }
            }
            catch (Exception ex)
            {
                IsBusy = false;
                IsEnabled = true;
                UserDialogs.Instance.HideLoading();
                await DialogService.ShowConfirmAsync(ex.Message, "ERROR", "OK", "Cancel");
            }
        }
        async void OnExit()
        {
            ParentViewModel.FillData();
            await NavigationService.NavigateBackAsync();
        }
    }
}
