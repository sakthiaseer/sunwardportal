﻿using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.ViewModel.Base;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class DispensorScanViewModel : ViewModelBase
    {

        #region fields

        private AppDispenserDispensingDrumDetailModel _model = new AppDispenserDispensingDrumDetailModel();

        public AppDispenserDispensingDrumDetailModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        private DispensorDispensingWorkModel _parentModel = new DispensorDispensingWorkModel();

        public DispensorDispensingWorkModel ParentModel
        {
            get { return _parentModel; }
            set
            {
                _parentModel = value; OnPropertyChanged(nameof(ParentModel));
            }
        }

        private ImageSource _weighingPhotoSource;
        public ImageSource WeighingPhotoSource { get { return _weighingPhotoSource; } set { _weighingPhotoSource = value; OnPropertyChanged(nameof(WeighingPhotoSource)); } }


        #endregion

        public Command AddDrumCommand { get; set; }
        public Command ClearCommand { get; set; }
        public ICommand FocusedCommand { get; set; }
        public ICommand TakePhotoCommand { get; set; }

        public DispensorScanViewModel()
        {
            AddDrumCommand = new Command(OnAddDrum);
            ClearCommand = new Command(OnClear);
            FocusedCommand = new Command(OnDrumScan);
            TakePhotoCommand = new Command<string>(OnTakePhoto);
        }
        public override Task InitializeAsync(object navigationData)
        {
            ParentModel = navigationData as DispensorDispensingWorkModel;
            return base.InitializeAsync(navigationData);
        }
        private async void OnAddDrum()
        {
            await NavigationService.NavigateBackAsync();
            Model.DispenserDispensingLineId = ParentModel.DispenserDispensingLineId;
            MessagingCenter.Send(Model, "AddDrums");
        }

        void OnDrumScan()
        {
#if DEBUG
            string qrCode = "WO19-M0258~RM19-M0258~Prozine Syrup 50X120ML~3";

            var barcode = qrCode.Split('~');
            if (barcode.Length > 3)
            {
                Model.MaterialNo = barcode[0].Trim();
                Model.LotNo = barcode[1].Trim();
                Model.QcrefNo = barcode[2].Trim();

            }

#endif
        }

        void OnClear()
        {
            Model.MaterialNo = string.Empty;
            Model.LotNo = string.Empty;
            Model.QcrefNo = string.Empty;
        }

        private async void OnTakePhoto(string photoType)
        {
            Image image = new Image();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DialogService.ShowConfirmAsync("No Camera", ":( No camera available.", "OK", "Cancel");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 30,
                //CustomPhotoSize = 50,
                PhotoSize = PhotoSize.Medium,
                //MaxWidthHeight = 2000,
                DefaultCamera = CameraDevice.Rear
            });

            if (file == null)
                return;

            if (photoType == "Weighing")
            {
                Model.WeighingPhotoStream = ReadFully(file.GetStream());
                Model.WeighingPhoto = Guid.NewGuid().ToString() + ".png";
                WeighingPhotoSource = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    file.Dispose();
                    return stream;
                });
            }
        }

        public byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        ~DispensorScanViewModel()
        {
        }

    }
}
