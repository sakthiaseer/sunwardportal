﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using FormsVideoLibrary;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class AssetItemViewModel : ViewModelBase
    {
        private VideoSource _videoURL = VideoSource.FromUri("");
        public VideoSource VideoUrl { get => _videoURL; set { _videoURL = value; OnPropertyChanged(); } }

        public AssetItemViewModel()
        {
            SaveAssetItemCommand = new Command(Save);
            ExitCommand = new Command(OnExit);
            ItemGroupSelectionChangedCommand = new Command(OnItemGroupSelectionChange);
            if (SelectedPlantMaintenanceEntryLineItem != null)
            {
                FillData();
            }
            TakeFrontPhotoCommand = new Command(OnTakeFrontPhoto);
            TakeBackPhotoCommand = new Command(OnTakeBackPhoto);
            TakeVideoCommand = new Command(OnTakeVideo);
        }
        public async void FillFromData()
        {
            UserDialogs.Instance.ShowLoading("Loading...");
            var restclient = new RestClient();
            var entries = await restclient.GetItem<PlantMaintenanceEntryLineModel>("PlantMaintenanceEntryLine/GetPlantMaintenanceEntryLines", SelectedPlantMaintenanceEntryLineItem.PlantEntryLineID);
            if (entries != null)
            {
                entries.FrontPhotoSource = ImageSource.FromStream(() => new MemoryStream(entries.FrontPhoto));
                entries.BackPhotoSource = ImageSource.FromStream(() => new MemoryStream(entries.BackPhoto));
                SelectedPlantMaintenanceEntryLineItem.LocationVideo = entries.LocationVideo;
                SelectedPlantMaintenanceEntryLineItem.BackPhoto = entries.BackPhoto;
                FrontPhotoSource = entries.FrontPhotoSource;
                BackPhotoSource = entries.BackPhotoSource;
            }
            UserDialogs.Instance.HideLoading();
        }
        public override Task InitializeAsync(object navigationData)
        {
            ParentViewModel = (navigationData as AssetItemsViewModel);
            SelectedPlantMaintenanceEntryLineItem = ParentViewModel.SelectedPlantMaintenanceEntry;


            if (!string.IsNullOrEmpty(SelectedPlantMaintenanceEntryLineItem.VideoFileName))
            {

                if (Settings.DBName == "LIVE")
                {
                    SelectedPlantMaintenanceEntryLineItem.VideoURL = Constant.LIVEURL + SelectedPlantMaintenanceEntryLineItem.VideoFileName;
                }
                else
                {
                    SelectedPlantMaintenanceEntryLineItem.VideoURL = Constant.DEVURL + SelectedPlantMaintenanceEntryLineItem.VideoFileName;
                }
                VideoUrl = VideoSource.FromUri(SelectedPlantMaintenanceEntryLineItem.VideoURL);
            }
            if (string.IsNullOrEmpty(SelectedPlantMaintenanceEntryLineItem.EntryNumber))
            {
                SelectedPlantMaintenanceEntryLineItem.EntryNumber = DateTime.Now.Ticks.ToString();
            }
            FrontPhotoSource = ParentViewModel.SelectedPlantMaintenanceEntry.FrontPhotoSource;
            BackPhotoSource = ParentViewModel.SelectedPlantMaintenanceEntry.BackPhotoSource;

            SelectedItemGroup = ItemGroups.FirstOrDefault(s => s.Id == SelectedPlantMaintenanceEntryLineItem.ItemGroupId);

            if (SelectedPlantMaintenanceEntryLineItem.PlantEntryLineID > 0)
            {
                // FillFromData();
            }
            else
            {
                if (Settings.DBName == "LIVE")
                {
                    FrontPhotoSource = ImageSource.FromUri(new Uri(Constant.LIVEURL + "/Noimages.png"));
                    BackPhotoSource = ImageSource.FromUri(new Uri(Constant.LIVEURL + "/Noimages.png"));
                }
                else
                {
                    FrontPhotoSource = ImageSource.FromUri(new Uri(Constant.DEVURL + "/Noimages.png"));
                    BackPhotoSource = ImageSource.FromUri(new Uri(Constant.DEVURL + "/Noimages.png"));
                }
            }

            return base.InitializeAsync(navigationData);
        }


        public Command ItemGroupSelectionChangedCommand { get; set; }
        public Command SaveAssetItemCommand { get; set; }
        public Command ExitCommand { get; set; }
        public Command TakeFrontPhotoCommand { get; set; }
        public Command PickFrontPhotoCommand { get; set; }
        public Command TakeBackPhotoCommand { get; set; }
        public Command PickBackPhotoCommand { get; set; }
        public Command TakeVideoCommand { get; set; }
        public Command PickVideoCommand { get; set; }

        public Command PlayVideoCommand { get; set; }
        public bool IsValidated { get; set; }


        public IList<SelectionModel> ItemGroups { get; set; } = new ObservableCollection<SelectionModel>();

        public AssetItemsViewModel ParentViewModel
        {
            get { return _parentViewModel; }
            set { _parentViewModel = value; OnPropertyChanged(nameof(ParentViewModel)); }
        }

        private AssetItemsViewModel _parentViewModel;
        public PlantMaintenanceEntryLineModel SelectedPlantMaintenanceEntryLineItem
        {
            get { return selectedPlantMaintenanceEntryLineItem; }
            set { selectedPlantMaintenanceEntryLineItem = value; OnPropertyChanged(nameof(SelectedPlantMaintenanceEntryLineItem)); }
        }

        private PlantMaintenanceEntryLineModel selectedPlantMaintenanceEntryLineItem = new PlantMaintenanceEntryLineModel();

        public SelectionModel SelectedItemGroup
        {
            get { return selectedItemGroup; }
            set { selectedItemGroup = value; OnPropertyChanged(nameof(SelectedItemGroup)); }
        }

        private SelectionModel selectedItemGroup = new SelectionModel();

        private ImageSource _frontPhotoSource;
        public ImageSource FrontPhotoSource { get { return _frontPhotoSource; } set { _frontPhotoSource = value; OnPropertyChanged(nameof(FrontPhotoSource)); } }

        private ImageSource _backPhotoSource;
        public ImageSource BackPhotoSource { get { return _backPhotoSource; } set { _backPhotoSource = value; OnPropertyChanged(nameof(BackPhotoSource)); } }


        async void FillData()
        {
            var restclient = new RestClient();
            var itemGroups = await restclient.Get<SourceListMobile>("SourceList/GetMobileSourceLists");

            itemGroups.ForEach(c =>
            {
                ItemGroups.Add(new SelectionModel { Id = c.SourceListID, Name = c.Name });
            });
        }

        async void Save()
        {
            try
            {
                if (SelectedItemGroup == null || SelectedItemGroup.Id <= 0)
                {
                    await Application.Current.MainPage.DisplayAlert("", "Item group is required field!", "OK");
                    return;
                }
                IsBusy = true;
                IsEnabled = false;
                UserDialogs.Instance.ShowLoading("Saving...");

                var restclient = new RestClient();
                SelectedPlantMaintenanceEntryLineItem.FrontPhotoSource = null;
                SelectedPlantMaintenanceEntryLineItem.BackPhotoSource = null;
                if (SelectedPlantMaintenanceEntryLineItem.PlantEntryLineID > 0)
                {
                    SelectedPlantMaintenanceEntryLineItem.ItemGroupId = SelectedItemGroup.Id;
                    var item = await restclient.PutAsync<PlantMaintenanceEntryLineModel>("PlantMaintenanceEntryLine/UpdatePlantMaintenanceEntryLines", SelectedPlantMaintenanceEntryLineItem);
                    SelectedPlantMaintenanceEntryLineItem.FrontPhotoSource = ImageSource.FromStream(() => new MemoryStream(item.FrontPhoto));
                    SelectedPlantMaintenanceEntryLineItem.BackPhotoSource = ImageSource.FromStream(() => new MemoryStream(item.BackPhoto));
                }
                else
                {
                    SelectedPlantMaintenanceEntryLineItem.PlantEntryId = ParentViewModel.PlantEntryID;
                    SelectedPlantMaintenanceEntryLineItem.ItemGroupId = SelectedItemGroup.Id;
                    await restclient.PostAsync<PlantMaintenanceEntryLineModel>("PlantMaintenanceEntryLine/InsertPlantMaintenanceEntryLines", SelectedPlantMaintenanceEntryLineItem);
                }
                IsBusy = false;
                IsEnabled = true;
                UserDialogs.Instance.HideLoading();
                await DialogService.ShowConfirmAsync("Record saved successfully.", "SAVE", "OK", "Cancel");

                await Task.Delay(500);

                OnExit();
            }
            catch (Exception ex)
            {
                await DialogService.ShowConfirmAsync(ex.Message, "ERROR", "OK", "Cancel");
            }
        }

        async void OnExit()
        {
            if (SelectedPlantMaintenanceEntryLineItem.PlantEntryLineID == 0)
            {
                ParentViewModel.FillData();
            }
            await NavigationService.NavigateBackAsync();
        }

        void OnItemGroupSelectionChange()
        {

        }

        async void OnTakeFrontPhoto()
        {
            Image image = new Image();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DialogService.ShowConfirmAsync("No Camera", ":( No camera available.", "OK", "Cancel");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 30,
                //CustomPhotoSize = 50,
                PhotoSize = PhotoSize.Medium,
                //MaxWidthHeight = 2000,
                DefaultCamera = CameraDevice.Rear
            });

            if (file == null)
                return;
            SelectedPlantMaintenanceEntryLineItem.FrontPhoto = ReadFully(file.GetStream());
            SelectedPlantMaintenanceEntryLineItem.FrontPhotoName = Guid.NewGuid().ToString() + ".png";
            FrontPhotoSource = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                file.Dispose();
                return stream;
            });
        }

        async void OnTakeBackPhoto()
        {
            Image image = new Image();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DialogService.ShowConfirmAsync("No Camera", ":( No camera available.", "OK", "Cancel");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 30,
                //CustomPhotoSize = 50,
                PhotoSize = PhotoSize.Medium,
                //MaxWidthHeight = 2000,
                DefaultCamera = CameraDevice.Rear
            });

            if (file == null)
                return;


            SelectedPlantMaintenanceEntryLineItem.BackPhoto = ReadFully(file.GetStream());
            SelectedPlantMaintenanceEntryLineItem.BackPhotoName = Guid.NewGuid().ToString() + ".png";
            BackPhotoSource = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                file.Dispose();
                return stream;
            });
        }

        async void OnTakeVideo()
        {
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakeVideoSupported)
            {
                await DialogService.ShowConfirmAsync("No Camera", ":( No camera available.", "OK", "Cancel");
                return;
            }
            var fileName = Guid.NewGuid().ToString() + ".mp4";
            var file = await CrossMedia.Current.TakeVideoAsync(new Plugin.Media.Abstractions.StoreVideoOptions
            {
                Name = fileName,
                Directory = "DefaultVideos",
                Quality = VideoQuality.Medium,
                DefaultCamera = CameraDevice.Rear,
                DesiredLength = TimeSpan.FromMinutes(2),
                CompressionQuality = 50,
            });

            if (file == null)
                return;

            SelectedPlantMaintenanceEntryLineItem.LocationVideo = ReadFully(file.GetStream());
            SelectedPlantMaintenanceEntryLineItem.VideoFileName = fileName;
            SelectedPlantMaintenanceEntryLineItem.VideoURL = file.Path;
            VideoUrl = VideoSource.FromUri(SelectedPlantMaintenanceEntryLineItem.VideoURL);


            file.Dispose();
        }

        public byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }


    }
}
