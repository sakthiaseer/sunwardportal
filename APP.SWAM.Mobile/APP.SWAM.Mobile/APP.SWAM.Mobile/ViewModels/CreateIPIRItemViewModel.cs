﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.MultiSelectListView;
using ZXing.Mobile;

namespace APP.SWAM.Mobile.ViewModels
{
    public class CreateIPIRItemViewModel : ViewModelBase
    {
        #region fields
        private IPIRModel _model;

        private bool isRefreshing;
        #endregion

        #region Properties

        public IPIRModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }


        public Command IntendedActionCommand { get; set; }
        public Command SaveCommand { get; set; }
        public Command ClearCommand { get; set; }
        public Command ProdCommand { get; set; }
        public Command LocationScanCommand { get; set; }

        private ImageSource _frontPhotoSource;
        public ImageSource FrontPhotoSource { get { return _frontPhotoSource; } set { _frontPhotoSource = value; OnPropertyChanged(nameof(FrontPhotoSource)); } }
        public Command TakeFrontPhotoCommand { get; set; }

        private ImageSource _frontPhotoSource1;
        public ImageSource FrontPhotoSource1 { get { return _frontPhotoSource1; } set { _frontPhotoSource1 = value; OnPropertyChanged(nameof(FrontPhotoSource1)); } }
        public Command TakeFrontPhoto1Command { get; set; }

        private ImageSource _frontPhotoSource2;
        public ImageSource FrontPhotoSource2 { get { return _frontPhotoSource2; } set { _frontPhotoSource2 = value; OnPropertyChanged(nameof(FrontPhotoSource2)); } }
        public Command TakeFrontPhoto2Command { get; set; }

        public Command IPIRIssueCommand { get; set; }
        public Command IPIRIssueTitleCommand { get; set; }

        public Command IPIRIssue1Command { get; set; }
        public Command IPIRIssue1TitleCommand { get; set; }
        public Command ApplicationUserCommand { get; set; }
        public Command ApplicationUser1Command { get; set; }

        private List<string> _intendedItems = new List<string>();
        public List<string> IntendedItems
        {
            get { return _intendedItems; }
            set { _intendedItems = value; OnPropertyChanged(nameof(IntendedItems)); }
        }

        private List<string> _ipirIssues = new List<string>();
        public List<string> IPIRIssues
        {
            get { return _ipirIssues; }
            set { _ipirIssues = value; OnPropertyChanged(nameof(_ipirIssues)); }

        }
        List<ApplicationMasterDetailModel> IPIRIssueItems = new List<ApplicationMasterDetailModel>();

        private List<string> _ipirIssueTitles = new List<string>();
        public List<string> IPIRIssueTitles
        {
            get { return _ipirIssueTitles; }
            set { _ipirIssueTitles = value; OnPropertyChanged(nameof(IPIRIssueTitles)); }

        }
        List<ApplicationMasterDetailModel> IPIRIssueTitleItems = new List<ApplicationMasterDetailModel>();

        private List<string> _ipirIssue1Titles = new List<string>();
        public List<string> IPIRIssue1Titles
        {
            get { return _ipirIssue1Titles; }
            set { _ipirIssue1Titles = value; OnPropertyChanged(nameof(IPIRIssue1Titles)); }

        }
        List<ApplicationMasterDetailModel> IPIRIssue1TitleItems = new List<ApplicationMasterDetailModel>();
        private List<string> _ipir1Issues = new List<string>();
        public List<string> IPIR1Issues
        {
            get { return _ipir1Issues; }
            set { _ipir1Issues = value; OnPropertyChanged(nameof(_ipir1Issues)); }

        }
        List<ApplicationMasterDetailModel> IPIRIssue1Items = new List<ApplicationMasterDetailModel>();
        List<CodeMasterModel> codeMasters = new List<CodeMasterModel>();
        List<ApplicationUserModel> applicationUserItems = new List<ApplicationUserModel>();
        private List<string> _applicationUsers = new List<string>();
        public List<string> ApplicationUsers
        {
            get { return _applicationUsers; }
            set { _applicationUsers = value; OnPropertyChanged(nameof(ApplicationUsers)); }

        }
        private List<string> _applicationUsers1 = new List<string>();
        public List<string> ApplicationUsers1
        {
            get { return _applicationUsers1; }
            set { _applicationUsers1 = value; OnPropertyChanged(nameof(ApplicationUsers1)); }

        }

        List<CodeMasterModel> StatusAssignmentItems = new List<CodeMasterModel>();

        private List<string> statusAssignments = new List<string>();
        public List<string> StatusAssignments
        {
            get { return statusAssignments; }
            set { statusAssignments = value; OnPropertyChanged(nameof(StatusAssignments)); }

        }

        List<CodeMasterModel> IPIRStatusItems = new List<CodeMasterModel>();

        private List<string> ipirStatus = new List<string>();
        public List<string> IPIRStatus
        {
            get { return ipirStatus; }
            set { ipirStatus = value; OnPropertyChanged(nameof(ipirStatus)); }

        }

        private bool _isEnableReportAnIssue;
        public bool IsEnableReportAnIssue
        {
            get { return _isEnableReportAnIssue; }
            set { _isEnableReportAnIssue = value; OnPropertyChanged(nameof(IsEnableReportAnIssue)); }
        }

        private bool _isEnableReportAndCreate;
        public bool IsEnableReportAndCreate
        {
            get { return _isEnableReportAndCreate; }
            set { _isEnableReportAndCreate = value; OnPropertyChanged(nameof(IsEnableReportAndCreate)); }
        }

        private bool _isEnableReportAssignment;
        public bool IsEnableReportAssignment
        {
            get { return _isEnableReportAssignment; }
            set { _isEnableReportAssignment = value; OnPropertyChanged(nameof(IsEnableReportAssignment)); }
        }

        #endregion

        public CreateIPIRItemViewModel()
        {
            SaveCommand = new Command(OnSave);
            ClearCommand = new Command(OnExit);
            ProdCommand = new Command(OnProdScan);
            LocationScanCommand = new Command(OnLocationScan);
            TakeFrontPhotoCommand = new Command(OnTakeFrontPhoto);
            TakeFrontPhoto1Command = new Command(OnTakeFrontPhoto1);
            TakeFrontPhoto2Command = new Command(OnTakeFrontPhoto2);
            IPIRIssueCommand = new Command(OnIPIRIssue);
            IPIRIssueTitleCommand = new Command(OnIPIRIssueTitle);
            IPIRIssue1Command = new Command(OnIPIR1Issue);
            IPIRIssue1TitleCommand = new Command(OnIPIRIssue1Title);
            ApplicationUserCommand = new Command(OnApplicationUser);
            ApplicationUser1Command = new Command(OnApplicationUser1);
            IntendedActionCommand = new Command(OnIntendedAction);
            Title = "Create IPIR " + "(" + Settings.DBName + ")";
            Model = new IPIRModel
            {
                LoginUser = Constant.Username,
                StartDate = DateTime.Now,
                CompanyName = Settings.NAVName,
            };

            MessagingCenter.Subscribe<MultiSelectionModel>(this, "AddIPIRIssue", mSelection =>
            {
                foreach (var item in mSelection.MultiselectionItems)
                {
                    if (item.IsSelected)
                    {
                        Model.IpirIssueIds.Add(item.Data.Id);
                        Model.IPIRIssue += item.Data.Title + ",";
                    }
                }
            });
            MessagingCenter.Subscribe<MultiSelectionModel>(this, "AddIPIRIssueTitle", mSelection =>
            {
                foreach (var item in mSelection.MultiselectionItems)
                {
                    if (item.IsSelected)
                    {
                        Model.IpirIssueTitleIds.Add(item.Data.Id);
                        Model.IPIRIssueTitle += item.Data.Title + ",";
                    }
                }
            });
            MessagingCenter.Subscribe<MultiSelectionModel>(this, "AddIPIRIssue1", mSelection =>
            {
                foreach (var item in mSelection.MultiselectionItems)
                {
                    if (item.IsSelected)
                    {
                        Model.IpirIssue1Ids.Add(item.Data.Id);
                        Model.IPIR1Issue += item.Data.Title + ",";
                    }
                }
            });
            MessagingCenter.Subscribe<MultiSelectionModel>(this, "AddIPIRIssueTitle1", mSelection =>
            {
                foreach (var item in mSelection.MultiselectionItems)
                {
                    if (item.IsSelected)
                    {
                        Model.IpirIssue1TitleIds.Add(item.Data.Id);
                        Model.IPIRIssue1Title += item.Data.Title + ",";
                    }
                }
            });

            MessagingCenter.Subscribe<ApplicationUserModel>(this, "AddSelectedUser", mSelection =>
            {
                Model.ApplicationUser = mSelection.Name;
                Model.ApplicationUserId = mSelection.UserID;
            });
            MessagingCenter.Subscribe<ApplicationUserModel>(this, "AddSelectedUser1", mSelection =>
            {
                Model.ApplicationUser1 = mSelection.Name;
                Model.ApplicationUserId1 = mSelection.UserID;
            });

           
            if (Settings.DBName == "LIVE")
            {
                FrontPhotoSource = ImageSource.FromUri(new Uri(Constant.LIVEURL + "/Noimages.png"));
            }
            else
            {
                FrontPhotoSource = ImageSource.FromUri(new Uri(Constant.DEVURL + "/Noimages.png"));
            }
            LoadChoices();
        }

        public override Task InitializeAsync(object navigationData)
        {
            Model = (navigationData as IPIRModel);

            IsEnableReportAnIssue = Model.IntendedAction == "Report an issue";
            if (Model.IntendedAction == "Report and create IPIR")
            {
                IsEnableReportAnIssue = true;
                IsEnableReportAndCreate = true;
            }
            if (Model.IntendedAction == "Report on assignment")
            {
                IsEnableReportAnIssue = true;
                IsEnableReportAndCreate = true;
                IsEnableReportAssignment = true;
            }
            return base.InitializeAsync(navigationData);
        }

        private async void LoadChoices()
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                codeMasters = await restClient.GetCodeMasterByType<CodeMasterModel>("ApplicationUser/GetCodeMasterByType", "IntendedAction");
                applicationUserItems = await restClient.Get<ApplicationUserModel>("ApplicationUser/GetApplicationUsers");
                ApplicationUsers = applicationUserItems.Select(u => u.Name).ToList();
                ApplicationUsers1 = applicationUserItems.Select(u => u.Name).ToList();
                IntendedItems = codeMasters.Select(q => q.CodeValue).ToList();
                StatusAssignmentItems = await restClient.GetCodeMasterByType<CodeMasterModel>("ApplicationUser/GetCodeMasterByType", "StatusAssignment");
                StatusAssignments = StatusAssignmentItems.Select(q => q.CodeValue).ToList();
                IPIRStatusItems = await restClient.GetCodeMasterByType<CodeMasterModel>("ApplicationUser/GetCodeMasterByType", "IPIRStatus");
                IPIRStatus = IPIRStatusItems.Select(q => q.CodeValue).ToList();
                IPIRIssueItems = await restClient.GetApplicationMasterByType<ApplicationMasterDetailModel>("ApplicationMaster/GetApplicationMasterDetailByType", 231);
                IPIRIssues = IPIRIssueItems.Select(q => q.Value).ToList();
                IPIR1Issues = IPIRIssueItems.Select(q => q.Value).ToList();
                IPIRIssueTitleItems = await restClient.GetApplicationMasterByType<ApplicationMasterDetailModel>("ApplicationMaster/GetApplicationMasterDetailByType", 232);
                IPIRIssueTitles = IPIRIssueTitleItems.Select(q => q.Value).ToList();
                IPIRIssue1Titles = IPIRIssueTitleItems.Select(q => q.Value).ToList();
                var iPIRMobilePost = await restClient.PostAsync<IPIRMobilePost>("IPIRMobileAction/GetIPIRMobileAction", new IPIRMobilePost { IpirmobileId = Model.IpirmobileId, IntendedActionId = Model.IntendedActionId });
                LoadIPIRActionItem(iPIRMobilePost);
            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            PageDialog.HideLoading();
        }
        void OnProdScan()
        {
#if DEBUG
            string qrCode = "MT18-1945-4-GR1~10000~Fenadium F Tablet Granules~704-19005~1~MT18-1945";

            var barcode = qrCode.Split('~');
            if (barcode.Length > 4)
            {
                Model.ProductionOrderNo = barcode[0].Trim();
                Model.ProductName = barcode[2].Trim();
                Model.BatchNo = barcode[3].Trim();
            }

#else
            if (string.IsNullOrEmpty(Model.Location))
            {
                var scanner = new MobileBarcodeScanner();
                scanner.Torch(true);
                scanner.AutoFocus();

                scanner.ScanContinuously(s =>
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        if (Model == null)
                            Model = new IPIRModel();
                        var barcode = s.Text.Split('~');
                        if (barcode.Length > 3)
                        {
                            Model.ProductionOrderNo = barcode[0].Trim();
                            Model.ProductName = barcode[2].Trim();
                            Model.BatchNo = barcode[3].Trim();
                        }
                        else
                        {
                            await DialogService.ShowConfirmAsync("In-Valid QR Code.QR Code must contains 4 segment(ex:Drum Info,Process Info,Weight).Please scan valid QR Code", "QR Code", "OK", "Cancel");
 
                        }
                    });
                    scanner.Cancel();
                    scanner.Torch(false);
                });


            }
#endif
        }

        private void LoadIPIRActionItem(IPIRMobilePost iPIRMobilePost)
        {
            iPIRMobilePost.MobileActionItems.ForEach(a =>
            {
                if (a.IntendedActionId == 3151)
                {
                    Model.IsConfirmIPIR = a.IsConfirmIpir;
                    Model.IpirIssueIds = a.IssueRelatedIds;
                    Model.IpirIssueTitleIds = a.IssueTitleIds;
                    Model.IssueDescription = a.IssueDescription;
                    Model.ApplicationUser = a.AssignmentToName;
                    Model.ApplicationUserId = a.AssignmentTo;
                   
                    if (Settings.DBName == "LIVE")
                    {
                        var imageUrl = Constant.LIVEURL + "/" + Model.Photo;
                        FrontPhotoSource = ImageSource.FromUri(new Uri(imageUrl));
                    }
                    else
                    {
                        var imageUrl = Constant.DEVURL + "/" + Model.Photo;
                        FrontPhotoSource = ImageSource.FromUri(new Uri(imageUrl));
                    }
                   
                    Model.IPIRIssue = string.Empty;
                    Model.IPIRIssueTitle = string.Empty;
                    Model.ApplicationUser = applicationUserItems.FirstOrDefault(m => m.UserID == a.AssignmentTo) != null ? applicationUserItems.FirstOrDefault(m => m.UserID == a.AssignmentTo)?.Name : null;
                    foreach (var id in Model.IpirIssueIds)
                    {
                        var item = IPIRIssueItems.FirstOrDefault(i => i.ApplicationMasterDetailId == id);
                        Model.IPIRIssue += item.Value + ",";
                    }
                    foreach (var id in Model.IpirIssueTitleIds)
                    {
                        var item = IPIRIssueTitleItems.FirstOrDefault(i => i.ApplicationMasterDetailId == id);
                        Model.IPIRIssueTitle += item.Value + ",";
                    }
                }
                if (a.IntendedActionId == 3152)
                {
                    Model.IpirIssue1Ids = a.IssueRelatedIds;
                    Model.IpirIssue1TitleIds = a.IssueTitleIds;
                    Model.IssueDescription1 = a.IssueDescription;
                    Model.ApplicationUser1 = a.AssignmentToName;
                    Model.ApplicationUserId1 = a.AssignmentTo;
                    
                    if (Settings.DBName == "LIVE")
                    {
                        var imageUrl = Constant.LIVEURL + "/" + Model.Photo;
                        FrontPhotoSource1 = ImageSource.FromUri(new Uri(imageUrl));
                    }
                    else
                    {
                        var imageUrl = Constant.DEVURL + "/" + Model.Photo;
                        FrontPhotoSource1 = ImageSource.FromUri(new Uri(imageUrl));
                    }
                    Model.IPIR1Issue = string.Empty;
                    Model.IPIRIssue1Title = string.Empty;
                    Model.ApplicationUser1 = applicationUserItems.FirstOrDefault(m => m.UserID == a.AssignmentTo) != null ? applicationUserItems.FirstOrDefault(m => m.UserID == a.AssignmentTo)?.Name : null;
                    foreach (var id in Model.IpirIssue1Ids)
                    {
                        var item = IPIRIssueItems.FirstOrDefault(i => i.ApplicationMasterDetailId == id);
                        Model.IPIR1Issue += item.Value + ",";
                    }
                    foreach (var id in Model.IpirIssue1TitleIds)
                    {
                        var item = IPIRIssueTitleItems.FirstOrDefault(i => i.ApplicationMasterDetailId == id);
                        Model.IPIRIssue1Title += item.Value + ",";
                    }
                }
                if (a.IntendedActionId == 3153)
                {
                    Model.IssueDescription2 = a.IssueDescription;
                    Model.StatusAssignmentId = a.AssignmentStatusId;
                    if (a.AssignmentStatusId != null)
                    {
                        Model.StatusAssignmentName = StatusAssignmentItems.FirstOrDefault(s => s.CodeID == a.AssignmentStatusId).CodeValue;
                    }
                }
            });
        }

        void OnLocationScan()
        {
#if DEBUG
            Model.Location = "SWMY";
#else
            if (string.IsNullOrEmpty(Model.Location))
            {
                var scanner = new MobileBarcodeScanner();
                scanner.Torch(true);
                scanner.AutoFocus();

                scanner.ScanContinuously(s =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (Model == null)
                            Model = new IPIRModel();
                        Model.Location = s.Text;
                    });
                    scanner.Cancel();
                    scanner.Torch(false);
                });
            }
#endif
        }
        async void OnSave()
        {
            if (Model.IsValid())
            {
                IsBusy = true;
                IsEnabled = false;

                UserDialogs.Instance.ShowLoading("Validating IPIR...");
                IPIRMobilePost iPIRMobilePost = new IPIRMobilePost();
                Model.AddedByUserId = Settings.UserId;
                Model.IntendedActionId = codeMasters.FirstOrDefault(m => m.CodeValue == Model.IntendedAction) != null ? codeMasters.FirstOrDefault(m => m.CodeValue == Model.IntendedAction).CodeID as int? : null;
                Model.StatusAssignmentId = StatusAssignmentItems.FirstOrDefault(m => m.CodeValue == Model.StatusAssignmentName) != null ? StatusAssignmentItems.FirstOrDefault(m => m.CodeValue == Model.StatusAssignmentName)?.CodeID as int? : null;
                Model.ApplicationUserId = applicationUserItems.FirstOrDefault(m => m.Name == Model.ApplicationUser) != null ? applicationUserItems.FirstOrDefault(m => m.Name == Model.ApplicationUser)?.UserID as long? : null;
                Model.ApplicationUserId1 = applicationUserItems.FirstOrDefault(m => m.Name == Model.ApplicationUser1) != null ? applicationUserItems.FirstOrDefault(m => m.Name == Model.ApplicationUser1)?.UserID as long? : null;
                iPIRMobilePost.IpirmobileId = Model.IpirmobileId;
                iPIRMobilePost.IntendedActionId = Model.IntendedActionId;

                Model.CompanyName = Settings.NAVName;

                iPIRMobilePost.IpirmobileId = Model.IpirmobileId;

                if (IsEnableReportAnIssue)
                {

                    iPIRMobilePost.MobileActionItems.Add(new IPIRMobileActionModel
                    {
                        IpirmobileId = Model.IpirmobileId,
                        IntendedActionId = 3151,
                        IsConfirmIpir = Model.IsConfirmIPIR,
                        AssignmentTo = Model.ApplicationUserId,
                        PhotoSource = Model.PhotoSource,
                        Photo = Model.Photo,
                        IssueDescription = Model.IssueDescription,
                        IssueRelatedIds = Model.IpirIssueIds,
                        IssueTitleIds = Model.IpirIssueTitleIds,

                    });
                }
                if (IsEnableReportAndCreate)
                {
                    iPIRMobilePost.MobileActionItems.Add(new IPIRMobileActionModel
                    {
                        IpirmobileId = Model.IpirmobileId,
                        IntendedActionId = 3152,
                        AssignmentTo = Model.ApplicationUserId1,
                        Photo = Model.Photo1,
                        PhotoSource = Model.PhotoSource1,
                        IssueDescription = Model.IssueDescription1,
                        IssueRelatedIds = Model.IpirIssue1Ids,
                        IssueTitleIds = Model.IpirIssue1TitleIds,
                    });
                }
                if (IsEnableReportAssignment)
                {
                    iPIRMobilePost.MobileActionItems.Add(new IPIRMobileActionModel
                    {
                        IpirmobileId = Model.IpirmobileId,
                        IntendedActionId = 3153,
                        Photo = Model.Photo2,
                        PhotoSource = Model.PhotoSource2,
                        AssignmentStatusId = Model.StatusAssignmentId,
                        IssueDescription = Model.IssueDescription2,
                    });
                }

                var restclient = new RestClient();
                iPIRMobilePost = await restclient.PostAsync<IPIRMobilePost>("IPIRMobileAction/InsertIPIRMobileAction", iPIRMobilePost);

                IsBusy = false;
                IsEnabled = true;
                UserDialogs.Instance.HideLoading();
                if (iPIRMobilePost.IpirmobileId > 0 && iPIRMobilePost.IsError == false)
                {
                    await DialogService.ShowConfirmAsync("Record validated successfully.", "Validation", "OK", "Cancel");
                    //MessagingCenter.Send(Model, "AddProcessTransfer");
                    await NavigationService.NavigateBackAsync();
                }
                else
                {
                    await DialogService.ShowConfirmAsync(iPIRMobilePost.Errormessage, "Validation", "OK", "Cancel");
                }

            }
            else
            {
                await DialogService.ShowConfirmAsync("Validation Failed.", "Next", "OK", "Cancel");
            }
        }
        async void OnTakeFrontPhoto()
        {
            Image image = new Image();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DialogService.ShowConfirmAsync("No Camera", ":( No camera available.", "OK", "Cancel");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 30,
                //CustomPhotoSize = 50,
                PhotoSize = PhotoSize.Medium,
                //MaxWidthHeight = 2000,
                DefaultCamera = CameraDevice.Rear
            });

            if (file == null)
                return;
            Model.PhotoSource = ReadFully(file.GetStream());
            Model.Photo = Guid.NewGuid().ToString() + ".png";
            FrontPhotoSource = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                file.Dispose();
                return stream;
            });
        }
        async void OnTakeFrontPhoto1()
        {
            Image image = new Image();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DialogService.ShowConfirmAsync("No Camera", ":( No camera available.", "OK", "Cancel");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 30,
                //CustomPhotoSize = 50,
                PhotoSize = PhotoSize.Medium,
                //MaxWidthHeight = 2000,
                DefaultCamera = CameraDevice.Rear
            });

            if (file == null)
                return;
            Model.PhotoSource1 = ReadFully(file.GetStream());
            Model.Photo1 = Guid.NewGuid().ToString() + ".png";
            FrontPhotoSource1 = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                file.Dispose();
                return stream;
            });
        }
        async void OnTakeFrontPhoto2()
        {
            Image image = new Image();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DialogService.ShowConfirmAsync("No Camera", ":( No camera available.", "OK", "Cancel");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 30,
                //CustomPhotoSize = 50,
                PhotoSize = PhotoSize.Medium,
                //MaxWidthHeight = 2000,
                DefaultCamera = CameraDevice.Rear
            });

            if (file == null)
                return;
            Model.PhotoSource2 = ReadFully(file.GetStream());
            Model.Photo2 = Guid.NewGuid().ToString() + ".png";
            FrontPhotoSource2 = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                file.Dispose();
                return stream;
            });
        }
        public byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        void OnExit()
        {
            Model = new IPIRModel
            {
                LoginUser = Constant.Username,
                StartDate = DateTime.Now,
                BatchNo = string.Empty,
                CompanyId = null,
                CompanyName = string.Empty,
                IntendedAction = string.Empty,
                IssueDescription = string.Empty,
                IntendedActionId = null,
                IpirmobileId = 0,
                IpirreportNo = string.Empty,
                Location = string.Empty,
                Photo = string.Empty,
                PhotoSource = null,
                ProductionOrderNo = string.Empty,
                ProductName = string.Empty
            };
        }

        private async void OnIPIRIssue()
        {
            MultiSelectObservableCollection<MultiSelectionModel> items = new MultiSelectObservableCollection<MultiSelectionModel>();
            IPIRIssueItems.ForEach(p =>
            {
                items.Add(new MultiSelectionModel
                {
                    Id = p.ApplicationMasterDetailId,
                    Title = p.Value,
                });
            });
            MultiSelectionModel multiSelectionModel = new MultiSelectionModel
            {
                MultiSelectionName = "IPIR Issue",
                MultiselectionItems = items
            };
            await NavigationService.NavigateToAsync<MultiselectionViewModel>(multiSelectionModel);
        }
        private async void OnIPIRIssueTitle()
        {
            MultiSelectObservableCollection<MultiSelectionModel> items = new MultiSelectObservableCollection<MultiSelectionModel>();
            IPIRIssueTitleItems.ForEach(p =>
            {
                items.Add(new MultiSelectionModel
                {
                    Id = p.ApplicationMasterDetailId,
                    Title = p.Value,
                });
            });
            MultiSelectionModel multiSelectionModel = new MultiSelectionModel
            {
                MultiSelectionName = "IPIR Issue Title",
                MultiselectionItems = items
            };
            await NavigationService.NavigateToAsync<MultiselectionViewModel>(multiSelectionModel);
        }

        private async void OnIPIR1Issue()
        {
            MultiSelectObservableCollection<MultiSelectionModel> items = new MultiSelectObservableCollection<MultiSelectionModel>();
            IPIRIssueItems.ForEach(p =>
            {
                items.Add(new MultiSelectionModel
                {
                    Id = p.ApplicationMasterDetailId,
                    Title = p.Value,
                });
            });
            MultiSelectionModel multiSelectionModel = new MultiSelectionModel
            {
                MultiSelectionName = "IPIR Issue 1",
                MultiselectionItems = items
            };
            await NavigationService.NavigateToAsync<MultiselectionViewModel>(multiSelectionModel);
        }
        private async void OnIPIRIssue1Title()
        {
            MultiSelectObservableCollection<MultiSelectionModel> items = new MultiSelectObservableCollection<MultiSelectionModel>();
            IPIRIssueTitleItems.ForEach(p =>
            {
                items.Add(new MultiSelectionModel
                {
                    Id = p.ApplicationMasterDetailId,
                    Title = p.Value,
                });
            });
            MultiSelectionModel multiSelectionModel = new MultiSelectionModel
            {
                MultiSelectionName = "IPIR Issue Title 1",
                MultiselectionItems = items
            };
            await NavigationService.NavigateToAsync<MultiselectionViewModel>(multiSelectionModel);
        }

        private async void OnApplicationUser()
        {
            ApplicationUserListViewModel applicationUserListViewModel = new ApplicationUserListViewModel();
            applicationUserListViewModel.ScreenName = "ApplicationUser";
            await NavigationService.NavigateToAsync<ApplicationUserListViewModel>(applicationUserListViewModel);
        }
        private async void OnApplicationUser1()
        {
            ApplicationUserListViewModel applicationUserListViewModel = new ApplicationUserListViewModel();
            applicationUserListViewModel.ScreenName = "ApplicationUser1";
            await NavigationService.NavigateToAsync<ApplicationUserListViewModel>(applicationUserListViewModel);
        }

        private void OnIntendedAction()
        {
            if (Model.IntendedAction == "Report an issue")
            {
                Model.IntendedActionId = 3151;
                IsEnableReportAnIssue = true;
            }
            if (Model.IntendedAction == "Report and create IPIR")
            {
                Model.IntendedActionId = 3152;
                IsEnableReportAnIssue = true;
                IsEnableReportAndCreate = true;
            }
            if (Model.IntendedAction == "Report on assignment")
            {
                Model.IntendedActionId = 3153;
                IsEnableReportAnIssue = true;
                IsEnableReportAndCreate = true;
                IsEnableReportAssignment = true;
            }
        }
    }
}