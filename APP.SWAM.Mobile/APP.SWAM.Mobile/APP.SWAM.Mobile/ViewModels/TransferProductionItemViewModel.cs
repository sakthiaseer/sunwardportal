﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using ZXing.Mobile;

namespace APP.SWAM.Mobile.ViewModels
{
    public class TransferProductionItemViewModel : ViewModelBase
    {
        public Command SaveCommand { get; set; }
        public Command ExitCommand { get; set; }
        public ICommand TextboxFocusedEvent { get; set; }
        public ICommand LotNoFocusedCommand { get; set; }
        public APPTransferReClassLineModel Model
        {
            get { return _model; }
            set { _model = value; OnPropertyChanged(nameof(Model)); }
        }
        private APPTransferReClassLineModel _model;

        public TransferProductionLineViewModel ParentViewModel
        {
            get { return _parentViewModel; }
            set { _parentViewModel = value; OnPropertyChanged(nameof(ParentViewModel)); }
        }
        private TransferProductionLineViewModel _parentViewModel;

        public override Task InitializeAsync(object navigationData)
        {
            ParentViewModel = (navigationData as TransferProductionLineViewModel);
            Model = ParentViewModel.SelectedModel;
            Model.TransferRelcassId = ParentViewModel.TransferReclassID;
            return base.InitializeAsync(navigationData);
        }

        public TransferProductionItemViewModel()
        {
            SaveCommand = new Command(OnSave);
            ExitCommand = new Command(OnExit);
            TextboxFocusedEvent = new Command(OnScan);
            LotNoFocusedCommand = new Command(OnLotFocused);
        }
        async void OnScan()
        {
            string qrCode = "DRUM001~Fenadium F Tablet Granules~MT18-1945-4-GR1~10000~KG~1";

#if DEBUG


            var barcode = qrCode.Split('~');
            if (barcode.Length > 4)
            {
                Model.DrumNo = barcode[0].Trim();
                Model.ItemNo = barcode[1].Trim();
                Model.QCRefNo = barcode[2].Trim();
                Model.UOM = barcode[4].Trim();
                Model.Quantity = barcode[5].Trim();
            }
#else
            if (string.IsNullOrEmpty(Model.DrumNo))
            {
                int onlyThisAmount = 4;
                string ticks = DateTime.Now.Ticks.ToString();
                ticks = ticks.Substring(ticks.Length - onlyThisAmount);
                int entryNumber = int.Parse(ticks);

                var scanner = new MobileBarcodeScanner
                {
                    UseCustomOverlay = false,
                    CameraUnsupportedMessage = "Camera not supporting.",
                };
                var result = await scanner.Scan();
                var barcode = result.Text.Split('~');
                if (barcode.Length > 3)
                {
                    Model.DrumNo = "DR-" + entryNumber.ToString();
                    Model.ItemNo = barcode[0].Trim();
                    Model.LotNo = barcode[1].Trim();
                    Model.QCRefNo = barcode[2].Trim();
                }
                else
                {
                    await DialogService.ShowAlertAsync("In-Valid QR Code.QR Code must contains 3 segment(ex:ItemNo,LotNo,QCRefNo).Please scan valid QR Code", "QR Code", "OK");
                }
            }
#endif
        }


        async void OnLotFocused()
        {
            string qrCode = "10000";

#if DEBUG


            if (!String.IsNullOrEmpty(qrCode))
            {
                Model.LotNo = qrCode.Trim();
            }
#else
            int onlyThisAmount = 4;
            string ticks = DateTime.Now.Ticks.ToString();
            ticks = ticks.Substring(ticks.Length - onlyThisAmount);
            int entryNumber = int.Parse(ticks);

            if (string.IsNullOrEmpty(Model.LotNo))
            {
                var scanner = new MobileBarcodeScanner();
                scanner.Torch(true);
                scanner.AutoFocus();

                scanner.ScanContinuously(s =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        var barcode = s.Text;

                        Model.LotNo = barcode.Trim();
                        Model.DrumNo = "DR-" + entryNumber.ToString();
                    });
                    scanner.Cancel();
                    scanner.Torch(false);
                });
            }
#endif
        }

        async void OnSave()
        {
            try
            {
                if (Model.IsValid())
                {
                    IsBusy = true;
                    IsEnabled = false;
                    UserDialogs.Instance.ShowLoading("Saving...");

                    var restclient = new RestClient();

                    if (Model.ApptransferRelcassLineID > 0)
                    {
                        var item = await restclient.PutAsync<APPTransferReClassLineModel>("ApptransferRelcassLines/UpdateApptransferReclassLine", Model);
                    }
                    else
                    {
                        Model.TransferRelcassId = ParentViewModel.TransferReclassID;
                        await restclient.PostAsync<APPTransferReClassLineModel>("ApptransferRelcassLines/InsertApptransferReclassLine", Model);
                    }
                    IsBusy = false;
                    IsEnabled = true;
                    UserDialogs.Instance.HideLoading();
                    await DialogService.ShowConfirmAsync("Record saved successfully.", "SAVE", "OK", "Cancel");

                    await Task.Delay(500);

                    OnExit();
                }
                else
                {
                    await DialogService.ShowConfirmAsync("Validation error occured!.", "Validation", "OK", "Cancel");
                }
            }
            catch (Exception ex)
            {
                IsBusy = false;
                IsEnabled = true;
                UserDialogs.Instance.HideLoading();
                await DialogService.ShowConfirmAsync(ex.Message, "ERROR", "OK", "Cancel");
            }
        }
        async void OnExit()
        {
            ParentViewModel.FillData();
            await NavigationService.NavigateBackAsync();
        }
    }
}

