﻿using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    class BMRToCartonLineViewModel : ViewModelBase
    {
        #region fields

        private BMRToCartonLineModel _model;
        private ObservableCollection<BMRToCartonLineModel> _modelEntries = new ObservableCollection<BMRToCartonLineModel>();
        private bool isRefreshing;
        private long _bmrtoCartonId;
        private BMRToCartonLineModel _selectedItem = new BMRToCartonLineModel();
        #endregion

        #region Properties

        public BMRToCartonLineModel SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value; OnPropertyChanged(nameof(SelectedItem));
            }
        }

        public ObservableCollection<BMRToCartonLineModel> BMRToCartonLines
        {
            get { return _modelEntries; }
            set { _modelEntries = value; OnPropertyChanged(nameof(BMRToCartonLines)); }
        }

        public BMRToCartonLineModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }

        public long BmrtoCartonId
        {
            get { return _bmrtoCartonId; }
            set { _bmrtoCartonId = value; OnPropertyChanged(nameof(BmrtoCartonId)); }
        }


        public Command AddLineCommand { get; set; }
        public Command DeleteCommand { get; set; }
        public ICommand SelectedItemLine { get; set; }
        #endregion

        public BMRToCartonLineViewModel()
        {
            AddLineCommand = new Command(OnAdd);
            DeleteCommand = new Command(OnDelete);
            SelectedItemLine = new Command(OnSelected);
            Model = new BMRToCartonLineModel
            {
                LoginUser = Constant.Username,
                StartDate = DateTime.Now,
            };

            MessagingCenter.Subscribe<BMRToCartonLineModel>(this, "AddBMRToCarton", jobModel =>
            {
                OnReloadJob(jobModel);
            });
        }

        public override Task InitializeAsync(object navigationData)
        {
            Model.BmrtoCartonId = (navigationData as BMRToCartonLineModel).BmrtoCartonId;
            Model.EstimatedNoOfBMR = (navigationData as BMRToCartonLineModel).EstimatedNoOfBMR;
            Device.BeginInvokeOnMainThread(async () =>
            {
               await LoadData(Model.BmrtoCartonId);
            });
            return base.InitializeAsync(navigationData);
        }
        async void OnDelete()
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                await restClient.DeleteAsync<bool>("BMRToCarton/DeleteBMRToCartonLine", SelectedItem.BmrtoCartonLineId);
            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            BMRToCartonLines.Remove(SelectedItem);
            PageDialog.HideLoading();
        }
        async void OnSelected()
        {
            if (SelectedItem.BmrtoCartonId > 0)
            {
                await NavigationService.NavigateToAsync<BMRToCartonLineViewModel>(SelectedItem);
            }
        }

        private async void OnReloadJob(BMRToCartonLineModel jobModel)
        {
            await LoadData(jobModel.BmrtoCartonId);
        }
        async void OnAdd()
        {
            if (BMRToCartonLines.Count == Model.EstimatedNoOfBMR)
            {
                await DialogService.ShowConfirmAsync("Estimated no of BMR already added!!!", "Validation", "OK", "Cancel");
            }
            else
            {
                await NavigationService.NavigateToAsync<BMRToCartonItemViewModel>(Model);
            }
        }
        private async Task LoadData(long? bmrCartonId)
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                var bMRToCartonLines = await restClient.Get<BMRToCartonLineModel>("BMRToCarton/GetBMRToCartonLines", bmrCartonId.Value);
                BMRToCartonLines = new ObservableCollection<BMRToCartonLineModel>(bMRToCartonLines);
            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            PageDialog.HideLoading();
        }

    }
}