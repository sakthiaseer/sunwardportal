﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using ZXing.Mobile;

namespace APP.SWAM.Mobile.ViewModels
{
    public class ConsumptionEntryViewModel : ViewModelBase
    {

        #region fields

        private ConsumptionModel _model;
        private bool isRefreshing;
        private long consumptionnEntryID;
        #endregion

        #region Properties


        public ConsumptionModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }

        public long ConsumptionEntryID
        {
            get { return consumptionnEntryID; }
            set { consumptionnEntryID = value; OnPropertyChanged(nameof(ConsumptionEntryID)); }
        }


        public Command SaveProductionEntryCommand { get; set; }
        public Command ExitCommand { get; set; }
        public ICommand TextboxFocusedEvent { get; set; }

        #endregion

        public override Task InitializeAsync(object navigationData)
        {
            return base.InitializeAsync(navigationData);
        }
        public ConsumptionEntryViewModel()
        {
            SaveProductionEntryCommand = new Command(OnSave);
            ExitCommand = new Command(OnExit);
            TextboxFocusedEvent = new Command(OnProdScan);
            Model = new ConsumptionModel
            {
                LoginUser = Settings.UserName,
                StartDate = DateTime.Now,
            };
            Title = "Ticket Consumption" + "(" + Settings.DBName + ")";
            MessagingCenter.Subscribe<ConsumptionModel>(this, "InitConsump", drummingModel =>
            {
                Model = new ConsumptionModel
                {
                    LoginUser = Settings.UserName,
                    StartDate = DateTime.Now,
                };
            });

        }
        async void OnProdScan()
        {
            try
            {
#if DEBUG
                string qrCode = "MS18-0932-1-MIX1~MS18-0932~WEIGHTAREA~PRODUCTION";

                var barcode = qrCode.Split('~');
                if (barcode.Length > 3)
                {
                    Model.ProdOrderNo = barcode[0].Trim();
                    Model.ReplanRefNo = barcode[1].Trim();
                    Model.Description = barcode[2].Trim();
                    Model.SublotNo = barcode[3].Trim();
                    Model.TransferFrom = barcode[2].Trim();
                    Model.TransferTo = barcode[3].Trim();
                }
#else
                if (string.IsNullOrEmpty(Model.ProdOrderNo))
                {
                    var scanner = new MobileBarcodeScanner
                    {
                        UseCustomOverlay = false,
                        CameraUnsupportedMessage = "Camera not supporting.",
                    };
                    var result = await scanner.Scan();
                    var barcode = result.Text.Split('~');


                    if (barcode.Length > 3)
                    {
                        Model.ProdOrderNo = barcode[0].Trim();
                        Model.ReplanRefNo = barcode[1].Trim();
                        Model.Description = barcode[2].Trim();
                        Model.SublotNo = barcode[3].Trim();
                        Model.TransferFrom = barcode[2].Trim();
                        Model.TransferTo = barcode[3].Trim();
                    } 
                    else
                    {
                        await DialogService.ShowAlertAsync("In-Valid QR Code.QR Code must contains 4 segment(ex:Prod.No,TransferFrom,TransferTo etc.).Please scan valid QR Code", "QR Code", "OK");
                        return;
                    }


                }
#endif
            }
            catch (Exception ex)
            {
                await DialogService.ShowAlertAsync("In-Valid QR Code.QR Code must contains 4 segment(ex:Prod.No,TransferFrom,TransferTo etc.).Please scan valid QR Code", "QR Code", "OK");
            }
        }
        async void OnSave()
        {
            try
            {
                if (Model.IsValid())
                {
                    IsBusy = true;
                    IsEnabled = false;

                    UserDialogs.Instance.ShowLoading("Validating Ticket Consumption...");
                    Model.AddedByUserId = Settings.UserId;
                    Model.CompanyName = Settings.NAVName;
                    var restclient = new RestClient();
                    Model = await restclient.PostAsync<ConsumptionModel>("ApptransferOrder/InsertAppConsumption", Model);

                    IsBusy = false;
                    IsEnabled = true;
                    UserDialogs.Instance.HideLoading();
                    if (Model != null && Model.ConsumptionEntryID > 0)
                    {
                        await NavigationService.NavigateToAsync<ConsumptionItemViewModel>(Model);
                    }
                    else
                    {
                        await DialogService.ShowAlertAsync("Consumption entry creation failed!.", "Next", "OK");
                    }
                }
                else
                {
                    await DialogService.ShowAlertAsync("Validation Failed. Please scan production order.", "Next", "OK");
                }
            }
            catch (Exception ex)
            {
                IsBusy = false;
                IsEnabled = true;
                UserDialogs.Instance.HideLoading();
                await DialogService.ShowAlertAsync(ex.Message, "ERROR", "OK");
            }
        }
        void OnExit()
        {
            Model = new ConsumptionModel();
            Model.LoginUser = Settings.UserName;
            Model.StartDate = DateTime.Now;
        }
    }
}
