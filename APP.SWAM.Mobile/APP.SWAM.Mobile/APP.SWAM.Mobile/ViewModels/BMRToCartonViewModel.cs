﻿using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using ZXing.Mobile;

namespace APP.SWAM.Mobile.ViewModels
{
    public class BMRToCartonViewModel : ViewModelBase
    {
        #region fields

        private BMRToCartonModel _model;
        private bool isRefreshing;
        private long _bmrToCartonId;
        #endregion

        #region Properties


        public BMRToCartonModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }

        public long BmrToCartonId
        {
            get { return _bmrToCartonId; }
            set { _bmrToCartonId = value; OnPropertyChanged(nameof(BmrToCartonId)); }
        }

        public long? EstimateNumberOfBmr { get; set; }


        public Command SaveCommand { get; set; }
        public Command ClearCommand { get; set; }
        public Command FocusedCommand { get; set; }

        #endregion

        public BMRToCartonViewModel()
        {
            SaveCommand = new Command(OnSave);
            ClearCommand = new Command(OnExit);
            FocusedCommand = new Command(OnBoxScan);
            Model = new BMRToCartonModel
            {
                LoginUser = Settings.UserName,
                StartDate = DateTime.Now,
                LoginUserId = Settings.UserId,
            };
            Title = "BMR To Carton " + "(" + Settings.DBName + ")";
        }

        public override Task InitializeAsync(object navigationData)
        {
            return base.InitializeAsync(navigationData);
        }

        void OnBoxScan()
        {
#if DEBUG
            string qrCode = "STD/20/000000014";
            Model.Displaybox = qrCode;
            Device.BeginInvokeOnMainThread(async () =>
            {
                await GetDisposalMasterBox();
            });

#else
            if (string.IsNullOrEmpty(Model.Displaybox))
            {
                var scanner = new MobileBarcodeScanner();
                scanner.Torch(true);
                scanner.AutoFocus();

                scanner.ScanContinuously(s =>
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        if (Model == null)
                            Model = new BMRToCartonModel();
                        Model.Displaybox = s.Text;
                        await GetDisposalMasterBox();
                    });
                    scanner.Cancel();
                    scanner.Torch(false);
                });
            }
#endif
        }
        async void OnSave()
        {
            if (Model.IsValid())
            {
                await CreateBmrToCarton();
                await NavigationService.NavigateToAsync<BMRToCartonLineViewModel>(new BMRToCartonLineModel { BmrtoCartonId = Model.BmrtoCartonId, EstimatedNoOfBMR = Model.EstimatedNoOfBMR });
            }
            else
            {
                await DialogService.ShowConfirmAsync("Validation Failed. Please scan work order.", "Next", "OK", "Cancel");
            }
        }

        private async Task CreateBmrToCarton()
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                Model = await restClient.PostAsync("BMRToCarton/InsertBMRToCarton", Model);
                Model.EstimatedNoOfBMR = EstimateNumberOfBmr;
            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            PageDialog.HideLoading();
        }

        private async Task GetDisposalMasterBox()
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                var bMRDisposalMasterBox = await restClient.GetItemByNo<BMRDisposalMasterBox>("BmrdisposalMasterBox/GetBmrdisposalMasterByBoxNo", Model.Displaybox);
                EstimateNumberOfBmr = bMRDisposalMasterBox.EstimateNumberOfBmr;
            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            PageDialog.HideLoading();
        }

        void OnExit()
        {
            Model = new BMRToCartonModel
            {
                LoginUser = Settings.UserName,
                StartDate = DateTime.Now,
                LoginUserId = Settings.UserId,
                Displaybox = string.Empty,
                EstimatedNoOfBMR = 0,
            };
        }
    }
}
