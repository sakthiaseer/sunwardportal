﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.Services.ChatService;
using APP.SWAM.Mobile.ViewModel.Base;
using Plugin.AudioRecorder;
using Plugin.LocalNotifications;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class ChatListViewModel : ViewModelBase
    {
        AudioRecorderService recorderService;
        AudioPlayer player;
        private ObservableCollection<ChatMessageModel> messagesList;

        public ObservableCollection<ChatMessageModel> Messages
        {
            get { return messagesList; }
            set { messagesList = value; OnPropertyChanged(); }
        }

        private string outgoingMessage;

        public string OutgoingMessage
        {
            get { return outgoingMessage; }
            set { outgoingMessage = value; OnPropertyChanged(); }
        }

        private string inComingMessage;

        public string InComingMessage
        {
            get { return inComingMessage; }
            set { inComingMessage = value; OnPropertyChanged(); }
        }

        private bool _isSendEnabled;

        public bool IsSendEnabled
        {
            get { return _isSendEnabled; }
            set { _isSendEnabled = value; OnPropertyChanged(); }
        }

        private bool _isRecordEnabled;

        public bool IsRecordEnabled
        {
            get { return _isRecordEnabled; }
            set { _isRecordEnabled = value; OnPropertyChanged(); }
        }

        private bool _isRecordVisible;
        public bool IsRecordVisible
        {
            get { return _isRecordVisible; }
            set { _isRecordVisible = value; OnPropertyChanged(); }
        }

        private bool _isStopVisible;
        public bool IsStopVisible
        {
            get { return _isStopVisible; }
            set { _isStopVisible = value; OnPropertyChanged(); }
        }

        private bool _isPlayVisible;
        public bool IsPlayVisible
        {
            get { return _isPlayVisible; }
            set { _isPlayVisible = value; OnPropertyChanged(); }
        }


        private bool _isCameraEnabled;

        public bool IsCameraEnabled
        {
            get { return _isCameraEnabled; }
            set { _isCameraEnabled = value; OnPropertyChanged(); }
        }

        private bool _isEntryEnabled;

        public bool IsEntryEnabled
        {
            get { return _isEntryEnabled; }
            set { _isEntryEnabled = value; OnPropertyChanged(); }
        }

        private ICommand sendCommand;

        public ICommand SendCommand
        {
            get
            {
                return sendCommand ??
                (sendCommand = new Command(OnSendMessage));
            }
        }

        private ICommand recordCommand;

        public ICommand RecordCommand
        {
            get
            {
                return recordCommand ??
                (recordCommand = new Command(OnAudioRecord));
            }
        }

        private ICommand stopCommand;

        public ICommand StopCommand
        {
            get
            {
                return stopCommand ??
                (stopCommand = new Command(OnStop));
            }
        }

        private ICommand cameraCommand;

        public ICommand CameraCommand
        {
            get
            {
                return cameraCommand ??
                (cameraCommand = new Command(OnTakePhoto));
            }
        }


        private ICommand playCommand;

        public ICommand PlayCommand
        {
            get
            {
                return playCommand ??
                (playCommand = new Command(OnPlay));
            }
        }




        private ImageSource _takenImage;
        public ImageSource TakenImage { get { return _takenImage; } set { _takenImage = value; OnPropertyChanged(); } }

        public ChatListViewModel()
        {

        }

        private User _chatUser = new User();
        public User ChatUser
        {
            get { return _chatUser; }
            set { _chatUser = value; OnPropertyChanged(); }
        }

        public override Task InitializeAsync(object navigationData)
        {
            ChatUser = (User)navigationData;
            Settings.CurrentChatUserId = ChatUser.UserId;
            Title = ChatUser.Name;
            FillData();
            OutgoingMessage = null;
            player = new AudioPlayer();
            player.FinishedPlaying += OnPlayerFinishPlaying;
            recorderService = new AudioRecorderService
            {
                StopRecordingAfterTimeout = true,
                TotalAudioTimeout = TimeSpan.FromSeconds(15),
                AudioSilenceTimeout = TimeSpan.FromSeconds(2)
            };


            IsEntryEnabled = true;
            IsSendEnabled = true;
            IsRecordVisible = true;

            SubscribeMessages();
            return base.InitializeAsync(navigationData);
        }

        async void FillData()
        {
            UserDialogs.Instance.ShowLoading();
            var restclient = new RestClient();
            var chatMessages = await restclient.GetByIds<ChatMessageModel>("ChatMessage/GetMessageByUser", ChatUser.UserId, Settings.UserId);
            Messages = new ObservableCollection<ChatMessageModel>(chatMessages);
            UserDialogs.Instance.HideLoading();
        }

        private void OnPlayerFinishPlaying(object sender, EventArgs e)
        {
            IsRecordEnabled = true;
            IsSendEnabled = true;
            IsRecordVisible = true;
            IsPlayVisible = false;
        }

        async void OnSendMessage()
        {
            try
            {
                if (IsConnected)
                {
                    IsBusy = true;
                    var chatMessage = new ChatMessageModel { ReceiveUserId = ChatUser.UserId, SentUserId = Settings.UserId, SenderName = Settings.UserName, CreatedDateTime = DateTime.Now, Message = OutgoingMessage, IsIncoming = false, MessageType = 1, ConnectionId = ChatUser.ConnectionId };
                    await ChatService.Send(new ChatMessageModel { ReceiveUserId = ChatUser.UserId, SentUserId = Settings.UserId, SenderName = Settings.UserName, CreatedDateTime = DateTime.Now, Message = OutgoingMessage, IsIncoming = false, MessageType = 1, ConnectionId = ChatUser.ConnectionId });
                    //if (String.IsNullOrEmpty(ChatUser.ConnectionId))
                    //{
                    Messages.Add(chatMessage);
                    //}
                    OutgoingMessage = "";
                    IsBusy = false;
                }
                else
                {
                    ShowConnectivityError();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        async void OnAudioRecord()
        {
            try
            {
                IsBusy = true;
                if (!recorderService.IsRecording) //Record button clicked
                {
                    IsRecordEnabled = false;
                    IsCameraEnabled = false;
                    IsSendEnabled = false;

                    //start recording audio
                    var audioRecordTask = await recorderService.StartRecording();

                    IsStopVisible = true;
                    IsRecordVisible = false;

                    await audioRecordTask;
                }
                IsBusy = false;
            }
            catch (Exception ex)
            {
                //blow up the app!
                throw ex;
            }

        }

        async void OnStop()
        {
            try
            {
                IsStopVisible = false;
                IsRecordEnabled = false;
                IsCameraEnabled = false;
                IsSendEnabled = true;
                IsRecordVisible = true;
                //IsPlayVisible = true;

                //stop the recording...
                await recorderService.StopRecording();

            }
            catch (Exception ex)
            {
                //blow up the app!
                throw ex;
            }
        }

        void OnPlay()
        {
            try
            {
                var filePath = recorderService.GetAudioFilePath();

                if (filePath != null)
                {
                    player.Play(filePath);
                }
                else
                {
                    IsRecordEnabled = true;
                    IsSendEnabled = true;
                    IsRecordVisible = true;
                    IsPlayVisible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        async void OnTakePhoto()
        {
            try
            {
                IsBusy = true;
                if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                {
                    await DialogService.ShowConfirmAsync("No Camera", ":( No camera available.", "OK", "Cancel");
                    return;
                }

                var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
                {
                    Directory = "Test",
                    SaveToAlbum = true,
                    CompressionQuality = 30,
                    //CustomPhotoSize = 50,
                    PhotoSize = PhotoSize.Medium,
                    //MaxWidthHeight = 2000,
                    DefaultCamera = CameraDevice.Rear
                });

                if (file == null)
                    return;
                TakenImage = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    file.Dispose();
                    return stream;
                });
                IsBusy = false;
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void SubscribeMessages()
        {
            MessagingCenter.Subscribe<ChatService, ChatMessageModel>(this, "AddMessages", (obj, item) =>
            {
                if (InComingMessage != item.Message)
                {
                    CrossLocalNotifications.Current.Show(item.SenderName, item.Message, (int)item.SentUserId, DateTime.Now.AddSeconds(5));
                    InComingMessage = item.Message;
                    Messages.Add(item);

                }
            });
        }
    }
}

