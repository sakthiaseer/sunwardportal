﻿using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using ZXing.Mobile;

namespace APP.SWAM.Mobile.ViewModels
{
    public class QcApprovalViewmodel : ViewModelBase
    {
        #region fields

        public ICommand ScanCommand { get; set; }
        private ObservableCollection<QcApprovalModel> qcApprovalModels = new ObservableCollection<QcApprovalModel>();
        private QcApprovalModel _model = new QcApprovalModel();

        public QcApprovalModel Model
        {
            get { return _model ?? (_model = new QcApprovalModel()); }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }


        private long? _qcapprovalId;

        public long? QcApprovalId
        {
            get { return _qcapprovalId; }
            set
            {
                _qcapprovalId = value; OnPropertyChanged(nameof(QcApprovalId));
            }
        }
        private List<string> _masterQCs = new List<string>();
        public List<string> MasterQCs
        {
            get { return _masterQCs; }
            set { _masterQCs = value; OnPropertyChanged(nameof(MasterQCs)); }

        }

        private List<string> _approvalItems = new List<string>();
        public List<string> ApprovalItems
        {
            get { return _approvalItems; }
            set { _approvalItems = value; OnPropertyChanged(nameof(ApprovalItems)); }

        }

        List<ApplicationMasterDetailModel> masterQcItems = new List<ApplicationMasterDetailModel>();
        List<CodeMasterModel> codeMasters = new List<CodeMasterModel>();

        #endregion



        public Command SaveCommand { get; set; }
        public Command ExitCommand { get; set; }
        public ICommand FocusedCommand { get; set; }
        public ICommand JobSelectedCommand { get; set; }
        public ICommand SelectedItemLine { get; set; }

        public ObservableCollection<QcApprovalModel> QcApprovalModels
        {
            get { return qcApprovalModels; }
            set { qcApprovalModels = value; OnPropertyChanged(nameof(QcApprovalModels)); }
        }

        private QcApprovalLineModel _selectedItem = new QcApprovalLineModel();
        public QcApprovalLineModel SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value; OnPropertyChanged(nameof(SelectedItem));
            }
        }

        private bool _isPostEnabled = false;

        public bool IsPostEnabled
        {
            get
            {
                return _isPostEnabled;
            }

            set
            {
                _isPostEnabled = value;
                OnPropertyChanged();
            }
        }

        void OnProductionOrderScan()
        {
#if DEBUG
            CreateTestJob();
#else
            if (string.IsNullOrEmpty(Model.ProductionOrderNo))
            {
                var scanner = new MobileBarcodeScanner();
                scanner.Torch(true);
                scanner.AutoFocus();
                string[] barcode = new string[0];
                scanner.ScanContinuously(async s =>
               {
                   Device.BeginInvokeOnMainThread(() =>
                   {
                       if (Model == null)
                           Model = new QcApprovalModel();

                       barcode = s.Text.Split('~');
                       if (barcode.Length ==6)
                       {
                           Model.ProductionOrderNo = barcode[0].Trim();
                           var produtionOrderNo = Model.ProductionOrderNo.Split('-');
                           if (produtionOrderNo.Length > 3)
                           {
                               Model.InspectionNo = produtionOrderNo[0].Trim() + "-" + produtionOrderNo[1].Trim() + "-" + produtionOrderNo[3].Trim().Substring(0, produtionOrderNo[3].Trim().Length - 1);
                               Model.LoginUser = Settings.UserName;
                               Model.CompanyName = Settings.NAVName;
                               Model.StartDate = DateTime.Now;
                               LoadQcLines();
                           }
                       }
                   });
                   scanner.Cancel();
                   scanner.Torch(false);

                   if (barcode.Length < 5)
                   {
                       await DialogService.ShowConfirmAsync("In-Valid QR Code.QR Code must contains 4 segment(ex:Prod.No,LineNo,Item,BatchNo).Please scan valid QR Code", "QR Code", "OK", "Cancel");
                       return;
                   }

               });
            }

#endif



        }

        public QcApprovalViewmodel()
        {
            ScanCommand = new Command(OnScanNewItem);
            FocusedCommand = new Command(OnProductionOrderScan);
            SaveCommand = new Command(OnSave);
            ExitCommand = new Command(OnExit);
            SelectedItemLine = new Command(OnSelected);
            Model = new QcApprovalModel
            {
                LoginUser = Settings.UserName,
                StartDate = DateTime.Now,
                AddedByUserId = Settings.UserId,
                SessionId = Guid.NewGuid()
            };
            LoadChoices();
        }

        async void OnScanNewItem()
        {
            await NavigationService.NavigateToAsync<ScannedImagesViewModel>(this);
        }

        public override Task InitializeAsync(object navigationData)
        {
            return base.InitializeAsync(navigationData);
        }

        private async void LoadChoices()
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                masterQcItems = await restClient.GetApplicationMasterByType<ApplicationMasterDetailModel>("ApplicationMaster/GetApplicationMasterDetailByType", 277);
                MasterQCs = masterQcItems.Select(q => q.Value).ToList();
                codeMasters = await restClient.GetCodeMasterByType<CodeMasterModel>("ApplicationUser/GetCodeMasterByType", "QCApproveAtatus");
                ApprovalItems = codeMasters.Select(q => q.CodeValue).ToList();
            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            PageDialog.HideLoading();
        }

        private async void LoadQcLines()
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                Model.IsSkipValidation = false;
                Model.AddedByUserId = Settings.UserId;
                Model.TestNameId = masterQcItems.FirstOrDefault(m => m.Value == Model.TestName) != null ? masterQcItems.FirstOrDefault(m => m.Value == Model.TestName).ApplicationMasterDetailId as long? : null;
                Model.StatusCodeID = codeMasters.FirstOrDefault(m => m.CodeValue == Model.StatusCode) != null ? codeMasters.FirstOrDefault(m => m.CodeValue == Model.StatusCode).CodeID as int? : null;
                if (Model.IsValid())
                {
                    Model = await restClient.PostAsync("QCApproval/GetApprovalLineByOrderNo", Model);
                    Model.LoginUser = Settings.UserName;
                    Model.StartDate = DateTime.Now;
                    if (Model.IsError)
                    {
                        await DialogService.ShowConfirmAsync(Model.Errormessage, "Validation", "OK", "Cancel");
                        OnExit();
                    }
                    IsPostEnabled = Model.QcApprovalLines.Count > 0;
                }
                else
                {
                    OnExit();
                }
            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            PageDialog.HideLoading();
        }

        private async void OnSave()
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();
                Model.IsSkipValidation = true;
                Model.TestNameId = masterQcItems.FirstOrDefault(m => m.Value == Model.TestName) != null ? masterQcItems.FirstOrDefault(m => m.Value == Model.TestName).ApplicationMasterDetailId as long? : null;
                Model.StatusCodeID = codeMasters.FirstOrDefault(m => m.CodeValue == Model.StatusCode) != null ? codeMasters.FirstOrDefault(m => m.CodeValue == Model.StatusCode).CodeID as int? : null;

                if (Model.IsValid())
                {
                    Model = await restClient.PostAsync("QCApproval/PostQCNAV", Model);
                    if (Model != null && !Model.IsError)
                    {
                        await DialogService.ShowConfirmAsync("Record Posted successfully.", "SAVE", "OK", "Cancel");
                        //OnExit();
                    }
                    else
                    {
                        await DialogService.ShowConfirmAsync(Model.Errormessage, "SAVE", "OK", "Cancel");
                    }
                }
            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            PageDialog.HideLoading();
            OnExit();
        }

        async void OnSelected()
        {
            if (SelectedItem.QcapprovalLineId > 0)
            {
                await NavigationService.NavigateToAsync<QcApprovalLineItemViewModel>(SelectedItem);
            }
        }

        private void CreateTestJob()
        {
            string qrCode = "MT20-0029-3-BLD2~10000~Diatrol Capsule Granules~500-20001~1~MT20-0033";

            var barcode = qrCode.Split('~');
            if (barcode.Length == 6)
            {
                Model.ProductionOrderNo = barcode[0].Trim();
                var produtionOrderNo = Model.ProductionOrderNo.Split('-');
                if (produtionOrderNo.Length > 3)
                {
                    Model.InspectionNo = produtionOrderNo[0].Trim() + "-" + produtionOrderNo[1].Trim() + "-" + produtionOrderNo[3].Trim().Substring(0, produtionOrderNo[3].Trim().Length - 1);
                    LoadQcLines();
                    

                }
            }
        }

        private void OnExit()
        {
            Model.LoginUser = Settings.UserName;
            Model.StartDate = DateTime.Now;
            Model.InspectionNo = string.Empty;
            Model.ProductionOrderNo = string.Empty;
            Model.TestName = string.Empty;
            Model.TestNameId = null;
            Model.StatusCode = string.Empty;
            Model.StatusCodeID = null;
            Model.IsRetest = false;
            IsPostEnabled = false;
            Model.QcApprovalLines = new List<QcApprovalLineModel>();
        }

    }
}