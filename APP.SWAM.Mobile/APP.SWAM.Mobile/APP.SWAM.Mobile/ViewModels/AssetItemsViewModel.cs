﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.Utils;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class AssetItemsViewModel : ViewModelBase
    {

        #region fields
        private ObservableCollection<PlantMaintenanceEntryLineModel> plantMaintenanceEntries = new ObservableCollection<PlantMaintenanceEntryLineModel>();
        private PlantMaintenanceEntryLineModel selectedPlantMaintenanceEntry = new PlantMaintenanceEntryLineModel();
        private bool isRefreshing;
        private long plantEntryID;
        #endregion

        #region Properties
        public ObservableCollection<PlantMaintenanceEntryLineModel> PlantManintenanceEntries
        {
            get { return plantMaintenanceEntries; }
            set { plantMaintenanceEntries = value; OnPropertyChanged(nameof(PlantManintenanceEntries)); }
        }

        public PlantMaintenanceEntryLineModel SelectedPlantMaintenanceEntry
        {
            get { return selectedPlantMaintenanceEntry; }
            set
            {
                selectedPlantMaintenanceEntry = value;
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }

        public long PlantEntryID
        {
            get { return plantEntryID; }
            set { plantEntryID = value; OnPropertyChanged(nameof(PlantEntryID)); }
        }

        public ICommand RefreshCommand { get; set; }

        public ICommand OpenPlantMaintenanceEntryCommand { get; set; }

        public ICommand AddNewPlantMaintenanceEntryCommand { get; set; }


        #endregion

        public override Task InitializeAsync(object navigationData)
        {
            PlantEntryID = (navigationData as PlantMaintenanceEntryModel).PlantEntryId;
            FillData();
            return base.InitializeAsync(navigationData);
        }

        public async void FillData()
        {
            UserDialogs.Instance.ShowLoading("Loading...");
            var restclient = new RestClient();
            var entries = await restclient.Get<PlantMaintenanceEntryLineModel>("PlantMaintenanceEntryLine/GetPlantMaintenanceEntryLineByID", PlantEntryID);
            entries.ForEach(e =>
            {
                //e.FrontPhotoSource = ImageSource.FromStream(() => new MemoryStream(e.FrontPhoto));
                //e.BackPhotoSource = ImageSource.FromStream(() => new MemoryStream(e.BackPhoto));

                if (Settings.DBName == "LIVE")
                {
                    e.FrontPhotoSource = ImageSource.FromUri(new Uri(Constant.LIVEURL + e.FrontPhotoName));
                    e.BackPhotoSource = ImageSource.FromUri(new Uri(Constant.LIVEURL + e.BackPhotoName));
                }
                else
                {
                    e.FrontPhotoSource = ImageSource.FromUri(new Uri(Constant.DEVURL + e.FrontPhotoName));
                    e.BackPhotoSource = ImageSource.FromUri(new Uri(Constant.DEVURL + e.BackPhotoName));
                }
                

            });
            PlantManintenanceEntries = new ObservableCollection<PlantMaintenanceEntryLineModel>(entries);
            UserDialogs.Instance.HideLoading();
        }

        public AssetItemsViewModel()
        {
            RefreshCommand = new Command(CmdRefresh);
            OpenPlantMaintenanceEntryCommand = new Command(OnOpenPlantMaintenanceEntryItem);
            AddNewPlantMaintenanceEntryCommand = new Command(OnAddNewPlantMaintenanceEntryItem);
            Subscribe();
        }

        private async void CmdRefresh()
        {
            IsRefreshing = true;
            // wait 3 secs for demo
            await Task.Delay(3000);
            IsRefreshing = false;
        }

        private async void OnOpenPlantMaintenanceEntryItem()
        {
            await NavigationService.NavigateToAsync<AssetItemViewModel>(this);
        }

        private async void OnAddNewPlantMaintenanceEntryItem()
        {
            SelectedPlantMaintenanceEntry = new PlantMaintenanceEntryLineModel();
            await NavigationService.NavigateToAsync<AssetItemViewModel>(this);
        }

        private void Subscribe()
        {
            //MessagingCenter.Subscribe<PlantMaintenanceEntryLineModel>(this, "AssetItemData", (value) =>
            //{
            //    PlantMaintenanceEntryLineModel assetItemData = value;
            //    PlantManintenanceEntries.Add(assetItemData);
            //    OnPropertyChanged(nameof(PlantManintenanceEntries));
            //});
        }
    }
}
