﻿using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace APP.SWAM.Mobile.ViewModels
{
    public class ApplicationUserListViewModel : ViewModelBase
    {
        public ApplicationUserListViewModel()
        {
            ExitCommand = new Command(OnExit);
        }

        public string ScreenName { get; set; }

        public ICommand ExitCommand { get; set; }

        private ApplicationUserModel _selectedUser = new ApplicationUserModel();
        public ApplicationUserModel SelectedUser { get { return _selectedUser; } set { _selectedUser = value; OnPropertyChanged(); } }

        public override Task InitializeAsync(object navigationData)
        {
            this.ScreenName = (navigationData as ApplicationUserListViewModel).ScreenName;
            return base.InitializeAsync(navigationData);
        }

        private async void OnExit()
        {
            if (SelectedUser != null)
            {
                if (ScreenName == "ApplicationUser")
                {
                    MessagingCenter.Send(SelectedUser, "AddSelectedUser");
                }
                if (ScreenName == "ApplicationUser1")
                {
                    MessagingCenter.Send(SelectedUser, "AddSelectedUser1");
                }
            }
            await NavigationService.NavigateBackAsync();
        }
    }
}
