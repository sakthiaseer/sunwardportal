﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using ZXing.Mobile;

namespace APP.SWAM.Mobile.ViewModels
{
    public class MaterialReturnViewModel : ViewModelBase
    {
        #region fields
       
        private MaterialReturnModel _model;
        private bool isRefreshing;
        private long productionEntryID;
        #endregion

        #region Properties
         

        public MaterialReturnModel Model
        {
            get { return _model; }
            set
            {
                _model = value; OnPropertyChanged(nameof(Model));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }

        public long ProductionEntryID
        {
            get { return productionEntryID; }
            set { productionEntryID = value; OnPropertyChanged(nameof(ProductionEntryID)); }
        }
        private ImageSource _frontPhotoSource;
        public ImageSource FrontPhotoSource { get { return _frontPhotoSource; } set { _frontPhotoSource = value; OnPropertyChanged(nameof(FrontPhotoSource)); } }




        public Command SaveCommand { get; set; }
        public Command ExitCommand { get; set; }

        public Command TakeFrontPhotoCommand { get; set; }
        public ICommand TextboxFocusedEvent { get; set; }

        #endregion

        public override Task InitializeAsync(object navigationData)
        {
            return base.InitializeAsync(navigationData);
        }

        public MaterialReturnViewModel()
        {
            SaveCommand = new Command(OnSave);
            ExitCommand = new Command(OnExit);
            TakeFrontPhotoCommand = new Command(OnTakeFrontPhoto);

            TextboxFocusedEvent = new Command(OnProdScan);

            Model = new MaterialReturnModel
            {
                LoginUser = Settings.UserName,
                StartDate = DateTime.Now,

            };
            Title = "Material Return" + "(" + Settings.DBName + ")";
            if (Settings.DBName == "LIVE")
            {
                FrontPhotoSource = ImageSource.FromUri(new Uri(Constant.LIVEURL + "/Noimages.png"));
            }
            else
            {
                FrontPhotoSource = ImageSource.FromUri(new Uri(Constant.DEVURL + "/Noimages.png"));
            }

        }

        async void OnSave()
        {
            try
            {
                if (Model.IsValid())
                {
                    IsBusy = true;
                    IsEnabled = false;
                    UserDialogs.Instance.ShowLoading("Saving...");
                    Model.AddedByUserId = Settings.UserId;
                    Model.PostedUserID = Constant.AppUserId;
                    var restclient = new RestClient();
                    var result = await restclient.PostAsync<MaterialReturnModel>("AppmaterialReturn/InsertAppmaterialReturn", Model);
                    if (result != null && !result.IsError)
                    {
                        await DialogService.ShowConfirmAsync("Record saved successfully.", "SAVE", "OK", "Cancel");
                        OnExit();
                    }
                    else
                    {
                        await DialogService.ShowConfirmAsync("Record insertion failed." + result.Errormessage, "Error", "OK", "Cancel");
                    }
                    IsBusy = false;
                    IsEnabled = true;
                    UserDialogs.Instance.HideLoading();
                }
                else
                {
                    await DialogService.ShowConfirmAsync("Please key-in all the required fields", "Validation", "OK", "Cancel");
                }
            }
            catch (Exception ex)
            {
                await DialogService.ShowConfirmAsync(ex.Message, "ERROR", "OK", "Cancel");
                IsBusy = false;
                IsEnabled = true;
                UserDialogs.Instance.HideLoading();
            }
        }
        async void OnTakeFrontPhoto()
        {
            Image image = new Image();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DialogService.ShowConfirmAsync("No Camera", ":( No camera available.", "OK", "Cancel");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 30,
                //CustomPhotoSize = 50,
                PhotoSize = PhotoSize.Medium,
                //MaxWidthHeight = 2000,
                DefaultCamera = CameraDevice.Rear
            });

            if (file == null)
                return;
            Model.FrontPhoto = ReadFully(file.GetStream());
            Model.Photo = Guid.NewGuid().ToString() + ".png";
            FrontPhotoSource = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                file.Dispose();
                return stream;
            });
        }
        public byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        void OnProdScan()
        {
#if DEBUG
            string qrCode = "DRUM001~MT18-1945-4-GR1~10000~Fenadium F Tablet Granules~704-19005~MT18-1945~KG";

            var barcode = qrCode.Split('~');
            if (barcode.Length > 4)
            {
                Model.DrumNo = barcode[0].Trim();
                Model.ProdOrderNo = barcode[1].Trim();
                Model.ProdLineNo = barcode[2].Trim();
                Model.ItemNo = barcode[2].Trim();
                Model.BatchNo = barcode[3].Trim();
                Model.LotNo = barcode[4].Trim();
                Model.UOM = barcode[5].Trim();
               
            }
#else
            if (string.IsNullOrEmpty(Model.DrumNo))
            {
                var scanner = new MobileBarcodeScanner();
                scanner.Torch(true);
                scanner.AutoFocus();

                scanner.ScanContinuously(s =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (Model == null)
                            Model = new MaterialReturnModel();
                        Model.DrumNo = s.Text;
                    });
                    scanner.Cancel();
                    scanner.Torch(false);
                });
            }
#endif
        }
        void OnExit()
        {
            Model = new MaterialReturnModel
            {
                LoginUser = Settings.UserName,
                StartDate = DateTime.Now,

            };
            if (Settings.DBName == "LIVE")
            {
                FrontPhotoSource = ImageSource.FromUri(new Uri(Constant.LIVEURL + "/Noimages.png"));
            }
            else
            {
                FrontPhotoSource = ImageSource.FromUri(new Uri(Constant.LIVEURL + "/Noimages.png"));
            }
           
        }
    }
}
