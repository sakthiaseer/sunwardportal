﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Helpers;
using APP.SWAM.Mobile.Models;
using APP.SWAM.Mobile.Services;
using APP.SWAM.Mobile.ViewModel.Base;

using Plugin.Media;
using Plugin.Media.Abstractions;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.MultiSelectListView;
using ZXing.Mobile;

namespace APP.SWAM.Mobile.ViewModels
{
    public class ProductionEntryViewModel : ViewModelBase
    {
        #region fields
        private ObservableCollection<ProductionEntryModel> productionEntries = new ObservableCollection<ProductionEntryModel>();
        private ProductionEntryModel selectedProductionEntry;
        private bool isRefreshing;
        private long productionEntryID;
        #endregion

        #region Properties

        public ICommand ScanCommand { get; set; }
        public ObservableCollection<ProductionEntryModel> ProductionEntrys
        {
            get { return productionEntries; }
            set { productionEntries = value; OnPropertyChanged(nameof(ProductionEntrys)); }
        }

        public ProductionEntryModel SelectedProductionEntry
        {
            get { return selectedProductionEntry; }
            set
            {
                selectedProductionEntry = value; OnPropertyChanged(nameof(selectedProductionEntry));
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set { isRefreshing = value; OnPropertyChanged(nameof(IsRefreshing)); }
        }

        public long ProductionEntryID
        {
            get { return productionEntryID; }
            set { productionEntryID = value; OnPropertyChanged(nameof(ProductionEntryID)); }
        }
        private ImageSource _frontPhotoSource;
        public ImageSource FrontPhotoSource { get { return _frontPhotoSource; } set { _frontPhotoSource = value; OnPropertyChanged(nameof(FrontPhotoSource)); } }

        private ImageSource _backPhotoSource;
        public ImageSource BackPhotoSource { get { return _backPhotoSource; } set { _backPhotoSource = value; OnPropertyChanged(nameof(BackPhotoSource)); } }


        private List<string> _prodTasks = new List<string>();
        public List<string> ProductionTasks
        {
            get { return _prodTasks; }
            set { _prodTasks = value; OnPropertyChanged(nameof(ProductionTasks)); }

        }
        List<OperationProcedureItem> prodTaskItems = new List<OperationProcedureItem>();
        public Command SaveProductionEntryCommand { get; set; }
        public Command ExitCommand { get; set; }

        public Command TakeFrontPhotoCommand { get; set; }
        public Command TakeBackPhotoCommand { get; set; }
        public ICommand TextboxFocusedEvent { get; set; }
        public ICommand LocationScanCommand { get; set; }
        public Command ProdTaskCommand { get; set; }
        #endregion

        public override Task InitializeAsync(object navigationData)
        {
            return base.InitializeAsync(navigationData);
        }



        public ProductionEntryViewModel()
        {
            ScanCommand = new Command(OnScanNewItem);
            ProdTaskCommand = new Command(OnProdTask);
            SaveProductionEntryCommand = new Command(Save);
            ExitCommand = new Command(OnExit);
            TakeFrontPhotoCommand = new Command(OnTakeFrontPhoto);
            TakeBackPhotoCommand = new Command(OnTakeBackPhoto);
            TextboxFocusedEvent = new Command(OnProdScan);
            LocationScanCommand = new Command(OnLocationScan);
            SelectedProductionEntry = new ProductionEntryModel
            {
                LoginUser = Settings.UserName,
                StartDate = DateTime.Now,
                ActionName = "Pre-line clearance Ready",
                SessionId = Guid.NewGuid()
            };
            Title = "Production Entry" + "(" + Settings.DBName + ")";
            selectedProductionEntry.ActionNames = new List<string> { "Pre-line clearance Ready", "Setting up of Machine" };
            if (Settings.DBName == "LIVE")
            {
                FrontPhotoSource = ImageSource.FromUri(new Uri(Constant.LIVEURL + "/Noimages.png"));
                BackPhotoSource = ImageSource.FromUri(new Uri(Constant.LIVEURL + "/Noimages.png"));
            }
            else
            {
                FrontPhotoSource = ImageSource.FromUri(new Uri(Constant.DEVURL + "/Noimages.png"));
                BackPhotoSource = ImageSource.FromUri(new Uri(Constant.DEVURL + "/Noimages.png"));
            }
            
            MessagingCenter.Subscribe<MultiSelectionModel>(this, "AddProdTaskSelectionItems", mSelection =>
            {
                foreach (var item in mSelection.MultiselectionItems)
                {
                    if (item.IsSelected)
                    {
                        SelectedProductionEntry.ProdTaskIds.Add(item.Data.Id);
                        SelectedProductionEntry.ProdTask += item.Data.Title + ",";
                    }
                }
            });
            LoadChoices();
        }

        private async void LoadChoices()
        {
            PageDialog.ShowLoading();
            try
            {
                RestClient restClient = new RestClient();

                prodTaskItems = await restClient.Get<OperationProcedureItem>("OperationProcedure/GetOperationProcedureItems");
                ProductionTasks = prodTaskItems.Select(q => q.Name).ToList();

            }
            catch (Exception ex)
            {
                await PageDialog.AlertAsync("Error", "Unrecognised Error", "OK");
                PageDialog.HideLoading();
                Debug.WriteLine("ExceptionDetails", ex);
            }
            PageDialog.HideLoading();
        }
        void OnLocationScan()
        {
#if DEBUG
            selectedProductionEntry.LocationName = "SWMY";
#else
            if (string.IsNullOrEmpty(selectedProductionEntry.LocationName))
            {
                var scanner = new MobileBarcodeScanner();
                scanner.Torch(true);
                scanner.AutoFocus();

                scanner.ScanContinuously(s =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (selectedProductionEntry == null)
                            selectedProductionEntry = new ProductionEntryModel();
                        selectedProductionEntry.LocationName = s.Text;
                    });
                    scanner.Cancel();
                    scanner.Torch(false);
                });
            }
#endif
        }

        async void OnScanNewItem()
        {
            await NavigationService.NavigateToAsync<ScannedImagesViewModel>(this);
        }
        private async void OnProdTask()
        {
            MultiSelectObservableCollection<MultiSelectionModel> items = new MultiSelectObservableCollection<MultiSelectionModel>();
            prodTaskItems.ForEach(p =>
            {
                items.Add(new MultiSelectionModel
                {
                    Id = p.Id,
                    Title = p.Name,
                });
            });
            MultiSelectionModel multiSelectionModel = new MultiSelectionModel
            {
                MultiSelectionName = "ProdTask",
                MultiselectionItems = items
            };
            await NavigationService.NavigateToAsync<MultiselectionViewModel>(multiSelectionModel);
        }

        async void OnProdScan()
        {
#if DEBUG

            string qrCode = "MT18-1945-4-GR1~10000~Fenadium F Tablet Granules~704-19005~1~MT18-1945";

            var barcode = qrCode.Split('~');
            if (barcode.Length > 4)
            {
                selectedProductionEntry.ProductionOrderNo = barcode[0].Trim();
                selectedProductionEntry.ProductionLineNo = barcode[1].Trim();
                selectedProductionEntry.ItemName = barcode[2].Trim();
                selectedProductionEntry.BatchNumber = barcode[3].Trim();
                selectedProductionEntry.ProcessNo = barcode[4].Trim();
                selectedProductionEntry.RePlanRefNo = barcode[5].Trim();
            }

#else
            if (string.IsNullOrEmpty(selectedProductionEntry.ProductionOrderNo))
            {
                var scanner = new MobileBarcodeScanner();
                scanner.Torch(true);
                scanner.AutoFocus();
                string[] barcode = new string[0];
                scanner.ScanContinuously(async s =>
               {
                   Device.BeginInvokeOnMainThread(() =>
                   {
                       if (selectedProductionEntry == null)
                           selectedProductionEntry = new ProductionEntryModel();

                       barcode = s.Text.Split('~');
                       if (barcode.Length > 4)
                       {
                           selectedProductionEntry.ProductionOrderNo = barcode[0].Trim();
                           selectedProductionEntry.ProductionLineNo = barcode[1].Trim();
                           selectedProductionEntry.ItemName = barcode[2].Trim();
                           selectedProductionEntry.BatchNumber = barcode[3].Trim();
                           selectedProductionEntry.ProcessNo = barcode[4].Trim();
                           selectedProductionEntry.RePlanRefNo = barcode[5].Trim();
                       }
                   });
                   scanner.Cancel();
                   scanner.Torch(false);

                   if (barcode.Length < 5)
                   {
                       await DialogService.ShowConfirmAsync("In-Valid QR Code.QR Code must contains 4 segment(ex:Prod.No,LineNo,Item,BatchNo).Please scan valid QR Code", "QR Code", "OK", "Cancel");
                       return;
                   }

               });
            }
#endif
        }
        async void OnTakeFrontPhoto()
        {
            Image image = new Image();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DialogService.ShowConfirmAsync("No Camera", ":( No camera available.", "OK", "Cancel");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 30,
                //CustomPhotoSize = 50,
                PhotoSize = PhotoSize.Medium,
                //MaxWidthHeight = 2000,
                DefaultCamera = CameraDevice.Rear
            });

            if (file == null)
                return;
            SelectedProductionEntry.FrontPhoto = ReadFully(file.GetStream());
            SelectedProductionEntry.RoomStatus = Guid.NewGuid().ToString() + ".png";
            FrontPhotoSource = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                file.Dispose();
                return stream;
            });
        }

        async void OnTakeBackPhoto()
        {
            Image image = new Image();
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DialogService.ShowConfirmAsync("No Camera", ":( No camera available.", "OK", "Cancel");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 30,
                //CustomPhotoSize = 50,
                PhotoSize = PhotoSize.Medium,
                //MaxWidthHeight = 2000,
                DefaultCamera = CameraDevice.Rear
            });

            if (file == null)
                return;


            SelectedProductionEntry.BackPhoto = ReadFully(file.GetStream());
            SelectedProductionEntry.RoomStatus1 = Guid.NewGuid().ToString() + ".png";
            BackPhotoSource = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                file.Dispose();
                return stream;
            });
        }

        public byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        async void Save()
        {
            try
            {
                if (selectedProductionEntry.IsValid())
                {
                    IsBusy = true;
                    IsEnabled = false;
                    UserDialogs.Instance.ShowLoading("Saving...");
                    selectedProductionEntry.CompanyName = Settings.NAVName;
                    selectedProductionEntry.AddedByUserId = Settings.UserId;
                    selectedProductionEntry.PostedUserID = Settings.UserId;
                    selectedProductionEntry.ProductionActionID = selectedProductionEntry.ActionName == "Setting up of Machine" ? 4 : 3;

                    var restclient = new RestClient();
                    var result = await restclient.PostAsync<ProductionEntryModel>("ProductionEntry/InsertProductionEntry", selectedProductionEntry);
                    if (result != null && !result.IsError)
                    {

                        await DialogService.ShowConfirmAsync("Record saved successfully.", "SAVE", "OK", "Cancel");
                        OnExit();
                    }
                    else
                    {
                        await DialogService.ShowConfirmAsync("Record insertion failed." + result.Errormessage, "Error", "OK", "Cancel");
                    }
                    IsBusy = false;
                    IsEnabled = true;
                    UserDialogs.Instance.HideLoading();
                }
                else
                {
                    await DialogService.ShowConfirmAsync("Please key-in all the required fields", "Validation", "OK", "Cancel");
                }
            }
            catch (Exception ex)
            {
                await DialogService.ShowConfirmAsync(ex.Message, "ERROR", "OK", "Cancel");
            }
        }

        void OnExit()
        {

            SelectedProductionEntry.LoginUser = Settings.UserName;
            SelectedProductionEntry.StartDate = DateTime.Now;
            SelectedProductionEntry.ActionName = "Pre-line clearance Ready";
            SelectedProductionEntry.BackPhoto = null;
            SelectedProductionEntry.FrontPhoto = null;
            SelectedProductionEntry.ItemName = string.Empty;
            SelectedProductionEntry.LocationName = string.Empty;
            SelectedProductionEntry.NumberOfWorker = string.Empty; ;
            SelectedProductionEntry.ProductionActionID = 3;
            SelectedProductionEntry.ProductionLineNo = string.Empty;
            SelectedProductionEntry.ProductionOrderNo = string.Empty;
            SelectedProductionEntry.RoomStatus = string.Empty;
            SelectedProductionEntry.RoomStatus1 = string.Empty;
            SelectedProductionEntry.Title = string.Empty;
            SelectedProductionEntry.ProductionEntryID = -1;
            SelectedProductionEntry.BatchNumber = string.Empty;
            if (Settings.DBName == "LIVE")
            {
                FrontPhotoSource = ImageSource.FromUri(new Uri(Constant.LIVEURL + "/Noimages.png"));
                BackPhotoSource = ImageSource.FromUri(new Uri(Constant.LIVEURL + "/Noimages.png"));
            }
            else
            {
                FrontPhotoSource = ImageSource.FromUri(new Uri(Constant.DEVURL + "/Noimages.png"));
                BackPhotoSource = ImageSource.FromUri(new Uri(Constant.DEVURL + "/Noimages.png"));
            }
           
        }

    }
}
