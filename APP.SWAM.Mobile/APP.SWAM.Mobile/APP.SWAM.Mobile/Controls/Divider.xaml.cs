﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace APP.SWAM.Mobile.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Divider : ContentView
    {
        public Divider()
        {
            InitializeComponent();
        }
    }
}