﻿using Xamarin.Forms;

namespace APP.SWAM.Mobile.Effects
{
    public class ListViewScrollEffect : RoutingEffect
    {
        public ListViewScrollEffect() : base("Restaurant.ListViewScrollEffect")
        {
        }
    }
}