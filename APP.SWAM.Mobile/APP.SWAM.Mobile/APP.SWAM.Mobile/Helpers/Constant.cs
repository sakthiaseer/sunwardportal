﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace APP.SWAM.Mobile.Helpers
{
    public static class Constant
    {
#if DEBUG
        public static string RestUrl = "http://10.10.10.129/APP.TaskManagementSystem/api/";
        public static string ChatHubUrl = "http://192.168.119.81:59194/ChatHub/";
        public static string DEVURL = "https://portal.sunwardpharma.com:2025/AppUpload/";
        public static string LIVEURL = "https://portal.sunwardpharma.com/AppUpload/";
#else
        public static string RestUrl = "https://portal.sunwardpharma.com/tmsapidev/api/"; 
        public static string ChatHubUrl = "https://portal.sunwardpharma.com/tmsapidev/ChatHub/";
        public static string DEVURL = "https://portal.sunwardpharma.com:2025/AppUpload/";
        public static string LIVEURL = "https://portal.sunwardpharma.com/AppUpload/";
#endif
        // Credentials that are hard coded into the REST service
        public static string Username = "";
        public static string Password = "";
        public static int Timeout { get; set; } = 5;
        public static long AppUserId { get; set; }
        public static string LoginDate = "";
    }

}
