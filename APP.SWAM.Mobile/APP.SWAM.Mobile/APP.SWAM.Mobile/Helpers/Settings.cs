using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System.Collections.Generic;

namespace APP.SWAM.Mobile.Helpers
{
    /// <summary>
    /// This is the Settings static class that can be used in your Core solution or in any
    /// of your client applications. All settings are laid out the same exact way with getters
    /// and setters. 
    /// </summary>
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants

        private const string UserIdKey = "user_id_key";
        private static readonly long UserIdDefault = 0;

        private const string ProfileIdKey = "profile_id_key";
        private static readonly int ProfileIdDefault = 0;

        private const string AccessTokenKey = "access_token_key";
        private static readonly string AccessTokenDefault = string.Empty;

        private const string CurrentBookingIdKey = "current_booking_id";
        private static readonly int CurrentBookingIdDefault = 0;

        private const string UwpWindowSizeKey = "uwp_window_size";
        private static readonly string UwpWindowSizeDefault = string.Empty;

        private const string loginname = "login_name";
        private static readonly string loginnamedefault = string.Empty;

        private const string username = "user_name";
        private static readonly string usernamedefault = string.Empty;


        private const string loginpassword = "login_password";
        private static readonly string loginpassworddefault = string.Empty;

        private const string isLoggedIn = "logged_in";
        private static readonly bool isLoggedInDefault = false;

        private const string isRestServiceURL = "Rest_URL";
        private static readonly string isRestServiceURLDefault = string.Empty;

        private const string isDBName = "DB_Name";
        private static readonly string isisDBNameDefault = string.Empty;

        private const string isNAVName = "NAV_Name";
        private static readonly string isNAVNameDefault = string.Empty;

        private const string isInsideChat = "inside_chat";
        private static readonly bool isInsideChatDefault = false;

        private const string isInsideChatUser = "inside_chatuser";
        private static readonly bool isInsideChatUserDefault = false;

        private const string currentChatUserId = "currentChatUser";
        private static readonly long currentChatUserIdDefault = 0;

        #endregion

        public static long UserId
        {
            get
            {
                return AppSettings.GetValueOrDefault(UserIdKey, UserIdDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(UserIdKey, value);
            }
        }

        public static int ProfileId
        {
            get
            {
                return AppSettings.GetValueOrDefault(ProfileIdKey, ProfileIdDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(ProfileIdKey, value);
            }
        }

        public static string AccessToken
        {
            get
            {
                return AppSettings.GetValueOrDefault(AccessTokenKey, AccessTokenDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(AccessTokenKey, value);
            }
        }

        public static int CurrentBookingId
        {
            get
            {
                return AppSettings.GetValueOrDefault(CurrentBookingIdKey, CurrentBookingIdDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(CurrentBookingIdKey, value);
            }
        }

        public static string UwpWindowSize
        {
            get
            {
                return AppSettings.GetValueOrDefault(UwpWindowSizeKey, UwpWindowSizeDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(UwpWindowSizeKey, value);
            }
        }

        public static string LoginName
        {
            get
            {
                return AppSettings.GetValueOrDefault(loginname, loginnamedefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(loginname, value);
            }
        }
        public static string UserName
        {
            get
            {
                return AppSettings.GetValueOrDefault(loginname, loginnamedefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(loginname, value);
            }
        }

        public static string LoginPassword
        {
            get
            {
                return AppSettings.GetValueOrDefault(loginpassword, loginpassworddefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(loginpassword, value);
            }
        }

        public static bool IsLoggedIn
        {
            get
            {
                return AppSettings.GetValueOrDefault(isLoggedIn, isLoggedInDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(isLoggedIn, value);
            }
        }

        public static string RestServiceURL
        {
            get
            {
                return AppSettings.GetValueOrDefault(isRestServiceURL, isRestServiceURLDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(isRestServiceURL, value);
            }
        }
        public static string DBName
        {
            get
            {
                return AppSettings.GetValueOrDefault(isDBName, isisDBNameDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(isDBName, value);
            }
        }
        public static string NAVName
        {
            get
            {
                return AppSettings.GetValueOrDefault(isNAVName, isNAVNameDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(isNAVName, value);
            }
        }

        public static bool InSideChat
        {
            get
            {
                return AppSettings.GetValueOrDefault(isInsideChat, isInsideChatDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(isInsideChat, value);
            }
        }
        public static bool InSideChatUser
        {
            get
            {
                return AppSettings.GetValueOrDefault(isInsideChatUser, isInsideChatUserDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(isInsideChatUser, value);
            }
        }
        public static long CurrentChatUserId
        {
            get
            {
                return AppSettings.GetValueOrDefault(currentChatUserId, currentChatUserIdDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(currentChatUserId, value);
            }
        }

        public static List<string> ExcludedMenus = new List<string>();



        public static void RemoveUserId()
        {
            AppSettings.Remove(UserIdKey);
        }

        public static void RemoveProfileId()
        {
            AppSettings.Remove(ProfileIdKey);
        }

        public static void RemoveAccessToken()
        {
            AppSettings.Remove(AccessTokenKey);
        }

        public static void RemoveCurrentBookingId()
        {
            AppSettings.Remove(CurrentBookingIdKey);
        }

        public static void RemoveLoginName()
        {
            AppSettings.Remove(loginname);
        }

        public static void RemoveLoginPassword()
        {
            AppSettings.Remove(loginpassword);
        }

        public static void RemoveIsLoggedIn()
        {
            AppSettings.Remove(isLoggedIn);
        }

        public static void RemoveIsInsideChat()
        {
            AppSettings.Remove(isInsideChat);
        }

        public static void RemoveIsInsideChatUser()
        {
            AppSettings.Remove(isInsideChatUser);
        }

        public static void RemoveCurrentChatUserId()
        {
            AppSettings.Remove(currentChatUserId);
        }
    }
}