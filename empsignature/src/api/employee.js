import myApi from '@/util/api'

export function getEmployee() {

    return myApi.getAll('employee/GetEmployeesByDropdownAll')
}

export function updateEmployeeSign(data) {
    return myApi.update(data.employeeId, data, "employee/UpdateEmployeeSign")
}