export default Object.freeze({
    SAVE: "Record Saved Successfully",
    UPDATE: "Record Updated Successfully",
    DELETE: "Record Deleted Successfully",
    VALIDATE: "validation error occured.Please check the required field values!.",
    DATEFORMAT: "DD-MMM-YYYY",
    YEARMONTHDAYFORMAT: "YYYY-MMM-DD",
    MONTHYEARFORMAT: "MMM-YYYY",
    DATETIMEFORMAT: "DD-MMM-YYYY hh:mm A",
    VERSION: "Version Saved Successfully",
    ITEMCLASSIFICATIONPROFILEERROR: "Profile Not Set In Application Form",
    AllowMaxSize: "1024",
    SuperUser: "47",
  });
  