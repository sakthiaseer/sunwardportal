import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import VueSignaturePad from 'vue-signature-pad';
import moment from "moment";
Vue.use(VueSignaturePad);
Vue.config.productionTip = false
Vue.use(moment);
Vue.filter("formatDate", function (value) {
  if (value) {
    return moment(value).format("DD-MMM-YYYY");
  }
});
Vue.filter("monthYear", function (value) {
  if (value) {
    return moment(value).format("MM-YYYY");
  }
});
Vue.filter('formatTime', function (value) {
  if (value) {
    return moment(value).format('hh:mm A');
  }
});
Vue.filter('formatDateTime', function (value) {
  if (value) {
    return moment(value).format('DD-MMM-YYYY hh:mm A');
  }
});
new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
