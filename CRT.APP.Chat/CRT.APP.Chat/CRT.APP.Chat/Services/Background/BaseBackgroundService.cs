﻿using APP.SWAM.Mobile.Services;
using CRT.APP.Chat.Models;
using Matcha.BackgroundService;
using Plugin.LocalNotifications;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace CRT.APP.Chat.Services.Background
{
    public abstract class BaseBackgroundService : IPeriodicTask
    {
        public BaseBackgroundService(int minutes, ChatMessageModel message)
        {
            Interval = TimeSpan.FromSeconds(minutes);
            Message = message;
        }

        public TimeSpan Interval { get; set; }

        public ChatMessageModel Message { get; set; }

        public async Task<bool> StartJob()
        {
            try
            {
                var userID = Preferences.Get("UserID", default(long));
                var currentChatUserId = Preferences.Get("CurrentChatUserId", default(long));
                if (userID > 0)
                {
                    long activeChatUserId = 0;
                    if (currentChatUserId > 0)
                    {
                        activeChatUserId = currentChatUserId;
                    }
                    RestClient restClient = new RestClient();
                    var chatMessageModels = await restClient.GetByIds<ChatMessageModel>("ChatMessage/GetUserNotificationMessages", userID, activeChatUserId);
                    chatMessageModels.ForEach(m =>
                    {
                        CrossLocalNotifications.Current.Show(m.SenderName, m.Message, (int)m.SentUserId, DateTime.Now.AddSeconds(5));
                    });
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

            return true;
        }
    }

}
