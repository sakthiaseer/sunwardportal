﻿using CRT.APP.Chat.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRT.APP.Chat.Services.Background
{
    public class PushNotificationPeriodicService : BaseBackgroundService
    {
        public PushNotificationPeriodicService(int minutes, ChatMessageModel message) : base(minutes, message)
        {

        }
    }
}
