﻿using Acr.UserDialogs;
using CRT.APP.Chat.Helpers;
using CRT.APP.Chat.Models;
using CRT.APP.Chat.Models.Chat;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace CRT.APP.Chat.Services.ChatService
{
    public class ChatService : IChatService, INotifyPropertyChanged
    {
        public HubConnection Connection;

        private ChatHubSingleton chatHubSingleton;

        public ChatService()
        {
            Connection = ChatHubSingleton.GetChatHubInstance().Connection;
            Connection.ServerTimeout = TimeSpan.FromDays(1);
        }


        public async Task Connect()
        {
            try
            {
                bool isConnectionFailed = false;

                if(Connection.State==HubConnectionState.Connected)
                {
                    return;
                }

                await Connection.StartAsync().ContinueWith(task =>
                {
                    if (task.IsFaulted)
                    {
                        isConnectionFailed = true;
                        UserDialogs.Instance.Toast("Connectivity Error", TimeSpan.FromSeconds(5));
                    }
                });

                if (!isConnectionFailed)
                {
                    string connectionGuid = string.Empty;
                    Connection.On<string>("UserConnected", async (connectionId) =>
                    {
                        ChatHubSingleton.GetChatHubInstance().ConnectionID = connectionId;
                        await InsertOnlineUser();
                    });

                    Connection.On("ReceiveMessage", (ChatMessageModel message) =>
                    {
                        MessagingCenter.Send(this, "AddMessages", message);
                    });
                }
            }

            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task InsertOnlineUser()
        {
            var userId = Preferences.Get("UserID", default(long));
            await Connection.SendAsync("InsertOnlineUserAsync", new User { UserId = userId, Name = Constant.Username, ConnectionId = ChatHubSingleton.GetChatHubInstance().ConnectionID });
        }

        public string ConnectionGUID { get; set; }

        public List<User> ActiveUsers { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public async Task Send(ChatMessageModel message)
        {
            try
            {
                await Connection.SendAsync("SendMessageToUser", message);
            }
            catch (Exception ex)
            {
                if (Connection.State == HubConnectionState.Disconnected)
                {
                    UserDialogs.Instance.Toast("Connectivity Error", TimeSpan.FromSeconds(5));
                    return;
                }
            }
        }

        public async Task JoinRoom(string roomName)
        {
            await Connection.SendAsync("JoinGroup", roomName);
        }

        public async Task Disconnect()
        {
            await Connection.StopAsync();
        }
    }
}
