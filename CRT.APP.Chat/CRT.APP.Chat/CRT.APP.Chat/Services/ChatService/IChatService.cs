﻿using CRT.APP.Chat.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRT.APP.Chat.Services.ChatService
{
    public interface IChatService
    {
        Task Connect();
        Task Send(ChatMessageModel message);
        Task JoinRoom(string roomName);
        Task Disconnect();
    }
}
