﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

using CRT.APP.Chat.Models;
using CRT.APP.Chat.Services;
using Xamarin.Essentials;
using Acr.UserDialogs;

namespace CRT.APP.Chat.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {

        bool isBusy = false;
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

        string title = string.Empty;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

        private bool _isConnected = (Connectivity.NetworkAccess == NetworkAccess.Internet);

        public bool IsConnected
        {
            get { return _isConnected; }
            set
            {
                _isConnected = value;
                OnPropertyChanged();
                if (!IsConnected)
                {
                    ShowConnectivityError();
                }
            }
        }

        public BaseViewModel()
        {
            Connectivity.ConnectivityChanged += OnNetworConnectiveChanged;
        }

        private void OnNetworConnectiveChanged(object sender, ConnectivityChangedEventArgs e)
        {
            var current = e.NetworkAccess;

            switch (current)
            {
                case NetworkAccess.Internet:
                    IsConnected = true;
                    break;
                case NetworkAccess.Local:
                    IsConnected = true;
                    break;
                case NetworkAccess.ConstrainedInternet:
                    IsConnected = true;
                    break;
                case NetworkAccess.None:
                    IsConnected = false;
                    break;
                case NetworkAccess.Unknown:
                    IsConnected = false;
                    break;
            }
        }

        public void ShowConnectivityError()
        {
            var stringResponse = "A problem has occurred with your network connection.";
            UserDialogs.Instance.Toast(stringResponse, TimeSpan.FromSeconds(5));
        }

        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName]string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
