﻿using Acr.UserDialogs;
using APP.SWAM.Mobile.Services;
using CRT.APP.Chat.Helpers;
using CRT.APP.Chat.Models;
using CRT.APP.Chat.Services;
using CRT.APP.Chat.Services.ChatService;
using CRT.APP.Chat.Views.Chat;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace CRT.APP.Chat.ViewModels.Login
{
    /// <summary>
    /// ViewModel for login page.
    /// </summary>
    [Preserve(AllMembers = true)]
    public class LoginPageViewModel : LoginViewModel
    {
        #region Fields

        private string password;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance for the <see cref="LoginPageViewModel" /> class.
        /// </summary>
        public LoginPageViewModel()
        {
            this.LoginCommand = new Command(this.LoginClicked);
            this.SignUpCommand = new Command(this.SignUpClicked);
            this.ForgotPasswordCommand = new Command(this.ForgotPasswordClicked);
            this.SocialMediaLoginCommand = new Command(this.SocialLoggedIn);
        }

        #endregion

        #region property

        /// <summary>
        /// Gets or sets the property that is bound with an entry that gets the password from user in the login page.
        /// </summary>
        public string Password
        {
            get
            {
                return this.password;
            }

            set
            {
                if (this.password == value)
                {
                    return;
                }

                this.password = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        #region Command

        /// <summary>
        /// Gets or sets the command that is executed when the Log In button is clicked.
        /// </summary>
        public Command LoginCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that is executed when the Sign Up button is clicked.
        /// </summary>
        public Command SignUpCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that is executed when the Forgot Password button is clicked.
        /// </summary>
        public Command ForgotPasswordCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that is executed when the social media login button is clicked.
        /// </summary>
        public Command SocialMediaLoginCommand { get; set; }

        #endregion

        #region methods

        /// <summary>
        /// Invoked when the Log In button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private async void LoginClicked(object obj)
        {
            // Do something

            IsBusy = true;
            UserDialogs.Instance.ShowLoading("Login Progress...");
            try
            {
                 var userID = Preferences.Get("UserID", default(long));
                if (IsConnected)
                {
                    if (!String.IsNullOrEmpty(LoginID) && !String.IsNullOrEmpty(LoginPassword))
                    {
                        var restclient = new RestClient();

                        ApplicationUserModel userModel = new ApplicationUserModel
                        {
                            LoginID = LoginID,
                            LoginPassword = LoginPassword
                        };

                        ApplicationUserModel applicationUserModel = await restclient.Login("UserAuth/Login", userModel);

                        if (applicationUserModel.UserID > 0)
                        {
                            Constant.AppUserId = applicationUserModel.UserID;
                            Constant.Username = applicationUserModel.UserName;
                            Constant.LoginDate = DateTime.Now.ToString();
                            Preferences.Set("UserID", applicationUserModel.UserID);
                            Preferences.Set("UserName", applicationUserModel.UserName);
                            Preferences.Set("LoginID", userModel.LoginID);
                            Preferences.Set("LoginPassword", userModel.LoginPassword);
                            await InitializeChatService();
                            App.Current.MainPage = new NavigationPage(new RecentChatPage());
                        }
                        else
                        {
                            await Application.Current.MainPage.DisplayAlert("Login Failure", "Please Enter Valid Details!", "OK");
                            IsBusy = false;
                            UserDialogs.Instance.HideLoading();
                            userModel = new ApplicationUserModel();
                            return;
                        }
                    }

                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Login Failure", "Please Enter Valid Details!", "OK");
                        IsBusy = false;
                        UserDialogs.Instance.HideLoading();
                        return;
                    }

                }
                else
                {
                    ShowConnectivityError();
                }
            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Login Failure", "Unexpected error!.Check the connectivity", "OK");
                IsBusy = false;
                UserDialogs.Instance.HideLoading();
                Debug.WriteLine("Login Exception details",ex);
            }


            IsBusy = false;
            UserDialogs.Instance.HideLoading();
        }

        /// <summary>
        /// Invoked when the Sign Up button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private void SignUpClicked(object obj)
        {
            // Do something
        }

        /// <summary>
        /// Invoked when the Forgot Password button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private async void ForgotPasswordClicked(object obj)
        {
            var label = obj as Label;
            label.BackgroundColor = Color.FromHex("#70FFFFFF");
            await Task.Delay(100);
            label.BackgroundColor = Color.Transparent;
        }

        /// <summary>
        /// Invoked when social media login button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private void SocialLoggedIn(object obj)
        {
            // Do something
        }


        private async Task InitializeChatService()
        {
            try
            {
                ChatService chatService = new ChatService();
                await chatService.Connect();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion
    }
}