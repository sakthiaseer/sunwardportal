﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using CRT.APP.Chat.Models.Chat;
using CRT.APP.Chat.Views.Chat;
using Acr.UserDialogs;
using APP.SWAM.Mobile.Services;
using CRT.APP.Chat.Models;
using Xamarin.Essentials;
using CRT.APP.Chat.Services.ChatService;
using Plugin.LocalNotifications;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System.IO;
using Plugin.AudioRecorder;
using System.Threading.Tasks;
using CRT.APP.Chat.Helpers;

namespace CRT.APP.Chat.ViewModels.Chat
{
    /// <summary>
    /// ViewModel for chat message page.
    /// </summary>
    [Preserve(AllMembers = true)]
    public class ChatMessageViewModel : BaseViewModel, INotifyPropertyChanged
    {
        #region Fields

        private string profileName = "Alex Russell";

        private string newMessage;

        private string profileImage = App.BaseImageUrl + "man_2.jpg";

        private ObservableCollection<ChatMessageModel> chatMessageInfo = new ObservableCollection<ChatMessageModel>();

        /// <summary>
        /// Stores the message text in an array. 
        /// </summary>
        private readonly string[] descriptions = {
            "Hi, can you tell me the specifications of the Dell Inspiron 5370 laptop?",
            " * Processor: Intel Core i5-8250U processor " +
            "\n" + " * OS: Pre-loaded Windows 10 with lifetime validity" +
            "\n" + " * Display: 13.3-inch FHD (1920 x 1080) LED display" +
            "\n" + " * Memory: 8GB DDR RAM with Intel UHD 620 Graphics" +
            "\n" + " * Battery: Lithium battery",
            "How much battery life does it have with wifi and without?",
            "Approximately 5 hours with wifi. About 7 hours without.",
        };
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="ChatMessageViewModel" /> class.
        /// </summary>
        public ChatMessageViewModel()
        {
            this.MakeVoiceCall = new Command(this.VoiceCallClicked);
            this.MakeVideoCall = new Command(this.VideoCallClicked);
            this.MenuCommand = new Command(this.MenuClicked);
            this.ShowCamera = new Command(this.CameraClicked);
            this.SendAttachment = new Command(this.AttachmentClicked);
            this.SendCommand = new Command(this.SendClicked);
            this.BackCommand = new Command(this.BackButtonClicked);
            this.ProfileCommand = new Command(this.ProfileClicked);
            this.SendAudio = new Command(this.SendAudioClicked);
            this.PlayAudio = new Command(this.PlayAudioClicked);

            SubscribeMessages();
            this.FillData();
            this.InitializeAudioRecorder();
        }

        public ChatMessageViewModel(ChatDetail chatDetail)
        {
            this.ChatDetailItem = chatDetail;
            this.MakeVoiceCall = new Command(this.VoiceCallClicked);
            this.MakeVideoCall = new Command(this.VideoCallClicked);
            this.MenuCommand = new Command(this.MenuClicked);
            this.ShowCamera = new Command(this.CameraClicked);
            this.SendAttachment = new Command(this.AttachmentClicked);
            this.SendCommand = new Command(this.SendClicked);
            this.BackCommand = new Command(this.BackButtonClicked);
            this.ProfileCommand = new Command(this.ProfileClicked);
            this.SendAudio = new Command(this.SendAudioClicked);
            this.PlayAudio = new Command(this.PlayAudioClicked);

            SubscribeMessages();
            this.FillData();
            this.InitializeAudioRecorder();
        }

        #endregion

        #region Event
        /// <summary>
        /// The declaration of the property changed event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Public Properties
        public INavigation Navigation { get; set; }
        /// <summary>
        /// Gets or sets the profile name.
        /// </summary>
        public string ProfileName
        {
            get
            {
                return this.profileName;
            }

            set
            {
                this.profileName = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the profile image.
        /// </summary>
        public string ProfileImage
        {
            get
            {
                return this.profileImage;
            }

            set
            {
                this.profileImage = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a collection of chat messages.
        /// </summary>
        public ObservableCollection<ChatMessageModel> ChatMessageInfo
        {
            get
            {
                return this.chatMessageInfo;
            }

            set
            {
                this.chatMessageInfo = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a new message.
        /// </summary>
        public string NewMessage
        {
            get
            {
                return this.newMessage;
            }

            set
            {
                this.newMessage = value;
                this.NotifyPropertyChanged();
            }
        }


        /// <summary>
        /// Gets or sets a new message.
        /// </summary>
        public ChatDetail ChatDetailItem
        {
            get
            {
                return this.chatDetailItem;
            }

            set
            {
                this.chatDetailItem = value;
                this.NotifyPropertyChanged();
            }
        }

        private ChatDetail chatDetailItem = new ChatDetail();

        private string inComingMessage;

        public string InComingMessage
        {
            get { return inComingMessage; }
            set { inComingMessage = value; OnPropertyChanged(); }
        }

        private bool isStopped;
        public bool IsStopped
        {
            get
            {
                return this.isStopped;
            }

            set
            {
                this.isStopped = value;
                this.NotifyPropertyChanged();
            }
        }

        private bool isRecord = true;
        public bool IsRecord
        {
            get
            {
                return this.isRecord;
            }

            set
            {
                this.isRecord = value;
                this.NotifyPropertyChanged();
            }
        }


        #endregion

        #region Commands

        /// <summary>
        /// Gets or sets the command that is executed when the profile name is clicked.
        /// </summary>
        public Command ProfileCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that is executed when the voice call button is clicked.
        /// </summary>
        public Command MakeVoiceCall { get; set; }

        /// <summary>
        /// Gets or sets the command that is executed when the video call button is clicked.
        /// </summary>
        public Command MakeVideoCall { get; set; }

        /// <summary>
        /// Gets or sets the command that is executed when the menu button is clicked.
        /// </summary>
        public Command MenuCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that is executed when the camera button is clicked.
        /// </summary>
        public Command ShowCamera { get; set; }

        /// <summary>
        /// Gets or sets the command that is executed when the attachment button is clicked.
        /// </summary>
        public Command SendAttachment { get; set; }

        /// <summary>
        /// Gets or sets the command that is executed when the send button is clicked.
        /// </summary>
        public Command SendCommand { get; set; }

        public Command SendAudio { get; set; }

        public Command PlayAudio { get; set; }

        /// <summary>
        /// Gets or sets the command that is executed when the back button is clicked.
        /// </summary>
        public Command BackCommand { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// The PropertyChanged event occurs when changing the value of property.
        /// </summary>
        /// <param name="propertyName">Property name</param>
        public void NotifyPropertyChanged([CallerMemberName]string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Initializes a collection and add it to the message items.
        /// </summary>
        private void GenerateMessageInfo()
        {
            //var currentTime = DateTime.Now;
            //this.ChatMessageInfo = new ObservableCollection<ChatMessageModel>
            //{
            //    new ChatMessageModel
            //    {
            //        Message = this.descriptions[0],
            //        Time = currentTime.AddMinutes(-2517),
            //        IsReceived = true,
            //    },
            //    new ChatMessageModel
            //    {
            //        Message = this.descriptions[1],
            //        Time = currentTime.AddMinutes(-2408),
            //    },
            //    new ChatMessageModel
            //    {
            //        ImagePath = App.BaseImageUrl + "Electronics.png",
            //        Time = currentTime.AddMinutes(-2405),
            //    },
            //    new ChatMessageModel
            //    {
            //        Message = this.descriptions[2],
            //        Time = currentTime.AddMinutes(-1103),
            //        IsReceived = true,
            //    },
            //    new ChatMessageModel
            //    {
            //        Message = this.descriptions[3],
            //        Time = currentTime.AddMinutes(-1006),
            //    },
            //};


        }

        AudioRecorderService recorder;
        AudioPlayer player;

        private void InitializeAudioRecorder()
        {
            recorder = new AudioRecorderService
            {
                StopRecordingAfterTimeout = true,
                TotalAudioTimeout = TimeSpan.FromSeconds(15),
                AudioSilenceTimeout = TimeSpan.FromSeconds(2)
            };

            player = new AudioPlayer();
            player.FinishedPlaying += OnAudiorecordFinished;
        }

        private void OnAudiorecordFinished(object sender, EventArgs e)
        {
        }

        async void FillData()
        {
            var userID = Preferences.Get("UserID", default(long));
            UserDialogs.Instance.ShowLoading();
            var restclient = new RestClient();
            var chatMessages = await restclient.GetByIds<ChatMessageModel>("ChatMessage/GetMessageByUser", ChatDetailItem.UserId, userID);
            chatMessages.ForEach(c => 
            {
                if (c.MessageType == 2)
                {
                    c.FilePath = Constant.ImageUrl + "Chat/ChatImage/" + c.FileName;
                }
                else if (c.MessageType == 3)
                {
                    c.FilePath = Constant.ImageUrl + "Chat/ChatAudio/" + c.FileName;
                }
            });
            this.ChatMessageInfo = new ObservableCollection<ChatMessageModel>(chatMessages);
            UserDialogs.Instance.HideLoading();
        }

        /// <summary>
        /// Invoked when the voice call button is clicked.
        /// </summary>
        /// <param name="obj">The object</param>
        private void VoiceCallClicked(object obj)
        {
            // Do something
        }

        /// <summary>
        /// Invoked when the video call button is clicked.
        /// </summary>
        /// <param name="obj">The object</param>
        private void VideoCallClicked(object obj)
        {
            // Do something
        }

        /// <summary>
        /// Invoked when the menu button is clicked.
        /// </summary>
        /// <param name="obj">The object</param>
        private void MenuClicked(object obj)
        {
            // Do something
        }

        /// <summary>
        /// Invoked when the camera icon is clicked.
        /// </summary>
        /// <param name="obj">The object</param>
        private async void CameraClicked(object obj)
        {
            // Do something
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                UserDialogs.Instance.Alert("No Camera", ":( No camera available.", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 75,
                CustomPhotoSize = 50,
                PhotoSize = PhotoSize.MaxWidthHeight,
                MaxWidthHeight = 2000,
                DefaultCamera = CameraDevice.Front
            });

            if (file == null)
                return;

            var imageSource = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                file.Dispose();
                return stream;
            });

            ChatMessageAttachmentModel chatMessageImageModel = new ChatMessageAttachmentModel
            {
                FileName = Guid.NewGuid().ToString() + ".png",
                UploadFile = ReadFully(file.GetStream()),
                MessageType = 2
            };

            UserDialogs.Instance.ShowLoading();
            var restclient = new RestClient();
            var uploadedChatmessageModel = await restclient.PostAsync<ChatMessageAttachmentModel>("ChatMessage/UploadChatImage", chatMessageImageModel);
            SendMessage(uploadedChatmessageModel);
            UserDialogs.Instance.HideLoading();
        }

        /// <summary>
        /// Invoked when the attachment icon is clicked.
        /// </summary>
        /// <param name="obj">The object</param>
        private async void AttachmentClicked(object obj)
        {
            // Do something
            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                UserDialogs.Instance.Alert("Photos Not Supported", ":( Permission not granted to photos.", "OK");
                return;
            }
            var file = await Plugin.Media.CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
            {
                PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium,

            });


            if (file == null)
                return;

            var imageSource = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                file.Dispose();
                return stream;
            });

            ChatMessageAttachmentModel chatMessageImageModel = new ChatMessageAttachmentModel
            {
                FileName = Guid.NewGuid().ToString() + ".png",
                UploadFile = ReadFully(file.GetStream()),
                MessageType = 2
            };

            UserDialogs.Instance.ShowLoading();
            var restclient = new RestClient();
            var uploadedChatmessageModel = await restclient.PostAsync<ChatMessageAttachmentModel>("ChatMessage/UploadChatImage", chatMessageImageModel);
            SendMessage(uploadedChatmessageModel);
            UserDialogs.Instance.HideLoading();
        }

        /// <summary>
        /// Invoked when the send button is clicked.
        /// </summary>
        /// <param name="obj">The object</param>
        private void SendClicked(object obj)
        {
            if (!string.IsNullOrWhiteSpace(this.NewMessage))
            {
                SendMessage();
            }
            this.NewMessage = null;
        }

        private async void SendAudioClicked(object obj)
        {
            await RecordAudio();
        }

        async Task RecordAudio()
        {
            try
            {
                IsStopped = true;
                IsRecord = false;
                if (!recorder.IsRecording) //Record button clicked
                {
                    //start recording audio
                    recorder.StopRecordingOnSilence = false;
                    var audioRecordTask = await recorder.StartRecording();

                    await audioRecordTask;
                }
                else //Stop button clicked
                {
                    //stop the recording...
                    await recorder.StopRecording();
                    IsStopped = false;
                    IsRecord = true;
                }
            }
            catch (Exception ex)
            {
                //blow up the app!
                throw ex;
            }
        }

        private async void PlayAudioClicked(object obj)
        {
            await recorder.StopRecording();
            PlayAudioItem();
        }

        private async void PlayAudioItem()
        {
            try
            {
                UserDialogs.Instance.ShowLoading();
                var filestream = recorder.GetAudioFileStream();
                // var filePath = recorder.GetAudioFilePath();

                if (filestream != null)
                {
                    await recorder.StopRecording();
                    IsStopped = false;
                    IsRecord = true;
                    // player.Play(filePath);
                    ChatMessageAttachmentModel chatMessageImageModel = new ChatMessageAttachmentModel
                    {
                        FileName = Guid.NewGuid().ToString() + ".wav",
                        UploadFile = ReadFully(filestream),
                        MessageType = 3
                    };
                    var restclient = new RestClient();
                    var uploadedChatmessageModel = await restclient.PostAsync<ChatMessageAttachmentModel>("ChatMessage/UploadChatAudio", chatMessageImageModel);
                    SendMessage(uploadedChatmessageModel);
                    UserDialogs.Instance.HideLoading();
                }
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.HideLoading();
                //blow up the app!
                throw ex;
            }
        }


        /// <summary>
        /// Invoked when the back button is clicked.
        /// </summary>
        /// <param name="obj">The object</param>
        private void BackButtonClicked(object obj)
        {
            // Do something
            Navigation.PopAsync();
        }

        /// <summary>
        /// Invoked when the Profile name is clicked.
        /// </summary>
        private void ProfileClicked(object obj)
        {
            // Do something
            Navigation.PushAsync(new ContactProfilePage());
        }

        private void SubscribeMessages()
        {
            MessagingCenter.Subscribe<ChatService, ChatMessageModel>(this, "AddMessages", (obj, item) =>
            {
                if (InComingMessage != item.Message)
                {
                    CrossLocalNotifications.Current.Show(item.SenderName, item.Message, (int)item.SentUserId, DateTime.Now.AddSeconds(5));
                    InComingMessage = item.Message;
                    ChatMessageInfo.Add(item);
                }
            });
        }

        public byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        private async void SendMessage(ChatMessageAttachmentModel chatMessageImageModel = null)
        {
            try
            {
                if (IsConnected)
                {
                    var userID = Preferences.Get("UserID", default(long));
                    var userName = Preferences.Get("UserName", default(string));
                    ChatService chatService = new ChatService();
                    IsBusy = true;
                    UserDialogs.Instance.ShowLoading();
                    if (chatMessageImageModel != null)
                    {
                        var chatMessage = new ChatMessageModel { ReceiveUserId = ChatDetailItem.UserId, SentUserId = userID, SenderName = userName, CreatedDateTime = DateTime.Now, IsIncoming = false, ConnectionId = ChatDetailItem.ConnectionId };
                        if (chatMessageImageModel.MessageType == 2)
                        {
                            chatMessage.ImagePath = chatMessageImageModel.UploadedFilePath;
                            chatMessage.FilePath = Constant.ImageUrl + "Chat/ChatImage/" + chatMessageImageModel.FileName;
                            chatMessage.FileName = chatMessageImageModel.FileName;
                            chatMessage.MessageType = 2;
                        }
                        if (chatMessageImageModel.MessageType == 3)
                        {
                            chatMessage.ImagePath = chatMessageImageModel.UploadedFilePath;
                            chatMessage.FilePath = Constant.ImageUrl + "Chat/ChatAudio/" + chatMessageImageModel.FileName;
                            chatMessage.FileName = chatMessageImageModel.FileName;
                            chatMessage.MessageType = 3;
                        }
                        await chatService.Send(chatMessage);
                        chatMessage.Time = DateTime.Now;
                        ChatMessageInfo.Add(chatMessage);
                    }
                    else
                    {
                        var chatMessage = new ChatMessageModel { ReceiveUserId = ChatDetailItem.UserId, SentUserId = userID, SenderName = userName, CreatedDateTime = DateTime.Now, Message = this.NewMessage, IsIncoming = false, MessageType = 1, ConnectionId = ChatDetailItem.ConnectionId };
                        await chatService.Send(chatMessage);
                        chatMessage.Time = DateTime.Now;
                        ChatMessageInfo.Add(chatMessage);
                        NewMessage = "";
                    }
                    IsBusy = false;
                    UserDialogs.Instance.HideLoading();
                }
                else
                {
                    ShowConnectivityError();
                }
            }
            catch (Exception e)
            {
                UserDialogs.Instance.HideLoading();
                throw e;
            }
        }

        #endregion
    }
}
