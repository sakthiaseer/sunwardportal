﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRT.APP.Chat.Models
{
    public class ApplicationUserModel : BaseModel
    {

        public long UserID { get; set; }
        public Nullable<long> EmployeeID { get; set; }
        //[Required(AllowEmptyStrings = false, ErrorMessage = "User code is required!.")]
        public string UserCode { get; set; }

        // [Required(AllowEmptyStrings = false, ErrorMessage = "User name is required!.")]
        public string UserName { get; set; }
        public string Name { get; set; }
        public string EmployeeNo { get; set; }

        //[Required(AllowEmptyStrings = false, ErrorMessage = "Email is required!.")]
        //[DataType(DataType.EmailAddress)]
        //[EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string UserEmail { get; set; }

        public byte AuthenticationType { get; set; }
        // [Required(AllowEmptyStrings = false, ErrorMessage = "Login Id is required!.")]
        public string LoginID { get; set; }
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Login password is required!.")]
        //[DataType(DataType.Password)]
        public string LoginPassword { get; set; }
        public Nullable<long> DepartmentID { get; set; }

        public string NickName { get; set; }

        public string Designation { get; set; }

        public string Section { get; set; }

        public string Department { get; set; }

    }
}
