﻿using CRT.APP.Chat.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRT.APP.Chat.Models
{
    public class ChatMessageModel : BaseViewModel
    {

        private long chatMessageId;
        public long ChatMessageId
        {
            get { return chatMessageId; }
            set { SetProperty(ref chatMessageId, value); }
        }

        private long? chatGroupId;
        public long? ChatGroupId
        {
            get { return chatGroupId; }
            set { SetProperty(ref chatGroupId, value); }
        }

        private long? chatUserId;
        public long? ChatUserId
        {
            get { return chatUserId; }
            set { SetProperty(ref chatUserId, value); }
        }

        private long receiveUserId;

        public long ReceiveUserId
        {
            get { return receiveUserId; }
            set { SetProperty(ref receiveUserId, value); }
        }

        private string receiverName;

        public string ReceiverName
        {
            get { return receiverName; }
            set { SetProperty(ref receiverName, value); }
        }

        private long sentUserId;

        public long SentUserId
        {
            get { return sentUserId; }
            set { SetProperty(ref sentUserId, value); }
        }

        private string senderName;

        public string SenderName
        {
            get { return senderName; }
            set { SetProperty(ref senderName, value); }
        }

        private string message;

        public string Message
        {
            get { return message; }
            set { SetProperty(ref message, value); }
        }

        private DateTime createdDateTime;

        public DateTime CreatedDateTime
        {
            get { return createdDateTime; }
            set { SetProperty(ref createdDateTime, value); }
        }

        private bool isIncoming;

        public bool IsIncoming
        {
            get { return isIncoming; }
            set { SetProperty(ref isIncoming, value); }
        }

        private bool isReceived;

        public bool IsReceived
        {
            get { return isReceived; }
            set { SetProperty(ref isReceived, value); }
        }

        public DateTime Time
        {
            get
            {
                return this.time;
            }

            set
            {
                this.time = value;
                this.OnPropertyChanged("Time");
            }
        }

        private DateTime time;


        /// <summary>
        /// Gets or sets the profile image.
        /// </summary>
        public string ImagePath
        {
            get
            {
                return this.imagePath;
            }

            set
            {
                this.imagePath = value;
                this.OnPropertyChanged("ImagePath");
            }
        }


        private string imagePath;

        public string FilePath
        {
            get
            {
                return this.filePath;
            }

            set
            {
                this.filePath = value;
                this.OnPropertyChanged("FilePath");
            }
        }

        private string filePath;

        public string FileName
        {
            get
            {
                return this.fileName;
            }

            set
            {
                this.fileName = value;
                this.OnPropertyChanged("FileName");
            }
        }

        private string fileName;

        public bool HasAttachement => !string.IsNullOrEmpty(attachementUrl);

        private string attachementUrl;

        public string AttachementUrl
        {
            get { return attachementUrl; }
            set { SetProperty(ref attachementUrl, value); }
        }

        private string connectionId;

        public string ConnectionId
        {
            get { return connectionId; }
            set { SetProperty(ref connectionId, value); }
        }

        private int messageType;

        public int MessageType
        {
            get { return messageType; }
            set { SetProperty(ref messageType, value); }
        }



    }

}
