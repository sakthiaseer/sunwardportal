﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRT.APP.Chat.Models
{
    public class User
    {
        public long UserId { get; set; }
        public string Id { get; set; }
        public string ConnectionId { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string AvatarUrl { get; set; }
        public string ImageURL { get; set; }
        public long ChatUserId { get; set; }
        public string UserName { get; set; }
        public Guid? SessionId { get; set; }
        public bool? IsOnline { get; set; }
        public string LastMessage { get; set; }
        public DateTime? LastChattedDate { get; set; }

        public string AvailableStatus { get; set; }

        public string Time { get; set; }
        public string MessageType { get; set; }

        public string NotificationType { get; set; }
    }
}
