﻿using Xamarin.Forms.Internals;

namespace CRT.APP.Chat.Models.Chat
{
    [Preserve(AllMembers = true)]
    public class ContactProfile
    {
        #region Public Property

        /// <summary>
        /// Gets or sets the profile image path.
        /// </summary>
        public string ImagePath { get; set; }

        #endregion
    }
}