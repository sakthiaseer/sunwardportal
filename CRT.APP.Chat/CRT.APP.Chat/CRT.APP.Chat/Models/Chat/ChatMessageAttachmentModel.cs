﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRT.APP.Chat.Models.Chat
{
    public class ChatMessageAttachmentModel
    {
        public string FileName { get; set; }
        public byte[] UploadFile { get; set; }
        public string UploadedFilePath { get; set; }
        public int MessageType { get; set; }
    }
}
