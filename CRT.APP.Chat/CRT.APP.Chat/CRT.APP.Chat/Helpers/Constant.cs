﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRT.APP.Chat.Helpers
{
    public static class Constant
    {
#if DEBUG
        public static string RestUrl = "https://portal.sunwardpharma.com/tmsapidev/api/";
        public static string ChatHubUrl = "https://portal.sunwardpharma.com/tmsapidev/ChatHub";
        public static string ImageUrl = "https://portal.sunwardpharma.com:2025/AppUpload/";
#else
        public static string RestUrl = "https://portal.sunwardpharma.com/tmsapidev/api/"; 
        public static string ChatHubUrl = "https://portal.sunwardpharma.com/tmsapidev/ChatHub";
        public static string ImageUrl = "https://portal.sunwardpharma.com:2025/AppUpload/";
#endif
        // Credentials that are hard coded into the REST service
        public static string Username = "";
        public static string Password = "";
        public static int Timeout { get; set; } = 5;
        public static long AppUserId { get; set; }
        public static string LoginDate = "";
    }
}
