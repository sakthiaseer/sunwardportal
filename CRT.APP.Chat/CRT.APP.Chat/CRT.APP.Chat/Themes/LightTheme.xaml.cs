﻿using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace CRT.APP.Chat.Themes
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LightTheme
    {
        public LightTheme()
        {
            InitializeComponent();
        }
    }
}