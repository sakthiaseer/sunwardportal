﻿using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace CRT.APP.Chat.Views.Chat
{
    /// <summary>
    /// Page to show profile page
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilePage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProfilePage" /> class.
        /// </summary>
        public ProfilePage()
        {
            this.InitializeComponent();
            this.ProfileImage.Source = App.BaseImageUrl + "man_2.jpg";
        }
    }
}