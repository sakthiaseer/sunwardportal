﻿using Syncfusion.DataSource;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;
using CRT.APP.Chat.Models.Chat;
using CRT.APP.Chat.ViewModels.Chat;
using CRT.APP.Chat.Models;
using Xamarin.Essentials;

namespace CRT.APP.Chat.Views.Chat
{
    /// <summary>
    /// Page to show chat message list
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChatMessagePage
    {
        ChatMessageViewModel viewModel;
        /// <summary>
        /// Initializes a new instance of the <see cref="ChatMessagePage" /> class.
        /// </summary>
        public ChatMessagePage()
        {
            viewModel = new ChatMessageViewModel
            {
                Navigation = Navigation
            };
            this.BindingContext = viewModel;
            this.InitializeComponent();

            ListView.DataSource.GroupDescriptors.Add(new GroupDescriptor
            {
                PropertyName = "CreatedDateTime",
                KeySelector = obj =>
                {
                    var item = obj as ChatMessageModel;
                    return item.CreatedDateTime.Date;
                }
            });
        }
        public ChatMessagePage(ChatDetail chatDetail)
        {
            viewModel = new ChatMessageViewModel(new ChatDetail { SenderName = chatDetail.SenderName, ConnectionId = chatDetail.ConnectionId, UserId = chatDetail.UserId, Time = chatDetail.Time, NotificationType = chatDetail.NotificationType, MessageType = chatDetail.MessageType, AvailableStatus = chatDetail.AvailableStatus, ImagePath = chatDetail.ImagePath, Message = chatDetail.Message })
            {
                Navigation = Navigation,
                ProfileName = chatDetail?.SenderName,
                ProfileImage = chatDetail?.ImagePath
            };
            this.BindingContext = viewModel;
            Preferences.Set("CurrentChatUserId", chatDetail.UserId);
            this.InitializeComponent();

            ListView.DataSource.GroupDescriptors.Add(new GroupDescriptor
            {
                PropertyName = "CreatedDateTime",
                KeySelector = obj =>
                {
                    var item = obj as ChatMessageModel;
                    return item.CreatedDateTime.Date;
                }
            });
        }


        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            Preferences.Remove("CurrentChatUserId");
        }
    }
}