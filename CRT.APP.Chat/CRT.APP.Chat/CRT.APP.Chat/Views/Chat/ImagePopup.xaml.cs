﻿using FFImageLoading.Forms;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CRT.APP.Chat.Views.Chat
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ImagePopup : PopupPage
    {
        public ImagePopup()
        {
            InitializeComponent();
        }

        public ImagePopup(object sender)
        {
            InitializeComponent();
            CachedImage cachedImage = sender as CachedImage;
            this.chatImage.Source = cachedImage.Source;
        }
    }
}