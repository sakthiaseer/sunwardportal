﻿using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;
using CRT.APP.Chat.Models.Chat;
using CRT.APP.Chat.Models;

namespace CRT.APP.Chat.Views.Chat
{
    /// <summary>
    /// Implements the message data template selector class.
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public class MessageDataTemplateSelector : DataTemplateSelector
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageDataTemplateSelector" /> class.
        /// </summary>
        public MessageDataTemplateSelector()
        {
            this.IncomingTextTemplate = new DataTemplate(typeof(IncomingTextTemplate));
            this.OutgoingTextTemplate = new DataTemplate(typeof(OutgoingTextTemplate));
            this.IncomingImageTemplate = new DataTemplate(typeof(IncomingImageTemplate));
            this.OutgoingImageTemplate = new DataTemplate(typeof(OutgoingImageTemplate));
            this.IncomingAudioTemplate = new DataTemplate(typeof(IncomingAudioTemplate));
            this.OutgoingAudioTemplate = new DataTemplate(typeof(OutgoingAudioTemplate));
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the incoming text template.
        /// </summary>
        public DataTemplate IncomingTextTemplate { get; set; }

        /// <summary>
        /// Gets or sets the outgoing text template.
        /// </summary>
        public DataTemplate OutgoingTextTemplate { get; set; }

        /// <summary>
        /// Gets or sets the incoming image template.
        /// </summary>
        public DataTemplate IncomingImageTemplate { get; set; }

        /// <summary>
        /// Gets or sets the outgoing text template.
        /// </summary>
        public DataTemplate OutgoingImageTemplate { get; set; }

        /// <summary>
        /// Gets or sets the incoming audio template.
        /// </summary>
        public DataTemplate IncomingAudioTemplate { get; set; }

        /// <summary>
        /// Gets or sets the outgoing audio template.
        /// </summary>
        public DataTemplate OutgoingAudioTemplate { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the incoming or outgoing text template.
        /// </summary>
        /// <param name="item">The item</param>
        /// <param name="container">The bindable object</param>
        /// <returns>Returns the data template</returns>
        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            if (((ChatMessageModel)item).IsIncoming)
            {
                if (string.IsNullOrEmpty(((ChatMessageModel)item).FilePath) && ((ChatMessageModel)item).MessageType == 1)
                {
                    return this.IncomingTextTemplate;
                }
                else if (!string.IsNullOrEmpty(((ChatMessageModel)item).FilePath) && ((ChatMessageModel)item).MessageType == 2)
                {
                    return this.IncomingImageTemplate;
                }
                else
                {
                    return this.IncomingAudioTemplate;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(((ChatMessageModel)item).FilePath) && ((ChatMessageModel)item).MessageType == 1)
                {
                    return this.OutgoingTextTemplate;
                }
                else if (!string.IsNullOrEmpty(((ChatMessageModel)item).FilePath) && ((ChatMessageModel)item).MessageType == 2)
                {
                    return this.OutgoingImageTemplate;
                }
                else
                {
                    return this.OutgoingAudioTemplate;
                }
            }
        }

        #endregion
    }
}
