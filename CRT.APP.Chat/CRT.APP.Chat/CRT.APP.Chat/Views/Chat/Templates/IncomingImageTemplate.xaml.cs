﻿
using Rg.Plugins.Popup.Services;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace CRT.APP.Chat.Views.Chat
{
    /// <summary>
    /// Which is used for incoming image template
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IncomingImageTemplate
    {
        public IncomingImageTemplate()
        {
            this.InitializeComponent();
        }

        private async void TapGestureRecognizer_Tapped(object sender, System.EventArgs e)
        {
            await PopupNavigation.PushAsync(new ImagePopup(sender));
        }
    }
}