﻿using CRT.APP.Chat.Models;
using CRT.APP.Chat.ViewModels;
using Plugin.AudioRecorder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace CRT.APP.Chat.Views.Chat
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OutgoingAudioTemplate 
    {
        public OutgoingAudioTemplate()
        {
            InitializeComponent();
            InitializeAudioRecorder();
            var test = cardView.Content.BindingContext;
        }

        public bool IsPlayed { get; set; } = true;
        public bool IsStopped { get; set; }

        AudioRecorderService recorder;
        AudioPlayer player;

        private void InitializeAudioRecorder()
        {
            recorder = new AudioRecorderService
            {
                StopRecordingAfterTimeout = true,
                TotalAudioTimeout = TimeSpan.FromSeconds(15),
                AudioSilenceTimeout = TimeSpan.FromSeconds(2)
            };

            player = new AudioPlayer();
            btnPlay.IsVisible = true;
            btnStop.IsVisible = false;
        }

        private void BtnPlay_Clicked(object sender, EventArgs e)
        {
            btnPlay.IsVisible = false;
            btnStop.IsVisible = true;
            var chatMessageModel = ((sender as Syncfusion.XForms.Buttons.SfButton).BindingContext) as ChatMessageModel;
            player.Play(chatMessageModel.FilePath);
        }

        private void BtnStop_Clicked(object sender, EventArgs e)
        {
            player.Pause();
            btnPlay.IsVisible = true;
            btnStop.IsVisible = false;

        }
    }
}