﻿using CRT.APP.Chat.Models;
using Plugin.AudioRecorder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace CRT.APP.Chat.Views.Chat
{
    /// <summary>
    /// Which is used for incoming image template
    /// </summary>
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IncomingAudioTemplate
    {
        public IncomingAudioTemplate()
        {
            InitializeComponent();
            InitializeAudioRecorder();
        }

        public bool IsPlayed { get; set; }
        public bool IsStopped { get; set; }

        AudioRecorderService recorder;
        AudioPlayer player;

        private void InitializeAudioRecorder()
        {
            recorder = new AudioRecorderService
            {
                StopRecordingAfterTimeout = true,
                TotalAudioTimeout = TimeSpan.FromSeconds(15),
                AudioSilenceTimeout = TimeSpan.FromSeconds(2)
            };

            player = new AudioPlayer();
        }

        private void BtnPlay_Clicked(object sender, EventArgs e)
        {
            IsPlayed = false;
            IsStopped = true;
            var chatMessageModel = ((sender as Syncfusion.XForms.Buttons.SfButton).BindingContext) as ChatMessageModel;
            player.Play(chatMessageModel.FilePath);
        }

        private void BtnStop_Clicked(object sender, EventArgs e)
        {
            player.Pause();
            btnPlay.IsVisible = true;
            btnStop.IsVisible = false;
        }
    }
}