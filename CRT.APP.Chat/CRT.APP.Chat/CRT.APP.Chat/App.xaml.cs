using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CRT.APP.Chat.Services;
using CRT.APP.Chat.Views;
using CRT.APP.Chat.Views.Login;
using Matcha.BackgroundService;
using CRT.APP.Chat.Services.Background;
using CRT.APP.Chat.Models;
using Xamarin.Essentials;
using CRT.APP.Chat.Views.Chat;
using CRT.APP.Chat.Services.ChatService;
using Microsoft.AspNetCore.SignalR.Client;

namespace CRT.APP.Chat
{
    public partial class App : Application
    {
        public static string BaseImageUrl { get; } = "https://portal.sunwardpharma.com/static/avatar/";

        public App()
        {
            InitializeComponent();
            DependencyService.Register<MockDataStore>();
            Login();
        }

        private async void Login()
        {
            string loginId = Preferences.Get("LoginID", default(string));
            string loginPassword = Preferences.Get("LoginPassword", default(string));
            ChatService chatService = new ChatService();
            if (String.IsNullOrEmpty(loginId) && string.IsNullOrEmpty(loginPassword))
            {
                this.MainPage = new NavigationPage(new SimpleLoginPage());
            }
            else
            {
                if (chatService.Connection.State == HubConnectionState.Connected)
                {
                    App.Current.MainPage = new NavigationPage(new RecentChatPage());
                }
                else
                {
                    App.Current.MainPage = new NavigationPage(new RecentChatPage());
                    await chatService.Connect();
                }
            }
        }

        protected override void OnStart()
        {
            StartBackgroundService();
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        private static void StartBackgroundService()
        {
            //Rss gets updated every 3 minutes
            BackgroundAggregatorService.Add(() => new PushNotificationPeriodicService(30, new ChatMessageModel()));

            //you now running the periodic task in the background
            BackgroundAggregatorService.StartBackgroundService();
        }
    }
}
