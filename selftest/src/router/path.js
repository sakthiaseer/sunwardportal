export default [
    {
        path: '/',
        name: 'humanMovement',
        component: () => import(
            `@/components/HumanMovement`
        )
    },
    {
        path: 'success',
        name: 'success',
        component: () => import(
            `@/components/Success`
        )
    },
    {
        path: 'checkout',
        name: 'checkout',
        component: () => import(
            `@/components/checkOut`
        )
    },
]