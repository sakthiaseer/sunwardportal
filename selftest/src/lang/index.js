import en from './en';
import cn from './cn';
import ma from './ma';
export default {
    en: {
        message: en
    },
    cn: {
        message: cn
    },
    ma: {
        message: ma
    },
}