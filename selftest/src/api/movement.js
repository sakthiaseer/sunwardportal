import myApi from '@/util/api'

export function getEmployees (companyName, companyNames, employeeid = 0) {

    return myApi.getAll('Employee/GetEmployeesList?companyname=' + companyName + "&&companynames=" + companyNames + "&&employeeid=" + employeeid)
}

export function getCompanyListings () {

    return myApi.getAll('CompanyListing/GetCompanyListings')
}
export function createSelfTest (data) {
    return myApi.create(data, "SelfTest/InsertSelfTest")
}
export function updateSelfTest (data) {
    return myApi.update(data.selfTestId,data, "SelfTest/UpdateSelfTest")
}
export function uploadSelfTestDocuments (data, sessionId) {

    return myApi.uploadFile(data, "SelfTest/UploadSelfTestDocuments?sessionId=" + sessionId)
}
export function getLocationCompany (data) {

    return myApi.getItem(data, "SelfTest/GetLocationCompany")
}
export function getSelfTestSearchBySession (sessionId, statusId) {

    return myApi.getAll("SelfTest/GetSelfTestSearchBySession?sessionId=" + sessionId + "&&statusId=" + statusId)
}