//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NAVDataSync
{
    using System;
    using System.Collections.Generic;
    
    public partial class ApplicationMasterDetail
    {
        public long ApplicationMasterDetailID { get; set; }
        public long ApplicationMasterID { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public Nullable<int> StatusCodeID { get; set; }
        public Nullable<long> AddedByUserID { get; set; }
        public Nullable<System.DateTime> AddedDate { get; set; }
        public Nullable<long> ModifiedByUserID { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<long> ProfileID { get; set; }
    
        public virtual ApplicationMaster ApplicationMaster { get; set; }
    }
}
