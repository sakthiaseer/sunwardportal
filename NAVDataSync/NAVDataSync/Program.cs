﻿using APP.EntityModel;
using LinqToExcel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using System.Globalization;
using System.IO;
namespace NAVDataSync
{
    class Program
    {
        static void Main(string[] args)
        {
            string pathToExcelFile = ""
     + @"C:\Users\arun\Downloads\NavItemMaster.xlsx";
            //SynLocalClinic(pathToExcelFile);
            //SynAntha(pathToExcelFile);
            // SynRecipe(pathToExcelFile);
            //SynApex(@"D:\Apex Stock Balance as at 31 Aug 19.xlsx");
            //SyncACItems(pathToExcelFile);
            // SynInventoryItem(pathToExcelFile);
            //SynProdOrder(pathToExcelFile);
            //SynDistItemMap(pathToExcelFile);
            SynNavItem(pathToExcelFile);
           // SyncWiki();
        }
        static void SyncWiki()
        {
            List<ApplicationWiki> applicationWikiModels = new List<ApplicationWiki>();
            string filePath = @"C:\Users\Aravinth\Downloads\Copy of ExcelData.xlsx";
            if (File.Exists(filePath))
            {
                Application xlApp = new Application();
                Workbook xlWorkBook = xlApp.Workbooks.Open(filePath);
                Worksheet xlWorkSheet = (Worksheet)xlWorkBook.Worksheets.get_Item(1);
                Range xlRange = xlWorkSheet.UsedRange;
                int totalRows = xlRange.Rows.Count;
                long? wikiTypeId = null;
                using (var context = new ACDataSyncModel())
                {
                    var applicationMasterID = context.ApplicationMasters.Where(w => w.ApplicationMasterCodeID == 147).FirstOrDefault().ApplicationMasterID;
                    wikiTypeId = context.ApplicationMasterDetails.Where(w => w.ApplicationMasterID == applicationMasterID && w.Value.ToLower() == "sop").FirstOrDefault().ApplicationMasterDetailID;
                }
                for (int rowCount = 1; rowCount <= totalRows; rowCount++)
                {
                    if (rowCount == 1)
                    {
                        Console.WriteLine("Start");
                    }
                    else
                    {
                        Console.WriteLine("..");
                    }
                    using (var context = new ACDataSyncModel())
                    {
                        var datetime = ((xlRange.Cells[rowCount, 6] as Range).Text);
                        var title = Convert.ToString((xlRange.Cells[rowCount, 4] as Range).Text);
                        if (!string.IsNullOrEmpty(title) || !string.IsNullOrEmpty(datetime))
                        {
                            var ApplicationWiki = new ApplicationWiki
                            {
                                ProfileReferenceNo = Convert.ToString((xlRange.Cells[rowCount, 2] as Range).Text),
                                Title = title,
                                VersionNo = Convert.ToString((xlRange.Cells[rowCount, 5] as Range).Text),
                                NewReviewDate = Convert.ToDateTime(datetime, CultureInfo.InvariantCulture),
                                Remarks = Convert.ToString((xlRange.Cells[rowCount, 7] as Range).Text),
                                StatusCodeID = 840,
                                AddedByUserID = 1,
                                AddedDate = DateTime.Now,
                                SessionID = Guid.NewGuid(),
                                WikiTypeID = wikiTypeId,
                            };
                            //context.ApplicationWikis.Add(ApplicationWiki);
                            //context.SaveChanges();
                        }
                        if (totalRows == rowCount)
                        {
                            Console.WriteLine("Completed");
                        }
                    }
                }
                xlWorkBook.Close();
                xlApp.Quit();
                Marshal.ReleaseComObject(xlWorkSheet);
                Marshal.ReleaseComObject(xlWorkBook);
                Marshal.ReleaseComObject(xlApp);
                Console.ReadLine();
            }
        }
        static void SynNavItem(string pathToExcelFile)
        {
            string sheetName = "Sheet1";
            List<NavItemMasterModel> itemMasterModels = new List<NavItemMasterModel>();
            var excelFile = new ExcelQueryFactory(pathToExcelFile);
            var navItemMasterModels = (from a in excelFile.Worksheet<NavItemMasterModel>(sheetName) select a).ToList();

            navItemMasterModels.ForEach(item =>
            {
                if (!string.IsNullOrEmpty(item.Company))
                {
                    var itemMasterModel = new NavItemMasterModel
                    {
                        ItemId = Convert.ToInt64(item.ItemId),
                        Company = item.Company,
                        CompanyId = Convert.ToInt64(item.CompanyId),
                        No = item.No
                    };
                    itemMasterModels.Add(itemMasterModel);
                }

            }); 
            RestClient restClient = new RestClient();
            var result = restClient.PostAsync<List<NavItemMasterModel>>("NavItem/DeleteNavItems", itemMasterModels).GetAwaiter().GetResult();

        }


        static void SynLocalClinic(string pathToExcelFile)
        {
            //string sheetName = "Sheet1";

            //var excelFile = new ExcelQueryFactory(pathToExcelFile);
            //var claimdata = (from a in excelFile.Worksheet<LocalClinicModel>(sheetName) select a).ToList();
            //DateTime? nulldate = null;

            //claimdata.ForEach(item =>
            //{
            //    if (!string.IsNullOrEmpty(item.COMPANY))
            //    {
            //        var companyId = item.Country == "SWSG" ? 2 : 1;
            //        using (var context = new ACDataSyncModel())
            //        {
            //            var clinic = new LocalClinic
            //            {
            //                AddedByUserID = 1,
            //                AddedDate = DateTime.Now,
            //                Address1 = item.ADDRESS1,
            //                Address2 = item.ADDRESS2,
            //                Company = item.COMPANY,
            //                CountryID = companyId,
            //                PostalCode = item.ZIP,
            //                CompanyStatusID = 2250,
            //                ToSWStatusID = 2271
            //            };
            //            context.LocalClinics.Add(clinic);
            //            context.SaveChanges();
            //        }
            //    }

            //});

        }
        private static int GetWeekNumberOfMonth(DateTime date)
        {
            DateTime firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            int firstDay = (int)firstDayOfMonth.DayOfWeek;
            if (firstDay == 0)
            {
                firstDay = 7;
            }
            double d = (firstDay + date.Day - 1) / 7.0;
            return d > 5 ? (int)Math.Floor(d) : (int)Math.Ceiling(d);
        }
        //   static void AVG6Month(string pathToExcelFile)
        //   {
        //       string sheetName = "Sheet 1";

        //       var excelFile = new ExcelQueryFactory(pathToExcelFile);
        //       var claimdata = (from a in excelFile.Worksheet<AnthItemExcel>(sheetName) select a).ToList();

        //       var month = DateTime.Now.Month;// lastDayLastMonth.Month;
        //       var year = DateTime.Now.Year;// lastDayLastMonth.Year;
        //       var stockdate = DateTime.Parse("13-11-2020");
        //       var weekofMonth = GetWeekNumberOfMonth(stockdate);

        //       claimdata.ForEach(item =>
        //       {
        //           using (var context = new ACDataSyncModel())
        //           {
        //               if (item.Item == "SW0603P")
        //               {

        //               }
        //               var exist = context.ACItems.FirstOrDefault(d => d.ItemNo == item.Item && d.CompanyId == 2 && d.CustomerId == 21);
        //               if (exist != null)
        //               {
        //                   var line = context.DistStockBalances.FirstOrDefault(d => d.StockBalMonth.Value.Month == month && d.StockBalMonth.Value.Year == year && d.StockBalWeek == weekofMonth && d.DistItemId == exist.DistACID);
        //                   if (line != null)
        //                   {
        //                       line.Avg6MonthQty = line.Avg6MonthQty.GetValueOrDefault(0) + decimal.Parse(item.Avg6Month);

        //                   }
        //                   else
        //                   {
        //                       line = new DistStockBalance
        //                       {
        //                           AddedByUserID = 1,
        //                           AddedDate = stockdate,
        //                           Avg6MonthQty = decimal.Parse(item.Avg6Month),
        //                           POQuantity = 0,
        //                           Quantity = 0,
        //                           StatusCodeID = 1,
        //                           StockBalMonth = stockdate,
        //                           StockBalWeek = weekofMonth,
        //                           DistItemId = exist.DistACID,
        //                       };
        //                       context.DistStockBalances.Add(line);
        //                   }
        //                   context.SaveChanges();
        //               }
        //           }
        //       });
        //   }
        //   static void SynItemCode(string pathToExcelFile)
        //   {
        //       try
        //       {
        //           string sheetName = "ItemMaster_SG";

        //           var excelFile = new ExcelQueryFactory(pathToExcelFile);
        //           var claimdata = (from a in excelFile.Worksheet<ItemMaster>(sheetName) select a).ToList();

        //           claimdata.ForEach(item =>
        //           {
        //               using (var context = new ACDataSyncModel())
        //               {
        //                   var genericCode = new NavMethodCode();

        //                   if (!string.IsNullOrEmpty(item.ItemNo))
        //                   {
        //                       var categoryID = 1;

        //                       if (item.ItemGroup == "Capsule")
        //                       {
        //                           categoryID = 1;
        //                       }
        //                       else if (item.ItemGroup == "Cream")
        //                       {
        //                           categoryID = 2;
        //                       }
        //                       else if (item.ItemGroup == "Syrup")
        //                       {
        //                           categoryID = 4;
        //                       }
        //                       else if (item.ItemGroup == "Tablet")
        //                       {
        //                           categoryID = 5;
        //                       }
        //                       else if (item.ItemGroup == "Vet")
        //                       {
        //                           categoryID = 6;
        //                       }
        //                       else if (item.ItemGroup == "Powder")
        //                       {
        //                           categoryID = 7;
        //                       }
        //                       else if (item.ItemGroup == "INJ")
        //                       {
        //                           categoryID = 8;
        //                       }
        //                       var items = context.NAVItems.FirstOrDefault(f => f.No == item.ItemNo && f.CompanyId == 2);
        //                       if (items != null)
        //                       {
        //                           items.CategoryID = categoryID;
        //                           items.PackSize = int.Parse(item.PackSize);
        //                           items.PackUOM = item.PackUOM;
        //                           //items.StatusCodeID = 1;
        //                       }
        //                       context.SaveChanges();
        //                   }
        //               }
        //           });
        //       }
        //       catch (Exception ex)
        //       {
        //           Console.WriteLine(ex.Message);
        //           Console.Read();
        //       }
        //   }

        //   static void SynSimulationAddhoc(string pathToExcelFile)
        //   {
        //       string sheetName = "Sheet1";

        //       var excelFile = new ExcelQueryFactory(pathToExcelFile);
        //       var claimdata = (from a in excelFile.Worksheet<SimulationAddhocModel>(sheetName) select a).ToList();
        //       DateTime? nulldate = null;

        //       claimdata.ForEach(item =>
        //       {
        //           if (!string.IsNullOrEmpty(item.ItemNo))
        //           {
        //               var companyId = item.Company == "SWSIN" ? 2 : 1;
        //               using (var context = new ACDataSyncModel())
        //               {
        //                   var itemMas = context.NAVItems.FirstOrDefault(f => f.No == item.ItemNo && f.CompanyId == companyId);
        //                   if (itemMas != null)
        //                   {
        //                       var addhoc = new SimulationAddhoc
        //                       {
        //                           Categories = item.Catogories,
        //                           CustomerName = item.CustomerName,
        //                           Description = item.Description,
        //                           Description1 = item.Description2,
        //                           DocumantType = item.DocumentType,
        //                           DocumentNo = item.DocumentNo,
        //                           ItemID = itemMas.ItemId,
        //                           ItemNo = item.ItemNo,
        //                           OutstandingQty = decimal.Parse(item.OutstandingQuantity),
        //                           PromisedDate = !string.IsNullOrEmpty(item.PromisedDeliveryDate) ? DateTime.Parse(item.PromisedDeliveryDate) : nulldate,
        //                           SelltoCustomerNo = item.SelltoCustomerNo,
        //                           ShipmentDate = !string.IsNullOrEmpty(item.ShipmentDate) ? DateTime.Parse(item.ShipmentDate) : nulldate,
        //                           UOMCode = item.UnitofMeasureCode,
        //                           ExternalDocNo = item.ExternalDocumentNo,
        //                       };
        //                       context.SimulationAddhocs.Add(addhoc);
        //                       context.SaveChanges();
        //                   }
        //               }
        //           }

        //       });

        //   }

        //   static void SynNavProcess(string pathToExcelFile)
        //   {
        //       string sheetName = "Sheet1";

        //       var excelFile = new ExcelQueryFactory(pathToExcelFile);
        //       var claimdata = (from a in excelFile.Worksheet<NAVProcessModel>(sheetName) select a).ToList();
        //       DateTime? nulldate = null;

        //       claimdata.ForEach(item =>
        //       {
        //           if (!string.IsNullOrEmpty(item.ItemNo))
        //           {
        //               var companyId = 1;
        //               using (var context = new ACDataSyncModel())
        //               {
        //                   var itemMas = context.NAVItems.FirstOrDefault(f => f.No == item.ItemNo && f.CompanyId == companyId);
        //                   if (itemMas != null)
        //                   {
        //                       var addhoc = new NavManufacturingProcess
        //                       {
        //                           AddedByUserID = 1,
        //                           CompanyId = companyId,
        //                           AddedDate = DateTime.Now,
        //                           CompletionDate = !string.IsNullOrEmpty(item.CompletionDate) ? DateTime.Parse(item.CompletionDate) : nulldate,
        //                           Description = item.Description,
        //                           Description2 = item.Description2,
        //                           ItemId = itemMas.ItemId,
        //                           ItemNo = item.ItemNo,
        //                           NoChangeinDate = item.NoChangeinDate == "No" ? false : true,
        //                           PigeonHoleVersion = item.PigeonHoleVersion,
        //                           Process = item.Process,
        //                           ProdOrderNo = item.ProdOrderNo,
        //                           ReleaseFromPlanner = item.ReleaseFromPlanner == "No" ? false : true,
        //                           Remarks = item.Remarks,
        //                           ReplanRefNo = item.ReplanRefNo,
        //                           StartingDate = !string.IsNullOrEmpty(item.StartingDate) ? DateTime.Parse(item.StartingDate) : nulldate,
        //                           Substatus = item.Substatus,
        //                           StatusCodeID = 1
        //                       };
        //                       context.NavManufacturingProcesses.Add(addhoc);
        //                       context.SaveChanges();
        //                   }
        //               }
        //           }

        //       });

        //   }

        //   static void SynAntha(string pathToExcelFile)
        //   {
        //       string sheetName = "Stock Balance Listing 011019";
        //       pathToExcelFile = ""
        //+ @"D:\DB\Latest\Stock Balance Listing 011019 Antah.xlsx";
        //       var excelFile = new ExcelQueryFactory(pathToExcelFile);
        //       var claimdata = (from a in excelFile.Worksheet<AnthItemExcel>(sheetName) select a).ToList();

        //       claimdata.ForEach(item =>
        //       {
        //           if (item.Item != "Grand Total:" && item.Item != "Item" && !string.IsNullOrEmpty(item.Item))
        //           {
        //               using (var context = new ACDataSyncModel())
        //               {
        //                   var exist = context.ACItems.FirstOrDefault(d => d.ItemDesc == item.Description && d.ItemNo == item.Item && d.ACMonth.Value.Month == 9);
        //                   if (exist != null)
        //                   {
        //                       var distBalc = new DistStockBalance
        //                       {
        //                           DistItemId = exist.DistACID,
        //                           Quantity = decimal.Parse(item.Qtyonhand),
        //                           StockBalMonth = DateTime.Parse("03-Oct-2019"),
        //                       };
        //                       context.DistStockBalances.Add(distBalc);
        //                   }
        //                   context.SaveChanges();
        //               }
        //           }

        //       });
        //   }
        //   static void SynSGTender(string pathToExcelFile)
        //   {
        //       string sheetName = "PSB SEP";
        //       pathToExcelFile = ""
        //+ @"D:\DB\ACItemImport_ByMonth.xlsx";
        //       var excelFile = new ExcelQueryFactory(pathToExcelFile);
        //       var claimdata = (from a in excelFile.Worksheet<SGTender>(sheetName) select a).ToList();

        //       claimdata.ForEach(item =>
        //       {
        //           if (!string.IsNullOrEmpty(item.DistItem))
        //           {
        //               using (var context = new ACDataSyncModel())
        //               {
        //                   var exist = context.ACItems.FirstOrDefault(d => d.ItemDesc == item.Description && d.ItemNo == item.DistItem);
        //                   if (exist != null)
        //                   {

        //                   }
        //                   else
        //                   {
        //                       var acItem = new ACItem
        //                       {
        //                           DistName = "MSS",
        //                           ACQty = decimal.Parse(item.QtyOnHand),
        //                           ACMonth = DateTime.Parse("03-09-2019"),
        //                           ItemNo = item.DistItem,
        //                           ItemDesc = item.Description,
        //                           StatusCodeID = 1,
        //                           AddedByUserID = 1,
        //                           AddedDate = DateTime.Now,
        //                       };
        //                       acItem.DistStockBalances = new List<DistStockBalance>();
        //                       var stkBal = new DistStockBalance
        //                       {
        //                           Quantity = decimal.Parse(item.QtyOnHand),
        //                           StockBalMonth = DateTime.Parse("03-09-2019"),
        //                       };
        //                       acItem.DistStockBalances.Add(stkBal);
        //                       context.ACItems.Add(acItem);
        //                       context.SaveChanges();
        //                       if (acItem.DistACID > 0)
        //                       {
        //                           var navItem = context.NAVItems.FirstOrDefault(f => f.No == item.DistItem);
        //                           if (navItem != null)
        //                           {
        //                               var line = new NavItemCItemList
        //                               {
        //                                   NavItemId = navItem.ItemId,
        //                                   NavItemCustomerItemId = acItem.DistACID,
        //                               };
        //                               context.NavItemCItemLists.Add(line);
        //                               context.SaveChanges();
        //                           }
        //                       }

        //                   }

        //               }
        //           }

        //       });
        //   }
        //   static void SynApex(string pathToExcelFile)
        //   {
        //       string sheetName = "Sheet1";

        //       var excelFile = new ExcelQueryFactory(pathToExcelFile);
        //       var claimdata = (from a in excelFile.Worksheet<AnthItemExcel>(sheetName) select a).ToList();

        //       claimdata.ForEach(item =>
        //       {
        //           if (item.Item != "Grand Total:" && item.Item != "Item" && !string.IsNullOrEmpty(item.Item))
        //           {
        //               using (var context = new ACDataSyncModel())
        //               {
        //                   var exist = context.ACItems.FirstOrDefault(d => d.ItemDesc == item.Description && d.ItemNo == item.Item && d.ACMonth.Value.Month == 9);
        //                   if (exist != null)
        //                   {
        //                       var distBalc = new DistStockBalance
        //                       {
        //                           DistItemId = exist.DistACID,
        //                           Quantity = decimal.Parse(item.Qtyonhand),
        //                           StockBalMonth = DateTime.Parse("03-Oct-2019"),
        //                       };
        //                       context.DistStockBalances.Add(distBalc);
        //                   }
        //                   context.SaveChanges();
        //               }
        //           }

        //       });
        //   }
        //   //   static void SynItem(string pathToExcelFile)
        //   //   {
        //   //       string sheetName = "Item Master";

        //   //       var excelFile = new ExcelQueryFactory(pathToExcelFile);
        //   //       var claimdata = (from a in excelFile.Worksheet<ItemExcel>(sheetName) select a).ToList();

        //   //       claimdata.ForEach(item =>
        //   //       {
        //   //           using (var context = new Entities())
        //   //           {
        //   //               if (!string.IsNullOrEmpty(item.PackSize))
        //   //               {
        //   //                   var navItem = context.NAVItems.FirstOrDefault(f => f.No == item.No);
        //   //                   if (navItem != null)
        //   //                   {
        //   //                       navItem.PackSize = int.Parse(item.PackSize);
        //   //                       navItem.PackUOM = item.PackUOM;
        //   //                   }
        //   //                   context.SaveChanges();
        //   //               }

        //   //           }

        //   //       });
        //   //   }
        //   //   static void SynRecipe(string pathToExcelFile)
        //   //   {
        //   //       string sheetName = "Recipe List";

        //   //       var excelFile = new ExcelQueryFactory(pathToExcelFile);
        //   //       var claimdata = (from a in excelFile.Worksheet<RecipeExcel>(sheetName) select a).ToList();

        //   //       claimdata.ForEach(item =>
        //   //       {
        //   //           using (var context = new Entities())
        //   //           {
        //   //               if (!string.IsNullOrEmpty(item.BatchQty))
        //   //               {
        //   //                   //var navItem = context.NAVRecipes.FirstOrDefault(f => f.RecipeNo == item.RecipeNo);
        //   //                   //if (navItem != null)
        //   //                   //{
        //   //                   //    navItem.BatchQty = int.Parse(item.BatchQty);
        //   //                   //    navItem.ProdTime = (int.Parse(item.BatchQty) / 30000).ToString();
        //   //                   //}
        //   //                   //context.SaveChanges();
        //   //               }

        //   //           }

        //   //       });
        //   //   }
        //   //   static void SynGenericCode(string pathToExcelFile)
        //   //   {
        //   //       try
        //   //       {
        //   //           //string sheetName = "ItemMaster";

        //   //           //var excelFile = new ExcelQueryFactory(pathToExcelFile);
        //   //           //var claimdata = (from a in excelFile.Worksheet<ItemMaster>(sheetName) select a).ToList();

        //   //           //claimdata.ForEach(item =>
        //   //           //{
        //   //           //    using (var context = new Entities())
        //   //           //    {
        //   //           //        if (!string.IsNullOrEmpty(item.GenericCode))
        //   //           //        {
        //   //           //            var genCode = context.GenericCodes.FirstOrDefault(a => a.Code == item.GenericCode);
        //   //           //            if (genCode == null)
        //   //           //            {
        //   //           //                var genericCode = new GenericCode
        //   //           //                {
        //   //           //                    Code = item.GenericCode,
        //   //           //                    Description = item.Description,
        //   //           //                    StatusCodeID = 1,
        //   //           //                    AddedByUserID = 1,
        //   //           //                    AddedDate = DateTime.Now,
        //   //           //                };
        //   //           //                context.GenericCodes.Add(genericCode);
        //   //           //                context.SaveChanges();
        //   //           //                if (genericCode.GenericCodeId > 0)
        //   //           //                {
        //   //           //                    var itemMas = context.NAVItems.FirstOrDefault(f => f.No == item.ItemNo);
        //   //           //                    if (itemMas != null)
        //   //           //                    {
        //   //           //                        itemMas.GenericCodeId = genericCode.GenericCodeId;
        //   //           //                    }
        //   //           //                    context.SaveChanges();
        //   //           //                }
        //   //           //            }
        //   //           //            else
        //   //           //            {
        //   //           //                if (genCode.GenericCodeId > 0)
        //   //           //                {
        //   //           //                    var itemMas = context.NAVItems.FirstOrDefault(f => f.No == item.ItemNo);
        //   //           //                    if (itemMas != null)
        //   //           //                    {
        //   //           //                        itemMas.GenericCodeId = genCode.GenericCodeId;
        //   //           //                    }
        //   //           //                    context.SaveChanges();
        //   //           //                }
        //   //           //            }
        //   //           //        }

        //   //           //    }

        //   //           //});
        //   //       }
        //   //       catch (Exception ex)
        //   //       {
        //   //           Console.WriteLine(ex.Message);
        //   //           Console.Read();
        //   //       }
        //   //   }

        //   static void SynMethodCode(string pathToExcelFile)
        //   {
        //       try
        //       {
        //           string sheetName = "ItemMaster";

        //           var excelFile = new ExcelQueryFactory(pathToExcelFile);
        //           var claimdata = (from a in excelFile.Worksheet<ItemMaster>(sheetName) select a).ToList();

        //           claimdata.ForEach(item =>
        //           {
        //               using (var context = new ACDataSyncModel())
        //               {
        //                   var genericCode = new NavMethodCode();

        //                   if (!string.IsNullOrEmpty(item.MethodCode))
        //                   {
        //                       var MethodCode = item.MethodCode;
        //                       var genCode = context.NavMethodCodes.FirstOrDefault(a => a.MethodName == MethodCode);
        //                       if (genCode == null)
        //                       {
        //                           genericCode = new NavMethodCode
        //                           {
        //                               MethodName = MethodCode,
        //                               MethodDescription = item.MethodCode,
        //                           };
        //                           context.NavMethodCodes.Add(genericCode);
        //                           context.SaveChanges();
        //                       }
        //                       else
        //                       {
        //                           genericCode.MethodCodeID = genCode.MethodCodeID;
        //                       }
        //                       if (genericCode.MethodCodeID > 0)
        //                       {
        //                           var itemMas = context.NAVItems.FirstOrDefault(f => f.No == item.ItemNo);
        //                           if (itemMas != null)
        //                           {
        //                               var line = new NavMethodCodeLine
        //                               {
        //                                   ItemID = itemMas.ItemId,
        //                                   MethodCodeID = genericCode.MethodCodeID,
        //                               };
        //                               context.NavMethodCodeLines.Add(line);
        //                               context.SaveChanges();
        //                           }

        //                       }
        //                   }
        //               }
        //           });
        //       }
        //       catch (Exception ex)
        //       {
        //           Console.WriteLine(ex.Message);
        //           Console.Read();
        //       }
        //   }
        //   static void SynAcItemMap(string pathToExcelFile)
        //   {
        //       try
        //       {
        //           string sheetName = "Dist Items";

        //           var excelFile = new ExcelQueryFactory(pathToExcelFile);
        //           var claimdata = (from a in excelFile.Worksheet<ItemMaster>(sheetName) select a).ToList();

        //           claimdata.ForEach(item =>
        //           {
        //               using (var context = new ACDataSyncModel())
        //               {
        //                   if (!string.IsNullOrEmpty(item.SwItemNo))
        //                   {
        //                       if (int.Parse(item.DistACID) > 0)
        //                       {
        //                           var itemMas = context.NAVItems.FirstOrDefault(f => f.No == item.SwItemNo);
        //                           if (itemMas != null)
        //                           {
        //                               var line = new NavItemCItemList
        //                               {
        //                                   NavItemId = itemMas.ItemId,
        //                                   NavItemCustomerItemId = int.Parse(item.DistACID),
        //                               };
        //                               context.NavItemCItemLists.Add(line);
        //                           }
        //                           //if (!string.IsNullOrEmpty(item.SwItemNo2))
        //                           //{
        //                           //    var itemMas2 = context.NAVItems.FirstOrDefault(f => f.No == item.SwItemNo2);
        //                           //    if (itemMas2 != null)
        //                           //    {
        //                           //        var line = new NavItemCItemList
        //                           //        {
        //                           //            NavItemId = itemMas2.ItemId,
        //                           //            NavItemCustomerItemId = int.Parse(item.DistACID),
        //                           //        };
        //                           //        context.NavItemCItemLists.Add(line);

        //                           //    }
        //                           //}
        //                           //if (!string.IsNullOrEmpty(item.SwItemNo3))
        //                           //{
        //                           //    var itemMas3 = context.NAVItems.FirstOrDefault(f => f.No == item.SwItemNo3);
        //                           //    if (itemMas3 != null)
        //                           //    {
        //                           //        var line = new NavItemCItemList
        //                           //        {
        //                           //            NavItemId = itemMas3.ItemId,
        //                           //            NavItemCustomerItemId = int.Parse(item.DistACID),
        //                           //        };
        //                           //        context.NavItemCItemLists.Add(line);

        //                           //    }
        //                           //}
        //                           context.SaveChanges();
        //                       }
        //                   }
        //               }
        //           });
        //       }
        //       catch (Exception ex)
        //       {
        //           Console.WriteLine(ex.Message);
        //           Console.Read();
        //       }
        //   }

        //   static void SyncACItems(string pathToExcelFile)
        //   {
        //       try
        //       {
        //           string sheetName = "ACImport";

        //           var excelFile = new ExcelQueryFactory(pathToExcelFile);
        //           var claimdata = (from a in excelFile.Worksheet<ACImportModel>(sheetName) select a).ToList();
        //           var itemcard = new List<ACItem>();
        //           claimdata.ForEach(d =>
        //           {
        //               if (!string.IsNullOrEmpty(d.DistributorName))
        //               {
        //                   var custId = d.DistributorName == "Antah" ? 1 : 21;
        //                   var acMonth = DateTime.Parse("01-Sep-2019");
        //                   var itemId = -1;
        //                   using (var context = new ACDataSyncModel())
        //                   {
        //                       if (itemcard.Count == 0)
        //                           itemcard = context.ACItems.ToList();

        //                       var existACentry = context.ACEntries.FirstOrDefault(a => a.FromDate.Value.Month == acMonth.Month && a.FromDate.Value.Year == acMonth.Year && a.CustomerId == custId && a.CompanyId == 2);

        //                       if (existACentry == null)
        //                       {
        //                           var entry = new ACEntry
        //                           {
        //                               CustomerId = custId,
        //                               AddedByUserID = 1,
        //                               AddedDate = DateTime.Now,
        //                               FromDate = DateTime.Parse("01-Sep-2019"),
        //                               ToDate = DateTime.Parse("31-Dec-2020"),
        //                               StatusCodeID = 1,
        //                               CompanyId = 2,
        //                           };
        //                           var acItem = itemcard.FirstOrDefault(f => f.ItemNo == d.ItemNo && f.CompanyId == 2);
        //                           if (acItem != null)
        //                           {
        //                               var itemexist = context.NavItemCItemLists.FirstOrDefault(f => f.NavItemCustomerItemId == acItem.DistACID);
        //                               if (itemexist != null)
        //                               {
        //                                   var line = context.ACEntryLines.FirstOrDefault(f => f.ItemId == acItem.DistACID && f.ACEntryId == entry.ACEntryId);
        //                                   if (line == null)
        //                                   {
        //                                       entry.ACEntryLines = new List<ACEntryLine>();
        //                                       var entryLine = new ACEntryLine
        //                                       {
        //                                           ItemId = itemexist.NavItemId.Value,
        //                                           Quantity = !string.IsNullOrEmpty(d.ACQty) ? decimal.Parse(d.ACQty) : 0,
        //                                       };
        //                                       entry.ACEntryLines.Add(entryLine);
        //                                   }
        //                                   else
        //                                   {
        //                                       line.Quantity = !string.IsNullOrEmpty(d.ACQty) ? decimal.Parse(d.ACQty) : 0;
        //                                   }
        //                               }
        //                           }
        //                           context.ACEntries.Add(entry);
        //                       }
        //                       else
        //                       {
        //                           existACentry.ACEntryLines = new List<ACEntryLine>();
        //                           var acItem = itemcard.FirstOrDefault(f => f.ItemNo == d.ItemNo && f.CompanyId == 2);
        //                           if (acItem != null)
        //                           {
        //                               var itemexist = context.NavItemCItemLists.FirstOrDefault(f => f.NavItemCustomerItemId == acItem.DistACID);
        //                               if (itemexist != null)
        //                               {
        //                                   var line = context.ACEntryLines.FirstOrDefault(f => f.ItemId == itemexist.NavItemId.Value && f.ACEntryId == existACentry.ACEntryId);
        //                                   if (line == null)
        //                                   {
        //                                       var entryLine = new ACEntryLine
        //                                       {
        //                                           ItemId = itemexist.NavItemId.Value,
        //                                           Quantity = !string.IsNullOrEmpty(d.ACQty) ? decimal.Parse(d.ACQty) : 0,
        //                                       };
        //                                       existACentry.ACEntryLines.Add(entryLine);
        //                                   }
        //                                   else
        //                                   {
        //                                       line.Quantity = !string.IsNullOrEmpty(d.ACQty) ? decimal.Parse(d.ACQty) : 0;
        //                                   }
        //                               }
        //                           }
        //                       }
        //                       context.SaveChanges();
        //                   }
        //               }
        //           });

        //       }
        //       catch (Exception ex)
        //       {

        //       }
        //   }

        //   static void SynDistItemMap(string pathToExcelFile)
        //   {
        //       try
        //       {
        //           string sheetName = "Sheet1";

        //           var excelFile = new ExcelQueryFactory(pathToExcelFile);
        //           var claimdata = (from a in excelFile.Worksheet<ItemMaster>(sheetName) select a).ToList();

        //           claimdata.ForEach(item =>
        //           {
        //               using (var context = new ACDataSyncModel())
        //               {
        //                   //var DistACID = long.Parse(item.DistACID);
        //                   if (!string.IsNullOrEmpty(item.ItemNo))
        //                   {
        //                       var distItem = context.ACItems.FirstOrDefault(f => f.ItemNo == item.DistItem && f.CompanyId == 2);
        //                       //var mappedItem = context.NavItemCItemLists.Where(c => c.NavItemCustomerItemId == DistACID).ToList();

        //                       //context.NavItemCItemLists.RemoveRange(mappedItem);
        //                       //context.SaveChanges();
        //                       var itemMas = context.NAVItems.FirstOrDefault(f => f.No == item.ItemNo && f.CompanyId == 2);
        //                       if (distItem != null)
        //                       {

        //                           if (itemMas != null)
        //                           {
        //                               var line = new NavItemCItemList
        //                               {
        //                                   NavItemId = itemMas.ItemId,
        //                                   NavItemCustomerItemId = distItem.DistACID,
        //                               };
        //                               context.NavItemCItemLists.Add(line);
        //                               context.SaveChanges();
        //                           }
        //                       }
        //                       else
        //                       {
        //                           var dItems = new ACItem
        //                           {
        //                               DistName = "Antah Pharma Sdn Bhd",
        //                               CustomerId = 1,
        //                               CompanyId = 2,
        //                               ItemDesc = item.Description,
        //                               ItemNo = item.DistItem,
        //                               AddedByUserID = 1,
        //                               AddedDate = DateTime.Now,
        //                               NavItemCItemLists = new List<NavItemCItemList>(),
        //                           };

        //                           dItems.NavItemCItemLists.Add(new NavItemCItemList
        //                           {
        //                               NavItemId = itemMas.ItemId,
        //                               NavItemCustomerItemId = dItems.DistACID,
        //                           });
        //                           context.ACItems.Add(dItems);
        //                           context.SaveChanges();
        //                       }
        //                   }
        //               }
        //           });
        //       }
        //       catch (Exception ex)
        //       {
        //           Console.WriteLine(ex.Message);
        //           Console.Read();
        //       }
        //   }

        //   static void SynAnthaQty(string pathToExcelFile)
        //   {
        //       string sheetName = "Apex Sep Stock Balance";

        //       var excelFile = new ExcelQueryFactory(pathToExcelFile);
        //       var claimdata = (from a in excelFile.Worksheet<AnthItemExcel>(sheetName) select a).ToList();

        //       claimdata.ForEach(item =>
        //       {
        //           if (item.Item != "Grand Total:" && item.Item != "Item" && !string.IsNullOrEmpty(item.Item))
        //           {
        //               using (var context = new ACDataSyncModel())
        //               {
        //                   var exist = context.ACItems.FirstOrDefault(d => d.ItemNo == item.Item);
        //                   if (exist != null)
        //                   {
        //                       //exist.ItemNo = item.DistItem;
        //                       var qty = claimdata.Where(d => d.Item == item.Item).Sum(s => decimal.Parse(s.OnHandQty));
        //                       var stkMonth = DateTime.Parse("03-Sep-2019");
        //                       var line = context.DistStockBalances.FirstOrDefault(d => d.StockBalMonth.Value == stkMonth && d.DistItemId == exist.DistACID);
        //                       if (line == null)
        //                       {
        //                           var distBalc = new DistStockBalance
        //                           {
        //                               DistItemId = exist.DistACID,
        //                               Quantity = qty,
        //                               StockBalMonth = DateTime.Parse("03-Sep-2019"),
        //                           };
        //                           context.DistStockBalances.Add(distBalc);
        //                       }
        //                       else
        //                       {
        //                           line.Quantity = qty;
        //                       }
        //                       context.SaveChanges();
        //                   }
        //                   else
        //                   {

        //                   }

        //               }
        //           }

        //       });
        //   }


        //   static void SynInventoryItem(string pathToExcelFile)
        //   {
        //       try
        //       {
        //           string sheetName = "Sheet1";

        //           var excelFile = new ExcelQueryFactory(pathToExcelFile);
        //           var claimdata = (from a in excelFile.Worksheet<ItemMaster>(sheetName) select a).ToList();

        //           claimdata.ForEach(item =>
        //           {
        //               using (var context = new ACDataSyncModel())
        //               {
        //                   var itemcard = context.NAVItems.FirstOrDefault(f => f.No == item.No && f.CompanyId == 2);
        //                   if (itemcard != null)
        //                   {
        //                       itemcard.Inventory = decimal.Parse(item.Inventory);

        //                       itemcard.NAVItemStockBalances = new List<NAVItemStockBalance>();
        //                       var stkBalance = new NAVItemStockBalance
        //                       {
        //                           ItemId = itemcard.ItemId,
        //                           AddedByUserID = 1,
        //                           AddedDate = DateTime.Now,
        //                           StatusCodeID = 1,
        //                           Quantity = decimal.Parse(item.Inventory),
        //                           RejectQuantity = 0,
        //                           StockBalMonth = DateTime.Parse("29-Feb-2020"),
        //                       };
        //                       itemcard.NAVItemStockBalances.Add(stkBalance);
        //                       context.SaveChanges();
        //                   }


        //               }
        //           });
        //       }
        //       catch (Exception ex)
        //       {
        //           Console.WriteLine(ex.Message);
        //           Console.Read();
        //       }
        //   }

        //   static void SynGenCode(string pathToExcelFile)
        //   {
        //       try
        //       {
        //           string sheetName = "ItemMaster_SG";

        //           var excelFile = new ExcelQueryFactory(pathToExcelFile);
        //           var claimdata = (from a in excelFile.Worksheet<ItemMaster>(sheetName) select a).ToList();

        //           claimdata.ForEach(item =>
        //           {
        //               using (var context = new ACDataSyncModel())
        //               {
        //                   var itemcard = context.GenericCodes.FirstOrDefault(f => f.Code == item.GenericCode);
        //                   if (itemcard == null)
        //                   {
        //                       var gencode = new GenericCode
        //                       {
        //                           Code = item.GenericCode,
        //                           Description = item.GenericCode,
        //                           AddedByUserID = 1,
        //                           AddedDate = DateTime.Now,
        //                           StatusCodeID = 1,
        //                       };
        //                       context.GenericCodes.Add(gencode);
        //                       context.SaveChanges();
        //                   }
        //                   else
        //                   {
        //                       var itemmas = context.NAVItems.FirstOrDefault(f => f.No == item.ItemNo && f.CompanyId == 2);
        //                       if (itemmas != null)
        //                       {
        //                           itemmas.GenericCodeId = itemcard.GenericCodeId;
        //                           context.SaveChanges();
        //                       }
        //                   }


        //               }
        //           });
        //       }
        //       catch (Exception ex)
        //       {
        //           Console.WriteLine(ex.Message);
        //           Console.Read();
        //       }
        //   }
        //   static void SynMethCode(string pathToExcelFile)
        //   {
        //       try
        //       {
        //           string sheetName = "ItemMaster_SG";

        //           var excelFile = new ExcelQueryFactory(pathToExcelFile);
        //           var claimdata = (from a in excelFile.Worksheet<ItemMaster>(sheetName) select a).ToList();

        //           claimdata.ForEach(item =>
        //           {
        //               using (var context = new ACDataSyncModel())
        //               {
        //                   var itemcard = context.NavMethodCodes.FirstOrDefault(f => f.MethodName == item.MethodCode);
        //                   if (itemcard == null)
        //                   {
        //                       var gencode = new NavMethodCode
        //                       {
        //                           MethodName = item.MethodCode,
        //                           MethodDescription = item.MethodCode,
        //                           AddedByUserID = 1,
        //                           AddedDate = DateTime.Now,
        //                           StatusCodeID = 1,
        //                       };
        //                       context.NavMethodCodes.Add(gencode);
        //                       context.SaveChanges();
        //                   }
        //                   else
        //                   {
        //                       var itemmas = context.NAVItems.FirstOrDefault(f => f.No == item.ItemNo && f.CompanyId == 2);
        //                       if (itemmas != null)
        //                       {
        //                           var codeLine = new NavMethodCodeLine
        //                           {
        //                               ItemID = itemmas.ItemId,
        //                               AddedByUserID = 1,
        //                               AddedDate = DateTime.Now,
        //                               MethodCodeID = itemcard.MethodCodeID,
        //                               StatusCodeID = 1,
        //                           };
        //                           itemcard.NavMethodCodeLines.Add(codeLine);
        //                           context.SaveChanges();
        //                       }
        //                   }


        //               }
        //           });
        //       }
        //       catch (Exception ex)
        //       {
        //           Console.WriteLine(ex.Message);
        //           Console.Read();
        //       }
        //   }

        //   static void SynDistItem(string pathToExcelFile)
        //   {
        //       try
        //       {
        //           string sheetName = "Sheet1";

        //           var excelFile = new ExcelQueryFactory(pathToExcelFile);
        //           var claimdata = (from a in excelFile.Worksheet<ItemMaster>(sheetName) select a).ToList();

        //           claimdata.ForEach(item =>
        //           {
        //               using (var context = new ACDataSyncModel())
        //               {
        //                   if (!string.IsNullOrEmpty(item.DistItem))
        //                   {
        //                       var itemcard = context.ACItems.FirstOrDefault(f => f.ItemNo == item.DistItem && f.CompanyId == 2);
        //                       if (itemcard == null)
        //                       {
        //                           var gencode = new ACItem
        //                           {
        //                               ItemNo = item.DistItem,
        //                               ItemDesc = item.Description,
        //                               DistName = item.DistACID,
        //                               CompanyId = 2,
        //                               CustomerId = 21,
        //                               ACMonth = DateTime.Parse("03-09-2019"),
        //                               AddedByUserID = 1,
        //                               AddedDate = DateTime.Now,
        //                               StatusCodeID = 1,
        //                           };
        //                           gencode.DistStockBalances = new List<DistStockBalance>
        //                           {
        //                               new DistStockBalance
        //                               {
        //                                   Quantity = 0,
        //                                   StockBalMonth = DateTime.Parse("03-09-2019"),
        //                                   AddedByUserID = 1,
        //                                   AddedDate = DateTime.Now,
        //                                   StatusCodeID = 1,

        //                               }
        //                           };
        //                           context.ACItems.Add(gencode);
        //                           context.SaveChanges();
        //                       }
        //                       else
        //                       {

        //                       }
        //                   }
        //               }
        //           });
        //       }
        //       catch (Exception ex)
        //       {
        //           Console.WriteLine(ex.Message);
        //           Console.Read();
        //       }
        //   }

        //   static void SynProdOrder(string pathToExcelFile)
        //   {
        //       try
        //       {
        //           string sheetName = "Sheet1";

        //           var excelFile = new ExcelQueryFactory(pathToExcelFile);
        //           var columns = excelFile.GetColumnNames(sheetName);
        //           var claimdata = (from a in excelFile.Worksheet<ProdOrderFile>(sheetName) select a).ToList();
        //           var itemMaster = new List<NAVItem>();
        //           long itemId = -1;
        //           claimdata.ForEach(item =>
        //           {
        //               using (var context = new ACDataSyncModel())
        //               {
        //                   if (itemMaster.Count == 0)
        //                       itemMaster = context.NAVItems.ToList();

        //                   var itemNav = itemMaster.FirstOrDefault(f => f.No == item.ItemNo && f.CompanyId == 2);
        //                   if (itemNav != null)
        //                   {
        //                       itemId = itemNav.ItemId;
        //                       var prodOrder = context.ProductionSimulations.FirstOrDefault(f => f.ProdOrderNo == item.ProdOrderNo && f.ItemNo == item.ItemNo);

        //                       if (prodOrder == null)
        //                       {
        //                           prodOrder = new ProductionSimulation
        //                           {
        //                               AddedByUserID = 1,
        //                               AddedDate = DateTime.Today,
        //                               BatchNo = item.BatchNo,
        //                               Description = item.Description,
        //                               ItemNo = item.ItemNo,
        //                               PackSize = item.PackSize,
        //                               PerQtyUOM = item.PackUOM,
        //                               PerQuantity = decimal.Parse(item.TotalUnit),
        //                               ProdOrderNo = item.ProdOrderNo,
        //                               Quantity = decimal.Parse(item.Quantity),
        //                               StartingDate = DateTime.Parse(item.StartingDate),
        //                               StatusCodeID = 1,
        //                               UOM = item.UOM,
        //                               ItemID = itemId,
        //                           };
        //                           context.ProductionSimulations.Add(prodOrder);
        //                       }
        //                       else
        //                       {
        //                           prodOrder.BatchNo = item.BatchNo;
        //                           prodOrder.PackSize = item.PackSize;
        //                           prodOrder.PerQtyUOM = item.PackUOM;
        //                           prodOrder.PerQuantity = decimal.Parse(item.TotalUnit);
        //                           prodOrder.ProdOrderNo = item.ProdOrderNo;
        //                           prodOrder.Quantity = decimal.Parse(item.Quantity);
        //                           prodOrder.StartingDate = DateTime.Parse(item.StartingDate);
        //                           prodOrder.StatusCodeID = 1;
        //                           prodOrder.UOM = item.UOM;
        //                           prodOrder.ItemID = itemId;
        //                       }
        //                       context.SaveChanges();
        //                   }
        //               }
        //           });
        //       }
        //       catch (Exception ex)
        //       {
        //           Console.WriteLine(ex.Message);
        //           Console.Read();
        //       }
        //   }

    }
    class LocalClinicModel
    {
        public string Country { get; set; }
        public string CUSTNO { get; set; }
        public string COMPANY { get; set; }
        public string ADDRESS1 { get; set; }
        public string ADDRESS2 { get; set; }
        public string ZIP { get; set; }
        public string SWRepCode { get; set; }
        public string DESCRIPTION { get; set; }
    }
    class SGTender
    {
        public string DistName { get; set; }
        public string SwItemNo { get; set; }
        public string DistItem { get; set; }
        public string Description { get; set; }
        public string ACQty { get; set; }
        public string QtyOnHand { get; set; }
    }
    class ItemExcel
    {
        public string No { get; set; }
        public string Description { get; set; }
        public string PackSize { get; set; }
        public string PackUOM { get; set; }
    }
    class RecipeExcel
    {
        public string RecipeNo { get; set; }
        public string Description { get; set; }
        public string BatchQty { get; set; }
        public string ProdTime { get; set; }
    }

    class AnthItemExcel
    {
        public string No { get; set; }
        public string ItemDescription { get; set; }
        public string Qtyonhand { get; set; }
        public string QtyonSO { get; set; }

        public string Item { get; set; }
        public string SinNo { get; set; }
        public string Description { get; set; }
        public string Batch { get; set; }
        public string Expiry { get; set; }
        public string OnHandQty { get; set; }
        public string DistItem { get; set; }

        public string Avg6Month { get; set; }
    }
    class ItemMaster
    {
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string ItemGroup { get; set; }
        public string GenericCode { get; set; }
        public string MethodCode { get; set; }
        public string PackSize { get; set; }
        public string PackUOM { get; set; }
        public string DistItem { get; set; }
        public string SwItemNo { get; set; }
        public string DistACID { get; set; }
        public string SwItemNo2 { get; set; }
        public string SwItemNo3 { get; set; }
        public string No { get; set; }
        public string Inventory { get; set; }

    }
    public class ACImportModel
    {
        public string DistributorName { get; set; }
        public string ItemNo { get; set; }
        public string ItemDescription { get; set; }
        public string ACQty { get; set; }
        public string ACMonth { get; set; }
        public string QuantityOnHand { get; set; }
        public string StockBalDate { get; set; }
        public string MethodCode { get; set; }
        public string GenericCode { get; set; }
    }
    class ProdOrderFile
    {
        public string ProdOrderNo { get; set; }
        public string ItemNo { get; set; }
        public string PackSize { get; set; }
        public string Description { get; set; }
        public string Quantity { get; set; }
        public string UOM { get; set; }
        public string TotalUnit { get; set; }
        public string PackUOM { get; set; }
        public string BatchNo { get; set; }
        public string StartingDate { get; set; }
        public string TicketNo { get; set; }
    }
    class NAVProcessModel
    {
        public long ManufacturingProcessID { get; set; }

        public string PigeonHoleVersion { get; set; }
        public string Process { get; set; }
        public string NoChangeinDate { get; set; }
        public string ReplanRefNo { get; set; }
        public string ProdOrderNo { get; set; }

        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string StartingDate { get; set; }
        public string CompletionDate { get; set; }
        public string ReleaseFromPlanner { get; set; }
        public string Substatus { get; set; }
        public string Remarks { get; set; }



    }
    class SimulationAddhocModel
    {
        public string DocumentType { get; set; }
        public string ItemNo { get; set; }
        public string SelltoCustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string Catogories { get; set; }
        public string DocumentNo { get; set; }
        public string OutstandingQuantity { get; set; }
        public string PromisedDeliveryDate { get; set; }
        public string ShipmentDate { get; set; }
        public string UnitofMeasureCode { get; set; }
        public string ExternalDocumentNo { get; set; }
        public string Company { get; set; }

    }
}