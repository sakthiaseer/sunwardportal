import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import VueI18n from "vue-i18n";
Vue.config.productionTip = false;
Vue.use(VueI18n);
import VueRouter from "vue-router";
import moment from "moment";
Vue.use(moment);
Vue.use(VueRouter);
import paths from "./router/path";
const router = new VueRouter({
  base: "/",
  mode: "history",
  linkActiveClass: "active",
  routes: paths, // short for `routes: routes`
});
// messages
import messages from "./lang";
// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: "en", // set locale
  messages, // set locale messages
});
new Vue({
  vuetify,
  i18n,
  router,
  render: (h) => h(App),
}).$mount("#app");
