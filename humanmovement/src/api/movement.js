import myApi from '@/util/api'

export function getEmployees (companyName, companyNames, employeeid = 0) {

    return myApi.getAll('Employee/GetEmployeesList?companyname=' + companyName + "&&companynames=" + companyNames + "&&employeeid=" + employeeid)
}

export function getCompanyListings () {

    return myApi.getAll('CompanyListing/GetCompanyListings')
}
export function createHumanMovement (data) {
    return myApi.create(data, "HumanMovement/InsertHumanMovement")
}
export function updateHumanMovement (data) {
    return myApi.update(data.humanMovementId,data, "HumanMovement/UpdateHumanMovement")
}
export function uploadHumanDocuments (data, sessionId) {

    return myApi.uploadFile(data, "HumanMovement/UploadHumanDocuments?sessionId=" + sessionId)
}
export function getLocationCompany (data) {

    return myApi.getItem(data, "HumanMovement/GetLocationCompany")
}
export function getHumanMovementSearchBySession (sessionId, statusId) {

    return myApi.getAll("HumanMovement/GetHumanMovementSearchBySession?sessionId=" + sessionId + "&&statusId=" + statusId)
}