﻿using AttendanceSync;

using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SW.AttendanceSync
{
    class Program
    {
        static StringBuilder sb = new StringBuilder();
        static void Main(string[] args)
        {
            var filename = "Logfile_" + DateTime.Now.Ticks.ToString();
            var filePath = "";
            try
            {
                sb.AppendLine("Reading configuration file");
                var builder = new ConfigurationBuilder()
              .SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile("appsettings.json");
                sb.AppendLine("Procesing text file");
                var config = builder.Build();
                filePath = config["LogPath"];
                var result = ReadTextFile(config);
                sb.AppendLine("Webservice result : " + result);
                MoveTextFile(config);
                sb.AppendLine("Scanned shipments updated to SW Portal");
                Constant.WriteLog(filename, sb.ToString(), filePath);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Console Error : " + ex.ToString());
                sb.AppendLine("Console Error : " + ex.ToString());
                Constant.WriteLog(filename, sb.ToString(), filePath);
            }
        }
        static string ReadTextFile(IConfiguration config)
        {
            var filePath = config["FIlePath"];
            var attendanceList = new List<AttendanceMode>();
            string[] filePaths = Directory.GetFiles(filePath, "*.txt");
            filePaths.ToList().ForEach(file =>
            {
                var instance = config["Instance"];
                Constant.RestUrl = config[instance + ":restUrl"];
                Constant.Username = config[instance + ":UserName"];
                Constant.Password = config[instance + ":Password"];

                Console.WriteLine(file);
                string[] lines = File.ReadAllLines(file);
                sb.AppendLine("Contents of scanned file : " + file); ;
                System.Console.WriteLine("Contents of scanned file ");
                foreach (string line in lines.Skip(1).ToList())
                {
                    var fileData = line.Split(";").ToList();
                    if (fileData.Count > 1)
                    {
                        sb.AppendLine("Processing Scanned file : " + file);
                        var dateString = fileData[0] + "-" + fileData[1] + "-" + fileData[2] + " " + fileData[4] + ":" + fileData[5] + ":" + fileData[6];
                        DateTime myDate = DateTime.ParseExact(dateString, "yyyy-MM-dd HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);

                        attendanceList.Add(new AttendanceMode
                        {
                            SageId = fileData[3],
                            AttendenceDate = myDate,
                            CheckinDateTime = myDate,
                            CheckinTime = fileData[4] + ":" + fileData[5] + ":" + fileData[6]
                        });


                        System.Console.WriteLine("Processing Scanned file : " + file);

                    }

                }

            });

            sb.AppendLine("Post  Scanned file to Portal");
            System.Console.WriteLine("Post  Scanned file to Portal");
            RestClient restClient = new RestClient();
            var result = restClient.PostAsync<List<AttendanceMode>>("HumanMovement/AttendanceSync", attendanceList).GetAwaiter().GetResult();
            System.Console.WriteLine("Received Response from Portal");
            sb.AppendLine("Received Response from Portal");
            return "";
        }
        static void MoveTextFile(IConfiguration config)
        {
            sb.AppendLine("Moving Processed file to archive folder");
            var filePath = config["FIlePath"];
            var archivePath = config["ArchivePath"];
            string[] filePaths = Directory.GetFiles(filePath, "*.txt");
            filePaths.ToList().ForEach(file =>
            {
                string fileName = Path.GetFileName(file);
                string destFile = Path.Combine(archivePath, fileName);
                File.Move(file, destFile);

            });
        }
    }
    class AttendanceMode
    {
        public string SageId { get; set; }
        public DateTime AttendenceDate { get; set; }
        public DateTime CheckinDateTime { get; set; }
        public string CheckinTime { get; set; }
    }
}
