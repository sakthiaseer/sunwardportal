# vue3

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## changes

1. depedencies update 

```
"core-js": "^3.8.2",
    "vue": "^3.0.5",
    "vue-class-component": "^8.0.0-rc.1",
    "vue-router": "^4.0.8"
```

2. devdepedencies update

```
"@vue/cli-plugin-babel": "^4.5.10",
    "@vue/cli-plugin-eslint": "^4.5.10",
    "@vue/cli-service": "^4.5.10",
    "@vue/compiler-sfc": "^3.0.5",
    "babel-eslint": "^10.1.0",
    "eslint": "^6.8.0",
    "eslint-plugin-vue": "^7.4.1",
    "vue-loader-v16": "^16.0.0-beta.5.4"
```

3. chnage router config

vue 2 - 

```
import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

export default new VueRouter({
  mode: "history",
  routes: [
    { path: "/", component: Grid },
  ]
});
```

vue 3 -

```
import { createWebHistory, createRouter } from "vue-router";

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;

```

4. update `main.js` file as per vue 3 syntax


