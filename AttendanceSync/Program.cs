﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Linq;
using System.Text;

namespace AttendanceSync
{
   public class Program
    {
        static StringBuilder sb = new StringBuilder();
       public static void Main(string[] args)
        {
            var filename = "Logfile_" + DateTime.Now.Ticks.ToString();
            var filePath = "";
            try
            {
                sb.AppendLine("Reading configuration file");
                var builder = new ConfigurationBuilder()
              .SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile("appsettings.json");
                sb.AppendLine("Procesing text file");
                var config = builder.Build();
                filePath = config["LogPath"];
                //var result = ReadTextFile(config);
                sb.AppendLine("Webservice result : " + result);
                 // MoveTextFile(config);
                sb.AppendLine("Scanned shipments updated to acumatica");
                Constant.WriteLog(filename, sb.ToString(), filePath);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Console Error : " + ex.ToString());
                sb.AppendLine("Console Error : " + ex.ToString());
                Constant.WriteLog(filename, sb.ToString(), filePath);
            }
        }
        static string ReadTextFile(IConfiguration config)
        {
            var filePath = config["FIlePath"];
            
            string[] filePaths = Directory.GetFiles(filePath, "*.txt");
            filePaths.ToList().ForEach(file =>
            {
                var instance = config["Instance"];
                Constant.RestUrl = config[instance + ":restUrl"];
                Constant.Username = config[instance + ":UserName"];
                Constant.Password = config[instance + ":Password"];

                Console.WriteLine(file);
                string[] lines = File.ReadAllLines(file);
                sb.AppendLine("Contents of scanned file : " + file); ;
                System.Console.WriteLine("Contents of scanned file ");
                foreach (string line in lines)
                {
                    var fileData = line.Split("|").ToList();
                    if (fileData.Count > 1)
                    {
                        sb.AppendLine("Processing Scanned file : " + file);
                        System.Console.WriteLine("Processing Scanned file : " + file);
                        
                    }

                }

            });

            sb.AppendLine("Post  Scanned file to acumatica");
            System.Console.WriteLine("Post  Scanned file to acumatica");
            
            System.Console.WriteLine("Received Response from acumatica");
            sb.AppendLine("Received Response from acumatica");
            return "";
        }
        static void MoveTextFile(IConfiguration config)
        {
            sb.AppendLine("Moving Processed file to archive folder");
            var filePath = config["FIlePath"];
            var archivePath = config["ArchivePath"];
            string[] filePaths = Directory.GetFiles(filePath, "*.txt");
            filePaths.ToList().ForEach(file =>
            {
                string fileName = Path.GetFileName(file);
                string destFile = Path.Combine(archivePath, fileName);
                File.Move(file, destFile);

            });
        }
    }
}
