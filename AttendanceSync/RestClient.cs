﻿

using AttendanceSync;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CRT.TTS.Scanner
{
    public class RestClient
    {
        public async Task<List<T>> Get<T>(string method)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Constant.RestUrl);
                var response = await client.GetAsync(method);
                var result = response.Content.ReadAsStringAsync().Result;
                if (result != "")
                {
                    var Items = JsonConvert.DeserializeObject<List<T>>(result);
                    return Items;
                }

                return null;
            }
        }
        public async Task<List<T>> Get<T>(string method, long Id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Constant.RestUrl);
                var response = await client.GetAsync(method + "/" + Id);
                var result = response.Content.ReadAsStringAsync().Result;
                if (result != "")
                {
                    var Items = JsonConvert.DeserializeObject<List<T>>(result);
                    return Items;
                }

                return null;
            }
        }
        public async Task<T> GetItem<T>(string method, string name)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Constant.RestUrl);
                var response = await client.GetAsync(method + "?name=" + name);
                var result = response.Content.ReadAsStringAsync().Result;
                var Item = default(T);
                if (result != "")
                {
                    Item = JsonConvert.DeserializeObject<T>(result);
                    return Item;
                }
                return Item;
            }
        }
        public async Task<List<T>> GetItems<T>(string method, string name)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Constant.RestUrl);
                var response = await client.GetAsync(method + "?name=" + name);
                var result = response.Content.ReadAsStringAsync().Result;
                if (result != "")
                {
                    var Items = JsonConvert.DeserializeObject<List<T>>(result);
                    return Items;
                }

                return null;
            }
        }
        public async Task<T> GetItem<T>(string method, long Id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Constant.RestUrl);
                var response = await client.GetAsync(method + "/" + Id);
                var result = response.Content.ReadAsStringAsync().Result;
                var Item = default(T);
                if (result != "")
                {
                    Item = JsonConvert.DeserializeObject<T>(result);
                    return Item;
                }
                return Item;
            }
        }
        public async Task<string> PostAsync<T>(string method, T item)
        {
            //var credentials = new NetworkCredential(Constant.Username, Constant.Password);
            //var handler = new HttpClientHandler { Credentials = credentials };
            using (var client = new HttpClient())
            {

                //client.Timeout = TimeSpan.Parse(Constant.Timeout);
                client.BaseAddress = new Uri(Constant.RestUrl);
                var json = JsonConvert.SerializeObject(item);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = null;
                response = await client.PostAsync(method, content); 

                return await response.Content.ReadAsStringAsync();
            }
        }


        public async Task<bool> PostMediaAsync<T>(string method, T item)
        {
            using (var stream = new MemoryStream())
            using (var bson = new BsonWriter(stream))
            {
                var jsonSerializer = new JsonSerializer();



                jsonSerializer.Serialize(bson, item);

                using (var client = new HttpClient
                {
                    BaseAddress = new Uri(Constant.RestUrl)
                })
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                            new MediaTypeWithQualityHeaderValue("application/bson"));

                    var byteArrayContent = new ByteArrayContent(stream.ToArray());
                    byteArrayContent.Headers.ContentType = new MediaTypeHeaderValue("application/bson");

                    var result = await client.PostAsync(
                            method, byteArrayContent);

                    return result.EnsureSuccessStatusCode().IsSuccessStatusCode;
                }
            }
        }
        public async Task<bool> PutAsync<T>(string method, T item)
        {

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Constant.RestUrl);
                client.DefaultRequestHeaders.Add("ContentType", "application/json");
                //This is the key section you were missing    
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(Constant.Username + ":" + Constant.Password);
                string val = System.Convert.ToBase64String(plainTextBytes);
                client.DefaultRequestHeaders.Add("Authorization", "Basic " + val);
                var json = JsonConvert.SerializeObject(item);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = null;
                response = await client.PutAsync(method, content);

                return response.IsSuccessStatusCode;
            }
        }
        public async Task<bool> DeleteAsync<T>(string method, long Id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Constant.RestUrl);
                HttpResponseMessage response = null;
                response = await client.DeleteAsync(method + "/" + Id);

                return response.IsSuccessStatusCode;
            }
        }

        public Exception CreateExceptionFromResponseErrors(HttpResponseMessage response)
        {
            var httpErrorObject = response.Content.ReadAsStringAsync().Result;

            // Create an anonymous object to use as the template for deserialization:
            var anonymousErrorObject =
                new { message = "", ModelState = new Dictionary<string, string[]>() };

            // Deserialize:
            var deserializedErrorObject =
                JsonConvert.DeserializeAnonymousType(httpErrorObject, anonymousErrorObject);

            // Now wrap into an exception which best fullfills the needs of your application:
            var ex = new Exception();

            // Sometimes, there may be Model Errors:
            if (deserializedErrorObject.ModelState != null)
            {
                var errors =
                    deserializedErrorObject.ModelState
                                            .Select(kvp => string.Join(". ", kvp.Value));
                for (int i = 0; i < errors.Count(); i++)
                {
                    // Wrap the errors up into the base Exception.Data Dictionary:
                    ex.Data.Add(i, errors.ElementAt(i));
                }
            }
            // Othertimes, there may not be Model Errors:
            else
            {
                var error =
                    JsonConvert.DeserializeObject<Dictionary<string, string>>(httpErrorObject);
                foreach (var kvp in error)
                {
                    // Wrap the errors up into the base Exception.Data Dictionary:
                    ex.Data.Add(kvp.Key, kvp.Value);
                }
            }
            return ex;
        }

    }

}
