﻿using JBNAV;
using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace NAVSync
{
    class Program
    {
        public static JBNAV.NAV JBContext { get; private set; }
        public static SGNAV.NAV SGContext { get; private set; }
        static void Main(string[] args)
        {
            SGContext = new SGNAV.NAV(new Uri($"http://sgnav.sunwardpharma.com:7548/DynamicsNAV100/OData/Company('SWSG%20LIVE')"))
            {
                Credentials = new NetworkCredential("mobile.app","P@ssw0rd")
            };

            JBContext = new JBNAV.NAV(new Uri($"http://jbnav.sunwardpharma.com:7548/DynamicsNAV100/OData/Company('SWSG%20LIVE')"))
            {
                Credentials = new NetworkCredential("mobile.app", "P@ssw0rd")
            };

        }

        static async void SyncNAVData()
        {
            int pageSize = 1000;
            int page = 0;
            while (true)
            {
                var nquery = SGContext.ItemPicture.Skip(page * pageSize).Take(pageSize);
                DataServiceQuery<SGNAV.ItemPicture> query = (DataServiceQuery<SGNAV.ItemPicture>)nquery;
                TaskFactory<IEnumerable<SGNAV.ItemPicture>> taskFactory = new TaskFactory<IEnumerable<SGNAV.ItemPicture>>();
                IEnumerable<SGNAV.ItemPicture> SGitemPictures = await taskFactory.FromAsync(query.BeginExecute(null, null), iar => query.EndExecute(iar));
                var SGItems = SGitemPictures.ToList();

                SGItems.ForEach(async f => 
                {

                    var jbnquery = JBContext.ItemPicture.Where(w=>w.No ==f.No);
                    DataServiceQuery<ItemPicture> jbquery = (DataServiceQuery<ItemPicture>)jbnquery;
                    TaskFactory<IEnumerable<ItemPicture>> jbtaskFactory = new TaskFactory<IEnumerable<ItemPicture>>();
                    IEnumerable<ItemPicture> itemPictures = await jbtaskFactory.FromAsync(query.BeginExecute(null, null), iar => jbquery.EndExecute(iar));

                    var updateItem = itemPictures.FirstOrDefault();

                   
                    JBContext.UpdateObject(updateItem);
                });

                if (SGItems.Count < 1000)
                    break;
                page++;

            }
        }
    }
}
