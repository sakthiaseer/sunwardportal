
export default [
    {
        path: "/",
        meta: {},
        name: "Root",
        redirect: {
            name: "login",
        },
    },
    {
        path: '/login',
        name: 'login',
        component: () =>
            import(
                '../components/Login.vue'
            ),
    },
    {
        path: "/login",
        meta: {
            public: true,
        },
        name: "SignOut",
        component: () =>
            import(
                '../components/Login.vue'
            ),
    },
    {
        path: '/survey',
        name: 'survey',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "Survey" */ '../components/Survey.vue')
    }
]