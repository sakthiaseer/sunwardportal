import myApi from '@/util/api'
export function authenticate(data) {
    return myApi.getItem(data, "UserAuth/Login")
}