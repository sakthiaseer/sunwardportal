import myApi from '@/util/api'
export function getLocalClinic () {
    return myApi.getAll("LocalClinic/GetLocalClinic")
}
export function getProductGroupSurvey () {
    return myApi.getAll("ProductGroupSurvey/GetProductGroupSurvey")
}
export function getSalesSurveyByFieldForce () {
    return myApi.getAll("SalesSurveyByFieldForce/GetSalesSurveyByFieldForce")
}
export function getSalesSurveyByFieldForceBySalesPersonId (id) {
    return myApi.getByID(id, "SalesSurveyByFieldForce/GetSalesSurveyByFieldForceBySalesPerson")
}
export function createSalesSurveyByFieldForce (data) {
    return myApi.create(data, "SalesSurveyByFieldForce/InsertSalesSurveyByFieldForce")
}

export function updateSalesSurveyByFieldForce (data) {

    return myApi.update(data.salesSurveyByFieldForceId, data, "SalesSurveyByFieldForce/UpdateSalesSurveyByFieldForce")
}
export function deleteSalesSurveyByFieldForce (data) {

    return myApi.delete(data.salesSurveyByFieldForceId, 'SalesSurveyByFieldForce/DeleteSalesSurveyByFieldForce')
}
export function getSalesSurveyByFieldForceById (id) {
    return myApi.getByID(id, "SalesSurveyByFieldForce/GetSalesSurveyByFieldForceLine")
}
export function createSalesSurveyByFieldForceLine (data) {
    return myApi.create(data, "SalesSurveyByFieldForce/InsertSalesSurveyByFieldForceLine")
}

export function updateSalesSurveyByFieldForceLine (data) {

    return myApi.update(data.salesSurveyByFieldForceLineId, data, "SalesSurveyByFieldForce/UpdateSalesSurveyByFieldForceLine")
}
export function deleteSalesSurveyByFieldForceLine (data) {

    return myApi.delete(data.salesSurveyByFieldForceLineId, 'SalesSurveyByFieldForce/DeleteSalesSurveyByFieldForceLine')
}
export function getLocationAddess (dataURL) {
    return myApi.getLocationAddess(dataURL);
}