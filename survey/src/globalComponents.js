import DeleteConfirmationDialog from "@/components/DeleteConfirmationDialog/DeleteConfirmationDialog";
import AppSectionLoader from "@/components/AppSectionLoader/AppSectionLoader";
const GlobalComponents = {
    install(Vue) {
        Vue.component('deleteConfirmationDialog', DeleteConfirmationDialog);
        Vue.component('appSectionLoader', AppSectionLoader);
    }
};
export default GlobalComponents;