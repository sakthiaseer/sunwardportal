

import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'
const userName = 'User-Name'
const userID = 'USER-ID'
const theme = 'USER-Theme'
const pageSize = 'USER-PAGE'

export function getToken() {
    return localStorage.getItem(TokenKey)
}
export function getuserName() {
    return localStorage.getItem(userName)
}
export function getuserID() {
    return localStorage.getItem(userID)
}
export function setToken(token) {
    return localStorage.setItem(TokenKey, token)
}
export function setuserName(uName) {
    return localStorage.setItem(userName, uName)
}
export function setuserID(token) {
    return localStorage.setItem(userID, token)
}
export function removeToken() {
    return localStorage.removeItem(TokenKey)
}
export function setTheme(color) {
    return localStorage.setItem(theme, color)
}
export function getTheme() {
    return localStorage.getItem(theme)
}
export function setPage(page) {
    return localStorage.setItem(pageSize, page)
}
export function getPage() {
    return localStorage.getItem(pageSize)
}