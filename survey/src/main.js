import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
import moment from "moment";

Vue.use(moment);
import VueHotkey from 'v-hotkey'
 
import VueGeolocation from 'vue-browser-geolocation';
Vue.use(VueGeolocation);

Vue.use(VueHotkey)
import store from './store';
Vue.config.productionTip = false
Vue.filter('formatDate', function (value) {
  if (value) {
    return moment(value).format('DD-MMM-YYYY');
  }
});
Vue.filter('formatDateTime', function (value) {
  if (value) {
    return moment(value).format('DD-MMM-YYYY hh:mm A');
  }
});
import GlobalComponents from './globalComponents';
Vue.use(GlobalComponents);
Vue.prototype.$eventHub = new Vue(); // Global event bus
new Vue({
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
