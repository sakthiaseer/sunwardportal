import {
    AUTH_REQUEST,
    AUTH_ERROR,
    AUTH_SUCCESS,
    AUTH_LOGOUT,
    AUTH_ASSIGN,
} from '@/store/actions/auth';
export default [{
    name: 'APP_LOGIN_SUCCESS',
    callback: function (e) {
        this.snackbar = {
            show: true,
            color: 'success',
            text: 'Welcome to SW Portal.'
        };

        this.$router.push({
            path: '/survey'
        });
    }
},
{
    name: 'APP_LOGOUT',
    callback: function (e) {
        // this.snackbar = {
        //   show: true,
        //   color: 'green',
        //   text: 'Logout successfully.'
        // };
        this.$store.dispatch(AUTH_LOGOUT).then(() => {

        }).catch(error => {
            window.getApp.$emit('APP_API_ERROR', 'Auth Assign Error');
        });

        this.$router.replace({
            path: '/login'
        });
    }
},
{
    name: 'APP_API_SUCCESS',
    callback: function (e) {

        this.snackbar = {
            show: true,
            color: 'success',
            text: e
        };
    }
},
{
    name: 'APP_API_ERROR',
    callback: function (e) {

        this.snackbar = {
            show: true,
            color: 'red',
            text: e
        };
    }
},
{
    name: 'APP_API_VAL',
    callback: function (e) {
        console.log(e);
        this.snackbar = {
            show: true,
            color: 'orange',
            text: e
        };
    }
},
{
    name: 'APP_PAGE_LOADED',
    callback: function (e) { }
},
{
    name: 'APP_AUTH_FAILED',
    callback: function (e) {
        this.$router.push('/login');
        this.$message.error('Token has expired');
    }
},
{
    name: 'APP_BAD_REQUEST',
    // @error api response data
    callback: function (msg) {
        this.$message.error(msg);
    }
},
{
    name: 'APP_ACCESS_DENIED',
    // @error api response data
    callback: function (msg) {
        this.$message.error(msg);
        this.$router.push('/forbidden');
    }
},
{
    name: 'APP_RESOURCE_DELETED',
    // @error api response data
    callback: function (msg) {
        this.$message.success(msg);
    }
},
{
    name: 'APP_RESOURCE_UPDATED',
    // @error api response data
    callback: function (msg) {
        this.$message.success(msg);
    }
},

];