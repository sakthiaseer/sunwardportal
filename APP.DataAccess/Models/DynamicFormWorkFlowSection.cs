﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormWorkFlowSection
    {
        public DynamicFormWorkFlowSection()
        {
            DynamicFormWorkFlowForm = new HashSet<DynamicFormWorkFlowForm>();
        }

        public long DynamicFormWorkFlowSectionId { get; set; }
        public long? DynamicFormSectionId { get; set; }
        public long? DynamicFormWorkFlowId { get; set; }
        public long? DynamicFormDataId { get; set; }

        public virtual DynamicFormData DynamicFormData { get; set; }
        public virtual DynamicFormSection DynamicFormSection { get; set; }
        public virtual DynamicFormWorkFlow DynamicFormWorkFlow { get; set; }
        public virtual ICollection<DynamicFormWorkFlowForm> DynamicFormWorkFlowForm { get; set; }
    }
}
