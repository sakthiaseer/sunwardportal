﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CatalogCalibrationService
    {
        public long CatalogCalibrationServiceId { get; set; }
        public long? DeviceCatalogMasterId { get; set; }
        public long? CalibrationServiceInfoId { get; set; }

        public virtual DeviceCatalogMaster DeviceCatalogMaster { get; set; }
    }
}
