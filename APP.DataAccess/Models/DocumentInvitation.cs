﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DocumentInvitation
    {
        public long DocumentFolderInvitationId { get; set; }
        public long? DocumentId { get; set; }
        public long? UserId { get; set; }
        public long? FolderId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Documents Document { get; set; }
        public virtual Folders Folder { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
