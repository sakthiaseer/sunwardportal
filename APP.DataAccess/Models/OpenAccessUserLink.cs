﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class OpenAccessUserLink
    {
        public long OpenAccessUserLinkId { get; set; }
        public long? OpenAccessUserId { get; set; }
        public long? UserId { get; set; }
        public long? UserGroupId { get; set; }
        public long? LevelId { get; set; }
        public bool? IsDmsAccess { get; set; }
        public bool? IsDmsCreateMainFolder { get; set; }
        public bool? IsAdd { get; set; }
        public bool? IsEdit { get; set; }
        public bool? IsDelete { get; set; }
        public bool? IsPermission { get; set; }

        public virtual LevelMaster Level { get; set; }
        public virtual OpenAccessUser OpenAccessUser { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual UserGroup UserGroup { get; set; }
    }
}
