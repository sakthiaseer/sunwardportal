﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ExcipientInformationLineByProcess
    {
        public long ExcipientInformationLineByProcessId { get; set; }
        public long? PerUnitFormulationLineId { get; set; }
        public long? MaterialNameId { get; set; }
        public long? MaterialFunctionId { get; set; }
        public string Dosage { get; set; }
        public long? DosageInformationDosageUnitsId { get; set; }
        public long? PerDosageId { get; set; }
        public string Overage { get; set; }
        public long? OverageUnitsId { get; set; }
        public long? OverageInformationDosageUnitsId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? NavisionSpecificationId { get; set; }
        public long? TypeOfIngredientId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ItemClassificationHeader MaterialName { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual NavisionSpecification NavisionSpecification { get; set; }
        public virtual PerUnitFormulationLine PerUnitFormulationLine { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
