﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionActivityRoutineAppLineQaChecker
    {
        public long ProductionActivityRoutineAppLineQaCheckerId { get; set; }
        public long? ProductionActivityRoutineAppLineId { get; set; }
        public bool? QaCheck { get; set; }
        public long? QaCheckUserId { get; set; }
        public DateTime? QaCheckDate { get; set; }

        public virtual ProductionActivityRoutineAppLine ProductionActivityRoutineAppLine { get; set; }
        public virtual ApplicationUser QaCheckUser { get; set; }
    }
}
