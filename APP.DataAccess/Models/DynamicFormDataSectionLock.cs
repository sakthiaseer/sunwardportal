﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormDataSectionLock
    {
        public long DynamicFormDataSectionLockId { get; set; }
        public long? DynamicFormDataId { get; set; }
        public long? DynamicFormSectionId { get; set; }
        public bool? IsLocked { get; set; }
        public long? LockedUserId { get; set; }

        public virtual DynamicFormData DynamicFormData { get; set; }
        public virtual DynamicFormSection DynamicFormSection { get; set; }
        public virtual ApplicationUser LockedUser { get; set; }
    }
}
