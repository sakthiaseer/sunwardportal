﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ToDoNotesHistory
    {
        public long Id { get; set; }
        public long NotesId { get; set; }
        public string Description { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? RemainDate { get; set; }
        public int? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public int? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public bool? Completed { get; set; }
        public long? TopicId { get; set; }
        public string Status { get; set; }
        public string ColourCode { get; set; }
        public long? Users { get; set; }
        public string Url { get; set; }
    }
}
