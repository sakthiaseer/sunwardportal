﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class JobProgressTemplateNotify
    {
        public long JobProgressTemplateNotifyId { get; set; }
        public long? JobProgressTemplateId { get; set; }
        public string UserType { get; set; }
        public long? EmployeeId { get; set; }
        public long? UserGroupId { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual JobProgressTemplate JobProgressTemplate { get; set; }
        public virtual UserGroup UserGroup { get; set; }
    }
}
