﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductGroupingNavDifference
    {
        public long ProductGroupingNavDifferenceId { get; set; }
        public long? ProductGroupingNavId { get; set; }
        public long? TypeOfDifferenceId { get; set; }
        public string DifferenceInfo { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProductGroupingNav ProductGroupingNav { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
