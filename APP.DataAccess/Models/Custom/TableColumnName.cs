﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.DataAccess.Models
{
    public class TableColumnName
    {
        public string EntityName { get; set; }
        public string ColumnName { get; set; }
    }
}
