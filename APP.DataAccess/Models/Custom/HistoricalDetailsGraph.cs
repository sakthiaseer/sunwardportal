﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.DataAccess.Models
{
    public class HistoricalDetailsGraph
    {
        public string Date { get; set; }
        public int? TotalSales { get; set; }
        public decimal? Qty { get; set; }
        public decimal? Bouns { get; set; }
        public decimal? Nett { get; set; }
        public decimal? Gross { get; set; }
    }
}
