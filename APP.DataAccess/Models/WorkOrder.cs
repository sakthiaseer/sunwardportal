﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WorkOrder
    {
        public WorkOrder()
        {
            WorkOrderLine = new HashSet<WorkOrderLine>();
        }

        public long WorkOrderId { get; set; }
        public int? AssignmentId { get; set; }
        public long? RequirementId { get; set; }
        public long? ModuleId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public int? TypeId { get; set; }
        public long? AssignTo { get; set; }
        public long? PermissionId { get; set; }
        public long? UserGroupId { get; set; }
        public long? NavisionModuleId { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser AssignToNavigation { get; set; }
        public virtual CodeMaster Assignment { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationMasterDetail Module { get; set; }
        public virtual ApplicationMasterDetail NavisionModule { get; set; }
        public virtual ApplicationPermission Permission { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual ApplicationMasterDetail Requirement { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual UserGroup UserGroup { get; set; }
        public virtual ICollection<WorkOrderLine> WorkOrderLine { get; set; }
    }
}
