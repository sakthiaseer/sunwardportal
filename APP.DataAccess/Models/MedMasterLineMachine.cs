﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class MedMasterLineMachine
    {
        public long MedMasterLineMachineId { get; set; }
        public long? MedMasterLineId { get; set; }
        public long? MachineId { get; set; }

        public virtual CommonFieldsProductionMachine Machine { get; set; }
        public virtual MedMasterLine MedMasterLine { get; set; }
    }
}
