﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class JobSchedule
    {
        public JobSchedule()
        {
            JobScheduleWeekly = new HashSet<JobScheduleWeekly>();
        }

        public long JobScheduleId { get; set; }
        public int? FrequencyId { get; set; }
        public DateTime? StartDate { get; set; }
        public TimeSpan? StartTime { get; set; }
        public DateTime? EndDate { get; set; }
        public int? MonthlyDay { get; set; }
        public bool? DaysOfWeek { get; set; }
        public long? CompanyId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public long? JobScheduleFunUniqueId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual CodeMaster Frequency { get; set; }
        public virtual JobScheduleFun JobScheduleFunUnique { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<JobScheduleWeekly> JobScheduleWeekly { get; set; }
    }
}
