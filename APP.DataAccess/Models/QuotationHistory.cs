﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class QuotationHistory
    {
        public QuotationHistory()
        {
            QuotationAward = new HashSet<QuotationAward>();
            QuotationHistoryLine = new HashSet<QuotationHistoryLine>();
        }

        public long QuotationHistoryId { get; set; }
        public long? CompanyId { get; set; }
        public string SwreferenceNo { get; set; }
        public DateTime? Date { get; set; }
        public long? CustomerId { get; set; }
        public string CustomerRefNo { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<QuotationAward> QuotationAward { get; set; }
        public virtual ICollection<QuotationHistoryLine> QuotationHistoryLine { get; set; }
    }
}
