﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class StaffTraining
    {
        public long StaffTrainingId { get; set; }
        public long? PageId { get; set; }
        public long? StaffId { get; set; }
        public long? TrainnerId { get; set; }
        public DateTime? TrainingDate { get; set; }
        public string TestResult { get; set; }
        public int? TrainingFrequency { get; set; }
        public decimal? Score { get; set; }
        public decimal? Version { get; set; }
        public string Type { get; set; }
        public string SessionId { get; set; }

        public virtual WikiPage Page { get; set; }
        public virtual ApplicationUser Staff { get; set; }
        public virtual ApplicationUser Trainner { get; set; }
    }
}
