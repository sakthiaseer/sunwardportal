﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class GroupTicketPlaningReport
    {
        public long GroupPlaningId { get; set; }
        public long? CompanyId { get; set; }
        public string ItemNo { get; set; }
        public long? ItemId { get; set; }
        public int? ReportYear { get; set; }
        public int? ReportMonth { get; set; }
        public decimal? Ahqty { get; set; }
        public decimal? Ahmonth { get; set; }
        public decimal? PdtMonth { get; set; }
        public decimal? PdtQty { get; set; }
        public decimal? ProjHs { get; set; }
        public decimal? NoOfTickets { get; set; }
        public int? AhnoOfMonths { get; set; }
        public string VersionName { get; set; }
        public Guid? SessionId { get; set; }
    }
}
