﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TagMaster
    {
        public TagMaster()
        {
            TaskTag = new HashSet<TaskTag>();
        }

        public long TagMasterId { get; set; }
        public string Name { get; set; }
        public Guid? SessionId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<TaskTag> TaskTag { get; set; }
    }
}
