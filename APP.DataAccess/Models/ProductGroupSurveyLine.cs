﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductGroupSurveyLine
    {
        public long ProductGroupSurveyLineId { get; set; }
        public long? ProductGroupSurveyId { get; set; }
        public long? ProductGroupId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProductGroupSurvey ProductGroupSurvey { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
