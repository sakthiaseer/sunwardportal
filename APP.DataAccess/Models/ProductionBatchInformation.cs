﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionBatchInformation
    {
        public ProductionBatchInformation()
        {
            ProductionBatchInformationLine = new HashSet<ProductionBatchInformationLine>();
        }

        public long ProductionBatchInformationId { get; set; }
        public long? PlantId { get; set; }
        public string TicketNo { get; set; }
        public string ProductionOrderNo { get; set; }
        public DateTime? ManufacturingStartDate { get; set; }
        public long? BatchSizeId { get; set; }
        public long? UnitsofBatchSize { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsBmr { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Plant Plant { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ProductionBatchInformationLine> ProductionBatchInformationLine { get; set; }
    }
}
