﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ClassificationBmr
    {
        public ClassificationBmr()
        {
            ClassificationBmrLine = new HashSet<ClassificationBmrLine>();
        }

        public long ClassificationBmrId { get; set; }
        public long? CompanyId { get; set; }
        public long? NavItemId { get; set; }
        public DateTime? ManufacturingStartDate { get; set; }
        public string BatchSize { get; set; }
        public long? TypeOfProductionId { get; set; }
        public int? ProductionStatusId { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public long? ProfileId { get; set; }
        public string ProdOrderNo { get; set; }
        public Guid? SessionId { get; set; }
        public DateTime? ProductionSimulationDate { get; set; }
        public bool? IsProductionBatch { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ItemClassificationMaster ItemClassificationMaster { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Navitems NavItem { get; set; }
        public virtual CodeMaster ProductionStatus { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ClassificationBmrLine> ClassificationBmrLine { get; set; }
    }
}
