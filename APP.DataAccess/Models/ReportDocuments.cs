﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ReportDocuments
    {
        public long ReportDocumentId { get; set; }
        public string FileName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid SessionId { get; set; }
    }
}
