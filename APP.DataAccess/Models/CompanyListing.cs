﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CompanyListing
    {
        public CompanyListing()
        {
            BlanketOrder = new HashSet<BlanketOrder>();
            BlisterMouldInformation = new HashSet<BlisterMouldInformation>();
            CommonMasterMouldChangeParts = new HashSet<CommonMasterMouldChangeParts>();
            CompanyListingCompanyType = new HashSet<CompanyListingCompanyType>();
            CompanyListingCustomerCode = new HashSet<CompanyListingCustomerCode>();
            CompanyListingLine = new HashSet<CompanyListingLine>();
            ContractDistributionSalesEntryLine = new HashSet<ContractDistributionSalesEntryLine>();
            DistributorPricing = new HashSet<DistributorPricing>();
            FinishProductGeneralInfo = new HashSet<FinishProductGeneralInfo>();
            GenericCodeSupplyToMultiple = new HashSet<GenericCodeSupplyToMultiple>();
            GenericCodes = new HashSet<GenericCodes>();
            HandlingOfShipmentSpecificCustomer = new HashSet<HandlingOfShipmentSpecificCustomer>();
            HistoricalDetails = new HashSet<HistoricalDetails>();
            HrexternalPersonal = new HashSet<HrexternalPersonal>();
            HumanMovement = new HashSet<HumanMovement>();
            InverseLinkNonTransactionCompany = new HashSet<CompanyListing>();
            MarginInformation = new HashSet<MarginInformation>();
            MasterBlanketOrder = new HashSet<MasterBlanketOrder>();
            MasterInterCompanyPricingLine = new HashSet<MasterInterCompanyPricingLine>();
            NovateInformationLineNovateFrom = new HashSet<NovateInformationLine>();
            NovateInformationLineNovateTo = new HashSet<NovateInformationLine>();
            ProductGrouping = new HashSet<ProductGrouping>();
            ProductGroupingManufactureManufactureBy = new HashSet<ProductGroupingManufacture>();
            ProductGroupingManufactureSupplyTo = new HashSet<ProductGroupingManufacture>();
            PurchaseOrder = new HashSet<PurchaseOrder>();
            RequestAc = new HashSet<RequestAc>();
            SalesAdhocNovateFromCustomer = new HashSet<SalesAdhocNovate>();
            SalesAdhocNovateToCustomer = new HashSet<SalesAdhocNovate>();
            SalesBorrowHeaderBorrowFrom = new HashSet<SalesBorrowHeader>();
            SalesBorrowHeaderCustomerToSend = new HashSet<SalesBorrowHeader>();
            SalesOrder = new HashSet<SalesOrder>();
            SalesOrderEntryCustomer = new HashSet<SalesOrderEntry>();
            SalesOrderEntryTagAlongCustomer = new HashSet<SalesOrderEntry>();
            SampleRequestForDoctorHeader = new HashSet<SampleRequestForDoctorHeader>();
            ScheduleOfDelivery = new HashSet<ScheduleOfDelivery>();
            SelfTest = new HashSet<SelfTest>();
            SellingCatalogue = new HashSet<SellingCatalogue>();
            SocustomersIssueBuyingThrough = new HashSet<SocustomersIssue>();
            SocustomersIssueCentralizedWith = new HashSet<SocustomersIssue>();
            SocustomersIssueTenderAgency = new HashSet<SocustomersIssue>();
            TenderInformation = new HashSet<TenderInformation>();
            TenderPeriodPricing = new HashSet<TenderPeriodPricing>();
            TransferBalanceQty = new HashSet<TransferBalanceQty>();
        }

        public long CompanyListingId { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public long? ProfileId { get; set; }
        public string No { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string CompanyName { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? CustomerCodeId { get; set; }
        public Guid? SessionId { get; set; }
        public bool? IsNonTransaction { get; set; }
        public long? LinkNonTransactionCompanyId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ItemClassificationMaster ItemClassificationMaster { get; set; }
        public virtual CompanyListing LinkNonTransactionCompany { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<BlanketOrder> BlanketOrder { get; set; }
        public virtual ICollection<BlisterMouldInformation> BlisterMouldInformation { get; set; }
        public virtual ICollection<CommonMasterMouldChangeParts> CommonMasterMouldChangeParts { get; set; }
        public virtual ICollection<CompanyListingCompanyType> CompanyListingCompanyType { get; set; }
        public virtual ICollection<CompanyListingCustomerCode> CompanyListingCustomerCode { get; set; }
        public virtual ICollection<CompanyListingLine> CompanyListingLine { get; set; }
        public virtual ICollection<ContractDistributionSalesEntryLine> ContractDistributionSalesEntryLine { get; set; }
        public virtual ICollection<DistributorPricing> DistributorPricing { get; set; }
        public virtual ICollection<FinishProductGeneralInfo> FinishProductGeneralInfo { get; set; }
        public virtual ICollection<GenericCodeSupplyToMultiple> GenericCodeSupplyToMultiple { get; set; }
        public virtual ICollection<GenericCodes> GenericCodes { get; set; }
        public virtual ICollection<HandlingOfShipmentSpecificCustomer> HandlingOfShipmentSpecificCustomer { get; set; }
        public virtual ICollection<HistoricalDetails> HistoricalDetails { get; set; }
        public virtual ICollection<HrexternalPersonal> HrexternalPersonal { get; set; }
        public virtual ICollection<HumanMovement> HumanMovement { get; set; }
        public virtual ICollection<CompanyListing> InverseLinkNonTransactionCompany { get; set; }
        public virtual ICollection<MarginInformation> MarginInformation { get; set; }
        public virtual ICollection<MasterBlanketOrder> MasterBlanketOrder { get; set; }
        public virtual ICollection<MasterInterCompanyPricingLine> MasterInterCompanyPricingLine { get; set; }
        public virtual ICollection<NovateInformationLine> NovateInformationLineNovateFrom { get; set; }
        public virtual ICollection<NovateInformationLine> NovateInformationLineNovateTo { get; set; }
        public virtual ICollection<ProductGrouping> ProductGrouping { get; set; }
        public virtual ICollection<ProductGroupingManufacture> ProductGroupingManufactureManufactureBy { get; set; }
        public virtual ICollection<ProductGroupingManufacture> ProductGroupingManufactureSupplyTo { get; set; }
        public virtual ICollection<PurchaseOrder> PurchaseOrder { get; set; }
        public virtual ICollection<RequestAc> RequestAc { get; set; }
        public virtual ICollection<SalesAdhocNovate> SalesAdhocNovateFromCustomer { get; set; }
        public virtual ICollection<SalesAdhocNovate> SalesAdhocNovateToCustomer { get; set; }
        public virtual ICollection<SalesBorrowHeader> SalesBorrowHeaderBorrowFrom { get; set; }
        public virtual ICollection<SalesBorrowHeader> SalesBorrowHeaderCustomerToSend { get; set; }
        public virtual ICollection<SalesOrder> SalesOrder { get; set; }
        public virtual ICollection<SalesOrderEntry> SalesOrderEntryCustomer { get; set; }
        public virtual ICollection<SalesOrderEntry> SalesOrderEntryTagAlongCustomer { get; set; }
        public virtual ICollection<SampleRequestForDoctorHeader> SampleRequestForDoctorHeader { get; set; }
        public virtual ICollection<ScheduleOfDelivery> ScheduleOfDelivery { get; set; }
        public virtual ICollection<SelfTest> SelfTest { get; set; }
        public virtual ICollection<SellingCatalogue> SellingCatalogue { get; set; }
        public virtual ICollection<SocustomersIssue> SocustomersIssueBuyingThrough { get; set; }
        public virtual ICollection<SocustomersIssue> SocustomersIssueCentralizedWith { get; set; }
        public virtual ICollection<SocustomersIssue> SocustomersIssueTenderAgency { get; set; }
        public virtual ICollection<TenderInformation> TenderInformation { get; set; }
        public virtual ICollection<TenderPeriodPricing> TenderPeriodPricing { get; set; }
        public virtual ICollection<TransferBalanceQty> TransferBalanceQty { get; set; }
    }
}
