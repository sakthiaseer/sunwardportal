﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DispensedMeterial
    {
        public long DispensedMeterialId { get; set; }
        public string MaterialName { get; set; }
        public string Qcreference { get; set; }
        public string ProductionOrderNo { get; set; }
        public string Description { get; set; }
        public string BatchNo { get; set; }
        public string SubLotNo { get; set; }
        public string TareWeight { get; set; }
        public string ActualWeight { get; set; }
        public long? Uom { get; set; }
        public string PrintLabel { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
    }
}
