﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WikiPageActionReply
    {
        public long WikiPageActionReplyId { get; set; }
        public long? PageActionId { get; set; }
        public string ReplyNotes { get; set; }
        public string ReplyFileName { get; set; }
        public string SessionId { get; set; }
        public long? AddedUserId { get; set; }
        public DateTime? AddedDate { get; set; }

        public virtual ApplicationUser AddedUser { get; set; }
        public virtual WikiPageAction PageAction { get; set; }
    }
}
