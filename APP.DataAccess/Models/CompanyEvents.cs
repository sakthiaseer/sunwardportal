﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CompanyEvents
    {
        public long CompanyEventId { get; set; }
        public int? EventTypeId { get; set; }
        public DateTime? StartDate { get; set; }
        public long? CompanyId { get; set; }
        public long? DeptOwner { get; set; }
        public string Name { get; set; }
        public int? RepeatInfoId { get; set; }
        public int? StandardFrequencyId { get; set; }
        public int? FullfilmentStatus { get; set; }
        public int? NotLaterDays { get; set; }
        public DateTime? TriggerDate { get; set; }
        public string TaskContent { get; set; }
        public DateTime? TaskDueDate { get; set; }
        public bool? AuthorityOrCompanyRequirement { get; set; }
        public bool? IsEffectProdSchdule { get; set; }
        public bool? RequiredPreparation { get; set; }
        public int? PreparationTypeId { get; set; }
        public int? PreparationDays { get; set; }
        public DateTime? Reminderby { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser DeptOwnerNavigation { get; set; }
        public virtual CodeMaster EventType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster PreparationType { get; set; }
        public virtual CodeMaster RepeatInfo { get; set; }
        public virtual CodeMaster StandardFrequency { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
