﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FmglobalMove
    {
        public long FmglobalMoveId { get; set; }
        public long? LocationId { get; set; }
        public long? FmglobalLineId { get; set; }
        public int? IsHandQty { get; set; }
        public long? FmglobalLinePreviousId { get; set; }
        public long? FmglobalId { get; set; }
        public long? LocationToId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public int? TransactionQty { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Fmglobal Fmglobal { get; set; }
        public virtual FmglobalLine FmglobalLine { get; set; }
        public virtual FmglobalLine FmglobalLinePrevious { get; set; }
        public virtual Ictmaster Location { get; set; }
        public virtual Ictmaster LocationTo { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
    }
}
