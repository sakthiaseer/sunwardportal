﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavprodBom
    {
        public long ProdBomId { get; set; }
        public string ProdBomno { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? QuantityPer { get; set; }
        public decimal? QuantityPerUnit { get; set; }
        public string Uom { get; set; }
        public string Type { get; set; }
        public int? BomlineNo { get; set; }
        public string BatchSize { get; set; }
    }
}
