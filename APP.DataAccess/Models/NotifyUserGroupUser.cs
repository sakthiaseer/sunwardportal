﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NotifyUserGroupUser
    {
        public long NotifyUserGroupUserId { get; set; }
        public long? NotifyDocumentUserGroupId { get; set; }
        public long? UserGroupId { get; set; }
        public long? UserId { get; set; }
        public bool? IsClosed { get; set; }
        public bool? IsCcuser { get; set; }
        public bool? IsRead { get; set; }

        public virtual NotifyDocumentUserGroup NotifyDocumentUserGroup { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual UserGroup UserGroup { get; set; }
    }
}
