﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmployeeInformation
    {
        public long EmployeeInformationId { get; set; }
        public long? EmployeeId { get; set; }
        public long? CompanyId { get; set; }
        public long? DivisionId { get; set; }
        public long? DepartmentId { get; set; }
        public long? SectionId { get; set; }
        public long? SubSectionId { get; set; }
        public long? SubSectionTid { get; set; }
        public long? DesignationId { get; set; }
        public long? DesignationHeadCountId { get; set; }
        public bool? IsOversees { get; set; }
        public int? HeadCount { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual Department Department { get; set; }
        public virtual Designation Designation { get; set; }
        public virtual DesignationHeadCount DesignationHeadCount { get; set; }
        public virtual Division Division { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Section Section { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual SubSection SubSection { get; set; }
        public virtual SubSectionTwo SubSectionT { get; set; }
    }
}
