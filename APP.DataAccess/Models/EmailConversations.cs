﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmailConversations
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public long? TopicId { get; set; }
        public string Message { get; set; }
        public byte[] FileData { get; set; }
        public long? ParticipantId { get; set; }
        public long? ReplyId { get; set; }
        public DateTime? AddedDate { get; set; }
        public int? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid SessionId { get; set; }
        public int? AddedByUserId { get; set; }
        public int? StatusCodeId { get; set; }
        public bool? IsAllowParticipants { get; set; }
        public DateTime? DueDate { get; set; }
        public long? OnBehalf { get; set; }
        public string Follow { get; set; }
        public bool? Urgent { get; set; }
        public int? IsMobile { get; set; }
        public int? LastUpdateUserId { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public bool? NotifyUser { get; set; }
        public string UserType { get; set; }
        public int? NoOfDays { get; set; }
        public DateTime? ExpiryDueDate { get; set; }
        public Guid? DynamicFormDataUploadSessionId { get; set; }
        public bool? TagLock { get; set; }
    }
}
