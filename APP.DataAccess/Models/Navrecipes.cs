﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Navrecipes
    {
        public long ItemRecipeId { get; set; }
        public string RecipeNo { get; set; }
        public string Description { get; set; }
        public string ItemNo { get; set; }
        public string ItemDescription { get; set; }
        public string ItemDescription2 { get; set; }
        public string RoutingNo { get; set; }
        public string ProductionBomno { get; set; }
        public string Status { get; set; }
        public string Remark { get; set; }
        public string BatchSize { get; set; }
        public string MachineCenterCode { get; set; }
        public string MachineCenterName { get; set; }
        public decimal? BatchQty { get; set; }
        public string ProdTime { get; set; }
        public long? CompanyId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
