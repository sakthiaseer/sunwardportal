﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionActivity
    {
        public long ProductionActivityId { get; set; }
        public string Company { get; set; }
        public string TicketNo { get; set; }
        public string BatchNo { get; set; }
        public long? ProcessId { get; set; }
        public long? ActivityDocumentId { get; set; }
        public long? FileProfileTypeId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public bool? IsFinalProcess { get; set; }
        public bool? IsManualEntry { get; set; }
        public string SubLot { get; set; }
        public string Comment { get; set; }
        public bool? IsNotification { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }

        public virtual ApplicationMasterDetail ActivityDocument { get; set; }
        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationMasterDetail Process { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
