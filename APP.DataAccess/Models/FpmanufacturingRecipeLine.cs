﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FpmanufacturingRecipeLine
    {
        public long FpmanufacturingRecipeLineId { get; set; }
        public long? FinishProductGeneralInfoId { get; set; }
        public bool? IsMaster { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual FinishProductGeneralInfo FinishProductGeneralInfo { get; set; }
        public virtual ApplicationUser ModifiedUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
