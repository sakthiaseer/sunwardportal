﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DocumentRights
    {
        public long DocumentAccessId { get; set; }
        public long? DocumentId { get; set; }
        public long? UserId { get; set; }
        public bool? IsReadWrite { get; set; }
        public bool? IsRead { get; set; }

        public virtual Documents Document { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
