﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class BmrdisposalBox
    {
        public long BmrdisposalId { get; set; }
        public long? DisposalTypeId { get; set; }
        public string ProfileNo { get; set; }
        public DateTime? DisposalDate { get; set; }
        public long? EstimateNoOfBmr { get; set; }
        public long? LocationId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Locations Location { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
