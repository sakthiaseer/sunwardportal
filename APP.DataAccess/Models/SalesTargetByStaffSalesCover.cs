﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SalesTargetByStaffSalesCover
    {
        public long SalesTargetByStaffSalesCoverId { get; set; }
        public long? SalesTargetByStaffLineId { get; set; }
        public long? SalesCoverCodeId { get; set; }

        public virtual ApplicationMasterDetail SalesCoverCode { get; set; }
        public virtual SalesTargetByStaffLine SalesTargetByStaffLine { get; set; }
    }
}
