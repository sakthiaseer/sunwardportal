﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewEmployeeOtherDutyInformation
    {
        public long EmployeeOtherDutyInformationId { get; set; }
        public long? EmployeeId { get; set; }
        public long? DesignationTypeId { get; set; }
        public long? DesignationId { get; set; }
        public int? HeadCount { get; set; }
        public long? DutyTypeId { get; set; }
        public string EmployeeName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public int? StatusCodeId { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string StatusCode { get; set; }
        public string AddedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string PlantDescription { get; set; }
        public int? CodeId { get; set; }
        public string DutyType { get; set; }
        public string DesignationName { get; set; }
        public string SubSectionName { get; set; }
        public string SubSectionDescription { get; set; }
        public long? SectionId { get; set; }
        public string SectionName { get; set; }
        public string SectionDescription { get; set; }
        public long? DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentDescription { get; set; }
        public long? DivisionId { get; set; }
        public string DivisionName { get; set; }
        public string DivisionDescription { get; set; }
        public long? CompanyId { get; set; }
        public string PlantCode { get; set; }
        public string CompanyName { get; set; }
        public string LevelName { get; set; }
    }
}
