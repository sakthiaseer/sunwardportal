﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CalibrationExpireCalculation
    {
        public long CalibrationExpireCalculationId { get; set; }
        public int? Months { get; set; }
        public long? CalibrationServiceInformationId { get; set; }
        public long? DeviceCatalogMasterId { get; set; }

        public virtual DeviceCatalogMaster DeviceCatalogMaster { get; set; }
    }
}
