﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductActivityCase
    {
        public ProductActivityCase()
        {
            FileProfileSetupForm = new HashSet<FileProfileSetupForm>();
            IpirReport = new HashSet<IpirReport>();
            ProductActivityCaseActionMultiple = new HashSet<ProductActivityCaseActionMultiple>();
            ProductActivityCaseCategoryMultiple = new HashSet<ProductActivityCaseCategoryMultiple>();
            ProductActivityCaseCompany = new HashSet<ProductActivityCaseCompany>();
            ProductActivityCaseCompanyMultiple = new HashSet<ProductActivityCaseCompanyMultiple>();
            ProductActivityCaseLine = new HashSet<ProductActivityCaseLine>();
            ProductActivityCaseRespons = new HashSet<ProductActivityCaseRespons>();
            ProductionActivityAppLine = new HashSet<ProductionActivityAppLine>();
            ProductionActivityPlanningApp = new HashSet<ProductionActivityPlanningApp>();
            ProductionActivityPlanningAppLine = new HashSet<ProductionActivityPlanningAppLine>();
            ProductionActivityRoutineApp = new HashSet<ProductionActivityRoutineApp>();
            ProductionActivityRoutineAppLine = new HashSet<ProductionActivityRoutineAppLine>();
        }

        public long ProductActivityCaseId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public string Instruction { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public int? StatusCodeId { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public long? ManufacturingProcessChildId { get; set; }
        public long? CategoryActionId { get; set; }
        public long? ActionId { get; set; }
        public int? ProductionTypeId { get; set; }
        public string Reference { get; set; }
        public string CheckerNotes { get; set; }
        public string Upload { get; set; }
        public long? CompanyId { get; set; }
        public DateTime? ImplementationDate { get; set; }
        public string IsLanguage { get; set; }
        public string ReferenceMalay { get; set; }
        public string CheckerNotesMalay { get; set; }
        public string UploadMalay { get; set; }
        public string ReferenceChinese { get; set; }
        public string CheckerNotesChinese { get; set; }
        public string UploadChinese { get; set; }
        public long? VersionCodeStatusId { get; set; }
        public string VersionNo { get; set; }
        public bool? IsEnglishLanguage { get; set; }
        public bool? IsMalayLanguage { get; set; }
        public bool? IsChineseLanguage { get; set; }
        public long? ProfileId { get; set; }
        public long? FileProfileTypeId { get; set; }

        public virtual ApplicationMasterChild Action { get; set; }
        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterChild CategoryAction { get; set; }
        public virtual Plant Company { get; set; }
        public virtual FileProfileType FileProfileType { get; set; }
        public virtual ApplicationMasterDetail ManufacturingProcess { get; set; }
        public virtual ApplicationMasterChild ManufacturingProcessChild { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster ProductionType { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationMasterDetail VersionCodeStatus { get; set; }
        public virtual ICollection<FileProfileSetupForm> FileProfileSetupForm { get; set; }
        public virtual ICollection<IpirReport> IpirReport { get; set; }
        public virtual ICollection<ProductActivityCaseActionMultiple> ProductActivityCaseActionMultiple { get; set; }
        public virtual ICollection<ProductActivityCaseCategoryMultiple> ProductActivityCaseCategoryMultiple { get; set; }
        public virtual ICollection<ProductActivityCaseCompany> ProductActivityCaseCompany { get; set; }
        public virtual ICollection<ProductActivityCaseCompanyMultiple> ProductActivityCaseCompanyMultiple { get; set; }
        public virtual ICollection<ProductActivityCaseLine> ProductActivityCaseLine { get; set; }
        public virtual ICollection<ProductActivityCaseRespons> ProductActivityCaseRespons { get; set; }
        public virtual ICollection<ProductionActivityAppLine> ProductionActivityAppLine { get; set; }
        public virtual ICollection<ProductionActivityPlanningApp> ProductionActivityPlanningApp { get; set; }
        public virtual ICollection<ProductionActivityPlanningAppLine> ProductionActivityPlanningAppLine { get; set; }
        public virtual ICollection<ProductionActivityRoutineApp> ProductionActivityRoutineApp { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLine> ProductionActivityRoutineAppLine { get; set; }
    }
}
