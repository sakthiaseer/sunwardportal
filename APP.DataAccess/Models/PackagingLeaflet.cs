﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PackagingLeaflet
    {
        public long LeafletId { get; set; }
        public decimal? LengthSize { get; set; }
        public decimal? WidthSize { get; set; }
        public long? PackingMaterialId { get; set; }
        public long? PackingItemId { get; set; }
        public long? PackingUnitId { get; set; }
        public int? LeafletTypeId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public int? PrintedOnId { get; set; }
        public bool? IsVersion { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster LeafletType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster PrintedOn { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
