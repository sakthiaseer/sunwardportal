﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AppdispenserDispensing
    {
        public AppdispenserDispensing()
        {
            AppdispenserDispensingLine = new HashSet<AppdispenserDispensingLine>();
        }

        public long DispenserDispensingId { get; set; }
        public string WorkOrderNo { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public int? TotalItem { get; set; }
        public int? WeighingMaterial { get; set; }
        public string Company { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<AppdispenserDispensingLine> AppdispenserDispensingLine { get; set; }
    }
}
