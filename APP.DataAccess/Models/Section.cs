﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Section
    {
        public Section()
        {
            ApplicationWiki = new HashSet<ApplicationWiki>();
            Designation = new HashSet<Designation>();
            DraftApplicationWiki = new HashSet<DraftApplicationWiki>();
            Employee = new HashSet<Employee>();
            EmployeeInformation = new HashSet<EmployeeInformation>();
            EmployeeReportPerson = new HashSet<EmployeeReportPerson>();
            ProfileAutoNumber = new HashSet<ProfileAutoNumber>();
            SubSection = new HashSet<SubSection>();
            WikiAccess = new HashSet<WikiAccess>();
            WikiPage = new HashSet<WikiPage>();
        }

        public long SectionId { get; set; }
        public long? DepartmentId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool? IsWiki { get; set; }
        public bool? IsTestPaper { get; set; }
        public int? HeadCount { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileCode { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Department Department { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ApplicationWiki> ApplicationWiki { get; set; }
        public virtual ICollection<Designation> Designation { get; set; }
        public virtual ICollection<DraftApplicationWiki> DraftApplicationWiki { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
        public virtual ICollection<EmployeeInformation> EmployeeInformation { get; set; }
        public virtual ICollection<EmployeeReportPerson> EmployeeReportPerson { get; set; }
        public virtual ICollection<ProfileAutoNumber> ProfileAutoNumber { get; set; }
        public virtual ICollection<SubSection> SubSection { get; set; }
        public virtual ICollection<WikiAccess> WikiAccess { get; set; }
        public virtual ICollection<WikiPage> WikiPage { get; set; }
    }
}
