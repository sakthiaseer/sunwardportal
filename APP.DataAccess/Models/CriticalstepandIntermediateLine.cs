﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CriticalstepandIntermediateLine
    {
        public CriticalstepandIntermediateLine()
        {
            CriticalSamplingFrequency = new HashSet<CriticalSamplingFrequency>();
        }

        public long CriticalstepandIntermediateLineId { get; set; }
        public long? CriticalstepandIntermediateId { get; set; }
        public long? TestId { get; set; }
        public long? TestStageId { get; set; }
        public long? UnitsId { get; set; }
        public long? SpecificationLimitsId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CriticalstepandIntermediate CriticalstepandIntermediate { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<CriticalSamplingFrequency> CriticalSamplingFrequency { get; set; }
    }
}
