﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FolderStorageUser
    {
        public long FolderStorageUserId { get; set; }
        public long? FolderStorageId { get; set; }
        public long? UserSelectionId { get; set; }

        public virtual FolderStorage FolderStorage { get; set; }
        public virtual ApplicationUser UserSelection { get; set; }
    }
}
