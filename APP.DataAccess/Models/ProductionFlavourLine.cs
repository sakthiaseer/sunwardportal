﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionFlavourLine
    {
        public long ProductionFlavourLineId { get; set; }
        public long? ProductionFlavourId { get; set; }
        public long? DataBaseRequireId { get; set; }
        public long? NavisionId { get; set; }
        public string Buom { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProductionFlavour ProductionFlavour { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
