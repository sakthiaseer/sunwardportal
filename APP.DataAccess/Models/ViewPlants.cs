﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewPlants
    {
        public long PlantId { get; set; }
        public long CompanyId { get; set; }
        public string PlantCode { get; set; }
        public string Description { get; set; }
        public long? RegisteredCountryId { get; set; }
        public string RegistrationNo { get; set; }
        public DateTime? EstablishedDate { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public int? StatusCodeId { get; set; }
        public string Gstno { get; set; }
        public string NavSoapAddress { get; set; }
        public string NavOdataAddress { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsLinkNav { get; set; }
        public string NavCompanyName { get; set; }
        public string NavUserName { get; set; }
        public string NavPassword { get; set; }
        public string StatusCode { get; set; }
        public string AddedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string CompanyName { get; set; }
        public int? CodeId { get; set; }
        public string NavCompany { get; set; }
    }
}
