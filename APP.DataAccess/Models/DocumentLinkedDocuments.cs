﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DocumentLinkedDocuments
    {
        public long DocumentLinkedDocumentsId { get; set; }
        public long? DocumentLinkId { get; set; }
        public long? DocumentId { get; set; }
        public string DocumentType { get; set; }
        public long? FileProfileTypeId { get; set; }
        public long? FolderId { get; set; }

        public virtual Documents Document { get; set; }
        public virtual DocumentLink DocumentLink { get; set; }
    }
}
