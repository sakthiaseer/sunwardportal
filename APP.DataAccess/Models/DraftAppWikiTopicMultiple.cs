﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DraftAppWikiTopicMultiple
    {
        public long AppWikiTopicMultipleId { get; set; }
        public long? ApplicationWikiId { get; set; }
        public long? WikiTopicId { get; set; }

        public virtual DraftApplicationWiki ApplicationWiki { get; set; }
        public virtual ApplicationMasterChild WikiTopic { get; set; }
    }
}
