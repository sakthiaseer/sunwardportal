﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApplicationFormPermission
    {
        public long ApplicationFormFieldPermissionId { get; set; }
        public long? ApplicationFormFieldId { get; set; }
        public long? RoleId { get; set; }
        public bool IsReadOnly { get; set; }
        public bool IsVisible { get; set; }
        public bool IsValueFilter { get; set; }
        public string FieldValue { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedByUserId { get; set; }

        public virtual ApplicationFormFields ApplicationFormField { get; set; }
        public virtual ApplicationRole Role { get; set; }
    }
}
