﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewEmployeeEmailInfo
    {
        public long EmployeeEmailInfoId { get; set; }
        public long? EmployeeId { get; set; }
        public long? SubscriptionId { get; set; }
        public long? EmailGuideId { get; set; }
        public string EmailGuide { get; set; }
        public string Subscription { get; set; }
    }
}
