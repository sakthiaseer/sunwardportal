﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class HumanMovementAction
    {
        public long HumanMovementActionId { get; set; }
        public long? HumanMovementId { get; set; }
        public long? PharmacistId { get; set; }
        public DateTime? LogInTime { get; set; }
        public long? ActionToTakeId { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? HrloginId { get; set; }
        public string Hrdescription { get; set; }
        public long? HractionToTakeId { get; set; }
        public string Hrcomment { get; set; }
        public int? HrstatusCodeId { get; set; }
        public DateTime? HrLogInTime { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Employee Hrlogin { get; set; }
        public virtual CodeMaster HrstatusCode { get; set; }
        public virtual HumanMovement HumanMovement { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Employee Pharmacist { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
