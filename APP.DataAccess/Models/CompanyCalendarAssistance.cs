﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CompanyCalendarAssistance
    {
        public long CompanyCalendarAssistanceId { get; set; }
        public long? CompanyCalendarId { get; set; }
        public long? UserId { get; set; }

        public virtual CompanyCalendar CompanyCalendar { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
