﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NotificationHandler
    {
        public long NotificationHandlerId { get; set; }
        public Guid? SessionId { get; set; }
        public DateTime? NextNotifyDate { get; set; }
        public int? NotifyStatusId { get; set; }
        public int? AddedDays { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public long? NotifyTo { get; set; }
        public string ScreenId { get; set; }

        public virtual CodeMaster NotifyStatus { get; set; }
        public virtual ApplicationUser NotifyToNavigation { get; set; }
    }
}
