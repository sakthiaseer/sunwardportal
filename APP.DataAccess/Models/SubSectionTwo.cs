﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SubSectionTwo
    {
        public SubSectionTwo()
        {
            Designation = new HashSet<Designation>();
            Employee = new HashSet<Employee>();
            EmployeeInformation = new HashSet<EmployeeInformation>();
            EmployeeOtherDutyInformation = new HashSet<EmployeeOtherDutyInformation>();
        }

        public long SubSectionTid { get; set; }
        public long? SubSectonId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int? HeadCount { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual SubSection SubSecton { get; set; }
        public virtual ICollection<Designation> Designation { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
        public virtual ICollection<EmployeeInformation> EmployeeInformation { get; set; }
        public virtual ICollection<EmployeeOtherDutyInformation> EmployeeOtherDutyInformation { get; set; }
    }
}
