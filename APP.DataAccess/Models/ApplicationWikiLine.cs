﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApplicationWikiLine
    {
        public ApplicationWikiLine()
        {
            ApplicationWikiLineDuty = new HashSet<ApplicationWikiLineDuty>();
            ApplicationWikiLineNotify = new HashSet<ApplicationWikiLineNotify>();
            ApplicationWikiRecurrence = new HashSet<ApplicationWikiRecurrence>();
            ApplicationWikiWeekly = new HashSet<ApplicationWikiWeekly>();
        }

        public long ApplicationWikiLineId { get; set; }
        public long? ApplicationWikiId { get; set; }
        public long? DutyId { get; set; }
        public string Responsibility { get; set; }
        public long? PageLink { get; set; }
        public long? FunctionLink { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? NotificationAdvice { get; set; }
        public int? NotificationAdviceTypeId { get; set; }
        public int? RepeatId { get; set; }
        public int? CustomId { get; set; }
        public DateTime? DueDate { get; set; }
        public int? Monthly { get; set; }
        public int? Yearly { get; set; }
        public string EventDescription { get; set; }
        public bool? DaysOfWeek { get; set; }
        public int? NotificationStatusId { get; set; }
        public Guid? SessionId { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public long? NotifyTo { get; set; }
        public string ScreenId { get; set; }
        public DateTime? NotifyEndDate { get; set; }
        public bool? IsAllowDocAccess { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationWiki ApplicationWiki { get; set; }
        public virtual CodeMaster Custom { get; set; }
        public virtual ApplicationPermission FunctionLinkNavigation { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster NotificationAdviceType { get; set; }
        public virtual Employee NotifyToNavigation { get; set; }
        public virtual ApplicationPermission PageLinkNavigation { get; set; }
        public virtual CodeMaster Repeat { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ApplicationWikiLineDuty> ApplicationWikiLineDuty { get; set; }
        public virtual ICollection<ApplicationWikiLineNotify> ApplicationWikiLineNotify { get; set; }
        public virtual ICollection<ApplicationWikiRecurrence> ApplicationWikiRecurrence { get; set; }
        public virtual ICollection<ApplicationWikiWeekly> ApplicationWikiWeekly { get; set; }
    }
}
