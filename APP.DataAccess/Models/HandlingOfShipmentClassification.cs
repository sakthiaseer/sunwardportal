﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class HandlingOfShipmentClassification
    {
        public HandlingOfShipmentClassification()
        {
            HandlingOfShipmentSpecificCustomer = new HashSet<HandlingOfShipmentSpecificCustomer>();
        }

        public long HandlingOfShipmentId { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public long? ShipmentMethodId { get; set; }
        public long? SpecificTypeOfOrderId { get; set; }
        public string ProcessingTimeDay { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileReferenceNo { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ItemClassificationMaster ItemClassificationMaster { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual ApplicationMasterDetail ShipmentMethod { get; set; }
        public virtual ApplicationMasterDetail SpecificTypeOfOrder { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<HandlingOfShipmentSpecificCustomer> HandlingOfShipmentSpecificCustomer { get; set; }
    }
}
