﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavstockBalanace
    {
        public long AvnavStockBalId { get; set; }
        public DateTime? StockBalMonth { get; set; }
        public string ItemDecs { get; set; }
        public string Uom { get; set; }
        public decimal? RemainingQty { get; set; }
        public bool? IsNav { get; set; }
        public int? PackSize { get; set; }
        public int? Ac { get; set; }
        public string Dist { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
