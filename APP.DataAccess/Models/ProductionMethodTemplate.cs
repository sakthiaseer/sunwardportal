﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionMethodTemplate
    {
        public ProductionMethodTemplate()
        {
            ProductionMethodManufacturing = new HashSet<ProductionMethodManufacturing>();
        }

        public long ProductionMethodTemplateId { get; set; }
        public long? IcmasterOperationId { get; set; }
        public long? ProfileId { get; set; }
        public string RefNo { get; set; }
        public long? LocationId { get; set; }
        public long? MachineId { get; set; }
        public int? ManPower { get; set; }
        public int? HoursMinutes { get; set; }
        public bool? IsEachNewDay { get; set; }
        public bool? IsEndOfTheDay { get; set; }
        public bool? IsNewBatch { get; set; }
        public bool? IsNewSubLot { get; set; }
        public int? IdlingForXdays { get; set; }
        public int? IdlingForXhours { get; set; }
        public int? CampaignByXbatches { get; set; }
        public bool? ChangeShift { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual IcmasterOperation IcmasterOperation { get; set; }
        public virtual Ictmaster Location { get; set; }
        public virtual CommonFieldsProductionMachine Machine { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ProductionMethodManufacturing> ProductionMethodManufacturing { get; set; }
    }
}
