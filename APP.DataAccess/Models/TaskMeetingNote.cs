﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TaskMeetingNote
    {
        public long TaskMeetingNoteId { get; set; }
        public long? TaskAttachmentId { get; set; }
        public string MeetingNotes { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? MeetingDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual TaskAttachment TaskAttachment { get; set; }
    }
}
