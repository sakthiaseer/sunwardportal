﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ClassificationProduction
    {
        public long ClassificationProductionId { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileReferenceNo { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ItemClassificationMaster ItemClassificationMaster { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
