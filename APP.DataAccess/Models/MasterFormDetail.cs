﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class MasterFormDetail
    {
        public long MasterFormDetailId { get; set; }
        public long? MasterFormId { get; set; }
        public string MasterFormData { get; set; }
        public long? UserId { get; set; }
        public Guid? SessionId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual MasterForm MasterForm { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
