﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class BlisterScrapMaster
    {
        public BlisterScrapMaster()
        {
            BlisterAllowable = new HashSet<BlisterAllowable>();
            BlisterScrapLine = new HashSet<BlisterScrapLine>();
        }

        public long BlisterScrapId { get; set; }
        public string BlisterComponent { get; set; }
        public string Description { get; set; }
        public string Description1 { get; set; }
        public string Sublot { get; set; }
        public string BatchSize { get; set; }
        public string PackSize { get; set; }
        public string Bomtype { get; set; }
        public string Uom { get; set; }
        public string ScrapUom { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<BlisterAllowable> BlisterAllowable { get; set; }
        public virtual ICollection<BlisterScrapLine> BlisterScrapLine { get; set; }
    }
}
