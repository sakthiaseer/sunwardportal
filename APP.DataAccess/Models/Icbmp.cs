﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Icbmp
    {
        public Icbmp()
        {
            IcBmpManufacturingSite = new HashSet<IcBmpManufacturingSite>();
            Icbmpline = new HashSet<Icbmpline>();
        }

        public long IcBmpId { get; set; }
        public string BmpNo { get; set; }
        public string WiLink { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<IcBmpManufacturingSite> IcBmpManufacturingSite { get; set; }
        public virtual ICollection<Icbmpline> Icbmpline { get; set; }
    }
}
