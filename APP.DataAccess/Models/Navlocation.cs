﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Navlocation
    {
        public Navlocation()
        {
            IssueRequestSampleLine = new HashSet<IssueRequestSampleLine>();
            SampleRequestForm = new HashSet<SampleRequestForm>();
            SampleRequestFormLine = new HashSet<SampleRequestFormLine>();
        }

        public long NavlocationId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public long? ItemId { get; set; }
        public long? PlantId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Plant Plant { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<IssueRequestSampleLine> IssueRequestSampleLine { get; set; }
        public virtual ICollection<SampleRequestForm> SampleRequestForm { get; set; }
        public virtual ICollection<SampleRequestFormLine> SampleRequestFormLine { get; set; }
    }
}
