﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class LinkFileProfileTypeDocument
    {
        public LinkFileProfileTypeDocument()
        {
            DynamicFormDataUpload = new HashSet<DynamicFormDataUpload>();
        }

        public long LinkFileProfileTypeDocumentId { get; set; }
        public Guid? TransactionSessionId { get; set; }
        public long? DocumentId { get; set; }
        public long? FileProfileTypeId { get; set; }
        public long? FolderId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public string Description { get; set; }
        public bool? IsWiki { get; set; }
        public bool? IsWikiDraft { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Documents Document { get; set; }
        public virtual FileProfileType FileProfileType { get; set; }
        public virtual ICollection<DynamicFormDataUpload> DynamicFormDataUpload { get; set; }
    }
}
