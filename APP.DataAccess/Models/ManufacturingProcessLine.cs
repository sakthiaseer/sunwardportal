﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ManufacturingProcessLine
    {
        public long ManufacturingProcessLineId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public long? ProcessStageId { get; set; }
        public long? ManufacturingProcessUploadId { get; set; }
        public string Remarks { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ManufacturingProcess ManufacturingProcess { get; set; }
        public virtual Documents ManufacturingProcessUpload { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
