﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class InterCompanyPurchaseOrder
    {
        public InterCompanyPurchaseOrder()
        {
            InterCompanyPurchaseOrderLine = new HashSet<InterCompanyPurchaseOrderLine>();
        }

        public long InterCompanyPurchaseOrderId { get; set; }
        public DateTime? Date { get; set; }
        public long? LoginId { get; set; }
        public long? BuyerCompanyId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant BuyerCompany { get; set; }
        public virtual ApplicationUser Login { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<InterCompanyPurchaseOrderLine> InterCompanyPurchaseOrderLine { get; set; }
    }
}
