﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FinishProductGeneralInfoSpecialIns
    {
        public long FinishProductSpecialInsId { get; set; }
        public long? FinishProductGeneralInfoLineId { get; set; }
        public long? SpecialInstructionId { get; set; }

        public virtual FinishProdcutGeneralInfoLine FinishProductGeneralInfoLine { get; set; }
    }
}
