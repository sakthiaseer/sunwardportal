﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WorkOrderComment
    {
        public WorkOrderComment()
        {
            InverseParentComment = new HashSet<WorkOrderComment>();
            WorkOrderCommentUser = new HashSet<WorkOrderCommentUser>();
        }

        public long WorkOrderCommentId { get; set; }
        public long? WorkorderId { get; set; }
        public long? ParentCommentId { get; set; }
        public string Comment { get; set; }
        public long? CommentedBy { get; set; }
        public DateTime? CommentedDate { get; set; }
        public bool? IsRead { get; set; }
        public bool? IsEdit { get; set; }
        public long? EditedBy { get; set; }
        public string Subject { get; set; }
        public bool? IsClosed { get; set; }

        public virtual ApplicationUser CommentedByNavigation { get; set; }
        public virtual ApplicationUser EditedByNavigation { get; set; }
        public virtual WorkOrderComment ParentComment { get; set; }
        public virtual WorkOrderLine Workorder { get; set; }
        public virtual ICollection<WorkOrderComment> InverseParentComment { get; set; }
        public virtual ICollection<WorkOrderCommentUser> WorkOrderCommentUser { get; set; }
    }
}
