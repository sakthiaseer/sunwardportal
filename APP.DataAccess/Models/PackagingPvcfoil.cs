﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PackagingPvcfoil
    {
        public PackagingPvcfoil()
        {
            PackagingPvcfoilBlister = new HashSet<PackagingPvcfoilBlister>();
        }

        public long PvcFoilId { get; set; }
        public long? PvcTypeId { get; set; }
        public decimal? FoilThickness { get; set; }
        public decimal? FoilWidth { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public int? PvcFoilTypeId { get; set; }
        public bool? IsVersion { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster PvcFoilType { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<PackagingPvcfoilBlister> PackagingPvcfoilBlister { get; set; }
    }
}
