﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class BlisterAllowable
    {
        public long BlisterAllowableId { get; set; }
        public long? BlisterScrapId { get; set; }
        public string MachineCode { get; set; }
        public long? MachineId { get; set; }
        public int? NoOfCuts { get; set; }
        public string AttachedlayoutPlan { get; set; }
        public long? BlisterScrapTypeId { get; set; }
        public string BlisterType { get; set; }
        public decimal? BlisterValue { get; set; }
        public decimal? TestRun { get; set; }
        public decimal? Trimming { get; set; }
        public decimal? DeBlister { get; set; }

        public virtual BlisterScrapMaster BlisterScrap { get; set; }
    }
}
