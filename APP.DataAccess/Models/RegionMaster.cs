﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class RegionMaster
    {
        public RegionMaster()
        {
            Employee = new HashSet<Employee>();
        }

        public long RegionId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
    }
}
