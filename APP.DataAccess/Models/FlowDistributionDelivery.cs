﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FlowDistributionDelivery
    {
        public long FlowDistributionDeliveryId { get; set; }
        public long? FlowDistributionId { get; set; }
        public long? DynamicFlowStepId { get; set; }
        public bool? IsParallel { get; set; }
        public string DynamicFlowSteps { get; set; }
        public int? DistributionStatus { get; set; }

        public virtual CodeMaster DistributionStatusNavigation { get; set; }
        public virtual FlowDistribution FlowDistribution { get; set; }
    }
}
