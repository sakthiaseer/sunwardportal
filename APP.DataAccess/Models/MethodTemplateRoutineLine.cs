﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class MethodTemplateRoutineLine
    {
        public MethodTemplateRoutineLine()
        {
            StandardManufacturingProcessLine = new HashSet<StandardManufacturingProcessLine>();
        }

        public long MethodTemplateRoutineLineId { get; set; }
        public long? MethodTemplateRoutineId { get; set; }
        public string No { get; set; }
        public long? ProcesssStepId { get; set; }
        public long? OperationId { get; set; }
        public long? DetermineFactorId { get; set; }
        public string FixTimingorMin { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual MethodTemplateRoutine MethodTemplateRoutine { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<StandardManufacturingProcessLine> StandardManufacturingProcessLine { get; set; }
    }
}
