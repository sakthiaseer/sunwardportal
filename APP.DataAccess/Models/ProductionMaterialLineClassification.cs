﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionMaterialLineClassification
    {
        public long MaterialClassificationId { get; set; }
        public string NavisionType { get; set; }
        public long? DrugClassificationId { get; set; }
        public long? ProductionMaterialLineId { get; set; }

        public virtual ProductionMaterialLine ProductionMaterialLine { get; set; }
    }
}
