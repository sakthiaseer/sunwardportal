﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CompanyCalendarLineMeetingNotes
    {
        public long CompanyCalendarLineMeetingNotesId { get; set; }
        public long? CompanyCalendarLineId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? StatusCodeId { get; set; }
        public string MeetingData { get; set; }
        public string Subject { get; set; }
        public bool? IsTitleFlag { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CompanyCalendarLine CompanyCalendarLine { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
