﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Country
    {
        public Country()
        {
            CompanyWorkDayMaster = new HashSet<CompanyWorkDayMaster>();
            Contacts = new HashSet<Contacts>();
            State = new HashSet<State>();
        }

        public long CountryId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<CompanyWorkDayMaster> CompanyWorkDayMaster { get; set; }
        public virtual ICollection<Contacts> Contacts { get; set; }
        public virtual ICollection<State> State { get; set; }
    }
}
