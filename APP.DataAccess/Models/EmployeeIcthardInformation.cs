﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmployeeIcthardInformation
    {
        public long EmployeeIcthardInformationId { get; set; }
        public long? EmployeeId { get; set; }
        public string LoginId { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? CompanyId { get; set; }
        public string Instruction { get; set; }
        public long? HardwareId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual ApplicationMasterDetail Hardware { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
