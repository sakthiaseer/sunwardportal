﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TimeSheetForQc
    {
        public long QctimesheetId { get; set; }
        public string ItemName { get; set; }
        public string RefNo { get; set; }
        public string Stage { get; set; }
        public string TestName { get; set; }
        public bool? Qrcode { get; set; }
        public string DetailEntry { get; set; }
        public string Comment { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public long? ActivityStatusId { get; set; }
    }
}
