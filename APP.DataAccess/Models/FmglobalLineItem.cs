﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FmglobalLineItem
    {
        public long FmglobalLineItemId { get; set; }
        public long? FmglobalLineId { get; set; }
        public long? ItemId { get; set; }
        public long? BatchInfoId { get; set; }
        public long? CartonPackingId { get; set; }
        public int? NoOfCarton { get; set; }
        public int? NoOfUnitsPerCarton { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public long? ItemBatchInfoId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual NavItemBatchNo BatchInfo { get; set; }
        public virtual ApplicationMasterDetail CartonPacking { get; set; }
        public virtual FmglobalLine FmglobalLine { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ItemBatchInfo ItemBatchInfo { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
