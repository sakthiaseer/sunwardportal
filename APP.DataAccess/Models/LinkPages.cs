﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class LinkPages
    {
        public long LinkPageId { get; set; }
        public long? PageId { get; set; }
        public long? ParentPageId { get; set; }
        public long? LinkPageConditionId { get; set; }
        public bool? IsParentPage { get; set; }
        public long? ModifiedUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual PageLinkCondition LinkPageCondition { get; set; }
        public virtual WikiPage Page { get; set; }
        public virtual WikiPage ParentPage { get; set; }
    }
}
