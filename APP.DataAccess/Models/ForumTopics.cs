﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ForumTopics
    {
        public ForumTopics()
        {
            ForumTopicCc = new HashSet<ForumTopicCc>();
            ForumTopicTo = new HashSet<ForumTopicTo>();
        }

        public long Id { get; set; }
        public string TicketNo { get; set; }
        public string TopicName { get; set; }
        public long? TypeId { get; set; }
        public long? CategoryId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? DueDate { get; set; }
        public long TopicFrom { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public string Description { get; set; }
        public byte[] FileData { get; set; }
        public string Type { get; set; }
        public int? SeqNo { get; set; }
        public string SubjectName { get; set; }
        public int StatusCodeId { get; set; }
        public int AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public int? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public string To { get; set; }
        public string Cc { get; set; }
        public string Participants { get; set; }
        public string Label { get; set; }
        public string TypeName { get; set; }
        public string Follow { get; set; }
        public string OnBehalf { get; set; }
        public bool? Urgent { get; set; }
        public bool? OverDue { get; set; }

        public virtual ICollection<ForumTopicCc> ForumTopicCc { get; set; }
        public virtual ICollection<ForumTopicTo> ForumTopicTo { get; set; }
    }
}
