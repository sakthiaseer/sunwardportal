﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class BlisterMouldInformationDocument
    {
        public long BlisterMouldInformationDocumentId { get; set; }
        public long? DocumentId { get; set; }
        public long? BlisterMouldInformationId { get; set; }

        public virtual BlisterMouldInformation BlisterMouldInformation { get; set; }
        public virtual Documents Document { get; set; }
    }
}
