﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CommonPackagingSetInfo
    {
        public CommonPackagingSetInfo()
        {
            CommonPackingInfoOptional = new HashSet<CommonPackingInfoOptional>();
            CommonPackingInformation = new HashSet<CommonPackingInformation>();
        }

        public long SetInformationId { get; set; }
        public long? PackagingItemSetInfoId { get; set; }
        public bool? Set { get; set; }
        public long? PackagingMaterialId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CommonPackagingItemHeader PackagingMaterial { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<CommonPackingInfoOptional> CommonPackingInfoOptional { get; set; }
        public virtual ICollection<CommonPackingInformation> CommonPackingInformation { get; set; }
    }
}
