﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ItemClassificationHeader
    {
        public ItemClassificationHeader()
        {
            ExcipientInformationLineByProcess = new HashSet<ExcipientInformationLineByProcess>();
            FpmanufacturingRecipe = new HashSet<FpmanufacturingRecipe>();
        }

        public long ItemClassificationHeaderId { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public long? ProfileId { get; set; }
        public string MaterialNo { get; set; }
        public string MaterialName { get; set; }
        public long? MaterialId { get; set; }
        public string AlsoKnownAs { get; set; }
        public long? Uomid { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string LinkProfileReferenceNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ItemClassificationMaster ItemClassificationMaster { get; set; }
        public virtual ApplicationMasterDetail Material { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationMasterDetail Uom { get; set; }
        public virtual ICollection<ExcipientInformationLineByProcess> ExcipientInformationLineByProcess { get; set; }
        public virtual ICollection<FpmanufacturingRecipe> FpmanufacturingRecipe { get; set; }
    }
}
