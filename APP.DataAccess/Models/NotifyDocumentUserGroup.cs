﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NotifyDocumentUserGroup
    {
        public NotifyDocumentUserGroup()
        {
            NotifyUserGroupUser = new HashSet<NotifyUserGroupUser>();
        }

        public long NotifyDocumentUserGroupId { get; set; }
        public long? NotifyDocumentId { get; set; }
        public long? UserId { get; set; }
        public long? UserGroupId { get; set; }
        public bool? IsClosed { get; set; }
        public bool? IsCcuser { get; set; }
        public bool? IsRead { get; set; }

        public virtual NotifyDocument NotifyDocument { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual UserGroup UserGroup { get; set; }
        public virtual ICollection<NotifyUserGroupUser> NotifyUserGroupUser { get; set; }
    }
}
