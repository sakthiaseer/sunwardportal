﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AppdispenserDispensingLine
    {
        public AppdispenserDispensingLine()
        {
            AppdispenserDispensingDrumDetails = new HashSet<AppdispenserDispensingDrumDetails>();
        }

        public long DispenserDispensingLineId { get; set; }
        public long? DispenserDispensingId { get; set; }
        public string ProdOrderNo { get; set; }
        public int? ProdLineNo { get; set; }
        public string SubLotNo { get; set; }
        public string JobNo { get; set; }
        public string BagNo { get; set; }
        public decimal? BeforeTareWeight { get; set; }
        public string ImageBeforeTare { get; set; }
        public decimal? AfterTareWeight { get; set; }
        public string ImageAfterTare { get; set; }
        public string ItemNo { get; set; }
        public string LotNo { get; set; }
        public string QcrefNo { get; set; }
        public decimal? Weight { get; set; }
        public string WeighingPhoto { get; set; }
        public bool? PrintLabel { get; set; }
        public bool? PostedtoNav { get; set; }

        public virtual AppdispenserDispensing DispenserDispensing { get; set; }
        public virtual ICollection<AppdispenserDispensingDrumDetails> AppdispenserDispensingDrumDetails { get; set; }
    }
}
