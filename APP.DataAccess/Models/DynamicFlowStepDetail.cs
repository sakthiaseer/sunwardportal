﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFlowStepDetail
    {
        public DynamicFlowStepDetail()
        {
            DynamicFlowStepDetailField = new HashSet<DynamicFlowStepDetailField>();
        }

        public long DynamicFlowStepDetailId { get; set; }
        public long? DynamicFlowStepId { get; set; }
        public long? PersonIncharge { get; set; }
        public string Notes { get; set; }
        public string Duration { get; set; }
        public string TableName { get; set; }
        public long? TableId { get; set; }
        public long? UserGroupId { get; set; }

        public virtual DynamicFlowStep DynamicFlowStep { get; set; }
        public virtual UserGroup UserGroup { get; set; }
        public virtual ICollection<DynamicFlowStepDetailField> DynamicFlowStepDetailField { get; set; }
    }
}
