﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Locations
    {
        public Locations()
        {
            AppIpirentry = new HashSet<AppIpirentry>();
            AssetTransferLocation = new HashSet<AssetTransfer>();
            AssetTransferTransferToNavigation = new HashSet<AssetTransfer>();
            BmrdisposalBox = new HashSet<BmrdisposalBox>();
            GroupLocationLink = new HashSet<GroupLocationLink>();
            LocationWiki = new HashSet<LocationWiki>();
            MasterDocumentInformation = new HashSet<MasterDocumentInformation>();
            WikiPageCopyInfo = new HashSet<WikiPageCopyInfo>();
        }

        public long LocationId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long? PlantId { get; set; }
        public string HyperLink { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Plant Plant { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<AppIpirentry> AppIpirentry { get; set; }
        public virtual ICollection<AssetTransfer> AssetTransferLocation { get; set; }
        public virtual ICollection<AssetTransfer> AssetTransferTransferToNavigation { get; set; }
        public virtual ICollection<BmrdisposalBox> BmrdisposalBox { get; set; }
        public virtual ICollection<GroupLocationLink> GroupLocationLink { get; set; }
        public virtual ICollection<LocationWiki> LocationWiki { get; set; }
        public virtual ICollection<MasterDocumentInformation> MasterDocumentInformation { get; set; }
        public virtual ICollection<WikiPageCopyInfo> WikiPageCopyInfo { get; set; }
    }
}
