﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SalesItemPackingInforLine
    {
        public SalesItemPackingInforLine()
        {
            SalesItemPackingRequirement = new HashSet<SalesItemPackingRequirement>();
        }

        public long SalesItemPackingInfoLineId { get; set; }
        public long? SalesItemPackingInfoId { get; set; }
        public int? SalesFactor { get; set; }
        public long? SalesPerPack { get; set; }
        public string Fpname { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual SalesItemPackingInfo SalesItemPackingInfo { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<SalesItemPackingRequirement> SalesItemPackingRequirement { get; set; }
    }
}
