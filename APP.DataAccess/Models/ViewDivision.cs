﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewDivision
    {
        public long DivisionId { get; set; }
        public string Code { get; set; }
        public long? CompanyId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public int StatusCodeId { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string StatusCode { get; set; }
        public string AddedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string PlantCode { get; set; }
        public string PlantDescription { get; set; }
        public int? CodeId { get; set; }
        public string DivisionDrop { get; set; }
    }
}
