﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class IcBmpManufacturingSite
    {
        public long IcBmpManufacturingSiteId { get; set; }
        public long? IcBmpId { get; set; }
        public long? ManufacturingSiteId { get; set; }

        public virtual Icbmp IcBmp { get; set; }
    }
}
