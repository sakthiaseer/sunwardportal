﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmployeeEmailInfo
    {
        public EmployeeEmailInfo()
        {
            EmployeeEmailInfoAuthority = new HashSet<EmployeeEmailInfoAuthority>();
            EmployeeEmailInfoForward = new HashSet<EmployeeEmailInfoForward>();
        }

        public long EmployeeEmailInfoId { get; set; }
        public long? EmailGuideId { get; set; }
        public long? SubscriptionId { get; set; }
        public long? EmployeeId { get; set; }

        public virtual ApplicationMasterDetail EmailGuide { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual ApplicationMasterDetail Subscription { get; set; }
        public virtual ICollection<EmployeeEmailInfoAuthority> EmployeeEmailInfoAuthority { get; set; }
        public virtual ICollection<EmployeeEmailInfoForward> EmployeeEmailInfoForward { get; set; }
    }
}
