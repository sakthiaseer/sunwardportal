﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class BlisterMachineCommonFieldMultiple
    {
        public long BlisterMachineCommonFieldId { get; set; }
        public long? BlisterMouldInformationId { get; set; }
        public long? CommonFiledsProductionMachineId { get; set; }

        public virtual BlisterMouldInformation BlisterMouldInformation { get; set; }
        public virtual CommonFieldsProductionMachine CommonFiledsProductionMachine { get; set; }
    }
}
