﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CompanyCalendarLine
    {
        public CompanyCalendarLine()
        {
            CompanyCalendarLineLink = new HashSet<CompanyCalendarLineLink>();
            CompanyCalendarLineLinkPrintUser = new HashSet<CompanyCalendarLineLinkPrintUser>();
            CompanyCalendarLineLocation = new HashSet<CompanyCalendarLineLocation>();
            CompanyCalendarLineMeetingNotes = new HashSet<CompanyCalendarLineMeetingNotes>();
            CompanyCalendarLineNotes = new HashSet<CompanyCalendarLineNotes>();
            CompanyCalendarLineUser = new HashSet<CompanyCalendarLineUser>();
            InverseCalanderParent = new HashSet<CompanyCalendarLine>();
        }

        public long CompanyCalendarLineId { get; set; }
        public long? CompanyCalendarId { get; set; }
        public DateTime? EventDate { get; set; }
        public long? VendorId { get; set; }
        public long? TypeOfServiceId { get; set; }
        public long? MachineId { get; set; }
        public string Notes { get; set; }
        public decimal? NoOfHoursRequire { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string AllEventFlag { get; set; }
        public TimeSpan? FromTime { get; set; }
        public TimeSpan? ToTime { get; set; }
        public string LinkDocComment { get; set; }
        public long? TypeOfEventId { get; set; }
        public DateTime? DueDate { get; set; }
        public string Subject { get; set; }
        public Guid? SessionId { get; set; }
        public long? CalenderStatusId { get; set; }
        public bool? Tentative { get; set; }
        public long? CalanderParentId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CompanyCalendarLine CalanderParent { get; set; }
        public virtual ApplicationMasterDetail CalenderStatus { get; set; }
        public virtual CompanyCalendar CompanyCalendar { get; set; }
        public virtual CommonFieldsProductionMachine Machine { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationMasterDetail TypeOfEvent { get; set; }
        public virtual Navvendor Vendor { get; set; }
        public virtual ICollection<CompanyCalendarLineLink> CompanyCalendarLineLink { get; set; }
        public virtual ICollection<CompanyCalendarLineLinkPrintUser> CompanyCalendarLineLinkPrintUser { get; set; }
        public virtual ICollection<CompanyCalendarLineLocation> CompanyCalendarLineLocation { get; set; }
        public virtual ICollection<CompanyCalendarLineMeetingNotes> CompanyCalendarLineMeetingNotes { get; set; }
        public virtual ICollection<CompanyCalendarLineNotes> CompanyCalendarLineNotes { get; set; }
        public virtual ICollection<CompanyCalendarLineUser> CompanyCalendarLineUser { get; set; }
        public virtual ICollection<CompanyCalendarLine> InverseCalanderParent { get; set; }
    }
}
