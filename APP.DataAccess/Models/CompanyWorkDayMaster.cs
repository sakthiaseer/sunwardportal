﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CompanyWorkDayMaster
    {
        public CompanyWorkDayMaster()
        {
            WorkDayShift = new HashSet<WorkDayShift>();
        }

        public long WorkDateId { get; set; }
        public string CodeNo { get; set; }
        public long? CountryId { get; set; }
        public long? StateId { get; set; }
        public string Description { get; set; }
        public string Reference { get; set; }
        public bool? IsMon { get; set; }
        public bool? IsTue { get; set; }
        public bool? IsWed { get; set; }
        public bool? IsThur { get; set; }
        public bool? IsFri { get; set; }
        public bool? IsSat { get; set; }
        public bool? IsSun { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Country Country { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual State State { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<WorkDayShift> WorkDayShift { get; set; }
    }
}
