﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmployeeIctrole
    {
        public long EmployeeRoleId { get; set; }
        public long? EmployeeIctinformationId { get; set; }
        public long? RoleId { get; set; }

        public virtual EmployeeIctinformation EmployeeIctinformation { get; set; }
        public virtual ApplicationRole Role { get; set; }
    }
}
