﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class LoginSession
    {
        public long LoginSessionId { get; set; }
        public long UserId { get; set; }
        public string Ipaddress { get; set; }
        public string MacAddress { get; set; }
        public DateTime? LoginTime { get; set; }
        public DateTime? LogoutTime { get; set; }
        public string SessionId { get; set; }

        public virtual ApplicationUser User { get; set; }
    }
}
