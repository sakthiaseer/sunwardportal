﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PackagingAluminiumFoilLine
    {
        public long PackagingAluminiumFoilLineId { get; set; }
        public long? PackagingAluminiumFoilId { get; set; }
        public decimal? RepeatedLength { get; set; }
        public decimal? BlisterWidth { get; set; }
        public decimal? BlisterLength { get; set; }
        public decimal? BlisterGap { get; set; }
        public decimal? BlisterAndFoilGap { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual PackagingAluminiumFoil PackagingAluminiumFoil { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
