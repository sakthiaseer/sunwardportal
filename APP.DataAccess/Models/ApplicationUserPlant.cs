﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApplicationUserPlant
    {
        public long ApplicationUserPlantId { get; set; }
        public long UserId { get; set; }
        public long PlantId { get; set; }
        public string DefaultPlant { get; set; }
        public long? EmployeeId { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Plant Plant { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
