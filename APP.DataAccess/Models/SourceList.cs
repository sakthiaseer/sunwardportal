﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SourceList
    {
        public SourceList()
        {
            CalibrationServiceInfo = new HashSet<CalibrationServiceInfo>();
            DeviceCatalogMaster = new HashSet<DeviceCatalogMaster>();
            DeviceManufacturerGroup = new HashSet<DeviceManufacturerGroup>();
            Ictcertificate = new HashSet<Ictcertificate>();
            IctcontactDetails = new HashSet<IctcontactDetails>();
            VendorSourceList = new HashSet<VendorSourceList>();
        }

        public long SourceListId { get; set; }
        public string Name { get; set; }
        public string SourceNo { get; set; }
        public long? OfficeAddressId { get; set; }
        public long? SiteAddressId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Address OfficeAddress { get; set; }
        public virtual Address SiteAddress { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<CalibrationServiceInfo> CalibrationServiceInfo { get; set; }
        public virtual ICollection<DeviceCatalogMaster> DeviceCatalogMaster { get; set; }
        public virtual ICollection<DeviceManufacturerGroup> DeviceManufacturerGroup { get; set; }
        public virtual ICollection<Ictcertificate> Ictcertificate { get; set; }
        public virtual ICollection<IctcontactDetails> IctcontactDetails { get; set; }
        public virtual ICollection<VendorSourceList> VendorSourceList { get; set; }
    }
}
