﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SellingPricingTiers
    {
        public long SellingPricingTiersId { get; set; }
        public long? SellingPriceInfoId { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Bonus { get; set; }
        public long? PricingMethodId { get; set; }
        public long? SellingCurrencyId { get; set; }
        public decimal? SellingPrice { get; set; }
        public decimal? MinQty { get; set; }

        public virtual ApplicationMasterDetail PricingMethod { get; set; }
        public virtual ApplicationMasterDetail SellingCurrency { get; set; }
        public virtual SellingPriceInformation SellingPriceInfo { get; set; }
    }
}
