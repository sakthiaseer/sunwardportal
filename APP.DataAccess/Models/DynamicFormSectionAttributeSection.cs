﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormSectionAttributeSection
    {
        public long DynamicFormSectionAttributeSectionId { get; set; }
        public long? DynamicFormSectionAttributeSectionParentId { get; set; }
        public long? DynamicFormSectionId { get; set; }
        public long? DynamicFormSectionSelectionId { get; set; }
        public string DynamicFormSectionSelectionById { get; set; }

        public virtual DynamicFormSection DynamicFormSection { get; set; }
        public virtual DynamicFormSectionAttributeSectionParent DynamicFormSectionAttributeSectionParent { get; set; }
    }
}
