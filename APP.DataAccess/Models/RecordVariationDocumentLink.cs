﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class RecordVariationDocumentLink
    {
        public long RecordVariationDocumentLinkId { get; set; }
        public long? RecordVariationId { get; set; }
        public long? RecordVariationLineId { get; set; }
        public bool? IsHeader { get; set; }
        public string DocumentLink { get; set; }

        public virtual RecordVariation RecordVariation { get; set; }
        public virtual RecordVariationLine RecordVariationLine { get; set; }
    }
}
