﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TopicToDoList
    {
        public long Id { get; set; }
        public long? TopicId { get; set; }
        public byte[] ToDoName { get; set; }
        public bool? Iscompleted { get; set; }
        public int? StatusCodeId { get; set; }
        public int? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public int? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
    }
}
