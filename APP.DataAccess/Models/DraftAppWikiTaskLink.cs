﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DraftAppWikiTaskLink
    {
        public long AppWikiTaskLinkId { get; set; }
        public string TaskLink { get; set; }
        public long? ApplicationWikiId { get; set; }
        public string Subject { get; set; }

        public virtual DraftApplicationWiki ApplicationWiki { get; set; }
    }
}
