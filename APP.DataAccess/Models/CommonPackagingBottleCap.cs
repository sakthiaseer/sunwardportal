﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CommonPackagingBottleCap
    {
        public CommonPackagingBottleCap()
        {
            BottleCapUseForItems = new HashSet<BottleCapUseForItems>();
        }

        public long BottleCapSpecificationId { get; set; }
        public long? PackagingItemSetInfoId { get; set; }
        public bool? VersionControl { get; set; }
        public bool? Set { get; set; }
        public decimal? CapMeasurement { get; set; }
        public string Code { get; set; }
        public long? PackagingMaterialColorId { get; set; }
        public long? CapTypeId { get; set; }
        public long? PackagingTypeId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<BottleCapUseForItems> BottleCapUseForItems { get; set; }
    }
}
