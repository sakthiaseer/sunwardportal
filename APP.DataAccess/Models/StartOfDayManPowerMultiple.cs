﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class StartOfDayManPowerMultiple
    {
        public long StartOfDayManpowerMultipleId { get; set; }
        public long? StartOfDayManpowerId { get; set; }
        public long? StartOfDayId { get; set; }

        public virtual StartOfDay StartOfDay { get; set; }
        public virtual Employee StartOfDayManpower { get; set; }
    }
}
