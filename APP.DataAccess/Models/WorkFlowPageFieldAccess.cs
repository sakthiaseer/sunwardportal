﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WorkFlowPageFieldAccess
    {
        public long WorkFlowPageFieldAccessId { get; set; }
        public long? WorkFlowPageFieldId { get; set; }
        public int? PermissionId { get; set; }
        public long? UserGroupId { get; set; }

        public virtual CodeMaster Permission { get; set; }
        public virtual UserGroup UserGroup { get; set; }
        public virtual WorkFlowPageField WorkFlowPageField { get; set; }
    }
}
