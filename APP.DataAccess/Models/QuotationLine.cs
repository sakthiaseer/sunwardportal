﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class QuotationLine
    {
        public long QuotationLineId { get; set; }
        public int? ItemTypeId { get; set; }
        public long? ItemId { get; set; }
        public string CustomerItemName { get; set; }
        public decimal? QtyinSmallest { get; set; }
        public string UominSmallest { get; set; }
        public decimal? Quantity { get; set; }
        public long? Uomid { get; set; }
        public int? TradingMethod { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? Amount { get; set; }
        public string Result { get; set; }
        public string JobSpecifications { get; set; }
        public string Packing { get; set; }
        public bool? IsTemp { get; set; }
        public long? QuotationHeaderId { get; set; }
        public string SessionId { get; set; }

        public virtual ItemMaster Item { get; set; }
        public virtual CodeMaster ItemType { get; set; }
        public virtual QuotationHeader QuotationHeader { get; set; }
        public virtual BaseUom Uom { get; set; }
    }
}
