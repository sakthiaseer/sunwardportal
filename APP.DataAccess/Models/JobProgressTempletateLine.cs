﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class JobProgressTempletateLine
    {
        public JobProgressTempletateLine()
        {
            JobProgressTemplateLineLineProcess = new HashSet<JobProgressTemplateLineLineProcess>();
            JobProgressTemplateLineProcess = new HashSet<JobProgressTemplateLineProcess>();
            JobProgressTemplateLineProcessStatus = new HashSet<JobProgressTemplateLineProcessStatus>();
        }

        public long JobProgressTemplateLineId { get; set; }
        public long? JobProgressTemplateId { get; set; }
        public long? ProfileId { get; set; }
        public string ActionNo { get; set; }
        public string SectionNo { get; set; }
        public string SectionDescription { get; set; }
        public string SubSectionNo { get; set; }
        public string SubSectionDescription { get; set; }
        public long? Pia { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public string Message { get; set; }
        public int? NoOfWorkingDays { get; set; }
        public int? SpecificDateOfEachMonth { get; set; }
        public string JobDescription { get; set; }
        public long? AssignedTo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser AssignedToNavigation { get; set; }
        public virtual JobProgressTemplate JobProgressTemplate { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationUser PiaNavigation { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<JobProgressTemplateLineLineProcess> JobProgressTemplateLineLineProcess { get; set; }
        public virtual ICollection<JobProgressTemplateLineProcess> JobProgressTemplateLineProcess { get; set; }
        public virtual ICollection<JobProgressTemplateLineProcessStatus> JobProgressTemplateLineProcessStatus { get; set; }
    }
}
