﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewLayOutPlanType
    {
        public long LayoutPlanTypeId { get; set; }
        public string Name { get; set; }
        public string VersionNo { get; set; }
        public string Description { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public Guid? SessionId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public int? StatusCodeId { get; set; }
        public string StatusCode { get; set; }
        public string AddedBy { get; set; }
        public string ModifiedBy { get; set; }
        public int? CodeId { get; set; }
    }
}
