﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Company
    {
        public Company()
        {
            Plant = new HashSet<Plant>();
            SunwardAssetList = new HashSet<SunwardAssetList>();
        }

        public long CompanyId { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }

        public virtual ICollection<Plant> Plant { get; set; }
        public virtual ICollection<SunwardAssetList> SunwardAssetList { get; set; }
    }
}
