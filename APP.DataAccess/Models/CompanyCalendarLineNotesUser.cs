﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CompanyCalendarLineNotesUser
    {
        public long CompanyCalendarLineNotesUserId { get; set; }
        public long? CompanyCalendarLineNotesId { get; set; }
        public long? UserId { get; set; }
        public int? StatusCodeId { get; set; }

        public virtual CompanyCalendarLineNotes CompanyCalendarLineNotes { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
