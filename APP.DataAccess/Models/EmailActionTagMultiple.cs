﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmailActionTagMultiple
    {
        public long Id { get; set; }
        public long TopicId { get; set; }
        public long? ActionId { get; set; }
    }
}
