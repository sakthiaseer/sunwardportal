﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ItemClassificationForms
    {
        public long ItemClassificationFormsId { get; set; }
        public long? ItemClassificationId { get; set; }
        public long? FormId { get; set; }

        public virtual ApplicationForm Form { get; set; }
        public virtual ItemClassificationMaster ItemClassification { get; set; }
    }
}
