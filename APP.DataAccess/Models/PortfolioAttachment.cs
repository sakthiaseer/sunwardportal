﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PortfolioAttachment
    {
        public long PortfolioAttachmentId { get; set; }
        public long? PortfolioLineId { get; set; }
        public long? DocumentId { get; set; }
        public long? PreviousDocumentId { get; set; }
        public bool? IsLatest { get; set; }
        public bool? IsLocked { get; set; }
        public string VersionNo { get; set; }
        public long? LockedByUserId { get; set; }
        public DateTime? LockedDate { get; set; }
        public long? UploadedByUserId { get; set; }
        public DateTime? UploadedDate { get; set; }
        public string FileName { get; set; }

        public virtual Documents Document { get; set; }
        public virtual ApplicationUser LockedByUser { get; set; }
        public virtual ApplicationUser UploadedByUser { get; set; }
    }
}
