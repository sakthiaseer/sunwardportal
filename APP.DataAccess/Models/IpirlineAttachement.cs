﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class IpirlineAttachement
    {
        public long IpirlineAttachementId { get; set; }
        public long? IpirlineId { get; set; }
        public long? IpirlineDocumentId { get; set; }

        public virtual Ipirline Ipirline { get; set; }
        public virtual IpirlineDocument IpirlineDocument { get; set; }
    }
}
