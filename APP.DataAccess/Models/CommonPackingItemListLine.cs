﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CommonPackingItemListLine
    {
        public long CommonPackingItemListLineId { get; set; }
        public long? PackingInformationLineId { get; set; }
        public long? PackagingItemNameId { get; set; }
        public long? UomId { get; set; }
        public decimal? NoOfUnits { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CommonPackagingItemHeader PackagingItemName { get; set; }
        public virtual CommonPackingInformationLine PackingInformationLine { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
