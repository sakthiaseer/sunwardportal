﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class RoutineInfo
    {
        public long RoutineInfoId { get; set; }
        public long? ProductionActivityRoutineAppLineId { get; set; }
        public string Description { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProductionActivityRoutineAppLine ProductionActivityRoutineAppLine { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
