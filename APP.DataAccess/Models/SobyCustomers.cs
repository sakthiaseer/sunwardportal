﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SobyCustomers
    {
        public SobyCustomers()
        {
            SobyCustomerContactInfo = new HashSet<SobyCustomerContactInfo>();
            SobyCustomerSunwardEquivalent = new HashSet<SobyCustomerSunwardEquivalent>();
            SobyCustomersAddress = new HashSet<SobyCustomersAddress>();
            SobyCustomersMasterAddress = new HashSet<SobyCustomersMasterAddress>();
            SobyCustomersSalesAddress = new HashSet<SobyCustomersSalesAddress>();
            SocustomersDelivery = new HashSet<SocustomersDelivery>();
            SocustomersIssue = new HashSet<SocustomersIssue>();
            SocustomersItemCrossReference = new HashSet<SocustomersItemCrossReference>();
            SocustomersSalesInfomation = new HashSet<SocustomersSalesInfomation>();
        }

        public long SobyCustomersId { get; set; }
        public bool? MustSupplyInFull { get; set; }
        public bool? MustFollowDeliveryDate { get; set; }
        public long? PaymentModeId { get; set; }
        public long? PlaceOrderId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public long? ActivationOfOrderId { get; set; }
        public string CustomerReferenceNo { get; set; }
        public string Description { get; set; }
        public long? CustomerPackingUomId { get; set; }
        public long? PerUomId { get; set; }
        public DateTime? ShelfLifeDate { get; set; }
        public bool? RequireSpecialPermit { get; set; }
        public DateTime? ShelfLifeRequirementDate { get; set; }
        public int? BillingAddressStatusId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster BillingAddressStatus { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<SobyCustomerContactInfo> SobyCustomerContactInfo { get; set; }
        public virtual ICollection<SobyCustomerSunwardEquivalent> SobyCustomerSunwardEquivalent { get; set; }
        public virtual ICollection<SobyCustomersAddress> SobyCustomersAddress { get; set; }
        public virtual ICollection<SobyCustomersMasterAddress> SobyCustomersMasterAddress { get; set; }
        public virtual ICollection<SobyCustomersSalesAddress> SobyCustomersSalesAddress { get; set; }
        public virtual ICollection<SocustomersDelivery> SocustomersDelivery { get; set; }
        public virtual ICollection<SocustomersIssue> SocustomersIssue { get; set; }
        public virtual ICollection<SocustomersItemCrossReference> SocustomersItemCrossReference { get; set; }
        public virtual ICollection<SocustomersSalesInfomation> SocustomersSalesInfomation { get; set; }
    }
}
