﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewDesignation
    {
        public long DesignationId { get; set; }
        public long? LevelId { get; set; }
        public string Code { get; set; }
        public int? HeadCount { get; set; }
        public long? SubSectionId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string SubSectionName { get; set; }
        public string SubSectionDescription { get; set; }
        public long? SectionId { get; set; }
        public string SectionName { get; set; }
        public string SectionDescription { get; set; }
        public long? DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentDescription { get; set; }
        public long? DivisionId { get; set; }
        public string DivisionName { get; set; }
        public string DivisionDescription { get; set; }
        public long? CompanyId { get; set; }
        public string PlantCode { get; set; }
        public string CompanyName { get; set; }
        public string LevelName { get; set; }
        public string StatusCode { get; set; }
        public string AddedBy { get; set; }
        public string ModifiedBy { get; set; }
    }
}
