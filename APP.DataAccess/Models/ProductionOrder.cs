﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionOrder
    {
        public ProductionOrder()
        {
            ProductionOrderMaster = new HashSet<ProductionOrderMaster>();
            ProductionOrderSplit = new HashSet<ProductionOrderSplit>();
        }

        public long ProductionOrderId { get; set; }
        public long? ProductId { get; set; }
        public long? ProductionBatchSizeId { get; set; }
        public int? NoOfProductionOrder { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Navitems Product { get; set; }
        public virtual NavmethodCodeBatch ProductionBatchSize { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ProductionOrderMaster> ProductionOrderMaster { get; set; }
        public virtual ICollection<ProductionOrderSplit> ProductionOrderSplit { get; set; }
    }
}
