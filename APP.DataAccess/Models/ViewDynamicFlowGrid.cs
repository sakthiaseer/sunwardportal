﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewDynamicFlowGrid
    {
        public long DynamicFlowId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string TotalProjectTime { get; set; }
        public long? Owner { get; set; }
        public string Recurrance { get; set; }
        public long DynamicFlowDetailId { get; set; }
        public string FlowName { get; set; }
        public string FlowDescription { get; set; }
        public long DynamicFlowStepId { get; set; }
        public string StepName { get; set; }
        public string StepDescription { get; set; }
        public string Instruction { get; set; }
        public long? PersonIncharge { get; set; }
        public long DynamicFlowStepDetailId { get; set; }
        public string TableName { get; set; }
        public long DynamicFlowStepDetailFieldId { get; set; }
        public string LableText { get; set; }
        public string FieldName { get; set; }
        public string FieldType { get; set; }
        public string FieldValue { get; set; }
        public bool? IsRequired { get; set; }
    }
}
