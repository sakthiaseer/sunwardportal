﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ChatGroup
    {
        public ChatGroup()
        {
            ChatGroupUsers = new HashSet<ChatGroupUsers>();
            ChatMessage = new HashSet<ChatMessage>();
        }

        public long ChatGroupId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid SessionId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ChatGroupUsers> ChatGroupUsers { get; set; }
        public virtual ICollection<ChatMessage> ChatMessage { get; set; }
    }
}
