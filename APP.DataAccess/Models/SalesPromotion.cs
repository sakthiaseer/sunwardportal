﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SalesPromotion
    {
        public SalesPromotion()
        {
            SalesMainPromotionList = new HashSet<SalesMainPromotionList>();
        }

        public long SalesPromotionId { get; set; }
        public long? CompanyId { get; set; }
        public long? ProfileId { get; set; }
        public string PromotionNo { get; set; }
        public long? PurposeOfPromotionId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<SalesMainPromotionList> SalesMainPromotionList { get; set; }
    }
}
