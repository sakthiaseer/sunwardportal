﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PlantMaintenanceEntry
    {
        public PlantMaintenanceEntry()
        {
            PlantMaintenanceEntryLine = new HashSet<PlantMaintenanceEntryLine>();
        }

        public long PlantEntryId { get; set; }
        public long? CompanyId { get; set; }
        public long? SiteId { get; set; }
        public long? ZoneId { get; set; }
        public long? LocationId { get; set; }
        public long? AreaId { get; set; }
        public long? SpecificAreaId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Ictmaster SpecificArea { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<PlantMaintenanceEntryLine> PlantMaintenanceEntryLine { get; set; }
    }
}
