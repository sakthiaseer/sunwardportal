﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ActiveFlowFormValue
    {
        public long ActiveFlowFormValueId { get; set; }
        public long ActieFlowDetailId { get; set; }
        public long? PageBlockId { get; set; }
        public string ColumnName { get; set; }
        public string ColumnValue { get; set; }
        public string Type { get; set; }

        public virtual ActiveFlowDetails ActieFlowDetail { get; set; }
        public virtual WorkFlowPageBlock PageBlock { get; set; }
    }
}
