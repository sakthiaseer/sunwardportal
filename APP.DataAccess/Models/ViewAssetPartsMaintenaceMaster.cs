﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewAssetPartsMaintenaceMaster
    {
        public long AssetPartsMaintenaceMasterId { get; set; }
        public long? AssetCatalogMasterId { get; set; }
        public long? AssetCatalogId { get; set; }
        public long? SectionId { get; set; }
        public long? SubSectionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string StatusCode { get; set; }
        public string AddedBy { get; set; }
        public string ModifiedBy { get; set; }
        public int? CodeId { get; set; }
        public string AssetCatalog { get; set; }
        public string AssetSection { get; set; }
        public string AssetSubSection { get; set; }
    }
}
