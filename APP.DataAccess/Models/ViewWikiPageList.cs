﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewWikiPageList
    {
        public long PageId { get; set; }
        public long? CategoryId { get; set; }
        public long? DepartmentId { get; set; }
        public long? LanguageId { get; set; }
        public long? ParentPageId { get; set; }
        public long? GroupId { get; set; }
        public long? SectionId { get; set; }
        public string Titile { get; set; }
        public string PageContent { get; set; }
        public string EditContent { get; set; }
        public string ShortDescription { get; set; }
        public string Notes { get; set; }
        public long? WikiSearchId { get; set; }
        public string SessionId { get; set; }
        public string SearchDocument { get; set; }
        public string WorkingDocument { get; set; }
        public bool? NoTransalation { get; set; }
        public bool? IsChinese { get; set; }
        public string ProposedTag { get; set; }
        public bool? IsMalay { get; set; }
        public int? MethodOfContent { get; set; }
        public bool? IsChangeApproval { get; set; }
        public bool? IsGmprelated { get; set; }
        public bool? SmecommentRequired { get; set; }
        public DateTime? SmedueDate { get; set; }
        public long? PrimaryApproverId { get; set; }
        public long? NextApproverId { get; set; }
        public bool? ApproveMySelf { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public long? TableId { get; set; }
        public long? WorkFlowId { get; set; }
        public int? VersionType { get; set; }
        public DateTime? NextReviewDate { get; set; }
        public bool? UnderConstruction { get; set; }
        public bool? NeedFurtherReview { get; set; }
        public string SameControlStatement { get; set; }
        public string ControlStatement { get; set; }
        public long? OwnerId { get; set; }
        public long? Assignee { get; set; }
        public string ReviewComment { get; set; }
        public DateTime? PublishDate { get; set; }
        public DateTime? StartReviewDate { get; set; }
        public bool? ContentApproved { get; set; }
        public bool? Comment { get; set; }
        public string CommentDocument { get; set; }
        public string IssueNo { get; set; }
        public string ChangeControlNo { get; set; }
        public DateTime? ImplementationDate { get; set; }
        public string GroupReference { get; set; }
        public string NextAction { get; set; }
        public bool? IsPublishToAll { get; set; }
        public bool? IsTestPage { get; set; }
        public bool? IsReturned { get; set; }
        public long? SmechairPerson { get; set; }
        public bool? ApproveEditPage { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string AddedByUser { get; set; }
        public string ModifiedUser { get; set; }
        public string StatusCode { get; set; }
        public string Section { get; set; }
        public string Group { get; set; }
        public string Category { get; set; }
        public string Department { get; set; }
        public string Owner { get; set; }
        public string TrainingMethod { get; set; }
        public string Companyids { get; set; }
        public string VersionNo { get; set; }
        public string ArchiveReason { get; set; }
        public int? TypeOfRightId { get; set; }
        public string Primaryapprover { get; set; }
        public string RevisionNo { get; set; }
    }
}
