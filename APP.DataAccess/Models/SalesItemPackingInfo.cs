﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SalesItemPackingInfo
    {
        public SalesItemPackingInfo()
        {
            SalesItemPackingInforLine = new HashSet<SalesItemPackingInforLine>();
            SalesItemPackingRequirement = new HashSet<SalesItemPackingRequirement>();
        }

        public long SalesItemPackingInfoId { get; set; }
        public long? ManufacturingSiteId { get; set; }
        public long? FinishProductGeneralInfoId { get; set; }
        public long? FinishProductGeneralInfoLineId { get; set; }
        public string ManufacturingSite { get; set; }
        public string RegisterCountry { get; set; }
        public string ProductionRegistrationHolder { get; set; }
        public string ProductName { get; set; }
        public string ProductOwner { get; set; }
        public string PackagingType { get; set; }
        public decimal? SmallestPackQty { get; set; }
        public string SmallestQtyunit { get; set; }
        public string SmallestQtyperPack { get; set; }
        public string RegistrationFactor { get; set; }
        public string RegistrationPerPack { get; set; }
        public bool? MasterPackagingStatus { get; set; }
        public bool? SalesInformation { get; set; }
        public bool? IsSalesPackRegistration { get; set; }
        public int? SalesFactor { get; set; }
        public string SalePerPack { get; set; }
        public long? Fpno { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string Fpname { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual FinishProductGeneralInfo FinishProductGeneralInfo { get; set; }
        public virtual FinishProdcutGeneralInfoLine FinishProductGeneralInfoLine { get; set; }
        public virtual DocumentProfileNoSeries FpnoNavigation { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<SalesItemPackingInforLine> SalesItemPackingInforLine { get; set; }
        public virtual ICollection<SalesItemPackingRequirement> SalesItemPackingRequirement { get; set; }
    }
}
