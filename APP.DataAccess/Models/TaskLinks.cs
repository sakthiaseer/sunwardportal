﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TaskLinks
    {
        public long TaskLinkId { get; set; }
        public long? TaskId { get; set; }
        public long? LinkedTaskId { get; set; }
        public string Description { get; set; }

        public virtual TaskMaster LinkedTask { get; set; }
        public virtual TaskMaster Task { get; set; }
    }
}
