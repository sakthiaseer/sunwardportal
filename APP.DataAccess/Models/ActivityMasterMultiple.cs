﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ActivityMasterMultiple
    {
        public long ActivityMasterMultipleId { get; set; }
        public long? AcitivityMasterId { get; set; }
        public long? ProductionActivityAppLineId { get; set; }

        public virtual ApplicationMasterDetail AcitivityMaster { get; set; }
        public virtual ProductionActivityAppLine ProductionActivityAppLine { get; set; }
    }
}
