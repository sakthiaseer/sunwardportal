﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SaleAdhocNovateOrder
    {
        public long SalesAdhocNovateOrderId { get; set; }
        public long? SalesAdhocNovateId { get; set; }
        public long? SalesAdhocId { get; set; }

        public virtual SalesOrderEntry SalesAdhoc { get; set; }
        public virtual SalesAdhocNovate SalesAdhocNovate { get; set; }
    }
}
