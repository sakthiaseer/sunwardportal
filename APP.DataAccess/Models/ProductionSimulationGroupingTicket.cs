﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionSimulationGroupingTicket
    {
        public long ProductionSimulationGroupingTicketId { get; set; }
        public long? MethodCodeId { get; set; }
        public long? ItemId { get; set; }
        public decimal? ExistingTicket { get; set; }
        public decimal? FullTicket { get; set; }
        public decimal? SplitTicket { get; set; }
        public decimal? Qty { get; set; }
        public DateTime? WeekOfDate { get; set; }
        public int? WeekNo { get; set; }
        public DateTime? AddedDate { get; set; }
        public int? StatusCodeId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? AddedByUserId { get; set; }
        public long? ModifiedByUserId { get; set; }
        public string Remarks { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual NavMethodCode MethodCode { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
