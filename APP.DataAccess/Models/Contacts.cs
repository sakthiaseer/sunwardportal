﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Contacts
    {
        public Contacts()
        {
            ContactActivities = new HashSet<ContactActivities>();
        }

        public long ContactId { get; set; }
        public long? BusinessAccount { get; set; }
        public long? Owner { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long? WorkGroup { get; set; }
        public long? ContactClass { get; set; }
        public string JobTitle { get; set; }
        public int? StatusCodeId { get; set; }
        public int? Type { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public long? StateId { get; set; }
        public long? CountryId { get; set; }
        public string PostalCode { get; set; }
        public bool? IsDataPrivacy { get; set; }
        public DateTime? DateOfConsent { get; set; }
        public DateTime? ConsentExpires { get; set; }
        public int? ContactMethod { get; set; }
        public string AccountReference { get; set; }
        public string ParentAccount { get; set; }
        public string InComingActivity { get; set; }
        public string OutGoingActivity { get; set; }
        public string CompanyName { get; set; }
        public DateTime? Dob { get; set; }
        public string SpouseName { get; set; }
        public int? Gender { get; set; }
        public int? MaritalStatus { get; set; }
        public DateTime? LastAccessDate { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Country Country { get; set; }
        public virtual CodeMaster GenderNavigation { get; set; }
        public virtual CodeMaster MaritalStatusNavigation { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationUser OwnerNavigation { get; set; }
        public virtual State State { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CodeMaster TypeNavigation { get; set; }
        public virtual ICollection<ContactActivities> ContactActivities { get; set; }
    }
}
