﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ItemClassificationMaster
    {
        public ItemClassificationMaster()
        {
            ClassificationBmr = new HashSet<ClassificationBmr>();
            ClassificationProduction = new HashSet<ClassificationProduction>();
            CommonFieldsProductionMachine = new HashSet<CommonFieldsProductionMachine>();
            CommonMasterToolingClassification = new HashSet<CommonMasterToolingClassification>();
            CommonPackagingItemHeader = new HashSet<CommonPackagingItemHeader>();
            CommonProcess = new HashSet<CommonProcess>();
            CompanyListing = new HashSet<CompanyListing>();
            FpcommonField = new HashSet<FpcommonField>();
            FpdrugClassification = new HashSet<FpdrugClassification>();
            HandlingOfShipmentClassification = new HashSet<HandlingOfShipmentClassification>();
            InverseParent = new HashSet<ItemClassificationMaster>();
            ItemClassificationAccess = new HashSet<ItemClassificationAccess>();
            ItemClassificationForms = new HashSet<ItemClassificationForms>();
            ItemClassificationHeader = new HashSet<ItemClassificationHeader>();
            ItemClassificationPermission = new HashSet<ItemClassificationPermission>();
            ItemClassificationTransport = new HashSet<ItemClassificationTransport>();
            MedCatalogClassification = new HashSet<MedCatalogClassification>();
            ProductionActivityClassification = new HashSet<ProductionActivityClassification>();
        }

        public long ItemClassificationId { get; set; }
        public long? MainClassificationId { get; set; }
        public long? ParentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string FormTitle { get; set; }
        public bool? IsForm { get; set; }
        public long? FormId { get; set; }
        public long? ProfileId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileReferenceNo { get; set; }
        public int? ClassificationTypeId { get; set; }
        public string ClassificationLinkType { get; set; }
        public Guid? SessionId { get; set; }
        public bool? IsApprovalForm { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster ClassificationType { get; set; }
        public virtual ApplicationForm Form { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ItemClassificationMaster Parent { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ClassificationBmr> ClassificationBmr { get; set; }
        public virtual ICollection<ClassificationProduction> ClassificationProduction { get; set; }
        public virtual ICollection<CommonFieldsProductionMachine> CommonFieldsProductionMachine { get; set; }
        public virtual ICollection<CommonMasterToolingClassification> CommonMasterToolingClassification { get; set; }
        public virtual ICollection<CommonPackagingItemHeader> CommonPackagingItemHeader { get; set; }
        public virtual ICollection<CommonProcess> CommonProcess { get; set; }
        public virtual ICollection<CompanyListing> CompanyListing { get; set; }
        public virtual ICollection<FpcommonField> FpcommonField { get; set; }
        public virtual ICollection<FpdrugClassification> FpdrugClassification { get; set; }
        public virtual ICollection<HandlingOfShipmentClassification> HandlingOfShipmentClassification { get; set; }
        public virtual ICollection<ItemClassificationMaster> InverseParent { get; set; }
        public virtual ICollection<ItemClassificationAccess> ItemClassificationAccess { get; set; }
        public virtual ICollection<ItemClassificationForms> ItemClassificationForms { get; set; }
        public virtual ICollection<ItemClassificationHeader> ItemClassificationHeader { get; set; }
        public virtual ICollection<ItemClassificationPermission> ItemClassificationPermission { get; set; }
        public virtual ICollection<ItemClassificationTransport> ItemClassificationTransport { get; set; }
        public virtual ICollection<MedCatalogClassification> MedCatalogClassification { get; set; }
        public virtual ICollection<ProductionActivityClassification> ProductionActivityClassification { get; set; }
    }
}
