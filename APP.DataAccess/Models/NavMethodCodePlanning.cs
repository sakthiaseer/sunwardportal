﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavMethodCodePlanning
    {
        public long MethodCodePlanningId { get; set; }
        public long? MethodCodeId { get; set; }
        public string BatchSize { get; set; }
        public int? NoOfTickets { get; set; }
        public int? PackQty { get; set; }
        public bool? Jan { get; set; }
        public bool? Feb { get; set; }
        public bool? Mar { get; set; }
        public bool? Apr { get; set; }
        public bool? May { get; set; }
        public bool? Jun { get; set; }
        public bool? Jul { get; set; }
        public bool? Aug { get; set; }
        public bool? Sep { get; set; }
        public bool? Oct { get; set; }
        public bool? Nov { get; set; }
        public bool? Dec { get; set; }

        public virtual NavMethodCode MethodCode { get; set; }
    }
}
