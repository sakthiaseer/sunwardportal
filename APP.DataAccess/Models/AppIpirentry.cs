﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AppIpirentry
    {
        public long IpirentryId { get; set; }
        public string Ipirno { get; set; }
        public int? IpiruploadType { get; set; }
        public string IpiruploadName { get; set; }
        public long? AdditionalProcessId { get; set; }
        public int? NoOfDrums { get; set; }
        public long? LocationId { get; set; }
        public int? NoOfManpower { get; set; }
        public int? NoOfHours { get; set; }
        public long? DocumentId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual AdditionalProcess AdditionalProcess { get; set; }
        public virtual Documents Document { get; set; }
        public virtual CodeMaster IpiruploadTypeNavigation { get; set; }
        public virtual Locations Location { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
