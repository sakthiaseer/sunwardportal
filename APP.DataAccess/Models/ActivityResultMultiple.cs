﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ActivityResultMultiple
    {
        public long ActivityResultMultipleId { get; set; }
        public long? ProductionActivityAppLineId { get; set; }
        public long? AcitivityResultId { get; set; }

        public virtual ApplicationMasterDetail AcitivityResult { get; set; }
        public virtual ProductionActivityAppLine ProductionActivityAppLine { get; set; }
    }
}
