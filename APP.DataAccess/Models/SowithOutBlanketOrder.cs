﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SowithOutBlanketOrder
    {
        public SowithOutBlanketOrder()
        {
            SolotInformation = new HashSet<SolotInformation>();
        }

        public long SowithoutBlanketOrderId { get; set; }
        public long? SalesOrderId { get; set; }
        public long? ShipToCodeId { get; set; }
        public long? SoproductId { get; set; }
        public bool? IsLotInformation { get; set; }
        public decimal? OrderQty { get; set; }
        public long? OrderCurrencyId { get; set; }
        public decimal? SellingPrice { get; set; }
        public long? PriceUnitId { get; set; }
        public bool? IsFoc { get; set; }
        public string Foc { get; set; }
        public DateTime? RequestShipmentDate { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual SalesOrder SalesOrder { get; set; }
        public virtual SobyCustomersAddress ShipToCode { get; set; }
        public virtual SocustomersItemCrossReference Soproduct { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<SolotInformation> SolotInformation { get; set; }
    }
}
