﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionActivityPlanningAppLineDoc
    {
        public long ProductionActivityPlanningAppLineDocId { get; set; }
        public long? ProductionActivityPlanningAppLineId { get; set; }
        public long? DocumentId { get; set; }
        public string Type { get; set; }
        public long? IpirReportId { get; set; }

        public virtual Documents Document { get; set; }
        public virtual IpirReport IpirReport { get; set; }
        public virtual ProductionActivityPlanningAppLine ProductionActivityPlanningAppLine { get; set; }
    }
}
