﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormWorkFlowForm
    {
        public DynamicFormWorkFlowForm()
        {
            DynamicFormWorkFlowApprovedForm = new HashSet<DynamicFormWorkFlowApprovedForm>();
            DynamicFormWorkFlowFormDelegate = new HashSet<DynamicFormWorkFlowFormDelegate>();
            DynamicFormWorkFlowSectionForm = new HashSet<DynamicFormWorkFlowSectionForm>();
        }

        public long DynamicFormWorkFlowFormId { get; set; }
        public long? DynamicFormWorkFlowSectionId { get; set; }
        public long? DynamicFormDataId { get; set; }
        public long? UserId { get; set; }
        public DateTime? CompletedDate { get; set; }
        public int? FlowStatusId { get; set; }
        public int? SequenceNo { get; set; }
        public long? DynamicFormWorkFlowId { get; set; }
        public bool? IsAllowDelegateUserForm { get; set; }

        public virtual DynamicFormData DynamicFormData { get; set; }
        public virtual DynamicFormWorkFlow DynamicFormWorkFlow { get; set; }
        public virtual DynamicFormWorkFlowSection DynamicFormWorkFlowSection { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual ICollection<DynamicFormWorkFlowApprovedForm> DynamicFormWorkFlowApprovedForm { get; set; }
        public virtual ICollection<DynamicFormWorkFlowFormDelegate> DynamicFormWorkFlowFormDelegate { get; set; }
        public virtual ICollection<DynamicFormWorkFlowSectionForm> DynamicFormWorkFlowSectionForm { get; set; }
    }
}
