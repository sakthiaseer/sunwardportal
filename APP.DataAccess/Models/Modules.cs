﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Modules
    {
        public Modules()
        {
            ApproverSetup = new HashSet<ApproverSetup>();
            WorkFlowApproverSetup = new HashSet<WorkFlowApproverSetup>();
        }

        public long ModuleId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ApproverSetup> ApproverSetup { get; set; }
        public virtual ICollection<WorkFlowApproverSetup> WorkFlowApproverSetup { get; set; }
    }
}
