﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Ipir
    {
        public Ipir()
        {
            IpirissueRelatedMultiple = new HashSet<IpirissueRelatedMultiple>();
            IpirissueTitleMultiple = new HashSet<IpirissueTitleMultiple>();
            Ipirline = new HashSet<Ipirline>();
        }

        public long Ipirid { get; set; }
        public long? CompanyId { get; set; }
        public string Ipirno { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? Time { get; set; }
        public string ReportBy { get; set; }
        public string DetectedBy { get; set; }
        public long? DepartmentId { get; set; }
        public long? LocationId { get; set; }
        public long? MachineId { get; set; }
        public long? ProductId { get; set; }
        public long? BatchNoId { get; set; }
        public string IssueDescription { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual Department Department { get; set; }
        public virtual Ictmaster Location { get; set; }
        public virtual CommonFieldsProductionMachine Machine { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Navitems Product { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<IpirissueRelatedMultiple> IpirissueRelatedMultiple { get; set; }
        public virtual ICollection<IpirissueTitleMultiple> IpirissueTitleMultiple { get; set; }
        public virtual ICollection<Ipirline> Ipirline { get; set; }
    }
}
