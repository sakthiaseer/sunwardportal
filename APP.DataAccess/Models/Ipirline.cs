﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Ipirline
    {
        public Ipirline()
        {
            IpirlineAttachement = new HashSet<IpirlineAttachement>();
        }

        public long IpirlineId { get; set; }
        public long? Ipirid { get; set; }
        public long? Picid { get; set; }
        public string Picadvice { get; set; }
        public long? Ipiraction { get; set; }
        public string AssignTo { get; set; }
        public Guid? DiscussionSessionId { get; set; }
        public Guid? AttachSessionId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Ipir Ipir { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationUser Pic { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<IpirlineAttachement> IpirlineAttachement { get; set; }
    }
}
