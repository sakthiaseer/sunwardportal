﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FbOutputCartons
    {
        public long FbOutputCartonId { get; set; }
        public string LocationName { get; set; }
        public string ProductionOrderNo { get; set; }
        public string CartonNo { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string BatchNo { get; set; }
        public bool? FullCarton { get; set; }
        public decimal? FullCartonQty { get; set; }
        public decimal? LooseCartonQty { get; set; }
        public string PalletNo { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public int? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
    }
}
