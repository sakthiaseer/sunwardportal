﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CompanyListingCustomerCode
    {
        public long CompanyListingCustomerCodeId { get; set; }
        public long? CompanyListingId { get; set; }
        public long? CustomerCodeId { get; set; }

        public virtual CompanyListing CompanyListing { get; set; }
        public virtual ApplicationMasterDetail CustomerCode { get; set; }
    }
}
