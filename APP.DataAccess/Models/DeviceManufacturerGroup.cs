﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DeviceManufacturerGroup
    {
        public long DeviceManufacturerId { get; set; }
        public long? DeviceCatalogMasterId { get; set; }
        public long? ManufacturerSourceListId { get; set; }

        public virtual DeviceCatalogMaster DeviceCatalogMaster { get; set; }
        public virtual SourceList ManufacturerSourceList { get; set; }
    }
}
