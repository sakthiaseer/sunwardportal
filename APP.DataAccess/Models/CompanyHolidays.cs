﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CompanyHolidays
    {
        public CompanyHolidays()
        {
            CompanyHolidayDetail = new HashSet<CompanyHolidayDetail>();
        }

        public long CompanyHolidayId { get; set; }
        public int? CalendarYear { get; set; }
        public long? PlantId { get; set; }
        public long? StateId { get; set; }
        public decimal? NoOfHolidays { get; set; }
        public string Reference { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual State State { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<CompanyHolidayDetail> CompanyHolidayDetail { get; set; }
    }
}
