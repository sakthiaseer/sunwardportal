﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class QuotationAward
    {
        public QuotationAward()
        {
            QuotationAwardLine = new HashSet<QuotationAwardLine>();
        }

        public long QuotationAwardId { get; set; }
        public long? TypeOfOrderId { get; set; }
        public long? ItemId { get; set; }
        public decimal? TotalQty { get; set; }
        public bool? IsCommitment { get; set; }
        public decimal? CommittedQty { get; set; }
        public DateTime? CommittedOn { get; set; }
        public bool? IsFollowDate { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public long? QuotationHistoryId { get; set; }
        public decimal? Price { get; set; }
        public string ReasonForDifferentQty { get; set; }
        public string ReasonForDifferentPrice { get; set; }
        public bool? IsDifferentQty { get; set; }
        public bool? IsDifferentPrice { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual GenericCodes Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual QuotationHistory QuotationHistory { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationMasterDetail TypeOfOrder { get; set; }
        public virtual ICollection<QuotationAwardLine> QuotationAwardLine { get; set; }
    }
}
