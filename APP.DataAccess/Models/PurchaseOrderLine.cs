﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PurchaseOrderLine
    {
        public long PurchaseOrderLineId { get; set; }
        public long? PurchaseOrderId { get; set; }
        public long? NavItemId { get; set; }
        public decimal? Qty { get; set; }
        public long? CurrencyId { get; set; }
        public decimal? UnitPrice { get; set; }
        public long? DeliveryTypeId { get; set; }
        public DateTime? ExpectedDeliveryDate { get; set; }
        public long? DeliveryToId { get; set; }
        public string Remarks { get; set; }

        public virtual Plant DeliveryTo { get; set; }
        public virtual Navitems NavItem { get; set; }
        public virtual PurchaseOrder PurchaseOrder { get; set; }
    }
}
