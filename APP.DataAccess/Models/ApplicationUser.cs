﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApplicationUser
    {
        public ApplicationUser()
        {
            AcentryAddedByUser = new HashSet<Acentry>();
            AcentryLinesAddedByUser = new HashSet<AcentryLines>();
            AcentryLinesModifiedByUser = new HashSet<AcentryLines>();
            AcentryModifiedByUser = new HashSet<Acentry>();
            AcitemsAddedByUser = new HashSet<Acitems>();
            AcitemsModifiedByUser = new HashSet<Acitems>();
            ActiveFlow = new HashSet<ActiveFlow>();
            ActiveFlowDetails = new HashSet<ActiveFlowDetails>();
            ActivityEmailTopicsAddedByUser = new HashSet<ActivityEmailTopics>();
            ActivityEmailTopicsModifiedByUser = new HashSet<ActivityEmailTopics>();
            AdditionalProcessAddedByUser = new HashSet<AdditionalProcess>();
            AdditionalProcessModifiedByUser = new HashSet<AdditionalProcess>();
            AppIpirentryAddedByUser = new HashSet<AppIpirentry>();
            AppIpirentryModifiedByUser = new HashSet<AppIpirentry>();
            AppSamplingAddedByUser = new HashSet<AppSampling>();
            AppSamplingLineAddedByUser = new HashSet<AppSamplingLine>();
            AppSamplingLineModifiedByUser = new HashSet<AppSamplingLine>();
            AppSamplingModifiedByUser = new HashSet<AppSampling>();
            AppTranDocumentLineAddedByUser = new HashSet<AppTranDocumentLine>();
            AppTranDocumentLineModifiedByUser = new HashSet<AppTranDocumentLine>();
            AppTranLfromLtoAddedByUser = new HashSet<AppTranLfromLto>();
            AppTranLfromLtoLineAddedByUser = new HashSet<AppTranLfromLtoLine>();
            AppTranLfromLtoLineModifiedByUser = new HashSet<AppTranLfromLtoLine>();
            AppTranLfromLtoModifiedByUser = new HashSet<AppTranLfromLto>();
            AppTranStockAddedByUser = new HashSet<AppTranStock>();
            AppTranStockLineAddedByUser = new HashSet<AppTranStockLine>();
            AppTranStockLineModifiedByUser = new HashSet<AppTranStockLine>();
            AppTranStockModifiedByUser = new HashSet<AppTranStock>();
            AppconsumptionEntryAddedByUser = new HashSet<AppconsumptionEntry>();
            AppconsumptionEntryModifiedByUser = new HashSet<AppconsumptionEntry>();
            AppconsumptionLines = new HashSet<AppconsumptionLines>();
            AppdispenserDispensingAddedByUser = new HashSet<AppdispenserDispensing>();
            AppdispenserDispensingModifiedByUser = new HashSet<AppdispenserDispensing>();
            AppdrummingAddedByUser = new HashSet<Appdrumming>();
            AppdrummingModifiedByUser = new HashSet<Appdrumming>();
            ApplicationAbbreviationAddedByUser = new HashSet<ApplicationAbbreviation>();
            ApplicationAbbreviationModifiedByUser = new HashSet<ApplicationAbbreviation>();
            ApplicationFormAddedByUser = new HashSet<ApplicationForm>();
            ApplicationFormFields = new HashSet<ApplicationFormFields>();
            ApplicationFormModifiedByUser = new HashSet<ApplicationForm>();
            ApplicationFormPersonIncharge = new HashSet<ApplicationForm>();
            ApplicationFormSearchAddedByUser = new HashSet<ApplicationFormSearch>();
            ApplicationFormSearchModifiedByUser = new HashSet<ApplicationFormSearch>();
            ApplicationGlossaryAddedByUser = new HashSet<ApplicationGlossary>();
            ApplicationGlossaryModifiedByUser = new HashSet<ApplicationGlossary>();
            ApplicationMasterAccess = new HashSet<ApplicationMasterAccess>();
            ApplicationMasterChildAddedByUser = new HashSet<ApplicationMasterChild>();
            ApplicationMasterChildModifiedByUser = new HashSet<ApplicationMasterChild>();
            ApplicationMasterDetailAddedByUser = new HashSet<ApplicationMasterDetail>();
            ApplicationMasterDetailModifiedByUser = new HashSet<ApplicationMasterDetail>();
            ApplicationMasterParentAddedByUser = new HashSet<ApplicationMasterParent>();
            ApplicationMasterParentModifiedByUser = new HashSet<ApplicationMasterParent>();
            ApplicationReminderAddedByUser = new HashSet<ApplicationReminder>();
            ApplicationReminderModifiedUser = new HashSet<ApplicationReminder>();
            ApplicationRoleAddedByUser = new HashSet<ApplicationRole>();
            ApplicationRoleModifiedByUser = new HashSet<ApplicationRole>();
            ApplicationUserPlant = new HashSet<ApplicationUserPlant>();
            ApplicationUserRole = new HashSet<ApplicationUserRole>();
            ApplicationWikiAddedByUser = new HashSet<ApplicationWiki>();
            ApplicationWikiLineAddedByUser = new HashSet<ApplicationWikiLine>();
            ApplicationWikiLineDutyAddedByUser = new HashSet<ApplicationWikiLineDuty>();
            ApplicationWikiLineDutyModifiedByUser = new HashSet<ApplicationWikiLineDuty>();
            ApplicationWikiLineModifiedByUser = new HashSet<ApplicationWikiLine>();
            ApplicationWikiLineNotify = new HashSet<ApplicationWikiLineNotify>();
            ApplicationWikiModifiedByUser = new HashSet<ApplicationWiki>();
            ApplicationWikiRecurrenceAddedByUser = new HashSet<ApplicationWikiRecurrence>();
            ApplicationWikiRecurrenceModifiedByUser = new HashSet<ApplicationWikiRecurrence>();
            ApplicationWikiWikiEntryBy = new HashSet<ApplicationWiki>();
            AppmaterialReturnAddedByUser = new HashSet<AppmaterialReturn>();
            AppmaterialReturnModifiedByUser = new HashSet<AppmaterialReturn>();
            ApprovalDetailsAddedByUser = new HashSet<ApprovalDetails>();
            ApprovalDetailsModifiedByUser = new HashSet<ApprovalDetails>();
            ApproveMasterAddedByUser = new HashSet<ApproveMaster>();
            ApproveMasterModifiedByUser = new HashSet<ApproveMaster>();
            ApproverSetupAddedByUser = new HashSet<ApproverSetup>();
            ApproverSetupApprover = new HashSet<ApproverSetup>();
            ApproverSetupModifiedByUser = new HashSet<ApproverSetup>();
            AppsupervisorDispensingEntryAddedByUser = new HashSet<AppsupervisorDispensingEntry>();
            AppsupervisorDispensingEntryModifiedByUser = new HashSet<AppsupervisorDispensingEntry>();
            ApptransferRelcassEntryAddedByUser = new HashSet<ApptransferRelcassEntry>();
            ApptransferRelcassEntryModifiedByUser = new HashSet<ApptransferRelcassEntry>();
            AssetAssignmentAddedByUser = new HashSet<AssetAssignment>();
            AssetAssignmentAssetManagedByNavigation = new HashSet<AssetAssignment>();
            AssetAssignmentAssignedToNavigation = new HashSet<AssetAssignment>();
            AssetAssignmentModifiedByUser = new HashSet<AssetAssignment>();
            AssetCatalogMasterAddedByUser = new HashSet<AssetCatalogMaster>();
            AssetCatalogMasterModifiedByUser = new HashSet<AssetCatalogMaster>();
            AssetDisposalAddedByUser = new HashSet<AssetDisposal>();
            AssetDisposalModifiedByUser = new HashSet<AssetDisposal>();
            AssetEquipmentMaintenaceMasterAddedByUser = new HashSet<AssetEquipmentMaintenaceMaster>();
            AssetEquipmentMaintenaceMasterModifiedByUser = new HashSet<AssetEquipmentMaintenaceMaster>();
            AssetMaintenanceAddedByUser = new HashSet<AssetMaintenance>();
            AssetMaintenanceModifiedByUser = new HashSet<AssetMaintenance>();
            AssetMaintenancePerformedByNavigation = new HashSet<AssetMaintenance>();
            AssetMasterAddedByUser = new HashSet<AssetMaster>();
            AssetMasterModifiedByUser = new HashSet<AssetMaster>();
            AssetPartsMaintenaceMasterAddedByUser = new HashSet<AssetPartsMaintenaceMaster>();
            AssetPartsMaintenaceMasterModifiedByUser = new HashSet<AssetPartsMaintenaceMaster>();
            AssetTicketAddedByUser = new HashSet<AssetTicket>();
            AssetTicketModifiedByUser = new HashSet<AssetTicket>();
            AssetTransferAddedByUser = new HashSet<AssetTransfer>();
            AssetTransferModifiedByUser = new HashSet<AssetTransfer>();
            AttributeDetailsAddedByUser = new HashSet<AttributeDetails>();
            AttributeDetailsModifiedByUser = new HashSet<AttributeDetails>();
            AttributeHeaderAddedByUser = new HashSet<AttributeHeader>();
            AttributeHeaderModifiedByUser = new HashSet<AttributeHeader>();
            BaseUomAddedByUser = new HashSet<BaseUom>();
            BaseUomModifiedByUser = new HashSet<BaseUom>();
            BlanketOrderAddedByUser = new HashSet<BlanketOrder>();
            BlanketOrderModifiedByUser = new HashSet<BlanketOrder>();
            BlisterMouldInformationAddedByUser = new HashSet<BlisterMouldInformation>();
            BlisterMouldInformationModifiedByUser = new HashSet<BlisterMouldInformation>();
            BlisterScrapCodeAddedByUser = new HashSet<BlisterScrapCode>();
            BlisterScrapCodeModifiedByUser = new HashSet<BlisterScrapCode>();
            BlisterScrapEntryAddedByUser = new HashSet<BlisterScrapEntry>();
            BlisterScrapEntryModifiedByUser = new HashSet<BlisterScrapEntry>();
            BlisterScrapMasterAddedByUser = new HashSet<BlisterScrapMaster>();
            BlisterScrapMasterModifiedByUser = new HashSet<BlisterScrapMaster>();
            BmrdisposalBoxAddedByUser = new HashSet<BmrdisposalBox>();
            BmrdisposalBoxModifiedByUser = new HashSet<BmrdisposalBox>();
            BmrdisposalMasterBoxAddedByUser = new HashSet<BmrdisposalMasterBox>();
            BmrdisposalMasterBoxModifiedByUser = new HashSet<BmrdisposalMasterBox>();
            BmrmovementAddedByUser = new HashSet<Bmrmovement>();
            BmrmovementLineAddedByUser = new HashSet<BmrmovementLine>();
            BmrmovementLineModifiedByUser = new HashSet<BmrmovementLine>();
            BmrmovementModifiedByUser = new HashSet<Bmrmovement>();
            BmrtoCartonAddedByUser = new HashSet<BmrtoCarton>();
            BmrtoCartonLineAddedByUser = new HashSet<BmrtoCartonLine>();
            BmrtoCartonLineModifiedByUser = new HashSet<BmrtoCartonLine>();
            BmrtoCartonModifiedByUser = new HashSet<BmrtoCarton>();
            BompackingMasterAddedByUser = new HashSet<BompackingMaster>();
            BompackingMasterLineAddedByUser = new HashSet<BompackingMasterLine>();
            BompackingMasterLineModifiedByUser = new HashSet<BompackingMasterLine>();
            BompackingMasterModifiedByUser = new HashSet<BompackingMaster>();
            BomproductionGroupAddedByUser = new HashSet<BomproductionGroup>();
            BomproductionGroupModifiedByUser = new HashSet<BomproductionGroup>();
            CalibrationServiceInfoAddedByUser = new HashSet<CalibrationServiceInfo>();
            CalibrationServiceInfoModifiedByUser = new HashSet<CalibrationServiceInfo>();
            CalibrationTypeAddedByUser = new HashSet<CalibrationType>();
            CalibrationTypeModifiedByUser = new HashSet<CalibrationType>();
            CategoryAddedByUser = new HashSet<Category>();
            CategoryModifiedByUser = new HashSet<Category>();
            CcfainformationAddedByUser = new HashSet<Ccfainformation>();
            CcfainformationModifiedByUser = new HashSet<Ccfainformation>();
            CcfainformationPiaNavigation = new HashSet<Ccfainformation>();
            CcfainformationPicNavigation = new HashSet<Ccfainformation>();
            CcfbevaluationAddedByUser = new HashSet<Ccfbevaluation>();
            CcfbevaluationEngineeringEvaluatedByNavigation = new HashSet<Ccfbevaluation>();
            CcfbevaluationEvaluatedByNavigation = new HashSet<Ccfbevaluation>();
            CcfbevaluationModifiedByUser = new HashSet<Ccfbevaluation>();
            CcfbevaluationProductionEvaluatedByNavigation = new HashSet<Ccfbevaluation>();
            CcfbevaluationQaevaluatedByNavigation = new HashSet<Ccfbevaluation>();
            CcfbevaluationQualityControlEvaluatedByNavigation = new HashSet<Ccfbevaluation>();
            CcfbevaluationRequlatoryEvaluatedByNavigation = new HashSet<Ccfbevaluation>();
            CcfbevaluationStoreEvaluatedByNavigation = new HashSet<Ccfbevaluation>();
            CcfcapprovalAddedByUser = new HashSet<Ccfcapproval>();
            CcfcapprovalModifiedByUser = new HashSet<Ccfcapproval>();
            CcfcapprovalVerifiedByNavigation = new HashSet<Ccfcapproval>();
            CcfdimplementationAddedByUser = new HashSet<Ccfdimplementation>();
            CcfdimplementationDetailsAddedByUser = new HashSet<CcfdimplementationDetails>();
            CcfdimplementationDetailsDoneByNavigation = new HashSet<CcfdimplementationDetails>();
            CcfdimplementationDetailsModifiedByUser = new HashSet<CcfdimplementationDetails>();
            CcfdimplementationDetailsResponsibiltyToNavigation = new HashSet<CcfdimplementationDetails>();
            CcfdimplementationModifiedByUser = new HashSet<Ccfdimplementation>();
            CcfdimplementationVerifiedByNavigation = new HashSet<Ccfdimplementation>();
            ChangeControlFormAddedByUser = new HashSet<ChangeControlForm>();
            ChangeControlFormModifiedByUser = new HashSet<ChangeControlForm>();
            ChatGroupAddedByUser = new HashSet<ChatGroup>();
            ChatGroupModifiedByUser = new HashSet<ChatGroup>();
            ChatGroupUsers = new HashSet<ChatGroupUsers>();
            ChatMessageReceiveUser = new HashSet<ChatMessage>();
            ChatMessageSentUser = new HashSet<ChatMessage>();
            ChatUserAddedByUser = new HashSet<ChatUser>();
            ChatUserModifiedByUser = new HashSet<ChatUser>();
            ChatUserUser = new HashSet<ChatUser>();
            CityMasterAddedByUser = new HashSet<CityMaster>();
            CityMasterModifiedByUser = new HashSet<CityMaster>();
            ClassificationBmrAddedByUser = new HashSet<ClassificationBmr>();
            ClassificationBmrLineAddedByUser = new HashSet<ClassificationBmrLine>();
            ClassificationBmrLineModifiedByUser = new HashSet<ClassificationBmrLine>();
            ClassificationBmrModifiedByUser = new HashSet<ClassificationBmr>();
            ClassificationProductionAddedByUser = new HashSet<ClassificationProduction>();
            ClassificationProductionModifiedByUser = new HashSet<ClassificationProduction>();
            CloseDocumentPermission = new HashSet<CloseDocumentPermission>();
            CommentAttachment = new HashSet<CommentAttachment>();
            CommitmentOrdersAddedByUser = new HashSet<CommitmentOrders>();
            CommitmentOrdersLineAddedByUser = new HashSet<CommitmentOrdersLine>();
            CommitmentOrdersLineModifiedByUser = new HashSet<CommitmentOrdersLine>();
            CommitmentOrdersModifiedByUser = new HashSet<CommitmentOrders>();
            CommonFieldsProductionMachineAddedByUser = new HashSet<CommonFieldsProductionMachine>();
            CommonFieldsProductionMachineDocumentAddedByUser = new HashSet<CommonFieldsProductionMachineDocument>();
            CommonFieldsProductionMachineDocumentModifiedByUser = new HashSet<CommonFieldsProductionMachineDocument>();
            CommonFieldsProductionMachineLineAddedByUser = new HashSet<CommonFieldsProductionMachineLine>();
            CommonFieldsProductionMachineLineLineAddedByUser = new HashSet<CommonFieldsProductionMachineLineLine>();
            CommonFieldsProductionMachineLineLineModifiedByUser = new HashSet<CommonFieldsProductionMachineLineLine>();
            CommonFieldsProductionMachineLineModifiedByUser = new HashSet<CommonFieldsProductionMachineLine>();
            CommonFieldsProductionMachineModifiedByUser = new HashSet<CommonFieldsProductionMachine>();
            CommonMasterMouldChangePartsAddedByUser = new HashSet<CommonMasterMouldChangeParts>();
            CommonMasterMouldChangePartsModifiedByUser = new HashSet<CommonMasterMouldChangeParts>();
            CommonMasterToolingClassificationAddedByUser = new HashSet<CommonMasterToolingClassification>();
            CommonMasterToolingClassificationModifiedByUser = new HashSet<CommonMasterToolingClassification>();
            CommonPackagingBottleAddedByUser = new HashSet<CommonPackagingBottle>();
            CommonPackagingBottleCapAddedByUser = new HashSet<CommonPackagingBottleCap>();
            CommonPackagingBottleCapModifiedByUser = new HashSet<CommonPackagingBottleCap>();
            CommonPackagingBottleInsertAddedByUser = new HashSet<CommonPackagingBottleInsert>();
            CommonPackagingBottleInsertModifiedByUser = new HashSet<CommonPackagingBottleInsert>();
            CommonPackagingBottleModifiedByUser = new HashSet<CommonPackagingBottle>();
            CommonPackagingCapsealAddedByUser = new HashSet<CommonPackagingCapseal>();
            CommonPackagingCapsealModifiedByUser = new HashSet<CommonPackagingCapseal>();
            CommonPackagingCartonAddedByUser = new HashSet<CommonPackagingCarton>();
            CommonPackagingCartonModifiedByUser = new HashSet<CommonPackagingCarton>();
            CommonPackagingDividerAddedByUser = new HashSet<CommonPackagingDivider>();
            CommonPackagingDividerModifiedByUser = new HashSet<CommonPackagingDivider>();
            CommonPackagingItemHeaderAddedByUser = new HashSet<CommonPackagingItemHeader>();
            CommonPackagingItemHeaderModifiedByUser = new HashSet<CommonPackagingItemHeader>();
            CommonPackagingLayerPadAddedByUser = new HashSet<CommonPackagingLayerPad>();
            CommonPackagingLayerPadModifiedByUser = new HashSet<CommonPackagingLayerPad>();
            CommonPackagingNestingAddedByUser = new HashSet<CommonPackagingNesting>();
            CommonPackagingNestingModifiedByUser = new HashSet<CommonPackagingNesting>();
            CommonPackagingSetInfoAddedByUser = new HashSet<CommonPackagingSetInfo>();
            CommonPackagingSetInfoModifiedByUser = new HashSet<CommonPackagingSetInfo>();
            CommonPackagingShrinkWrapAddedByUser = new HashSet<CommonPackagingShrinkWrap>();
            CommonPackagingShrinkWrapModifiedByUser = new HashSet<CommonPackagingShrinkWrap>();
            CommonPackingInformationAddedByUser = new HashSet<CommonPackingInformation>();
            CommonPackingInformationLineAddedByUser = new HashSet<CommonPackingInformationLine>();
            CommonPackingInformationLineModifiedByUser = new HashSet<CommonPackingInformationLine>();
            CommonPackingInformationModifiedByUser = new HashSet<CommonPackingInformation>();
            CommonPackingItemListLineAddedByUser = new HashSet<CommonPackingItemListLine>();
            CommonPackingItemListLineModifiedByUser = new HashSet<CommonPackingItemListLine>();
            CommonProcessAddedByUser = new HashSet<CommonProcess>();
            CommonProcessLineAddedByUser = new HashSet<CommonProcessLine>();
            CommonProcessLineModifiedByUser = new HashSet<CommonProcessLine>();
            CommonProcessModifiedByUser = new HashSet<CommonProcess>();
            CompanyCalandarDocumentPermissionAddedByUser = new HashSet<CompanyCalandarDocumentPermission>();
            CompanyCalandarDocumentPermissionModifiedByUser = new HashSet<CompanyCalandarDocumentPermission>();
            CompanyCalandarDocumentPermissionUser = new HashSet<CompanyCalandarDocumentPermission>();
            CompanyCalendarAddedByUser = new HashSet<CompanyCalendar>();
            CompanyCalendarAssistance = new HashSet<CompanyCalendarAssistance>();
            CompanyCalendarAssistanceFrom = new HashSet<CompanyCalendar>();
            CompanyCalendarLineAddedByUser = new HashSet<CompanyCalendarLine>();
            CompanyCalendarLineLinkAddedByUser = new HashSet<CompanyCalendarLineLink>();
            CompanyCalendarLineLinkModifiedByUser = new HashSet<CompanyCalendarLineLink>();
            CompanyCalendarLineLinkPrintUser = new HashSet<CompanyCalendarLineLinkPrintUser>();
            CompanyCalendarLineLinkUser = new HashSet<CompanyCalendarLineLinkUser>();
            CompanyCalendarLineLinkUserLinkAddedByUser = new HashSet<CompanyCalendarLineLinkUserLink>();
            CompanyCalendarLineLinkUserLinkModifiedByUser = new HashSet<CompanyCalendarLineLinkUserLink>();
            CompanyCalendarLineLinkUserLinkUser = new HashSet<CompanyCalendarLineLinkUserLink>();
            CompanyCalendarLineMeetingNotesAddedByUser = new HashSet<CompanyCalendarLineMeetingNotes>();
            CompanyCalendarLineMeetingNotesModifiedByUser = new HashSet<CompanyCalendarLineMeetingNotes>();
            CompanyCalendarLineModifiedByUser = new HashSet<CompanyCalendarLine>();
            CompanyCalendarLineNotesAddedByUser = new HashSet<CompanyCalendarLineNotes>();
            CompanyCalendarLineNotesModifiedByUser = new HashSet<CompanyCalendarLineNotes>();
            CompanyCalendarLineNotesUser = new HashSet<CompanyCalendarLineNotes>();
            CompanyCalendarLineNotesUserNavigation = new HashSet<CompanyCalendarLineNotesUser>();
            CompanyCalendarLineUser = new HashSet<CompanyCalendarLineUser>();
            CompanyCalendarModifiedByUser = new HashSet<CompanyCalendar>();
            CompanyCalendarOnBehalf = new HashSet<CompanyCalendar>();
            CompanyCalendarOwner = new HashSet<CompanyCalendar>();
            CompanyCalendarUser = new HashSet<CompanyCalendar>();
            CompanyEventsAddedByUser = new HashSet<CompanyEvents>();
            CompanyEventsDeptOwnerNavigation = new HashSet<CompanyEvents>();
            CompanyEventsModifiedByUser = new HashSet<CompanyEvents>();
            CompanyHolidaysAddedByUser = new HashSet<CompanyHolidays>();
            CompanyHolidaysModifiedByUser = new HashSet<CompanyHolidays>();
            CompanyListingAddedByUser = new HashSet<CompanyListing>();
            CompanyListingLineAddedByUser = new HashSet<CompanyListingLine>();
            CompanyListingLineModifiedByUser = new HashSet<CompanyListingLine>();
            CompanyListingModifiedByUser = new HashSet<CompanyListing>();
            CompanyWorkDayMasterAddedByUser = new HashSet<CompanyWorkDayMaster>();
            CompanyWorkDayMasterModifiedByUser = new HashSet<CompanyWorkDayMaster>();
            ContactAddedByUser = new HashSet<Contact>();
            ContactModifiedByUser = new HashSet<Contact>();
            ContactsAddedByUser = new HashSet<Contacts>();
            ContactsModifiedByUser = new HashSet<Contacts>();
            ContactsOwnerNavigation = new HashSet<Contacts>();
            ContractDistributionAttachmentAddedByUser = new HashSet<ContractDistributionAttachment>();
            ContractDistributionAttachmentModifiedByUser = new HashSet<ContractDistributionAttachment>();
            ContractDistributionSalesEntryLineAddedByUser = new HashSet<ContractDistributionSalesEntryLine>();
            ContractDistributionSalesEntryLineModifiedByUser = new HashSet<ContractDistributionSalesEntryLine>();
            CountryAddedByUser = new HashSet<Country>();
            CountryInformationStatus = new HashSet<CountryInformationStatus>();
            CountryModifiedByUser = new HashSet<Country>();
            CriticalstepandIntermediateAddedByUser = new HashSet<CriticalstepandIntermediate>();
            CriticalstepandIntermediateLineAddedByUser = new HashSet<CriticalstepandIntermediateLine>();
            CriticalstepandIntermediateLineModifiedByUser = new HashSet<CriticalstepandIntermediateLine>();
            CriticalstepandIntermediateModifiedByUser = new HashSet<CriticalstepandIntermediate>();
            CustomerAcceptanceConfirmationAddedByUser = new HashSet<CustomerAcceptanceConfirmation>();
            CustomerAcceptanceConfirmationModifiedByUser = new HashSet<CustomerAcceptanceConfirmation>();
            DepartmentAddedByUser = new HashSet<Department>();
            DepartmentModifiedByUser = new HashSet<Department>();
            DesignationAddedByUser = new HashSet<Designation>();
            DesignationModifiedByUser = new HashSet<Designation>();
            DeviceCatalogMasterAddedByUser = new HashSet<DeviceCatalogMaster>();
            DeviceCatalogMasterModifiedByUser = new HashSet<DeviceCatalogMaster>();
            DeviceGroupListAddedByUser = new HashSet<DeviceGroupList>();
            DeviceGroupListModifiedByUser = new HashSet<DeviceGroupList>();
            DisposalItemAddedByUser = new HashSet<DisposalItem>();
            DisposalItemModifiedByUser = new HashSet<DisposalItem>();
            DistStockBalanceAddedByUser = new HashSet<DistStockBalance>();
            DistStockBalanceModifiedByUser = new HashSet<DistStockBalance>();
            DistributorPricingAddedByUser = new HashSet<DistributorPricing>();
            DistributorPricingModifiedByUser = new HashSet<DistributorPricing>();
            DistributorReplenishmentAddedByUser = new HashSet<DistributorReplenishment>();
            DistributorReplenishmentLineAddedByUser = new HashSet<DistributorReplenishmentLine>();
            DistributorReplenishmentLineModifiedByUser = new HashSet<DistributorReplenishmentLine>();
            DistributorReplenishmentModifiedByUser = new HashSet<DistributorReplenishment>();
            DivisionAddedByUser = new HashSet<Division>();
            DivisionModifiedByUser = new HashSet<Division>();
            DocumentAlertAddedByUser = new HashSet<DocumentAlert>();
            DocumentAlertModifiedByUser = new HashSet<DocumentAlert>();
            DocumentDmsshareAddedByUser = new HashSet<DocumentDmsshare>();
            DocumentDmsshareModifiedByUser = new HashSet<DocumentDmsshare>();
            DocumentInvitationAddedByUser = new HashSet<DocumentInvitation>();
            DocumentInvitationModifiedByUser = new HashSet<DocumentInvitation>();
            DocumentInvitationUser = new HashSet<DocumentInvitation>();
            DocumentLinkAddedByUser = new HashSet<DocumentLink>();
            DocumentLinkModifiedByUser = new HashSet<DocumentLink>();
            DocumentNoSeriesAddedByUser = new HashSet<DocumentNoSeries>();
            DocumentNoSeriesModifiedByUser = new HashSet<DocumentNoSeries>();
            DocumentNoSeriesRequestor = new HashSet<DocumentNoSeries>();
            DocumentPermissionAddedByUser = new HashSet<DocumentPermission>();
            DocumentPermissionModifiedByUser = new HashSet<DocumentPermission>();
            DocumentProfileAddedByUser = new HashSet<DocumentProfile>();
            DocumentProfileModifiedByUser = new HashSet<DocumentProfile>();
            DocumentProfileNoSeriesAddedByUser = new HashSet<DocumentProfileNoSeries>();
            DocumentProfileNoSeriesModifiedByUser = new HashSet<DocumentProfileNoSeries>();
            DocumentRights = new HashSet<DocumentRights>();
            DocumentRoleAddedByUser = new HashSet<DocumentRole>();
            DocumentRoleModifiedByUser = new HashSet<DocumentRole>();
            DocumentRolePermissionAddedByUser = new HashSet<DocumentRolePermission>();
            DocumentRolePermissionModifiedByUser = new HashSet<DocumentRolePermission>();
            DocumentShareAddedByUser = new HashSet<DocumentShare>();
            DocumentShareModifiedByUser = new HashSet<DocumentShare>();
            DocumentShareUser = new HashSet<DocumentShareUser>();
            DocumentUserRole = new HashSet<DocumentUserRole>();
            DocumentsAddedByUser = new HashSet<Documents>();
            DocumentsDeleteByUser = new HashSet<Documents>();
            DocumentsLockedByUser = new HashSet<Documents>();
            DocumentsModifiedByUser = new HashSet<Documents>();
            DocumentsRestoreByUser = new HashSet<Documents>();
            DraftApplicationWikiAddedByUser = new HashSet<DraftApplicationWiki>();
            DraftApplicationWikiLineAddedByUser = new HashSet<DraftApplicationWikiLine>();
            DraftApplicationWikiLineDutyAddedByUser = new HashSet<DraftApplicationWikiLineDuty>();
            DraftApplicationWikiLineDutyModifiedByUser = new HashSet<DraftApplicationWikiLineDuty>();
            DraftApplicationWikiLineModifiedByUser = new HashSet<DraftApplicationWikiLine>();
            DraftApplicationWikiLineNotify = new HashSet<DraftApplicationWikiLineNotify>();
            DraftApplicationWikiModifiedByUser = new HashSet<DraftApplicationWiki>();
            DraftApplicationWikiRecurrenceAddedByUser = new HashSet<DraftApplicationWikiRecurrence>();
            DraftApplicationWikiRecurrenceModifiedByUser = new HashSet<DraftApplicationWikiRecurrence>();
            DraftApplicationWikiWikiEntryBy = new HashSet<DraftApplicationWiki>();
            DraftWikiResponsible = new HashSet<DraftWikiResponsible>();
            DynamicFlowAddedByUser = new HashSet<DynamicFlow>();
            DynamicFlowModifiedByUser = new HashSet<DynamicFlow>();
            DynamicFlowOwnerNavigation = new HashSet<DynamicFlow>();
            DynamicFlowProgress = new HashSet<DynamicFlowProgress>();
            DynamicFlowStep = new HashSet<DynamicFlowStep>();
            DynamicFormAddedByUser = new HashSet<DynamicForm>();
            DynamicFormApprovalAddedByUser = new HashSet<DynamicFormApproval>();
            DynamicFormApprovalApprovalUser = new HashSet<DynamicFormApproval>();
            DynamicFormApprovalModifiedByUser = new HashSet<DynamicFormApproval>();
            DynamicFormApprovedApprovedByUser = new HashSet<DynamicFormApproved>();
            DynamicFormApprovedChanged = new HashSet<DynamicFormApprovedChanged>();
            DynamicFormApprovedUser = new HashSet<DynamicFormApproved>();
            DynamicFormDataAddedByUser = new HashSet<DynamicFormData>();
            DynamicFormDataLockedUser = new HashSet<DynamicFormData>();
            DynamicFormDataModifiedByUser = new HashSet<DynamicFormData>();
            DynamicFormDataSectionLock = new HashSet<DynamicFormDataSectionLock>();
            DynamicFormDataUploadAddedByUser = new HashSet<DynamicFormDataUpload>();
            DynamicFormDataUploadModifiedByUser = new HashSet<DynamicFormDataUpload>();
            DynamicFormItemAddedByUser = new HashSet<DynamicFormItem>();
            DynamicFormItemLineAddedByUser = new HashSet<DynamicFormItemLine>();
            DynamicFormItemLineModifiedByUser = new HashSet<DynamicFormItemLine>();
            DynamicFormItemModifiedByUser = new HashSet<DynamicFormItem>();
            DynamicFormModifiedByUser = new HashSet<DynamicForm>();
            DynamicFormReportAddedByUser = new HashSet<DynamicFormReport>();
            DynamicFormReportModifiedByUser = new HashSet<DynamicFormReport>();
            DynamicFormSectionAddedByUser = new HashSet<DynamicFormSection>();
            DynamicFormSectionAttributeAddedByUser = new HashSet<DynamicFormSectionAttribute>();
            DynamicFormSectionAttributeModifiedByUser = new HashSet<DynamicFormSectionAttribute>();
            DynamicFormSectionAttributeSecurity = new HashSet<DynamicFormSectionAttributeSecurity>();
            DynamicFormSectionModifiedByUser = new HashSet<DynamicFormSection>();
            DynamicFormSectionSecurity = new HashSet<DynamicFormSectionSecurity>();
            DynamicFormSectionWorkFlow = new HashSet<DynamicFormSectionWorkFlow>();
            DynamicFormWorkFlow = new HashSet<DynamicFormWorkFlow>();
            DynamicFormWorkFlowApproval = new HashSet<DynamicFormWorkFlowApproval>();
            DynamicFormWorkFlowApprovedFormApprovedByUser = new HashSet<DynamicFormWorkFlowApprovedForm>();
            DynamicFormWorkFlowApprovedFormChanged = new HashSet<DynamicFormWorkFlowApprovedFormChanged>();
            DynamicFormWorkFlowApprovedFormUser = new HashSet<DynamicFormWorkFlowApprovedForm>();
            DynamicFormWorkFlowForm = new HashSet<DynamicFormWorkFlowForm>();
            DynamicFormWorkFlowFormDelegate = new HashSet<DynamicFormWorkFlowFormDelegate>();
            EmailsAddedByUser = new HashSet<Emails>();
            EmailsModifiedByUser = new HashSet<Emails>();
            EmployeeAddedByUser = new HashSet<Employee>();
            EmployeeIcthardInformationAddedByUser = new HashSet<EmployeeIcthardInformation>();
            EmployeeIcthardInformationModifiedByUser = new HashSet<EmployeeIcthardInformation>();
            EmployeeIctinformationAddedByUser = new HashSet<EmployeeIctinformation>();
            EmployeeIctinformationModifiedByUser = new HashSet<EmployeeIctinformation>();
            EmployeeInformationAddedByUser = new HashSet<EmployeeInformation>();
            EmployeeInformationModifiedByUser = new HashSet<EmployeeInformation>();
            EmployeeLeaveInformationAddedByUser = new HashSet<EmployeeLeaveInformation>();
            EmployeeLeaveInformationModifiedByUser = new HashSet<EmployeeLeaveInformation>();
            EmployeeModifiedByUser = new HashSet<Employee>();
            EmployeeOtherDutyInformationAddedByUser = new HashSet<EmployeeOtherDutyInformation>();
            EmployeeOtherDutyInformationModifiedByUser = new HashSet<EmployeeOtherDutyInformation>();
            EmployeeReport = new HashSet<Employee>();
            EmployeeReportTo = new HashSet<EmployeeReportTo>();
            EmployeeResignation = new HashSet<EmployeeResignation>();
            EmployeeSageInformationAddedByUser = new HashSet<EmployeeSageInformation>();
            EmployeeSageInformationModifiedByUser = new HashSet<EmployeeSageInformation>();
            EmployeeUser = new HashSet<Employee>();
            ExchangeRateAddedByUser = new HashSet<ExchangeRate>();
            ExchangeRateLineAddedByUser = new HashSet<ExchangeRateLine>();
            ExchangeRateLineModifiedByUser = new HashSet<ExchangeRateLine>();
            ExchangeRateModifiedByUser = new HashSet<ExchangeRate>();
            ExcipientInformationLineByProcessAddedByUser = new HashSet<ExcipientInformationLineByProcess>();
            ExcipientInformationLineByProcessModifiedByUser = new HashSet<ExcipientInformationLineByProcess>();
            ExtensionInformationLineAddedByUser = new HashSet<ExtensionInformationLine>();
            ExtensionInformationLineModifiedByUser = new HashSet<ExtensionInformationLine>();
            FileProfileSetupFormAddedByUser = new HashSet<FileProfileSetupForm>();
            FileProfileSetupFormModifiedByUser = new HashSet<FileProfileSetupForm>();
            FileProfileTypeAddedByUser = new HashSet<FileProfileType>();
            FileProfileTypeDeleteByUser = new HashSet<FileProfileType>();
            FileProfileTypeDynamicFormAddedByUser = new HashSet<FileProfileTypeDynamicForm>();
            FileProfileTypeDynamicFormModifiedByUser = new HashSet<FileProfileTypeDynamicForm>();
            FileProfileTypeModifiedByUser = new HashSet<FileProfileType>();
            FileProfileTypeRestoreByUser = new HashSet<FileProfileType>();
            FileProfileTypeSetAccessAddedByUser = new HashSet<FileProfileTypeSetAccess>();
            FileProfileTypeSetAccessUser = new HashSet<FileProfileTypeSetAccess>();
            FinishProductAddedByUser = new HashSet<FinishProduct>();
            FinishProductExcipientAddedByUser = new HashSet<FinishProductExcipient>();
            FinishProductExcipientLineAddedByUser = new HashSet<FinishProductExcipientLine>();
            FinishProductExcipientLineModifiedByUser = new HashSet<FinishProductExcipientLine>();
            FinishProductExcipientModifiedByUser = new HashSet<FinishProductExcipient>();
            FinishProductGeneralInfoAddedByUser = new HashSet<FinishProductGeneralInfo>();
            FinishProductGeneralInfoDocumentAddedByUser = new HashSet<FinishProductGeneralInfoDocument>();
            FinishProductGeneralInfoDocumentModifiedByUser = new HashSet<FinishProductGeneralInfoDocument>();
            FinishProductGeneralInfoModifiedUser = new HashSet<FinishProductGeneralInfo>();
            FinishProductModifiedByUser = new HashSet<FinishProduct>();
            FlowDistributionAddedByUser = new HashSet<FlowDistribution>();
            FlowDistributionModifiedByUser = new HashSet<FlowDistribution>();
            FlowDistributionPiasectionNavigation = new HashSet<FlowDistribution>();
            FlowDistributionPicsectionNavigation = new HashSet<FlowDistribution>();
            FmglobalAddedByUser = new HashSet<Fmglobal>();
            FmglobalLineAddedByUser = new HashSet<FmglobalLine>();
            FmglobalLineItemAddedByUser = new HashSet<FmglobalLineItem>();
            FmglobalLineItemModifiedByUser = new HashSet<FmglobalLineItem>();
            FmglobalLineModifiedByUser = new HashSet<FmglobalLine>();
            FmglobalModifiedByUser = new HashSet<Fmglobal>();
            FmglobalMoveAddedByUser = new HashSet<FmglobalMove>();
            FmglobalMoveModifiedByUser = new HashSet<FmglobalMove>();
            FolderDiscussionAttachment = new HashSet<FolderDiscussionAttachment>();
            FolderDiscussionDiscussedByNavigation = new HashSet<FolderDiscussion>();
            FolderDiscussionEditedByNavigation = new HashSet<FolderDiscussion>();
            FolderDiscussionUser = new HashSet<FolderDiscussionUser>();
            FolderStorageAddedByUser = new HashSet<FolderStorage>();
            FolderStorageModifiedByUser = new HashSet<FolderStorage>();
            FolderStorageUser = new HashSet<FolderStorage>();
            FolderStorageUserNavigation = new HashSet<FolderStorageUser>();
            FoldersAddedByUser = new HashSet<Folders>();
            FoldersModifiedByUser = new HashSet<Folders>();
            FormTableFieldsAddedByUser = new HashSet<FormTableFields>();
            FormTableFieldsModifiedByUser = new HashSet<FormTableFields>();
            FormTablesAddedByUser = new HashSet<FormTables>();
            FormTablesModifiedByUser = new HashSet<FormTables>();
            FpcommonFieldAddedByUser = new HashSet<FpcommonField>();
            FpcommonFieldLineAddedByUser = new HashSet<FpcommonFieldLine>();
            FpcommonFieldLineModifiedByUser = new HashSet<FpcommonFieldLine>();
            FpcommonFieldModifiedByUser = new HashSet<FpcommonField>();
            FpdrugClassificationAddedByUser = new HashSet<FpdrugClassification>();
            FpdrugClassificationLineAddedByUser = new HashSet<FpdrugClassificationLine>();
            FpdrugClassificationLineModifiedByUser = new HashSet<FpdrugClassificationLine>();
            FpdrugClassificationModifiedByUser = new HashSet<FpdrugClassification>();
            FpmanufacturingRecipeAddedByUser = new HashSet<FpmanufacturingRecipe>();
            FpmanufacturingRecipeModifiedUser = new HashSet<FpmanufacturingRecipe>();
            FpproductAddedByUser = new HashSet<Fpproduct>();
            FpproductLineAddedByUser = new HashSet<FpproductLine>();
            FpproductLineModifiedByUser = new HashSet<FpproductLine>();
            FpproductModifiedByUser = new HashSet<Fpproduct>();
            FpproductNavisionLineAddedByUser = new HashSet<FpproductNavisionLine>();
            FpproductNavisionLineModifiedByUser = new HashSet<FpproductNavisionLine>();
            GeneralEquivalaentNavisionAddedByUser = new HashSet<GeneralEquivalaentNavision>();
            GeneralEquivalaentNavisionModifiedByUser = new HashSet<GeneralEquivalaentNavision>();
            GenericCodesAddedByUser = new HashSet<GenericCodes>();
            GenericCodesModifiedByUser = new HashSet<GenericCodes>();
            GenericItemNameListingAddedByUser = new HashSet<GenericItemNameListing>();
            GenericItemNameListingModifiedByUser = new HashSet<GenericItemNameListing>();
            HandlingOfShipmentClassificationAddedByUser = new HashSet<HandlingOfShipmentClassification>();
            HandlingOfShipmentClassificationModifiedByUser = new HashSet<HandlingOfShipmentClassification>();
            HistoricalDetailsAddedByUser = new HashSet<HistoricalDetails>();
            HistoricalDetailsModifiedByUser = new HashSet<HistoricalDetails>();
            HolidayMasterAddedByUser = new HashSet<HolidayMaster>();
            HolidayMasterModifiedByUser = new HashSet<HolidayMaster>();
            HrexternalPersonalAddedByUser = new HashSet<HrexternalPersonal>();
            HrexternalPersonalModifiedByUser = new HashSet<HrexternalPersonal>();
            HumanMovementActionAddedByUser = new HashSet<HumanMovementAction>();
            HumanMovementActionModifiedByUser = new HashSet<HumanMovementAction>();
            IcbmpAddedByUser = new HashSet<Icbmp>();
            IcbmpModifiedByUser = new HashSet<Icbmp>();
            IcbmplineAddedByUser = new HashSet<Icbmpline>();
            IcbmplineModifiedByUser = new HashSet<Icbmpline>();
            IcmasterOperationAddedByUser = new HashSet<IcmasterOperation>();
            IcmasterOperationModifiedByUser = new HashSet<IcmasterOperation>();
            IctcertificateAddedByUser = new HashSet<Ictcertificate>();
            IctcertificateModifiedByUser = new HashSet<Ictcertificate>();
            IctcontactDetailsAddedByUser = new HashSet<IctcontactDetails>();
            IctcontactDetailsModifiedByUser = new HashSet<IctcontactDetails>();
            IctlayoutPlanTypesAddedByUser = new HashSet<IctlayoutPlanTypes>();
            IctlayoutPlanTypesModifiedByUser = new HashSet<IctlayoutPlanTypes>();
            IctmasterAddedByUser = new HashSet<Ictmaster>();
            IctmasterDocumentAddedByUser = new HashSet<IctmasterDocument>();
            IctmasterDocumentModifiedByUser = new HashSet<IctmasterDocument>();
            IctmasterModifiedByUser = new HashSet<Ictmaster>();
            InstructionTypeAddedByUser = new HashSet<InstructionType>();
            InstructionTypeModifiedByUser = new HashSet<InstructionType>();
            InterCompanyPurchaseOrderAddedByUser = new HashSet<InterCompanyPurchaseOrder>();
            InterCompanyPurchaseOrderLineAddedByUser = new HashSet<InterCompanyPurchaseOrderLine>();
            InterCompanyPurchaseOrderLineModifiedByUser = new HashSet<InterCompanyPurchaseOrderLine>();
            InterCompanyPurchaseOrderLogin = new HashSet<InterCompanyPurchaseOrder>();
            InterCompanyPurchaseOrderModifiedByUser = new HashSet<InterCompanyPurchaseOrder>();
            InventoryTypeAddedByUser = new HashSet<InventoryType>();
            InventoryTypeModifiedByUser = new HashSet<InventoryType>();
            InventoryTypeOpeningStockBalanceAddedByUser = new HashSet<InventoryTypeOpeningStockBalance>();
            InventoryTypeOpeningStockBalanceLineAddedByUser = new HashSet<InventoryTypeOpeningStockBalanceLine>();
            InventoryTypeOpeningStockBalanceLineModifiedByUser = new HashSet<InventoryTypeOpeningStockBalanceLine>();
            InventoryTypeOpeningStockBalanceModifiedByUser = new HashSet<InventoryTypeOpeningStockBalance>();
            InverseAddedByUser = new HashSet<ApplicationUser>();
            InverseModifiedByUser = new HashSet<ApplicationUser>();
            IpirAddedByUser = new HashSet<Ipir>();
            IpirAppAddedByUser = new HashSet<IpirApp>();
            IpirAppCheckedDetailsAddedByUser = new HashSet<IpirAppCheckedDetails>();
            IpirAppCheckedDetailsCheckedBy = new HashSet<IpirAppCheckedDetails>();
            IpirAppCheckedDetailsModifiedByUser = new HashSet<IpirAppCheckedDetails>();
            IpirAppDetectedByNavigation = new HashSet<IpirApp>();
            IpirAppModifiedByUser = new HashSet<IpirApp>();
            IpirAppReportingPersonalNavigation = new HashSet<IpirApp>();
            IpirModifiedByUser = new HashSet<Ipir>();
            IpirReportAddedByUser = new HashSet<IpirReport>();
            IpirReportAssignmentAddedByUser = new HashSet<IpirReportAssignment>();
            IpirReportAssignmentModifiedByUser = new HashSet<IpirReportAssignment>();
            IpirReportAssignmentUser = new HashSet<IpirReportAssignmentUser>();
            IpirReportModifiedByUser = new HashSet<IpirReport>();
            IpirlineAddedByUser = new HashSet<Ipirline>();
            IpirlineDocumentAddedByUser = new HashSet<IpirlineDocument>();
            IpirlineDocumentModifiedByUser = new HashSet<IpirlineDocument>();
            IpirlineModifiedByUser = new HashSet<Ipirline>();
            IpirlinePic = new HashSet<Ipirline>();
            IpirmobileActionAddedByUser = new HashSet<IpirmobileAction>();
            IpirmobileActionAssignmentToNavigation = new HashSet<IpirmobileAction>();
            IpirmobileActionModifiedByUser = new HashSet<IpirmobileAction>();
            IpirmobileAddedByUser = new HashSet<Ipirmobile>();
            IpirmobileModifiedByUser = new HashSet<Ipirmobile>();
            IssueReportIpirAddedByUser = new HashSet<IssueReportIpir>();
            IssueReportIpirModifiedByUser = new HashSet<IssueReportIpir>();
            IssueRequestSampleLineAddedByUser = new HashSet<IssueRequestSampleLine>();
            IssueRequestSampleLineModifiedByUser = new HashSet<IssueRequestSampleLine>();
            ItemBatchInfoAddedByUser = new HashSet<ItemBatchInfo>();
            ItemBatchInfoModifiedByUser = new HashSet<ItemBatchInfo>();
            ItemClassificationAccess = new HashSet<ItemClassificationAccess>();
            ItemClassificationHeaderAddedByUser = new HashSet<ItemClassificationHeader>();
            ItemClassificationHeaderModifiedByUser = new HashSet<ItemClassificationHeader>();
            ItemClassificationMasterAddedByUser = new HashSet<ItemClassificationMaster>();
            ItemClassificationMasterModifiedByUser = new HashSet<ItemClassificationMaster>();
            ItemClassificationPermission = new HashSet<ItemClassificationPermission>();
            ItemClassificationTransportAddedByUser = new HashSet<ItemClassificationTransport>();
            ItemClassificationTransportModifiedByUser = new HashSet<ItemClassificationTransport>();
            ItemCostAddedByUser = new HashSet<ItemCost>();
            ItemCostLineAddedByUser = new HashSet<ItemCostLine>();
            ItemCostLineModifiedByUser = new HashSet<ItemCostLine>();
            ItemCostModifiedByUser = new HashSet<ItemCost>();
            ItemManufactureAddedByUser = new HashSet<ItemManufacture>();
            ItemManufactureModifiedByUser = new HashSet<ItemManufacture>();
            ItemMasterAddedByUser = new HashSet<ItemMaster>();
            ItemMasterModifiedByUser = new HashSet<ItemMaster>();
            ItemSalesPriceAddedByUser = new HashSet<ItemSalesPrice>();
            ItemSalesPriceModifiedByUser = new HashSet<ItemSalesPrice>();
            ItemStockInfoAddedByUser = new HashSet<ItemStockInfo>();
            ItemStockInfoModifiedByUser = new HashSet<ItemStockInfo>();
            JobProgressTemplateAddedByUser = new HashSet<JobProgressTemplate>();
            JobProgressTemplateLineLineProcessAddedByUser = new HashSet<JobProgressTemplateLineLineProcess>();
            JobProgressTemplateLineLineProcessModifiedByUser = new HashSet<JobProgressTemplateLineLineProcess>();
            JobProgressTemplateLineProcessAddedByUser = new HashSet<JobProgressTemplateLineProcess>();
            JobProgressTemplateLineProcessAssignedToNavigation = new HashSet<JobProgressTemplateLineProcess>();
            JobProgressTemplateLineProcessModifiedByUser = new HashSet<JobProgressTemplateLineProcess>();
            JobProgressTemplateLineProcessPia = new HashSet<JobProgressTemplateLineProcessPia>();
            JobProgressTemplateLineProcessPiaNavigation = new HashSet<JobProgressTemplateLineProcess>();
            JobProgressTemplateLineProcessPicNavigation = new HashSet<JobProgressTemplateLineProcess>();
            JobProgressTemplateModifiedByUser = new HashSet<JobProgressTemplate>();
            JobProgressTemplatePicNavigation = new HashSet<JobProgressTemplate>();
            JobProgressTemplateRecurrenceAddedByUser = new HashSet<JobProgressTemplateRecurrence>();
            JobProgressTemplateRecurrenceModifiedByUser = new HashSet<JobProgressTemplateRecurrence>();
            JobProgressTempletateLineAddedByUser = new HashSet<JobProgressTempletateLine>();
            JobProgressTempletateLineAssignedToNavigation = new HashSet<JobProgressTempletateLine>();
            JobProgressTempletateLineModifiedByUser = new HashSet<JobProgressTempletateLine>();
            JobProgressTempletateLinePiaNavigation = new HashSet<JobProgressTempletateLine>();
            JobScheduleAddedByUser = new HashSet<JobSchedule>();
            JobScheduleModifiedByUser = new HashSet<JobSchedule>();
            LayoutPlanTypeAddedByUser = new HashSet<LayoutPlanType>();
            LayoutPlanTypeModifiedByUser = new HashSet<LayoutPlanType>();
            LevelMasterAddedByUser = new HashSet<LevelMaster>();
            LevelMasterModifiedByUser = new HashSet<LevelMaster>();
            LinkFileProfileTypeDocument = new HashSet<LinkFileProfileTypeDocument>();
            LocalClinicAddedByUser = new HashSet<LocalClinic>();
            LocalClinicModifiedByUser = new HashSet<LocalClinic>();
            LocationsAddedByUser = new HashSet<Locations>();
            LocationsModifiedByUser = new HashSet<Locations>();
            LoginSession = new HashSet<LoginSession>();
            MachineTimeManHourInfoAddedByUser = new HashSet<MachineTimeManHourInfo>();
            MachineTimeManHourInfoModifiedByUser = new HashSet<MachineTimeManHourInfo>();
            ManpowerInformationAddedByUser = new HashSet<ManpowerInformation>();
            ManpowerInformationLineAddedByUser = new HashSet<ManpowerInformationLine>();
            ManpowerInformationLineModifiedByUser = new HashSet<ManpowerInformationLine>();
            ManpowerInformationModifiedByUser = new HashSet<ManpowerInformation>();
            ManufacturingProcessAddedByUser = new HashSet<ManufacturingProcess>();
            ManufacturingProcessLineAddedByUser = new HashSet<ManufacturingProcessLine>();
            ManufacturingProcessLineModifiedByUser = new HashSet<ManufacturingProcessLine>();
            ManufacturingProcessModifiedByUser = new HashSet<ManufacturingProcess>();
            MarginInformationAddedByUser = new HashSet<MarginInformation>();
            MarginInformationLineAddedByUser = new HashSet<MarginInformationLine>();
            MarginInformationLineModifiedByUser = new HashSet<MarginInformationLine>();
            MarginInformationModifiedByUser = new HashSet<MarginInformation>();
            MasterBlanketOrderAddedByUser = new HashSet<MasterBlanketOrder>();
            MasterBlanketOrderLineAddedByUser = new HashSet<MasterBlanketOrderLine>();
            MasterBlanketOrderLineModifiedByUser = new HashSet<MasterBlanketOrderLine>();
            MasterBlanketOrderModifiedByUser = new HashSet<MasterBlanketOrder>();
            MasterDocumentInformationAddedByUser = new HashSet<MasterDocumentInformation>();
            MasterDocumentInformationModifiedByUser = new HashSet<MasterDocumentInformation>();
            MasterFormAddedByUser = new HashSet<MasterForm>();
            MasterFormDetailAddedByUser = new HashSet<MasterFormDetail>();
            MasterFormDetailModifiedByUser = new HashSet<MasterFormDetail>();
            MasterFormDetailUser = new HashSet<MasterFormDetail>();
            MasterFormModifiedByUser = new HashSet<MasterForm>();
            MasterInterCompanyPricingAddedByUser = new HashSet<MasterInterCompanyPricing>();
            MasterInterCompanyPricingLineAddedByUser = new HashSet<MasterInterCompanyPricingLine>();
            MasterInterCompanyPricingLineModifiedByUser = new HashSet<MasterInterCompanyPricingLine>();
            MasterInterCompanyPricingModifiedByUser = new HashSet<MasterInterCompanyPricing>();
            MedCatalogClassificationAddedByUser = new HashSet<MedCatalogClassification>();
            MedCatalogClassificationModifiedByUser = new HashSet<MedCatalogClassification>();
            MedMasterAddedByUser = new HashSet<MedMaster>();
            MedMasterLineAddedByUser = new HashSet<MedMasterLine>();
            MedMasterLineModifiedByUser = new HashSet<MedMasterLine>();
            MedMasterModifiedByUser = new HashSet<MedMaster>();
            MemoAddedByUser = new HashSet<Memo>();
            MemoModifiedByUser = new HashSet<Memo>();
            MemoUser = new HashSet<MemoUser>();
            MethodTemplateRoutineAddedByUser = new HashSet<MethodTemplateRoutine>();
            MethodTemplateRoutineLineAddedByUser = new HashSet<MethodTemplateRoutineLine>();
            MethodTemplateRoutineLineModifiedByUser = new HashSet<MethodTemplateRoutineLine>();
            MethodTemplateRoutineModifiedByUser = new HashSet<MethodTemplateRoutine>();
            MobileShiftAddedByUser = new HashSet<MobileShift>();
            MobileShiftModifiedByUser = new HashSet<MobileShift>();
            ModulesAddedByUser = new HashSet<Modules>();
            ModulesModifiedByUser = new HashSet<Modules>();
            NavItemBatchNoAddedByUser = new HashSet<NavItemBatchNo>();
            NavItemBatchNoModifiedByUser = new HashSet<NavItemBatchNo>();
            NavManufacturingProcessAddedByUser = new HashSet<NavManufacturingProcess>();
            NavManufacturingProcessModifiedByUser = new HashSet<NavManufacturingProcess>();
            NavMethodCodeAddedByUser = new HashSet<NavMethodCode>();
            NavMethodCodeLinesAddedByUser = new HashSet<NavMethodCodeLines>();
            NavMethodCodeLinesModifiedByUser = new HashSet<NavMethodCodeLines>();
            NavMethodCodeModifiedByUser = new HashSet<NavMethodCode>();
            NavPackingMethodAddedByUser = new HashSet<NavPackingMethod>();
            NavPackingMethodModifiedByUser = new HashSet<NavPackingMethod>();
            NavProductMasterAddedByUser = new HashSet<NavProductMaster>();
            NavProductMasterModifiedByUser = new HashSet<NavProductMaster>();
            NavSaleCategoryAddedByUser = new HashSet<NavSaleCategory>();
            NavSaleCategoryModifiedByUser = new HashSet<NavSaleCategory>();
            NavbaseUnitAddedByUser = new HashSet<NavbaseUnit>();
            NavbaseUnitModifiedByUser = new HashSet<NavbaseUnit>();
            Navcustomer = new HashSet<Navcustomer>();
            NavisionCompanyAddedByUser = new HashSet<NavisionCompany>();
            NavisionCompanyModifiedByUser = new HashSet<NavisionCompany>();
            NavisionSpecificationAddedByUser = new HashSet<NavisionSpecification>();
            NavisionSpecificationModifiedByUser = new HashSet<NavisionSpecification>();
            NavitemLinksAddedByUser = new HashSet<NavitemLinks>();
            NavitemLinksModifiedByUser = new HashSet<NavitemLinks>();
            NavitemStockBalanceAddedByUser = new HashSet<NavitemStockBalance>();
            NavitemStockBalanceModifiedByUser = new HashSet<NavitemStockBalance>();
            NavitemsAddedByUser = new HashSet<Navitems>();
            NavitemsLastSyncByNavigation = new HashSet<Navitems>();
            NavitemsModifiedByUser = new HashSet<Navitems>();
            NavlocationAddedByUser = new HashSet<Navlocation>();
            NavlocationModifiedByUser = new HashSet<Navlocation>();
            NavplannedProdOrderAddedByUser = new HashSet<NavplannedProdOrder>();
            NavplannedProdOrderModifiedByUser = new HashSet<NavplannedProdOrder>();
            NavpostedShipmentAddedByUser = new HashSet<NavpostedShipment>();
            NavpostedShipmentModifiedByUser = new HashSet<NavpostedShipment>();
            NavproductCodeAddedByUser = new HashSet<NavproductCode>();
            NavproductCodeModifiedByUser = new HashSet<NavproductCode>();
            NavrecipesAddedByUser = new HashSet<Navrecipes>();
            NavrecipesModifiedByUser = new HashSet<Navrecipes>();
            NavsettingsAddedByUser = new HashSet<Navsettings>();
            NavsettingsModifiedByUser = new HashSet<Navsettings>();
            NavstockBalanaceAddedByUser = new HashSet<NavstockBalanace>();
            NavstockBalanaceModifiedByUser = new HashSet<NavstockBalanace>();
            NavvendorAddedByUser = new HashSet<Navvendor>();
            NavvendorModifiedByUser = new HashSet<Navvendor>();
            NonDeliverSoAddedByUser = new HashSet<NonDeliverSo>();
            NonDeliverSoModifiedByUser = new HashSet<NonDeliverSo>();
            NotesAddedByUser = new HashSet<Notes>();
            NotesModifiedByUser = new HashSet<Notes>();
            NoticeAddedByUser = new HashSet<Notice>();
            NoticeLineAddedByUser = new HashSet<NoticeLine>();
            NoticeLineModifiedByUser = new HashSet<NoticeLine>();
            NoticeModifiedByUser = new HashSet<Notice>();
            NoticeUser = new HashSet<NoticeUser>();
            Notification = new HashSet<Notification>();
            NotificationHandler = new HashSet<NotificationHandler>();
            NotifyDocumentAddedByUser = new HashSet<NotifyDocument>();
            NotifyDocumentModifiedByUser = new HashSet<NotifyDocument>();
            NotifyDocumentUserGroup = new HashSet<NotifyDocumentUserGroup>();
            NotifyUserGroupUser = new HashSet<NotifyUserGroupUser>();
            NovateInformationLineAddedByUser = new HashSet<NovateInformationLine>();
            NovateInformationLineModifiedByUser = new HashSet<NovateInformationLine>();
            NpraformulationActiveIngredientAddedByUser = new HashSet<NpraformulationActiveIngredient>();
            NpraformulationActiveIngredientModifiedByUser = new HashSet<NpraformulationActiveIngredient>();
            NpraformulationAddedByUser = new HashSet<Npraformulation>();
            NpraformulationExcipientAddedByUser = new HashSet<NpraformulationExcipient>();
            NpraformulationExcipientModifiedByUser = new HashSet<NpraformulationExcipient>();
            NpraformulationModifiedByUser = new HashSet<Npraformulation>();
            OpenAccessUserAddedByUser = new HashSet<OpenAccessUser>();
            OpenAccessUserLink = new HashSet<OpenAccessUserLink>();
            OpenAccessUserModifiedByUser = new HashSet<OpenAccessUser>();
            OperationProcedureAddedByUser = new HashSet<OperationProcedure>();
            OperationProcedureModifiedByUser = new HashSet<OperationProcedure>();
            OperationSequenceAddedByUser = new HashSet<OperationSequence>();
            OperationSequenceModifiedByUser = new HashSet<OperationSequence>();
            OrderRequirementAddedByUser = new HashSet<OrderRequirement>();
            OrderRequirementGrouPinglineSplitAddedByUser = new HashSet<OrderRequirementGrouPinglineSplit>();
            OrderRequirementGrouPinglineSplitModifiedByUser = new HashSet<OrderRequirementGrouPinglineSplit>();
            OrderRequirementGroupingLineAddedByUser = new HashSet<OrderRequirementGroupingLine>();
            OrderRequirementGroupingLineModifiedByUser = new HashSet<OrderRequirementGroupingLine>();
            OrderRequirementLineAddedByUser = new HashSet<OrderRequirementLine>();
            OrderRequirementLineModifiedByUser = new HashSet<OrderRequirementLine>();
            OrderRequirementLineSplitAddedByUser = new HashSet<OrderRequirementLineSplit>();
            OrderRequirementLineSplitModifiedByUser = new HashSet<OrderRequirementLineSplit>();
            OrderRequirementModifiedByUser = new HashSet<OrderRequirement>();
            PackagingAluminiumFoilAddedByUser = new HashSet<PackagingAluminiumFoil>();
            PackagingAluminiumFoilLineAddedByUser = new HashSet<PackagingAluminiumFoilLine>();
            PackagingAluminiumFoilLineModifiedByUser = new HashSet<PackagingAluminiumFoilLine>();
            PackagingAluminiumFoilModifiedByUser = new HashSet<PackagingAluminiumFoil>();
            PackagingHistoryItemLineDocumentAddedByUser = new HashSet<PackagingHistoryItemLineDocument>();
            PackagingHistoryItemLineDocumentModifiedByUser = new HashSet<PackagingHistoryItemLineDocument>();
            PackagingItemHistoryAddedByUser = new HashSet<PackagingItemHistory>();
            PackagingItemHistoryLineAddedByUser = new HashSet<PackagingItemHistoryLine>();
            PackagingItemHistoryLineModifiedByUser = new HashSet<PackagingItemHistoryLine>();
            PackagingItemHistoryModifiedByUser = new HashSet<PackagingItemHistory>();
            PackagingLabelAddedByUser = new HashSet<PackagingLabel>();
            PackagingLabelModifiedByUser = new HashSet<PackagingLabel>();
            PackagingLeafletAddedByUser = new HashSet<PackagingLeaflet>();
            PackagingLeafletModifiedByUser = new HashSet<PackagingLeaflet>();
            PackagingPvcfoilAddedByUser = new HashSet<PackagingPvcfoil>();
            PackagingPvcfoilModifiedByUser = new HashSet<PackagingPvcfoil>();
            PackagingTubeAddedByUser = new HashSet<PackagingTube>();
            PackagingTubeModifiedByUser = new HashSet<PackagingTube>();
            PackagingUnitBoxAddedByUser = new HashSet<PackagingUnitBox>();
            PackagingUnitBoxModifiedByUser = new HashSet<PackagingUnitBox>();
            PacketInformationLineAddedByUser = new HashSet<PacketInformationLine>();
            PacketInformationLineModifiedByUser = new HashSet<PacketInformationLine>();
            PageApproval = new HashSet<PageApproval>();
            PageComment = new HashSet<PageComment>();
            PageLikes = new HashSet<PageLikes>();
            PageLinkConditionAddedByUser = new HashSet<PageLinkCondition>();
            PageLinkConditionModifiedByUser = new HashSet<PageLinkCondition>();
            PageVersion = new HashSet<PageVersion>();
            PerUnitFormulationLineAddedByUser = new HashSet<PerUnitFormulationLine>();
            PerUnitFormulationLineModifiedByUser = new HashSet<PerUnitFormulationLine>();
            PhramacologicalPropertiesAddedByUser = new HashSet<PhramacologicalProperties>();
            PhramacologicalPropertiesModifiedByUser = new HashSet<PhramacologicalProperties>();
            PkgapprovalInformationAddedByUser = new HashSet<PkgapprovalInformation>();
            PkgapprovalInformationModifiedByUser = new HashSet<PkgapprovalInformation>();
            PkginformationByPackSizeAddedByUser = new HashSet<PkginformationByPackSize>();
            PkginformationByPackSizeModifiedByUser = new HashSet<PkginformationByPackSize>();
            PkgregisteredPackingInformationAddedByUser = new HashSet<PkgregisteredPackingInformation>();
            PkgregisteredPackingInformationModifiedByUser = new HashSet<PkgregisteredPackingInformation>();
            PlantAddedByUser = new HashSet<Plant>();
            PlantMaintenanceEntryAddedByUser = new HashSet<PlantMaintenanceEntry>();
            PlantMaintenanceEntryMasterAddedByUser = new HashSet<PlantMaintenanceEntryMaster>();
            PlantMaintenanceEntryMasterModifiedByUser = new HashSet<PlantMaintenanceEntryMaster>();
            PlantMaintenanceEntryModifiedByUser = new HashSet<PlantMaintenanceEntry>();
            PlantModifiedByUser = new HashSet<Plant>();
            PlasticBagAddedByUser = new HashSet<PlasticBag>();
            PlasticBagModifiedByUser = new HashSet<PlasticBag>();
            PortfolioAddedByUser = new HashSet<Portfolio>();
            PortfolioAttachmentLockedByUser = new HashSet<PortfolioAttachment>();
            PortfolioAttachmentUploadedByUser = new HashSet<PortfolioAttachment>();
            PortfolioLineAddedByUser = new HashSet<PortfolioLine>();
            PortfolioLineModifiedByUser = new HashSet<PortfolioLine>();
            PortfolioModifiedByUser = new HashSet<Portfolio>();
            PortfolioOnBehalf = new HashSet<Portfolio>();
            PortfolioOnBehalfNavigation = new HashSet<PortfolioOnBehalf>();
            PrintInfo = new HashSet<PrintInfo>();
            ProcessMachineTimeAddedByUser = new HashSet<ProcessMachineTime>();
            ProcessMachineTimeModifiedByUser = new HashSet<ProcessMachineTime>();
            ProcessMachineTimeProductionLineAddedByUser = new HashSet<ProcessMachineTimeProductionLine>();
            ProcessMachineTimeProductionLineLineAddedByUser = new HashSet<ProcessMachineTimeProductionLineLine>();
            ProcessMachineTimeProductionLineLineModifiedByUser = new HashSet<ProcessMachineTimeProductionLineLine>();
            ProcessMachineTimeProductionLineModifiedByUser = new HashSet<ProcessMachineTimeProductionLine>();
            ProcessMachineTimeProductionMachineProcessAddedByUser = new HashSet<ProcessMachineTimeProductionMachineProcess>();
            ProcessMachineTimeProductionMachineProcessModifiedByUser = new HashSet<ProcessMachineTimeProductionMachineProcess>();
            ProcessManPowerSpecialNotesAddedByUser = new HashSet<ProcessManPowerSpecialNotes>();
            ProcessManPowerSpecialNotesModifiedByUser = new HashSet<ProcessManPowerSpecialNotes>();
            ProcessTransferAddedByUser = new HashSet<ProcessTransfer>();
            ProcessTransferLineAddedByUser = new HashSet<ProcessTransferLine>();
            ProcessTransferLineModifiedByUser = new HashSet<ProcessTransferLine>();
            ProcessTransferModifiedByUser = new HashSet<ProcessTransfer>();
            ProductActivityCaseAddedByUser = new HashSet<ProductActivityCase>();
            ProductActivityCaseLineAddedByUser = new HashSet<ProductActivityCaseLine>();
            ProductActivityCaseLineModifiedByUser = new HashSet<ProductActivityCaseLine>();
            ProductActivityCaseModifiedByUser = new HashSet<ProductActivityCase>();
            ProductActivityCaseResponsAddedByUser = new HashSet<ProductActivityCaseRespons>();
            ProductActivityCaseResponsDutyAddedByUser = new HashSet<ProductActivityCaseResponsDuty>();
            ProductActivityCaseResponsDutyModifiedByUser = new HashSet<ProductActivityCaseResponsDuty>();
            ProductActivityCaseResponsModifiedByUser = new HashSet<ProductActivityCaseRespons>();
            ProductActivityCaseResponsRecurrenceAddedByUser = new HashSet<ProductActivityCaseResponsRecurrence>();
            ProductActivityCaseResponsRecurrenceModifiedByUser = new HashSet<ProductActivityCaseResponsRecurrence>();
            ProductActivityCaseResponsResponsible = new HashSet<ProductActivityCaseResponsResponsible>();
            ProductActivityPermissionAddedByUser = new HashSet<ProductActivityPermission>();
            ProductActivityPermissionModifiedByUser = new HashSet<ProductActivityPermission>();
            ProductActivityPermissionUser = new HashSet<ProductActivityPermission>();
            ProductGroupSurveyAddedByUser = new HashSet<ProductGroupSurvey>();
            ProductGroupSurveyLineAddedByUser = new HashSet<ProductGroupSurveyLine>();
            ProductGroupSurveyLineModifiedByUser = new HashSet<ProductGroupSurveyLine>();
            ProductGroupSurveyModifiedByUser = new HashSet<ProductGroupSurvey>();
            ProductGroupingAddedByUser = new HashSet<ProductGrouping>();
            ProductGroupingManufactureAddedByUser = new HashSet<ProductGroupingManufacture>();
            ProductGroupingManufactureModifiedByUser = new HashSet<ProductGroupingManufacture>();
            ProductGroupingModifiedByUser = new HashSet<ProductGrouping>();
            ProductGroupingNavAddedByUser = new HashSet<ProductGroupingNav>();
            ProductGroupingNavDifferenceAddedByUser = new HashSet<ProductGroupingNavDifference>();
            ProductGroupingNavDifferenceModifiedByUser = new HashSet<ProductGroupingNavDifference>();
            ProductGroupingNavModifiedByUser = new HashSet<ProductGroupingNav>();
            ProductRecipeDocumentAddedByUser = new HashSet<ProductRecipeDocument>();
            ProductRecipeDocumentModifiedByUser = new HashSet<ProductRecipeDocument>();
            ProductionActionAddedByUser = new HashSet<ProductionAction>();
            ProductionActionModifiedByUser = new HashSet<ProductionAction>();
            ProductionActivityAddedByUser = new HashSet<ProductionActivity>();
            ProductionActivityAppAddedByUser = new HashSet<ProductionActivityApp>();
            ProductionActivityAppLineAddedByUser = new HashSet<ProductionActivityAppLine>();
            ProductionActivityAppLineModifiedByUser = new HashSet<ProductionActivityAppLine>();
            ProductionActivityAppLineQaCheckUser = new HashSet<ProductionActivityAppLine>();
            ProductionActivityAppLineQaChecker = new HashSet<ProductionActivityAppLineQaChecker>();
            ProductionActivityAppModifiedByUser = new HashSet<ProductionActivityApp>();
            ProductionActivityCheckedDetailsAddedByUser = new HashSet<ProductionActivityCheckedDetails>();
            ProductionActivityCheckedDetailsCheckedBy = new HashSet<ProductionActivityCheckedDetails>();
            ProductionActivityCheckedDetailsModifiedByUser = new HashSet<ProductionActivityCheckedDetails>();
            ProductionActivityClassificationAddedByUser = new HashSet<ProductionActivityClassification>();
            ProductionActivityClassificationModifiedByUser = new HashSet<ProductionActivityClassification>();
            ProductionActivityMasterAddedByUser = new HashSet<ProductionActivityMaster>();
            ProductionActivityMasterLineAddedByUser = new HashSet<ProductionActivityMasterLine>();
            ProductionActivityMasterLineModifiedByUser = new HashSet<ProductionActivityMasterLine>();
            ProductionActivityMasterModifiedByUser = new HashSet<ProductionActivityMaster>();
            ProductionActivityMasterResponsAddedByUser = new HashSet<ProductionActivityMasterRespons>();
            ProductionActivityMasterResponsModifiedByUser = new HashSet<ProductionActivityMasterRespons>();
            ProductionActivityMasterResponsRecurrenceAddedByUser = new HashSet<ProductionActivityMasterResponsRecurrence>();
            ProductionActivityMasterResponsRecurrenceModifiedByUser = new HashSet<ProductionActivityMasterResponsRecurrence>();
            ProductionActivityModifiedByUser = new HashSet<ProductionActivity>();
            ProductionActivityNonComplianceAddedByUser = new HashSet<ProductionActivityNonCompliance>();
            ProductionActivityNonComplianceModifiedByUser = new HashSet<ProductionActivityNonCompliance>();
            ProductionActivityNonComplianceUser = new HashSet<ProductionActivityNonComplianceUser>();
            ProductionActivityPlanningAppAddedByUser = new HashSet<ProductionActivityPlanningApp>();
            ProductionActivityPlanningAppLineAddedByUser = new HashSet<ProductionActivityPlanningAppLine>();
            ProductionActivityPlanningAppLineModifiedByUser = new HashSet<ProductionActivityPlanningAppLine>();
            ProductionActivityPlanningAppLineQaCheckUser = new HashSet<ProductionActivityPlanningAppLine>();
            ProductionActivityPlanningAppLineQaChecker = new HashSet<ProductionActivityPlanningAppLineQaChecker>();
            ProductionActivityPlanningAppModifiedByUser = new HashSet<ProductionActivityPlanningApp>();
            ProductionActivityRoutineAppAddedByUser = new HashSet<ProductionActivityRoutineApp>();
            ProductionActivityRoutineAppLineAddedByUser = new HashSet<ProductionActivityRoutineAppLine>();
            ProductionActivityRoutineAppLineModifiedByUser = new HashSet<ProductionActivityRoutineAppLine>();
            ProductionActivityRoutineAppLineQaCheckUser = new HashSet<ProductionActivityRoutineAppLine>();
            ProductionActivityRoutineAppLineQaChecker = new HashSet<ProductionActivityRoutineAppLineQaChecker>();
            ProductionActivityRoutineAppModifiedByUser = new HashSet<ProductionActivityRoutineApp>();
            ProductionActivityRoutineCheckedDetailsAddedByUser = new HashSet<ProductionActivityRoutineCheckedDetails>();
            ProductionActivityRoutineCheckedDetailsCheckedBy = new HashSet<ProductionActivityRoutineCheckedDetails>();
            ProductionActivityRoutineCheckedDetailsModifiedByUser = new HashSet<ProductionActivityRoutineCheckedDetails>();
            ProductionBatchInformationAddedByUser = new HashSet<ProductionBatchInformation>();
            ProductionBatchInformationLineAddedByUser = new HashSet<ProductionBatchInformationLine>();
            ProductionBatchInformationLineModifiedByUser = new HashSet<ProductionBatchInformationLine>();
            ProductionBatchInformationModifiedByUser = new HashSet<ProductionBatchInformation>();
            ProductionBatchSizeAddedByUser = new HashSet<ProductionBatchSize>();
            ProductionBatchSizeModifiedByUser = new HashSet<ProductionBatchSize>();
            ProductionCapsuleAddedByUser = new HashSet<ProductionCapsule>();
            ProductionCapsuleLineAddedByUser = new HashSet<ProductionCapsuleLine>();
            ProductionCapsuleLineModifiedByUser = new HashSet<ProductionCapsuleLine>();
            ProductionCapsuleModifiedByUser = new HashSet<ProductionCapsule>();
            ProductionColorAddedByUser = new HashSet<ProductionColor>();
            ProductionColorLineAddedByUser = new HashSet<ProductionColorLine>();
            ProductionColorLineModifiedByUser = new HashSet<ProductionColorLine>();
            ProductionColorModifiedByUser = new HashSet<ProductionColor>();
            ProductionCycleAddedByUser = new HashSet<ProductionCycle>();
            ProductionCycleModifiedByUser = new HashSet<ProductionCycle>();
            ProductionDocumentAddedByUser = new HashSet<ProductionDocument>();
            ProductionDocumentModifiedByUser = new HashSet<ProductionDocument>();
            ProductionEntryAddedByUser = new HashSet<ProductionEntry>();
            ProductionEntryModifiedByUser = new HashSet<ProductionEntry>();
            ProductionFlavourAddedByUser = new HashSet<ProductionFlavour>();
            ProductionFlavourLineAddedByUser = new HashSet<ProductionFlavourLine>();
            ProductionFlavourLineModifiedByUser = new HashSet<ProductionFlavourLine>();
            ProductionFlavourModifiedByUser = new HashSet<ProductionFlavour>();
            ProductionForecastAddedByUser = new HashSet<ProductionForecast>();
            ProductionForecastModifiedByUser = new HashSet<ProductionForecast>();
            ProductionMachineAddedByUser = new HashSet<ProductionMachine>();
            ProductionMachineModifiedByUser = new HashSet<ProductionMachine>();
            ProductionMaterialAddedByUser = new HashSet<ProductionMaterial>();
            ProductionMaterialModifiedByUser = new HashSet<ProductionMaterial>();
            ProductionMethodTemplateAddedByUser = new HashSet<ProductionMethodTemplate>();
            ProductionMethodTemplateModifiedByUser = new HashSet<ProductionMethodTemplate>();
            ProductionOrderAddedByUser = new HashSet<ProductionOrder>();
            ProductionOrderMasterAddedByUser = new HashSet<ProductionOrderMaster>();
            ProductionOrderMasterModifiedByUser = new HashSet<ProductionOrderMaster>();
            ProductionOrderModifiedByUser = new HashSet<ProductionOrder>();
            ProductionOrderSplitAddedByUser = new HashSet<ProductionOrderSplit>();
            ProductionOrderSplitModifiedByUser = new HashSet<ProductionOrderSplit>();
            ProductionOutputAddedByUser = new HashSet<ProductionOutput>();
            ProductionOutputModifiedByUser = new HashSet<ProductionOutput>();
            ProductionSimulationGroupingAddedByUser = new HashSet<ProductionSimulationGrouping>();
            ProductionSimulationGroupingModifiedByUser = new HashSet<ProductionSimulationGrouping>();
            ProductionSimulationGroupingTicketAddedByUser = new HashSet<ProductionSimulationGroupingTicket>();
            ProductionSimulationGroupingTicketModifiedByUser = new HashSet<ProductionSimulationGroupingTicket>();
            ProductionSimulationHistoryAddedByUser = new HashSet<ProductionSimulationHistory>();
            ProductionSimulationHistoryModifiedByUser = new HashSet<ProductionSimulationHistory>();
            ProductionTicketAddedByUser = new HashSet<ProductionTicket>();
            ProductionTicketModifiedByUser = new HashSet<ProductionTicket>();
            ProfileDocument = new HashSet<ProfileDocument>();
            ProjectAddedByUser = new HashSet<Project>();
            ProjectModifiedByUser = new HashSet<Project>();
            ProjectedDeliveryBlanketOthersAddedByUser = new HashSet<ProjectedDeliveryBlanketOthers>();
            ProjectedDeliveryBlanketOthersModifiedByUser = new HashSet<ProjectedDeliveryBlanketOthers>();
            ProjectedDeliverySalesOrderLineAddedByUser = new HashSet<ProjectedDeliverySalesOrderLine>();
            ProjectedDeliverySalesOrderLineModifiedByUser = new HashSet<ProjectedDeliverySalesOrderLine>();
            PsbreportCommentDetailAddedByUser = new HashSet<PsbreportCommentDetail>();
            PsbreportCommentDetailModifiedByUser = new HashSet<PsbreportCommentDetail>();
            PurchaseItemSalesEntryLineAddedByUser = new HashSet<PurchaseItemSalesEntryLine>();
            PurchaseItemSalesEntryLineModifiedByUser = new HashSet<PurchaseItemSalesEntryLine>();
            PurchaseOrderAddedByUser = new HashSet<PurchaseOrder>();
            PurchaseOrderDocumentAddedByUser = new HashSet<PurchaseOrderDocument>();
            PurchaseOrderDocumentModifiedByUser = new HashSet<PurchaseOrderDocument>();
            PurchaseOrderModifiedByUser = new HashSet<PurchaseOrder>();
            QcapprovalAddedByUser = new HashSet<Qcapproval>();
            QcapprovalLineAddedByUser = new HashSet<QcapprovalLine>();
            QcapprovalLineModifiedByUser = new HashSet<QcapprovalLine>();
            QcapprovalModifiedByUser = new HashSet<Qcapproval>();
            QuotationAwardAddedByUser = new HashSet<QuotationAward>();
            QuotationAwardLineAddedByUser = new HashSet<QuotationAwardLine>();
            QuotationAwardLineModifiedByUser = new HashSet<QuotationAwardLine>();
            QuotationAwardModifiedByUser = new HashSet<QuotationAward>();
            QuotationCategoryAddedByUser = new HashSet<QuotationCategory>();
            QuotationCategoryModifiedByUser = new HashSet<QuotationCategory>();
            QuotationHeaderAddedByUser = new HashSet<QuotationHeader>();
            QuotationHeaderModifiedByUser = new HashSet<QuotationHeader>();
            QuotationHistoryAddedByUser = new HashSet<QuotationHistory>();
            QuotationHistoryDocumentAddedByUser = new HashSet<QuotationHistoryDocument>();
            QuotationHistoryDocumentModifiedByUser = new HashSet<QuotationHistoryDocument>();
            QuotationHistoryLineAddedByUser = new HashSet<QuotationHistoryLine>();
            QuotationHistoryLineModifiedByUser = new HashSet<QuotationHistoryLine>();
            QuotationHistoryModifiedByUser = new HashSet<QuotationHistory>();
            ReAssignmentJobAddedByUser = new HashSet<ReAssignmentJob>();
            ReAssignmentJobModifiedByUser = new HashSet<ReAssignmentJob>();
            ReceiveEmailAddedByUser = new HashSet<ReceiveEmail>();
            ReceiveEmailModifiedByUser = new HashSet<ReceiveEmail>();
            RecordVariationAddedByUser = new HashSet<RecordVariation>();
            RecordVariationModifiedByUser = new HashSet<RecordVariation>();
            RegionMasterAddedByUser = new HashSet<RegionMaster>();
            RegionMasterModifiedByUser = new HashSet<RegionMaster>();
            RegistrationFormatInformationAddedByUser = new HashSet<RegistrationFormatInformation>();
            RegistrationFormatInformationLineAddedByUser = new HashSet<RegistrationFormatInformationLine>();
            RegistrationFormatInformationLineModifiedByUser = new HashSet<RegistrationFormatInformationLine>();
            RegistrationFormatInformationLineUsersAddedByUser = new HashSet<RegistrationFormatInformationLineUsers>();
            RegistrationFormatInformationLineUsersModifiedByUser = new HashSet<RegistrationFormatInformationLineUsers>();
            RegistrationFormatInformationLineUsersUser = new HashSet<RegistrationFormatInformationLineUsers>();
            RegistrationFormatInformationModifiedByUser = new HashSet<RegistrationFormatInformation>();
            RegistrationVariationAddedByUser = new HashSet<RegistrationVariation>();
            RegistrationVariationModifiedByUser = new HashSet<RegistrationVariation>();
            RequestAcAddedByUser = new HashSet<RequestAc>();
            RequestAcModifiedByUser = new HashSet<RequestAc>();
            RequestAclineAddedByUser = new HashSet<RequestAcline>();
            RequestAclineModifiedByUser = new HashSet<RequestAcline>();
            RoutineInfoAddedByUser = new HashSet<RoutineInfo>();
            RoutineInfoModifiedByUser = new HashSet<RoutineInfo>();
            RoutineInfoMultipleAddedByUser = new HashSet<RoutineInfoMultiple>();
            RoutineInfoMultipleModifiedByUser = new HashSet<RoutineInfoMultiple>();
            SalesAdhocNovateAddedByUser = new HashSet<SalesAdhocNovate>();
            SalesAdhocNovateAttachmentAddedByUser = new HashSet<SalesAdhocNovateAttachment>();
            SalesAdhocNovateAttachmentModifiedByUser = new HashSet<SalesAdhocNovateAttachment>();
            SalesAdhocNovateModifiedByUser = new HashSet<SalesAdhocNovate>();
            SalesBorrowHeaderAddedByUser = new HashSet<SalesBorrowHeader>();
            SalesBorrowHeaderModifiedByUser = new HashSet<SalesBorrowHeader>();
            SalesBorrowHeaderOnBehalf = new HashSet<SalesBorrowHeader>();
            SalesBorrowLineAddedByUser = new HashSet<SalesBorrowLine>();
            SalesBorrowLineModifiedByUser = new HashSet<SalesBorrowLine>();
            SalesItemPackingInfoAddedByUser = new HashSet<SalesItemPackingInfo>();
            SalesItemPackingInfoModifiedByUser = new HashSet<SalesItemPackingInfo>();
            SalesItemPackingInforLineAddedByUser = new HashSet<SalesItemPackingInforLine>();
            SalesItemPackingInforLineModifiedByUser = new HashSet<SalesItemPackingInforLine>();
            SalesMainPromotionListAddedByUser = new HashSet<SalesMainPromotionList>();
            SalesMainPromotionListModifiedByUser = new HashSet<SalesMainPromotionList>();
            SalesOrderAddedByUser = new HashSet<SalesOrder>();
            SalesOrderAttachmentAddedByUser = new HashSet<SalesOrderAttachment>();
            SalesOrderAttachmentModifiedByUser = new HashSet<SalesOrderAttachment>();
            SalesOrderEntryAddedByUser = new HashSet<SalesOrderEntry>();
            SalesOrderEntryModifiedByUser = new HashSet<SalesOrderEntry>();
            SalesOrderMasterPricingAddedByUser = new HashSet<SalesOrderMasterPricing>();
            SalesOrderMasterPricingLineAddedByUser = new HashSet<SalesOrderMasterPricingLine>();
            SalesOrderMasterPricingLineModifiedByUser = new HashSet<SalesOrderMasterPricingLine>();
            SalesOrderMasterPricingModifiedByUser = new HashSet<SalesOrderMasterPricing>();
            SalesOrderModifiedByUser = new HashSet<SalesOrder>();
            SalesOrderProductAddedByUser = new HashSet<SalesOrderProduct>();
            SalesOrderProductModifiedByUser = new HashSet<SalesOrderProduct>();
            SalesOrderProfileAddedByUser = new HashSet<SalesOrderProfile>();
            SalesOrderProfileModifiedByUser = new HashSet<SalesOrderProfile>();
            SalesPromotionAddedByUser = new HashSet<SalesPromotion>();
            SalesPromotionModifiedByUser = new HashSet<SalesPromotion>();
            SalesSurveyByFieldForceAddedByUser = new HashSet<SalesSurveyByFieldForce>();
            SalesSurveyByFieldForceLineAddedByUser = new HashSet<SalesSurveyByFieldForceLine>();
            SalesSurveyByFieldForceLineModifiedByUser = new HashSet<SalesSurveyByFieldForceLine>();
            SalesSurveyByFieldForceModifiedByUser = new HashSet<SalesSurveyByFieldForce>();
            SalesSurveyByFieldForceSalesPerson = new HashSet<SalesSurveyByFieldForce>();
            SalesTargetByStaffAddedByUser = new HashSet<SalesTargetByStaff>();
            SalesTargetByStaffLineAddedByUser = new HashSet<SalesTargetByStaffLine>();
            SalesTargetByStaffLineModifiedByUser = new HashSet<SalesTargetByStaffLine>();
            SalesTargetByStaffModifiedByUser = new HashSet<SalesTargetByStaff>();
            SampleRequestForDoctorAddedByUser = new HashSet<SampleRequestForDoctor>();
            SampleRequestForDoctorHeaderAddedByUser = new HashSet<SampleRequestForDoctorHeader>();
            SampleRequestForDoctorHeaderModifiedByUser = new HashSet<SampleRequestForDoctorHeader>();
            SampleRequestForDoctorHeaderRequestBy = new HashSet<SampleRequestForDoctorHeader>();
            SampleRequestForDoctorModifiedByUser = new HashSet<SampleRequestForDoctor>();
            SampleRequestFormAddedByUser = new HashSet<SampleRequestForm>();
            SampleRequestFormLineAddedByUser = new HashSet<SampleRequestFormLine>();
            SampleRequestFormLineModifiedByUser = new HashSet<SampleRequestFormLine>();
            SampleRequestFormModifiedByUser = new HashSet<SampleRequestForm>();
            ScheduleOfDeliveryAddedByUser = new HashSet<ScheduleOfDelivery>();
            ScheduleOfDeliveryModifiedByUser = new HashSet<ScheduleOfDelivery>();
            SectionAddedByUser = new HashSet<Section>();
            SectionModifiedByUser = new HashSet<Section>();
            SellingCatalogueAddedByUser = new HashSet<SellingCatalogue>();
            SellingCatalogueModifiedByUser = new HashSet<SellingCatalogue>();
            SellingPriceInformationAddedByUser = new HashSet<SellingPriceInformation>();
            SellingPriceInformationModifiedByUser = new HashSet<SellingPriceInformation>();
            ShiftMasterAddedByUser = new HashSet<ShiftMaster>();
            ShiftMasterModifiedByUser = new HashSet<ShiftMaster>();
            ShortCutAddedByUser = new HashSet<ShortCut>();
            ShortCutModifiedByUser = new HashSet<ShortCut>();
            Smecomment = new HashSet<Smecomment>();
            SoSalesOrderAddedByUser = new HashSet<SoSalesOrder>();
            SoSalesOrderLineAddedByUser = new HashSet<SoSalesOrderLine>();
            SoSalesOrderLineModifiedByUser = new HashSet<SoSalesOrderLine>();
            SoSalesOrderModifiedByUser = new HashSet<SoSalesOrder>();
            SobyCustomersAddedByUser = new HashSet<SobyCustomers>();
            SobyCustomersMasterAddressAddedByUser = new HashSet<SobyCustomersMasterAddress>();
            SobyCustomersMasterAddressModifiedByUser = new HashSet<SobyCustomersMasterAddress>();
            SobyCustomersModifiedByUser = new HashSet<SobyCustomers>();
            SocustomersDeliveryAddedByUser = new HashSet<SocustomersDelivery>();
            SocustomersDeliveryModifiedByUser = new HashSet<SocustomersDelivery>();
            SocustomersIssueAddedByUser = new HashSet<SocustomersIssue>();
            SocustomersIssueModifiedByUser = new HashSet<SocustomersIssue>();
            SocustomersItemCrossReferenceAddedByUser = new HashSet<SocustomersItemCrossReference>();
            SocustomersItemCrossReferenceModifiedByUser = new HashSet<SocustomersItemCrossReference>();
            SocustomersSalesInfomationAddedByUser = new HashSet<SocustomersSalesInfomation>();
            SocustomersSalesInfomationModifiedByUser = new HashSet<SocustomersSalesInfomation>();
            SolotInformationAddedByUser = new HashSet<SolotInformation>();
            SolotInformationModifiedByUser = new HashSet<SolotInformation>();
            SourceListAddedByUser = new HashSet<SourceList>();
            SourceListModifiedByUser = new HashSet<SourceList>();
            SowithOutBlanketOrderAddedByUser = new HashSet<SowithOutBlanketOrder>();
            SowithOutBlanketOrderModifiedByUser = new HashSet<SowithOutBlanketOrder>();
            StaffTrainingStaff = new HashSet<StaffTraining>();
            StaffTrainingTrainner = new HashSet<StaffTraining>();
            StandardManufacturingProcessAddedByUser = new HashSet<StandardManufacturingProcess>();
            StandardManufacturingProcessLineAddedByUser = new HashSet<StandardManufacturingProcessLine>();
            StandardManufacturingProcessLineModifiedByUser = new HashSet<StandardManufacturingProcessLine>();
            StandardManufacturingProcessModifiedByUser = new HashSet<StandardManufacturingProcess>();
            StandardProcedureAddedByUser = new HashSet<StandardProcedure>();
            StandardProcedureModifiedByUser = new HashSet<StandardProcedure>();
            StartOfDayAddedByUser = new HashSet<StartOfDay>();
            StartOfDayModifiedByUser = new HashSet<StartOfDay>();
            StateAddedByUser = new HashSet<State>();
            StateModifiedByUser = new HashSet<State>();
            SubSectionAddedByUser = new HashSet<SubSection>();
            SubSectionModifiedByUser = new HashSet<SubSection>();
            SubSectionTwoAddedByUser = new HashSet<SubSectionTwo>();
            SubSectionTwoModifiedByUser = new HashSet<SubSectionTwo>();
            SunwardAssetListAddedByUser = new HashSet<SunwardAssetList>();
            SunwardAssetListModifiedByUser = new HashSet<SunwardAssetList>();
            SunwardGroupCompanyAddedByUser = new HashSet<SunwardGroupCompany>();
            SunwardGroupCompanyModifiedByUser = new HashSet<SunwardGroupCompany>();
            TableDataVersionInfo = new HashSet<TableDataVersionInfo>();
            TagAddedByUser = new HashSet<Tag>();
            TagMasterAddedByUser = new HashSet<TagMaster>();
            TagMasterModifiedByUser = new HashSet<TagMaster>();
            TagModifiedByUser = new HashSet<Tag>();
            TaskAppointment = new HashSet<TaskAppointment>();
            TaskAssignedTaskOwner = new HashSet<TaskAssigned>();
            TaskAssignedUser = new HashSet<TaskAssigned>();
            TaskAttachmentLockedByUser = new HashSet<TaskAttachment>();
            TaskAttachmentUploadedByUser = new HashSet<TaskAttachment>();
            TaskCommentCommentedByNavigation = new HashSet<TaskComment>();
            TaskCommentEditedByNavigation = new HashSet<TaskComment>();
            TaskCommentUserPinnedByNavigation = new HashSet<TaskCommentUser>();
            TaskCommentUserUser = new HashSet<TaskCommentUser>();
            TaskDiscussion = new HashSet<TaskDiscussion>();
            TaskInviteUser = new HashSet<TaskInviteUser>();
            TaskMasterAddedByUser = new HashSet<TaskMaster>();
            TaskMasterDescriptionVersionAddedByUser = new HashSet<TaskMasterDescriptionVersion>();
            TaskMasterDescriptionVersionModifiedByUser = new HashSet<TaskMasterDescriptionVersion>();
            TaskMasterModifiedByUser = new HashSet<TaskMaster>();
            TaskMasterOnBehalfNavigation = new HashSet<TaskMaster>();
            TaskMasterOwner = new HashSet<TaskMaster>();
            TaskMeetingNote = new HashSet<TaskMeetingNote>();
            TaskMembers = new HashSet<TaskMembers>();
            TaskNotes = new HashSet<TaskNotes>();
            TaskUnReadNotes = new HashSet<TaskUnReadNotes>();
            TeamMasterAddedByUser = new HashSet<TeamMaster>();
            TeamMasterModifiedByUser = new HashSet<TeamMaster>();
            TeamMember = new HashSet<TeamMember>();
            TempSalesPackInformationAddedByUser = new HashSet<TempSalesPackInformation>();
            TempSalesPackInformationFactorAddedByUser = new HashSet<TempSalesPackInformationFactor>();
            TempSalesPackInformationFactorModifiedByUser = new HashSet<TempSalesPackInformationFactor>();
            TempSalesPackInformationModifiedByUser = new HashSet<TempSalesPackInformation>();
            TempVersionAddedByUser = new HashSet<TempVersion>();
            TempVersionModifiedByUser = new HashSet<TempVersion>();
            TempVersionPersonIncharge = new HashSet<TempVersion>();
            TemplateCaseFormNotesAddedByUser = new HashSet<TemplateCaseFormNotes>();
            TemplateCaseFormNotesModifiedByUser = new HashSet<TemplateCaseFormNotes>();
            TemplateTestCaseAddedByUser = new HashSet<TemplateTestCase>();
            TemplateTestCaseCheckListAddedByUser = new HashSet<TemplateTestCaseCheckList>();
            TemplateTestCaseCheckListModifiedByUser = new HashSet<TemplateTestCaseCheckList>();
            TemplateTestCaseCheckListResponseAddedByUser = new HashSet<TemplateTestCaseCheckListResponse>();
            TemplateTestCaseCheckListResponseDutyAddedByUser = new HashSet<TemplateTestCaseCheckListResponseDuty>();
            TemplateTestCaseCheckListResponseDutyModifiedByUser = new HashSet<TemplateTestCaseCheckListResponseDuty>();
            TemplateTestCaseCheckListResponseModifiedByUser = new HashSet<TemplateTestCaseCheckListResponse>();
            TemplateTestCaseCheckListResponseRecurrenceAddedByUser = new HashSet<TemplateTestCaseCheckListResponseRecurrence>();
            TemplateTestCaseCheckListResponseRecurrenceModifiedByUser = new HashSet<TemplateTestCaseCheckListResponseRecurrence>();
            TemplateTestCaseCheckListResponseResponsible = new HashSet<TemplateTestCaseCheckListResponseResponsible>();
            TemplateTestCaseFormAddedByUser = new HashSet<TemplateTestCaseForm>();
            TemplateTestCaseFormModifiedByUser = new HashSet<TemplateTestCaseForm>();
            TemplateTestCaseLinkAddedByUser = new HashSet<TemplateTestCaseLink>();
            TemplateTestCaseLinkDocAddedByUser = new HashSet<TemplateTestCaseLinkDoc>();
            TemplateTestCaseLinkDocModifiedByUser = new HashSet<TemplateTestCaseLinkDoc>();
            TemplateTestCaseLinkModifiedByUser = new HashSet<TemplateTestCaseLink>();
            TemplateTestCaseModifiedByUser = new HashSet<TemplateTestCase>();
            TemplateTestCaseProposalAddedByUser = new HashSet<TemplateTestCaseProposal>();
            TemplateTestCaseProposalModifiedByUser = new HashSet<TemplateTestCaseProposal>();
            TemplateTestCaseProposalReassignUser = new HashSet<TemplateTestCaseProposal>();
            TemplateTestCaseProposalUser = new HashSet<TemplateTestCaseProposal>();
            TenderInformationAddedByUser = new HashSet<TenderInformation>();
            TenderInformationModifiedByUser = new HashSet<TenderInformation>();
            TenderPeriodPricingAddedByUser = new HashSet<TenderPeriodPricing>();
            TenderPeriodPricingLineAddedByUser = new HashSet<TenderPeriodPricingLine>();
            TenderPeriodPricingLineModifiedByUser = new HashSet<TenderPeriodPricingLine>();
            TenderPeriodPricingModifiedByUser = new HashSet<TenderPeriodPricing>();
            TransferBalanceQtyAddedByUser = new HashSet<TransferBalanceQty>();
            TransferBalanceQtyDocumentAddedByUser = new HashSet<TransferBalanceQtyDocument>();
            TransferBalanceQtyDocumentModifiedByUser = new HashSet<TransferBalanceQtyDocument>();
            TransferBalanceQtyModifiedByUser = new HashSet<TransferBalanceQty>();
            TransferPermissionLogAddedByUser = new HashSet<TransferPermissionLog>();
            TransferPermissionLogCurrentUser = new HashSet<TransferPermissionLog>();
            TransferPermissionLogPreviousUser = new HashSet<TransferPermissionLog>();
            TransferSettingsAddedByUser = new HashSet<TransferSettings>();
            TransferSettingsModifiedByUser = new HashSet<TransferSettings>();
            UnitConversionAddedByUser = new HashSet<UnitConversion>();
            UnitConversionModifiedByUser = new HashSet<UnitConversion>();
            UserDepartment = new HashSet<UserDepartment>();
            UserGroupAddedByUser = new HashSet<UserGroup>();
            UserGroupModifiedByUser = new HashSet<UserGroup>();
            UserGroupUser = new HashSet<UserGroupUser>();
            UserSettings = new HashSet<UserSettings>();
            VendorsListAddedByUser = new HashSet<VendorsList>();
            VendorsListModifiedByUser = new HashSet<VendorsList>();
            WikiAccess = new HashSet<WikiAccess>();
            WikiAccessRight = new HashSet<WikiAccessRight>();
            WikiGroupAddedByUser = new HashSet<WikiGroup>();
            WikiGroupModifiedByUser = new HashSet<WikiGroup>();
            WikiNotes = new HashSet<WikiNotes>();
            WikiPageAction = new HashSet<WikiPageAction>();
            WikiPageActionReply = new HashSet<WikiPageActionReply>();
            WikiPageAddedByUser = new HashSet<WikiPage>();
            WikiPageModifiedByUser = new HashSet<WikiPage>();
            WikiPageNextApprover = new HashSet<WikiPage>();
            WikiPageOwner = new HashSet<WikiPage>();
            WikiPagePrimaryApprover = new HashSet<WikiPage>();
            WikiResponsible = new HashSet<WikiResponsible>();
            WikiSearch = new HashSet<WikiSearch>();
            WorkFlowAddedByUser = new HashSet<WorkFlow>();
            WorkFlowApproverSetupAddedByUser = new HashSet<WorkFlowApproverSetup>();
            WorkFlowApproverSetupApprover = new HashSet<WorkFlowApproverSetup>();
            WorkFlowApproverSetupModifiedByUser = new HashSet<WorkFlowApproverSetup>();
            WorkFlowApproverSetupUser = new HashSet<WorkFlowApproverSetup>();
            WorkFlowInteractionAddedByUser = new HashSet<WorkFlowInteraction>();
            WorkFlowInteractionModifiedByUser = new HashSet<WorkFlowInteraction>();
            WorkFlowInteractionRecipient = new HashSet<WorkFlowInteractionRecipient>();
            WorkFlowModifiedByUser = new HashSet<WorkFlow>();
            WorkFlowPagesAddedByUser = new HashSet<WorkFlowPages>();
            WorkFlowPagesModifiedByUser = new HashSet<WorkFlowPages>();
            WorkFlowUserGroupUsers = new HashSet<WorkFlowUserGroupUsers>();
            WorkListAddedByUser = new HashSet<WorkList>();
            WorkListAssignedByNavigation = new HashSet<WorkList>();
            WorkListAssignedToNavigation = new HashSet<WorkList>();
            WorkListModifiedByUser = new HashSet<WorkList>();
            WorkOrderAddedByUser = new HashSet<WorkOrder>();
            WorkOrderAssignToNavigation = new HashSet<WorkOrder>();
            WorkOrderCommentCommentedByNavigation = new HashSet<WorkOrderComment>();
            WorkOrderCommentEditedByNavigation = new HashSet<WorkOrderComment>();
            WorkOrderCommentUser = new HashSet<WorkOrderCommentUser>();
            WorkOrderLineAddedByUser = new HashSet<WorkOrderLine>();
            WorkOrderLineModifiedByUser = new HashSet<WorkOrderLine>();
            WorkOrderModifiedByUser = new HashSet<WorkOrder>();
        }

        public long UserId { get; set; }
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public string EmployeeNo { get; set; }
        public string UserEmail { get; set; }
        public byte AuthenticationType { get; set; }
        public string LoginId { get; set; }
        public string LoginPassword { get; set; }
        public long? DepartmentId { get; set; }
        public bool IsPasswordChanged { get; set; }
        public int? StatusCodeId { get; set; }
        public DateTime? LastAccessDate { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? EmployeId { get; set; }
        public Guid? SessionId { get; set; }
        public int? InvalidAttempts { get; set; }
        public bool? Locked { get; set; }
        public DateTime? LastPasswordChanged { get; set; }
        public string MobileDeviceId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Department Department { get; set; }
        public virtual Employee Employe { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<Acentry> AcentryAddedByUser { get; set; }
        public virtual ICollection<AcentryLines> AcentryLinesAddedByUser { get; set; }
        public virtual ICollection<AcentryLines> AcentryLinesModifiedByUser { get; set; }
        public virtual ICollection<Acentry> AcentryModifiedByUser { get; set; }
        public virtual ICollection<Acitems> AcitemsAddedByUser { get; set; }
        public virtual ICollection<Acitems> AcitemsModifiedByUser { get; set; }
        public virtual ICollection<ActiveFlow> ActiveFlow { get; set; }
        public virtual ICollection<ActiveFlowDetails> ActiveFlowDetails { get; set; }
        public virtual ICollection<ActivityEmailTopics> ActivityEmailTopicsAddedByUser { get; set; }
        public virtual ICollection<ActivityEmailTopics> ActivityEmailTopicsModifiedByUser { get; set; }
        public virtual ICollection<AdditionalProcess> AdditionalProcessAddedByUser { get; set; }
        public virtual ICollection<AdditionalProcess> AdditionalProcessModifiedByUser { get; set; }
        public virtual ICollection<AppIpirentry> AppIpirentryAddedByUser { get; set; }
        public virtual ICollection<AppIpirentry> AppIpirentryModifiedByUser { get; set; }
        public virtual ICollection<AppSampling> AppSamplingAddedByUser { get; set; }
        public virtual ICollection<AppSamplingLine> AppSamplingLineAddedByUser { get; set; }
        public virtual ICollection<AppSamplingLine> AppSamplingLineModifiedByUser { get; set; }
        public virtual ICollection<AppSampling> AppSamplingModifiedByUser { get; set; }
        public virtual ICollection<AppTranDocumentLine> AppTranDocumentLineAddedByUser { get; set; }
        public virtual ICollection<AppTranDocumentLine> AppTranDocumentLineModifiedByUser { get; set; }
        public virtual ICollection<AppTranLfromLto> AppTranLfromLtoAddedByUser { get; set; }
        public virtual ICollection<AppTranLfromLtoLine> AppTranLfromLtoLineAddedByUser { get; set; }
        public virtual ICollection<AppTranLfromLtoLine> AppTranLfromLtoLineModifiedByUser { get; set; }
        public virtual ICollection<AppTranLfromLto> AppTranLfromLtoModifiedByUser { get; set; }
        public virtual ICollection<AppTranStock> AppTranStockAddedByUser { get; set; }
        public virtual ICollection<AppTranStockLine> AppTranStockLineAddedByUser { get; set; }
        public virtual ICollection<AppTranStockLine> AppTranStockLineModifiedByUser { get; set; }
        public virtual ICollection<AppTranStock> AppTranStockModifiedByUser { get; set; }
        public virtual ICollection<AppconsumptionEntry> AppconsumptionEntryAddedByUser { get; set; }
        public virtual ICollection<AppconsumptionEntry> AppconsumptionEntryModifiedByUser { get; set; }
        public virtual ICollection<AppconsumptionLines> AppconsumptionLines { get; set; }
        public virtual ICollection<AppdispenserDispensing> AppdispenserDispensingAddedByUser { get; set; }
        public virtual ICollection<AppdispenserDispensing> AppdispenserDispensingModifiedByUser { get; set; }
        public virtual ICollection<Appdrumming> AppdrummingAddedByUser { get; set; }
        public virtual ICollection<Appdrumming> AppdrummingModifiedByUser { get; set; }
        public virtual ICollection<ApplicationAbbreviation> ApplicationAbbreviationAddedByUser { get; set; }
        public virtual ICollection<ApplicationAbbreviation> ApplicationAbbreviationModifiedByUser { get; set; }
        public virtual ICollection<ApplicationForm> ApplicationFormAddedByUser { get; set; }
        public virtual ICollection<ApplicationFormFields> ApplicationFormFields { get; set; }
        public virtual ICollection<ApplicationForm> ApplicationFormModifiedByUser { get; set; }
        public virtual ICollection<ApplicationForm> ApplicationFormPersonIncharge { get; set; }
        public virtual ICollection<ApplicationFormSearch> ApplicationFormSearchAddedByUser { get; set; }
        public virtual ICollection<ApplicationFormSearch> ApplicationFormSearchModifiedByUser { get; set; }
        public virtual ICollection<ApplicationGlossary> ApplicationGlossaryAddedByUser { get; set; }
        public virtual ICollection<ApplicationGlossary> ApplicationGlossaryModifiedByUser { get; set; }
        public virtual ICollection<ApplicationMasterAccess> ApplicationMasterAccess { get; set; }
        public virtual ICollection<ApplicationMasterChild> ApplicationMasterChildAddedByUser { get; set; }
        public virtual ICollection<ApplicationMasterChild> ApplicationMasterChildModifiedByUser { get; set; }
        public virtual ICollection<ApplicationMasterDetail> ApplicationMasterDetailAddedByUser { get; set; }
        public virtual ICollection<ApplicationMasterDetail> ApplicationMasterDetailModifiedByUser { get; set; }
        public virtual ICollection<ApplicationMasterParent> ApplicationMasterParentAddedByUser { get; set; }
        public virtual ICollection<ApplicationMasterParent> ApplicationMasterParentModifiedByUser { get; set; }
        public virtual ICollection<ApplicationReminder> ApplicationReminderAddedByUser { get; set; }
        public virtual ICollection<ApplicationReminder> ApplicationReminderModifiedUser { get; set; }
        public virtual ICollection<ApplicationRole> ApplicationRoleAddedByUser { get; set; }
        public virtual ICollection<ApplicationRole> ApplicationRoleModifiedByUser { get; set; }
        public virtual ICollection<ApplicationUserPlant> ApplicationUserPlant { get; set; }
        public virtual ICollection<ApplicationUserRole> ApplicationUserRole { get; set; }
        public virtual ICollection<ApplicationWiki> ApplicationWikiAddedByUser { get; set; }
        public virtual ICollection<ApplicationWikiLine> ApplicationWikiLineAddedByUser { get; set; }
        public virtual ICollection<ApplicationWikiLineDuty> ApplicationWikiLineDutyAddedByUser { get; set; }
        public virtual ICollection<ApplicationWikiLineDuty> ApplicationWikiLineDutyModifiedByUser { get; set; }
        public virtual ICollection<ApplicationWikiLine> ApplicationWikiLineModifiedByUser { get; set; }
        public virtual ICollection<ApplicationWikiLineNotify> ApplicationWikiLineNotify { get; set; }
        public virtual ICollection<ApplicationWiki> ApplicationWikiModifiedByUser { get; set; }
        public virtual ICollection<ApplicationWikiRecurrence> ApplicationWikiRecurrenceAddedByUser { get; set; }
        public virtual ICollection<ApplicationWikiRecurrence> ApplicationWikiRecurrenceModifiedByUser { get; set; }
        public virtual ICollection<ApplicationWiki> ApplicationWikiWikiEntryBy { get; set; }
        public virtual ICollection<AppmaterialReturn> AppmaterialReturnAddedByUser { get; set; }
        public virtual ICollection<AppmaterialReturn> AppmaterialReturnModifiedByUser { get; set; }
        public virtual ICollection<ApprovalDetails> ApprovalDetailsAddedByUser { get; set; }
        public virtual ICollection<ApprovalDetails> ApprovalDetailsModifiedByUser { get; set; }
        public virtual ICollection<ApproveMaster> ApproveMasterAddedByUser { get; set; }
        public virtual ICollection<ApproveMaster> ApproveMasterModifiedByUser { get; set; }
        public virtual ICollection<ApproverSetup> ApproverSetupAddedByUser { get; set; }
        public virtual ICollection<ApproverSetup> ApproverSetupApprover { get; set; }
        public virtual ICollection<ApproverSetup> ApproverSetupModifiedByUser { get; set; }
        public virtual ICollection<AppsupervisorDispensingEntry> AppsupervisorDispensingEntryAddedByUser { get; set; }
        public virtual ICollection<AppsupervisorDispensingEntry> AppsupervisorDispensingEntryModifiedByUser { get; set; }
        public virtual ICollection<ApptransferRelcassEntry> ApptransferRelcassEntryAddedByUser { get; set; }
        public virtual ICollection<ApptransferRelcassEntry> ApptransferRelcassEntryModifiedByUser { get; set; }
        public virtual ICollection<AssetAssignment> AssetAssignmentAddedByUser { get; set; }
        public virtual ICollection<AssetAssignment> AssetAssignmentAssetManagedByNavigation { get; set; }
        public virtual ICollection<AssetAssignment> AssetAssignmentAssignedToNavigation { get; set; }
        public virtual ICollection<AssetAssignment> AssetAssignmentModifiedByUser { get; set; }
        public virtual ICollection<AssetCatalogMaster> AssetCatalogMasterAddedByUser { get; set; }
        public virtual ICollection<AssetCatalogMaster> AssetCatalogMasterModifiedByUser { get; set; }
        public virtual ICollection<AssetDisposal> AssetDisposalAddedByUser { get; set; }
        public virtual ICollection<AssetDisposal> AssetDisposalModifiedByUser { get; set; }
        public virtual ICollection<AssetEquipmentMaintenaceMaster> AssetEquipmentMaintenaceMasterAddedByUser { get; set; }
        public virtual ICollection<AssetEquipmentMaintenaceMaster> AssetEquipmentMaintenaceMasterModifiedByUser { get; set; }
        public virtual ICollection<AssetMaintenance> AssetMaintenanceAddedByUser { get; set; }
        public virtual ICollection<AssetMaintenance> AssetMaintenanceModifiedByUser { get; set; }
        public virtual ICollection<AssetMaintenance> AssetMaintenancePerformedByNavigation { get; set; }
        public virtual ICollection<AssetMaster> AssetMasterAddedByUser { get; set; }
        public virtual ICollection<AssetMaster> AssetMasterModifiedByUser { get; set; }
        public virtual ICollection<AssetPartsMaintenaceMaster> AssetPartsMaintenaceMasterAddedByUser { get; set; }
        public virtual ICollection<AssetPartsMaintenaceMaster> AssetPartsMaintenaceMasterModifiedByUser { get; set; }
        public virtual ICollection<AssetTicket> AssetTicketAddedByUser { get; set; }
        public virtual ICollection<AssetTicket> AssetTicketModifiedByUser { get; set; }
        public virtual ICollection<AssetTransfer> AssetTransferAddedByUser { get; set; }
        public virtual ICollection<AssetTransfer> AssetTransferModifiedByUser { get; set; }
        public virtual ICollection<AttributeDetails> AttributeDetailsAddedByUser { get; set; }
        public virtual ICollection<AttributeDetails> AttributeDetailsModifiedByUser { get; set; }
        public virtual ICollection<AttributeHeader> AttributeHeaderAddedByUser { get; set; }
        public virtual ICollection<AttributeHeader> AttributeHeaderModifiedByUser { get; set; }
        public virtual ICollection<BaseUom> BaseUomAddedByUser { get; set; }
        public virtual ICollection<BaseUom> BaseUomModifiedByUser { get; set; }
        public virtual ICollection<BlanketOrder> BlanketOrderAddedByUser { get; set; }
        public virtual ICollection<BlanketOrder> BlanketOrderModifiedByUser { get; set; }
        public virtual ICollection<BlisterMouldInformation> BlisterMouldInformationAddedByUser { get; set; }
        public virtual ICollection<BlisterMouldInformation> BlisterMouldInformationModifiedByUser { get; set; }
        public virtual ICollection<BlisterScrapCode> BlisterScrapCodeAddedByUser { get; set; }
        public virtual ICollection<BlisterScrapCode> BlisterScrapCodeModifiedByUser { get; set; }
        public virtual ICollection<BlisterScrapEntry> BlisterScrapEntryAddedByUser { get; set; }
        public virtual ICollection<BlisterScrapEntry> BlisterScrapEntryModifiedByUser { get; set; }
        public virtual ICollection<BlisterScrapMaster> BlisterScrapMasterAddedByUser { get; set; }
        public virtual ICollection<BlisterScrapMaster> BlisterScrapMasterModifiedByUser { get; set; }
        public virtual ICollection<BmrdisposalBox> BmrdisposalBoxAddedByUser { get; set; }
        public virtual ICollection<BmrdisposalBox> BmrdisposalBoxModifiedByUser { get; set; }
        public virtual ICollection<BmrdisposalMasterBox> BmrdisposalMasterBoxAddedByUser { get; set; }
        public virtual ICollection<BmrdisposalMasterBox> BmrdisposalMasterBoxModifiedByUser { get; set; }
        public virtual ICollection<Bmrmovement> BmrmovementAddedByUser { get; set; }
        public virtual ICollection<BmrmovementLine> BmrmovementLineAddedByUser { get; set; }
        public virtual ICollection<BmrmovementLine> BmrmovementLineModifiedByUser { get; set; }
        public virtual ICollection<Bmrmovement> BmrmovementModifiedByUser { get; set; }
        public virtual ICollection<BmrtoCarton> BmrtoCartonAddedByUser { get; set; }
        public virtual ICollection<BmrtoCartonLine> BmrtoCartonLineAddedByUser { get; set; }
        public virtual ICollection<BmrtoCartonLine> BmrtoCartonLineModifiedByUser { get; set; }
        public virtual ICollection<BmrtoCarton> BmrtoCartonModifiedByUser { get; set; }
        public virtual ICollection<BompackingMaster> BompackingMasterAddedByUser { get; set; }
        public virtual ICollection<BompackingMasterLine> BompackingMasterLineAddedByUser { get; set; }
        public virtual ICollection<BompackingMasterLine> BompackingMasterLineModifiedByUser { get; set; }
        public virtual ICollection<BompackingMaster> BompackingMasterModifiedByUser { get; set; }
        public virtual ICollection<BomproductionGroup> BomproductionGroupAddedByUser { get; set; }
        public virtual ICollection<BomproductionGroup> BomproductionGroupModifiedByUser { get; set; }
        public virtual ICollection<CalibrationServiceInfo> CalibrationServiceInfoAddedByUser { get; set; }
        public virtual ICollection<CalibrationServiceInfo> CalibrationServiceInfoModifiedByUser { get; set; }
        public virtual ICollection<CalibrationType> CalibrationTypeAddedByUser { get; set; }
        public virtual ICollection<CalibrationType> CalibrationTypeModifiedByUser { get; set; }
        public virtual ICollection<Category> CategoryAddedByUser { get; set; }
        public virtual ICollection<Category> CategoryModifiedByUser { get; set; }
        public virtual ICollection<Ccfainformation> CcfainformationAddedByUser { get; set; }
        public virtual ICollection<Ccfainformation> CcfainformationModifiedByUser { get; set; }
        public virtual ICollection<Ccfainformation> CcfainformationPiaNavigation { get; set; }
        public virtual ICollection<Ccfainformation> CcfainformationPicNavigation { get; set; }
        public virtual ICollection<Ccfbevaluation> CcfbevaluationAddedByUser { get; set; }
        public virtual ICollection<Ccfbevaluation> CcfbevaluationEngineeringEvaluatedByNavigation { get; set; }
        public virtual ICollection<Ccfbevaluation> CcfbevaluationEvaluatedByNavigation { get; set; }
        public virtual ICollection<Ccfbevaluation> CcfbevaluationModifiedByUser { get; set; }
        public virtual ICollection<Ccfbevaluation> CcfbevaluationProductionEvaluatedByNavigation { get; set; }
        public virtual ICollection<Ccfbevaluation> CcfbevaluationQaevaluatedByNavigation { get; set; }
        public virtual ICollection<Ccfbevaluation> CcfbevaluationQualityControlEvaluatedByNavigation { get; set; }
        public virtual ICollection<Ccfbevaluation> CcfbevaluationRequlatoryEvaluatedByNavigation { get; set; }
        public virtual ICollection<Ccfbevaluation> CcfbevaluationStoreEvaluatedByNavigation { get; set; }
        public virtual ICollection<Ccfcapproval> CcfcapprovalAddedByUser { get; set; }
        public virtual ICollection<Ccfcapproval> CcfcapprovalModifiedByUser { get; set; }
        public virtual ICollection<Ccfcapproval> CcfcapprovalVerifiedByNavigation { get; set; }
        public virtual ICollection<Ccfdimplementation> CcfdimplementationAddedByUser { get; set; }
        public virtual ICollection<CcfdimplementationDetails> CcfdimplementationDetailsAddedByUser { get; set; }
        public virtual ICollection<CcfdimplementationDetails> CcfdimplementationDetailsDoneByNavigation { get; set; }
        public virtual ICollection<CcfdimplementationDetails> CcfdimplementationDetailsModifiedByUser { get; set; }
        public virtual ICollection<CcfdimplementationDetails> CcfdimplementationDetailsResponsibiltyToNavigation { get; set; }
        public virtual ICollection<Ccfdimplementation> CcfdimplementationModifiedByUser { get; set; }
        public virtual ICollection<Ccfdimplementation> CcfdimplementationVerifiedByNavigation { get; set; }
        public virtual ICollection<ChangeControlForm> ChangeControlFormAddedByUser { get; set; }
        public virtual ICollection<ChangeControlForm> ChangeControlFormModifiedByUser { get; set; }
        public virtual ICollection<ChatGroup> ChatGroupAddedByUser { get; set; }
        public virtual ICollection<ChatGroup> ChatGroupModifiedByUser { get; set; }
        public virtual ICollection<ChatGroupUsers> ChatGroupUsers { get; set; }
        public virtual ICollection<ChatMessage> ChatMessageReceiveUser { get; set; }
        public virtual ICollection<ChatMessage> ChatMessageSentUser { get; set; }
        public virtual ICollection<ChatUser> ChatUserAddedByUser { get; set; }
        public virtual ICollection<ChatUser> ChatUserModifiedByUser { get; set; }
        public virtual ICollection<ChatUser> ChatUserUser { get; set; }
        public virtual ICollection<CityMaster> CityMasterAddedByUser { get; set; }
        public virtual ICollection<CityMaster> CityMasterModifiedByUser { get; set; }
        public virtual ICollection<ClassificationBmr> ClassificationBmrAddedByUser { get; set; }
        public virtual ICollection<ClassificationBmrLine> ClassificationBmrLineAddedByUser { get; set; }
        public virtual ICollection<ClassificationBmrLine> ClassificationBmrLineModifiedByUser { get; set; }
        public virtual ICollection<ClassificationBmr> ClassificationBmrModifiedByUser { get; set; }
        public virtual ICollection<ClassificationProduction> ClassificationProductionAddedByUser { get; set; }
        public virtual ICollection<ClassificationProduction> ClassificationProductionModifiedByUser { get; set; }
        public virtual ICollection<CloseDocumentPermission> CloseDocumentPermission { get; set; }
        public virtual ICollection<CommentAttachment> CommentAttachment { get; set; }
        public virtual ICollection<CommitmentOrders> CommitmentOrdersAddedByUser { get; set; }
        public virtual ICollection<CommitmentOrdersLine> CommitmentOrdersLineAddedByUser { get; set; }
        public virtual ICollection<CommitmentOrdersLine> CommitmentOrdersLineModifiedByUser { get; set; }
        public virtual ICollection<CommitmentOrders> CommitmentOrdersModifiedByUser { get; set; }
        public virtual ICollection<CommonFieldsProductionMachine> CommonFieldsProductionMachineAddedByUser { get; set; }
        public virtual ICollection<CommonFieldsProductionMachineDocument> CommonFieldsProductionMachineDocumentAddedByUser { get; set; }
        public virtual ICollection<CommonFieldsProductionMachineDocument> CommonFieldsProductionMachineDocumentModifiedByUser { get; set; }
        public virtual ICollection<CommonFieldsProductionMachineLine> CommonFieldsProductionMachineLineAddedByUser { get; set; }
        public virtual ICollection<CommonFieldsProductionMachineLineLine> CommonFieldsProductionMachineLineLineAddedByUser { get; set; }
        public virtual ICollection<CommonFieldsProductionMachineLineLine> CommonFieldsProductionMachineLineLineModifiedByUser { get; set; }
        public virtual ICollection<CommonFieldsProductionMachineLine> CommonFieldsProductionMachineLineModifiedByUser { get; set; }
        public virtual ICollection<CommonFieldsProductionMachine> CommonFieldsProductionMachineModifiedByUser { get; set; }
        public virtual ICollection<CommonMasterMouldChangeParts> CommonMasterMouldChangePartsAddedByUser { get; set; }
        public virtual ICollection<CommonMasterMouldChangeParts> CommonMasterMouldChangePartsModifiedByUser { get; set; }
        public virtual ICollection<CommonMasterToolingClassification> CommonMasterToolingClassificationAddedByUser { get; set; }
        public virtual ICollection<CommonMasterToolingClassification> CommonMasterToolingClassificationModifiedByUser { get; set; }
        public virtual ICollection<CommonPackagingBottle> CommonPackagingBottleAddedByUser { get; set; }
        public virtual ICollection<CommonPackagingBottleCap> CommonPackagingBottleCapAddedByUser { get; set; }
        public virtual ICollection<CommonPackagingBottleCap> CommonPackagingBottleCapModifiedByUser { get; set; }
        public virtual ICollection<CommonPackagingBottleInsert> CommonPackagingBottleInsertAddedByUser { get; set; }
        public virtual ICollection<CommonPackagingBottleInsert> CommonPackagingBottleInsertModifiedByUser { get; set; }
        public virtual ICollection<CommonPackagingBottle> CommonPackagingBottleModifiedByUser { get; set; }
        public virtual ICollection<CommonPackagingCapseal> CommonPackagingCapsealAddedByUser { get; set; }
        public virtual ICollection<CommonPackagingCapseal> CommonPackagingCapsealModifiedByUser { get; set; }
        public virtual ICollection<CommonPackagingCarton> CommonPackagingCartonAddedByUser { get; set; }
        public virtual ICollection<CommonPackagingCarton> CommonPackagingCartonModifiedByUser { get; set; }
        public virtual ICollection<CommonPackagingDivider> CommonPackagingDividerAddedByUser { get; set; }
        public virtual ICollection<CommonPackagingDivider> CommonPackagingDividerModifiedByUser { get; set; }
        public virtual ICollection<CommonPackagingItemHeader> CommonPackagingItemHeaderAddedByUser { get; set; }
        public virtual ICollection<CommonPackagingItemHeader> CommonPackagingItemHeaderModifiedByUser { get; set; }
        public virtual ICollection<CommonPackagingLayerPad> CommonPackagingLayerPadAddedByUser { get; set; }
        public virtual ICollection<CommonPackagingLayerPad> CommonPackagingLayerPadModifiedByUser { get; set; }
        public virtual ICollection<CommonPackagingNesting> CommonPackagingNestingAddedByUser { get; set; }
        public virtual ICollection<CommonPackagingNesting> CommonPackagingNestingModifiedByUser { get; set; }
        public virtual ICollection<CommonPackagingSetInfo> CommonPackagingSetInfoAddedByUser { get; set; }
        public virtual ICollection<CommonPackagingSetInfo> CommonPackagingSetInfoModifiedByUser { get; set; }
        public virtual ICollection<CommonPackagingShrinkWrap> CommonPackagingShrinkWrapAddedByUser { get; set; }
        public virtual ICollection<CommonPackagingShrinkWrap> CommonPackagingShrinkWrapModifiedByUser { get; set; }
        public virtual ICollection<CommonPackingInformation> CommonPackingInformationAddedByUser { get; set; }
        public virtual ICollection<CommonPackingInformationLine> CommonPackingInformationLineAddedByUser { get; set; }
        public virtual ICollection<CommonPackingInformationLine> CommonPackingInformationLineModifiedByUser { get; set; }
        public virtual ICollection<CommonPackingInformation> CommonPackingInformationModifiedByUser { get; set; }
        public virtual ICollection<CommonPackingItemListLine> CommonPackingItemListLineAddedByUser { get; set; }
        public virtual ICollection<CommonPackingItemListLine> CommonPackingItemListLineModifiedByUser { get; set; }
        public virtual ICollection<CommonProcess> CommonProcessAddedByUser { get; set; }
        public virtual ICollection<CommonProcessLine> CommonProcessLineAddedByUser { get; set; }
        public virtual ICollection<CommonProcessLine> CommonProcessLineModifiedByUser { get; set; }
        public virtual ICollection<CommonProcess> CommonProcessModifiedByUser { get; set; }
        public virtual ICollection<CompanyCalandarDocumentPermission> CompanyCalandarDocumentPermissionAddedByUser { get; set; }
        public virtual ICollection<CompanyCalandarDocumentPermission> CompanyCalandarDocumentPermissionModifiedByUser { get; set; }
        public virtual ICollection<CompanyCalandarDocumentPermission> CompanyCalandarDocumentPermissionUser { get; set; }
        public virtual ICollection<CompanyCalendar> CompanyCalendarAddedByUser { get; set; }
        public virtual ICollection<CompanyCalendarAssistance> CompanyCalendarAssistance { get; set; }
        public virtual ICollection<CompanyCalendar> CompanyCalendarAssistanceFrom { get; set; }
        public virtual ICollection<CompanyCalendarLine> CompanyCalendarLineAddedByUser { get; set; }
        public virtual ICollection<CompanyCalendarLineLink> CompanyCalendarLineLinkAddedByUser { get; set; }
        public virtual ICollection<CompanyCalendarLineLink> CompanyCalendarLineLinkModifiedByUser { get; set; }
        public virtual ICollection<CompanyCalendarLineLinkPrintUser> CompanyCalendarLineLinkPrintUser { get; set; }
        public virtual ICollection<CompanyCalendarLineLinkUser> CompanyCalendarLineLinkUser { get; set; }
        public virtual ICollection<CompanyCalendarLineLinkUserLink> CompanyCalendarLineLinkUserLinkAddedByUser { get; set; }
        public virtual ICollection<CompanyCalendarLineLinkUserLink> CompanyCalendarLineLinkUserLinkModifiedByUser { get; set; }
        public virtual ICollection<CompanyCalendarLineLinkUserLink> CompanyCalendarLineLinkUserLinkUser { get; set; }
        public virtual ICollection<CompanyCalendarLineMeetingNotes> CompanyCalendarLineMeetingNotesAddedByUser { get; set; }
        public virtual ICollection<CompanyCalendarLineMeetingNotes> CompanyCalendarLineMeetingNotesModifiedByUser { get; set; }
        public virtual ICollection<CompanyCalendarLine> CompanyCalendarLineModifiedByUser { get; set; }
        public virtual ICollection<CompanyCalendarLineNotes> CompanyCalendarLineNotesAddedByUser { get; set; }
        public virtual ICollection<CompanyCalendarLineNotes> CompanyCalendarLineNotesModifiedByUser { get; set; }
        public virtual ICollection<CompanyCalendarLineNotes> CompanyCalendarLineNotesUser { get; set; }
        public virtual ICollection<CompanyCalendarLineNotesUser> CompanyCalendarLineNotesUserNavigation { get; set; }
        public virtual ICollection<CompanyCalendarLineUser> CompanyCalendarLineUser { get; set; }
        public virtual ICollection<CompanyCalendar> CompanyCalendarModifiedByUser { get; set; }
        public virtual ICollection<CompanyCalendar> CompanyCalendarOnBehalf { get; set; }
        public virtual ICollection<CompanyCalendar> CompanyCalendarOwner { get; set; }
        public virtual ICollection<CompanyCalendar> CompanyCalendarUser { get; set; }
        public virtual ICollection<CompanyEvents> CompanyEventsAddedByUser { get; set; }
        public virtual ICollection<CompanyEvents> CompanyEventsDeptOwnerNavigation { get; set; }
        public virtual ICollection<CompanyEvents> CompanyEventsModifiedByUser { get; set; }
        public virtual ICollection<CompanyHolidays> CompanyHolidaysAddedByUser { get; set; }
        public virtual ICollection<CompanyHolidays> CompanyHolidaysModifiedByUser { get; set; }
        public virtual ICollection<CompanyListing> CompanyListingAddedByUser { get; set; }
        public virtual ICollection<CompanyListingLine> CompanyListingLineAddedByUser { get; set; }
        public virtual ICollection<CompanyListingLine> CompanyListingLineModifiedByUser { get; set; }
        public virtual ICollection<CompanyListing> CompanyListingModifiedByUser { get; set; }
        public virtual ICollection<CompanyWorkDayMaster> CompanyWorkDayMasterAddedByUser { get; set; }
        public virtual ICollection<CompanyWorkDayMaster> CompanyWorkDayMasterModifiedByUser { get; set; }
        public virtual ICollection<Contact> ContactAddedByUser { get; set; }
        public virtual ICollection<Contact> ContactModifiedByUser { get; set; }
        public virtual ICollection<Contacts> ContactsAddedByUser { get; set; }
        public virtual ICollection<Contacts> ContactsModifiedByUser { get; set; }
        public virtual ICollection<Contacts> ContactsOwnerNavigation { get; set; }
        public virtual ICollection<ContractDistributionAttachment> ContractDistributionAttachmentAddedByUser { get; set; }
        public virtual ICollection<ContractDistributionAttachment> ContractDistributionAttachmentModifiedByUser { get; set; }
        public virtual ICollection<ContractDistributionSalesEntryLine> ContractDistributionSalesEntryLineAddedByUser { get; set; }
        public virtual ICollection<ContractDistributionSalesEntryLine> ContractDistributionSalesEntryLineModifiedByUser { get; set; }
        public virtual ICollection<Country> CountryAddedByUser { get; set; }
        public virtual ICollection<CountryInformationStatus> CountryInformationStatus { get; set; }
        public virtual ICollection<Country> CountryModifiedByUser { get; set; }
        public virtual ICollection<CriticalstepandIntermediate> CriticalstepandIntermediateAddedByUser { get; set; }
        public virtual ICollection<CriticalstepandIntermediateLine> CriticalstepandIntermediateLineAddedByUser { get; set; }
        public virtual ICollection<CriticalstepandIntermediateLine> CriticalstepandIntermediateLineModifiedByUser { get; set; }
        public virtual ICollection<CriticalstepandIntermediate> CriticalstepandIntermediateModifiedByUser { get; set; }
        public virtual ICollection<CustomerAcceptanceConfirmation> CustomerAcceptanceConfirmationAddedByUser { get; set; }
        public virtual ICollection<CustomerAcceptanceConfirmation> CustomerAcceptanceConfirmationModifiedByUser { get; set; }
        public virtual ICollection<Department> DepartmentAddedByUser { get; set; }
        public virtual ICollection<Department> DepartmentModifiedByUser { get; set; }
        public virtual ICollection<Designation> DesignationAddedByUser { get; set; }
        public virtual ICollection<Designation> DesignationModifiedByUser { get; set; }
        public virtual ICollection<DeviceCatalogMaster> DeviceCatalogMasterAddedByUser { get; set; }
        public virtual ICollection<DeviceCatalogMaster> DeviceCatalogMasterModifiedByUser { get; set; }
        public virtual ICollection<DeviceGroupList> DeviceGroupListAddedByUser { get; set; }
        public virtual ICollection<DeviceGroupList> DeviceGroupListModifiedByUser { get; set; }
        public virtual ICollection<DisposalItem> DisposalItemAddedByUser { get; set; }
        public virtual ICollection<DisposalItem> DisposalItemModifiedByUser { get; set; }
        public virtual ICollection<DistStockBalance> DistStockBalanceAddedByUser { get; set; }
        public virtual ICollection<DistStockBalance> DistStockBalanceModifiedByUser { get; set; }
        public virtual ICollection<DistributorPricing> DistributorPricingAddedByUser { get; set; }
        public virtual ICollection<DistributorPricing> DistributorPricingModifiedByUser { get; set; }
        public virtual ICollection<DistributorReplenishment> DistributorReplenishmentAddedByUser { get; set; }
        public virtual ICollection<DistributorReplenishmentLine> DistributorReplenishmentLineAddedByUser { get; set; }
        public virtual ICollection<DistributorReplenishmentLine> DistributorReplenishmentLineModifiedByUser { get; set; }
        public virtual ICollection<DistributorReplenishment> DistributorReplenishmentModifiedByUser { get; set; }
        public virtual ICollection<Division> DivisionAddedByUser { get; set; }
        public virtual ICollection<Division> DivisionModifiedByUser { get; set; }
        public virtual ICollection<DocumentAlert> DocumentAlertAddedByUser { get; set; }
        public virtual ICollection<DocumentAlert> DocumentAlertModifiedByUser { get; set; }
        public virtual ICollection<DocumentDmsshare> DocumentDmsshareAddedByUser { get; set; }
        public virtual ICollection<DocumentDmsshare> DocumentDmsshareModifiedByUser { get; set; }
        public virtual ICollection<DocumentInvitation> DocumentInvitationAddedByUser { get; set; }
        public virtual ICollection<DocumentInvitation> DocumentInvitationModifiedByUser { get; set; }
        public virtual ICollection<DocumentInvitation> DocumentInvitationUser { get; set; }
        public virtual ICollection<DocumentLink> DocumentLinkAddedByUser { get; set; }
        public virtual ICollection<DocumentLink> DocumentLinkModifiedByUser { get; set; }
        public virtual ICollection<DocumentNoSeries> DocumentNoSeriesAddedByUser { get; set; }
        public virtual ICollection<DocumentNoSeries> DocumentNoSeriesModifiedByUser { get; set; }
        public virtual ICollection<DocumentNoSeries> DocumentNoSeriesRequestor { get; set; }
        public virtual ICollection<DocumentPermission> DocumentPermissionAddedByUser { get; set; }
        public virtual ICollection<DocumentPermission> DocumentPermissionModifiedByUser { get; set; }
        public virtual ICollection<DocumentProfile> DocumentProfileAddedByUser { get; set; }
        public virtual ICollection<DocumentProfile> DocumentProfileModifiedByUser { get; set; }
        public virtual ICollection<DocumentProfileNoSeries> DocumentProfileNoSeriesAddedByUser { get; set; }
        public virtual ICollection<DocumentProfileNoSeries> DocumentProfileNoSeriesModifiedByUser { get; set; }
        public virtual ICollection<DocumentRights> DocumentRights { get; set; }
        public virtual ICollection<DocumentRole> DocumentRoleAddedByUser { get; set; }
        public virtual ICollection<DocumentRole> DocumentRoleModifiedByUser { get; set; }
        public virtual ICollection<DocumentRolePermission> DocumentRolePermissionAddedByUser { get; set; }
        public virtual ICollection<DocumentRolePermission> DocumentRolePermissionModifiedByUser { get; set; }
        public virtual ICollection<DocumentShare> DocumentShareAddedByUser { get; set; }
        public virtual ICollection<DocumentShare> DocumentShareModifiedByUser { get; set; }
        public virtual ICollection<DocumentShareUser> DocumentShareUser { get; set; }
        public virtual ICollection<DocumentUserRole> DocumentUserRole { get; set; }
        public virtual ICollection<Documents> DocumentsAddedByUser { get; set; }
        public virtual ICollection<Documents> DocumentsDeleteByUser { get; set; }
        public virtual ICollection<Documents> DocumentsLockedByUser { get; set; }
        public virtual ICollection<Documents> DocumentsModifiedByUser { get; set; }
        public virtual ICollection<Documents> DocumentsRestoreByUser { get; set; }
        public virtual ICollection<DraftApplicationWiki> DraftApplicationWikiAddedByUser { get; set; }
        public virtual ICollection<DraftApplicationWikiLine> DraftApplicationWikiLineAddedByUser { get; set; }
        public virtual ICollection<DraftApplicationWikiLineDuty> DraftApplicationWikiLineDutyAddedByUser { get; set; }
        public virtual ICollection<DraftApplicationWikiLineDuty> DraftApplicationWikiLineDutyModifiedByUser { get; set; }
        public virtual ICollection<DraftApplicationWikiLine> DraftApplicationWikiLineModifiedByUser { get; set; }
        public virtual ICollection<DraftApplicationWikiLineNotify> DraftApplicationWikiLineNotify { get; set; }
        public virtual ICollection<DraftApplicationWiki> DraftApplicationWikiModifiedByUser { get; set; }
        public virtual ICollection<DraftApplicationWikiRecurrence> DraftApplicationWikiRecurrenceAddedByUser { get; set; }
        public virtual ICollection<DraftApplicationWikiRecurrence> DraftApplicationWikiRecurrenceModifiedByUser { get; set; }
        public virtual ICollection<DraftApplicationWiki> DraftApplicationWikiWikiEntryBy { get; set; }
        public virtual ICollection<DraftWikiResponsible> DraftWikiResponsible { get; set; }
        public virtual ICollection<DynamicFlow> DynamicFlowAddedByUser { get; set; }
        public virtual ICollection<DynamicFlow> DynamicFlowModifiedByUser { get; set; }
        public virtual ICollection<DynamicFlow> DynamicFlowOwnerNavigation { get; set; }
        public virtual ICollection<DynamicFlowProgress> DynamicFlowProgress { get; set; }
        public virtual ICollection<DynamicFlowStep> DynamicFlowStep { get; set; }
        public virtual ICollection<DynamicForm> DynamicFormAddedByUser { get; set; }
        public virtual ICollection<DynamicFormApproval> DynamicFormApprovalAddedByUser { get; set; }
        public virtual ICollection<DynamicFormApproval> DynamicFormApprovalApprovalUser { get; set; }
        public virtual ICollection<DynamicFormApproval> DynamicFormApprovalModifiedByUser { get; set; }
        public virtual ICollection<DynamicFormApproved> DynamicFormApprovedApprovedByUser { get; set; }
        public virtual ICollection<DynamicFormApprovedChanged> DynamicFormApprovedChanged { get; set; }
        public virtual ICollection<DynamicFormApproved> DynamicFormApprovedUser { get; set; }
        public virtual ICollection<DynamicFormData> DynamicFormDataAddedByUser { get; set; }
        public virtual ICollection<DynamicFormData> DynamicFormDataLockedUser { get; set; }
        public virtual ICollection<DynamicFormData> DynamicFormDataModifiedByUser { get; set; }
        public virtual ICollection<DynamicFormDataSectionLock> DynamicFormDataSectionLock { get; set; }
        public virtual ICollection<DynamicFormDataUpload> DynamicFormDataUploadAddedByUser { get; set; }
        public virtual ICollection<DynamicFormDataUpload> DynamicFormDataUploadModifiedByUser { get; set; }
        public virtual ICollection<DynamicFormItem> DynamicFormItemAddedByUser { get; set; }
        public virtual ICollection<DynamicFormItemLine> DynamicFormItemLineAddedByUser { get; set; }
        public virtual ICollection<DynamicFormItemLine> DynamicFormItemLineModifiedByUser { get; set; }
        public virtual ICollection<DynamicFormItem> DynamicFormItemModifiedByUser { get; set; }
        public virtual ICollection<DynamicForm> DynamicFormModifiedByUser { get; set; }
        public virtual ICollection<DynamicFormReport> DynamicFormReportAddedByUser { get; set; }
        public virtual ICollection<DynamicFormReport> DynamicFormReportModifiedByUser { get; set; }
        public virtual ICollection<DynamicFormSection> DynamicFormSectionAddedByUser { get; set; }
        public virtual ICollection<DynamicFormSectionAttribute> DynamicFormSectionAttributeAddedByUser { get; set; }
        public virtual ICollection<DynamicFormSectionAttribute> DynamicFormSectionAttributeModifiedByUser { get; set; }
        public virtual ICollection<DynamicFormSectionAttributeSecurity> DynamicFormSectionAttributeSecurity { get; set; }
        public virtual ICollection<DynamicFormSection> DynamicFormSectionModifiedByUser { get; set; }
        public virtual ICollection<DynamicFormSectionSecurity> DynamicFormSectionSecurity { get; set; }
        public virtual ICollection<DynamicFormSectionWorkFlow> DynamicFormSectionWorkFlow { get; set; }
        public virtual ICollection<DynamicFormWorkFlow> DynamicFormWorkFlow { get; set; }
        public virtual ICollection<DynamicFormWorkFlowApproval> DynamicFormWorkFlowApproval { get; set; }
        public virtual ICollection<DynamicFormWorkFlowApprovedForm> DynamicFormWorkFlowApprovedFormApprovedByUser { get; set; }
        public virtual ICollection<DynamicFormWorkFlowApprovedFormChanged> DynamicFormWorkFlowApprovedFormChanged { get; set; }
        public virtual ICollection<DynamicFormWorkFlowApprovedForm> DynamicFormWorkFlowApprovedFormUser { get; set; }
        public virtual ICollection<DynamicFormWorkFlowForm> DynamicFormWorkFlowForm { get; set; }
        public virtual ICollection<DynamicFormWorkFlowFormDelegate> DynamicFormWorkFlowFormDelegate { get; set; }
        public virtual ICollection<Emails> EmailsAddedByUser { get; set; }
        public virtual ICollection<Emails> EmailsModifiedByUser { get; set; }
        public virtual ICollection<Employee> EmployeeAddedByUser { get; set; }
        public virtual ICollection<EmployeeIcthardInformation> EmployeeIcthardInformationAddedByUser { get; set; }
        public virtual ICollection<EmployeeIcthardInformation> EmployeeIcthardInformationModifiedByUser { get; set; }
        public virtual ICollection<EmployeeIctinformation> EmployeeIctinformationAddedByUser { get; set; }
        public virtual ICollection<EmployeeIctinformation> EmployeeIctinformationModifiedByUser { get; set; }
        public virtual ICollection<EmployeeInformation> EmployeeInformationAddedByUser { get; set; }
        public virtual ICollection<EmployeeInformation> EmployeeInformationModifiedByUser { get; set; }
        public virtual ICollection<EmployeeLeaveInformation> EmployeeLeaveInformationAddedByUser { get; set; }
        public virtual ICollection<EmployeeLeaveInformation> EmployeeLeaveInformationModifiedByUser { get; set; }
        public virtual ICollection<Employee> EmployeeModifiedByUser { get; set; }
        public virtual ICollection<EmployeeOtherDutyInformation> EmployeeOtherDutyInformationAddedByUser { get; set; }
        public virtual ICollection<EmployeeOtherDutyInformation> EmployeeOtherDutyInformationModifiedByUser { get; set; }
        public virtual ICollection<Employee> EmployeeReport { get; set; }
        public virtual ICollection<EmployeeReportTo> EmployeeReportTo { get; set; }
        public virtual ICollection<EmployeeResignation> EmployeeResignation { get; set; }
        public virtual ICollection<EmployeeSageInformation> EmployeeSageInformationAddedByUser { get; set; }
        public virtual ICollection<EmployeeSageInformation> EmployeeSageInformationModifiedByUser { get; set; }
        public virtual ICollection<Employee> EmployeeUser { get; set; }
        public virtual ICollection<ExchangeRate> ExchangeRateAddedByUser { get; set; }
        public virtual ICollection<ExchangeRateLine> ExchangeRateLineAddedByUser { get; set; }
        public virtual ICollection<ExchangeRateLine> ExchangeRateLineModifiedByUser { get; set; }
        public virtual ICollection<ExchangeRate> ExchangeRateModifiedByUser { get; set; }
        public virtual ICollection<ExcipientInformationLineByProcess> ExcipientInformationLineByProcessAddedByUser { get; set; }
        public virtual ICollection<ExcipientInformationLineByProcess> ExcipientInformationLineByProcessModifiedByUser { get; set; }
        public virtual ICollection<ExtensionInformationLine> ExtensionInformationLineAddedByUser { get; set; }
        public virtual ICollection<ExtensionInformationLine> ExtensionInformationLineModifiedByUser { get; set; }
        public virtual ICollection<FileProfileSetupForm> FileProfileSetupFormAddedByUser { get; set; }
        public virtual ICollection<FileProfileSetupForm> FileProfileSetupFormModifiedByUser { get; set; }
        public virtual ICollection<FileProfileType> FileProfileTypeAddedByUser { get; set; }
        public virtual ICollection<FileProfileType> FileProfileTypeDeleteByUser { get; set; }
        public virtual ICollection<FileProfileTypeDynamicForm> FileProfileTypeDynamicFormAddedByUser { get; set; }
        public virtual ICollection<FileProfileTypeDynamicForm> FileProfileTypeDynamicFormModifiedByUser { get; set; }
        public virtual ICollection<FileProfileType> FileProfileTypeModifiedByUser { get; set; }
        public virtual ICollection<FileProfileType> FileProfileTypeRestoreByUser { get; set; }
        public virtual ICollection<FileProfileTypeSetAccess> FileProfileTypeSetAccessAddedByUser { get; set; }
        public virtual ICollection<FileProfileTypeSetAccess> FileProfileTypeSetAccessUser { get; set; }
        public virtual ICollection<FinishProduct> FinishProductAddedByUser { get; set; }
        public virtual ICollection<FinishProductExcipient> FinishProductExcipientAddedByUser { get; set; }
        public virtual ICollection<FinishProductExcipientLine> FinishProductExcipientLineAddedByUser { get; set; }
        public virtual ICollection<FinishProductExcipientLine> FinishProductExcipientLineModifiedByUser { get; set; }
        public virtual ICollection<FinishProductExcipient> FinishProductExcipientModifiedByUser { get; set; }
        public virtual ICollection<FinishProductGeneralInfo> FinishProductGeneralInfoAddedByUser { get; set; }
        public virtual ICollection<FinishProductGeneralInfoDocument> FinishProductGeneralInfoDocumentAddedByUser { get; set; }
        public virtual ICollection<FinishProductGeneralInfoDocument> FinishProductGeneralInfoDocumentModifiedByUser { get; set; }
        public virtual ICollection<FinishProductGeneralInfo> FinishProductGeneralInfoModifiedUser { get; set; }
        public virtual ICollection<FinishProduct> FinishProductModifiedByUser { get; set; }
        public virtual ICollection<FlowDistribution> FlowDistributionAddedByUser { get; set; }
        public virtual ICollection<FlowDistribution> FlowDistributionModifiedByUser { get; set; }
        public virtual ICollection<FlowDistribution> FlowDistributionPiasectionNavigation { get; set; }
        public virtual ICollection<FlowDistribution> FlowDistributionPicsectionNavigation { get; set; }
        public virtual ICollection<Fmglobal> FmglobalAddedByUser { get; set; }
        public virtual ICollection<FmglobalLine> FmglobalLineAddedByUser { get; set; }
        public virtual ICollection<FmglobalLineItem> FmglobalLineItemAddedByUser { get; set; }
        public virtual ICollection<FmglobalLineItem> FmglobalLineItemModifiedByUser { get; set; }
        public virtual ICollection<FmglobalLine> FmglobalLineModifiedByUser { get; set; }
        public virtual ICollection<Fmglobal> FmglobalModifiedByUser { get; set; }
        public virtual ICollection<FmglobalMove> FmglobalMoveAddedByUser { get; set; }
        public virtual ICollection<FmglobalMove> FmglobalMoveModifiedByUser { get; set; }
        public virtual ICollection<FolderDiscussionAttachment> FolderDiscussionAttachment { get; set; }
        public virtual ICollection<FolderDiscussion> FolderDiscussionDiscussedByNavigation { get; set; }
        public virtual ICollection<FolderDiscussion> FolderDiscussionEditedByNavigation { get; set; }
        public virtual ICollection<FolderDiscussionUser> FolderDiscussionUser { get; set; }
        public virtual ICollection<FolderStorage> FolderStorageAddedByUser { get; set; }
        public virtual ICollection<FolderStorage> FolderStorageModifiedByUser { get; set; }
        public virtual ICollection<FolderStorage> FolderStorageUser { get; set; }
        public virtual ICollection<FolderStorageUser> FolderStorageUserNavigation { get; set; }
        public virtual ICollection<Folders> FoldersAddedByUser { get; set; }
        public virtual ICollection<Folders> FoldersModifiedByUser { get; set; }
        public virtual ICollection<FormTableFields> FormTableFieldsAddedByUser { get; set; }
        public virtual ICollection<FormTableFields> FormTableFieldsModifiedByUser { get; set; }
        public virtual ICollection<FormTables> FormTablesAddedByUser { get; set; }
        public virtual ICollection<FormTables> FormTablesModifiedByUser { get; set; }
        public virtual ICollection<FpcommonField> FpcommonFieldAddedByUser { get; set; }
        public virtual ICollection<FpcommonFieldLine> FpcommonFieldLineAddedByUser { get; set; }
        public virtual ICollection<FpcommonFieldLine> FpcommonFieldLineModifiedByUser { get; set; }
        public virtual ICollection<FpcommonField> FpcommonFieldModifiedByUser { get; set; }
        public virtual ICollection<FpdrugClassification> FpdrugClassificationAddedByUser { get; set; }
        public virtual ICollection<FpdrugClassificationLine> FpdrugClassificationLineAddedByUser { get; set; }
        public virtual ICollection<FpdrugClassificationLine> FpdrugClassificationLineModifiedByUser { get; set; }
        public virtual ICollection<FpdrugClassification> FpdrugClassificationModifiedByUser { get; set; }
        public virtual ICollection<FpmanufacturingRecipe> FpmanufacturingRecipeAddedByUser { get; set; }
        public virtual ICollection<FpmanufacturingRecipe> FpmanufacturingRecipeModifiedUser { get; set; }
        public virtual ICollection<Fpproduct> FpproductAddedByUser { get; set; }
        public virtual ICollection<FpproductLine> FpproductLineAddedByUser { get; set; }
        public virtual ICollection<FpproductLine> FpproductLineModifiedByUser { get; set; }
        public virtual ICollection<Fpproduct> FpproductModifiedByUser { get; set; }
        public virtual ICollection<FpproductNavisionLine> FpproductNavisionLineAddedByUser { get; set; }
        public virtual ICollection<FpproductNavisionLine> FpproductNavisionLineModifiedByUser { get; set; }
        public virtual ICollection<GeneralEquivalaentNavision> GeneralEquivalaentNavisionAddedByUser { get; set; }
        public virtual ICollection<GeneralEquivalaentNavision> GeneralEquivalaentNavisionModifiedByUser { get; set; }
        public virtual ICollection<GenericCodes> GenericCodesAddedByUser { get; set; }
        public virtual ICollection<GenericCodes> GenericCodesModifiedByUser { get; set; }
        public virtual ICollection<GenericItemNameListing> GenericItemNameListingAddedByUser { get; set; }
        public virtual ICollection<GenericItemNameListing> GenericItemNameListingModifiedByUser { get; set; }
        public virtual ICollection<HandlingOfShipmentClassification> HandlingOfShipmentClassificationAddedByUser { get; set; }
        public virtual ICollection<HandlingOfShipmentClassification> HandlingOfShipmentClassificationModifiedByUser { get; set; }
        public virtual ICollection<HistoricalDetails> HistoricalDetailsAddedByUser { get; set; }
        public virtual ICollection<HistoricalDetails> HistoricalDetailsModifiedByUser { get; set; }
        public virtual ICollection<HolidayMaster> HolidayMasterAddedByUser { get; set; }
        public virtual ICollection<HolidayMaster> HolidayMasterModifiedByUser { get; set; }
        public virtual ICollection<HrexternalPersonal> HrexternalPersonalAddedByUser { get; set; }
        public virtual ICollection<HrexternalPersonal> HrexternalPersonalModifiedByUser { get; set; }
        public virtual ICollection<HumanMovementAction> HumanMovementActionAddedByUser { get; set; }
        public virtual ICollection<HumanMovementAction> HumanMovementActionModifiedByUser { get; set; }
        public virtual ICollection<Icbmp> IcbmpAddedByUser { get; set; }
        public virtual ICollection<Icbmp> IcbmpModifiedByUser { get; set; }
        public virtual ICollection<Icbmpline> IcbmplineAddedByUser { get; set; }
        public virtual ICollection<Icbmpline> IcbmplineModifiedByUser { get; set; }
        public virtual ICollection<IcmasterOperation> IcmasterOperationAddedByUser { get; set; }
        public virtual ICollection<IcmasterOperation> IcmasterOperationModifiedByUser { get; set; }
        public virtual ICollection<Ictcertificate> IctcertificateAddedByUser { get; set; }
        public virtual ICollection<Ictcertificate> IctcertificateModifiedByUser { get; set; }
        public virtual ICollection<IctcontactDetails> IctcontactDetailsAddedByUser { get; set; }
        public virtual ICollection<IctcontactDetails> IctcontactDetailsModifiedByUser { get; set; }
        public virtual ICollection<IctlayoutPlanTypes> IctlayoutPlanTypesAddedByUser { get; set; }
        public virtual ICollection<IctlayoutPlanTypes> IctlayoutPlanTypesModifiedByUser { get; set; }
        public virtual ICollection<Ictmaster> IctmasterAddedByUser { get; set; }
        public virtual ICollection<IctmasterDocument> IctmasterDocumentAddedByUser { get; set; }
        public virtual ICollection<IctmasterDocument> IctmasterDocumentModifiedByUser { get; set; }
        public virtual ICollection<Ictmaster> IctmasterModifiedByUser { get; set; }
        public virtual ICollection<InstructionType> InstructionTypeAddedByUser { get; set; }
        public virtual ICollection<InstructionType> InstructionTypeModifiedByUser { get; set; }
        public virtual ICollection<InterCompanyPurchaseOrder> InterCompanyPurchaseOrderAddedByUser { get; set; }
        public virtual ICollection<InterCompanyPurchaseOrderLine> InterCompanyPurchaseOrderLineAddedByUser { get; set; }
        public virtual ICollection<InterCompanyPurchaseOrderLine> InterCompanyPurchaseOrderLineModifiedByUser { get; set; }
        public virtual ICollection<InterCompanyPurchaseOrder> InterCompanyPurchaseOrderLogin { get; set; }
        public virtual ICollection<InterCompanyPurchaseOrder> InterCompanyPurchaseOrderModifiedByUser { get; set; }
        public virtual ICollection<InventoryType> InventoryTypeAddedByUser { get; set; }
        public virtual ICollection<InventoryType> InventoryTypeModifiedByUser { get; set; }
        public virtual ICollection<InventoryTypeOpeningStockBalance> InventoryTypeOpeningStockBalanceAddedByUser { get; set; }
        public virtual ICollection<InventoryTypeOpeningStockBalanceLine> InventoryTypeOpeningStockBalanceLineAddedByUser { get; set; }
        public virtual ICollection<InventoryTypeOpeningStockBalanceLine> InventoryTypeOpeningStockBalanceLineModifiedByUser { get; set; }
        public virtual ICollection<InventoryTypeOpeningStockBalance> InventoryTypeOpeningStockBalanceModifiedByUser { get; set; }
        public virtual ICollection<ApplicationUser> InverseAddedByUser { get; set; }
        public virtual ICollection<ApplicationUser> InverseModifiedByUser { get; set; }
        public virtual ICollection<Ipir> IpirAddedByUser { get; set; }
        public virtual ICollection<IpirApp> IpirAppAddedByUser { get; set; }
        public virtual ICollection<IpirAppCheckedDetails> IpirAppCheckedDetailsAddedByUser { get; set; }
        public virtual ICollection<IpirAppCheckedDetails> IpirAppCheckedDetailsCheckedBy { get; set; }
        public virtual ICollection<IpirAppCheckedDetails> IpirAppCheckedDetailsModifiedByUser { get; set; }
        public virtual ICollection<IpirApp> IpirAppDetectedByNavigation { get; set; }
        public virtual ICollection<IpirApp> IpirAppModifiedByUser { get; set; }
        public virtual ICollection<IpirApp> IpirAppReportingPersonalNavigation { get; set; }
        public virtual ICollection<Ipir> IpirModifiedByUser { get; set; }
        public virtual ICollection<IpirReport> IpirReportAddedByUser { get; set; }
        public virtual ICollection<IpirReportAssignment> IpirReportAssignmentAddedByUser { get; set; }
        public virtual ICollection<IpirReportAssignment> IpirReportAssignmentModifiedByUser { get; set; }
        public virtual ICollection<IpirReportAssignmentUser> IpirReportAssignmentUser { get; set; }
        public virtual ICollection<IpirReport> IpirReportModifiedByUser { get; set; }
        public virtual ICollection<Ipirline> IpirlineAddedByUser { get; set; }
        public virtual ICollection<IpirlineDocument> IpirlineDocumentAddedByUser { get; set; }
        public virtual ICollection<IpirlineDocument> IpirlineDocumentModifiedByUser { get; set; }
        public virtual ICollection<Ipirline> IpirlineModifiedByUser { get; set; }
        public virtual ICollection<Ipirline> IpirlinePic { get; set; }
        public virtual ICollection<IpirmobileAction> IpirmobileActionAddedByUser { get; set; }
        public virtual ICollection<IpirmobileAction> IpirmobileActionAssignmentToNavigation { get; set; }
        public virtual ICollection<IpirmobileAction> IpirmobileActionModifiedByUser { get; set; }
        public virtual ICollection<Ipirmobile> IpirmobileAddedByUser { get; set; }
        public virtual ICollection<Ipirmobile> IpirmobileModifiedByUser { get; set; }
        public virtual ICollection<IssueReportIpir> IssueReportIpirAddedByUser { get; set; }
        public virtual ICollection<IssueReportIpir> IssueReportIpirModifiedByUser { get; set; }
        public virtual ICollection<IssueRequestSampleLine> IssueRequestSampleLineAddedByUser { get; set; }
        public virtual ICollection<IssueRequestSampleLine> IssueRequestSampleLineModifiedByUser { get; set; }
        public virtual ICollection<ItemBatchInfo> ItemBatchInfoAddedByUser { get; set; }
        public virtual ICollection<ItemBatchInfo> ItemBatchInfoModifiedByUser { get; set; }
        public virtual ICollection<ItemClassificationAccess> ItemClassificationAccess { get; set; }
        public virtual ICollection<ItemClassificationHeader> ItemClassificationHeaderAddedByUser { get; set; }
        public virtual ICollection<ItemClassificationHeader> ItemClassificationHeaderModifiedByUser { get; set; }
        public virtual ICollection<ItemClassificationMaster> ItemClassificationMasterAddedByUser { get; set; }
        public virtual ICollection<ItemClassificationMaster> ItemClassificationMasterModifiedByUser { get; set; }
        public virtual ICollection<ItemClassificationPermission> ItemClassificationPermission { get; set; }
        public virtual ICollection<ItemClassificationTransport> ItemClassificationTransportAddedByUser { get; set; }
        public virtual ICollection<ItemClassificationTransport> ItemClassificationTransportModifiedByUser { get; set; }
        public virtual ICollection<ItemCost> ItemCostAddedByUser { get; set; }
        public virtual ICollection<ItemCostLine> ItemCostLineAddedByUser { get; set; }
        public virtual ICollection<ItemCostLine> ItemCostLineModifiedByUser { get; set; }
        public virtual ICollection<ItemCost> ItemCostModifiedByUser { get; set; }
        public virtual ICollection<ItemManufacture> ItemManufactureAddedByUser { get; set; }
        public virtual ICollection<ItemManufacture> ItemManufactureModifiedByUser { get; set; }
        public virtual ICollection<ItemMaster> ItemMasterAddedByUser { get; set; }
        public virtual ICollection<ItemMaster> ItemMasterModifiedByUser { get; set; }
        public virtual ICollection<ItemSalesPrice> ItemSalesPriceAddedByUser { get; set; }
        public virtual ICollection<ItemSalesPrice> ItemSalesPriceModifiedByUser { get; set; }
        public virtual ICollection<ItemStockInfo> ItemStockInfoAddedByUser { get; set; }
        public virtual ICollection<ItemStockInfo> ItemStockInfoModifiedByUser { get; set; }
        public virtual ICollection<JobProgressTemplate> JobProgressTemplateAddedByUser { get; set; }
        public virtual ICollection<JobProgressTemplateLineLineProcess> JobProgressTemplateLineLineProcessAddedByUser { get; set; }
        public virtual ICollection<JobProgressTemplateLineLineProcess> JobProgressTemplateLineLineProcessModifiedByUser { get; set; }
        public virtual ICollection<JobProgressTemplateLineProcess> JobProgressTemplateLineProcessAddedByUser { get; set; }
        public virtual ICollection<JobProgressTemplateLineProcess> JobProgressTemplateLineProcessAssignedToNavigation { get; set; }
        public virtual ICollection<JobProgressTemplateLineProcess> JobProgressTemplateLineProcessModifiedByUser { get; set; }
        public virtual ICollection<JobProgressTemplateLineProcessPia> JobProgressTemplateLineProcessPia { get; set; }
        public virtual ICollection<JobProgressTemplateLineProcess> JobProgressTemplateLineProcessPiaNavigation { get; set; }
        public virtual ICollection<JobProgressTemplateLineProcess> JobProgressTemplateLineProcessPicNavigation { get; set; }
        public virtual ICollection<JobProgressTemplate> JobProgressTemplateModifiedByUser { get; set; }
        public virtual ICollection<JobProgressTemplate> JobProgressTemplatePicNavigation { get; set; }
        public virtual ICollection<JobProgressTemplateRecurrence> JobProgressTemplateRecurrenceAddedByUser { get; set; }
        public virtual ICollection<JobProgressTemplateRecurrence> JobProgressTemplateRecurrenceModifiedByUser { get; set; }
        public virtual ICollection<JobProgressTempletateLine> JobProgressTempletateLineAddedByUser { get; set; }
        public virtual ICollection<JobProgressTempletateLine> JobProgressTempletateLineAssignedToNavigation { get; set; }
        public virtual ICollection<JobProgressTempletateLine> JobProgressTempletateLineModifiedByUser { get; set; }
        public virtual ICollection<JobProgressTempletateLine> JobProgressTempletateLinePiaNavigation { get; set; }
        public virtual ICollection<JobSchedule> JobScheduleAddedByUser { get; set; }
        public virtual ICollection<JobSchedule> JobScheduleModifiedByUser { get; set; }
        public virtual ICollection<LayoutPlanType> LayoutPlanTypeAddedByUser { get; set; }
        public virtual ICollection<LayoutPlanType> LayoutPlanTypeModifiedByUser { get; set; }
        public virtual ICollection<LevelMaster> LevelMasterAddedByUser { get; set; }
        public virtual ICollection<LevelMaster> LevelMasterModifiedByUser { get; set; }
        public virtual ICollection<LinkFileProfileTypeDocument> LinkFileProfileTypeDocument { get; set; }
        public virtual ICollection<LocalClinic> LocalClinicAddedByUser { get; set; }
        public virtual ICollection<LocalClinic> LocalClinicModifiedByUser { get; set; }
        public virtual ICollection<Locations> LocationsAddedByUser { get; set; }
        public virtual ICollection<Locations> LocationsModifiedByUser { get; set; }
        public virtual ICollection<LoginSession> LoginSession { get; set; }
        public virtual ICollection<MachineTimeManHourInfo> MachineTimeManHourInfoAddedByUser { get; set; }
        public virtual ICollection<MachineTimeManHourInfo> MachineTimeManHourInfoModifiedByUser { get; set; }
        public virtual ICollection<ManpowerInformation> ManpowerInformationAddedByUser { get; set; }
        public virtual ICollection<ManpowerInformationLine> ManpowerInformationLineAddedByUser { get; set; }
        public virtual ICollection<ManpowerInformationLine> ManpowerInformationLineModifiedByUser { get; set; }
        public virtual ICollection<ManpowerInformation> ManpowerInformationModifiedByUser { get; set; }
        public virtual ICollection<ManufacturingProcess> ManufacturingProcessAddedByUser { get; set; }
        public virtual ICollection<ManufacturingProcessLine> ManufacturingProcessLineAddedByUser { get; set; }
        public virtual ICollection<ManufacturingProcessLine> ManufacturingProcessLineModifiedByUser { get; set; }
        public virtual ICollection<ManufacturingProcess> ManufacturingProcessModifiedByUser { get; set; }
        public virtual ICollection<MarginInformation> MarginInformationAddedByUser { get; set; }
        public virtual ICollection<MarginInformationLine> MarginInformationLineAddedByUser { get; set; }
        public virtual ICollection<MarginInformationLine> MarginInformationLineModifiedByUser { get; set; }
        public virtual ICollection<MarginInformation> MarginInformationModifiedByUser { get; set; }
        public virtual ICollection<MasterBlanketOrder> MasterBlanketOrderAddedByUser { get; set; }
        public virtual ICollection<MasterBlanketOrderLine> MasterBlanketOrderLineAddedByUser { get; set; }
        public virtual ICollection<MasterBlanketOrderLine> MasterBlanketOrderLineModifiedByUser { get; set; }
        public virtual ICollection<MasterBlanketOrder> MasterBlanketOrderModifiedByUser { get; set; }
        public virtual ICollection<MasterDocumentInformation> MasterDocumentInformationAddedByUser { get; set; }
        public virtual ICollection<MasterDocumentInformation> MasterDocumentInformationModifiedByUser { get; set; }
        public virtual ICollection<MasterForm> MasterFormAddedByUser { get; set; }
        public virtual ICollection<MasterFormDetail> MasterFormDetailAddedByUser { get; set; }
        public virtual ICollection<MasterFormDetail> MasterFormDetailModifiedByUser { get; set; }
        public virtual ICollection<MasterFormDetail> MasterFormDetailUser { get; set; }
        public virtual ICollection<MasterForm> MasterFormModifiedByUser { get; set; }
        public virtual ICollection<MasterInterCompanyPricing> MasterInterCompanyPricingAddedByUser { get; set; }
        public virtual ICollection<MasterInterCompanyPricingLine> MasterInterCompanyPricingLineAddedByUser { get; set; }
        public virtual ICollection<MasterInterCompanyPricingLine> MasterInterCompanyPricingLineModifiedByUser { get; set; }
        public virtual ICollection<MasterInterCompanyPricing> MasterInterCompanyPricingModifiedByUser { get; set; }
        public virtual ICollection<MedCatalogClassification> MedCatalogClassificationAddedByUser { get; set; }
        public virtual ICollection<MedCatalogClassification> MedCatalogClassificationModifiedByUser { get; set; }
        public virtual ICollection<MedMaster> MedMasterAddedByUser { get; set; }
        public virtual ICollection<MedMasterLine> MedMasterLineAddedByUser { get; set; }
        public virtual ICollection<MedMasterLine> MedMasterLineModifiedByUser { get; set; }
        public virtual ICollection<MedMaster> MedMasterModifiedByUser { get; set; }
        public virtual ICollection<Memo> MemoAddedByUser { get; set; }
        public virtual ICollection<Memo> MemoModifiedByUser { get; set; }
        public virtual ICollection<MemoUser> MemoUser { get; set; }
        public virtual ICollection<MethodTemplateRoutine> MethodTemplateRoutineAddedByUser { get; set; }
        public virtual ICollection<MethodTemplateRoutineLine> MethodTemplateRoutineLineAddedByUser { get; set; }
        public virtual ICollection<MethodTemplateRoutineLine> MethodTemplateRoutineLineModifiedByUser { get; set; }
        public virtual ICollection<MethodTemplateRoutine> MethodTemplateRoutineModifiedByUser { get; set; }
        public virtual ICollection<MobileShift> MobileShiftAddedByUser { get; set; }
        public virtual ICollection<MobileShift> MobileShiftModifiedByUser { get; set; }
        public virtual ICollection<Modules> ModulesAddedByUser { get; set; }
        public virtual ICollection<Modules> ModulesModifiedByUser { get; set; }
        public virtual ICollection<NavItemBatchNo> NavItemBatchNoAddedByUser { get; set; }
        public virtual ICollection<NavItemBatchNo> NavItemBatchNoModifiedByUser { get; set; }
        public virtual ICollection<NavManufacturingProcess> NavManufacturingProcessAddedByUser { get; set; }
        public virtual ICollection<NavManufacturingProcess> NavManufacturingProcessModifiedByUser { get; set; }
        public virtual ICollection<NavMethodCode> NavMethodCodeAddedByUser { get; set; }
        public virtual ICollection<NavMethodCodeLines> NavMethodCodeLinesAddedByUser { get; set; }
        public virtual ICollection<NavMethodCodeLines> NavMethodCodeLinesModifiedByUser { get; set; }
        public virtual ICollection<NavMethodCode> NavMethodCodeModifiedByUser { get; set; }
        public virtual ICollection<NavPackingMethod> NavPackingMethodAddedByUser { get; set; }
        public virtual ICollection<NavPackingMethod> NavPackingMethodModifiedByUser { get; set; }
        public virtual ICollection<NavProductMaster> NavProductMasterAddedByUser { get; set; }
        public virtual ICollection<NavProductMaster> NavProductMasterModifiedByUser { get; set; }
        public virtual ICollection<NavSaleCategory> NavSaleCategoryAddedByUser { get; set; }
        public virtual ICollection<NavSaleCategory> NavSaleCategoryModifiedByUser { get; set; }
        public virtual ICollection<NavbaseUnit> NavbaseUnitAddedByUser { get; set; }
        public virtual ICollection<NavbaseUnit> NavbaseUnitModifiedByUser { get; set; }
        public virtual ICollection<Navcustomer> Navcustomer { get; set; }
        public virtual ICollection<NavisionCompany> NavisionCompanyAddedByUser { get; set; }
        public virtual ICollection<NavisionCompany> NavisionCompanyModifiedByUser { get; set; }
        public virtual ICollection<NavisionSpecification> NavisionSpecificationAddedByUser { get; set; }
        public virtual ICollection<NavisionSpecification> NavisionSpecificationModifiedByUser { get; set; }
        public virtual ICollection<NavitemLinks> NavitemLinksAddedByUser { get; set; }
        public virtual ICollection<NavitemLinks> NavitemLinksModifiedByUser { get; set; }
        public virtual ICollection<NavitemStockBalance> NavitemStockBalanceAddedByUser { get; set; }
        public virtual ICollection<NavitemStockBalance> NavitemStockBalanceModifiedByUser { get; set; }
        public virtual ICollection<Navitems> NavitemsAddedByUser { get; set; }
        public virtual ICollection<Navitems> NavitemsLastSyncByNavigation { get; set; }
        public virtual ICollection<Navitems> NavitemsModifiedByUser { get; set; }
        public virtual ICollection<Navlocation> NavlocationAddedByUser { get; set; }
        public virtual ICollection<Navlocation> NavlocationModifiedByUser { get; set; }
        public virtual ICollection<NavplannedProdOrder> NavplannedProdOrderAddedByUser { get; set; }
        public virtual ICollection<NavplannedProdOrder> NavplannedProdOrderModifiedByUser { get; set; }
        public virtual ICollection<NavpostedShipment> NavpostedShipmentAddedByUser { get; set; }
        public virtual ICollection<NavpostedShipment> NavpostedShipmentModifiedByUser { get; set; }
        public virtual ICollection<NavproductCode> NavproductCodeAddedByUser { get; set; }
        public virtual ICollection<NavproductCode> NavproductCodeModifiedByUser { get; set; }
        public virtual ICollection<Navrecipes> NavrecipesAddedByUser { get; set; }
        public virtual ICollection<Navrecipes> NavrecipesModifiedByUser { get; set; }
        public virtual ICollection<Navsettings> NavsettingsAddedByUser { get; set; }
        public virtual ICollection<Navsettings> NavsettingsModifiedByUser { get; set; }
        public virtual ICollection<NavstockBalanace> NavstockBalanaceAddedByUser { get; set; }
        public virtual ICollection<NavstockBalanace> NavstockBalanaceModifiedByUser { get; set; }
        public virtual ICollection<Navvendor> NavvendorAddedByUser { get; set; }
        public virtual ICollection<Navvendor> NavvendorModifiedByUser { get; set; }
        public virtual ICollection<NonDeliverSo> NonDeliverSoAddedByUser { get; set; }
        public virtual ICollection<NonDeliverSo> NonDeliverSoModifiedByUser { get; set; }
        public virtual ICollection<Notes> NotesAddedByUser { get; set; }
        public virtual ICollection<Notes> NotesModifiedByUser { get; set; }
        public virtual ICollection<Notice> NoticeAddedByUser { get; set; }
        public virtual ICollection<NoticeLine> NoticeLineAddedByUser { get; set; }
        public virtual ICollection<NoticeLine> NoticeLineModifiedByUser { get; set; }
        public virtual ICollection<Notice> NoticeModifiedByUser { get; set; }
        public virtual ICollection<NoticeUser> NoticeUser { get; set; }
        public virtual ICollection<Notification> Notification { get; set; }
        public virtual ICollection<NotificationHandler> NotificationHandler { get; set; }
        public virtual ICollection<NotifyDocument> NotifyDocumentAddedByUser { get; set; }
        public virtual ICollection<NotifyDocument> NotifyDocumentModifiedByUser { get; set; }
        public virtual ICollection<NotifyDocumentUserGroup> NotifyDocumentUserGroup { get; set; }
        public virtual ICollection<NotifyUserGroupUser> NotifyUserGroupUser { get; set; }
        public virtual ICollection<NovateInformationLine> NovateInformationLineAddedByUser { get; set; }
        public virtual ICollection<NovateInformationLine> NovateInformationLineModifiedByUser { get; set; }
        public virtual ICollection<NpraformulationActiveIngredient> NpraformulationActiveIngredientAddedByUser { get; set; }
        public virtual ICollection<NpraformulationActiveIngredient> NpraformulationActiveIngredientModifiedByUser { get; set; }
        public virtual ICollection<Npraformulation> NpraformulationAddedByUser { get; set; }
        public virtual ICollection<NpraformulationExcipient> NpraformulationExcipientAddedByUser { get; set; }
        public virtual ICollection<NpraformulationExcipient> NpraformulationExcipientModifiedByUser { get; set; }
        public virtual ICollection<Npraformulation> NpraformulationModifiedByUser { get; set; }
        public virtual ICollection<OpenAccessUser> OpenAccessUserAddedByUser { get; set; }
        public virtual ICollection<OpenAccessUserLink> OpenAccessUserLink { get; set; }
        public virtual ICollection<OpenAccessUser> OpenAccessUserModifiedByUser { get; set; }
        public virtual ICollection<OperationProcedure> OperationProcedureAddedByUser { get; set; }
        public virtual ICollection<OperationProcedure> OperationProcedureModifiedByUser { get; set; }
        public virtual ICollection<OperationSequence> OperationSequenceAddedByUser { get; set; }
        public virtual ICollection<OperationSequence> OperationSequenceModifiedByUser { get; set; }
        public virtual ICollection<OrderRequirement> OrderRequirementAddedByUser { get; set; }
        public virtual ICollection<OrderRequirementGrouPinglineSplit> OrderRequirementGrouPinglineSplitAddedByUser { get; set; }
        public virtual ICollection<OrderRequirementGrouPinglineSplit> OrderRequirementGrouPinglineSplitModifiedByUser { get; set; }
        public virtual ICollection<OrderRequirementGroupingLine> OrderRequirementGroupingLineAddedByUser { get; set; }
        public virtual ICollection<OrderRequirementGroupingLine> OrderRequirementGroupingLineModifiedByUser { get; set; }
        public virtual ICollection<OrderRequirementLine> OrderRequirementLineAddedByUser { get; set; }
        public virtual ICollection<OrderRequirementLine> OrderRequirementLineModifiedByUser { get; set; }
        public virtual ICollection<OrderRequirementLineSplit> OrderRequirementLineSplitAddedByUser { get; set; }
        public virtual ICollection<OrderRequirementLineSplit> OrderRequirementLineSplitModifiedByUser { get; set; }
        public virtual ICollection<OrderRequirement> OrderRequirementModifiedByUser { get; set; }
        public virtual ICollection<PackagingAluminiumFoil> PackagingAluminiumFoilAddedByUser { get; set; }
        public virtual ICollection<PackagingAluminiumFoilLine> PackagingAluminiumFoilLineAddedByUser { get; set; }
        public virtual ICollection<PackagingAluminiumFoilLine> PackagingAluminiumFoilLineModifiedByUser { get; set; }
        public virtual ICollection<PackagingAluminiumFoil> PackagingAluminiumFoilModifiedByUser { get; set; }
        public virtual ICollection<PackagingHistoryItemLineDocument> PackagingHistoryItemLineDocumentAddedByUser { get; set; }
        public virtual ICollection<PackagingHistoryItemLineDocument> PackagingHistoryItemLineDocumentModifiedByUser { get; set; }
        public virtual ICollection<PackagingItemHistory> PackagingItemHistoryAddedByUser { get; set; }
        public virtual ICollection<PackagingItemHistoryLine> PackagingItemHistoryLineAddedByUser { get; set; }
        public virtual ICollection<PackagingItemHistoryLine> PackagingItemHistoryLineModifiedByUser { get; set; }
        public virtual ICollection<PackagingItemHistory> PackagingItemHistoryModifiedByUser { get; set; }
        public virtual ICollection<PackagingLabel> PackagingLabelAddedByUser { get; set; }
        public virtual ICollection<PackagingLabel> PackagingLabelModifiedByUser { get; set; }
        public virtual ICollection<PackagingLeaflet> PackagingLeafletAddedByUser { get; set; }
        public virtual ICollection<PackagingLeaflet> PackagingLeafletModifiedByUser { get; set; }
        public virtual ICollection<PackagingPvcfoil> PackagingPvcfoilAddedByUser { get; set; }
        public virtual ICollection<PackagingPvcfoil> PackagingPvcfoilModifiedByUser { get; set; }
        public virtual ICollection<PackagingTube> PackagingTubeAddedByUser { get; set; }
        public virtual ICollection<PackagingTube> PackagingTubeModifiedByUser { get; set; }
        public virtual ICollection<PackagingUnitBox> PackagingUnitBoxAddedByUser { get; set; }
        public virtual ICollection<PackagingUnitBox> PackagingUnitBoxModifiedByUser { get; set; }
        public virtual ICollection<PacketInformationLine> PacketInformationLineAddedByUser { get; set; }
        public virtual ICollection<PacketInformationLine> PacketInformationLineModifiedByUser { get; set; }
        public virtual ICollection<PageApproval> PageApproval { get; set; }
        public virtual ICollection<PageComment> PageComment { get; set; }
        public virtual ICollection<PageLikes> PageLikes { get; set; }
        public virtual ICollection<PageLinkCondition> PageLinkConditionAddedByUser { get; set; }
        public virtual ICollection<PageLinkCondition> PageLinkConditionModifiedByUser { get; set; }
        public virtual ICollection<PageVersion> PageVersion { get; set; }
        public virtual ICollection<PerUnitFormulationLine> PerUnitFormulationLineAddedByUser { get; set; }
        public virtual ICollection<PerUnitFormulationLine> PerUnitFormulationLineModifiedByUser { get; set; }
        public virtual ICollection<PhramacologicalProperties> PhramacologicalPropertiesAddedByUser { get; set; }
        public virtual ICollection<PhramacologicalProperties> PhramacologicalPropertiesModifiedByUser { get; set; }
        public virtual ICollection<PkgapprovalInformation> PkgapprovalInformationAddedByUser { get; set; }
        public virtual ICollection<PkgapprovalInformation> PkgapprovalInformationModifiedByUser { get; set; }
        public virtual ICollection<PkginformationByPackSize> PkginformationByPackSizeAddedByUser { get; set; }
        public virtual ICollection<PkginformationByPackSize> PkginformationByPackSizeModifiedByUser { get; set; }
        public virtual ICollection<PkgregisteredPackingInformation> PkgregisteredPackingInformationAddedByUser { get; set; }
        public virtual ICollection<PkgregisteredPackingInformation> PkgregisteredPackingInformationModifiedByUser { get; set; }
        public virtual ICollection<Plant> PlantAddedByUser { get; set; }
        public virtual ICollection<PlantMaintenanceEntry> PlantMaintenanceEntryAddedByUser { get; set; }
        public virtual ICollection<PlantMaintenanceEntryMaster> PlantMaintenanceEntryMasterAddedByUser { get; set; }
        public virtual ICollection<PlantMaintenanceEntryMaster> PlantMaintenanceEntryMasterModifiedByUser { get; set; }
        public virtual ICollection<PlantMaintenanceEntry> PlantMaintenanceEntryModifiedByUser { get; set; }
        public virtual ICollection<Plant> PlantModifiedByUser { get; set; }
        public virtual ICollection<PlasticBag> PlasticBagAddedByUser { get; set; }
        public virtual ICollection<PlasticBag> PlasticBagModifiedByUser { get; set; }
        public virtual ICollection<Portfolio> PortfolioAddedByUser { get; set; }
        public virtual ICollection<PortfolioAttachment> PortfolioAttachmentLockedByUser { get; set; }
        public virtual ICollection<PortfolioAttachment> PortfolioAttachmentUploadedByUser { get; set; }
        public virtual ICollection<PortfolioLine> PortfolioLineAddedByUser { get; set; }
        public virtual ICollection<PortfolioLine> PortfolioLineModifiedByUser { get; set; }
        public virtual ICollection<Portfolio> PortfolioModifiedByUser { get; set; }
        public virtual ICollection<Portfolio> PortfolioOnBehalf { get; set; }
        public virtual ICollection<PortfolioOnBehalf> PortfolioOnBehalfNavigation { get; set; }
        public virtual ICollection<PrintInfo> PrintInfo { get; set; }
        public virtual ICollection<ProcessMachineTime> ProcessMachineTimeAddedByUser { get; set; }
        public virtual ICollection<ProcessMachineTime> ProcessMachineTimeModifiedByUser { get; set; }
        public virtual ICollection<ProcessMachineTimeProductionLine> ProcessMachineTimeProductionLineAddedByUser { get; set; }
        public virtual ICollection<ProcessMachineTimeProductionLineLine> ProcessMachineTimeProductionLineLineAddedByUser { get; set; }
        public virtual ICollection<ProcessMachineTimeProductionLineLine> ProcessMachineTimeProductionLineLineModifiedByUser { get; set; }
        public virtual ICollection<ProcessMachineTimeProductionLine> ProcessMachineTimeProductionLineModifiedByUser { get; set; }
        public virtual ICollection<ProcessMachineTimeProductionMachineProcess> ProcessMachineTimeProductionMachineProcessAddedByUser { get; set; }
        public virtual ICollection<ProcessMachineTimeProductionMachineProcess> ProcessMachineTimeProductionMachineProcessModifiedByUser { get; set; }
        public virtual ICollection<ProcessManPowerSpecialNotes> ProcessManPowerSpecialNotesAddedByUser { get; set; }
        public virtual ICollection<ProcessManPowerSpecialNotes> ProcessManPowerSpecialNotesModifiedByUser { get; set; }
        public virtual ICollection<ProcessTransfer> ProcessTransferAddedByUser { get; set; }
        public virtual ICollection<ProcessTransferLine> ProcessTransferLineAddedByUser { get; set; }
        public virtual ICollection<ProcessTransferLine> ProcessTransferLineModifiedByUser { get; set; }
        public virtual ICollection<ProcessTransfer> ProcessTransferModifiedByUser { get; set; }
        public virtual ICollection<ProductActivityCase> ProductActivityCaseAddedByUser { get; set; }
        public virtual ICollection<ProductActivityCaseLine> ProductActivityCaseLineAddedByUser { get; set; }
        public virtual ICollection<ProductActivityCaseLine> ProductActivityCaseLineModifiedByUser { get; set; }
        public virtual ICollection<ProductActivityCase> ProductActivityCaseModifiedByUser { get; set; }
        public virtual ICollection<ProductActivityCaseRespons> ProductActivityCaseResponsAddedByUser { get; set; }
        public virtual ICollection<ProductActivityCaseResponsDuty> ProductActivityCaseResponsDutyAddedByUser { get; set; }
        public virtual ICollection<ProductActivityCaseResponsDuty> ProductActivityCaseResponsDutyModifiedByUser { get; set; }
        public virtual ICollection<ProductActivityCaseRespons> ProductActivityCaseResponsModifiedByUser { get; set; }
        public virtual ICollection<ProductActivityCaseResponsRecurrence> ProductActivityCaseResponsRecurrenceAddedByUser { get; set; }
        public virtual ICollection<ProductActivityCaseResponsRecurrence> ProductActivityCaseResponsRecurrenceModifiedByUser { get; set; }
        public virtual ICollection<ProductActivityCaseResponsResponsible> ProductActivityCaseResponsResponsible { get; set; }
        public virtual ICollection<ProductActivityPermission> ProductActivityPermissionAddedByUser { get; set; }
        public virtual ICollection<ProductActivityPermission> ProductActivityPermissionModifiedByUser { get; set; }
        public virtual ICollection<ProductActivityPermission> ProductActivityPermissionUser { get; set; }
        public virtual ICollection<ProductGroupSurvey> ProductGroupSurveyAddedByUser { get; set; }
        public virtual ICollection<ProductGroupSurveyLine> ProductGroupSurveyLineAddedByUser { get; set; }
        public virtual ICollection<ProductGroupSurveyLine> ProductGroupSurveyLineModifiedByUser { get; set; }
        public virtual ICollection<ProductGroupSurvey> ProductGroupSurveyModifiedByUser { get; set; }
        public virtual ICollection<ProductGrouping> ProductGroupingAddedByUser { get; set; }
        public virtual ICollection<ProductGroupingManufacture> ProductGroupingManufactureAddedByUser { get; set; }
        public virtual ICollection<ProductGroupingManufacture> ProductGroupingManufactureModifiedByUser { get; set; }
        public virtual ICollection<ProductGrouping> ProductGroupingModifiedByUser { get; set; }
        public virtual ICollection<ProductGroupingNav> ProductGroupingNavAddedByUser { get; set; }
        public virtual ICollection<ProductGroupingNavDifference> ProductGroupingNavDifferenceAddedByUser { get; set; }
        public virtual ICollection<ProductGroupingNavDifference> ProductGroupingNavDifferenceModifiedByUser { get; set; }
        public virtual ICollection<ProductGroupingNav> ProductGroupingNavModifiedByUser { get; set; }
        public virtual ICollection<ProductRecipeDocument> ProductRecipeDocumentAddedByUser { get; set; }
        public virtual ICollection<ProductRecipeDocument> ProductRecipeDocumentModifiedByUser { get; set; }
        public virtual ICollection<ProductionAction> ProductionActionAddedByUser { get; set; }
        public virtual ICollection<ProductionAction> ProductionActionModifiedByUser { get; set; }
        public virtual ICollection<ProductionActivity> ProductionActivityAddedByUser { get; set; }
        public virtual ICollection<ProductionActivityApp> ProductionActivityAppAddedByUser { get; set; }
        public virtual ICollection<ProductionActivityAppLine> ProductionActivityAppLineAddedByUser { get; set; }
        public virtual ICollection<ProductionActivityAppLine> ProductionActivityAppLineModifiedByUser { get; set; }
        public virtual ICollection<ProductionActivityAppLine> ProductionActivityAppLineQaCheckUser { get; set; }
        public virtual ICollection<ProductionActivityAppLineQaChecker> ProductionActivityAppLineQaChecker { get; set; }
        public virtual ICollection<ProductionActivityApp> ProductionActivityAppModifiedByUser { get; set; }
        public virtual ICollection<ProductionActivityCheckedDetails> ProductionActivityCheckedDetailsAddedByUser { get; set; }
        public virtual ICollection<ProductionActivityCheckedDetails> ProductionActivityCheckedDetailsCheckedBy { get; set; }
        public virtual ICollection<ProductionActivityCheckedDetails> ProductionActivityCheckedDetailsModifiedByUser { get; set; }
        public virtual ICollection<ProductionActivityClassification> ProductionActivityClassificationAddedByUser { get; set; }
        public virtual ICollection<ProductionActivityClassification> ProductionActivityClassificationModifiedByUser { get; set; }
        public virtual ICollection<ProductionActivityMaster> ProductionActivityMasterAddedByUser { get; set; }
        public virtual ICollection<ProductionActivityMasterLine> ProductionActivityMasterLineAddedByUser { get; set; }
        public virtual ICollection<ProductionActivityMasterLine> ProductionActivityMasterLineModifiedByUser { get; set; }
        public virtual ICollection<ProductionActivityMaster> ProductionActivityMasterModifiedByUser { get; set; }
        public virtual ICollection<ProductionActivityMasterRespons> ProductionActivityMasterResponsAddedByUser { get; set; }
        public virtual ICollection<ProductionActivityMasterRespons> ProductionActivityMasterResponsModifiedByUser { get; set; }
        public virtual ICollection<ProductionActivityMasterResponsRecurrence> ProductionActivityMasterResponsRecurrenceAddedByUser { get; set; }
        public virtual ICollection<ProductionActivityMasterResponsRecurrence> ProductionActivityMasterResponsRecurrenceModifiedByUser { get; set; }
        public virtual ICollection<ProductionActivity> ProductionActivityModifiedByUser { get; set; }
        public virtual ICollection<ProductionActivityNonCompliance> ProductionActivityNonComplianceAddedByUser { get; set; }
        public virtual ICollection<ProductionActivityNonCompliance> ProductionActivityNonComplianceModifiedByUser { get; set; }
        public virtual ICollection<ProductionActivityNonComplianceUser> ProductionActivityNonComplianceUser { get; set; }
        public virtual ICollection<ProductionActivityPlanningApp> ProductionActivityPlanningAppAddedByUser { get; set; }
        public virtual ICollection<ProductionActivityPlanningAppLine> ProductionActivityPlanningAppLineAddedByUser { get; set; }
        public virtual ICollection<ProductionActivityPlanningAppLine> ProductionActivityPlanningAppLineModifiedByUser { get; set; }
        public virtual ICollection<ProductionActivityPlanningAppLine> ProductionActivityPlanningAppLineQaCheckUser { get; set; }
        public virtual ICollection<ProductionActivityPlanningAppLineQaChecker> ProductionActivityPlanningAppLineQaChecker { get; set; }
        public virtual ICollection<ProductionActivityPlanningApp> ProductionActivityPlanningAppModifiedByUser { get; set; }
        public virtual ICollection<ProductionActivityRoutineApp> ProductionActivityRoutineAppAddedByUser { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLine> ProductionActivityRoutineAppLineAddedByUser { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLine> ProductionActivityRoutineAppLineModifiedByUser { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLine> ProductionActivityRoutineAppLineQaCheckUser { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLineQaChecker> ProductionActivityRoutineAppLineQaChecker { get; set; }
        public virtual ICollection<ProductionActivityRoutineApp> ProductionActivityRoutineAppModifiedByUser { get; set; }
        public virtual ICollection<ProductionActivityRoutineCheckedDetails> ProductionActivityRoutineCheckedDetailsAddedByUser { get; set; }
        public virtual ICollection<ProductionActivityRoutineCheckedDetails> ProductionActivityRoutineCheckedDetailsCheckedBy { get; set; }
        public virtual ICollection<ProductionActivityRoutineCheckedDetails> ProductionActivityRoutineCheckedDetailsModifiedByUser { get; set; }
        public virtual ICollection<ProductionBatchInformation> ProductionBatchInformationAddedByUser { get; set; }
        public virtual ICollection<ProductionBatchInformationLine> ProductionBatchInformationLineAddedByUser { get; set; }
        public virtual ICollection<ProductionBatchInformationLine> ProductionBatchInformationLineModifiedByUser { get; set; }
        public virtual ICollection<ProductionBatchInformation> ProductionBatchInformationModifiedByUser { get; set; }
        public virtual ICollection<ProductionBatchSize> ProductionBatchSizeAddedByUser { get; set; }
        public virtual ICollection<ProductionBatchSize> ProductionBatchSizeModifiedByUser { get; set; }
        public virtual ICollection<ProductionCapsule> ProductionCapsuleAddedByUser { get; set; }
        public virtual ICollection<ProductionCapsuleLine> ProductionCapsuleLineAddedByUser { get; set; }
        public virtual ICollection<ProductionCapsuleLine> ProductionCapsuleLineModifiedByUser { get; set; }
        public virtual ICollection<ProductionCapsule> ProductionCapsuleModifiedByUser { get; set; }
        public virtual ICollection<ProductionColor> ProductionColorAddedByUser { get; set; }
        public virtual ICollection<ProductionColorLine> ProductionColorLineAddedByUser { get; set; }
        public virtual ICollection<ProductionColorLine> ProductionColorLineModifiedByUser { get; set; }
        public virtual ICollection<ProductionColor> ProductionColorModifiedByUser { get; set; }
        public virtual ICollection<ProductionCycle> ProductionCycleAddedByUser { get; set; }
        public virtual ICollection<ProductionCycle> ProductionCycleModifiedByUser { get; set; }
        public virtual ICollection<ProductionDocument> ProductionDocumentAddedByUser { get; set; }
        public virtual ICollection<ProductionDocument> ProductionDocumentModifiedByUser { get; set; }
        public virtual ICollection<ProductionEntry> ProductionEntryAddedByUser { get; set; }
        public virtual ICollection<ProductionEntry> ProductionEntryModifiedByUser { get; set; }
        public virtual ICollection<ProductionFlavour> ProductionFlavourAddedByUser { get; set; }
        public virtual ICollection<ProductionFlavourLine> ProductionFlavourLineAddedByUser { get; set; }
        public virtual ICollection<ProductionFlavourLine> ProductionFlavourLineModifiedByUser { get; set; }
        public virtual ICollection<ProductionFlavour> ProductionFlavourModifiedByUser { get; set; }
        public virtual ICollection<ProductionForecast> ProductionForecastAddedByUser { get; set; }
        public virtual ICollection<ProductionForecast> ProductionForecastModifiedByUser { get; set; }
        public virtual ICollection<ProductionMachine> ProductionMachineAddedByUser { get; set; }
        public virtual ICollection<ProductionMachine> ProductionMachineModifiedByUser { get; set; }
        public virtual ICollection<ProductionMaterial> ProductionMaterialAddedByUser { get; set; }
        public virtual ICollection<ProductionMaterial> ProductionMaterialModifiedByUser { get; set; }
        public virtual ICollection<ProductionMethodTemplate> ProductionMethodTemplateAddedByUser { get; set; }
        public virtual ICollection<ProductionMethodTemplate> ProductionMethodTemplateModifiedByUser { get; set; }
        public virtual ICollection<ProductionOrder> ProductionOrderAddedByUser { get; set; }
        public virtual ICollection<ProductionOrderMaster> ProductionOrderMasterAddedByUser { get; set; }
        public virtual ICollection<ProductionOrderMaster> ProductionOrderMasterModifiedByUser { get; set; }
        public virtual ICollection<ProductionOrder> ProductionOrderModifiedByUser { get; set; }
        public virtual ICollection<ProductionOrderSplit> ProductionOrderSplitAddedByUser { get; set; }
        public virtual ICollection<ProductionOrderSplit> ProductionOrderSplitModifiedByUser { get; set; }
        public virtual ICollection<ProductionOutput> ProductionOutputAddedByUser { get; set; }
        public virtual ICollection<ProductionOutput> ProductionOutputModifiedByUser { get; set; }
        public virtual ICollection<ProductionSimulationGrouping> ProductionSimulationGroupingAddedByUser { get; set; }
        public virtual ICollection<ProductionSimulationGrouping> ProductionSimulationGroupingModifiedByUser { get; set; }
        public virtual ICollection<ProductionSimulationGroupingTicket> ProductionSimulationGroupingTicketAddedByUser { get; set; }
        public virtual ICollection<ProductionSimulationGroupingTicket> ProductionSimulationGroupingTicketModifiedByUser { get; set; }
        public virtual ICollection<ProductionSimulationHistory> ProductionSimulationHistoryAddedByUser { get; set; }
        public virtual ICollection<ProductionSimulationHistory> ProductionSimulationHistoryModifiedByUser { get; set; }
        public virtual ICollection<ProductionTicket> ProductionTicketAddedByUser { get; set; }
        public virtual ICollection<ProductionTicket> ProductionTicketModifiedByUser { get; set; }
        public virtual ICollection<ProfileDocument> ProfileDocument { get; set; }
        public virtual ICollection<Project> ProjectAddedByUser { get; set; }
        public virtual ICollection<Project> ProjectModifiedByUser { get; set; }
        public virtual ICollection<ProjectedDeliveryBlanketOthers> ProjectedDeliveryBlanketOthersAddedByUser { get; set; }
        public virtual ICollection<ProjectedDeliveryBlanketOthers> ProjectedDeliveryBlanketOthersModifiedByUser { get; set; }
        public virtual ICollection<ProjectedDeliverySalesOrderLine> ProjectedDeliverySalesOrderLineAddedByUser { get; set; }
        public virtual ICollection<ProjectedDeliverySalesOrderLine> ProjectedDeliverySalesOrderLineModifiedByUser { get; set; }
        public virtual ICollection<PsbreportCommentDetail> PsbreportCommentDetailAddedByUser { get; set; }
        public virtual ICollection<PsbreportCommentDetail> PsbreportCommentDetailModifiedByUser { get; set; }
        public virtual ICollection<PurchaseItemSalesEntryLine> PurchaseItemSalesEntryLineAddedByUser { get; set; }
        public virtual ICollection<PurchaseItemSalesEntryLine> PurchaseItemSalesEntryLineModifiedByUser { get; set; }
        public virtual ICollection<PurchaseOrder> PurchaseOrderAddedByUser { get; set; }
        public virtual ICollection<PurchaseOrderDocument> PurchaseOrderDocumentAddedByUser { get; set; }
        public virtual ICollection<PurchaseOrderDocument> PurchaseOrderDocumentModifiedByUser { get; set; }
        public virtual ICollection<PurchaseOrder> PurchaseOrderModifiedByUser { get; set; }
        public virtual ICollection<Qcapproval> QcapprovalAddedByUser { get; set; }
        public virtual ICollection<QcapprovalLine> QcapprovalLineAddedByUser { get; set; }
        public virtual ICollection<QcapprovalLine> QcapprovalLineModifiedByUser { get; set; }
        public virtual ICollection<Qcapproval> QcapprovalModifiedByUser { get; set; }
        public virtual ICollection<QuotationAward> QuotationAwardAddedByUser { get; set; }
        public virtual ICollection<QuotationAwardLine> QuotationAwardLineAddedByUser { get; set; }
        public virtual ICollection<QuotationAwardLine> QuotationAwardLineModifiedByUser { get; set; }
        public virtual ICollection<QuotationAward> QuotationAwardModifiedByUser { get; set; }
        public virtual ICollection<QuotationCategory> QuotationCategoryAddedByUser { get; set; }
        public virtual ICollection<QuotationCategory> QuotationCategoryModifiedByUser { get; set; }
        public virtual ICollection<QuotationHeader> QuotationHeaderAddedByUser { get; set; }
        public virtual ICollection<QuotationHeader> QuotationHeaderModifiedByUser { get; set; }
        public virtual ICollection<QuotationHistory> QuotationHistoryAddedByUser { get; set; }
        public virtual ICollection<QuotationHistoryDocument> QuotationHistoryDocumentAddedByUser { get; set; }
        public virtual ICollection<QuotationHistoryDocument> QuotationHistoryDocumentModifiedByUser { get; set; }
        public virtual ICollection<QuotationHistoryLine> QuotationHistoryLineAddedByUser { get; set; }
        public virtual ICollection<QuotationHistoryLine> QuotationHistoryLineModifiedByUser { get; set; }
        public virtual ICollection<QuotationHistory> QuotationHistoryModifiedByUser { get; set; }
        public virtual ICollection<ReAssignmentJob> ReAssignmentJobAddedByUser { get; set; }
        public virtual ICollection<ReAssignmentJob> ReAssignmentJobModifiedByUser { get; set; }
        public virtual ICollection<ReceiveEmail> ReceiveEmailAddedByUser { get; set; }
        public virtual ICollection<ReceiveEmail> ReceiveEmailModifiedByUser { get; set; }
        public virtual ICollection<RecordVariation> RecordVariationAddedByUser { get; set; }
        public virtual ICollection<RecordVariation> RecordVariationModifiedByUser { get; set; }
        public virtual ICollection<RegionMaster> RegionMasterAddedByUser { get; set; }
        public virtual ICollection<RegionMaster> RegionMasterModifiedByUser { get; set; }
        public virtual ICollection<RegistrationFormatInformation> RegistrationFormatInformationAddedByUser { get; set; }
        public virtual ICollection<RegistrationFormatInformationLine> RegistrationFormatInformationLineAddedByUser { get; set; }
        public virtual ICollection<RegistrationFormatInformationLine> RegistrationFormatInformationLineModifiedByUser { get; set; }
        public virtual ICollection<RegistrationFormatInformationLineUsers> RegistrationFormatInformationLineUsersAddedByUser { get; set; }
        public virtual ICollection<RegistrationFormatInformationLineUsers> RegistrationFormatInformationLineUsersModifiedByUser { get; set; }
        public virtual ICollection<RegistrationFormatInformationLineUsers> RegistrationFormatInformationLineUsersUser { get; set; }
        public virtual ICollection<RegistrationFormatInformation> RegistrationFormatInformationModifiedByUser { get; set; }
        public virtual ICollection<RegistrationVariation> RegistrationVariationAddedByUser { get; set; }
        public virtual ICollection<RegistrationVariation> RegistrationVariationModifiedByUser { get; set; }
        public virtual ICollection<RequestAc> RequestAcAddedByUser { get; set; }
        public virtual ICollection<RequestAc> RequestAcModifiedByUser { get; set; }
        public virtual ICollection<RequestAcline> RequestAclineAddedByUser { get; set; }
        public virtual ICollection<RequestAcline> RequestAclineModifiedByUser { get; set; }
        public virtual ICollection<RoutineInfo> RoutineInfoAddedByUser { get; set; }
        public virtual ICollection<RoutineInfo> RoutineInfoModifiedByUser { get; set; }
        public virtual ICollection<RoutineInfoMultiple> RoutineInfoMultipleAddedByUser { get; set; }
        public virtual ICollection<RoutineInfoMultiple> RoutineInfoMultipleModifiedByUser { get; set; }
        public virtual ICollection<SalesAdhocNovate> SalesAdhocNovateAddedByUser { get; set; }
        public virtual ICollection<SalesAdhocNovateAttachment> SalesAdhocNovateAttachmentAddedByUser { get; set; }
        public virtual ICollection<SalesAdhocNovateAttachment> SalesAdhocNovateAttachmentModifiedByUser { get; set; }
        public virtual ICollection<SalesAdhocNovate> SalesAdhocNovateModifiedByUser { get; set; }
        public virtual ICollection<SalesBorrowHeader> SalesBorrowHeaderAddedByUser { get; set; }
        public virtual ICollection<SalesBorrowHeader> SalesBorrowHeaderModifiedByUser { get; set; }
        public virtual ICollection<SalesBorrowHeader> SalesBorrowHeaderOnBehalf { get; set; }
        public virtual ICollection<SalesBorrowLine> SalesBorrowLineAddedByUser { get; set; }
        public virtual ICollection<SalesBorrowLine> SalesBorrowLineModifiedByUser { get; set; }
        public virtual ICollection<SalesItemPackingInfo> SalesItemPackingInfoAddedByUser { get; set; }
        public virtual ICollection<SalesItemPackingInfo> SalesItemPackingInfoModifiedByUser { get; set; }
        public virtual ICollection<SalesItemPackingInforLine> SalesItemPackingInforLineAddedByUser { get; set; }
        public virtual ICollection<SalesItemPackingInforLine> SalesItemPackingInforLineModifiedByUser { get; set; }
        public virtual ICollection<SalesMainPromotionList> SalesMainPromotionListAddedByUser { get; set; }
        public virtual ICollection<SalesMainPromotionList> SalesMainPromotionListModifiedByUser { get; set; }
        public virtual ICollection<SalesOrder> SalesOrderAddedByUser { get; set; }
        public virtual ICollection<SalesOrderAttachment> SalesOrderAttachmentAddedByUser { get; set; }
        public virtual ICollection<SalesOrderAttachment> SalesOrderAttachmentModifiedByUser { get; set; }
        public virtual ICollection<SalesOrderEntry> SalesOrderEntryAddedByUser { get; set; }
        public virtual ICollection<SalesOrderEntry> SalesOrderEntryModifiedByUser { get; set; }
        public virtual ICollection<SalesOrderMasterPricing> SalesOrderMasterPricingAddedByUser { get; set; }
        public virtual ICollection<SalesOrderMasterPricingLine> SalesOrderMasterPricingLineAddedByUser { get; set; }
        public virtual ICollection<SalesOrderMasterPricingLine> SalesOrderMasterPricingLineModifiedByUser { get; set; }
        public virtual ICollection<SalesOrderMasterPricing> SalesOrderMasterPricingModifiedByUser { get; set; }
        public virtual ICollection<SalesOrder> SalesOrderModifiedByUser { get; set; }
        public virtual ICollection<SalesOrderProduct> SalesOrderProductAddedByUser { get; set; }
        public virtual ICollection<SalesOrderProduct> SalesOrderProductModifiedByUser { get; set; }
        public virtual ICollection<SalesOrderProfile> SalesOrderProfileAddedByUser { get; set; }
        public virtual ICollection<SalesOrderProfile> SalesOrderProfileModifiedByUser { get; set; }
        public virtual ICollection<SalesPromotion> SalesPromotionAddedByUser { get; set; }
        public virtual ICollection<SalesPromotion> SalesPromotionModifiedByUser { get; set; }
        public virtual ICollection<SalesSurveyByFieldForce> SalesSurveyByFieldForceAddedByUser { get; set; }
        public virtual ICollection<SalesSurveyByFieldForceLine> SalesSurveyByFieldForceLineAddedByUser { get; set; }
        public virtual ICollection<SalesSurveyByFieldForceLine> SalesSurveyByFieldForceLineModifiedByUser { get; set; }
        public virtual ICollection<SalesSurveyByFieldForce> SalesSurveyByFieldForceModifiedByUser { get; set; }
        public virtual ICollection<SalesSurveyByFieldForce> SalesSurveyByFieldForceSalesPerson { get; set; }
        public virtual ICollection<SalesTargetByStaff> SalesTargetByStaffAddedByUser { get; set; }
        public virtual ICollection<SalesTargetByStaffLine> SalesTargetByStaffLineAddedByUser { get; set; }
        public virtual ICollection<SalesTargetByStaffLine> SalesTargetByStaffLineModifiedByUser { get; set; }
        public virtual ICollection<SalesTargetByStaff> SalesTargetByStaffModifiedByUser { get; set; }
        public virtual ICollection<SampleRequestForDoctor> SampleRequestForDoctorAddedByUser { get; set; }
        public virtual ICollection<SampleRequestForDoctorHeader> SampleRequestForDoctorHeaderAddedByUser { get; set; }
        public virtual ICollection<SampleRequestForDoctorHeader> SampleRequestForDoctorHeaderModifiedByUser { get; set; }
        public virtual ICollection<SampleRequestForDoctorHeader> SampleRequestForDoctorHeaderRequestBy { get; set; }
        public virtual ICollection<SampleRequestForDoctor> SampleRequestForDoctorModifiedByUser { get; set; }
        public virtual ICollection<SampleRequestForm> SampleRequestFormAddedByUser { get; set; }
        public virtual ICollection<SampleRequestFormLine> SampleRequestFormLineAddedByUser { get; set; }
        public virtual ICollection<SampleRequestFormLine> SampleRequestFormLineModifiedByUser { get; set; }
        public virtual ICollection<SampleRequestForm> SampleRequestFormModifiedByUser { get; set; }
        public virtual ICollection<ScheduleOfDelivery> ScheduleOfDeliveryAddedByUser { get; set; }
        public virtual ICollection<ScheduleOfDelivery> ScheduleOfDeliveryModifiedByUser { get; set; }
        public virtual ICollection<Section> SectionAddedByUser { get; set; }
        public virtual ICollection<Section> SectionModifiedByUser { get; set; }
        public virtual ICollection<SellingCatalogue> SellingCatalogueAddedByUser { get; set; }
        public virtual ICollection<SellingCatalogue> SellingCatalogueModifiedByUser { get; set; }
        public virtual ICollection<SellingPriceInformation> SellingPriceInformationAddedByUser { get; set; }
        public virtual ICollection<SellingPriceInformation> SellingPriceInformationModifiedByUser { get; set; }
        public virtual ICollection<ShiftMaster> ShiftMasterAddedByUser { get; set; }
        public virtual ICollection<ShiftMaster> ShiftMasterModifiedByUser { get; set; }
        public virtual ICollection<ShortCut> ShortCutAddedByUser { get; set; }
        public virtual ICollection<ShortCut> ShortCutModifiedByUser { get; set; }
        public virtual ICollection<Smecomment> Smecomment { get; set; }
        public virtual ICollection<SoSalesOrder> SoSalesOrderAddedByUser { get; set; }
        public virtual ICollection<SoSalesOrderLine> SoSalesOrderLineAddedByUser { get; set; }
        public virtual ICollection<SoSalesOrderLine> SoSalesOrderLineModifiedByUser { get; set; }
        public virtual ICollection<SoSalesOrder> SoSalesOrderModifiedByUser { get; set; }
        public virtual ICollection<SobyCustomers> SobyCustomersAddedByUser { get; set; }
        public virtual ICollection<SobyCustomersMasterAddress> SobyCustomersMasterAddressAddedByUser { get; set; }
        public virtual ICollection<SobyCustomersMasterAddress> SobyCustomersMasterAddressModifiedByUser { get; set; }
        public virtual ICollection<SobyCustomers> SobyCustomersModifiedByUser { get; set; }
        public virtual ICollection<SocustomersDelivery> SocustomersDeliveryAddedByUser { get; set; }
        public virtual ICollection<SocustomersDelivery> SocustomersDeliveryModifiedByUser { get; set; }
        public virtual ICollection<SocustomersIssue> SocustomersIssueAddedByUser { get; set; }
        public virtual ICollection<SocustomersIssue> SocustomersIssueModifiedByUser { get; set; }
        public virtual ICollection<SocustomersItemCrossReference> SocustomersItemCrossReferenceAddedByUser { get; set; }
        public virtual ICollection<SocustomersItemCrossReference> SocustomersItemCrossReferenceModifiedByUser { get; set; }
        public virtual ICollection<SocustomersSalesInfomation> SocustomersSalesInfomationAddedByUser { get; set; }
        public virtual ICollection<SocustomersSalesInfomation> SocustomersSalesInfomationModifiedByUser { get; set; }
        public virtual ICollection<SolotInformation> SolotInformationAddedByUser { get; set; }
        public virtual ICollection<SolotInformation> SolotInformationModifiedByUser { get; set; }
        public virtual ICollection<SourceList> SourceListAddedByUser { get; set; }
        public virtual ICollection<SourceList> SourceListModifiedByUser { get; set; }
        public virtual ICollection<SowithOutBlanketOrder> SowithOutBlanketOrderAddedByUser { get; set; }
        public virtual ICollection<SowithOutBlanketOrder> SowithOutBlanketOrderModifiedByUser { get; set; }
        public virtual ICollection<StaffTraining> StaffTrainingStaff { get; set; }
        public virtual ICollection<StaffTraining> StaffTrainingTrainner { get; set; }
        public virtual ICollection<StandardManufacturingProcess> StandardManufacturingProcessAddedByUser { get; set; }
        public virtual ICollection<StandardManufacturingProcessLine> StandardManufacturingProcessLineAddedByUser { get; set; }
        public virtual ICollection<StandardManufacturingProcessLine> StandardManufacturingProcessLineModifiedByUser { get; set; }
        public virtual ICollection<StandardManufacturingProcess> StandardManufacturingProcessModifiedByUser { get; set; }
        public virtual ICollection<StandardProcedure> StandardProcedureAddedByUser { get; set; }
        public virtual ICollection<StandardProcedure> StandardProcedureModifiedByUser { get; set; }
        public virtual ICollection<StartOfDay> StartOfDayAddedByUser { get; set; }
        public virtual ICollection<StartOfDay> StartOfDayModifiedByUser { get; set; }
        public virtual ICollection<State> StateAddedByUser { get; set; }
        public virtual ICollection<State> StateModifiedByUser { get; set; }
        public virtual ICollection<SubSection> SubSectionAddedByUser { get; set; }
        public virtual ICollection<SubSection> SubSectionModifiedByUser { get; set; }
        public virtual ICollection<SubSectionTwo> SubSectionTwoAddedByUser { get; set; }
        public virtual ICollection<SubSectionTwo> SubSectionTwoModifiedByUser { get; set; }
        public virtual ICollection<SunwardAssetList> SunwardAssetListAddedByUser { get; set; }
        public virtual ICollection<SunwardAssetList> SunwardAssetListModifiedByUser { get; set; }
        public virtual ICollection<SunwardGroupCompany> SunwardGroupCompanyAddedByUser { get; set; }
        public virtual ICollection<SunwardGroupCompany> SunwardGroupCompanyModifiedByUser { get; set; }
        public virtual ICollection<TableDataVersionInfo> TableDataVersionInfo { get; set; }
        public virtual ICollection<Tag> TagAddedByUser { get; set; }
        public virtual ICollection<TagMaster> TagMasterAddedByUser { get; set; }
        public virtual ICollection<TagMaster> TagMasterModifiedByUser { get; set; }
        public virtual ICollection<Tag> TagModifiedByUser { get; set; }
        public virtual ICollection<TaskAppointment> TaskAppointment { get; set; }
        public virtual ICollection<TaskAssigned> TaskAssignedTaskOwner { get; set; }
        public virtual ICollection<TaskAssigned> TaskAssignedUser { get; set; }
        public virtual ICollection<TaskAttachment> TaskAttachmentLockedByUser { get; set; }
        public virtual ICollection<TaskAttachment> TaskAttachmentUploadedByUser { get; set; }
        public virtual ICollection<TaskComment> TaskCommentCommentedByNavigation { get; set; }
        public virtual ICollection<TaskComment> TaskCommentEditedByNavigation { get; set; }
        public virtual ICollection<TaskCommentUser> TaskCommentUserPinnedByNavigation { get; set; }
        public virtual ICollection<TaskCommentUser> TaskCommentUserUser { get; set; }
        public virtual ICollection<TaskDiscussion> TaskDiscussion { get; set; }
        public virtual ICollection<TaskInviteUser> TaskInviteUser { get; set; }
        public virtual ICollection<TaskMaster> TaskMasterAddedByUser { get; set; }
        public virtual ICollection<TaskMasterDescriptionVersion> TaskMasterDescriptionVersionAddedByUser { get; set; }
        public virtual ICollection<TaskMasterDescriptionVersion> TaskMasterDescriptionVersionModifiedByUser { get; set; }
        public virtual ICollection<TaskMaster> TaskMasterModifiedByUser { get; set; }
        public virtual ICollection<TaskMaster> TaskMasterOnBehalfNavigation { get; set; }
        public virtual ICollection<TaskMaster> TaskMasterOwner { get; set; }
        public virtual ICollection<TaskMeetingNote> TaskMeetingNote { get; set; }
        public virtual ICollection<TaskMembers> TaskMembers { get; set; }
        public virtual ICollection<TaskNotes> TaskNotes { get; set; }
        public virtual ICollection<TaskUnReadNotes> TaskUnReadNotes { get; set; }
        public virtual ICollection<TeamMaster> TeamMasterAddedByUser { get; set; }
        public virtual ICollection<TeamMaster> TeamMasterModifiedByUser { get; set; }
        public virtual ICollection<TeamMember> TeamMember { get; set; }
        public virtual ICollection<TempSalesPackInformation> TempSalesPackInformationAddedByUser { get; set; }
        public virtual ICollection<TempSalesPackInformationFactor> TempSalesPackInformationFactorAddedByUser { get; set; }
        public virtual ICollection<TempSalesPackInformationFactor> TempSalesPackInformationFactorModifiedByUser { get; set; }
        public virtual ICollection<TempSalesPackInformation> TempSalesPackInformationModifiedByUser { get; set; }
        public virtual ICollection<TempVersion> TempVersionAddedByUser { get; set; }
        public virtual ICollection<TempVersion> TempVersionModifiedByUser { get; set; }
        public virtual ICollection<TempVersion> TempVersionPersonIncharge { get; set; }
        public virtual ICollection<TemplateCaseFormNotes> TemplateCaseFormNotesAddedByUser { get; set; }
        public virtual ICollection<TemplateCaseFormNotes> TemplateCaseFormNotesModifiedByUser { get; set; }
        public virtual ICollection<TemplateTestCase> TemplateTestCaseAddedByUser { get; set; }
        public virtual ICollection<TemplateTestCaseCheckList> TemplateTestCaseCheckListAddedByUser { get; set; }
        public virtual ICollection<TemplateTestCaseCheckList> TemplateTestCaseCheckListModifiedByUser { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponse> TemplateTestCaseCheckListResponseAddedByUser { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponseDuty> TemplateTestCaseCheckListResponseDutyAddedByUser { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponseDuty> TemplateTestCaseCheckListResponseDutyModifiedByUser { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponse> TemplateTestCaseCheckListResponseModifiedByUser { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponseRecurrence> TemplateTestCaseCheckListResponseRecurrenceAddedByUser { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponseRecurrence> TemplateTestCaseCheckListResponseRecurrenceModifiedByUser { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponseResponsible> TemplateTestCaseCheckListResponseResponsible { get; set; }
        public virtual ICollection<TemplateTestCaseForm> TemplateTestCaseFormAddedByUser { get; set; }
        public virtual ICollection<TemplateTestCaseForm> TemplateTestCaseFormModifiedByUser { get; set; }
        public virtual ICollection<TemplateTestCaseLink> TemplateTestCaseLinkAddedByUser { get; set; }
        public virtual ICollection<TemplateTestCaseLinkDoc> TemplateTestCaseLinkDocAddedByUser { get; set; }
        public virtual ICollection<TemplateTestCaseLinkDoc> TemplateTestCaseLinkDocModifiedByUser { get; set; }
        public virtual ICollection<TemplateTestCaseLink> TemplateTestCaseLinkModifiedByUser { get; set; }
        public virtual ICollection<TemplateTestCase> TemplateTestCaseModifiedByUser { get; set; }
        public virtual ICollection<TemplateTestCaseProposal> TemplateTestCaseProposalAddedByUser { get; set; }
        public virtual ICollection<TemplateTestCaseProposal> TemplateTestCaseProposalModifiedByUser { get; set; }
        public virtual ICollection<TemplateTestCaseProposal> TemplateTestCaseProposalReassignUser { get; set; }
        public virtual ICollection<TemplateTestCaseProposal> TemplateTestCaseProposalUser { get; set; }
        public virtual ICollection<TenderInformation> TenderInformationAddedByUser { get; set; }
        public virtual ICollection<TenderInformation> TenderInformationModifiedByUser { get; set; }
        public virtual ICollection<TenderPeriodPricing> TenderPeriodPricingAddedByUser { get; set; }
        public virtual ICollection<TenderPeriodPricingLine> TenderPeriodPricingLineAddedByUser { get; set; }
        public virtual ICollection<TenderPeriodPricingLine> TenderPeriodPricingLineModifiedByUser { get; set; }
        public virtual ICollection<TenderPeriodPricing> TenderPeriodPricingModifiedByUser { get; set; }
        public virtual ICollection<TransferBalanceQty> TransferBalanceQtyAddedByUser { get; set; }
        public virtual ICollection<TransferBalanceQtyDocument> TransferBalanceQtyDocumentAddedByUser { get; set; }
        public virtual ICollection<TransferBalanceQtyDocument> TransferBalanceQtyDocumentModifiedByUser { get; set; }
        public virtual ICollection<TransferBalanceQty> TransferBalanceQtyModifiedByUser { get; set; }
        public virtual ICollection<TransferPermissionLog> TransferPermissionLogAddedByUser { get; set; }
        public virtual ICollection<TransferPermissionLog> TransferPermissionLogCurrentUser { get; set; }
        public virtual ICollection<TransferPermissionLog> TransferPermissionLogPreviousUser { get; set; }
        public virtual ICollection<TransferSettings> TransferSettingsAddedByUser { get; set; }
        public virtual ICollection<TransferSettings> TransferSettingsModifiedByUser { get; set; }
        public virtual ICollection<UnitConversion> UnitConversionAddedByUser { get; set; }
        public virtual ICollection<UnitConversion> UnitConversionModifiedByUser { get; set; }
        public virtual ICollection<UserDepartment> UserDepartment { get; set; }
        public virtual ICollection<UserGroup> UserGroupAddedByUser { get; set; }
        public virtual ICollection<UserGroup> UserGroupModifiedByUser { get; set; }
        public virtual ICollection<UserGroupUser> UserGroupUser { get; set; }
        public virtual ICollection<UserSettings> UserSettings { get; set; }
        public virtual ICollection<VendorsList> VendorsListAddedByUser { get; set; }
        public virtual ICollection<VendorsList> VendorsListModifiedByUser { get; set; }
        public virtual ICollection<WikiAccess> WikiAccess { get; set; }
        public virtual ICollection<WikiAccessRight> WikiAccessRight { get; set; }
        public virtual ICollection<WikiGroup> WikiGroupAddedByUser { get; set; }
        public virtual ICollection<WikiGroup> WikiGroupModifiedByUser { get; set; }
        public virtual ICollection<WikiNotes> WikiNotes { get; set; }
        public virtual ICollection<WikiPageAction> WikiPageAction { get; set; }
        public virtual ICollection<WikiPageActionReply> WikiPageActionReply { get; set; }
        public virtual ICollection<WikiPage> WikiPageAddedByUser { get; set; }
        public virtual ICollection<WikiPage> WikiPageModifiedByUser { get; set; }
        public virtual ICollection<WikiPage> WikiPageNextApprover { get; set; }
        public virtual ICollection<WikiPage> WikiPageOwner { get; set; }
        public virtual ICollection<WikiPage> WikiPagePrimaryApprover { get; set; }
        public virtual ICollection<WikiResponsible> WikiResponsible { get; set; }
        public virtual ICollection<WikiSearch> WikiSearch { get; set; }
        public virtual ICollection<WorkFlow> WorkFlowAddedByUser { get; set; }
        public virtual ICollection<WorkFlowApproverSetup> WorkFlowApproverSetupAddedByUser { get; set; }
        public virtual ICollection<WorkFlowApproverSetup> WorkFlowApproverSetupApprover { get; set; }
        public virtual ICollection<WorkFlowApproverSetup> WorkFlowApproverSetupModifiedByUser { get; set; }
        public virtual ICollection<WorkFlowApproverSetup> WorkFlowApproverSetupUser { get; set; }
        public virtual ICollection<WorkFlowInteraction> WorkFlowInteractionAddedByUser { get; set; }
        public virtual ICollection<WorkFlowInteraction> WorkFlowInteractionModifiedByUser { get; set; }
        public virtual ICollection<WorkFlowInteractionRecipient> WorkFlowInteractionRecipient { get; set; }
        public virtual ICollection<WorkFlow> WorkFlowModifiedByUser { get; set; }
        public virtual ICollection<WorkFlowPages> WorkFlowPagesAddedByUser { get; set; }
        public virtual ICollection<WorkFlowPages> WorkFlowPagesModifiedByUser { get; set; }
        public virtual ICollection<WorkFlowUserGroupUsers> WorkFlowUserGroupUsers { get; set; }
        public virtual ICollection<WorkList> WorkListAddedByUser { get; set; }
        public virtual ICollection<WorkList> WorkListAssignedByNavigation { get; set; }
        public virtual ICollection<WorkList> WorkListAssignedToNavigation { get; set; }
        public virtual ICollection<WorkList> WorkListModifiedByUser { get; set; }
        public virtual ICollection<WorkOrder> WorkOrderAddedByUser { get; set; }
        public virtual ICollection<WorkOrder> WorkOrderAssignToNavigation { get; set; }
        public virtual ICollection<WorkOrderComment> WorkOrderCommentCommentedByNavigation { get; set; }
        public virtual ICollection<WorkOrderComment> WorkOrderCommentEditedByNavigation { get; set; }
        public virtual ICollection<WorkOrderCommentUser> WorkOrderCommentUser { get; set; }
        public virtual ICollection<WorkOrderLine> WorkOrderLineAddedByUser { get; set; }
        public virtual ICollection<WorkOrderLine> WorkOrderLineModifiedByUser { get; set; }
        public virtual ICollection<WorkOrder> WorkOrderModifiedByUser { get; set; }
    }
}
