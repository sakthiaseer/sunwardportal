﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavProductionInformation
    {
        public long NavProductionInformationId { get; set; }
        public long? ItemId { get; set; }
        public int? NoOfBuominOneCarton { get; set; }

        public virtual Navitems Item { get; set; }
    }
}
