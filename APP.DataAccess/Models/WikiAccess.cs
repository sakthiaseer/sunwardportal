﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WikiAccess
    {
        public long WikiAccessId { get; set; }
        public long? PageId { get; set; }
        public long? DepartmentId { get; set; }
        public long? SectionId { get; set; }
        public long? SubSectionId { get; set; }
        public long? UserId { get; set; }
        public bool? IsRequiredTraining { get; set; }
        public int? TrainingMethod { get; set; }
        public bool? TestRequired { get; set; }
        public long? TestPaperLink { get; set; }
        public DateTime? CompletionDate { get; set; }
        public string Notes { get; set; }

        public virtual Department Department { get; set; }
        public virtual WikiPage Page { get; set; }
        public virtual Section Section { get; set; }
        public virtual SubSection SubSection { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
