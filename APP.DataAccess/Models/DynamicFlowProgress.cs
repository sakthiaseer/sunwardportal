﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFlowProgress
    {
        public DynamicFlowProgress()
        {
            DynamicFlowProgressValue = new HashSet<DynamicFlowProgressValue>();
        }

        public long DynamicFlowProgressId { get; set; }
        public long DynamicFlowId { get; set; }
        public long DynamicFlowDetailId { get; set; }
        public long DynamicFlowStepId { get; set; }
        public long WorkFlowPageId { get; set; }
        public long? PageBlockId { get; set; }
        public string Name { get; set; }
        public string FlowName { get; set; }
        public string FlowDescription { get; set; }
        public int? FlowType { get; set; }
        public string FlowNo { get; set; }
        public string FlowLoopTo { get; set; }
        public string StepNo { get; set; }
        public string StepName { get; set; }
        public string StepDescription { get; set; }
        public string Instruction { get; set; }
        public int? StepType { get; set; }
        public string StepToLoop { get; set; }
        public long? PersonIncharge { get; set; }
        public long? UserGroupId { get; set; }
        public string Notes { get; set; }
        public string Duration { get; set; }
        public string TmeSheet { get; set; }
        public int? StatusCodeId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string SessionId { get; set; }

        public virtual DynamicFlow DynamicFlow { get; set; }
        public virtual DynamicFlowDetail DynamicFlowDetail { get; set; }
        public virtual DynamicFlowStep DynamicFlowStep { get; set; }
        public virtual CodeMaster FlowTypeNavigation { get; set; }
        public virtual WorkFlowPageBlock PageBlock { get; set; }
        public virtual ApplicationUser PersonInchargeNavigation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CodeMaster StepTypeNavigation { get; set; }
        public virtual UserGroup UserGroup { get; set; }
        public virtual WorkFlowPages WorkFlowPage { get; set; }
        public virtual ICollection<DynamicFlowProgressValue> DynamicFlowProgressValue { get; set; }
    }
}
