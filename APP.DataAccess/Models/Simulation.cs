﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Simulation
    {
        public long SimulationId { get; set; }
        public long? ItemId { get; set; }
        public bool? IsSteroid { get; set; }
        public long? SalesCategoryId { get; set; }
        public long? MethodCodeId { get; set; }
        public string Customer { get; set; }
        public string Description { get; set; }
        public long? PackSize1 { get; set; }
        public long? PackSize2 { get; set; }
        public long? PackSize3 { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? ApexQty { get; set; }
        public decimal? AntahQty { get; set; }
        public decimal? PxQty { get; set; }
        public decimal? MissQty { get; set; }
        public decimal? SymlQty { get; set; }
        public decimal? Acqty { get; set; }
        public decimal? Acsum { get; set; }
        public string ProdRecipe { get; set; }
        public long? BatchSize { get; set; }
        public decimal? NoOfTickets { get; set; }
        public decimal? DistStockBalance { get; set; }
        public decimal? NavstockBalance { get; set; }
        public decimal? SgStockBalance { get; set; }
        public string MethodCode { get; set; }
        public string Month { get; set; }
        public string Remarks { get; set; }
        public decimal? BatchSize90 { get; set; }
        public decimal? RoundUp1 { get; set; }
        public decimal? RoundUp2 { get; set; }
        public DateTime? ReportMonth { get; set; }
        public string ItemNo { get; set; }
        public decimal? MyStokBalance { get; set; }
    }
}
