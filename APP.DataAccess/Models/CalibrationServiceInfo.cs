﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CalibrationServiceInfo
    {
        public CalibrationServiceInfo()
        {
            AcceptableCalibrationInfo = new HashSet<AcceptableCalibrationInfo>();
            CalibrationVendorInfo = new HashSet<CalibrationVendorInfo>();
            InstructionType = new HashSet<InstructionType>();
            RangeCalibration = new HashSet<RangeCalibration>();
        }

        public long CalibrationServiceInformationId { get; set; }
        public int? CalibrationNo { get; set; }
        public string Instrument { get; set; }
        public string Lot { get; set; }
        public string Machine { get; set; }
        public string SerialNo { get; set; }
        public string DeviceId { get; set; }
        public long? ManufacturerId { get; set; }
        public string Model { get; set; }
        public string Range { get; set; }
        public string RangeOfCalibration { get; set; }
        public DateTime? LastCalibrationDate { get; set; }
        public DateTime? NextCalibrationDate { get; set; }
        public string Status { get; set; }
        public string Remark { get; set; }
        public bool? Certificate { get; set; }
        public string StickerCode { get; set; }
        public int? ExpireMonths { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual SourceList Manufacturer { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<AcceptableCalibrationInfo> AcceptableCalibrationInfo { get; set; }
        public virtual ICollection<CalibrationVendorInfo> CalibrationVendorInfo { get; set; }
        public virtual ICollection<InstructionType> InstructionType { get; set; }
        public virtual ICollection<RangeCalibration> RangeCalibration { get; set; }
    }
}
