﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CriticalSamplingFrequency
    {
        public long CriticalSamplingFrequencyId { get; set; }
        public long? SamplingFrequencyId { get; set; }
        public decimal? SampleFrequencyValue { get; set; }
        public long? CriticalstepandIntermediateLineId { get; set; }

        public virtual CriticalstepandIntermediateLine CriticalstepandIntermediateLine { get; set; }
    }
}
