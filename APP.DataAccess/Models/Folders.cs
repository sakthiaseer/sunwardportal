﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Folders
    {
        public Folders()
        {
            DocumentFolder = new HashSet<DocumentFolder>();
            DocumentInvitation = new HashSet<DocumentInvitation>();
            DocumentPermission = new HashSet<DocumentPermission>();
            DocumentUserRole = new HashSet<DocumentUserRole>();
            FolderDiscussion = new HashSet<FolderDiscussion>();
            FolderStorage = new HashSet<FolderStorage>();
            InverseParentFolder = new HashSet<Folders>();
            TaskAttachment = new HashSet<TaskAttachment>();
        }

        public long FolderId { get; set; }
        public long? MainFolderId { get; set; }
        public long? ParentFolderId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? FolderTypeId { get; set; }
        public bool? IsRead { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster FolderType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Folders ParentFolder { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<DocumentFolder> DocumentFolder { get; set; }
        public virtual ICollection<DocumentInvitation> DocumentInvitation { get; set; }
        public virtual ICollection<DocumentPermission> DocumentPermission { get; set; }
        public virtual ICollection<DocumentUserRole> DocumentUserRole { get; set; }
        public virtual ICollection<FolderDiscussion> FolderDiscussion { get; set; }
        public virtual ICollection<FolderStorage> FolderStorage { get; set; }
        public virtual ICollection<Folders> InverseParentFolder { get; set; }
        public virtual ICollection<TaskAttachment> TaskAttachment { get; set; }
    }
}
