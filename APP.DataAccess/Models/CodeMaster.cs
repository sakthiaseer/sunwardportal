﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CodeMaster
    {
        public CodeMaster()
        {
            Acentry = new HashSet<Acentry>();
            AcentryLines = new HashSet<AcentryLines>();
            Acitems = new HashSet<Acitems>();
            ActiveFlow = new HashSet<ActiveFlow>();
            ActiveFlowDetails = new HashSet<ActiveFlowDetails>();
            ActivityEmailTopics = new HashSet<ActivityEmailTopics>();
            AdditionalProcess = new HashSet<AdditionalProcess>();
            Address = new HashSet<Address>();
            AppIpirentryIpiruploadTypeNavigation = new HashSet<AppIpirentry>();
            AppIpirentryStatusCode = new HashSet<AppIpirentry>();
            AppSampling = new HashSet<AppSampling>();
            AppSamplingLine = new HashSet<AppSamplingLine>();
            AppTranDocumentLine = new HashSet<AppTranDocumentLine>();
            AppTranLfromLto = new HashSet<AppTranLfromLto>();
            AppTranLfromLtoLine = new HashSet<AppTranLfromLtoLine>();
            AppTranStock = new HashSet<AppTranStock>();
            AppTranStockLine = new HashSet<AppTranStockLine>();
            AppconsumptionEntry = new HashSet<AppconsumptionEntry>();
            AppdispenserDispensing = new HashSet<AppdispenserDispensing>();
            Appdrumming = new HashSet<Appdrumming>();
            ApplicationAbbreviation = new HashSet<ApplicationAbbreviation>();
            ApplicationFormClassificationType = new HashSet<ApplicationForm>();
            ApplicationFormFields = new HashSet<ApplicationFormFields>();
            ApplicationFormMasterPermission = new HashSet<ApplicationFormMasterPermission>();
            ApplicationFormSearch = new HashSet<ApplicationFormSearch>();
            ApplicationFormStatusCode = new HashSet<ApplicationForm>();
            ApplicationGlossary = new HashSet<ApplicationGlossary>();
            ApplicationMasterChild = new HashSet<ApplicationMasterChild>();
            ApplicationMasterDetail = new HashSet<ApplicationMasterDetail>();
            ApplicationMasterParent = new HashSet<ApplicationMasterParent>();
            ApplicationReminder = new HashSet<ApplicationReminder>();
            ApplicationRole = new HashSet<ApplicationRole>();
            ApplicationUser = new HashSet<ApplicationUser>();
            ApplicationWikiLineCustom = new HashSet<ApplicationWikiLine>();
            ApplicationWikiLineDuty = new HashSet<ApplicationWikiLineDuty>();
            ApplicationWikiLineNotificationAdviceType = new HashSet<ApplicationWikiLine>();
            ApplicationWikiLineRepeat = new HashSet<ApplicationWikiLine>();
            ApplicationWikiLineStatusCode = new HashSet<ApplicationWikiLine>();
            ApplicationWikiRecurrenceOccurenceOption = new HashSet<ApplicationWikiRecurrence>();
            ApplicationWikiRecurrenceStatusCode = new HashSet<ApplicationWikiRecurrence>();
            ApplicationWikiRecurrenceType = new HashSet<ApplicationWikiRecurrence>();
            ApplicationWikiStatusCode = new HashSet<ApplicationWiki>();
            ApplicationWikiTranslationRequired = new HashSet<ApplicationWiki>();
            ApplicationWikiWeekly = new HashSet<ApplicationWikiWeekly>();
            ApplicationWikiWikiOwnerType = new HashSet<ApplicationWiki>();
            AppmaterialReturn = new HashSet<AppmaterialReturn>();
            ApprovalDetails = new HashSet<ApprovalDetails>();
            ApproveMasterApproveLevel = new HashSet<ApproveMaster>();
            ApproveMasterApproveType = new HashSet<ApproveMaster>();
            ApproveMasterStatusCode = new HashSet<ApproveMaster>();
            ApproverSetup = new HashSet<ApproverSetup>();
            AppsupervisorDispensingEntry = new HashSet<AppsupervisorDispensingEntry>();
            ApptransferRelcassEntryStatusCode = new HashSet<ApptransferRelcassEntry>();
            ApptransferRelcassEntryTransferType = new HashSet<ApptransferRelcassEntry>();
            AssetAssignmentAssetStatusNavigation = new HashSet<AssetAssignment>();
            AssetAssignmentStatusCode = new HashSet<AssetAssignment>();
            AssetCatalogMaster = new HashSet<AssetCatalogMaster>();
            AssetDisposalDisposalType = new HashSet<AssetDisposal>();
            AssetDisposalStatusCode = new HashSet<AssetDisposal>();
            AssetEquipmentMaintenaceMaster = new HashSet<AssetEquipmentMaintenaceMaster>();
            AssetMaintenance = new HashSet<AssetMaintenance>();
            AssetMasterDepricationMethodNavigation = new HashSet<AssetMaster>();
            AssetMasterStatusCode = new HashSet<AssetMaster>();
            AssetPartsMaintenaceMaster = new HashSet<AssetPartsMaintenaceMaster>();
            AssetTicketImpactStatusNavigation = new HashSet<AssetTicket>();
            AssetTicketPriorityStatusNavigation = new HashSet<AssetTicket>();
            AssetTicketStatusCode = new HashSet<AssetTicket>();
            AssetTicketUrgencyStatusNavigation = new HashSet<AssetTicket>();
            AssetTransfer = new HashSet<AssetTransfer>();
            AttributeDetails = new HashSet<AttributeDetails>();
            AttributeHeaderControlTypeNavigation = new HashSet<AttributeHeader>();
            AttributeHeaderStatusCode = new HashSet<AttributeHeader>();
            BaseUom = new HashSet<BaseUom>();
            BlanketOrderExtensionInformation = new HashSet<BlanketOrder>();
            BlanketOrderExtensionStatus = new HashSet<BlanketOrder>();
            BlanketOrderQtyExtensionStatus = new HashSet<BlanketOrder>();
            BlanketOrderStatusCode = new HashSet<BlanketOrder>();
            BlanketOrderTypeOfRequest = new HashSet<BlanketOrder>();
            BlisterMouldInformationStatusCode = new HashSet<BlisterMouldInformation>();
            BlisterMouldInformationUpperKnurlingCoverage = new HashSet<BlisterMouldInformation>();
            BlisterScrapCode = new HashSet<BlisterScrapCode>();
            BlisterScrapEntry = new HashSet<BlisterScrapEntry>();
            BlisterScrapMaster = new HashSet<BlisterScrapMaster>();
            BmrdisposalBox = new HashSet<BmrdisposalBox>();
            BmrdisposalMasterBox = new HashSet<BmrdisposalMasterBox>();
            Bmrmovement = new HashSet<Bmrmovement>();
            BmrmovementLine = new HashSet<BmrmovementLine>();
            BmrtoCarton = new HashSet<BmrtoCarton>();
            BmrtoCartonLine = new HashSet<BmrtoCartonLine>();
            BompackingMasterDosageForm = new HashSet<BompackingMaster>();
            BompackingMasterLineCalculationMethod = new HashSet<BompackingMasterLine>();
            BompackingMasterLineStatusCode = new HashSet<BompackingMasterLine>();
            BompackingMasterStatusCode = new HashSet<BompackingMaster>();
            BomproductionGroup = new HashSet<BomproductionGroup>();
            CalibrationServiceInfo = new HashSet<CalibrationServiceInfo>();
            CalibrationType = new HashSet<CalibrationType>();
            CalibrationVendorInfo = new HashSet<CalibrationVendorInfo>();
            Category = new HashSet<Category>();
            Ccfainformation = new HashSet<Ccfainformation>();
            Ccfbevaluation = new HashSet<Ccfbevaluation>();
            Ccfcapproval = new HashSet<Ccfcapproval>();
            Ccfdimplementation = new HashSet<Ccfdimplementation>();
            CcfdimplementationDetails = new HashSet<CcfdimplementationDetails>();
            ChangeControlForm = new HashSet<ChangeControlForm>();
            ChatGroup = new HashSet<ChatGroup>();
            ChatUser = new HashSet<ChatUser>();
            CityMaster = new HashSet<CityMaster>();
            ClassificationBmrLine = new HashSet<ClassificationBmrLine>();
            ClassificationBmrProductionStatus = new HashSet<ClassificationBmr>();
            ClassificationBmrStatusCode = new HashSet<ClassificationBmr>();
            ClassificationProduction = new HashSet<ClassificationProduction>();
            CommitmentOrders = new HashSet<CommitmentOrders>();
            CommitmentOrdersLine = new HashSet<CommitmentOrdersLine>();
            CommonFieldsProductionMachine = new HashSet<CommonFieldsProductionMachine>();
            CommonFieldsProductionMachineDocument = new HashSet<CommonFieldsProductionMachineDocument>();
            CommonFieldsProductionMachineLine = new HashSet<CommonFieldsProductionMachineLine>();
            CommonFieldsProductionMachineLineLineStatusCode = new HashSet<CommonFieldsProductionMachineLineLine>();
            CommonFieldsProductionMachineLineLineStatusOfRequalification = new HashSet<CommonFieldsProductionMachineLineLine>();
            CommonMasterMouldChangePartsStatusCode = new HashSet<CommonMasterMouldChangeParts>();
            CommonMasterMouldChangePartsTypeoftooling = new HashSet<CommonMasterMouldChangeParts>();
            CommonMasterToolingClassificationStatusCode = new HashSet<CommonMasterToolingClassification>();
            CommonMasterToolingClassificationToolInfo = new HashSet<CommonMasterToolingClassification>();
            CommonPackagingBottle = new HashSet<CommonPackagingBottle>();
            CommonPackagingBottleCap = new HashSet<CommonPackagingBottleCap>();
            CommonPackagingBottleInsert = new HashSet<CommonPackagingBottleInsert>();
            CommonPackagingCapseal = new HashSet<CommonPackagingCapseal>();
            CommonPackagingCarton = new HashSet<CommonPackagingCarton>();
            CommonPackagingDivider = new HashSet<CommonPackagingDivider>();
            CommonPackagingItemHeader = new HashSet<CommonPackagingItemHeader>();
            CommonPackagingLayerPad = new HashSet<CommonPackagingLayerPad>();
            CommonPackagingNesting = new HashSet<CommonPackagingNesting>();
            CommonPackagingSetInfo = new HashSet<CommonPackagingSetInfo>();
            CommonPackagingShrinkWrap = new HashSet<CommonPackagingShrinkWrap>();
            CommonPackingInformation = new HashSet<CommonPackingInformation>();
            CommonPackingInformationLine = new HashSet<CommonPackingInformationLine>();
            CommonPackingItemListLine = new HashSet<CommonPackingItemListLine>();
            CommonProcess = new HashSet<CommonProcess>();
            CommonProcessLine = new HashSet<CommonProcessLine>();
            CompanyCalendar = new HashSet<CompanyCalendar>();
            CompanyCalendarLine = new HashSet<CompanyCalendarLine>();
            CompanyCalendarLineLink = new HashSet<CompanyCalendarLineLink>();
            CompanyCalendarLineLinkUserLink = new HashSet<CompanyCalendarLineLinkUserLink>();
            CompanyCalendarLineMeetingNotes = new HashSet<CompanyCalendarLineMeetingNotes>();
            CompanyCalendarLineNotes = new HashSet<CompanyCalendarLineNotes>();
            CompanyCalendarLineNotesUser = new HashSet<CompanyCalendarLineNotesUser>();
            CompanyEventsEventType = new HashSet<CompanyEvents>();
            CompanyEventsPreparationType = new HashSet<CompanyEvents>();
            CompanyEventsRepeatInfo = new HashSet<CompanyEvents>();
            CompanyEventsStandardFrequency = new HashSet<CompanyEvents>();
            CompanyEventsStatusCode = new HashSet<CompanyEvents>();
            CompanyHolidayDetailAllowEarlyRelease = new HashSet<CompanyHolidayDetail>();
            CompanyHolidayDetailHolidayType = new HashSet<CompanyHolidayDetail>();
            CompanyHolidays = new HashSet<CompanyHolidays>();
            CompanyListing = new HashSet<CompanyListing>();
            CompanyListingLine = new HashSet<CompanyListingLine>();
            CompanyWorkDayMaster = new HashSet<CompanyWorkDayMaster>();
            Contact = new HashSet<Contact>();
            ContactActivities = new HashSet<ContactActivities>();
            ContactsGenderNavigation = new HashSet<Contacts>();
            ContactsMaritalStatusNavigation = new HashSet<Contacts>();
            ContactsStatusCode = new HashSet<Contacts>();
            ContactsTypeNavigation = new HashSet<Contacts>();
            ContractDistributionAttachment = new HashSet<ContractDistributionAttachment>();
            ContractDistributionSalesEntryLine = new HashSet<ContractDistributionSalesEntryLine>();
            Country = new HashSet<Country>();
            CountryInformationStatus = new HashSet<CountryInformationStatus>();
            CriticalstepandIntermediateLine = new HashSet<CriticalstepandIntermediateLine>();
            CriticalstepandIntermediateRegisterationCode = new HashSet<CriticalstepandIntermediate>();
            CriticalstepandIntermediateStatusCode = new HashSet<CriticalstepandIntermediate>();
            CustomerAcceptanceConfirmation = new HashSet<CustomerAcceptanceConfirmation>();
            Department = new HashSet<Department>();
            Designation = new HashSet<Designation>();
            DeviceCatalogMaster = new HashSet<DeviceCatalogMaster>();
            DeviceGroupList = new HashSet<DeviceGroupList>();
            DisposalItem = new HashSet<DisposalItem>();
            DistStockBalance = new HashSet<DistStockBalance>();
            DistributorPricing = new HashSet<DistributorPricing>();
            DistributorReplenishment = new HashSet<DistributorReplenishment>();
            DistributorReplenishmentLine = new HashSet<DistributorReplenishmentLine>();
            Division = new HashSet<Division>();
            DocumentAlert = new HashSet<DocumentAlert>();
            DocumentDmsshare = new HashSet<DocumentDmsshare>();
            DocumentInvitation = new HashSet<DocumentInvitation>();
            DocumentLink = new HashSet<DocumentLink>();
            DocumentNoSeries = new HashSet<DocumentNoSeries>();
            DocumentPermission = new HashSet<DocumentPermission>();
            DocumentProfile = new HashSet<DocumentProfile>();
            DocumentProfileNoSeriesLink = new HashSet<DocumentProfileNoSeries>();
            DocumentProfileNoSeriesProfileType = new HashSet<DocumentProfileNoSeries>();
            DocumentProfileNoSeriesStatusCode = new HashSet<DocumentProfileNoSeries>();
            DocumentRole = new HashSet<DocumentRole>();
            DocumentRolePermission = new HashSet<DocumentRolePermission>();
            DocumentShare = new HashSet<DocumentShare>();
            DocumentsArchiveStatus = new HashSet<Documents>();
            DocumentsCloseDocument = new HashSet<Documents>();
            DocumentsStatusCode = new HashSet<Documents>();
            DraftApplicationWikiLineCustom = new HashSet<DraftApplicationWikiLine>();
            DraftApplicationWikiLineDuty = new HashSet<DraftApplicationWikiLineDuty>();
            DraftApplicationWikiLineNotificationAdviceType = new HashSet<DraftApplicationWikiLine>();
            DraftApplicationWikiLineRepeat = new HashSet<DraftApplicationWikiLine>();
            DraftApplicationWikiLineStatusCode = new HashSet<DraftApplicationWikiLine>();
            DraftApplicationWikiRecurrenceOccurenceOption = new HashSet<DraftApplicationWikiRecurrence>();
            DraftApplicationWikiRecurrenceStatusCode = new HashSet<DraftApplicationWikiRecurrence>();
            DraftApplicationWikiRecurrenceType = new HashSet<DraftApplicationWikiRecurrence>();
            DraftApplicationWikiStatusCode = new HashSet<DraftApplicationWiki>();
            DraftApplicationWikiTranslationRequired = new HashSet<DraftApplicationWiki>();
            DraftApplicationWikiWeekly = new HashSet<DraftApplicationWikiWeekly>();
            DraftApplicationWikiWikiOwnerType = new HashSet<DraftApplicationWiki>();
            DynamicFlowCategory = new HashSet<DynamicFlow>();
            DynamicFlowDetail = new HashSet<DynamicFlowDetail>();
            DynamicFlowProgressFlowTypeNavigation = new HashSet<DynamicFlowProgress>();
            DynamicFlowProgressStatusCode = new HashSet<DynamicFlowProgress>();
            DynamicFlowProgressStepTypeNavigation = new HashSet<DynamicFlowProgress>();
            DynamicFlowStatusCode = new HashSet<DynamicFlow>();
            DynamicForm = new HashSet<DynamicForm>();
            DynamicFormData = new HashSet<DynamicFormData>();
            DynamicFormDataUpload = new HashSet<DynamicFormDataUpload>();
            DynamicFormItemLine = new HashSet<DynamicFormItemLine>();
            DynamicFormItemStatusCode = new HashSet<DynamicFormItem>();
            DynamicFormItemTransactionType = new HashSet<DynamicFormItem>();
            DynamicFormReport = new HashSet<DynamicFormReport>();
            DynamicFormSection = new HashSet<DynamicFormSection>();
            DynamicFormSectionAttribute = new HashSet<DynamicFormSectionAttribute>();
            Emails = new HashSet<Emails>();
            EmployeeIcthardInformation = new HashSet<EmployeeIcthardInformation>();
            EmployeeIctinformation = new HashSet<EmployeeIctinformation>();
            EmployeeInformation = new HashSet<EmployeeInformation>();
            EmployeeLeaveInformationNonPresence = new HashSet<EmployeeLeaveInformation>();
            EmployeeLeaveInformationStatusCode = new HashSet<EmployeeLeaveInformation>();
            EmployeeOtherDutyInformation = new HashSet<EmployeeOtherDutyInformation>();
            EmployeeResignationApproveStatusNavigation = new HashSet<EmployeeResignation>();
            EmployeeResignationResignationTypeNavigation = new HashSet<EmployeeResignation>();
            EmployeeSageInformation = new HashSet<EmployeeSageInformation>();
            EmployeeStatusCode = new HashSet<Employee>();
            EmployeeTypeOfEmployeementNavigation = new HashSet<Employee>();
            ExchangeRate = new HashSet<ExchangeRate>();
            ExchangeRateLine = new HashSet<ExchangeRateLine>();
            ExcipientInformationLineByProcess = new HashSet<ExcipientInformationLineByProcess>();
            ExtensionInformationLine = new HashSet<ExtensionInformationLine>();
            FileProfileSetupFormControlType = new HashSet<FileProfileSetupForm>();
            FileProfileSetupFormStatusCode = new HashSet<FileProfileSetupForm>();
            FileProfileTypeDynamicForm = new HashSet<FileProfileTypeDynamicForm>();
            FileProfileTypeShelfLifeDurationNavigation = new HashSet<FileProfileType>();
            FileProfileTypeStatusCode = new HashSet<FileProfileType>();
            FinishProdcutGeneralInfoLine = new HashSet<FinishProdcutGeneralInfoLine>();
            FinishProductExcipientLine = new HashSet<FinishProductExcipientLine>();
            FinishProductExcipientRegisterationCode = new HashSet<FinishProductExcipient>();
            FinishProductExcipientStatusCode = new HashSet<FinishProductExcipient>();
            FinishProductGeneralInfoDocument = new HashSet<FinishProductGeneralInfoDocument>();
            FinishProductGeneralInfoRegisterationCode = new HashSet<FinishProductGeneralInfo>();
            FinishProductGeneralInfoStatusCode = new HashSet<FinishProductGeneralInfo>();
            FinishProductRegisterationCode = new HashSet<FinishProduct>();
            FinishProductStatusCode = new HashSet<FinishProduct>();
            FlowDistribution = new HashSet<FlowDistribution>();
            FlowDistributionDelivery = new HashSet<FlowDistributionDelivery>();
            FlowDistributionDetail = new HashSet<FlowDistributionDetail>();
            Fmglobal = new HashSet<Fmglobal>();
            FmglobalLine = new HashSet<FmglobalLine>();
            FmglobalLineItem = new HashSet<FmglobalLineItem>();
            FolderDiscussion = new HashSet<FolderDiscussion>();
            FolderStorageFolderType = new HashSet<FolderStorage>();
            FolderStorageStatusCode = new HashSet<FolderStorage>();
            FoldersFolderType = new HashSet<Folders>();
            FoldersStatusCode = new HashSet<Folders>();
            FormTableFields = new HashSet<FormTableFields>();
            FormTables = new HashSet<FormTables>();
            FpcommonField = new HashSet<FpcommonField>();
            FpcommonFieldLine = new HashSet<FpcommonFieldLine>();
            FpdrugClassification = new HashSet<FpdrugClassification>();
            FpdrugClassificationLinePurpose = new HashSet<FpdrugClassificationLine>();
            FpdrugClassificationLineStatusCode = new HashSet<FpdrugClassificationLine>();
            FpmanufacturingRecipe = new HashSet<FpmanufacturingRecipe>();
            Fpproduct = new HashSet<Fpproduct>();
            FpproductLine = new HashSet<FpproductLine>();
            FpproductNavisionLine = new HashSet<FpproductNavisionLine>();
            GeneralEquivalaentNavision = new HashSet<GeneralEquivalaentNavision>();
            GenericCodes = new HashSet<GenericCodes>();
            GenericItemNameListing = new HashSet<GenericItemNameListing>();
            HandlingOfShipmentClassification = new HashSet<HandlingOfShipmentClassification>();
            HistoricalDetails = new HashSet<HistoricalDetails>();
            HolidayMaster = new HashSet<HolidayMaster>();
            HrexternalPersonal = new HashSet<HrexternalPersonal>();
            HumanMovement = new HashSet<HumanMovement>();
            HumanMovementActionHrstatusCode = new HashSet<HumanMovementAction>();
            HumanMovementActionStatusCode = new HashSet<HumanMovementAction>();
            Icbmp = new HashSet<Icbmp>();
            Icbmpline = new HashSet<Icbmpline>();
            IcmasterOperation = new HashSet<IcmasterOperation>();
            IctcertificateCertificateTypeNavigation = new HashSet<Ictcertificate>();
            IctcertificateStatusCode = new HashSet<Ictcertificate>();
            IctcontactDetailsContactType = new HashSet<IctcontactDetails>();
            IctcontactDetailsStatusCode = new HashSet<IctcontactDetails>();
            IctlayoutPlanTypes = new HashSet<IctlayoutPlanTypes>();
            IctmasterDocument = new HashSet<IctmasterDocument>();
            IctmasterMasterTypeNavigation = new HashSet<Ictmaster>();
            IctmasterStatusCode = new HashSet<Ictmaster>();
            InstructionType = new HashSet<InstructionType>();
            InterCompanyPurchaseOrder = new HashSet<InterCompanyPurchaseOrder>();
            InterCompanyPurchaseOrderLine = new HashSet<InterCompanyPurchaseOrderLine>();
            InventoryTypeItemSource = new HashSet<InventoryType>();
            InventoryTypeOpeningStockBalance = new HashSet<InventoryTypeOpeningStockBalance>();
            InventoryTypeOpeningStockBalanceLine = new HashSet<InventoryTypeOpeningStockBalanceLine>();
            InventoryTypeStatusCode = new HashSet<InventoryType>();
            Ipir = new HashSet<Ipir>();
            IpirApp = new HashSet<IpirApp>();
            IpirAppCheckedDetailsActivityInfo = new HashSet<IpirAppCheckedDetails>();
            IpirAppCheckedDetailsStatusCode = new HashSet<IpirAppCheckedDetails>();
            IpirReport = new HashSet<IpirReport>();
            IpirReportAssignmentIntendAction = new HashSet<IpirReportAssignment>();
            IpirReportAssignmentStatusCode = new HashSet<IpirReportAssignment>();
            Ipirline = new HashSet<Ipirline>();
            IpirlineDocument = new HashSet<IpirlineDocument>();
            IpirmobileActionAssignmentStatus = new HashSet<IpirmobileAction>();
            IpirmobileActionIntendedAction = new HashSet<IpirmobileAction>();
            IpirmobileActionStatusCode = new HashSet<IpirmobileAction>();
            IpirmobileIntendedAction = new HashSet<Ipirmobile>();
            IpirmobileStatusCode = new HashSet<Ipirmobile>();
            IssueReportIpir = new HashSet<IssueReportIpir>();
            ItemBatchInfo = new HashSet<ItemBatchInfo>();
            ItemClassificationHeader = new HashSet<ItemClassificationHeader>();
            ItemClassificationMasterClassificationType = new HashSet<ItemClassificationMaster>();
            ItemClassificationMasterStatusCode = new HashSet<ItemClassificationMaster>();
            ItemClassificationTransport = new HashSet<ItemClassificationTransport>();
            ItemCost = new HashSet<ItemCost>();
            ItemCostLine = new HashSet<ItemCostLine>();
            ItemManufacture = new HashSet<ItemManufacture>();
            ItemMaster = new HashSet<ItemMaster>();
            ItemSalesPrice = new HashSet<ItemSalesPrice>();
            ItemStockInfo = new HashSet<ItemStockInfo>();
            JobProgressTemplateLineLineProcess = new HashSet<JobProgressTemplateLineLineProcess>();
            JobProgressTemplateLineProcess = new HashSet<JobProgressTemplateLineProcess>();
            JobProgressTemplateLineProcessStatus = new HashSet<JobProgressTemplateLineProcessStatus>();
            JobProgressTemplateRecurrenceOccurenceOption = new HashSet<JobProgressTemplateRecurrence>();
            JobProgressTemplateRecurrenceStatusCode = new HashSet<JobProgressTemplateRecurrence>();
            JobProgressTemplateRecurrenceType = new HashSet<JobProgressTemplateRecurrence>();
            JobProgressTemplateSoftware = new HashSet<JobProgressTemplate>();
            JobProgressTemplateStatusCode = new HashSet<JobProgressTemplate>();
            JobProgressTempletateLine = new HashSet<JobProgressTempletateLine>();
            JobScheduleFrequency = new HashSet<JobSchedule>();
            JobScheduleStatusCode = new HashSet<JobSchedule>();
            JobScheduleWeekly = new HashSet<JobScheduleWeekly>();
            LayoutPlanType = new HashSet<LayoutPlanType>();
            LevelMaster = new HashSet<LevelMaster>();
            LocalClinicCompanyStatus = new HashSet<LocalClinic>();
            LocalClinicToSwstatus = new HashSet<LocalClinic>();
            Locations = new HashSet<Locations>();
            MachineTimeManHourInfoIntermittentCleaningAtNavigation = new HashSet<MachineTimeManHourInfo>();
            MachineTimeManHourInfoLevel1CleaningNavigation = new HashSet<MachineTimeManHourInfo>();
            MachineTimeManHourInfoLevel2CleaningNavigation = new HashSet<MachineTimeManHourInfo>();
            MachineTimeManHourInfoNextProdItemMaxTimeNavigation = new HashSet<MachineTimeManHourInfo>();
            MachineTimeManHourInfoNextProdItemMinTimeNavigation = new HashSet<MachineTimeManHourInfo>();
            MachineTimeManHourInfoNextStepMaxTimeGapNavigation = new HashSet<MachineTimeManHourInfo>();
            MachineTimeManHourInfoNextStepMinTimeGapNavigation = new HashSet<MachineTimeManHourInfo>();
            MachineTimeManHourInfoPreparationTimeNavigation = new HashSet<MachineTimeManHourInfo>();
            MachineTimeManHourInfoProductionTimeNavigation = new HashSet<MachineTimeManHourInfo>();
            MachineTimeManHourInfoStatusCode = new HashSet<MachineTimeManHourInfo>();
            MachineTimeManHourInfoTypeOfFeed = new HashSet<MachineTimeManHourInfo>();
            ManpowerInformation = new HashSet<ManpowerInformation>();
            ManpowerInformationLine = new HashSet<ManpowerInformationLine>();
            ManufacturingProcessLine = new HashSet<ManufacturingProcessLine>();
            ManufacturingProcessRegisterationCode = new HashSet<ManufacturingProcess>();
            ManufacturingProcessStatusCode = new HashSet<ManufacturingProcess>();
            MarginInformation = new HashSet<MarginInformation>();
            MarginInformationLine = new HashSet<MarginInformationLine>();
            MasterBlanketOrder = new HashSet<MasterBlanketOrder>();
            MasterBlanketOrderLine = new HashSet<MasterBlanketOrderLine>();
            MasterDocumentInformation = new HashSet<MasterDocumentInformation>();
            MasterForm = new HashSet<MasterForm>();
            MasterFormDetail = new HashSet<MasterFormDetail>();
            MasterInterCompanyPricing = new HashSet<MasterInterCompanyPricing>();
            MasterInterCompanyPricingLine = new HashSet<MasterInterCompanyPricingLine>();
            MedCatalogClassification = new HashSet<MedCatalogClassification>();
            MedMaster = new HashSet<MedMaster>();
            MedMasterLineDedicatedUsage = new HashSet<MedMasterLine>();
            MedMasterLineStatusCode = new HashSet<MedMasterLine>();
            Memo = new HashSet<Memo>();
            MethodTemplateRoutineLine = new HashSet<MethodTemplateRoutineLine>();
            MethodTemplateRoutineManufacturingType = new HashSet<MethodTemplateRoutine>();
            MethodTemplateRoutineStatusCode = new HashSet<MethodTemplateRoutine>();
            MobileShift = new HashSet<MobileShift>();
            Modules = new HashSet<Modules>();
            NavItemBatchNo = new HashSet<NavItemBatchNo>();
            NavManufacturingProcess = new HashSet<NavManufacturingProcess>();
            NavMethodCode = new HashSet<NavMethodCode>();
            NavMethodCodeLines = new HashSet<NavMethodCodeLines>();
            NavPackingMethod = new HashSet<NavPackingMethod>();
            NavProductMasterProductCreationTypeNavigation = new HashSet<NavProductMaster>();
            NavProductMasterRegistrationReference = new HashSet<NavProductMaster>();
            NavProductMasterStatusCode = new HashSet<NavProductMaster>();
            NavProductMasterTypeOfCoatingNavigation = new HashSet<NavProductMaster>();
            NavSaleCategory = new HashSet<NavSaleCategory>();
            NavbaseUnit = new HashSet<NavbaseUnit>();
            Navcustomer = new HashSet<Navcustomer>();
            NavisionCompany = new HashSet<NavisionCompany>();
            NavisionSpecification = new HashSet<NavisionSpecification>();
            NavitemLinks = new HashSet<NavitemLinks>();
            NavitemStockBalance = new HashSet<NavitemStockBalance>();
            Navitems = new HashSet<Navitems>();
            Navlocation = new HashSet<Navlocation>();
            NavplannedProdOrder = new HashSet<NavplannedProdOrder>();
            NavpostedShipment = new HashSet<NavpostedShipment>();
            NavproductCode = new HashSet<NavproductCode>();
            Navrecipes = new HashSet<Navrecipes>();
            Navsettings = new HashSet<Navsettings>();
            NavstockBalanace = new HashSet<NavstockBalanace>();
            Navvendor = new HashSet<Navvendor>();
            NonDeliverSo = new HashSet<NonDeliverSo>();
            Notes = new HashSet<Notes>();
            NoticeLineFrequency = new HashSet<NoticeLine>();
            NoticeLineModule = new HashSet<NoticeLine>();
            NoticeLineStatusCode = new HashSet<NoticeLine>();
            NoticeLineWeekly = new HashSet<NoticeLineWeekly>();
            NoticeModule = new HashSet<Notice>();
            NoticeStatusCode = new HashSet<Notice>();
            Notification = new HashSet<Notification>();
            NotificationHandler = new HashSet<NotificationHandler>();
            NotifyDocument = new HashSet<NotifyDocument>();
            NovateInformationLine = new HashSet<NovateInformationLine>();
            NpraformulationActiveIngredient = new HashSet<NpraformulationActiveIngredient>();
            NpraformulationExcipient = new HashSet<NpraformulationExcipient>();
            NpraformulationRegistrationReference = new HashSet<Npraformulation>();
            NpraformulationStatusCode = new HashSet<Npraformulation>();
            OpenAccessUser = new HashSet<OpenAccessUser>();
            OperationProcedure = new HashSet<OperationProcedure>();
            OperationSequence = new HashSet<OperationSequence>();
            OrderRequirement = new HashSet<OrderRequirement>();
            OrderRequirementGrouPinglineSplit = new HashSet<OrderRequirementGrouPinglineSplit>();
            OrderRequirementGroupingLine = new HashSet<OrderRequirementGroupingLine>();
            OrderRequirementLine = new HashSet<OrderRequirementLine>();
            OrderRequirementLineSplit = new HashSet<OrderRequirementLineSplit>();
            PackagingAluminiumFoilAluminiumFoilType = new HashSet<PackagingAluminiumFoil>();
            PackagingAluminiumFoilLine = new HashSet<PackagingAluminiumFoilLine>();
            PackagingAluminiumFoilStatusCode = new HashSet<PackagingAluminiumFoil>();
            PackagingHistoryItemLineDocument = new HashSet<PackagingHistoryItemLineDocument>();
            PackagingItemHistory = new HashSet<PackagingItemHistory>();
            PackagingItemHistoryLine = new HashSet<PackagingItemHistoryLine>();
            PackagingLabelLabelType = new HashSet<PackagingLabel>();
            PackagingLabelStatusCode = new HashSet<PackagingLabel>();
            PackagingLeafletLeafletType = new HashSet<PackagingLeaflet>();
            PackagingLeafletPrintedOn = new HashSet<PackagingLeaflet>();
            PackagingLeafletStatusCode = new HashSet<PackagingLeaflet>();
            PackagingPvcfoilPvcFoilType = new HashSet<PackagingPvcfoil>();
            PackagingPvcfoilStatusCode = new HashSet<PackagingPvcfoil>();
            PackagingTubeStatusCode = new HashSet<PackagingTube>();
            PackagingTubeTubeType = new HashSet<PackagingTube>();
            PackagingUnitBoxFinishing = new HashSet<PackagingUnitBox>();
            PackagingUnitBoxStatusCode = new HashSet<PackagingUnitBox>();
            PackagingUnitBoxUnitBoxType = new HashSet<PackagingUnitBox>();
            PacketInformationLine = new HashSet<PacketInformationLine>();
            PageApproval = new HashSet<PageApproval>();
            PageLinkCondition = new HashSet<PageLinkCondition>();
            PerUnitFormulationLine = new HashSet<PerUnitFormulationLine>();
            PhramacologicalPropertiesRegisterationCode = new HashSet<PhramacologicalProperties>();
            PhramacologicalPropertiesStatusCode = new HashSet<PhramacologicalProperties>();
            PkgapprovalInformation = new HashSet<PkgapprovalInformation>();
            PkginformationByPackSize = new HashSet<PkginformationByPackSize>();
            PkgregisteredPackingInformation = new HashSet<PkgregisteredPackingInformation>();
            Plant = new HashSet<Plant>();
            PlantMaintenanceEntry = new HashSet<PlantMaintenanceEntry>();
            PlantMaintenanceEntryMasterMasterTypeNavigation = new HashSet<PlantMaintenanceEntryMaster>();
            PlantMaintenanceEntryMasterStatusCode = new HashSet<PlantMaintenanceEntryMaster>();
            PlasticBag = new HashSet<PlasticBag>();
            Portfolio = new HashSet<Portfolio>();
            PortfolioLine = new HashSet<PortfolioLine>();
            ProcessMachineTime = new HashSet<ProcessMachineTime>();
            ProcessMachineTimeProductionLine = new HashSet<ProcessMachineTimeProductionLine>();
            ProcessMachineTimeProductionLineLine = new HashSet<ProcessMachineTimeProductionLineLine>();
            ProcessMachineTimeProductionMachineProcess = new HashSet<ProcessMachineTimeProductionMachineProcess>();
            ProcessManPowerSpecialNotes = new HashSet<ProcessManPowerSpecialNotes>();
            ProcessTransfer = new HashSet<ProcessTransfer>();
            ProcessTransferLine = new HashSet<ProcessTransferLine>();
            ProdRecipeCycle = new HashSet<ProdRecipeCycle>();
            ProductActivityCaseCompany = new HashSet<ProductActivityCaseCompany>();
            ProductActivityCaseLine = new HashSet<ProductActivityCaseLine>();
            ProductActivityCaseProductionType = new HashSet<ProductActivityCase>();
            ProductActivityCaseResponsCustom = new HashSet<ProductActivityCaseRespons>();
            ProductActivityCaseResponsDuty = new HashSet<ProductActivityCaseResponsDuty>();
            ProductActivityCaseResponsNotificationAdviceType = new HashSet<ProductActivityCaseRespons>();
            ProductActivityCaseResponsRecurrenceOccurenceOption = new HashSet<ProductActivityCaseResponsRecurrence>();
            ProductActivityCaseResponsRecurrenceStatusCode = new HashSet<ProductActivityCaseResponsRecurrence>();
            ProductActivityCaseResponsRecurrenceType = new HashSet<ProductActivityCaseResponsRecurrence>();
            ProductActivityCaseResponsRepeat = new HashSet<ProductActivityCaseRespons>();
            ProductActivityCaseResponsStatusCode = new HashSet<ProductActivityCaseRespons>();
            ProductActivityCaseResponsWeekly = new HashSet<ProductActivityCaseResponsWeekly>();
            ProductActivityCaseStatusCode = new HashSet<ProductActivityCase>();
            ProductActivityPermission = new HashSet<ProductActivityPermission>();
            ProductGroupSurvey = new HashSet<ProductGroupSurvey>();
            ProductGroupSurveyLine = new HashSet<ProductGroupSurveyLine>();
            ProductGrouping = new HashSet<ProductGrouping>();
            ProductGroupingManufacture = new HashSet<ProductGroupingManufacture>();
            ProductGroupingNav = new HashSet<ProductGroupingNav>();
            ProductGroupingNavDifference = new HashSet<ProductGroupingNavDifference>();
            ProductRecipeDocument = new HashSet<ProductRecipeDocument>();
            ProductionAction = new HashSet<ProductionAction>();
            ProductionActivity = new HashSet<ProductionActivity>();
            ProductionActivityApp = new HashSet<ProductionActivityApp>();
            ProductionActivityAppLine = new HashSet<ProductionActivityAppLine>();
            ProductionActivityCheckedDetailsActivityInfo = new HashSet<ProductionActivityCheckedDetails>();
            ProductionActivityCheckedDetailsStatusCode = new HashSet<ProductionActivityCheckedDetails>();
            ProductionActivityClassification = new HashSet<ProductionActivityClassification>();
            ProductionActivityMasterLine = new HashSet<ProductionActivityMasterLine>();
            ProductionActivityMasterProductionType = new HashSet<ProductionActivityMaster>();
            ProductionActivityMasterResponsCustom = new HashSet<ProductionActivityMasterRespons>();
            ProductionActivityMasterResponsNotificationAdviceType = new HashSet<ProductionActivityMasterRespons>();
            ProductionActivityMasterResponsRecurrenceOccurenceOption = new HashSet<ProductionActivityMasterResponsRecurrence>();
            ProductionActivityMasterResponsRecurrenceStatusCode = new HashSet<ProductionActivityMasterResponsRecurrence>();
            ProductionActivityMasterResponsRecurrenceType = new HashSet<ProductionActivityMasterResponsRecurrence>();
            ProductionActivityMasterResponsRepeat = new HashSet<ProductionActivityMasterRespons>();
            ProductionActivityMasterResponsStatusCode = new HashSet<ProductionActivityMasterRespons>();
            ProductionActivityMasterResponsWeekly = new HashSet<ProductionActivityMasterResponsWeekly>();
            ProductionActivityMasterStatusCode = new HashSet<ProductionActivityMaster>();
            ProductionActivityNonCompliance = new HashSet<ProductionActivityNonCompliance>();
            ProductionActivityPlanningApp = new HashSet<ProductionActivityPlanningApp>();
            ProductionActivityPlanningAppLine = new HashSet<ProductionActivityPlanningAppLine>();
            ProductionActivityRoutineApp = new HashSet<ProductionActivityRoutineApp>();
            ProductionActivityRoutineAppLine = new HashSet<ProductionActivityRoutineAppLine>();
            ProductionActivityRoutineCheckedDetailsActivityInfo = new HashSet<ProductionActivityRoutineCheckedDetails>();
            ProductionActivityRoutineCheckedDetailsStatusCode = new HashSet<ProductionActivityRoutineCheckedDetails>();
            ProductionBatchInformation = new HashSet<ProductionBatchInformation>();
            ProductionBatchInformationLine = new HashSet<ProductionBatchInformationLine>();
            ProductionBatchSize = new HashSet<ProductionBatchSize>();
            ProductionCapsule = new HashSet<ProductionCapsule>();
            ProductionCapsuleLine = new HashSet<ProductionCapsuleLine>();
            ProductionColor = new HashSet<ProductionColor>();
            ProductionColorLine = new HashSet<ProductionColorLine>();
            ProductionCycle = new HashSet<ProductionCycle>();
            ProductionDocument = new HashSet<ProductionDocument>();
            ProductionEntry = new HashSet<ProductionEntry>();
            ProductionFlavour = new HashSet<ProductionFlavour>();
            ProductionFlavourLine = new HashSet<ProductionFlavourLine>();
            ProductionForecast = new HashSet<ProductionForecast>();
            ProductionMachine = new HashSet<ProductionMachine>();
            ProductionMaterial = new HashSet<ProductionMaterial>();
            ProductionMaterialLineDensityNavigation = new HashSet<ProductionMaterialLine>();
            ProductionMaterialLineMicronisedOrMesh = new HashSet<ProductionMaterialLine>();
            ProductionMaterialLineSaltOrBase = new HashSet<ProductionMaterialLine>();
            ProductionMethodTemplate = new HashSet<ProductionMethodTemplate>();
            ProductionOrder = new HashSet<ProductionOrder>();
            ProductionOrderMaster = new HashSet<ProductionOrderMaster>();
            ProductionOrderSplit = new HashSet<ProductionOrderSplit>();
            ProductionOutput = new HashSet<ProductionOutput>();
            ProductionSimulationGrouping = new HashSet<ProductionSimulationGrouping>();
            ProductionSimulationGroupingTicket = new HashSet<ProductionSimulationGroupingTicket>();
            ProductionTicket = new HashSet<ProductionTicket>();
            ProjectedDeliveryBlanketOthers = new HashSet<ProjectedDeliveryBlanketOthers>();
            ProjectedDeliverySalesOrderLine = new HashSet<ProjectedDeliverySalesOrderLine>();
            PsbreportCommentDetail = new HashSet<PsbreportCommentDetail>();
            PurchaseItemSalesEntryLine = new HashSet<PurchaseItemSalesEntryLine>();
            PurchaseOrder = new HashSet<PurchaseOrder>();
            PurchaseOrderDocument = new HashSet<PurchaseOrderDocument>();
            Qcapproval = new HashSet<Qcapproval>();
            QcapprovalLine = new HashSet<QcapprovalLine>();
            QuotationAward = new HashSet<QuotationAward>();
            QuotationAwardLine = new HashSet<QuotationAwardLine>();
            QuotationCategory = new HashSet<QuotationCategory>();
            QuotationHeaderCategory = new HashSet<QuotationHeader>();
            QuotationHeaderCustomerType = new HashSet<QuotationHeader>();
            QuotationHeaderShipment = new HashSet<QuotationHeader>();
            QuotationHistory = new HashSet<QuotationHistory>();
            QuotationHistoryDocument = new HashSet<QuotationHistoryDocument>();
            QuotationHistoryLine = new HashSet<QuotationHistoryLine>();
            QuotationLine = new HashSet<QuotationLine>();
            ReAssignmentJob = new HashSet<ReAssignmentJob>();
            ReceiveEmail = new HashSet<ReceiveEmail>();
            RecordVariationLine = new HashSet<RecordVariationLine>();
            RecordVariationRegisterationCode = new HashSet<RecordVariation>();
            RecordVariationStatusCode = new HashSet<RecordVariation>();
            RecordVariationSubmissionStatus = new HashSet<RecordVariation>();
            RegistrationFormatInformation = new HashSet<RegistrationFormatInformation>();
            RegistrationFormatInformationLineFieldType = new HashSet<RegistrationFormatInformationLine>();
            RegistrationFormatInformationLineStatusCode = new HashSet<RegistrationFormatInformationLine>();
            RegistrationFormatInformationLineUsers = new HashSet<RegistrationFormatInformationLineUsers>();
            RegistrationVariationRegisterationCode = new HashSet<RegistrationVariation>();
            RegistrationVariationStatustCode = new HashSet<RegistrationVariation>();
            RequestAc = new HashSet<RequestAc>();
            RequestAcline = new HashSet<RequestAcline>();
            RoutineInfo = new HashSet<RoutineInfo>();
            RoutineInfoMultiple = new HashSet<RoutineInfoMultiple>();
            SalesAdhocNovate = new HashSet<SalesAdhocNovate>();
            SalesAdhocNovateAttachment = new HashSet<SalesAdhocNovateAttachment>();
            SalesBorrowHeader = new HashSet<SalesBorrowHeader>();
            SalesBorrowLine = new HashSet<SalesBorrowLine>();
            SalesItemPackingInfo = new HashSet<SalesItemPackingInfo>();
            SalesItemPackingInforLine = new HashSet<SalesItemPackingInforLine>();
            SalesMainPromotionList = new HashSet<SalesMainPromotionList>();
            SalesOrder = new HashSet<SalesOrder>();
            SalesOrderAttachment = new HashSet<SalesOrderAttachment>();
            SalesOrderEntryOrderBy = new HashSet<SalesOrderEntry>();
            SalesOrderEntryStatusCode = new HashSet<SalesOrderEntry>();
            SalesOrderEntryTypeOfOrder = new HashSet<SalesOrderEntry>();
            SalesOrderEntryTypeOfSalesOrder = new HashSet<SalesOrderEntry>();
            SalesOrderMasterPricing = new HashSet<SalesOrderMasterPricing>();
            SalesOrderMasterPricingLine = new HashSet<SalesOrderMasterPricingLine>();
            SalesOrderProduct = new HashSet<SalesOrderProduct>();
            SalesOrderProfileOrderBy = new HashSet<SalesOrderProfile>();
            SalesOrderProfileStatusCode = new HashSet<SalesOrderProfile>();
            SalesPromotion = new HashSet<SalesPromotion>();
            SalesSurveyByFieldForce = new HashSet<SalesSurveyByFieldForce>();
            SalesSurveyByFieldForceLine = new HashSet<SalesSurveyByFieldForceLine>();
            SalesTargetByStaff = new HashSet<SalesTargetByStaff>();
            SalesTargetByStaffLine = new HashSet<SalesTargetByStaffLine>();
            SampleRequestForDoctor = new HashSet<SampleRequestForDoctor>();
            SampleRequestForDoctorHeader = new HashSet<SampleRequestForDoctorHeader>();
            SampleRequestForm = new HashSet<SampleRequestForm>();
            SampleRequestFormLineIssueStatus = new HashSet<SampleRequestFormLine>();
            SampleRequestFormLineStatusCode = new HashSet<SampleRequestFormLine>();
            ScheduleOfDeliveryDayOfTheWeek = new HashSet<ScheduleOfDelivery>();
            ScheduleOfDeliveryPlanOfWeek = new HashSet<ScheduleOfDeliveryPlanOfWeek>();
            ScheduleOfDeliveryStatusCode = new HashSet<ScheduleOfDelivery>();
            Section = new HashSet<Section>();
            SelfTest = new HashSet<SelfTest>();
            SellingCatalogue = new HashSet<SellingCatalogue>();
            SellingPriceInformation = new HashSet<SellingPriceInformation>();
            ShiftMaster = new HashSet<ShiftMaster>();
            ShortCut = new HashSet<ShortCut>();
            Smecomment = new HashSet<Smecomment>();
            SoSalesOrder = new HashSet<SoSalesOrder>();
            SoSalesOrderLine = new HashSet<SoSalesOrderLine>();
            SobyCustomerSunwardEquivalent = new HashSet<SobyCustomerSunwardEquivalent>();
            SobyCustomersAddress = new HashSet<SobyCustomersAddress>();
            SobyCustomersBillingAddressStatus = new HashSet<SobyCustomers>();
            SobyCustomersManner = new HashSet<SobyCustomersManner>();
            SobyCustomersMasterAddressAddressType = new HashSet<SobyCustomersMasterAddress>();
            SobyCustomersMasterAddressStatusCode = new HashSet<SobyCustomersMasterAddress>();
            SobyCustomersStatusCode = new HashSet<SobyCustomers>();
            SocustomersDelivery = new HashSet<SocustomersDelivery>();
            SocustomersIssue = new HashSet<SocustomersIssue>();
            SocustomersItemCrossReference = new HashSet<SocustomersItemCrossReference>();
            SocustomersSalesInfomation = new HashSet<SocustomersSalesInfomation>();
            SolotInformation = new HashSet<SolotInformation>();
            SourceList = new HashSet<SourceList>();
            SowithOutBlanketOrder = new HashSet<SowithOutBlanketOrder>();
            StandardManufacturingProcess = new HashSet<StandardManufacturingProcess>();
            StandardManufacturingProcessLine = new HashSet<StandardManufacturingProcessLine>();
            StandardProcedure = new HashSet<StandardProcedure>();
            StartOfDay = new HashSet<StartOfDay>();
            State = new HashSet<State>();
            SubSection = new HashSet<SubSection>();
            SubSectionTwo = new HashSet<SubSectionTwo>();
            SunwardAssetListCalibrationStatusNavigation = new HashSet<SunwardAssetList>();
            SunwardAssetListDeviceStatus = new HashSet<SunwardAssetList>();
            SunwardAssetListStatusCode = new HashSet<SunwardAssetList>();
            SunwardGroupCompany = new HashSet<SunwardGroupCompany>();
            TagMaster = new HashSet<TagMaster>();
            TaskAppointment = new HashSet<TaskAppointment>();
            TaskAssigned = new HashSet<TaskAssigned>();
            TaskCommentUser = new HashSet<TaskCommentUser>();
            TaskDiscussion = new HashSet<TaskDiscussion>();
            TaskInviteUser = new HashSet<TaskInviteUser>();
            TaskMasterDescriptionVersion = new HashSet<TaskMasterDescriptionVersion>();
            TaskMasterRequestType = new HashSet<TaskMaster>();
            TaskMasterStatusCode = new HashSet<TaskMaster>();
            TaskMasterWorkType = new HashSet<TaskMaster>();
            TeamMaster = new HashSet<TeamMaster>();
            TempSalesPackInformation = new HashSet<TempSalesPackInformation>();
            TempSalesPackInformationFactor = new HashSet<TempSalesPackInformationFactor>();
            TempVersion = new HashSet<TempVersion>();
            TemplateCaseFormNotes = new HashSet<TemplateCaseFormNotes>();
            TemplateTestCase = new HashSet<TemplateTestCase>();
            TemplateTestCaseCheckList = new HashSet<TemplateTestCaseCheckList>();
            TemplateTestCaseCheckListResponseCustom = new HashSet<TemplateTestCaseCheckListResponse>();
            TemplateTestCaseCheckListResponseDuty = new HashSet<TemplateTestCaseCheckListResponseDuty>();
            TemplateTestCaseCheckListResponseNotificationAdviceType = new HashSet<TemplateTestCaseCheckListResponse>();
            TemplateTestCaseCheckListResponseRecurrenceOccurenceOption = new HashSet<TemplateTestCaseCheckListResponseRecurrence>();
            TemplateTestCaseCheckListResponseRecurrenceStatusCode = new HashSet<TemplateTestCaseCheckListResponseRecurrence>();
            TemplateTestCaseCheckListResponseRecurrenceType = new HashSet<TemplateTestCaseCheckListResponseRecurrence>();
            TemplateTestCaseCheckListResponseRepeat = new HashSet<TemplateTestCaseCheckListResponse>();
            TemplateTestCaseCheckListResponseStatusCode = new HashSet<TemplateTestCaseCheckListResponse>();
            TemplateTestCaseCheckListResponseWeekly = new HashSet<TemplateTestCaseCheckListResponseWeekly>();
            TemplateTestCaseForm = new HashSet<TemplateTestCaseForm>();
            TemplateTestCaseLink = new HashSet<TemplateTestCaseLink>();
            TemplateTestCaseLinkDoc = new HashSet<TemplateTestCaseLinkDoc>();
            TemplateTestCaseProposal = new HashSet<TemplateTestCaseProposal>();
            TenderInformation = new HashSet<TenderInformation>();
            TenderPeriodPricing = new HashSet<TenderPeriodPricing>();
            TenderPeriodPricingLine = new HashSet<TenderPeriodPricingLine>();
            TransferBalanceQtyDocument = new HashSet<TransferBalanceQtyDocument>();
            TransferBalanceQtyStatusCode = new HashSet<TransferBalanceQty>();
            TransferBalanceQtyTransferTime = new HashSet<TransferBalanceQty>();
            TransferSettings = new HashSet<TransferSettings>();
            UnitConversion = new HashSet<UnitConversion>();
            UserGroup = new HashSet<UserGroup>();
            VendorsList = new HashSet<VendorsList>();
            WikiAccessRight = new HashSet<WikiAccessRight>();
            WikiGroup = new HashSet<WikiGroup>();
            WikiPage = new HashSet<WikiPage>();
            WorkFlow = new HashSet<WorkFlow>();
            WorkFlowApproverSetup = new HashSet<WorkFlowApproverSetup>();
            WorkFlowInteraction = new HashSet<WorkFlowInteraction>();
            WorkFlowPageBlock = new HashSet<WorkFlowPageBlock>();
            WorkFlowPageBlockAccess = new HashSet<WorkFlowPageBlockAccess>();
            WorkFlowPageFieldAccess = new HashSet<WorkFlowPageFieldAccess>();
            WorkFlowPages = new HashSet<WorkFlowPages>();
            WorkListRequestTypeNavigation = new HashSet<WorkList>();
            WorkListWorkListStatusNavigation = new HashSet<WorkList>();
            WorkListWorkListTypeNavigation = new HashSet<WorkList>();
            WorkOrderAssignment = new HashSet<WorkOrder>();
            WorkOrderCommentUser = new HashSet<WorkOrderCommentUser>();
            WorkOrderLineCrtstatus = new HashSet<WorkOrderLine>();
            WorkOrderLinePriority = new HashSet<WorkOrderLine>();
            WorkOrderLineStatusCode = new HashSet<WorkOrderLine>();
            WorkOrderLineSunwardStatus = new HashSet<WorkOrderLine>();
            WorkOrderStatusCode = new HashSet<WorkOrder>();
        }

        public int CodeId { get; set; }
        public string CodeType { get; set; }
        public string CodeValue { get; set; }
        public string CodeDescription { get; set; }
        public long CodeMasterId { get; set; }

        public virtual ICollection<Acentry> Acentry { get; set; }
        public virtual ICollection<AcentryLines> AcentryLines { get; set; }
        public virtual ICollection<Acitems> Acitems { get; set; }
        public virtual ICollection<ActiveFlow> ActiveFlow { get; set; }
        public virtual ICollection<ActiveFlowDetails> ActiveFlowDetails { get; set; }
        public virtual ICollection<ActivityEmailTopics> ActivityEmailTopics { get; set; }
        public virtual ICollection<AdditionalProcess> AdditionalProcess { get; set; }
        public virtual ICollection<Address> Address { get; set; }
        public virtual ICollection<AppIpirentry> AppIpirentryIpiruploadTypeNavigation { get; set; }
        public virtual ICollection<AppIpirentry> AppIpirentryStatusCode { get; set; }
        public virtual ICollection<AppSampling> AppSampling { get; set; }
        public virtual ICollection<AppSamplingLine> AppSamplingLine { get; set; }
        public virtual ICollection<AppTranDocumentLine> AppTranDocumentLine { get; set; }
        public virtual ICollection<AppTranLfromLto> AppTranLfromLto { get; set; }
        public virtual ICollection<AppTranLfromLtoLine> AppTranLfromLtoLine { get; set; }
        public virtual ICollection<AppTranStock> AppTranStock { get; set; }
        public virtual ICollection<AppTranStockLine> AppTranStockLine { get; set; }
        public virtual ICollection<AppconsumptionEntry> AppconsumptionEntry { get; set; }
        public virtual ICollection<AppdispenserDispensing> AppdispenserDispensing { get; set; }
        public virtual ICollection<Appdrumming> Appdrumming { get; set; }
        public virtual ICollection<ApplicationAbbreviation> ApplicationAbbreviation { get; set; }
        public virtual ICollection<ApplicationForm> ApplicationFormClassificationType { get; set; }
        public virtual ICollection<ApplicationFormFields> ApplicationFormFields { get; set; }
        public virtual ICollection<ApplicationFormMasterPermission> ApplicationFormMasterPermission { get; set; }
        public virtual ICollection<ApplicationFormSearch> ApplicationFormSearch { get; set; }
        public virtual ICollection<ApplicationForm> ApplicationFormStatusCode { get; set; }
        public virtual ICollection<ApplicationGlossary> ApplicationGlossary { get; set; }
        public virtual ICollection<ApplicationMasterChild> ApplicationMasterChild { get; set; }
        public virtual ICollection<ApplicationMasterDetail> ApplicationMasterDetail { get; set; }
        public virtual ICollection<ApplicationMasterParent> ApplicationMasterParent { get; set; }
        public virtual ICollection<ApplicationReminder> ApplicationReminder { get; set; }
        public virtual ICollection<ApplicationRole> ApplicationRole { get; set; }
        public virtual ICollection<ApplicationUser> ApplicationUser { get; set; }
        public virtual ICollection<ApplicationWikiLine> ApplicationWikiLineCustom { get; set; }
        public virtual ICollection<ApplicationWikiLineDuty> ApplicationWikiLineDuty { get; set; }
        public virtual ICollection<ApplicationWikiLine> ApplicationWikiLineNotificationAdviceType { get; set; }
        public virtual ICollection<ApplicationWikiLine> ApplicationWikiLineRepeat { get; set; }
        public virtual ICollection<ApplicationWikiLine> ApplicationWikiLineStatusCode { get; set; }
        public virtual ICollection<ApplicationWikiRecurrence> ApplicationWikiRecurrenceOccurenceOption { get; set; }
        public virtual ICollection<ApplicationWikiRecurrence> ApplicationWikiRecurrenceStatusCode { get; set; }
        public virtual ICollection<ApplicationWikiRecurrence> ApplicationWikiRecurrenceType { get; set; }
        public virtual ICollection<ApplicationWiki> ApplicationWikiStatusCode { get; set; }
        public virtual ICollection<ApplicationWiki> ApplicationWikiTranslationRequired { get; set; }
        public virtual ICollection<ApplicationWikiWeekly> ApplicationWikiWeekly { get; set; }
        public virtual ICollection<ApplicationWiki> ApplicationWikiWikiOwnerType { get; set; }
        public virtual ICollection<AppmaterialReturn> AppmaterialReturn { get; set; }
        public virtual ICollection<ApprovalDetails> ApprovalDetails { get; set; }
        public virtual ICollection<ApproveMaster> ApproveMasterApproveLevel { get; set; }
        public virtual ICollection<ApproveMaster> ApproveMasterApproveType { get; set; }
        public virtual ICollection<ApproveMaster> ApproveMasterStatusCode { get; set; }
        public virtual ICollection<ApproverSetup> ApproverSetup { get; set; }
        public virtual ICollection<AppsupervisorDispensingEntry> AppsupervisorDispensingEntry { get; set; }
        public virtual ICollection<ApptransferRelcassEntry> ApptransferRelcassEntryStatusCode { get; set; }
        public virtual ICollection<ApptransferRelcassEntry> ApptransferRelcassEntryTransferType { get; set; }
        public virtual ICollection<AssetAssignment> AssetAssignmentAssetStatusNavigation { get; set; }
        public virtual ICollection<AssetAssignment> AssetAssignmentStatusCode { get; set; }
        public virtual ICollection<AssetCatalogMaster> AssetCatalogMaster { get; set; }
        public virtual ICollection<AssetDisposal> AssetDisposalDisposalType { get; set; }
        public virtual ICollection<AssetDisposal> AssetDisposalStatusCode { get; set; }
        public virtual ICollection<AssetEquipmentMaintenaceMaster> AssetEquipmentMaintenaceMaster { get; set; }
        public virtual ICollection<AssetMaintenance> AssetMaintenance { get; set; }
        public virtual ICollection<AssetMaster> AssetMasterDepricationMethodNavigation { get; set; }
        public virtual ICollection<AssetMaster> AssetMasterStatusCode { get; set; }
        public virtual ICollection<AssetPartsMaintenaceMaster> AssetPartsMaintenaceMaster { get; set; }
        public virtual ICollection<AssetTicket> AssetTicketImpactStatusNavigation { get; set; }
        public virtual ICollection<AssetTicket> AssetTicketPriorityStatusNavigation { get; set; }
        public virtual ICollection<AssetTicket> AssetTicketStatusCode { get; set; }
        public virtual ICollection<AssetTicket> AssetTicketUrgencyStatusNavigation { get; set; }
        public virtual ICollection<AssetTransfer> AssetTransfer { get; set; }
        public virtual ICollection<AttributeDetails> AttributeDetails { get; set; }
        public virtual ICollection<AttributeHeader> AttributeHeaderControlTypeNavigation { get; set; }
        public virtual ICollection<AttributeHeader> AttributeHeaderStatusCode { get; set; }
        public virtual ICollection<BaseUom> BaseUom { get; set; }
        public virtual ICollection<BlanketOrder> BlanketOrderExtensionInformation { get; set; }
        public virtual ICollection<BlanketOrder> BlanketOrderExtensionStatus { get; set; }
        public virtual ICollection<BlanketOrder> BlanketOrderQtyExtensionStatus { get; set; }
        public virtual ICollection<BlanketOrder> BlanketOrderStatusCode { get; set; }
        public virtual ICollection<BlanketOrder> BlanketOrderTypeOfRequest { get; set; }
        public virtual ICollection<BlisterMouldInformation> BlisterMouldInformationStatusCode { get; set; }
        public virtual ICollection<BlisterMouldInformation> BlisterMouldInformationUpperKnurlingCoverage { get; set; }
        public virtual ICollection<BlisterScrapCode> BlisterScrapCode { get; set; }
        public virtual ICollection<BlisterScrapEntry> BlisterScrapEntry { get; set; }
        public virtual ICollection<BlisterScrapMaster> BlisterScrapMaster { get; set; }
        public virtual ICollection<BmrdisposalBox> BmrdisposalBox { get; set; }
        public virtual ICollection<BmrdisposalMasterBox> BmrdisposalMasterBox { get; set; }
        public virtual ICollection<Bmrmovement> Bmrmovement { get; set; }
        public virtual ICollection<BmrmovementLine> BmrmovementLine { get; set; }
        public virtual ICollection<BmrtoCarton> BmrtoCarton { get; set; }
        public virtual ICollection<BmrtoCartonLine> BmrtoCartonLine { get; set; }
        public virtual ICollection<BompackingMaster> BompackingMasterDosageForm { get; set; }
        public virtual ICollection<BompackingMasterLine> BompackingMasterLineCalculationMethod { get; set; }
        public virtual ICollection<BompackingMasterLine> BompackingMasterLineStatusCode { get; set; }
        public virtual ICollection<BompackingMaster> BompackingMasterStatusCode { get; set; }
        public virtual ICollection<BomproductionGroup> BomproductionGroup { get; set; }
        public virtual ICollection<CalibrationServiceInfo> CalibrationServiceInfo { get; set; }
        public virtual ICollection<CalibrationType> CalibrationType { get; set; }
        public virtual ICollection<CalibrationVendorInfo> CalibrationVendorInfo { get; set; }
        public virtual ICollection<Category> Category { get; set; }
        public virtual ICollection<Ccfainformation> Ccfainformation { get; set; }
        public virtual ICollection<Ccfbevaluation> Ccfbevaluation { get; set; }
        public virtual ICollection<Ccfcapproval> Ccfcapproval { get; set; }
        public virtual ICollection<Ccfdimplementation> Ccfdimplementation { get; set; }
        public virtual ICollection<CcfdimplementationDetails> CcfdimplementationDetails { get; set; }
        public virtual ICollection<ChangeControlForm> ChangeControlForm { get; set; }
        public virtual ICollection<ChatGroup> ChatGroup { get; set; }
        public virtual ICollection<ChatUser> ChatUser { get; set; }
        public virtual ICollection<CityMaster> CityMaster { get; set; }
        public virtual ICollection<ClassificationBmrLine> ClassificationBmrLine { get; set; }
        public virtual ICollection<ClassificationBmr> ClassificationBmrProductionStatus { get; set; }
        public virtual ICollection<ClassificationBmr> ClassificationBmrStatusCode { get; set; }
        public virtual ICollection<ClassificationProduction> ClassificationProduction { get; set; }
        public virtual ICollection<CommitmentOrders> CommitmentOrders { get; set; }
        public virtual ICollection<CommitmentOrdersLine> CommitmentOrdersLine { get; set; }
        public virtual ICollection<CommonFieldsProductionMachine> CommonFieldsProductionMachine { get; set; }
        public virtual ICollection<CommonFieldsProductionMachineDocument> CommonFieldsProductionMachineDocument { get; set; }
        public virtual ICollection<CommonFieldsProductionMachineLine> CommonFieldsProductionMachineLine { get; set; }
        public virtual ICollection<CommonFieldsProductionMachineLineLine> CommonFieldsProductionMachineLineLineStatusCode { get; set; }
        public virtual ICollection<CommonFieldsProductionMachineLineLine> CommonFieldsProductionMachineLineLineStatusOfRequalification { get; set; }
        public virtual ICollection<CommonMasterMouldChangeParts> CommonMasterMouldChangePartsStatusCode { get; set; }
        public virtual ICollection<CommonMasterMouldChangeParts> CommonMasterMouldChangePartsTypeoftooling { get; set; }
        public virtual ICollection<CommonMasterToolingClassification> CommonMasterToolingClassificationStatusCode { get; set; }
        public virtual ICollection<CommonMasterToolingClassification> CommonMasterToolingClassificationToolInfo { get; set; }
        public virtual ICollection<CommonPackagingBottle> CommonPackagingBottle { get; set; }
        public virtual ICollection<CommonPackagingBottleCap> CommonPackagingBottleCap { get; set; }
        public virtual ICollection<CommonPackagingBottleInsert> CommonPackagingBottleInsert { get; set; }
        public virtual ICollection<CommonPackagingCapseal> CommonPackagingCapseal { get; set; }
        public virtual ICollection<CommonPackagingCarton> CommonPackagingCarton { get; set; }
        public virtual ICollection<CommonPackagingDivider> CommonPackagingDivider { get; set; }
        public virtual ICollection<CommonPackagingItemHeader> CommonPackagingItemHeader { get; set; }
        public virtual ICollection<CommonPackagingLayerPad> CommonPackagingLayerPad { get; set; }
        public virtual ICollection<CommonPackagingNesting> CommonPackagingNesting { get; set; }
        public virtual ICollection<CommonPackagingSetInfo> CommonPackagingSetInfo { get; set; }
        public virtual ICollection<CommonPackagingShrinkWrap> CommonPackagingShrinkWrap { get; set; }
        public virtual ICollection<CommonPackingInformation> CommonPackingInformation { get; set; }
        public virtual ICollection<CommonPackingInformationLine> CommonPackingInformationLine { get; set; }
        public virtual ICollection<CommonPackingItemListLine> CommonPackingItemListLine { get; set; }
        public virtual ICollection<CommonProcess> CommonProcess { get; set; }
        public virtual ICollection<CommonProcessLine> CommonProcessLine { get; set; }
        public virtual ICollection<CompanyCalendar> CompanyCalendar { get; set; }
        public virtual ICollection<CompanyCalendarLine> CompanyCalendarLine { get; set; }
        public virtual ICollection<CompanyCalendarLineLink> CompanyCalendarLineLink { get; set; }
        public virtual ICollection<CompanyCalendarLineLinkUserLink> CompanyCalendarLineLinkUserLink { get; set; }
        public virtual ICollection<CompanyCalendarLineMeetingNotes> CompanyCalendarLineMeetingNotes { get; set; }
        public virtual ICollection<CompanyCalendarLineNotes> CompanyCalendarLineNotes { get; set; }
        public virtual ICollection<CompanyCalendarLineNotesUser> CompanyCalendarLineNotesUser { get; set; }
        public virtual ICollection<CompanyEvents> CompanyEventsEventType { get; set; }
        public virtual ICollection<CompanyEvents> CompanyEventsPreparationType { get; set; }
        public virtual ICollection<CompanyEvents> CompanyEventsRepeatInfo { get; set; }
        public virtual ICollection<CompanyEvents> CompanyEventsStandardFrequency { get; set; }
        public virtual ICollection<CompanyEvents> CompanyEventsStatusCode { get; set; }
        public virtual ICollection<CompanyHolidayDetail> CompanyHolidayDetailAllowEarlyRelease { get; set; }
        public virtual ICollection<CompanyHolidayDetail> CompanyHolidayDetailHolidayType { get; set; }
        public virtual ICollection<CompanyHolidays> CompanyHolidays { get; set; }
        public virtual ICollection<CompanyListing> CompanyListing { get; set; }
        public virtual ICollection<CompanyListingLine> CompanyListingLine { get; set; }
        public virtual ICollection<CompanyWorkDayMaster> CompanyWorkDayMaster { get; set; }
        public virtual ICollection<Contact> Contact { get; set; }
        public virtual ICollection<ContactActivities> ContactActivities { get; set; }
        public virtual ICollection<Contacts> ContactsGenderNavigation { get; set; }
        public virtual ICollection<Contacts> ContactsMaritalStatusNavigation { get; set; }
        public virtual ICollection<Contacts> ContactsStatusCode { get; set; }
        public virtual ICollection<Contacts> ContactsTypeNavigation { get; set; }
        public virtual ICollection<ContractDistributionAttachment> ContractDistributionAttachment { get; set; }
        public virtual ICollection<ContractDistributionSalesEntryLine> ContractDistributionSalesEntryLine { get; set; }
        public virtual ICollection<Country> Country { get; set; }
        public virtual ICollection<CountryInformationStatus> CountryInformationStatus { get; set; }
        public virtual ICollection<CriticalstepandIntermediateLine> CriticalstepandIntermediateLine { get; set; }
        public virtual ICollection<CriticalstepandIntermediate> CriticalstepandIntermediateRegisterationCode { get; set; }
        public virtual ICollection<CriticalstepandIntermediate> CriticalstepandIntermediateStatusCode { get; set; }
        public virtual ICollection<CustomerAcceptanceConfirmation> CustomerAcceptanceConfirmation { get; set; }
        public virtual ICollection<Department> Department { get; set; }
        public virtual ICollection<Designation> Designation { get; set; }
        public virtual ICollection<DeviceCatalogMaster> DeviceCatalogMaster { get; set; }
        public virtual ICollection<DeviceGroupList> DeviceGroupList { get; set; }
        public virtual ICollection<DisposalItem> DisposalItem { get; set; }
        public virtual ICollection<DistStockBalance> DistStockBalance { get; set; }
        public virtual ICollection<DistributorPricing> DistributorPricing { get; set; }
        public virtual ICollection<DistributorReplenishment> DistributorReplenishment { get; set; }
        public virtual ICollection<DistributorReplenishmentLine> DistributorReplenishmentLine { get; set; }
        public virtual ICollection<Division> Division { get; set; }
        public virtual ICollection<DocumentAlert> DocumentAlert { get; set; }
        public virtual ICollection<DocumentDmsshare> DocumentDmsshare { get; set; }
        public virtual ICollection<DocumentInvitation> DocumentInvitation { get; set; }
        public virtual ICollection<DocumentLink> DocumentLink { get; set; }
        public virtual ICollection<DocumentNoSeries> DocumentNoSeries { get; set; }
        public virtual ICollection<DocumentPermission> DocumentPermission { get; set; }
        public virtual ICollection<DocumentProfile> DocumentProfile { get; set; }
        public virtual ICollection<DocumentProfileNoSeries> DocumentProfileNoSeriesLink { get; set; }
        public virtual ICollection<DocumentProfileNoSeries> DocumentProfileNoSeriesProfileType { get; set; }
        public virtual ICollection<DocumentProfileNoSeries> DocumentProfileNoSeriesStatusCode { get; set; }
        public virtual ICollection<DocumentRole> DocumentRole { get; set; }
        public virtual ICollection<DocumentRolePermission> DocumentRolePermission { get; set; }
        public virtual ICollection<DocumentShare> DocumentShare { get; set; }
        public virtual ICollection<Documents> DocumentsArchiveStatus { get; set; }
        public virtual ICollection<Documents> DocumentsCloseDocument { get; set; }
        public virtual ICollection<Documents> DocumentsStatusCode { get; set; }
        public virtual ICollection<DraftApplicationWikiLine> DraftApplicationWikiLineCustom { get; set; }
        public virtual ICollection<DraftApplicationWikiLineDuty> DraftApplicationWikiLineDuty { get; set; }
        public virtual ICollection<DraftApplicationWikiLine> DraftApplicationWikiLineNotificationAdviceType { get; set; }
        public virtual ICollection<DraftApplicationWikiLine> DraftApplicationWikiLineRepeat { get; set; }
        public virtual ICollection<DraftApplicationWikiLine> DraftApplicationWikiLineStatusCode { get; set; }
        public virtual ICollection<DraftApplicationWikiRecurrence> DraftApplicationWikiRecurrenceOccurenceOption { get; set; }
        public virtual ICollection<DraftApplicationWikiRecurrence> DraftApplicationWikiRecurrenceStatusCode { get; set; }
        public virtual ICollection<DraftApplicationWikiRecurrence> DraftApplicationWikiRecurrenceType { get; set; }
        public virtual ICollection<DraftApplicationWiki> DraftApplicationWikiStatusCode { get; set; }
        public virtual ICollection<DraftApplicationWiki> DraftApplicationWikiTranslationRequired { get; set; }
        public virtual ICollection<DraftApplicationWikiWeekly> DraftApplicationWikiWeekly { get; set; }
        public virtual ICollection<DraftApplicationWiki> DraftApplicationWikiWikiOwnerType { get; set; }
        public virtual ICollection<DynamicFlow> DynamicFlowCategory { get; set; }
        public virtual ICollection<DynamicFlowDetail> DynamicFlowDetail { get; set; }
        public virtual ICollection<DynamicFlowProgress> DynamicFlowProgressFlowTypeNavigation { get; set; }
        public virtual ICollection<DynamicFlowProgress> DynamicFlowProgressStatusCode { get; set; }
        public virtual ICollection<DynamicFlowProgress> DynamicFlowProgressStepTypeNavigation { get; set; }
        public virtual ICollection<DynamicFlow> DynamicFlowStatusCode { get; set; }
        public virtual ICollection<DynamicForm> DynamicForm { get; set; }
        public virtual ICollection<DynamicFormData> DynamicFormData { get; set; }
        public virtual ICollection<DynamicFormDataUpload> DynamicFormDataUpload { get; set; }
        public virtual ICollection<DynamicFormItemLine> DynamicFormItemLine { get; set; }
        public virtual ICollection<DynamicFormItem> DynamicFormItemStatusCode { get; set; }
        public virtual ICollection<DynamicFormItem> DynamicFormItemTransactionType { get; set; }
        public virtual ICollection<DynamicFormReport> DynamicFormReport { get; set; }
        public virtual ICollection<DynamicFormSection> DynamicFormSection { get; set; }
        public virtual ICollection<DynamicFormSectionAttribute> DynamicFormSectionAttribute { get; set; }
        public virtual ICollection<Emails> Emails { get; set; }
        public virtual ICollection<EmployeeIcthardInformation> EmployeeIcthardInformation { get; set; }
        public virtual ICollection<EmployeeIctinformation> EmployeeIctinformation { get; set; }
        public virtual ICollection<EmployeeInformation> EmployeeInformation { get; set; }
        public virtual ICollection<EmployeeLeaveInformation> EmployeeLeaveInformationNonPresence { get; set; }
        public virtual ICollection<EmployeeLeaveInformation> EmployeeLeaveInformationStatusCode { get; set; }
        public virtual ICollection<EmployeeOtherDutyInformation> EmployeeOtherDutyInformation { get; set; }
        public virtual ICollection<EmployeeResignation> EmployeeResignationApproveStatusNavigation { get; set; }
        public virtual ICollection<EmployeeResignation> EmployeeResignationResignationTypeNavigation { get; set; }
        public virtual ICollection<EmployeeSageInformation> EmployeeSageInformation { get; set; }
        public virtual ICollection<Employee> EmployeeStatusCode { get; set; }
        public virtual ICollection<Employee> EmployeeTypeOfEmployeementNavigation { get; set; }
        public virtual ICollection<ExchangeRate> ExchangeRate { get; set; }
        public virtual ICollection<ExchangeRateLine> ExchangeRateLine { get; set; }
        public virtual ICollection<ExcipientInformationLineByProcess> ExcipientInformationLineByProcess { get; set; }
        public virtual ICollection<ExtensionInformationLine> ExtensionInformationLine { get; set; }
        public virtual ICollection<FileProfileSetupForm> FileProfileSetupFormControlType { get; set; }
        public virtual ICollection<FileProfileSetupForm> FileProfileSetupFormStatusCode { get; set; }
        public virtual ICollection<FileProfileTypeDynamicForm> FileProfileTypeDynamicForm { get; set; }
        public virtual ICollection<FileProfileType> FileProfileTypeShelfLifeDurationNavigation { get; set; }
        public virtual ICollection<FileProfileType> FileProfileTypeStatusCode { get; set; }
        public virtual ICollection<FinishProdcutGeneralInfoLine> FinishProdcutGeneralInfoLine { get; set; }
        public virtual ICollection<FinishProductExcipientLine> FinishProductExcipientLine { get; set; }
        public virtual ICollection<FinishProductExcipient> FinishProductExcipientRegisterationCode { get; set; }
        public virtual ICollection<FinishProductExcipient> FinishProductExcipientStatusCode { get; set; }
        public virtual ICollection<FinishProductGeneralInfoDocument> FinishProductGeneralInfoDocument { get; set; }
        public virtual ICollection<FinishProductGeneralInfo> FinishProductGeneralInfoRegisterationCode { get; set; }
        public virtual ICollection<FinishProductGeneralInfo> FinishProductGeneralInfoStatusCode { get; set; }
        public virtual ICollection<FinishProduct> FinishProductRegisterationCode { get; set; }
        public virtual ICollection<FinishProduct> FinishProductStatusCode { get; set; }
        public virtual ICollection<FlowDistribution> FlowDistribution { get; set; }
        public virtual ICollection<FlowDistributionDelivery> FlowDistributionDelivery { get; set; }
        public virtual ICollection<FlowDistributionDetail> FlowDistributionDetail { get; set; }
        public virtual ICollection<Fmglobal> Fmglobal { get; set; }
        public virtual ICollection<FmglobalLine> FmglobalLine { get; set; }
        public virtual ICollection<FmglobalLineItem> FmglobalLineItem { get; set; }
        public virtual ICollection<FolderDiscussion> FolderDiscussion { get; set; }
        public virtual ICollection<FolderStorage> FolderStorageFolderType { get; set; }
        public virtual ICollection<FolderStorage> FolderStorageStatusCode { get; set; }
        public virtual ICollection<Folders> FoldersFolderType { get; set; }
        public virtual ICollection<Folders> FoldersStatusCode { get; set; }
        public virtual ICollection<FormTableFields> FormTableFields { get; set; }
        public virtual ICollection<FormTables> FormTables { get; set; }
        public virtual ICollection<FpcommonField> FpcommonField { get; set; }
        public virtual ICollection<FpcommonFieldLine> FpcommonFieldLine { get; set; }
        public virtual ICollection<FpdrugClassification> FpdrugClassification { get; set; }
        public virtual ICollection<FpdrugClassificationLine> FpdrugClassificationLinePurpose { get; set; }
        public virtual ICollection<FpdrugClassificationLine> FpdrugClassificationLineStatusCode { get; set; }
        public virtual ICollection<FpmanufacturingRecipe> FpmanufacturingRecipe { get; set; }
        public virtual ICollection<Fpproduct> Fpproduct { get; set; }
        public virtual ICollection<FpproductLine> FpproductLine { get; set; }
        public virtual ICollection<FpproductNavisionLine> FpproductNavisionLine { get; set; }
        public virtual ICollection<GeneralEquivalaentNavision> GeneralEquivalaentNavision { get; set; }
        public virtual ICollection<GenericCodes> GenericCodes { get; set; }
        public virtual ICollection<GenericItemNameListing> GenericItemNameListing { get; set; }
        public virtual ICollection<HandlingOfShipmentClassification> HandlingOfShipmentClassification { get; set; }
        public virtual ICollection<HistoricalDetails> HistoricalDetails { get; set; }
        public virtual ICollection<HolidayMaster> HolidayMaster { get; set; }
        public virtual ICollection<HrexternalPersonal> HrexternalPersonal { get; set; }
        public virtual ICollection<HumanMovement> HumanMovement { get; set; }
        public virtual ICollection<HumanMovementAction> HumanMovementActionHrstatusCode { get; set; }
        public virtual ICollection<HumanMovementAction> HumanMovementActionStatusCode { get; set; }
        public virtual ICollection<Icbmp> Icbmp { get; set; }
        public virtual ICollection<Icbmpline> Icbmpline { get; set; }
        public virtual ICollection<IcmasterOperation> IcmasterOperation { get; set; }
        public virtual ICollection<Ictcertificate> IctcertificateCertificateTypeNavigation { get; set; }
        public virtual ICollection<Ictcertificate> IctcertificateStatusCode { get; set; }
        public virtual ICollection<IctcontactDetails> IctcontactDetailsContactType { get; set; }
        public virtual ICollection<IctcontactDetails> IctcontactDetailsStatusCode { get; set; }
        public virtual ICollection<IctlayoutPlanTypes> IctlayoutPlanTypes { get; set; }
        public virtual ICollection<IctmasterDocument> IctmasterDocument { get; set; }
        public virtual ICollection<Ictmaster> IctmasterMasterTypeNavigation { get; set; }
        public virtual ICollection<Ictmaster> IctmasterStatusCode { get; set; }
        public virtual ICollection<InstructionType> InstructionType { get; set; }
        public virtual ICollection<InterCompanyPurchaseOrder> InterCompanyPurchaseOrder { get; set; }
        public virtual ICollection<InterCompanyPurchaseOrderLine> InterCompanyPurchaseOrderLine { get; set; }
        public virtual ICollection<InventoryType> InventoryTypeItemSource { get; set; }
        public virtual ICollection<InventoryTypeOpeningStockBalance> InventoryTypeOpeningStockBalance { get; set; }
        public virtual ICollection<InventoryTypeOpeningStockBalanceLine> InventoryTypeOpeningStockBalanceLine { get; set; }
        public virtual ICollection<InventoryType> InventoryTypeStatusCode { get; set; }
        public virtual ICollection<Ipir> Ipir { get; set; }
        public virtual ICollection<IpirApp> IpirApp { get; set; }
        public virtual ICollection<IpirAppCheckedDetails> IpirAppCheckedDetailsActivityInfo { get; set; }
        public virtual ICollection<IpirAppCheckedDetails> IpirAppCheckedDetailsStatusCode { get; set; }
        public virtual ICollection<IpirReport> IpirReport { get; set; }
        public virtual ICollection<IpirReportAssignment> IpirReportAssignmentIntendAction { get; set; }
        public virtual ICollection<IpirReportAssignment> IpirReportAssignmentStatusCode { get; set; }
        public virtual ICollection<Ipirline> Ipirline { get; set; }
        public virtual ICollection<IpirlineDocument> IpirlineDocument { get; set; }
        public virtual ICollection<IpirmobileAction> IpirmobileActionAssignmentStatus { get; set; }
        public virtual ICollection<IpirmobileAction> IpirmobileActionIntendedAction { get; set; }
        public virtual ICollection<IpirmobileAction> IpirmobileActionStatusCode { get; set; }
        public virtual ICollection<Ipirmobile> IpirmobileIntendedAction { get; set; }
        public virtual ICollection<Ipirmobile> IpirmobileStatusCode { get; set; }
        public virtual ICollection<IssueReportIpir> IssueReportIpir { get; set; }
        public virtual ICollection<ItemBatchInfo> ItemBatchInfo { get; set; }
        public virtual ICollection<ItemClassificationHeader> ItemClassificationHeader { get; set; }
        public virtual ICollection<ItemClassificationMaster> ItemClassificationMasterClassificationType { get; set; }
        public virtual ICollection<ItemClassificationMaster> ItemClassificationMasterStatusCode { get; set; }
        public virtual ICollection<ItemClassificationTransport> ItemClassificationTransport { get; set; }
        public virtual ICollection<ItemCost> ItemCost { get; set; }
        public virtual ICollection<ItemCostLine> ItemCostLine { get; set; }
        public virtual ICollection<ItemManufacture> ItemManufacture { get; set; }
        public virtual ICollection<ItemMaster> ItemMaster { get; set; }
        public virtual ICollection<ItemSalesPrice> ItemSalesPrice { get; set; }
        public virtual ICollection<ItemStockInfo> ItemStockInfo { get; set; }
        public virtual ICollection<JobProgressTemplateLineLineProcess> JobProgressTemplateLineLineProcess { get; set; }
        public virtual ICollection<JobProgressTemplateLineProcess> JobProgressTemplateLineProcess { get; set; }
        public virtual ICollection<JobProgressTemplateLineProcessStatus> JobProgressTemplateLineProcessStatus { get; set; }
        public virtual ICollection<JobProgressTemplateRecurrence> JobProgressTemplateRecurrenceOccurenceOption { get; set; }
        public virtual ICollection<JobProgressTemplateRecurrence> JobProgressTemplateRecurrenceStatusCode { get; set; }
        public virtual ICollection<JobProgressTemplateRecurrence> JobProgressTemplateRecurrenceType { get; set; }
        public virtual ICollection<JobProgressTemplate> JobProgressTemplateSoftware { get; set; }
        public virtual ICollection<JobProgressTemplate> JobProgressTemplateStatusCode { get; set; }
        public virtual ICollection<JobProgressTempletateLine> JobProgressTempletateLine { get; set; }
        public virtual ICollection<JobSchedule> JobScheduleFrequency { get; set; }
        public virtual ICollection<JobSchedule> JobScheduleStatusCode { get; set; }
        public virtual ICollection<JobScheduleWeekly> JobScheduleWeekly { get; set; }
        public virtual ICollection<LayoutPlanType> LayoutPlanType { get; set; }
        public virtual ICollection<LevelMaster> LevelMaster { get; set; }
        public virtual ICollection<LocalClinic> LocalClinicCompanyStatus { get; set; }
        public virtual ICollection<LocalClinic> LocalClinicToSwstatus { get; set; }
        public virtual ICollection<Locations> Locations { get; set; }
        public virtual ICollection<MachineTimeManHourInfo> MachineTimeManHourInfoIntermittentCleaningAtNavigation { get; set; }
        public virtual ICollection<MachineTimeManHourInfo> MachineTimeManHourInfoLevel1CleaningNavigation { get; set; }
        public virtual ICollection<MachineTimeManHourInfo> MachineTimeManHourInfoLevel2CleaningNavigation { get; set; }
        public virtual ICollection<MachineTimeManHourInfo> MachineTimeManHourInfoNextProdItemMaxTimeNavigation { get; set; }
        public virtual ICollection<MachineTimeManHourInfo> MachineTimeManHourInfoNextProdItemMinTimeNavigation { get; set; }
        public virtual ICollection<MachineTimeManHourInfo> MachineTimeManHourInfoNextStepMaxTimeGapNavigation { get; set; }
        public virtual ICollection<MachineTimeManHourInfo> MachineTimeManHourInfoNextStepMinTimeGapNavigation { get; set; }
        public virtual ICollection<MachineTimeManHourInfo> MachineTimeManHourInfoPreparationTimeNavigation { get; set; }
        public virtual ICollection<MachineTimeManHourInfo> MachineTimeManHourInfoProductionTimeNavigation { get; set; }
        public virtual ICollection<MachineTimeManHourInfo> MachineTimeManHourInfoStatusCode { get; set; }
        public virtual ICollection<MachineTimeManHourInfo> MachineTimeManHourInfoTypeOfFeed { get; set; }
        public virtual ICollection<ManpowerInformation> ManpowerInformation { get; set; }
        public virtual ICollection<ManpowerInformationLine> ManpowerInformationLine { get; set; }
        public virtual ICollection<ManufacturingProcessLine> ManufacturingProcessLine { get; set; }
        public virtual ICollection<ManufacturingProcess> ManufacturingProcessRegisterationCode { get; set; }
        public virtual ICollection<ManufacturingProcess> ManufacturingProcessStatusCode { get; set; }
        public virtual ICollection<MarginInformation> MarginInformation { get; set; }
        public virtual ICollection<MarginInformationLine> MarginInformationLine { get; set; }
        public virtual ICollection<MasterBlanketOrder> MasterBlanketOrder { get; set; }
        public virtual ICollection<MasterBlanketOrderLine> MasterBlanketOrderLine { get; set; }
        public virtual ICollection<MasterDocumentInformation> MasterDocumentInformation { get; set; }
        public virtual ICollection<MasterForm> MasterForm { get; set; }
        public virtual ICollection<MasterFormDetail> MasterFormDetail { get; set; }
        public virtual ICollection<MasterInterCompanyPricing> MasterInterCompanyPricing { get; set; }
        public virtual ICollection<MasterInterCompanyPricingLine> MasterInterCompanyPricingLine { get; set; }
        public virtual ICollection<MedCatalogClassification> MedCatalogClassification { get; set; }
        public virtual ICollection<MedMaster> MedMaster { get; set; }
        public virtual ICollection<MedMasterLine> MedMasterLineDedicatedUsage { get; set; }
        public virtual ICollection<MedMasterLine> MedMasterLineStatusCode { get; set; }
        public virtual ICollection<Memo> Memo { get; set; }
        public virtual ICollection<MethodTemplateRoutineLine> MethodTemplateRoutineLine { get; set; }
        public virtual ICollection<MethodTemplateRoutine> MethodTemplateRoutineManufacturingType { get; set; }
        public virtual ICollection<MethodTemplateRoutine> MethodTemplateRoutineStatusCode { get; set; }
        public virtual ICollection<MobileShift> MobileShift { get; set; }
        public virtual ICollection<Modules> Modules { get; set; }
        public virtual ICollection<NavItemBatchNo> NavItemBatchNo { get; set; }
        public virtual ICollection<NavManufacturingProcess> NavManufacturingProcess { get; set; }
        public virtual ICollection<NavMethodCode> NavMethodCode { get; set; }
        public virtual ICollection<NavMethodCodeLines> NavMethodCodeLines { get; set; }
        public virtual ICollection<NavPackingMethod> NavPackingMethod { get; set; }
        public virtual ICollection<NavProductMaster> NavProductMasterProductCreationTypeNavigation { get; set; }
        public virtual ICollection<NavProductMaster> NavProductMasterRegistrationReference { get; set; }
        public virtual ICollection<NavProductMaster> NavProductMasterStatusCode { get; set; }
        public virtual ICollection<NavProductMaster> NavProductMasterTypeOfCoatingNavigation { get; set; }
        public virtual ICollection<NavSaleCategory> NavSaleCategory { get; set; }
        public virtual ICollection<NavbaseUnit> NavbaseUnit { get; set; }
        public virtual ICollection<Navcustomer> Navcustomer { get; set; }
        public virtual ICollection<NavisionCompany> NavisionCompany { get; set; }
        public virtual ICollection<NavisionSpecification> NavisionSpecification { get; set; }
        public virtual ICollection<NavitemLinks> NavitemLinks { get; set; }
        public virtual ICollection<NavitemStockBalance> NavitemStockBalance { get; set; }
        public virtual ICollection<Navitems> Navitems { get; set; }
        public virtual ICollection<Navlocation> Navlocation { get; set; }
        public virtual ICollection<NavplannedProdOrder> NavplannedProdOrder { get; set; }
        public virtual ICollection<NavpostedShipment> NavpostedShipment { get; set; }
        public virtual ICollection<NavproductCode> NavproductCode { get; set; }
        public virtual ICollection<Navrecipes> Navrecipes { get; set; }
        public virtual ICollection<Navsettings> Navsettings { get; set; }
        public virtual ICollection<NavstockBalanace> NavstockBalanace { get; set; }
        public virtual ICollection<Navvendor> Navvendor { get; set; }
        public virtual ICollection<NonDeliverSo> NonDeliverSo { get; set; }
        public virtual ICollection<Notes> Notes { get; set; }
        public virtual ICollection<NoticeLine> NoticeLineFrequency { get; set; }
        public virtual ICollection<NoticeLine> NoticeLineModule { get; set; }
        public virtual ICollection<NoticeLine> NoticeLineStatusCode { get; set; }
        public virtual ICollection<NoticeLineWeekly> NoticeLineWeekly { get; set; }
        public virtual ICollection<Notice> NoticeModule { get; set; }
        public virtual ICollection<Notice> NoticeStatusCode { get; set; }
        public virtual ICollection<Notification> Notification { get; set; }
        public virtual ICollection<NotificationHandler> NotificationHandler { get; set; }
        public virtual ICollection<NotifyDocument> NotifyDocument { get; set; }
        public virtual ICollection<NovateInformationLine> NovateInformationLine { get; set; }
        public virtual ICollection<NpraformulationActiveIngredient> NpraformulationActiveIngredient { get; set; }
        public virtual ICollection<NpraformulationExcipient> NpraformulationExcipient { get; set; }
        public virtual ICollection<Npraformulation> NpraformulationRegistrationReference { get; set; }
        public virtual ICollection<Npraformulation> NpraformulationStatusCode { get; set; }
        public virtual ICollection<OpenAccessUser> OpenAccessUser { get; set; }
        public virtual ICollection<OperationProcedure> OperationProcedure { get; set; }
        public virtual ICollection<OperationSequence> OperationSequence { get; set; }
        public virtual ICollection<OrderRequirement> OrderRequirement { get; set; }
        public virtual ICollection<OrderRequirementGrouPinglineSplit> OrderRequirementGrouPinglineSplit { get; set; }
        public virtual ICollection<OrderRequirementGroupingLine> OrderRequirementGroupingLine { get; set; }
        public virtual ICollection<OrderRequirementLine> OrderRequirementLine { get; set; }
        public virtual ICollection<OrderRequirementLineSplit> OrderRequirementLineSplit { get; set; }
        public virtual ICollection<PackagingAluminiumFoil> PackagingAluminiumFoilAluminiumFoilType { get; set; }
        public virtual ICollection<PackagingAluminiumFoilLine> PackagingAluminiumFoilLine { get; set; }
        public virtual ICollection<PackagingAluminiumFoil> PackagingAluminiumFoilStatusCode { get; set; }
        public virtual ICollection<PackagingHistoryItemLineDocument> PackagingHistoryItemLineDocument { get; set; }
        public virtual ICollection<PackagingItemHistory> PackagingItemHistory { get; set; }
        public virtual ICollection<PackagingItemHistoryLine> PackagingItemHistoryLine { get; set; }
        public virtual ICollection<PackagingLabel> PackagingLabelLabelType { get; set; }
        public virtual ICollection<PackagingLabel> PackagingLabelStatusCode { get; set; }
        public virtual ICollection<PackagingLeaflet> PackagingLeafletLeafletType { get; set; }
        public virtual ICollection<PackagingLeaflet> PackagingLeafletPrintedOn { get; set; }
        public virtual ICollection<PackagingLeaflet> PackagingLeafletStatusCode { get; set; }
        public virtual ICollection<PackagingPvcfoil> PackagingPvcfoilPvcFoilType { get; set; }
        public virtual ICollection<PackagingPvcfoil> PackagingPvcfoilStatusCode { get; set; }
        public virtual ICollection<PackagingTube> PackagingTubeStatusCode { get; set; }
        public virtual ICollection<PackagingTube> PackagingTubeTubeType { get; set; }
        public virtual ICollection<PackagingUnitBox> PackagingUnitBoxFinishing { get; set; }
        public virtual ICollection<PackagingUnitBox> PackagingUnitBoxStatusCode { get; set; }
        public virtual ICollection<PackagingUnitBox> PackagingUnitBoxUnitBoxType { get; set; }
        public virtual ICollection<PacketInformationLine> PacketInformationLine { get; set; }
        public virtual ICollection<PageApproval> PageApproval { get; set; }
        public virtual ICollection<PageLinkCondition> PageLinkCondition { get; set; }
        public virtual ICollection<PerUnitFormulationLine> PerUnitFormulationLine { get; set; }
        public virtual ICollection<PhramacologicalProperties> PhramacologicalPropertiesRegisterationCode { get; set; }
        public virtual ICollection<PhramacologicalProperties> PhramacologicalPropertiesStatusCode { get; set; }
        public virtual ICollection<PkgapprovalInformation> PkgapprovalInformation { get; set; }
        public virtual ICollection<PkginformationByPackSize> PkginformationByPackSize { get; set; }
        public virtual ICollection<PkgregisteredPackingInformation> PkgregisteredPackingInformation { get; set; }
        public virtual ICollection<Plant> Plant { get; set; }
        public virtual ICollection<PlantMaintenanceEntry> PlantMaintenanceEntry { get; set; }
        public virtual ICollection<PlantMaintenanceEntryMaster> PlantMaintenanceEntryMasterMasterTypeNavigation { get; set; }
        public virtual ICollection<PlantMaintenanceEntryMaster> PlantMaintenanceEntryMasterStatusCode { get; set; }
        public virtual ICollection<PlasticBag> PlasticBag { get; set; }
        public virtual ICollection<Portfolio> Portfolio { get; set; }
        public virtual ICollection<PortfolioLine> PortfolioLine { get; set; }
        public virtual ICollection<ProcessMachineTime> ProcessMachineTime { get; set; }
        public virtual ICollection<ProcessMachineTimeProductionLine> ProcessMachineTimeProductionLine { get; set; }
        public virtual ICollection<ProcessMachineTimeProductionLineLine> ProcessMachineTimeProductionLineLine { get; set; }
        public virtual ICollection<ProcessMachineTimeProductionMachineProcess> ProcessMachineTimeProductionMachineProcess { get; set; }
        public virtual ICollection<ProcessManPowerSpecialNotes> ProcessManPowerSpecialNotes { get; set; }
        public virtual ICollection<ProcessTransfer> ProcessTransfer { get; set; }
        public virtual ICollection<ProcessTransferLine> ProcessTransferLine { get; set; }
        public virtual ICollection<ProdRecipeCycle> ProdRecipeCycle { get; set; }
        public virtual ICollection<ProductActivityCaseCompany> ProductActivityCaseCompany { get; set; }
        public virtual ICollection<ProductActivityCaseLine> ProductActivityCaseLine { get; set; }
        public virtual ICollection<ProductActivityCase> ProductActivityCaseProductionType { get; set; }
        public virtual ICollection<ProductActivityCaseRespons> ProductActivityCaseResponsCustom { get; set; }
        public virtual ICollection<ProductActivityCaseResponsDuty> ProductActivityCaseResponsDuty { get; set; }
        public virtual ICollection<ProductActivityCaseRespons> ProductActivityCaseResponsNotificationAdviceType { get; set; }
        public virtual ICollection<ProductActivityCaseResponsRecurrence> ProductActivityCaseResponsRecurrenceOccurenceOption { get; set; }
        public virtual ICollection<ProductActivityCaseResponsRecurrence> ProductActivityCaseResponsRecurrenceStatusCode { get; set; }
        public virtual ICollection<ProductActivityCaseResponsRecurrence> ProductActivityCaseResponsRecurrenceType { get; set; }
        public virtual ICollection<ProductActivityCaseRespons> ProductActivityCaseResponsRepeat { get; set; }
        public virtual ICollection<ProductActivityCaseRespons> ProductActivityCaseResponsStatusCode { get; set; }
        public virtual ICollection<ProductActivityCaseResponsWeekly> ProductActivityCaseResponsWeekly { get; set; }
        public virtual ICollection<ProductActivityCase> ProductActivityCaseStatusCode { get; set; }
        public virtual ICollection<ProductActivityPermission> ProductActivityPermission { get; set; }
        public virtual ICollection<ProductGroupSurvey> ProductGroupSurvey { get; set; }
        public virtual ICollection<ProductGroupSurveyLine> ProductGroupSurveyLine { get; set; }
        public virtual ICollection<ProductGrouping> ProductGrouping { get; set; }
        public virtual ICollection<ProductGroupingManufacture> ProductGroupingManufacture { get; set; }
        public virtual ICollection<ProductGroupingNav> ProductGroupingNav { get; set; }
        public virtual ICollection<ProductGroupingNavDifference> ProductGroupingNavDifference { get; set; }
        public virtual ICollection<ProductRecipeDocument> ProductRecipeDocument { get; set; }
        public virtual ICollection<ProductionAction> ProductionAction { get; set; }
        public virtual ICollection<ProductionActivity> ProductionActivity { get; set; }
        public virtual ICollection<ProductionActivityApp> ProductionActivityApp { get; set; }
        public virtual ICollection<ProductionActivityAppLine> ProductionActivityAppLine { get; set; }
        public virtual ICollection<ProductionActivityCheckedDetails> ProductionActivityCheckedDetailsActivityInfo { get; set; }
        public virtual ICollection<ProductionActivityCheckedDetails> ProductionActivityCheckedDetailsStatusCode { get; set; }
        public virtual ICollection<ProductionActivityClassification> ProductionActivityClassification { get; set; }
        public virtual ICollection<ProductionActivityMasterLine> ProductionActivityMasterLine { get; set; }
        public virtual ICollection<ProductionActivityMaster> ProductionActivityMasterProductionType { get; set; }
        public virtual ICollection<ProductionActivityMasterRespons> ProductionActivityMasterResponsCustom { get; set; }
        public virtual ICollection<ProductionActivityMasterRespons> ProductionActivityMasterResponsNotificationAdviceType { get; set; }
        public virtual ICollection<ProductionActivityMasterResponsRecurrence> ProductionActivityMasterResponsRecurrenceOccurenceOption { get; set; }
        public virtual ICollection<ProductionActivityMasterResponsRecurrence> ProductionActivityMasterResponsRecurrenceStatusCode { get; set; }
        public virtual ICollection<ProductionActivityMasterResponsRecurrence> ProductionActivityMasterResponsRecurrenceType { get; set; }
        public virtual ICollection<ProductionActivityMasterRespons> ProductionActivityMasterResponsRepeat { get; set; }
        public virtual ICollection<ProductionActivityMasterRespons> ProductionActivityMasterResponsStatusCode { get; set; }
        public virtual ICollection<ProductionActivityMasterResponsWeekly> ProductionActivityMasterResponsWeekly { get; set; }
        public virtual ICollection<ProductionActivityMaster> ProductionActivityMasterStatusCode { get; set; }
        public virtual ICollection<ProductionActivityNonCompliance> ProductionActivityNonCompliance { get; set; }
        public virtual ICollection<ProductionActivityPlanningApp> ProductionActivityPlanningApp { get; set; }
        public virtual ICollection<ProductionActivityPlanningAppLine> ProductionActivityPlanningAppLine { get; set; }
        public virtual ICollection<ProductionActivityRoutineApp> ProductionActivityRoutineApp { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLine> ProductionActivityRoutineAppLine { get; set; }
        public virtual ICollection<ProductionActivityRoutineCheckedDetails> ProductionActivityRoutineCheckedDetailsActivityInfo { get; set; }
        public virtual ICollection<ProductionActivityRoutineCheckedDetails> ProductionActivityRoutineCheckedDetailsStatusCode { get; set; }
        public virtual ICollection<ProductionBatchInformation> ProductionBatchInformation { get; set; }
        public virtual ICollection<ProductionBatchInformationLine> ProductionBatchInformationLine { get; set; }
        public virtual ICollection<ProductionBatchSize> ProductionBatchSize { get; set; }
        public virtual ICollection<ProductionCapsule> ProductionCapsule { get; set; }
        public virtual ICollection<ProductionCapsuleLine> ProductionCapsuleLine { get; set; }
        public virtual ICollection<ProductionColor> ProductionColor { get; set; }
        public virtual ICollection<ProductionColorLine> ProductionColorLine { get; set; }
        public virtual ICollection<ProductionCycle> ProductionCycle { get; set; }
        public virtual ICollection<ProductionDocument> ProductionDocument { get; set; }
        public virtual ICollection<ProductionEntry> ProductionEntry { get; set; }
        public virtual ICollection<ProductionFlavour> ProductionFlavour { get; set; }
        public virtual ICollection<ProductionFlavourLine> ProductionFlavourLine { get; set; }
        public virtual ICollection<ProductionForecast> ProductionForecast { get; set; }
        public virtual ICollection<ProductionMachine> ProductionMachine { get; set; }
        public virtual ICollection<ProductionMaterial> ProductionMaterial { get; set; }
        public virtual ICollection<ProductionMaterialLine> ProductionMaterialLineDensityNavigation { get; set; }
        public virtual ICollection<ProductionMaterialLine> ProductionMaterialLineMicronisedOrMesh { get; set; }
        public virtual ICollection<ProductionMaterialLine> ProductionMaterialLineSaltOrBase { get; set; }
        public virtual ICollection<ProductionMethodTemplate> ProductionMethodTemplate { get; set; }
        public virtual ICollection<ProductionOrder> ProductionOrder { get; set; }
        public virtual ICollection<ProductionOrderMaster> ProductionOrderMaster { get; set; }
        public virtual ICollection<ProductionOrderSplit> ProductionOrderSplit { get; set; }
        public virtual ICollection<ProductionOutput> ProductionOutput { get; set; }
        public virtual ICollection<ProductionSimulationGrouping> ProductionSimulationGrouping { get; set; }
        public virtual ICollection<ProductionSimulationGroupingTicket> ProductionSimulationGroupingTicket { get; set; }
        public virtual ICollection<ProductionTicket> ProductionTicket { get; set; }
        public virtual ICollection<ProjectedDeliveryBlanketOthers> ProjectedDeliveryBlanketOthers { get; set; }
        public virtual ICollection<ProjectedDeliverySalesOrderLine> ProjectedDeliverySalesOrderLine { get; set; }
        public virtual ICollection<PsbreportCommentDetail> PsbreportCommentDetail { get; set; }
        public virtual ICollection<PurchaseItemSalesEntryLine> PurchaseItemSalesEntryLine { get; set; }
        public virtual ICollection<PurchaseOrder> PurchaseOrder { get; set; }
        public virtual ICollection<PurchaseOrderDocument> PurchaseOrderDocument { get; set; }
        public virtual ICollection<Qcapproval> Qcapproval { get; set; }
        public virtual ICollection<QcapprovalLine> QcapprovalLine { get; set; }
        public virtual ICollection<QuotationAward> QuotationAward { get; set; }
        public virtual ICollection<QuotationAwardLine> QuotationAwardLine { get; set; }
        public virtual ICollection<QuotationCategory> QuotationCategory { get; set; }
        public virtual ICollection<QuotationHeader> QuotationHeaderCategory { get; set; }
        public virtual ICollection<QuotationHeader> QuotationHeaderCustomerType { get; set; }
        public virtual ICollection<QuotationHeader> QuotationHeaderShipment { get; set; }
        public virtual ICollection<QuotationHistory> QuotationHistory { get; set; }
        public virtual ICollection<QuotationHistoryDocument> QuotationHistoryDocument { get; set; }
        public virtual ICollection<QuotationHistoryLine> QuotationHistoryLine { get; set; }
        public virtual ICollection<QuotationLine> QuotationLine { get; set; }
        public virtual ICollection<ReAssignmentJob> ReAssignmentJob { get; set; }
        public virtual ICollection<ReceiveEmail> ReceiveEmail { get; set; }
        public virtual ICollection<RecordVariationLine> RecordVariationLine { get; set; }
        public virtual ICollection<RecordVariation> RecordVariationRegisterationCode { get; set; }
        public virtual ICollection<RecordVariation> RecordVariationStatusCode { get; set; }
        public virtual ICollection<RecordVariation> RecordVariationSubmissionStatus { get; set; }
        public virtual ICollection<RegistrationFormatInformation> RegistrationFormatInformation { get; set; }
        public virtual ICollection<RegistrationFormatInformationLine> RegistrationFormatInformationLineFieldType { get; set; }
        public virtual ICollection<RegistrationFormatInformationLine> RegistrationFormatInformationLineStatusCode { get; set; }
        public virtual ICollection<RegistrationFormatInformationLineUsers> RegistrationFormatInformationLineUsers { get; set; }
        public virtual ICollection<RegistrationVariation> RegistrationVariationRegisterationCode { get; set; }
        public virtual ICollection<RegistrationVariation> RegistrationVariationStatustCode { get; set; }
        public virtual ICollection<RequestAc> RequestAc { get; set; }
        public virtual ICollection<RequestAcline> RequestAcline { get; set; }
        public virtual ICollection<RoutineInfo> RoutineInfo { get; set; }
        public virtual ICollection<RoutineInfoMultiple> RoutineInfoMultiple { get; set; }
        public virtual ICollection<SalesAdhocNovate> SalesAdhocNovate { get; set; }
        public virtual ICollection<SalesAdhocNovateAttachment> SalesAdhocNovateAttachment { get; set; }
        public virtual ICollection<SalesBorrowHeader> SalesBorrowHeader { get; set; }
        public virtual ICollection<SalesBorrowLine> SalesBorrowLine { get; set; }
        public virtual ICollection<SalesItemPackingInfo> SalesItemPackingInfo { get; set; }
        public virtual ICollection<SalesItemPackingInforLine> SalesItemPackingInforLine { get; set; }
        public virtual ICollection<SalesMainPromotionList> SalesMainPromotionList { get; set; }
        public virtual ICollection<SalesOrder> SalesOrder { get; set; }
        public virtual ICollection<SalesOrderAttachment> SalesOrderAttachment { get; set; }
        public virtual ICollection<SalesOrderEntry> SalesOrderEntryOrderBy { get; set; }
        public virtual ICollection<SalesOrderEntry> SalesOrderEntryStatusCode { get; set; }
        public virtual ICollection<SalesOrderEntry> SalesOrderEntryTypeOfOrder { get; set; }
        public virtual ICollection<SalesOrderEntry> SalesOrderEntryTypeOfSalesOrder { get; set; }
        public virtual ICollection<SalesOrderMasterPricing> SalesOrderMasterPricing { get; set; }
        public virtual ICollection<SalesOrderMasterPricingLine> SalesOrderMasterPricingLine { get; set; }
        public virtual ICollection<SalesOrderProduct> SalesOrderProduct { get; set; }
        public virtual ICollection<SalesOrderProfile> SalesOrderProfileOrderBy { get; set; }
        public virtual ICollection<SalesOrderProfile> SalesOrderProfileStatusCode { get; set; }
        public virtual ICollection<SalesPromotion> SalesPromotion { get; set; }
        public virtual ICollection<SalesSurveyByFieldForce> SalesSurveyByFieldForce { get; set; }
        public virtual ICollection<SalesSurveyByFieldForceLine> SalesSurveyByFieldForceLine { get; set; }
        public virtual ICollection<SalesTargetByStaff> SalesTargetByStaff { get; set; }
        public virtual ICollection<SalesTargetByStaffLine> SalesTargetByStaffLine { get; set; }
        public virtual ICollection<SampleRequestForDoctor> SampleRequestForDoctor { get; set; }
        public virtual ICollection<SampleRequestForDoctorHeader> SampleRequestForDoctorHeader { get; set; }
        public virtual ICollection<SampleRequestForm> SampleRequestForm { get; set; }
        public virtual ICollection<SampleRequestFormLine> SampleRequestFormLineIssueStatus { get; set; }
        public virtual ICollection<SampleRequestFormLine> SampleRequestFormLineStatusCode { get; set; }
        public virtual ICollection<ScheduleOfDelivery> ScheduleOfDeliveryDayOfTheWeek { get; set; }
        public virtual ICollection<ScheduleOfDeliveryPlanOfWeek> ScheduleOfDeliveryPlanOfWeek { get; set; }
        public virtual ICollection<ScheduleOfDelivery> ScheduleOfDeliveryStatusCode { get; set; }
        public virtual ICollection<Section> Section { get; set; }
        public virtual ICollection<SelfTest> SelfTest { get; set; }
        public virtual ICollection<SellingCatalogue> SellingCatalogue { get; set; }
        public virtual ICollection<SellingPriceInformation> SellingPriceInformation { get; set; }
        public virtual ICollection<ShiftMaster> ShiftMaster { get; set; }
        public virtual ICollection<ShortCut> ShortCut { get; set; }
        public virtual ICollection<Smecomment> Smecomment { get; set; }
        public virtual ICollection<SoSalesOrder> SoSalesOrder { get; set; }
        public virtual ICollection<SoSalesOrderLine> SoSalesOrderLine { get; set; }
        public virtual ICollection<SobyCustomerSunwardEquivalent> SobyCustomerSunwardEquivalent { get; set; }
        public virtual ICollection<SobyCustomersAddress> SobyCustomersAddress { get; set; }
        public virtual ICollection<SobyCustomers> SobyCustomersBillingAddressStatus { get; set; }
        public virtual ICollection<SobyCustomersManner> SobyCustomersManner { get; set; }
        public virtual ICollection<SobyCustomersMasterAddress> SobyCustomersMasterAddressAddressType { get; set; }
        public virtual ICollection<SobyCustomersMasterAddress> SobyCustomersMasterAddressStatusCode { get; set; }
        public virtual ICollection<SobyCustomers> SobyCustomersStatusCode { get; set; }
        public virtual ICollection<SocustomersDelivery> SocustomersDelivery { get; set; }
        public virtual ICollection<SocustomersIssue> SocustomersIssue { get; set; }
        public virtual ICollection<SocustomersItemCrossReference> SocustomersItemCrossReference { get; set; }
        public virtual ICollection<SocustomersSalesInfomation> SocustomersSalesInfomation { get; set; }
        public virtual ICollection<SolotInformation> SolotInformation { get; set; }
        public virtual ICollection<SourceList> SourceList { get; set; }
        public virtual ICollection<SowithOutBlanketOrder> SowithOutBlanketOrder { get; set; }
        public virtual ICollection<StandardManufacturingProcess> StandardManufacturingProcess { get; set; }
        public virtual ICollection<StandardManufacturingProcessLine> StandardManufacturingProcessLine { get; set; }
        public virtual ICollection<StandardProcedure> StandardProcedure { get; set; }
        public virtual ICollection<StartOfDay> StartOfDay { get; set; }
        public virtual ICollection<State> State { get; set; }
        public virtual ICollection<SubSection> SubSection { get; set; }
        public virtual ICollection<SubSectionTwo> SubSectionTwo { get; set; }
        public virtual ICollection<SunwardAssetList> SunwardAssetListCalibrationStatusNavigation { get; set; }
        public virtual ICollection<SunwardAssetList> SunwardAssetListDeviceStatus { get; set; }
        public virtual ICollection<SunwardAssetList> SunwardAssetListStatusCode { get; set; }
        public virtual ICollection<SunwardGroupCompany> SunwardGroupCompany { get; set; }
        public virtual ICollection<TagMaster> TagMaster { get; set; }
        public virtual ICollection<TaskAppointment> TaskAppointment { get; set; }
        public virtual ICollection<TaskAssigned> TaskAssigned { get; set; }
        public virtual ICollection<TaskCommentUser> TaskCommentUser { get; set; }
        public virtual ICollection<TaskDiscussion> TaskDiscussion { get; set; }
        public virtual ICollection<TaskInviteUser> TaskInviteUser { get; set; }
        public virtual ICollection<TaskMasterDescriptionVersion> TaskMasterDescriptionVersion { get; set; }
        public virtual ICollection<TaskMaster> TaskMasterRequestType { get; set; }
        public virtual ICollection<TaskMaster> TaskMasterStatusCode { get; set; }
        public virtual ICollection<TaskMaster> TaskMasterWorkType { get; set; }
        public virtual ICollection<TeamMaster> TeamMaster { get; set; }
        public virtual ICollection<TempSalesPackInformation> TempSalesPackInformation { get; set; }
        public virtual ICollection<TempSalesPackInformationFactor> TempSalesPackInformationFactor { get; set; }
        public virtual ICollection<TempVersion> TempVersion { get; set; }
        public virtual ICollection<TemplateCaseFormNotes> TemplateCaseFormNotes { get; set; }
        public virtual ICollection<TemplateTestCase> TemplateTestCase { get; set; }
        public virtual ICollection<TemplateTestCaseCheckList> TemplateTestCaseCheckList { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponse> TemplateTestCaseCheckListResponseCustom { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponseDuty> TemplateTestCaseCheckListResponseDuty { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponse> TemplateTestCaseCheckListResponseNotificationAdviceType { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponseRecurrence> TemplateTestCaseCheckListResponseRecurrenceOccurenceOption { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponseRecurrence> TemplateTestCaseCheckListResponseRecurrenceStatusCode { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponseRecurrence> TemplateTestCaseCheckListResponseRecurrenceType { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponse> TemplateTestCaseCheckListResponseRepeat { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponse> TemplateTestCaseCheckListResponseStatusCode { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponseWeekly> TemplateTestCaseCheckListResponseWeekly { get; set; }
        public virtual ICollection<TemplateTestCaseForm> TemplateTestCaseForm { get; set; }
        public virtual ICollection<TemplateTestCaseLink> TemplateTestCaseLink { get; set; }
        public virtual ICollection<TemplateTestCaseLinkDoc> TemplateTestCaseLinkDoc { get; set; }
        public virtual ICollection<TemplateTestCaseProposal> TemplateTestCaseProposal { get; set; }
        public virtual ICollection<TenderInformation> TenderInformation { get; set; }
        public virtual ICollection<TenderPeriodPricing> TenderPeriodPricing { get; set; }
        public virtual ICollection<TenderPeriodPricingLine> TenderPeriodPricingLine { get; set; }
        public virtual ICollection<TransferBalanceQtyDocument> TransferBalanceQtyDocument { get; set; }
        public virtual ICollection<TransferBalanceQty> TransferBalanceQtyStatusCode { get; set; }
        public virtual ICollection<TransferBalanceQty> TransferBalanceQtyTransferTime { get; set; }
        public virtual ICollection<TransferSettings> TransferSettings { get; set; }
        public virtual ICollection<UnitConversion> UnitConversion { get; set; }
        public virtual ICollection<UserGroup> UserGroup { get; set; }
        public virtual ICollection<VendorsList> VendorsList { get; set; }
        public virtual ICollection<WikiAccessRight> WikiAccessRight { get; set; }
        public virtual ICollection<WikiGroup> WikiGroup { get; set; }
        public virtual ICollection<WikiPage> WikiPage { get; set; }
        public virtual ICollection<WorkFlow> WorkFlow { get; set; }
        public virtual ICollection<WorkFlowApproverSetup> WorkFlowApproverSetup { get; set; }
        public virtual ICollection<WorkFlowInteraction> WorkFlowInteraction { get; set; }
        public virtual ICollection<WorkFlowPageBlock> WorkFlowPageBlock { get; set; }
        public virtual ICollection<WorkFlowPageBlockAccess> WorkFlowPageBlockAccess { get; set; }
        public virtual ICollection<WorkFlowPageFieldAccess> WorkFlowPageFieldAccess { get; set; }
        public virtual ICollection<WorkFlowPages> WorkFlowPages { get; set; }
        public virtual ICollection<WorkList> WorkListRequestTypeNavigation { get; set; }
        public virtual ICollection<WorkList> WorkListWorkListStatusNavigation { get; set; }
        public virtual ICollection<WorkList> WorkListWorkListTypeNavigation { get; set; }
        public virtual ICollection<WorkOrder> WorkOrderAssignment { get; set; }
        public virtual ICollection<WorkOrderCommentUser> WorkOrderCommentUser { get; set; }
        public virtual ICollection<WorkOrderLine> WorkOrderLineCrtstatus { get; set; }
        public virtual ICollection<WorkOrderLine> WorkOrderLinePriority { get; set; }
        public virtual ICollection<WorkOrderLine> WorkOrderLineStatusCode { get; set; }
        public virtual ICollection<WorkOrderLine> WorkOrderLineSunwardStatus { get; set; }
        public virtual ICollection<WorkOrder> WorkOrderStatusCode { get; set; }
    }
}
