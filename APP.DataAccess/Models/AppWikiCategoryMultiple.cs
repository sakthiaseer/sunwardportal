﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AppWikiCategoryMultiple
    {
        public long AppWikiCategoryMultipleId { get; set; }
        public long? ApplicationWikiId { get; set; }
        public long? WikiCategoryId { get; set; }

        public virtual ApplicationWiki ApplicationWiki { get; set; }
        public virtual ApplicationMasterChild WikiCategory { get; set; }
    }
}
