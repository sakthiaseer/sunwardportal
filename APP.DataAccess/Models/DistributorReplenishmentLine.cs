﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DistributorReplenishmentLine
    {
        public long DistributorReplenishmentLineId { get; set; }
        public long? DistributorReplenishmentId { get; set; }
        public long? ProductNameId { get; set; }
        public decimal? ReplenishmentTiming { get; set; }
        public bool? NeedVersionChecking { get; set; }
        public decimal? MaxHoldingStock { get; set; }
        public decimal? NoOfMonthToReplenish { get; set; }
        public bool? IsExceptionalItem { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual DistributorReplenishment DistributorReplenishment { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual GenericCodes ProductName { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
