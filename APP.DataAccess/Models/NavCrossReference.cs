﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavCrossReference
    {
        public long NavCrossReferenceId { get; set; }
        public long? ItemId { get; set; }
        public string TypeOfCompany { get; set; }
        public long? CompanyId { get; set; }
        public long? NavCustomerId { get; set; }
        public long? NavVendorId { get; set; }
        public string CrossReferenceNo { get; set; }
        public long? SoCustomerId { get; set; }

        public virtual Plant Company { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual Navcustomer NavCustomer { get; set; }
        public virtual Navvendor NavVendor { get; set; }
        public virtual SoCustomer SoCustomer { get; set; }
    }
}
