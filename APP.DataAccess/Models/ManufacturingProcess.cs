﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ManufacturingProcess
    {
        public ManufacturingProcess()
        {
            ManufacturingProcessLine = new HashSet<ManufacturingProcessLine>();
        }

        public long ManufacturingProcessId { get; set; }
        public long? FinishProductId { get; set; }
        public int? RegisterationCodeId { get; set; }
        public bool? IsMasterDocument { get; set; }
        public bool? IsInformationvsmaster { get; set; }
        public long? FinishProductGeneralInfoId { get; set; }
        public long? ManufacturingFlowChartId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual FinishProduct FinishProduct { get; set; }
        public virtual FinishProductGeneralInfo FinishProductGeneralInfo { get; set; }
        public virtual Documents ManufacturingFlowChart { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster RegisterationCode { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ManufacturingProcessLine> ManufacturingProcessLine { get; set; }
    }
}
