﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class LocationWiki
    {
        public long LocationWikiId { get; set; }
        public long LocationId { get; set; }
        public long PageId { get; set; }

        public virtual Locations Location { get; set; }
        public virtual WikiPage Page { get; set; }
    }
}
