﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Address
    {
        public Address()
        {
            FmglobalAddess = new HashSet<FmglobalAddess>();
            SourceListOfficeAddress = new HashSet<SourceList>();
            SourceListSiteAddress = new HashSet<SourceList>();
            VendorsListOfficeAddress = new HashSet<VendorsList>();
            VendorsListSiteAddress = new HashSet<VendorsList>();
        }

        public long AddressId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public int? AddressType { get; set; }
        public int? PostCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public int? OfficePhone { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public long? CountryId { get; set; }
        public long? StateId { get; set; }
        public long? CityId { get; set; }

        public virtual CodeMaster AddressTypeNavigation { get; set; }
        public virtual ApplicationMasterChild CityNavigation { get; set; }
        public virtual ApplicationMasterChild CountryNavigation { get; set; }
        public virtual ApplicationMasterChild State { get; set; }
        public virtual ICollection<FmglobalAddess> FmglobalAddess { get; set; }
        public virtual ICollection<SourceList> SourceListOfficeAddress { get; set; }
        public virtual ICollection<SourceList> SourceListSiteAddress { get; set; }
        public virtual ICollection<VendorsList> VendorsListOfficeAddress { get; set; }
        public virtual ICollection<VendorsList> VendorsListSiteAddress { get; set; }
    }
}
