﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FpmanufacturingRecipe
    {
        public FpmanufacturingRecipe()
        {
            BomproductionGroup = new HashSet<BomproductionGroup>();
        }

        public long FpmanufacturingRecipeId { get; set; }
        public long? ManufacturingSiteId { get; set; }
        public long? FinishProductGeneralInfoId { get; set; }
        public bool? IsMaster { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ItemClassificationHeaderId { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual FinishProductGeneralInfo FinishProductGeneralInfo { get; set; }
        public virtual ItemClassificationHeader ItemClassificationHeader { get; set; }
        public virtual ApplicationUser ModifiedUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<BomproductionGroup> BomproductionGroup { get; set; }
    }
}
