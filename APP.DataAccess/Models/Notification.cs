﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Notification
    {
        public long NotificationId { get; set; }
        public int? ModuleId { get; set; }
        public string Notification1 { get; set; }
        public bool? IsRead { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long? NotifiedUserId { get; set; }

        public virtual CodeMaster Module { get; set; }
        public virtual ApplicationUser NotifiedUser { get; set; }
    }
}
