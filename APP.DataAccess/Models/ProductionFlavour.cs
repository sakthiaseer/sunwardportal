﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionFlavour
    {
        public ProductionFlavour()
        {
            InverseProcessUseSpec = new HashSet<ProductionFlavour>();
            ProductionFlavourLine = new HashSet<ProductionFlavourLine>();
        }

        public long ProductionFlavourId { get; set; }
        public long? ItemNoId { get; set; }
        public long? FlavourId { get; set; }
        public long? SupplierId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string SupplierCode { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public bool? IsSameAsMaster { get; set; }
        public string MaterialName { get; set; }
        public int? SpecNo { get; set; }
        public long? PurposeId { get; set; }
        public long? ProcessUseSpecId { get; set; }
        public long? RndpurposeId { get; set; }
        public string StudyLink { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterDetail Flavour { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProductionFlavour ProcessUseSpec { get; set; }
        public virtual ApplicationMasterDetail Purpose { get; set; }
        public virtual ApplicationMasterDetail Rndpurpose { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationMasterDetail Supplier { get; set; }
        public virtual ICollection<ProductionFlavour> InverseProcessUseSpec { get; set; }
        public virtual ICollection<ProductionFlavourLine> ProductionFlavourLine { get; set; }
    }
}
