﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class RegistrationFormatInformation
    {
        public RegistrationFormatInformation()
        {
            RegistrationFormatInformationLine = new HashSet<RegistrationFormatInformationLine>();
            RegistrationFormatMultiple = new HashSet<RegistrationFormatMultiple>();
        }

        public long RegistrationFormatInformationId { get; set; }
        public long? FormatId { get; set; }
        public long? ProductCategoryId { get; set; }
        public long? TypeId { get; set; }
        public string VariationDescription { get; set; }
        public long? VariationOnId { get; set; }
        public long? VariationCategoryId { get; set; }
        public long? VariationCategoryNoId { get; set; }
        public int? StatusCodeId { get; set; }
        public bool? IsCombinationOfOtherFormat { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? VariationCodeId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual RegistrationVariation VariationCode { get; set; }
        public virtual ICollection<RegistrationFormatInformationLine> RegistrationFormatInformationLine { get; set; }
        public virtual ICollection<RegistrationFormatMultiple> RegistrationFormatMultiple { get; set; }
    }
}
