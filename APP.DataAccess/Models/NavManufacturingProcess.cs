﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavManufacturingProcess
    {
        public long ManufacturingProcessId { get; set; }
        public long? CompanyId { get; set; }
        public string PigeonHoleVersion { get; set; }
        public string Process { get; set; }
        public bool? NoChangeinDate { get; set; }
        public string ReplanRefNo { get; set; }
        public string ProdOrderNo { get; set; }
        public long? ItemId { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public DateTime? StartingDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public bool? ReleaseFromPlanner { get; set; }
        public string Substatus { get; set; }
        public string Remarks { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
