﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CustomerAcceptanceQtyOrder
    {
        public long CustomerAcceptanceQtyOrderId { get; set; }
        public decimal? Qty { get; set; }
        public long? CustomerAcceptanceConfirmationId { get; set; }

        public virtual CustomerAcceptanceConfirmation CustomerAcceptanceConfirmation { get; set; }
    }
}
