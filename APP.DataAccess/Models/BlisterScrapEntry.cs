﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class BlisterScrapEntry
    {
        public long ScrapEntryId { get; set; }
        public string ProdOrderNo { get; set; }
        public int? ProdLineNo { get; set; }
        public string SourceItemNo { get; set; }
        public string ItemDescription { get; set; }
        public string BatchNo { get; set; }
        public string BlisterBom { get; set; }
        public long? PlasticBagId { get; set; }
        public string PlasticBag { get; set; }
        public long? BlisterScrapTypeId { get; set; }
        public string BlisterScrapType { get; set; }
        public decimal? Weight { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual BlisterScrapCode BlisterScrapTypeNavigation { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual PlasticBag PlasticBagNavigation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
