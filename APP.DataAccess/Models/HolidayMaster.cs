﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class HolidayMaster
    {
        public HolidayMaster()
        {
            CompanyHolidayDetail = new HashSet<CompanyHolidayDetail>();
        }

        public long HolidayId { get; set; }
        public long? StateId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual State State { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<CompanyHolidayDetail> CompanyHolidayDetail { get; set; }
    }
}
