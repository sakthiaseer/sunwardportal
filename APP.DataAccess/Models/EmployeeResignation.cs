﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmployeeResignation
    {
        public long EmployeeResignationId { get; set; }
        public long? EmployeeId { get; set; }
        public int? ResignationType { get; set; }
        public byte[] ResignationLetter { get; set; }
        public DateTime? IntendedLastDate { get; set; }
        public DateTime? AgreedLastDate { get; set; }
        public string ResigneeName { get; set; }
        public long? DesignationId { get; set; }
        public long? DesignationHcno { get; set; }
        public long? ApprovedBy { get; set; }
        public int? ApproveStatus { get; set; }
        public long? DocumentId { get; set; }

        public virtual CodeMaster ApproveStatusNavigation { get; set; }
        public virtual ApplicationUser ApprovedByNavigation { get; set; }
        public virtual Designation Designation { get; set; }
        public virtual DesignationHeadCount DesignationHcnoNavigation { get; set; }
        public virtual Documents Document { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual CodeMaster ResignationTypeNavigation { get; set; }
    }
}
