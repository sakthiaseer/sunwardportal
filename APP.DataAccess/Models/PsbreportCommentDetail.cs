﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PsbreportCommentDetail
    {
        public long PsbreportCommentDetailId { get; set; }
        public long? DistAcId { get; set; }
        public long? ItemId { get; set; }
        public decimal? Qty { get; set; }
        public string Comment { get; set; }
        public DateTime? Date { get; set; }
        public int? Week { get; set; }
        public bool? IsApproval { get; set; }
        public decimal? IntendOrder { get; set; }
        public int? NewAcMonth { get; set; }
        public decimal? ProjectHoldingAmount { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Acitems DistAc { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
