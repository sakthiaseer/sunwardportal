﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApplicationReminder
    {
        public long ReminderId { get; set; }
        public string ReminderTitle { get; set; }
        public string ReminderDescription { get; set; }
        public DateTime? ReminderStartDate { get; set; }
        public DateTime? ReminderEndDate { get; set; }
        public long? TaskId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedUserId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual TaskMaster Task { get; set; }
    }
}
