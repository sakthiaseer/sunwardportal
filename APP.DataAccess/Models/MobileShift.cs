﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class MobileShift
    {
        public MobileShift()
        {
            ReAssignmentJob = new HashSet<ReAssignmentJob>();
        }

        public long MobileShiftId { get; set; }
        public long? PlantId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int? StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Plant Plant { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ReAssignmentJob> ReAssignmentJob { get; set; }
    }
}
