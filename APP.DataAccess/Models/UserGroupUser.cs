﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class UserGroupUser
    {
        public UserGroupUser()
        {
            FolderStorage = new HashSet<FolderStorage>();
        }

        public long UserGroupUserId { get; set; }
        public long? UserId { get; set; }
        public long? UserGroupId { get; set; }

        public virtual ApplicationUser User { get; set; }
        public virtual UserGroup UserGroup { get; set; }
        public virtual ICollection<FolderStorage> FolderStorage { get; set; }
    }
}
