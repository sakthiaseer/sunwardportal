﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AppWikiReleaseDoc
    {
        public long AppWikiReleaseDocId { get; set; }
        public long? DocumentId { get; set; }
        public long? ApplicationWikiId { get; set; }

        public virtual ApplicationWiki ApplicationWiki { get; set; }
        public virtual Documents Document { get; set; }
    }
}
