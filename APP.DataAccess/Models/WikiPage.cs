﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WikiPage
    {
        public WikiPage()
        {
            FlowDistribution = new HashSet<FlowDistribution>();
            LinkPagesPage = new HashSet<LinkPages>();
            LinkPagesParentPage = new HashSet<LinkPages>();
            LocationGroupWiki = new HashSet<LocationGroupWiki>();
            LocationWiki = new HashSet<LocationWiki>();
            PageApproval = new HashSet<PageApproval>();
            PageAttachment = new HashSet<PageAttachment>();
            PageComment = new HashSet<PageComment>();
            PageLikes = new HashSet<PageLikes>();
            PageVersion = new HashSet<PageVersion>();
            PrintInfo = new HashSet<PrintInfo>();
            StaffTraining = new HashSet<StaffTraining>();
            WikiAccess = new HashSet<WikiAccess>();
            WikiAccessRight = new HashSet<WikiAccessRight>();
            WikiChangeControl = new HashSet<WikiChangeControl>();
            WikiCompany = new HashSet<WikiCompany>();
            WikiNotes = new HashSet<WikiNotes>();
            WikiPageAction = new HashSet<WikiPageAction>();
            WikiPageCopyInfo = new HashSet<WikiPageCopyInfo>();
            WikiPageGroupPage = new HashSet<WikiPageGroup>();
            WikiPageGroupParent = new HashSet<WikiPageGroup>();
            WikiSearchNavigation = new HashSet<WikiSearch>();
            WikiTag = new HashSet<WikiTag>();
            WikiTranslation = new HashSet<WikiTranslation>();
        }

        public long PageId { get; set; }
        public long? CategoryId { get; set; }
        public long? DepartmentId { get; set; }
        public long? LanguageId { get; set; }
        public long? ParentPageId { get; set; }
        public long? GroupId { get; set; }
        public long? SectionId { get; set; }
        public string Titile { get; set; }
        public string PageContent { get; set; }
        public string EditContent { get; set; }
        public string ShortDescription { get; set; }
        public string Notes { get; set; }
        public long? WikiSearchId { get; set; }
        public string SessionId { get; set; }
        public string SearchDocument { get; set; }
        public string WorkingDocument { get; set; }
        public bool? NoTransalation { get; set; }
        public bool? IsChinese { get; set; }
        public string ProposedTag { get; set; }
        public bool? IsMalay { get; set; }
        public int? MethodOfContent { get; set; }
        public bool? IsChangeApproval { get; set; }
        public bool? IsGmprelated { get; set; }
        public bool? SmecommentRequired { get; set; }
        public DateTime? SmedueDate { get; set; }
        public long? PrimaryApproverId { get; set; }
        public long? NextApproverId { get; set; }
        public bool? ApproveMySelf { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public long? TableId { get; set; }
        public long? WorkFlowId { get; set; }
        public int? VersionType { get; set; }
        public DateTime? NextReviewDate { get; set; }
        public bool? UnderConstruction { get; set; }
        public bool? NeedFurtherReview { get; set; }
        public string SameControlStatement { get; set; }
        public string ControlStatement { get; set; }
        public long? OwnerId { get; set; }
        public long? Assignee { get; set; }
        public string ReviewComment { get; set; }
        public DateTime? PublishDate { get; set; }
        public DateTime? StartReviewDate { get; set; }
        public bool? ContentApproved { get; set; }
        public bool? Comment { get; set; }
        public string CommentDocument { get; set; }
        public string IssueNo { get; set; }
        public string RevisionNo { get; set; }
        public string ChangeControlNo { get; set; }
        public DateTime? ImplementationDate { get; set; }
        public string GroupReference { get; set; }
        public string NextAction { get; set; }
        public bool? IsPublishToAll { get; set; }
        public bool? IsTestPage { get; set; }
        public bool? IsReturned { get; set; }
        public long? SmechairPerson { get; set; }
        public bool? ApproveEditPage { get; set; }
        public string ArchiveReason { get; set; }
        public int? TypeOfRightId { get; set; }
        public int StatusCodeId { get; set; }
        public int? SecondaryStatusId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Category Category { get; set; }
        public virtual Department Department { get; set; }
        public virtual WikiGroup Group { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationUser NextApprover { get; set; }
        public virtual ApplicationUser Owner { get; set; }
        public virtual ApplicationUser PrimaryApprover { get; set; }
        public virtual Section Section { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual FormTables Table { get; set; }
        public virtual WikiSearch WikiSearch { get; set; }
        public virtual ICollection<FlowDistribution> FlowDistribution { get; set; }
        public virtual ICollection<LinkPages> LinkPagesPage { get; set; }
        public virtual ICollection<LinkPages> LinkPagesParentPage { get; set; }
        public virtual ICollection<LocationGroupWiki> LocationGroupWiki { get; set; }
        public virtual ICollection<LocationWiki> LocationWiki { get; set; }
        public virtual ICollection<PageApproval> PageApproval { get; set; }
        public virtual ICollection<PageAttachment> PageAttachment { get; set; }
        public virtual ICollection<PageComment> PageComment { get; set; }
        public virtual ICollection<PageLikes> PageLikes { get; set; }
        public virtual ICollection<PageVersion> PageVersion { get; set; }
        public virtual ICollection<PrintInfo> PrintInfo { get; set; }
        public virtual ICollection<StaffTraining> StaffTraining { get; set; }
        public virtual ICollection<WikiAccess> WikiAccess { get; set; }
        public virtual ICollection<WikiAccessRight> WikiAccessRight { get; set; }
        public virtual ICollection<WikiChangeControl> WikiChangeControl { get; set; }
        public virtual ICollection<WikiCompany> WikiCompany { get; set; }
        public virtual ICollection<WikiNotes> WikiNotes { get; set; }
        public virtual ICollection<WikiPageAction> WikiPageAction { get; set; }
        public virtual ICollection<WikiPageCopyInfo> WikiPageCopyInfo { get; set; }
        public virtual ICollection<WikiPageGroup> WikiPageGroupPage { get; set; }
        public virtual ICollection<WikiPageGroup> WikiPageGroupParent { get; set; }
        public virtual ICollection<WikiSearch> WikiSearchNavigation { get; set; }
        public virtual ICollection<WikiTag> WikiTag { get; set; }
        public virtual ICollection<WikiTranslation> WikiTranslation { get; set; }
    }
}
