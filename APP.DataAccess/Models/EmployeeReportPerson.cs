﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmployeeReportPerson
    {
        public long EmployeeReportPersonId { get; set; }
        public long? EmployeeId { get; set; }
        public long? DepartmentId { get; set; }
        public long? SectionId { get; set; }
        public long? SubSectionId { get; set; }
        public long? LevelId { get; set; }
        public long? PositionId { get; set; }
        public long? ReportId { get; set; }

        public virtual Department Department { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual LevelMaster Level { get; set; }
        public virtual Designation Position { get; set; }
        public virtual Section Section { get; set; }
        public virtual SubSection SubSection { get; set; }
    }
}
