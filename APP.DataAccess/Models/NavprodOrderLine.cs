﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavprodOrderLine
    {
        public NavprodOrderLine()
        {
            IpirApp = new HashSet<IpirApp>();
            ProductionActivityAppLine = new HashSet<ProductionActivityAppLine>();
            ProductionActivityPlanningAppLine = new HashSet<ProductionActivityPlanningAppLine>();
            ProductionActivityRoutineAppLine = new HashSet<ProductionActivityRoutineAppLine>();
        }

        public long NavprodOrderLineId { get; set; }
        public string Status { get; set; }
        public string ProdOrderNo { get; set; }
        public int? OrderLineNo { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string Description1 { get; set; }
        public DateTime? CompletionDate { get; set; }
        public decimal? RemainingQuantity { get; set; }
        public string UnitofMeasureCode { get; set; }
        public DateTime? LastSyncDate { get; set; }
        public long? LastSyncUserId { get; set; }
        public string RePlanRefNo { get; set; }
        public string BatchNo { get; set; }
        public DateTime? StartDate { get; set; }
        public decimal? OutputQty { get; set; }
        public long? CompanyId { get; set; }
        public string TopicId { get; set; }

        public virtual Plant Company { get; set; }
        public virtual ICollection<IpirApp> IpirApp { get; set; }
        public virtual ICollection<ProductionActivityAppLine> ProductionActivityAppLine { get; set; }
        public virtual ICollection<ProductionActivityPlanningAppLine> ProductionActivityPlanningAppLine { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLine> ProductionActivityRoutineAppLine { get; set; }
    }
}
