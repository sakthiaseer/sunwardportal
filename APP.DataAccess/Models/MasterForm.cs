﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class MasterForm
    {
        public MasterForm()
        {
            MasterFormDetail = new HashSet<MasterFormDetail>();
        }

        public long MasterFormId { get; set; }
        public string FormName { get; set; }
        public string FormSchema { get; set; }
        public string FormItems { get; set; }
        public string FormModel { get; set; }
        public Guid? SessionId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<MasterFormDetail> MasterFormDetail { get; set; }
    }
}
