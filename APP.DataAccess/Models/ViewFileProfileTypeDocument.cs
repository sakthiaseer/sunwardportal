﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewFileProfileTypeDocument
    {
        public string Profile { get; set; }
        public string Name { get; set; }
        public int? IsExpiryDate { get; set; }
        public bool? IsExpiryDates { get; set; }
        public long ProfileId { get; set; }
        public long FileProfileTypeId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public string Description { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string StatusCode { get; set; }
        public string AddedBy { get; set; }
        public string ModifiedBy { get; set; }
        public long? ParentId { get; set; }
        public Guid? SessionId { get; set; }
        public bool? IsDelete { get; set; }
        public long? DeleteByUserId { get; set; }
        public DateTime? DeleteByDate { get; set; }
        public string DeleteByUser { get; set; }
        public long? DynamicFormId { get; set; }
    }
}
