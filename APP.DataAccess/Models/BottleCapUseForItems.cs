﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class BottleCapUseForItems
    {
        public long BottleCapUseForItemsId { get; set; }
        public long? BottleCapSpecificationId { get; set; }
        public long? UseForItemId { get; set; }
        public long? CapsealSpecificationId { get; set; }
        public long? BottleInsertSpecificationId { get; set; }
        public long? BottleSpecificationId { get; set; }
        public long? CartonSpecificationId { get; set; }
        public long? DividerSpecificationId { get; set; }
        public long? LayerPadSpecificationId { get; set; }
        public long? NestingSpecificationId { get; set; }
        public long? ShrinkWrapSpecificationId { get; set; }

        public virtual CommonPackagingBottleCap BottleCapSpecification { get; set; }
        public virtual CommonPackagingBottleInsert BottleInsertSpecification { get; set; }
        public virtual CommonPackagingBottle BottleSpecification { get; set; }
        public virtual CommonPackagingCapseal CapsealSpecification { get; set; }
        public virtual CommonPackagingCarton CartonSpecification { get; set; }
        public virtual CommonPackagingDivider DividerSpecification { get; set; }
        public virtual CommonPackagingLayerPad LayerPadSpecification { get; set; }
        public virtual CommonPackagingNesting NestingSpecification { get; set; }
        public virtual CommonPackagingShrinkWrap ShrinkWrapSpecification { get; set; }
    }
}
