﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PortalCalendar
    {
        public long CalendarId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? CalendarTypeId { get; set; }
        public DateTime? EntryStartDate { get; set; }
        public DateTime? EntryEndDate { get; set; }
        public string HolidayMonth { get; set; }
        public bool? IsHoliday { get; set; }
        public int? HolidayTypeId { get; set; }
        public DateTime? TriggerDate { get; set; }
        public string TaskContent { get; set; }
        public DateTime? TaskDueDate { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
