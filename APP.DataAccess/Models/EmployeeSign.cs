﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmployeeSign
    {
        public long EmployeeSignId { get; set; }
        public long? EmployeeId { get; set; }
        public byte[] EmployeeSignature { get; set; }
        public byte[] EmployeeProfile { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
