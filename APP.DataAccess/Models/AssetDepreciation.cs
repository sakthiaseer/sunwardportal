﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AssetDepreciation
    {
        public long AssetDepreciationId { get; set; }
        public long AssetMasterId { get; set; }
        public decimal? DepriciationAmount { get; set; }
        public DateTime? DepriciationDate { get; set; }
        public decimal? BookValue { get; set; }

        public virtual AssetMaster AssetMaster { get; set; }
    }
}
