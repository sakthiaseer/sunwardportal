﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PackagingItemHistoryLine
    {
        public long PackagingItemHistoryLineId { get; set; }
        public long? PackagingItemHistoryId { get; set; }
        public long? ItemId { get; set; }
        public Guid? ImageSessionId { get; set; }
        public Guid? FileSessionId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual PackagingItemHistory PackagingItemHistory { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
