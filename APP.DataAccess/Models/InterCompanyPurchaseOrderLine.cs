﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class InterCompanyPurchaseOrderLine
    {
        public long InterCompanyPurchaseOrderLineId { get; set; }
        public long? InterCompanyPurchaseOrderId { get; set; }
        public long? NavItemId { get; set; }
        public decimal? Quantity { get; set; }
        public DateTime? ShipmentDate { get; set; }
        public DateTime? PromisedDeliveryDate { get; set; }
        public string Location { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual InterCompanyPurchaseOrder InterCompanyPurchaseOrder { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Navitems NavItem { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
