﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class IpirreportingInformation
    {
        public long ReportinginformationId { get; set; }
        public long? IpirAppId { get; set; }
        public bool? ReportBy { get; set; }
        public string IssueDescription { get; set; }
        public long? IssueRelatedTo { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
    }
}
