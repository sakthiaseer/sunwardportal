﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CommentAttachment
    {
        public int CommentAttachmentId { get; set; }
        public long TaskCommentId { get; set; }
        public long DocumentId { get; set; }
        public string FileName { get; set; }
        public long UserId { get; set; }

        public virtual Documents Document { get; set; }
        public virtual TaskComment TaskComment { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
