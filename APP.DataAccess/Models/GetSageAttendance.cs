﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class GetSageAttendance
    {
        public string EmpId { get; set; }
        public DateTime SubmitDate { get; set; }
        public string Code { get; set; }
        public string Remarks { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double Duration { get; set; }
    }
}
