﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DiscussionAttachment
    {
        public int DiscussionAttachmentId { get; set; }
        public long DiscussionNotesId { get; set; }
        public long DocumantId { get; set; }
        public string FileName { get; set; }
        public long UserId { get; set; }
    }
}
