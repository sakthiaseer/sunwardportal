﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class JobProgressTemplateLineProcessStatus
    {
        public long JobProgressTemplateLineProcessStatusId { get; set; }
        public long? TemplateId { get; set; }
        public long? TemplateLineId { get; set; }
        public Guid? SessionId { get; set; }
        public int? ProcessStatusId { get; set; }

        public virtual CodeMaster ProcessStatus { get; set; }
        public virtual JobProgressTemplate Template { get; set; }
        public virtual JobProgressTempletateLine TemplateLine { get; set; }
    }
}
