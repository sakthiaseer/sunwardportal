﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CommonPackagingBottle
    {
        public CommonPackagingBottle()
        {
            BottleCapUseForItems = new HashSet<BottleCapUseForItems>();
        }

        public long BottleSpecificationId { get; set; }
        public long? PackagingItemSetInfoId { get; set; }
        public bool? Set { get; set; }
        public bool? VersionControl { get; set; }
        public decimal? Volume { get; set; }
        public string Code { get; set; }
        public long? PackSizeunitsId { get; set; }
        public long? ColorId { get; set; }
        public long? TypeId { get; set; }
        public long? ShapeId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<BottleCapUseForItems> BottleCapUseForItems { get; set; }
    }
}
