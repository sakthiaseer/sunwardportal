﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CommonPackagingItemHeader
    {
        public CommonPackagingItemHeader()
        {
            CommonPackagingSetInfo = new HashSet<CommonPackagingSetInfo>();
            CommonPackingInformationLine = new HashSet<CommonPackingInformationLine>();
            CommonPackingItemListLine = new HashSet<CommonPackingItemListLine>();
        }

        public long CommonPackagingItemId { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public string PackagingItemName { get; set; }
        public long? PackagingItemCategoryId { get; set; }
        public long? ProfileId { get; set; }
        public string AlsoKownAs { get; set; }
        public long? UomId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileReferenceNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ItemClassificationMaster ItemClassificationMaster { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationMasterDetail PackagingItemCategory { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationMasterDetail Uom { get; set; }
        public virtual ICollection<CommonPackagingSetInfo> CommonPackagingSetInfo { get; set; }
        public virtual ICollection<CommonPackingInformationLine> CommonPackingInformationLine { get; set; }
        public virtual ICollection<CommonPackingItemListLine> CommonPackingItemListLine { get; set; }
    }
}
