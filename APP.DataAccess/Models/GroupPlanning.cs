﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class GroupPlanning
    {
        public long GroupPlanningId { get; set; }
        public long? CompanyId { get; set; }
        public string ProductGroupCode { get; set; }
        public DateTime? StartDate { get; set; }
        public string ItemNo { get; set; }
        public string ProductDescription { get; set; }
        public string ItemDescription { get; set; }
        public string ItemDescription1 { get; set; }
        public string RecipeNo { get; set; }
        public string BatchSize { get; set; }
        public decimal? Quantity { get; set; }
        public string Uom { get; set; }
        public int? NoOfTicket { get; set; }
        public bool? OrderCreated { get; set; }
    }
}
