﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SoSalesOrderLine
    {
        public long SoSalesOrderLineId { get; set; }
        public long? SoSalesOrderId { get; set; }
        public string ItemSerialNo { get; set; }
        public decimal? Qty { get; set; }
        public decimal? UnitPrice { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? StatusCodeId { get; set; }
        public long? ItemId { get; set; }
        public decimal? ManualPrice { get; set; }
        public bool? IsManual { get; set; }
        public decimal? OrderBounsQty { get; set; }
        public string PricingType { get; set; }
        public long? SellingMethodId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationMasterDetail SellingMethod { get; set; }
        public virtual SoSalesOrder SoSalesOrder { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
