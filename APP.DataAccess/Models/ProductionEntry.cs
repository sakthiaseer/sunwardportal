﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionEntry
    {
        public ProductionEntry()
        {
            StartOfDayProdTaskMultiple = new HashSet<StartOfDayProdTaskMultiple>();
        }

        public long ProductionEntryId { get; set; }
        public long? LocationId { get; set; }
        public string LocationName { get; set; }
        public string ProductionOrderNo { get; set; }
        public int? ProdLineNo { get; set; }
        public string ItemName { get; set; }
        public string Deascription { get; set; }
        public string Deascription2 { get; set; }
        public string BatchNo { get; set; }
        public string RoomStatus { get; set; }
        public string RoomStatus1 { get; set; }
        public long? ProductionActionId { get; set; }
        public int? NumberOfWorker { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ActualStartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Company { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProductionAction ProductionAction { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<StartOfDayProdTaskMultiple> StartOfDayProdTaskMultiple { get; set; }
    }
}
