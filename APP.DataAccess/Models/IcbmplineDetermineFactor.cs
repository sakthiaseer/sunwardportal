﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class IcbmplineDetermineFactor
    {
        public long IcbmplineDetermineFactorId { get; set; }
        public long? IcbmplineId { get; set; }
        public long? IcbmpdetermineFactorId { get; set; }

        public virtual Icbmpline Icbmpline { get; set; }
    }
}
