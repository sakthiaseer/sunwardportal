﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionActivityRoutineAppLineDoc
    {
        public long ProductionActivityRoutineAppLineDocId { get; set; }
        public long? ProductionActivityRoutineAppLineId { get; set; }
        public long? DocumentId { get; set; }
        public string Type { get; set; }
        public long? IpirReportId { get; set; }

        public virtual Documents Document { get; set; }
        public virtual IpirReport IpirReport { get; set; }
        public virtual ProductionActivityRoutineAppLine ProductionActivityRoutineAppLine { get; set; }
    }
}
