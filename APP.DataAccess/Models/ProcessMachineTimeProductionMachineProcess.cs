﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProcessMachineTimeProductionMachineProcess
    {
        public long ProcessMachineTimeProductionMachineProcessId { get; set; }
        public long? ProcessMachineTimeProductionLineLineId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public long? ManufacturingStepsId { get; set; }
        public bool? UseAnyMachine { get; set; }
        public long? MachineNameId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ClassificationCompanyId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterDetail ClassificationCompany { get; set; }
        public virtual CommonFieldsProductionMachine MachineName { get; set; }
        public virtual ApplicationMasterDetail ManufacturingProcess { get; set; }
        public virtual ApplicationMasterDetail ManufacturingSteps { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProcessMachineTimeProductionLineLine ProcessMachineTimeProductionLineLine { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
