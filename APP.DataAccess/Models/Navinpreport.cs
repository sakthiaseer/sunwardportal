﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Navinpreport
    {
        public long Inpid { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? QuantityBase { get; set; }
        public decimal? Soqty { get; set; }
        public decimal? BlanketQty { get; set; }
        public decimal? ProdCompQty { get; set; }
        public decimal? Poqty { get; set; }
        public decimal? Sorqty { get; set; }
        public decimal? ProdQty { get; set; }
        public string Uom { get; set; }
        public DateTime? CompletionDate { get; set; }
        public DateTime? ShipmentDate { get; set; }
        public string Company { get; set; }
        public decimal? AvailableQty { get; set; }
        public decimal? SaftyStock { get; set; }
        public string ProdOrder { get; set; }
        public DateTime? SaftyLeadTime { get; set; }
        public string DocumentNo { get; set; }
        public string SourceName { get; set; }
        public string LocationCode { get; set; }
        public decimal? Demand { get; set; }
        public decimal? Balance { get; set; }
        public decimal? Supply { get; set; }
        public decimal? Rpobalance { get; set; }
        public string Rpono { get; set; }
        public string ItemCategory { get; set; }
        public string MethodCode { get; set; }
        public decimal? ReservedQty { get; set; }
        public decimal? OutstandingQty { get; set; }
        public string BatchNo { get; set; }
        public string LotNo { get; set; }
        public string Qcstatus { get; set; }
        public string QcrefNo { get; set; }
        public string ShipingType { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime? OrderDate { get; set; }
        public string ReplanRefNo { get; set; }
        public decimal? Pobalance { get; set; }
        public string Pono { get; set; }
        public DateTime? ExpReciptDate { get; set; }
        public string FilterType { get; set; }
        public string TableName { get; set; }
        public string CustomerName { get; set; }
        public long? CustomerId { get; set; }
        public long? CompanyId { get; set; }
    }
}
