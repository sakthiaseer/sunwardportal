﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WorkFlowPageField
    {
        public WorkFlowPageField()
        {
            WorkFlowPageFieldAccess = new HashSet<WorkFlowPageFieldAccess>();
            WorkFlowPageTable = new HashSet<WorkFlowPageTable>();
        }

        public long WorkFlowPageFieldId { get; set; }
        public long? WorkFlowPageBlockId { get; set; }
        public string LableText { get; set; }
        public string FieldName { get; set; }
        public string FieldType { get; set; }
        public int? DataType { get; set; }
        public string FieldValue { get; set; }
        public bool? IsRequired { get; set; }
        public bool? IsExtra { get; set; }

        public virtual WorkFlowPageBlock WorkFlowPageBlock { get; set; }
        public virtual ICollection<WorkFlowPageFieldAccess> WorkFlowPageFieldAccess { get; set; }
        public virtual ICollection<WorkFlowPageTable> WorkFlowPageTable { get; set; }
    }
}
