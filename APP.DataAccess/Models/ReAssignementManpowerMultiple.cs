﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ReAssignementManpowerMultiple
    {
        public long ReassignmentManpowerId { get; set; }
        public long? ReassignmentId { get; set; }
        public long? ManPowerId { get; set; }

        public virtual Employee ManPower { get; set; }
        public virtual ReAssignmentJob Reassignment { get; set; }
    }
}
