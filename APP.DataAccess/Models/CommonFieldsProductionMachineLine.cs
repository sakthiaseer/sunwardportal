﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CommonFieldsProductionMachineLine
    {
        public long CommonFieldsProductionMachineLineId { get; set; }
        public long? CommonFieldsProductionMachineId { get; set; }
        public DateTime? QualifiedDate { get; set; }
        public decimal? FrequencyOfRequalificationYear { get; set; }
        public decimal? MinCapacity { get; set; }
        public decimal? MaxCapacity { get; set; }
        public decimal? PreferCapacity { get; set; }
        public long? CapacityUnitsId { get; set; }
        public string QualificationNotes { get; set; }
        public decimal? RecommendedManpower { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CommonFieldsProductionMachine CommonFieldsProductionMachine { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
