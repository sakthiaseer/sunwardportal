﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormItemLine
    {
        public long DynamicFormItemLineId { get; set; }
        public long? DynamicFormItemId { get; set; }
        public decimal? Qty { get; set; }
        public long? ItemDynamicFormTypeId { get; set; }
        public long? ItemDynamicFormDataId { get; set; }
        public string Description { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual DynamicFormItem DynamicFormItem { get; set; }
        public virtual DynamicFormData ItemDynamicFormData { get; set; }
        public virtual DynamicForm ItemDynamicFormType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
