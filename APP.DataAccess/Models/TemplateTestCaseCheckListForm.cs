﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TemplateTestCaseCheckListForm
    {
        public long TemplateTestCaseCheckListFormId { get; set; }
        public long? TemplateTestCaseFormId { get; set; }
        public bool? IsResponsibility { get; set; }
        public string TopicId { get; set; }

        public virtual TemplateTestCaseForm TemplateTestCaseForm { get; set; }
    }
}
