﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CompanyCalendar
    {
        public CompanyCalendar()
        {
            CompanyCalendarAssistance = new HashSet<CompanyCalendarAssistance>();
            CompanyCalendarLine = new HashSet<CompanyCalendarLine>();
        }

        public long CompanyCalendarId { get; set; }
        public DateTime? CalendarEventDate { get; set; }
        public long? CalenderEventId { get; set; }
        public long? CompanyId { get; set; }
        public long? SiteId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public long? UserId { get; set; }
        public bool? RequireAssistance { get; set; }
        public long? OwnerId { get; set; }
        public long? OnBehalfId { get; set; }
        public long? AssistanceFromId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser AssistanceFrom { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationUser OnBehalf { get; set; }
        public virtual ApplicationUser Owner { get; set; }
        public virtual Ictmaster Site { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual ICollection<CompanyCalendarAssistance> CompanyCalendarAssistance { get; set; }
        public virtual ICollection<CompanyCalendarLine> CompanyCalendarLine { get; set; }
    }
}
