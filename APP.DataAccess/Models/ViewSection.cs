﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewSection
    {
        public long SectionId { get; set; }
        public long? DepartmentId { get; set; }
        public string SectionCode { get; set; }
        public string SectionName { get; set; }
        public string Description { get; set; }
        public int? HeadCount { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentDescription { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileCode { get; set; }
        public string PlantCode { get; set; }
        public string Company { get; set; }
        public long? CompanyId { get; set; }
        public string StatusCode { get; set; }
        public string AddedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string Division { get; set; }
        public string DivisionDescription { get; set; }
        public long? DivisionId { get; set; }
        public string SectionDrop { get; set; }
    }
}
