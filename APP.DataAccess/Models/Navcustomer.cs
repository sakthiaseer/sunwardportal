﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Navcustomer
    {
        public Navcustomer()
        {
            Acentry = new HashSet<Acentry>();
            Acitems = new HashSet<Acitems>();
            DistributorReplenishment = new HashSet<DistributorReplenishment>();
            ItemSalesPrice = new HashSet<ItemSalesPrice>();
            NavCrossReference = new HashSet<NavCrossReference>();
            NavcustomerItems = new HashSet<NavcustomerItems>();
            NavisionCompany = new HashSet<NavisionCompany>();
        }

        public long CustomerId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ResponsibilityCenter { get; set; }
        public string LocationCode { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string County { get; set; }
        public string CountryRegionCode { get; set; }
        public string PhoneNo { get; set; }
        public string Contact { get; set; }
        public string SalespersonCode { get; set; }
        public string CustomerPostingGroup { get; set; }
        public string GenBusPostingGroup { get; set; }
        public string VatbusPostingGroup { get; set; }
        public string PaymentTermsCode { get; set; }
        public string CurrencyCode { get; set; }
        public string LanguageCode { get; set; }
        public string ShippingAdvice { get; set; }
        public string ShippingAgentCode { get; set; }
        public decimal? BalanceLcy { get; set; }
        public decimal? BalanceDueLcy { get; set; }
        public decimal? SalesLcy { get; set; }
        public string Company { get; set; }
        public DateTime? LastSyncDate { get; set; }
        public long? LastSyncBy { get; set; }
        public int? StatusCodeId { get; set; }
        public long? CompanyId { get; set; }

        public virtual Plant CompanyNavigation { get; set; }
        public virtual ApplicationUser LastSyncByNavigation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<Acentry> Acentry { get; set; }
        public virtual ICollection<Acitems> Acitems { get; set; }
        public virtual ICollection<DistributorReplenishment> DistributorReplenishment { get; set; }
        public virtual ICollection<ItemSalesPrice> ItemSalesPrice { get; set; }
        public virtual ICollection<NavCrossReference> NavCrossReference { get; set; }
        public virtual ICollection<NavcustomerItems> NavcustomerItems { get; set; }
        public virtual ICollection<NavisionCompany> NavisionCompany { get; set; }
    }
}
