﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmailConversationAssignTo
    {
        public long Id { get; set; }
        public long ConversationId { get; set; }
        public long TopicId { get; set; }
        public long UserId { get; set; }
        public int StatusCodeId { get; set; }
        public int AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public int? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
    }
}
