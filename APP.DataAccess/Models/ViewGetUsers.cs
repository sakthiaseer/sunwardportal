﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewGetUsers
    {
        public long UserId { get; set; }
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string LoginId { get; set; }
        public string LoginPassword { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public string StatusCode { get; set; }
        public string ModifiedByUser { get; set; }
        public string AddedByUser { get; set; }
        public long? DepartmentId { get; set; }
        public string CodeType { get; set; }
        public int CodeId { get; set; }
    }
}
