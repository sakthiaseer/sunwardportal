﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionMachine
    {
        public ProductionMachine()
        {
            OperationProcedureMultiple = new HashSet<OperationProcedureMultiple>();
        }

        public long ProductionMachineId { get; set; }
        public long? ItemNoId { get; set; }
        public long? PurposeOfProcessId { get; set; }
        public string NameOfTheMachine { get; set; }
        public long? ManufacturerId { get; set; }
        public string BasicModelNo { get; set; }
        public string SerialNo { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<OperationProcedureMultiple> OperationProcedureMultiple { get; set; }
    }
}
