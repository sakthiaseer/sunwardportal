﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CompanyListingCompanyType
    {
        public long CompanyListingCompanyTypeId { get; set; }
        public long? CompanyListingId { get; set; }
        public long? CompanyTypeId { get; set; }

        public virtual CompanyListing CompanyListing { get; set; }
    }
}
