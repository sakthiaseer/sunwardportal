﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SolotInformation
    {
        public long SolotInformationId { get; set; }
        public long? SowithoutBlanketOrderId { get; set; }
        public decimal? OrderQty { get; set; }
        public long? OrderCurrencyId { get; set; }
        public decimal? SellingPrice { get; set; }
        public long? PriceUnitId { get; set; }
        public bool? IsFoc { get; set; }
        public string Foc { get; set; }
        public DateTime? RequestShipmentDate { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual SowithOutBlanketOrder SowithoutBlanketOrder { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
