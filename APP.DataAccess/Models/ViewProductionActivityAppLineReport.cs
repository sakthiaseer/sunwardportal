﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewProductionActivityAppLineReport
    {
        public string Date { get; set; }
        public long? CompanyId { get; set; }
        public string DisciplineType { get; set; }
        public string Grouping { get; set; }
        public string Category { get; set; }
        public string Action { get; set; }
        public string Comment { get; set; }
        public string Result { get; set; }
        public string Status { get; set; }
        public string ReadBy { get; set; }
        public string Name { get; set; }
        public string ModifyBy { get; set; }
        public string Info { get; set; }
        public long ProductionActivityAppLineId { get; set; }
        public Guid? SessionId { get; set; }
    }
}
