﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PlantMaintenanceEntryLine
    {
        public PlantMaintenanceEntryLine()
        {
            PlantMaintenanceMasterDetail = new HashSet<PlantMaintenanceMasterDetail>();
        }

        public long PlantEntryLineId { get; set; }
        public long? PlantEntryId { get; set; }
        public long? ItemGroupId { get; set; }
        public string EntryNumber { get; set; }
        public string Manufacture { get; set; }
        public string Brand { get; set; }
        public string ModelNumber { get; set; }
        public string ManufactureIdNo { get; set; }
        public string ManufactureSerialNo { get; set; }
        public string SwexistingId { get; set; }
        public byte[] FrontPhoto { get; set; }
        public byte[] BackPhoto { get; set; }
        public byte[] LocationVideo { get; set; }
        public string VideoFileName { get; set; }
        public string Condition { get; set; }
        public string Recommendation { get; set; }
        public string FrontPhotoName { get; set; }
        public string BackPhotoName { get; set; }
        public long? CategoryId { get; set; }
        public long? PlantItemGroupId { get; set; }
        public long? NameGroupId { get; set; }
        public long? PartGroupId { get; set; }
        public long? SubPartGroupId { get; set; }
        public string SpecificItemName { get; set; }

        public virtual PlantMaintenanceEntryMaster Category { get; set; }
        public virtual PlantMaintenanceEntryMaster NameGroup { get; set; }
        public virtual PlantMaintenanceEntryMaster PartGroup { get; set; }
        public virtual PlantMaintenanceEntry PlantEntry { get; set; }
        public virtual PlantMaintenanceEntryMaster PlantItemGroup { get; set; }
        public virtual PlantMaintenanceEntryMaster SubPartGroup { get; set; }
        public virtual ICollection<PlantMaintenanceMasterDetail> PlantMaintenanceMasterDetail { get; set; }
    }
}
