﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AssetMaster
    {
        public AssetMaster()
        {
            AssetAssignment = new HashSet<AssetAssignment>();
            AssetDepreciation = new HashSet<AssetDepreciation>();
            AssetDisposal = new HashSet<AssetDisposal>();
            AssetMaintenance = new HashSet<AssetMaintenance>();
            AssetTicket = new HashSet<AssetTicket>();
            AssetTransfer = new HashSet<AssetTransfer>();
        }

        public long AssetMasterId { get; set; }
        public long? InventoryTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AssetNo { get; set; }
        public long? LocationId { get; set; }
        public long? CategoryId { get; set; }
        public long? SubCategoryId { get; set; }
        public long? VendorId { get; set; }
        public long? MakeId { get; set; }
        public string ModelNo { get; set; }
        public string SerialNo { get; set; }
        public DateTime? AcquiredDate { get; set; }
        public DateTime? DisposedDate { get; set; }
        public decimal? PurchasePrice { get; set; }
        public DateTime? DepriciatedToDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public decimal? DepriciationBasePrice { get; set; }
        public int? DepriciableLife { get; set; }
        public int? DepricationMethod { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public string Notes { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterChild Category { get; set; }
        public virtual CodeMaster DepricationMethodNavigation { get; set; }
        public virtual InventoryType InventoryType { get; set; }
        public virtual Ictmaster Location { get; set; }
        public virtual ApplicationMasterChild Make { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationMasterChild SubCategory { get; set; }
        public virtual ApplicationMasterDetail Vendor { get; set; }
        public virtual ICollection<AssetAssignment> AssetAssignment { get; set; }
        public virtual ICollection<AssetDepreciation> AssetDepreciation { get; set; }
        public virtual ICollection<AssetDisposal> AssetDisposal { get; set; }
        public virtual ICollection<AssetMaintenance> AssetMaintenance { get; set; }
        public virtual ICollection<AssetTicket> AssetTicket { get; set; }
        public virtual ICollection<AssetTransfer> AssetTransfer { get; set; }
    }
}
