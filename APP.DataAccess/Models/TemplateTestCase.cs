﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TemplateTestCase
    {
        public TemplateTestCase()
        {
            FileProfileType = new HashSet<FileProfileType>();
            TemplateTestCaseCheckList = new HashSet<TemplateTestCaseCheckList>();
            TemplateTestCaseDepartment = new HashSet<TemplateTestCaseDepartment>();
            TemplateTestCaseForm = new HashSet<TemplateTestCaseForm>();
            TemplateTestCaseLink = new HashSet<TemplateTestCaseLink>();
            TemplateTestCaseProposal = new HashSet<TemplateTestCaseProposal>();
        }

        public long TemplateTestCaseId { get; set; }
        public string TemplateName { get; set; }
        public string TemplateCode { get; set; }
        public long? CompanyId { get; set; }
        public long? DepartmentId { get; set; }
        public string Instruction { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public long? ProfileId { get; set; }
        public int? VersionNo { get; set; }
        public string SpecificCase { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual Department Department { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<FileProfileType> FileProfileType { get; set; }
        public virtual ICollection<TemplateTestCaseCheckList> TemplateTestCaseCheckList { get; set; }
        public virtual ICollection<TemplateTestCaseDepartment> TemplateTestCaseDepartment { get; set; }
        public virtual ICollection<TemplateTestCaseForm> TemplateTestCaseForm { get; set; }
        public virtual ICollection<TemplateTestCaseLink> TemplateTestCaseLink { get; set; }
        public virtual ICollection<TemplateTestCaseProposal> TemplateTestCaseProposal { get; set; }
    }
}
