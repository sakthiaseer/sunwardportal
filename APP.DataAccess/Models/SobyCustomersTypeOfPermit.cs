﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SobyCustomersTypeOfPermit
    {
        public long SobyCustomersTypeOfPermitId { get; set; }
        public long? SocustomersItemCrossReferenceId { get; set; }
        public string PermitType { get; set; }
        public long? TypeOfPermitId { get; set; }
        public long? SocustomersDeliveryId { get; set; }

        public virtual SocustomersDelivery SocustomersDelivery { get; set; }
        public virtual SocustomersItemCrossReference SocustomersItemCrossReference { get; set; }
    }
}
