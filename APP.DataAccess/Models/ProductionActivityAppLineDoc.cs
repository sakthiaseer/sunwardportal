﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionActivityAppLineDoc
    {
        public long ProductionActivityAppLineDocId { get; set; }
        public long? ProductionActivityAppLineId { get; set; }
        public long? DocumentId { get; set; }
        public string Type { get; set; }
        public long? IpirReportId { get; set; }
        public Guid? SessionId { get; set; }

        public virtual Documents Document { get; set; }
        public virtual IpirReport IpirReport { get; set; }
        public virtual ProductionActivityAppLine ProductionActivityAppLine { get; set; }
    }
}
