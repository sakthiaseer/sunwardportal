﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PackagingItemHistory
    {
        public PackagingItemHistory()
        {
            PackagingItemHistoryLine = new HashSet<PackagingItemHistoryLine>();
        }

        public long PackagingItemHistoryId { get; set; }
        public long? CategoryId { get; set; }
        public long? ItemId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual NavSaleCategory Category { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<PackagingItemHistoryLine> PackagingItemHistoryLine { get; set; }
    }
}
