﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TransferBalanceQty
    {
        public long TransferBalanceQtyId { get; set; }
        public long? CustomerId { get; set; }
        public long? ContractId { get; set; }
        public long? ProductId { get; set; }
        public long? TransferToContractId { get; set; }
        public int? TransferTimeId { get; set; }
        public DateTime? SpecificMonth { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual SalesOrderEntry Contract { get; set; }
        public virtual CompanyListing Customer { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual SocustomersItemCrossReference Product { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CodeMaster TransferTime { get; set; }
        public virtual SalesOrderEntry TransferToContract { get; set; }
    }
}
