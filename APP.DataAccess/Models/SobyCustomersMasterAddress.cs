﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SobyCustomersMasterAddress
    {
        public SobyCustomersMasterAddress()
        {
            SobyCustomerContactInfoSobyCustomersMasterAddress = new HashSet<SobyCustomerContactInfo>();
            SobyCustomerContactInfoStationLocation = new HashSet<SobyCustomerContactInfo>();
            SobyCustomersAddress = new HashSet<SobyCustomersAddress>();
            SobyCustomersSalesAddress = new HashSet<SobyCustomersSalesAddress>();
        }

        public long SobyCustomersMasterAddressId { get; set; }
        public long? SobyCustomersId { get; set; }
        public string AddressName { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string PostalCode { get; set; }
        public string ContactName { get; set; }
        public string PhoneNo { get; set; }
        public string EmailAddress { get; set; }
        public long? CountryId { get; set; }
        public long? StateId { get; set; }
        public long? CityId { get; set; }
        public string FaxNo { get; set; }
        public string Uenno { get; set; }
        public string CompanyRegisterationNo { get; set; }
        public string Vatgstno { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
        public string Name { get; set; }
        public string NickName { get; set; }
        public int? AddressTypeId { get; set; }
        public string DeliveryInformation { get; set; }
        public long? RefKeyNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster AddressType { get; set; }
        public virtual ApplicationMasterDetail City { get; set; }
        public virtual ApplicationMasterDetail Country { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual SobyCustomers SobyCustomers { get; set; }
        public virtual ApplicationMasterDetail State { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<SobyCustomerContactInfo> SobyCustomerContactInfoSobyCustomersMasterAddress { get; set; }
        public virtual ICollection<SobyCustomerContactInfo> SobyCustomerContactInfoStationLocation { get; set; }
        public virtual ICollection<SobyCustomersAddress> SobyCustomersAddress { get; set; }
        public virtual ICollection<SobyCustomersSalesAddress> SobyCustomersSalesAddress { get; set; }
    }
}
