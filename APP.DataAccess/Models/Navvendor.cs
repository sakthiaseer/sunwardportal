﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Navvendor
    {
        public Navvendor()
        {
            CompanyCalendarLine = new HashSet<CompanyCalendarLine>();
            NavCrossReference = new HashSet<NavCrossReference>();
            NavisionCompany = new HashSet<NavisionCompany>();
        }

        public long VendorId { get; set; }
        public string VendorNo { get; set; }
        public string VendorName { get; set; }
        public string VendorItemNo { get; set; }
        public long? CompanyId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<CompanyCalendarLine> CompanyCalendarLine { get; set; }
        public virtual ICollection<NavCrossReference> NavCrossReference { get; set; }
        public virtual ICollection<NavisionCompany> NavisionCompany { get; set; }
    }
}
