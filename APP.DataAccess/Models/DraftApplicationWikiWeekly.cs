﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DraftApplicationWikiWeekly
    {
        public long ApplicationWikiWeeklyId { get; set; }
        public long? ApplicationWikiLineId { get; set; }
        public int? WeeklyId { get; set; }
        public string CustomType { get; set; }

        public virtual DraftApplicationWikiLine ApplicationWikiLine { get; set; }
        public virtual CodeMaster Weekly { get; set; }
    }
}
