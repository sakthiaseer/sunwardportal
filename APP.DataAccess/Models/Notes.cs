﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Notes
    {
        public long NotesId { get; set; }
        public string Notes1 { get; set; }
        public Guid? SessionId { get; set; }
        public long? DocumentId { get; set; }
        public string Link { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsPersonalNotes { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Documents Document { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
