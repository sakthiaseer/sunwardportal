﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class StandardProcedure
    {
        public long StandardProcedureId { get; set; }
        public long? StandardManufacturingProcessId { get; set; }
        public long? ManufacturingSiteId { get; set; }
        public long? NoId { get; set; }
        public long? IcmasterOperationId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual IcmasterOperation IcmasterOperation { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual StandardManufacturingProcess StandardManufacturingProcess { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
