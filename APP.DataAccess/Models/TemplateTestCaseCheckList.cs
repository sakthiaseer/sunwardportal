﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TemplateTestCaseCheckList
    {
        public TemplateTestCaseCheckList()
        {
            TemplateTestCaseCheckListResponse = new HashSet<TemplateTestCaseCheckListResponse>();
        }

        public long TemplateTestCaseCheckListId { get; set; }
        public long? TemplateTestCaseId { get; set; }
        public string Description { get; set; }
        public string Instruction { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public string No { get; set; }
        public bool? IsResponsibility { get; set; }
        public long? TemplateTestCaseFormId { get; set; }
        public string TopicId { get; set; }
        public string SeqNo { get; set; }
        public bool? IsSkipTest { get; set; }
        public bool? IsMasterTemplate { get; set; }
        public long? TemplateTestCaseLinkId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual TemplateTestCase TemplateTestCase { get; set; }
        public virtual TemplateTestCaseForm TemplateTestCaseForm { get; set; }
        public virtual TemplateTestCaseLink TemplateTestCaseLink { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponse> TemplateTestCaseCheckListResponse { get; set; }
    }
}
