﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Ccfcapproval
    {
        public long CcfcapprovalId { get; set; }
        public bool? IsApproved { get; set; }
        public bool? IsNotApproved { get; set; }
        public string Comments { get; set; }
        public long? VerifiedBy { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationUser VerifiedByNavigation { get; set; }
    }
}
