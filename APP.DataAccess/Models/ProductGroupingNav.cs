﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductGroupingNav
    {
        public ProductGroupingNav()
        {
            ProductGroupingNavDifference = new HashSet<ProductGroupingNavDifference>();
        }

        public long ProductGroupingNavId { get; set; }
        public long? ProductGroupingManufactureId { get; set; }
        public long? ItemId { get; set; }
        public string VarianceNo { get; set; }
        public long? ReplenishmentMethodId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? GenericCodeSupplyToMultipleId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual GenericCodeSupplyToMultiple GenericCodeSupplyToMultiple { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProductGroupingManufacture ProductGroupingManufacture { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ProductGroupingNavDifference> ProductGroupingNavDifference { get; set; }
    }
}
