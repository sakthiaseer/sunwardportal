﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class BompackingMaster
    {
        public BompackingMaster()
        {
            BompackingLanguageMultiple = new HashSet<BompackingLanguageMultiple>();
            BompackingMasterLine = new HashSet<BompackingMasterLine>();
        }

        public long BompackingId { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileNo { get; set; }
        public int? DosageFormId { get; set; }
        public decimal? PackQty { get; set; }
        public long? PackQtyunitId { get; set; }
        public long? PerPackId { get; set; }
        public long? SalesPerPackId { get; set; }
        public long? BottleTypeId { get; set; }
        public string BompackingName { get; set; }
        public string Description { get; set; }
        public decimal? VesionNo { get; set; }
        public decimal? BlisterStripSizeLength { get; set; }
        public decimal? BlisterStripSizeWidth { get; set; }
        public decimal? BsalesPackQty { get; set; }
        public long? BsalesPackUnitId { get; set; }
        public decimal? CcapsuleSize { get; set; }
        public decimal? TbtabletSizeLength { get; set; }
        public decimal? TbtableSizeWidth { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster DosageForm { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<BompackingLanguageMultiple> BompackingLanguageMultiple { get; set; }
        public virtual ICollection<BompackingMasterLine> BompackingMasterLine { get; set; }
    }
}
