﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PortfolioOnBehalf
    {
        public long PortfolioOnBehalfId { get; set; }
        public long? PortfolioId { get; set; }
        public long? OnBehalfId { get; set; }

        public virtual ApplicationUser OnBehalf { get; set; }
        public virtual Portfolio Portfolio { get; set; }
    }
}
