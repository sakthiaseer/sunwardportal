﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FpdrugClassification
    {
        public FpdrugClassification()
        {
            FpdrugClassificationLine = new HashSet<FpdrugClassificationLine>();
        }

        public long FpdrugClassificationId { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public long? DrugClassificationId { get; set; }
        public long? ProfileId { get; set; }
        public long? CountryId { get; set; }
        public bool? IsInstruction { get; set; }
        public string ProfileReferenceNo { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterDetail Country { get; set; }
        public virtual ApplicationMasterDetail DrugClassification { get; set; }
        public virtual ItemClassificationMaster ItemClassificationMaster { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<FpdrugClassificationLine> FpdrugClassificationLine { get; set; }
    }
}
