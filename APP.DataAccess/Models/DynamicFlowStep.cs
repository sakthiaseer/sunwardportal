﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFlowStep
    {
        public DynamicFlowStep()
        {
            ActiveFlowDetails = new HashSet<ActiveFlowDetails>();
            DynamicFlowProgress = new HashSet<DynamicFlowProgress>();
            DynamicFlowStepDetail = new HashSet<DynamicFlowStepDetail>();
            FlowDistributionStep = new HashSet<FlowDistributionStep>();
        }

        public long DynamicFlowStepId { get; set; }
        public long? DynamicFlowDetailId { get; set; }
        public string StepNo { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Instruction { get; set; }
        public long? PageBlockId { get; set; }
        public string Decider { get; set; }
        public string StepLoopTo { get; set; }
        public long? PersonIncharge { get; set; }
        public long? UserGroupId { get; set; }
        public string Notes { get; set; }
        public string Duration { get; set; }

        public virtual DynamicFlowDetail DynamicFlowDetail { get; set; }
        public virtual WorkFlowPageBlock PageBlock { get; set; }
        public virtual ApplicationUser PersonInchargeNavigation { get; set; }
        public virtual UserGroup UserGroup { get; set; }
        public virtual ICollection<ActiveFlowDetails> ActiveFlowDetails { get; set; }
        public virtual ICollection<DynamicFlowProgress> DynamicFlowProgress { get; set; }
        public virtual ICollection<DynamicFlowStepDetail> DynamicFlowStepDetail { get; set; }
        public virtual ICollection<FlowDistributionStep> FlowDistributionStep { get; set; }
    }
}
