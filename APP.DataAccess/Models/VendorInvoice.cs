﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class VendorInvoice
    {
        public long InvoiceId { get; set; }
        public string GstRegistrationNo { get; set; }
        public long? VendorListId { get; set; }

        public virtual VendorsList VendorList { get; set; }
    }
}
