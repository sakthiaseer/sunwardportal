﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProfileAutoNumber
    {
        public long ProfileAutoNumberId { get; set; }
        public long? ProfileId { get; set; }
        public long? CompanyId { get; set; }
        public long? DepartmentId { get; set; }
        public long? SectionId { get; set; }
        public long? SubSectionId { get; set; }
        public string LastNoUsed { get; set; }
        public long? ProfileYear { get; set; }
        public string ScreenId { get; set; }
        public long? ScreenAutoNumberId { get; set; }

        public virtual Plant Company { get; set; }
        public virtual Department Department { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual Section Section { get; set; }
        public virtual SubSection SubSection { get; set; }
    }
}
