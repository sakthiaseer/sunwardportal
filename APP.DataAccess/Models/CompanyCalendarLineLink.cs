﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CompanyCalendarLineLink
    {
        public CompanyCalendarLineLink()
        {
            CompanyCalendarLineLinkPrintUser = new HashSet<CompanyCalendarLineLinkPrintUser>();
            CompanyCalendarLineLinkUser = new HashSet<CompanyCalendarLineLinkUser>();
        }

        public long CompanyCalendarLineLinkId { get; set; }
        public long? CompanyCalendarLineId { get; set; }
        public string UrlLink { get; set; }
        public string UrlText { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsPrint { get; set; }
        public string Description { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CompanyCalendarLine CompanyCalendarLine { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<CompanyCalendarLineLinkPrintUser> CompanyCalendarLineLinkPrintUser { get; set; }
        public virtual ICollection<CompanyCalendarLineLinkUser> CompanyCalendarLineLinkUser { get; set; }
    }
}
