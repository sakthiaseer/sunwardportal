﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WikiCompany
    {
        public long WikiCompanyId { get; set; }
        public long? CompanyId { get; set; }
        public long? PageId { get; set; }

        public virtual Plant Company { get; set; }
        public virtual WikiPage Page { get; set; }
    }
}
