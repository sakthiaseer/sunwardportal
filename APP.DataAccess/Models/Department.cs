﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Department
    {
        public Department()
        {
            ApplicationAbbreviation = new HashSet<ApplicationAbbreviation>();
            ApplicationUser = new HashSet<ApplicationUser>();
            ApplicationWikiDepartment = new HashSet<ApplicationWiki>();
            ApplicationWikiLineDuty = new HashSet<ApplicationWikiLineDuty>();
            ApplicationWikiProfileDepartment = new HashSet<ApplicationWiki>();
            ApplicationWikiWikiOwner = new HashSet<ApplicationWiki>();
            ApproverSetup = new HashSet<ApproverSetup>();
            Bmrmovement = new HashSet<Bmrmovement>();
            Ccfainformation = new HashSet<Ccfainformation>();
            DocumentProfileNoSeries = new HashSet<DocumentProfileNoSeries>();
            DraftApplicationWikiDepartment = new HashSet<DraftApplicationWiki>();
            DraftApplicationWikiLineDuty = new HashSet<DraftApplicationWikiLineDuty>();
            DraftApplicationWikiProfileDepartment = new HashSet<DraftApplicationWiki>();
            DraftApplicationWikiWikiOwner = new HashSet<DraftApplicationWiki>();
            Employee = new HashSet<Employee>();
            EmployeeInformation = new HashSet<EmployeeInformation>();
            EmployeeReportPerson = new HashSet<EmployeeReportPerson>();
            InventoryType = new HashSet<InventoryType>();
            Ipir = new HashSet<Ipir>();
            IpirAppIssueDep = new HashSet<IpirAppIssueDep>();
            ProcessManPowerSpecialNotes = new HashSet<ProcessManPowerSpecialNotes>();
            ProductActivityCaseResponsDuty = new HashSet<ProductActivityCaseResponsDuty>();
            ProfileAutoNumber = new HashSet<ProfileAutoNumber>();
            Section = new HashSet<Section>();
            TemplateTestCase = new HashSet<TemplateTestCase>();
            TemplateTestCaseCheckListResponseDuty = new HashSet<TemplateTestCaseCheckListResponseDuty>();
            TemplateTestCaseDepartment = new HashSet<TemplateTestCaseDepartment>();
            UserDepartment = new HashSet<UserDepartment>();
            WikiAccess = new HashSet<WikiAccess>();
            WikiPage = new HashSet<WikiPage>();
            WikiPageCopyInfo = new HashSet<WikiPageCopyInfo>();
            WorkFlowApproverSetup = new HashSet<WorkFlowApproverSetup>();
        }

        public long DepartmentId { get; set; }
        public long? CompanyId { get; set; }
        public long? Hodid { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int? HeadCount { get; set; }
        public long? DivisionId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileCode { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual Division Division { get; set; }
        public virtual Employee Hod { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ApplicationAbbreviation> ApplicationAbbreviation { get; set; }
        public virtual ICollection<ApplicationUser> ApplicationUser { get; set; }
        public virtual ICollection<ApplicationWiki> ApplicationWikiDepartment { get; set; }
        public virtual ICollection<ApplicationWikiLineDuty> ApplicationWikiLineDuty { get; set; }
        public virtual ICollection<ApplicationWiki> ApplicationWikiProfileDepartment { get; set; }
        public virtual ICollection<ApplicationWiki> ApplicationWikiWikiOwner { get; set; }
        public virtual ICollection<ApproverSetup> ApproverSetup { get; set; }
        public virtual ICollection<Bmrmovement> Bmrmovement { get; set; }
        public virtual ICollection<Ccfainformation> Ccfainformation { get; set; }
        public virtual ICollection<DocumentProfileNoSeries> DocumentProfileNoSeries { get; set; }
        public virtual ICollection<DraftApplicationWiki> DraftApplicationWikiDepartment { get; set; }
        public virtual ICollection<DraftApplicationWikiLineDuty> DraftApplicationWikiLineDuty { get; set; }
        public virtual ICollection<DraftApplicationWiki> DraftApplicationWikiProfileDepartment { get; set; }
        public virtual ICollection<DraftApplicationWiki> DraftApplicationWikiWikiOwner { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
        public virtual ICollection<EmployeeInformation> EmployeeInformation { get; set; }
        public virtual ICollection<EmployeeReportPerson> EmployeeReportPerson { get; set; }
        public virtual ICollection<InventoryType> InventoryType { get; set; }
        public virtual ICollection<Ipir> Ipir { get; set; }
        public virtual ICollection<IpirAppIssueDep> IpirAppIssueDep { get; set; }
        public virtual ICollection<ProcessManPowerSpecialNotes> ProcessManPowerSpecialNotes { get; set; }
        public virtual ICollection<ProductActivityCaseResponsDuty> ProductActivityCaseResponsDuty { get; set; }
        public virtual ICollection<ProfileAutoNumber> ProfileAutoNumber { get; set; }
        public virtual ICollection<Section> Section { get; set; }
        public virtual ICollection<TemplateTestCase> TemplateTestCase { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponseDuty> TemplateTestCaseCheckListResponseDuty { get; set; }
        public virtual ICollection<TemplateTestCaseDepartment> TemplateTestCaseDepartment { get; set; }
        public virtual ICollection<UserDepartment> UserDepartment { get; set; }
        public virtual ICollection<WikiAccess> WikiAccess { get; set; }
        public virtual ICollection<WikiPage> WikiPage { get; set; }
        public virtual ICollection<WikiPageCopyInfo> WikiPageCopyInfo { get; set; }
        public virtual ICollection<WorkFlowApproverSetup> WorkFlowApproverSetup { get; set; }
    }
}
