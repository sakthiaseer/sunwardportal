﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TaskMaster
    {
        public TaskMaster()
        {
            ApplicationReminder = new HashSet<ApplicationReminder>();
            ApplicationWiki = new HashSet<ApplicationWiki>();
            DraftApplicationWiki = new HashSet<DraftApplicationWiki>();
            InverseParentTask = new HashSet<TaskMaster>();
            TaskAppointmentSubTask = new HashSet<TaskAppointment>();
            TaskAppointmentTaskMaster = new HashSet<TaskAppointment>();
            TaskAssigned = new HashSet<TaskAssigned>();
            TaskAttachment = new HashSet<TaskAttachment>();
            TaskComment = new HashSet<TaskComment>();
            TaskDiscussion = new HashSet<TaskDiscussion>();
            TaskInviteUser = new HashSet<TaskInviteUser>();
            TaskLikes = new HashSet<TaskLikes>();
            TaskLinksLinkedTask = new HashSet<TaskLinks>();
            TaskLinksTask = new HashSet<TaskLinks>();
            TaskMembers = new HashSet<TaskMembers>();
            TaskNotes = new HashSet<TaskNotes>();
            TaskProject = new HashSet<TaskProject>();
            TaskTag = new HashSet<TaskTag>();
            TaskUnReadNotes = new HashSet<TaskUnReadNotes>();
        }

        public long TaskId { get; set; }
        public long? MainTaskId { get; set; }
        public long? ParentTaskId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public long? AssignedTo { get; set; }
        public DateTime? AssignedDate { get; set; }
        public DateTime? DueDate { get; set; }
        public long? ProjectId { get; set; }
        public long? OnBehalf { get; set; }
        public long? OwnerId { get; set; }
        public string FollowUp { get; set; }
        public string OverDueAllowed { get; set; }
        public bool? IsUrgent { get; set; }
        public DateTime? DiscussionDate { get; set; }
        public bool? IsRead { get; set; }
        public int? OrderNo { get; set; }
        public string Version { get; set; }
        public int? WorkTypeId { get; set; }
        public int? RequestTypeId { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string TaskNumber { get; set; }
        public int? TaskLevel { get; set; }
        public bool? IsNoDueDate { get; set; }
        public bool? IsAutoClose { get; set; }
        public bool? IsNotifyByMessage { get; set; }
        public bool? IsEmail { get; set; }
        public Guid? VersionSessionId { get; set; }
        public bool? IsAllowEditContent { get; set; }
        public DateTime? NewDueDate { get; set; }
        public long? SourceId { get; set; }
        public bool? IsFromProfileDocument { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationUser OnBehalfNavigation { get; set; }
        public virtual ApplicationUser Owner { get; set; }
        public virtual TaskMaster ParentTask { get; set; }
        public virtual CodeMaster RequestType { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CodeMaster WorkType { get; set; }
        public virtual ICollection<ApplicationReminder> ApplicationReminder { get; set; }
        public virtual ICollection<ApplicationWiki> ApplicationWiki { get; set; }
        public virtual ICollection<DraftApplicationWiki> DraftApplicationWiki { get; set; }
        public virtual ICollection<TaskMaster> InverseParentTask { get; set; }
        public virtual ICollection<TaskAppointment> TaskAppointmentSubTask { get; set; }
        public virtual ICollection<TaskAppointment> TaskAppointmentTaskMaster { get; set; }
        public virtual ICollection<TaskAssigned> TaskAssigned { get; set; }
        public virtual ICollection<TaskAttachment> TaskAttachment { get; set; }
        public virtual ICollection<TaskComment> TaskComment { get; set; }
        public virtual ICollection<TaskDiscussion> TaskDiscussion { get; set; }
        public virtual ICollection<TaskInviteUser> TaskInviteUser { get; set; }
        public virtual ICollection<TaskLikes> TaskLikes { get; set; }
        public virtual ICollection<TaskLinks> TaskLinksLinkedTask { get; set; }
        public virtual ICollection<TaskLinks> TaskLinksTask { get; set; }
        public virtual ICollection<TaskMembers> TaskMembers { get; set; }
        public virtual ICollection<TaskNotes> TaskNotes { get; set; }
        public virtual ICollection<TaskProject> TaskProject { get; set; }
        public virtual ICollection<TaskTag> TaskTag { get; set; }
        public virtual ICollection<TaskUnReadNotes> TaskUnReadNotes { get; set; }
    }
}
