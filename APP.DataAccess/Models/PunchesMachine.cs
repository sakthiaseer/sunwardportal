﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PunchesMachine
    {
        public long PunchesMachineId { get; set; }
        public long? CommonMasterMouldChangePartsId { get; set; }
        public long? PunchesCommonMachineId { get; set; }

        public virtual CommonMasterMouldChangeParts CommonMasterMouldChangeParts { get; set; }
        public virtual CommonFieldsProductionMachine PunchesCommonMachine { get; set; }
    }
}
