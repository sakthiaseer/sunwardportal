﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TenderPeriodPricingLine
    {
        public long TenderPeriodPricingLineId { get; set; }
        public long? TenderPeriodPricingId { get; set; }
        public long? ProductId { get; set; }
        public long? Currency { get; set; }
        public decimal? Price { get; set; }
        public long? Uomid { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual SocustomersItemCrossReference Product { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual TenderPeriodPricing TenderPeriodPricing { get; set; }
    }
}
