﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AppTranStock
    {
        public AppTranStock()
        {
            AppTranStockLine = new HashSet<AppTranStockLine>();
        }

        public int Id { get; set; }
        public string RequisitionNo { get; set; }
        public string LocationFrom { get; set; }
        public string LocationTo { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public string TransferNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<AppTranStockLine> AppTranStockLine { get; set; }
    }
}
