﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WorkFlowPages
    {
        public WorkFlowPages()
        {
            DynamicFlowDetail = new HashSet<DynamicFlowDetail>();
            DynamicFlowProgress = new HashSet<DynamicFlowProgress>();
            WorkFlowPageBlock = new HashSet<WorkFlowPageBlock>();
        }

        public long WorkFlowPageId { get; set; }
        public long? CompanyId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long? TableId { get; set; }
        public string TableName { get; set; }
        public string VersionNo { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<DynamicFlowDetail> DynamicFlowDetail { get; set; }
        public virtual ICollection<DynamicFlowProgress> DynamicFlowProgress { get; set; }
        public virtual ICollection<WorkFlowPageBlock> WorkFlowPageBlock { get; set; }
    }
}
