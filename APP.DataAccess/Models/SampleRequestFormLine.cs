﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SampleRequestFormLine
    {
        public SampleRequestFormLine()
        {
            IssueRequestSampleLine = new HashSet<IssueRequestSampleLine>();
        }

        public long SampleRequestFormLineId { get; set; }
        public long? SampleRequestFormId { get; set; }
        public long? ItemId { get; set; }
        public decimal? QtyRequire { get; set; }
        public decimal? QtyIssue { get; set; }
        public bool? IsSpecificBatch { get; set; }
        public string SpecialNotes { get; set; }
        public string BatchNo { get; set; }
        public bool? InStockSameItem { get; set; }
        public long? InventoryItemId { get; set; }
        public string Factor { get; set; }
        public DateTime? MfgDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string SpecialNotesIssue { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public decimal? BalanceQty { get; set; }
        public long? NavCompanyId { get; set; }
        public decimal? TotalIssueQty { get; set; }
        public int? IssueStatusId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems InventoryItem { get; set; }
        public virtual CodeMaster IssueStatus { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Navlocation NavCompany { get; set; }
        public virtual SampleRequestForm SampleRequestForm { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<IssueRequestSampleLine> IssueRequestSampleLine { get; set; }
    }
}
