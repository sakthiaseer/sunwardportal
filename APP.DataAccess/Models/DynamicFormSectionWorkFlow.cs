﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormSectionWorkFlow
    {
        public long DynamicFormSectionWorkFlowId { get; set; }
        public long? DynamicFormSectionId { get; set; }
        public long? DynamicFormSectionSecurityId { get; set; }
        public long? UserId { get; set; }

        public virtual DynamicFormSection DynamicFormSection { get; set; }
        public virtual DynamicFormSectionSecurity DynamicFormSectionSecurity { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
