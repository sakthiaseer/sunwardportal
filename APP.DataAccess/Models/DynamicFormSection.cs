﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormSection
    {
        public DynamicFormSection()
        {
            DynamicFormDataSectionLock = new HashSet<DynamicFormDataSectionLock>();
            DynamicFormDataUpload = new HashSet<DynamicFormDataUpload>();
            DynamicFormSectionAttribute = new HashSet<DynamicFormSectionAttribute>();
            DynamicFormSectionAttributeSection = new HashSet<DynamicFormSectionAttributeSection>();
            DynamicFormSectionSecurity = new HashSet<DynamicFormSectionSecurity>();
            DynamicFormSectionWorkFlow = new HashSet<DynamicFormSectionWorkFlow>();
            DynamicFormWorkFlowSection = new HashSet<DynamicFormWorkFlowSection>();
            DynamicFormWorkFlowSectionForm = new HashSet<DynamicFormWorkFlowSectionForm>();
        }

        public long DynamicFormSectionId { get; set; }
        public string SectionName { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? DynamicFormId { get; set; }
        public int? SortOrderBy { get; set; }
        public bool? IsReadWrite { get; set; }
        public bool? IsReadOnly { get; set; }
        public bool? IsVisible { get; set; }
        public string Instruction { get; set; }
        public bool? IsDeleted { get; set; }
        public long? SectionFileProfileTypeId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual DynamicForm DynamicForm { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual FileProfileType SectionFileProfileType { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<DynamicFormDataSectionLock> DynamicFormDataSectionLock { get; set; }
        public virtual ICollection<DynamicFormDataUpload> DynamicFormDataUpload { get; set; }
        public virtual ICollection<DynamicFormSectionAttribute> DynamicFormSectionAttribute { get; set; }
        public virtual ICollection<DynamicFormSectionAttributeSection> DynamicFormSectionAttributeSection { get; set; }
        public virtual ICollection<DynamicFormSectionSecurity> DynamicFormSectionSecurity { get; set; }
        public virtual ICollection<DynamicFormSectionWorkFlow> DynamicFormSectionWorkFlow { get; set; }
        public virtual ICollection<DynamicFormWorkFlowSection> DynamicFormWorkFlowSection { get; set; }
        public virtual ICollection<DynamicFormWorkFlowSectionForm> DynamicFormWorkFlowSectionForm { get; set; }
    }
}
