﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmployeeIctinformation
    {
        public EmployeeIctinformation()
        {
            EmployeeIctrole = new HashSet<EmployeeIctrole>();
        }

        public long EmployeeIctinformationId { get; set; }
        public long? EmployeeId { get; set; }
        public long? SoftwareId { get; set; }
        public string LoginId { get; set; }
        public string Password { get; set; }
        public long? RoleId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsPortal { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationRole Role { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<EmployeeIctrole> EmployeeIctrole { get; set; }
    }
}
