﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SalesSurveyByFieldForce
    {
        public SalesSurveyByFieldForce()
        {
            SalesSurveyByFieldForceLine = new HashSet<SalesSurveyByFieldForceLine>();
        }

        public long SalesSurveyByFieldForceId { get; set; }
        public long? SalesPersonId { get; set; }
        public long? ClinicId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string AddressName { get; set; }
        public string AddressDetails { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual LocalClinic Clinic { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationUser SalesPerson { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<SalesSurveyByFieldForceLine> SalesSurveyByFieldForceLine { get; set; }
    }
}
