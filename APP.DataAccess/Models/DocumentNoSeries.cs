﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DocumentNoSeries
    {
        public DocumentNoSeries()
        {
            ProfileDocument = new HashSet<ProfileDocument>();
        }

        public long NumberSeriesId { get; set; }
        public long? ProfileId { get; set; }
        public string DocumentNo { get; set; }
        public string VersionNo { get; set; }
        public string Title { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? Implementation { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? NextReviewDate { get; set; }
        public DateTime? Date { get; set; }
        public long? RequestorId { get; set; }
        public string Link { get; set; }
        public string ReasonToVoid { get; set; }
        public string Description { get; set; }
        public Guid? SessionId { get; set; }
        public bool? IsUpload { get; set; }
        public long? FileProfileTypeId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual FileProfileType FileProfileType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual ApplicationUser Requestor { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ProfileDocument> ProfileDocument { get; set; }
    }
}
