﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProcessMachineTimeProductionLineLine
    {
        public ProcessMachineTimeProductionLineLine()
        {
            ProcessMachineTimeProductionMachineProcess = new HashSet<ProcessMachineTimeProductionMachineProcess>();
        }

        public long ProcessMachineTimeProductionLineLineId { get; set; }
        public long? ProcessMachineTimeProductionLineId { get; set; }
        public string OperationStepNo { get; set; }
        public string Description { get; set; }
        public string Preference { get; set; }
        public long? ProductionCentreStepsId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProcessMachineTimeProductionLine ProcessMachineTimeProductionLine { get; set; }
        public virtual ApplicationMasterChild ProductionCentreSteps { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ProcessMachineTimeProductionMachineProcess> ProcessMachineTimeProductionMachineProcess { get; set; }
    }
}
