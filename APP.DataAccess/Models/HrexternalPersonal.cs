﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class HrexternalPersonal
    {
        public long HrexternalPersonalId { get; set; }
        public long? CompanyListingId { get; set; }
        public long? PurposeForExternalPersonalId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }
        public string Gender { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Icinformation { get; set; }
        public string HandphoneNo { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CompanyListing CompanyListing { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
