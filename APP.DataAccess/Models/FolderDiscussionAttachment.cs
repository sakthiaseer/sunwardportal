﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FolderDiscussionAttachment
    {
        public long FolderDiscussionAttachmentId { get; set; }
        public long? FolderDiscussionId { get; set; }
        public long? DocumentId { get; set; }
        public string FileName { get; set; }
        public long? UserId { get; set; }

        public virtual Documents Document { get; set; }
        public virtual FolderDiscussion FolderDiscussion { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
