﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.DataAccess.Models
{
   public class view_GetWorkOrderReport
    {
        public long WorkOrderId { get; set; }
        public int? AssignmentId { get; set; }
        public string AssignmentName { get; set; }
        public long? RequirementId { get; set; }
        public string RequirementName { get; set; }
        
        public long? AssignTo { get; set; }        
        public long WorkOrderLineId { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public int? PriorityId { get; set; }
        public string PriorityName { get; set; }
        public int? SunwardStatusId { get; set; }
        public string SunwardStatus { get; set; }
        public DateTime? StatusUpdateDate { get; set; }
        public DateTime? ExpectedReleaseDate { get; set; }
        public string TaskLink { get; set; }
        public string ResultLink { get; set; }
        public int? CrtstatusId { get; set; }
        public string Crtstatus { get; set; }        
       
        public long? PermissionID { get; set; }
       
        public string PermissionName { get; set; }
        public string NavisionModuleName { get; set; }
        public int? StatusCodeID { get; set; }
       
        public string StatusCode { get; set; }       
        public long? AddedByUserID { get; set; }       
        public string AddedByUser { get; set; }       
        public DateTime? AddedDate { get; set; }       
        public string ModifiedByUser { get; set; }
        public long? ModifiedByUserID { get; set; }       
        public DateTime? ModifiedDate { get; set; }
        public long? UserGroupID { get; set; }      
        public string ProfileNo { get; set; }


    }
}
