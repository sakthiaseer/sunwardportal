﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.DataAccess.Models
{
    public class view_AcByDistributor
    {
        public long ACEntryLineId { get; set; }
        public string ACFromDate { get; set; }
        public DateTime? FromDate { get; set; }
        public string ACTODate { get; set; }
        public DateTime? ToDate { get; set; }
        public long? CustomerId { get; set; }
        public long? ItemId { get; set; }
        public decimal? ACQty { get; set; }
        public string No { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public long? MethodCodeID { get; set; }
        public string MethodName { get; set; }
        public string MethodDescription { get; set; }
        public decimal? SellingPrice { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal? SaleAmount { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
