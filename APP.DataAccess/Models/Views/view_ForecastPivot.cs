﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.DataAccess.Models
{
    public class view_ForecastPivot
    {
        public long ACEntryLineId { get; set; }
        public string ACFromDate { set; get; }

        public DateTime FromDate { get; set; }
        public string ACToDate { get; set; }

        public DateTime ToDate { get; set; }

        public long CustomerId { get; set; }
        public long ItemId { get; set; }
        public decimal ACQty { get; set; }
        public string No { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public long MethodCodeID { get; set; }
        public string MethodName { get; set; }
        public string MethodDescription { get; set; }
        public string TableName { get; set; }
        public string ProdOrder { get; set; }
        public decimal? ProdQty { get; set; }
        public string DocumentNo { get; set; }
        public decimal? SOQty { get; set; }
        public decimal? Quantity { get; set; }
    }
}
