﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.DataAccess.Models
{
   public class View_UserPermission
    {
        public long PermissionID { get; set; }
        public string PermissionName { get; set; }
        public string PermissionCode { get; set; }
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public string Icon { get; set; }
        public string MenuId { get; set; }
        public long? ParentID { get; set; }
        public int PermissionLevel { get; set; }
        public string PermissionURL { get; set; }
        public string PermissionGroup { get; set; }
        public string PermissionOrder { get; set; }
        public bool IsDisplay { get; set; }
       // public long AppplicationPermissionID { get; set; }
    }
}
