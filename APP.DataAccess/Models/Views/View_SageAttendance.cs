﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.DataAccess
{
    public class View_SageAttendance
    {
        public string EmpID { get; set; }
        public DateTime? SubmitDate { get; set; }
        public string Code { get; set; }
        public string Remarks { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public double? Duration { get; set; }
        public long? PlantID { get; set; }
    }
}
