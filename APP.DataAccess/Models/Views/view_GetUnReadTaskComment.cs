﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.DataAccess.Models
{
    public class view_GetUnReadTaskComment
    {
        public long TaskCommentID { get; set; }
        public long? TaskMasterID { get; set; }
        public long? MainTaskId { get; set; }
        public string Comment { get; set; }
        public long? CommentedBy { get; set; }

        public string CommentedByName { get; set; }
        // [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MMM-yyyy HH:mm:ss tt}")]
        public DateTime? CommentedDate { get; set; }
        public long? ParentCommentId { get; set; }       
        public bool? IsRead { get; set; }  
        
        //public List<TaskCommentUser> TaskCommentUsers { get; set; }
       
        
        public string TaskSubject { get; set; }
        public string TaskName { get; set; }          
       
       
        public DateTime? TaskDueDate { get; set; }    

        public bool? IsNoDueDate { get; set; }
       
    }
}
