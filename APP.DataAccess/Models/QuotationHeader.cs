﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class QuotationHeader
    {
        public QuotationHeader()
        {
            QuotationLine = new HashSet<QuotationLine>();
        }

        public long QuotationId { get; set; }
        public string QuotationHeaderNo { get; set; }
        public long? CountryId { get; set; }
        public int? CustomerTypeId { get; set; }
        public long? CustomerId { get; set; }
        public string ContactPerson { get; set; }
        public int? CategoryId { get; set; }
        public long? CurencyCodeId { get; set; }
        public int? ShipmentId { get; set; }
        public string Remarks { get; set; }
        public string SessionId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster Category { get; set; }
        public virtual Plant Country { get; set; }
        public virtual CurrencyCode CurencyCode { get; set; }
        public virtual CodeMaster CustomerType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster Shipment { get; set; }
        public virtual ICollection<QuotationLine> QuotationLine { get; set; }
    }
}
