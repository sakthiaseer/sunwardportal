﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ToDoNotes
    {
        public long Id { get; set; }
        public string Notes { get; set; }
        public int? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public int? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public string Completed { get; set; }
        public long? TopicId { get; set; }
    }
}
