﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AssetEquipmentMaintenaceMasterAssetDocument
    {
        public long AssetEquipmentMaintenaceMasterAssetDocumentId { get; set; }
        public long? AssetEquipmentMaintenaceMasterId { get; set; }
        public long? AssetDocumentId { get; set; }

        public virtual ApplicationMasterDetail AssetDocument { get; set; }
        public virtual AssetEquipmentMaintenaceMaster AssetEquipmentMaintenaceMaster { get; set; }
    }
}
