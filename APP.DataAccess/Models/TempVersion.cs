﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TempVersion
    {
        public long TempVersionId { get; set; }
        public long? PersonInchargeId { get; set; }
        public long? VersionId { get; set; }
        public string ApproverRemarks { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationUser PersonIncharge { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual TableDataVersionInfo Version { get; set; }
    }
}
