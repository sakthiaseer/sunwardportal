﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionActivityInformation
    {
        public long ProductionActivityInformationId { get; set; }
        public long? CountryId { get; set; }
        public long? TicketNo { get; set; }
        public long? BatchNo { get; set; }
        public long? ProcessId { get; set; }
        public long? UploadDocumentId { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterDetail Country { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationMasterDetail Process { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationMasterDetail UploadDocument { get; set; }
    }
}
