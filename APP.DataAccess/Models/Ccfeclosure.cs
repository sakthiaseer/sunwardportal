﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Ccfeclosure
    {
        public long CcfeclosureId { get; set; }
        public bool? IsSatisfactory { get; set; }
        public bool? IsNotSatisfactory { get; set; }
        public string Comments { get; set; }
        public long? ClosedBy { get; set; }
        public DateTime? Date { get; set; }
        public bool? IsSatisfactoryForNotSatisfactory { get; set; }
        public long? NotSatisfactoryClosedBy { get; set; }
        public DateTime? NotSatisfactoryDate { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
    }
}
