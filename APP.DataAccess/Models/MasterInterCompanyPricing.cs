﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class MasterInterCompanyPricing
    {
        public MasterInterCompanyPricing()
        {
            MasterInterCompanyPricingLine = new HashSet<MasterInterCompanyPricingLine>();
        }

        public long MasterInterCompanyPricingId { get; set; }
        public DateTime? ValidityFrom { get; set; }
        public DateTime? ValiditiyTo { get; set; }
        public bool? IsNeedVersionFunction { get; set; }
        public long? FromCompanyId { get; set; }
        public long? ToCompanyId { get; set; }
        public decimal? InterCompanyMinMargin { get; set; }
        public decimal? InterCompanyProfitSharing { get; set; }
        public decimal? MinBillingToCustomer { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? CurrencyId { get; set; }
        public Guid? VersionSessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant FromCompany { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual Plant ToCompany { get; set; }
        public virtual ICollection<MasterInterCompanyPricingLine> MasterInterCompanyPricingLine { get; set; }
    }
}
