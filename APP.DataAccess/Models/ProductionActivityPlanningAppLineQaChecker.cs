﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionActivityPlanningAppLineQaChecker
    {
        public long ProductionActivityPlanningAppLineQaCheckerId { get; set; }
        public long? ProductionActivityPlanningAppLineId { get; set; }
        public bool? QaCheck { get; set; }
        public long? QaCheckUserId { get; set; }
        public DateTime? QaCheckDate { get; set; }

        public virtual ProductionActivityPlanningAppLine ProductionActivityPlanningAppLine { get; set; }
        public virtual ApplicationUser QaCheckUser { get; set; }
    }
}
