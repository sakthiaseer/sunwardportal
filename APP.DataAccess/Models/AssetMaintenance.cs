﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AssetMaintenance
    {
        public long AssetMaintenanceId { get; set; }
        public long? AssetId { get; set; }
        public DateTime? MaintenanceDate { get; set; }
        public DateTime? NextMaintenanceDate { get; set; }
        public string Description { get; set; }
        public long? PerformedBy { get; set; }
        public decimal? Cost { get; set; }
        public int? DownDuration { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual AssetMaster Asset { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationUser PerformedByNavigation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
