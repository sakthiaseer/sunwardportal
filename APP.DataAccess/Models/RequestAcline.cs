﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class RequestAcline
    {
        public long RequestAclineId { get; set; }
        public long? RequestAcid { get; set; }
        public long? NavItemId { get; set; }
        public long? DistributorItemId { get; set; }
        public string ShelfLife { get; set; }
        public decimal? Avg12Months { get; set; }
        public decimal? Avg6Months { get; set; }
        public decimal? Avg3Months { get; set; }
        public decimal? CurrentAc { get; set; }
        public decimal? ProposedNewAc { get; set; }
        public decimal? DifferenceInAc { get; set; }
        public decimal? PercentDifference { get; set; }
        public string Comments { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Acitems DistributorItem { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Navitems NavItem { get; set; }
        public virtual RequestAc RequestAc { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
