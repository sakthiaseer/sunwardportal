﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewFmglobalAddess
    {
        public long? AddressId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string Email { get; set; }
        public int? OfficePhone { get; set; }
        public int? PostCode { get; set; }
        public string Website { get; set; }
        public long? CountryId { get; set; }
        public long? StateId { get; set; }
        public long? CityId { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public long FmglobalAddessId { get; set; }
        public string AddressType { get; set; }
        public bool? IsBilling { get; set; }
        public bool? IsShipping { get; set; }
        public long FmglobalId { get; set; }
    }
}
