﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductActivityCaseResponsWeekly
    {
        public long ProductActivityCaseResponsWeeklyId { get; set; }
        public long? ProductActivityCaseResponsId { get; set; }
        public int? WeeklyId { get; set; }
        public string CustomType { get; set; }

        public virtual ProductActivityCaseRespons ProductActivityCaseRespons { get; set; }
        public virtual CodeMaster Weekly { get; set; }
    }
}
