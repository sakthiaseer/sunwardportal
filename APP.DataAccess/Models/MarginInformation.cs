﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class MarginInformation
    {
        public MarginInformation()
        {
            MarginInformationLine = new HashSet<MarginInformationLine>();
        }

        public long MarginInformationId { get; set; }
        public long? DistirbutorId { get; set; }
        public long? DistributeForId { get; set; }
        public long? CompanyId { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
        public bool? NeedToKeepTrackVersion { get; set; }
        public decimal? MarginPercentage { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? VersionSessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CompanyListing Distirbutor { get; set; }
        public virtual Plant DistributeFor { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<MarginInformationLine> MarginInformationLine { get; set; }
    }
}
