﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FpproductLine
    {
        public long FpproductLineId { get; set; }
        public long? DatabaseId { get; set; }
        public long? NavisionId { get; set; }
        public long? FpproductId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Fpproduct Fpproduct { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Navitems Navision { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
