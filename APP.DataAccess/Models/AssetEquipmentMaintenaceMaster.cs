﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AssetEquipmentMaintenaceMaster
    {
        public AssetEquipmentMaintenaceMaster()
        {
            AssetEquipmentMaintenaceMasterAssetDocument = new HashSet<AssetEquipmentMaintenaceMasterAssetDocument>();
        }

        public long AssetEquipmentMaintenaceMasterId { get; set; }
        public long? AssetPartsMaintenaceMasterId { get; set; }
        public string IsCalibarion { get; set; }
        public long? PreventiveMaintenaceId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual AssetPartsMaintenaceMaster AssetPartsMaintenaceMaster { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationMasterDetail PreventiveMaintenace { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<AssetEquipmentMaintenaceMasterAssetDocument> AssetEquipmentMaintenaceMasterAssetDocument { get; set; }
    }
}
