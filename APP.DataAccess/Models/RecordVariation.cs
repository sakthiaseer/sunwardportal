﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class RecordVariation
    {
        public RecordVariation()
        {
            RecordVariationDocumentLink = new HashSet<RecordVariationDocumentLink>();
            RecordVariationLine = new HashSet<RecordVariationLine>();
        }

        public long RecordVariationId { get; set; }
        public int? RegisterationCodeId { get; set; }
        public string RelatedChangeControlNo { get; set; }
        public DateTime? SubmissionDate { get; set; }
        public long? RegistrationHolderId { get; set; }
        public long? RegisterCountryId { get; set; }
        public long? ProductId { get; set; }
        public string VariationNo { get; set; }
        public DateTime? SubmittedPaidDate { get; set; }
        public DateTime? EstimateApprovalDate { get; set; }
        public string DocumentLink { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? SubmissionStatusId { get; set; }
        public DateTime? EstimateSubmissionDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual FinishProductGeneralInfo Product { get; set; }
        public virtual CodeMaster RegisterationCode { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CodeMaster SubmissionStatus { get; set; }
        public virtual ICollection<RecordVariationDocumentLink> RecordVariationDocumentLink { get; set; }
        public virtual ICollection<RecordVariationLine> RecordVariationLine { get; set; }
    }
}
