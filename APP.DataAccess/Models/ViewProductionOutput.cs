﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewProductionOutput
    {
        public string LocationName { get; set; }
        public string ProductionOrderNo { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string BatchNo { get; set; }
        public string SubLotNo { get; set; }
        public string DrumNo { get; set; }
        public string Buom { get; set; }
        public bool? IsLotComplete { get; set; }
        public bool? IsProdutionOrderComplete { get; set; }
        public decimal? NetWeight { get; set; }
        public string AddedBy { get; set; }
        public DateTime? AddedDate { get; set; }
        public long ProductionOutputId { get; set; }
    }
}
