﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormSectionAttributeSecurity
    {
        public long DynamicFormSectionAttributeSecurityId { get; set; }
        public long? DynamicFormSectionAttributeId { get; set; }
        public long? UserId { get; set; }
        public long? UserGroupId { get; set; }
        public long? LevelId { get; set; }
        public bool? IsAccess { get; set; }
        public bool? IsViewFormatOnly { get; set; }
        public string UserType { get; set; }

        public virtual DynamicFormSectionAttribute DynamicFormSectionAttribute { get; set; }
        public virtual LevelMaster Level { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual UserGroup UserGroup { get; set; }
    }
}
