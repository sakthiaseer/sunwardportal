﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SalesMainPromotionList
    {
        public long SalesMainPromotionListId { get; set; }
        public long? SalesPromotionId { get; set; }
        public long? GenericCodeId { get; set; }
        public bool? IsFocList { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual GenericCodes GenericCode { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual SalesPromotion SalesPromotion { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
