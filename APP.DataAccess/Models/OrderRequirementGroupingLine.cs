﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class OrderRequirementGroupingLine
    {
        public OrderRequirementGroupingLine()
        {
            OrderRequirementGrouPinglineSplit = new HashSet<OrderRequirementGrouPinglineSplit>();
        }

        public long OrderRequirementGroupingLineId { get; set; }
        public long? ProductionSimulationGroupingId { get; set; }
        public long? ProductId { get; set; }
        public string TicketBatchSizeId { get; set; }
        public decimal? NoOfTicket { get; set; }
        public long? NavLocationId { get; set; }
        public long? NavUomid { get; set; }
        public DateTime? ExpectedStartDate { get; set; }
        public decimal? ProductQty { get; set; }
        public bool? RequireToSplit { get; set; }
        public string Remarks { get; set; }
        public bool? IsNavSync { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Navitems Product { get; set; }
        public virtual ProductionSimulationGrouping ProductionSimulationGrouping { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<OrderRequirementGrouPinglineSplit> OrderRequirementGrouPinglineSplit { get; set; }
    }
}
