﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WikiGroup
    {
        public WikiGroup()
        {
            WikiPage = new HashSet<WikiPage>();
        }

        public long WikiGroupId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool? RequiredNotification { get; set; }
        public bool? IsTestPaper { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<WikiPage> WikiPage { get; set; }
    }
}
