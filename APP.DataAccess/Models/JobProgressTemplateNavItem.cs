﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class JobProgressTemplateNavItem
    {
        public long JobProgressTemplateNavItemId { get; set; }
        public long? JobProgressTemplateId { get; set; }
        public long? ItemId { get; set; }

        public virtual Navitems Item { get; set; }
        public virtual JobProgressTemplate JobProgressTemplate { get; set; }
    }
}
