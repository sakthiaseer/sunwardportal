﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WorkDayShift
    {
        public long WorkdayShiftId { get; set; }
        public long? WorkDatyId { get; set; }
        public long? ShiftId { get; set; }

        public virtual ShiftMaster Shift { get; set; }
        public virtual CompanyWorkDayMaster WorkDaty { get; set; }
    }
}
