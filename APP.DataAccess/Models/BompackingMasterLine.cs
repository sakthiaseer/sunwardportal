﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class BompackingMasterLine
    {
        public long BompackingMasterLineId { get; set; }
        public long? BompackingMasterId { get; set; }
        public long? SetTypeId { get; set; }
        public string No { get; set; }
        public string ItemName { get; set; }
        public int? CalculationMethodId { get; set; }
        public decimal? Qty { get; set; }
        public long? Uomid { get; set; }
        public string Bominformation { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual BompackingMaster BompackingMaster { get; set; }
        public virtual CodeMaster CalculationMethod { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
