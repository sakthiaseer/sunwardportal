﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PackagingTube
    {
        public long TubeId { get; set; }
        public bool? IsPrinted { get; set; }
        public decimal? Diameter { get; set; }
        public decimal? Width { get; set; }
        public decimal? Length { get; set; }
        public int? NoOfColor { get; set; }
        public long? PantoneColorId { get; set; }
        public long? ColorId { get; set; }
        public long? LetteringId { get; set; }
        public long? PackagingMaterialId { get; set; }
        public long? PackagingItemId { get; set; }
        public long? PackingUnitId { get; set; }
        public int? TubeTypeId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string LinkeProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public long? CoatingMaterialId { get; set; }
        public bool? IsVersion { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CodeMaster TubeType { get; set; }
    }
}
