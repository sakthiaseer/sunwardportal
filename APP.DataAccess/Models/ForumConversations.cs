﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ForumConversations
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public long? TopicId { get; set; }
        public string Message { get; set; }
        public byte[] FileData { get; set; }
        public long? ParticipantId { get; set; }
        public long? ReplyId { get; set; }
        public DateTime? AddedDate { get; set; }
        public int? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid SessionId { get; set; }
        public int? AddedByUserId { get; set; }
        public int? StatusCodeId { get; set; }
    }
}
