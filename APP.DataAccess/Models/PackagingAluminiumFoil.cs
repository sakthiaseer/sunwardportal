﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PackagingAluminiumFoil
    {
        public PackagingAluminiumFoil()
        {
            PackagingAluminiumFoilLine = new HashSet<PackagingAluminiumFoilLine>();
        }

        public long AluminiumFoilId { get; set; }
        public bool? IsPrinted { get; set; }
        public int? NoOfColor { get; set; }
        public long? PantoneColorId { get; set; }
        public long? ColorId { get; set; }
        public long? LetteringId { get; set; }
        public long? BgPantoneColorId { get; set; }
        public long? BgColorId { get; set; }
        public decimal? FoilThickness { get; set; }
        public decimal? FoilWidth { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public long? ReversePrint { get; set; }
        public decimal? WtPerRoll { get; set; }
        public decimal? LengthPerRoll { get; set; }
        public int? AluminiumFoilTypeId { get; set; }
        public bool? IsVersion { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster AluminiumFoilType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<PackagingAluminiumFoilLine> PackagingAluminiumFoilLine { get; set; }
    }
}
