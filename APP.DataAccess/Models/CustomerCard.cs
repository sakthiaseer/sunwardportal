﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CustomerCard
    {
        public long CustomerId { get; set; }
        public int? NavCutNo { get; set; }
        public string CustomerName { get; set; }
        public string UnitNo { get; set; }
        public string StreetAdress { get; set; }
        public string Postcode { get; set; }
        public string Postcode1 { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string City1 { get; set; }
        public string RegionCode { get; set; }
        public string RegionCode1 { get; set; }
        public string PhoneNo { get; set; }
        public string ContactPerson { get; set; }
        public string FaxNo { get; set; }
        public string EmailAddress { get; set; }
        public string HomePage { get; set; }
        public string ShipmentMethodCode { get; set; }
        public TimeSpan? ShippingTime { get; set; }
        public string ShippingAgentCode { get; set; }
        public string ShippingAgentServiceCode { get; set; }
        public string GstregistrationNo { get; set; }
        public string BusRegistrationNo { get; set; }
        public DateTime? GsteffectiveDate { get; set; }
        public string PaymentMethod { get; set; }
        public string PaymentTerms { get; set; }
        public string CurrencyCode { get; set; }
        public string GenBusPostingGroup { get; set; }
        public string GstbusPostingGroup { get; set; }
        public string CustPostingGroup { get; set; }
        public string ShipingCode { get; set; }
        public string ShipName { get; set; }
        public string ShipName2 { get; set; }
        public string ShippingUnitNo { get; set; }
        public string ShippingStreetAddress { get; set; }
        public string ShipPostcode { get; set; }
        public string ShipCountry { get; set; }
        public string ShipCity { get; set; }
        public string ShipRegion { get; set; }
        public string ShipPhoneNo { get; set; }
        public string ShipFaxNo { get; set; }
        public string ShipEmailAddress { get; set; }
        public string ShipHomePage { get; set; }
        public string ShiptoShipmentMethod { get; set; }
        public string ShiptoAgentCode { get; set; }
        public string ShiptoAgentServiceCode { get; set; }
        public string CreationLevel { get; set; }
        public long? AddedByUser { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
