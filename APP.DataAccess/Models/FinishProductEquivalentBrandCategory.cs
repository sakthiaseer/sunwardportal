﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FinishProductEquivalentBrandCategory
    {
        public long EquivalentBrandId { get; set; }
        public long? FinishProductId { get; set; }
        public long? EquivalentBrandCategoryId { get; set; }

        public virtual FinishProduct FinishProduct { get; set; }
    }
}
