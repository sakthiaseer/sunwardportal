﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ContactActivities
    {
        public long ContactActivitiesId { get; set; }
        public long? ContactId { get; set; }
        public int? Type { get; set; }
        public string Summary { get; set; }

        public virtual Contacts Contact { get; set; }
        public virtual CodeMaster TypeNavigation { get; set; }
    }
}
