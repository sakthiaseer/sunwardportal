﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class JobProgressTemplateLineProcessPia
    {
        public long JobProgressTemplateLineProcessPiaId { get; set; }
        public long? JobProgressTemplateLineLineProcessId { get; set; }
        public long? Pia { get; set; }

        public virtual JobProgressTemplateLineLineProcess JobProgressTemplateLineLineProcess { get; set; }
        public virtual ApplicationUser PiaNavigation { get; set; }
    }
}
