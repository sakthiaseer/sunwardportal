﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Designation
    {
        public Designation()
        {
            ApplicationWikiLineDuty = new HashSet<ApplicationWikiLineDuty>();
            DesignationHeadCount = new HashSet<DesignationHeadCount>();
            DraftApplicationWikiLineDuty = new HashSet<DraftApplicationWikiLineDuty>();
            Employee = new HashSet<Employee>();
            EmployeeInformation = new HashSet<EmployeeInformation>();
            EmployeeOtherDutyInformation = new HashSet<EmployeeOtherDutyInformation>();
            EmployeeReportPerson = new HashSet<EmployeeReportPerson>();
            EmployeeResignation = new HashSet<EmployeeResignation>();
            ProductActivityCaseResponsDuty = new HashSet<ProductActivityCaseResponsDuty>();
            TemplateTestCaseCheckListResponseDuty = new HashSet<TemplateTestCaseCheckListResponseDuty>();
        }

        public long DesignationId { get; set; }
        public long? LevelId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int? HeadCount { get; set; }
        public long? CompanyId { get; set; }
        public long? SubSectionTid { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? SectionId { get; set; }
        public long? SubSectionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual LevelMaster Level { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Section Section { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual SubSection SubSection { get; set; }
        public virtual SubSectionTwo SubSectionT { get; set; }
        public virtual ICollection<ApplicationWikiLineDuty> ApplicationWikiLineDuty { get; set; }
        public virtual ICollection<DesignationHeadCount> DesignationHeadCount { get; set; }
        public virtual ICollection<DraftApplicationWikiLineDuty> DraftApplicationWikiLineDuty { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
        public virtual ICollection<EmployeeInformation> EmployeeInformation { get; set; }
        public virtual ICollection<EmployeeOtherDutyInformation> EmployeeOtherDutyInformation { get; set; }
        public virtual ICollection<EmployeeReportPerson> EmployeeReportPerson { get; set; }
        public virtual ICollection<EmployeeResignation> EmployeeResignation { get; set; }
        public virtual ICollection<ProductActivityCaseResponsDuty> ProductActivityCaseResponsDuty { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponseDuty> TemplateTestCaseCheckListResponseDuty { get; set; }
    }
}
