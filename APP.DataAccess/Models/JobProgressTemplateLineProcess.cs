﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class JobProgressTemplateLineProcess
    {
        public long JobProgressTemplateLineProcessId { get; set; }
        public long? JobProgressTemplateId { get; set; }
        public long? JobProgressTemplateLineId { get; set; }
        public string ProfileNo { get; set; }
        public string ActionNo { get; set; }
        public string SectionNo { get; set; }
        public string SectionDescription { get; set; }
        public string SubSectionNo { get; set; }
        public string SubSectionDescription { get; set; }
        public long? Pia { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ActualStartDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public DateTime? ActualCompletionDate { get; set; }
        public string Message { get; set; }
        public long? Pic { get; set; }
        public int? NoOfWorkingDays { get; set; }
        public int? SpecificDateOfEachMonth { get; set; }
        public string JobDescription { get; set; }
        public long? AssignedTo { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser AssignedToNavigation { get; set; }
        public virtual JobProgressTemplate JobProgressTemplate { get; set; }
        public virtual JobProgressTempletateLine JobProgressTemplateLine { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationUser PiaNavigation { get; set; }
        public virtual ApplicationUser PicNavigation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
