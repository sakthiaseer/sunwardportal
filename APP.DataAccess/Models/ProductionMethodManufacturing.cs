﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionMethodManufacturing
    {
        public long ProductionMethodManufacturingId { get; set; }
        public long? ProductionMethodTemplateId { get; set; }
        public long? ManufacturingSiteId { get; set; }

        public virtual ProductionMethodTemplate ProductionMethodTemplate { get; set; }
    }
}
