﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PharmacologicalCategory
    {
        public long PharmacologicalId { get; set; }
        public long? FinishProductId { get; set; }
        public long? PharmacologicalCategoryId { get; set; }

        public virtual FinishProduct FinishProduct { get; set; }
    }
}
