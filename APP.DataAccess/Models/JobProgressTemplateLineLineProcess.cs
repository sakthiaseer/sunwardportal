﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class JobProgressTemplateLineLineProcess
    {
        public JobProgressTemplateLineLineProcess()
        {
            JobProgressTemplateLineLineTaskLink = new HashSet<JobProgressTemplateLineLineTaskLink>();
            JobProgressTemplateLineProcessPia = new HashSet<JobProgressTemplateLineProcessPia>();
        }

        public long JobProgressTemplateLineLineProcessId { get; set; }
        public long? JobProgressTemplateLineProcessId { get; set; }
        public string SubSectionNo { get; set; }
        public string SubSectionDescription { get; set; }
        public int? NoOfWorkingDays { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Message { get; set; }
        public long? MonitorTypeOfJobActionId { get; set; }
        public string UserType { get; set; }
        public long? UserGroupId { get; set; }
        public long? PlantId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual JobProgressTempletateLine JobProgressTemplateLineProcess { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationMasterDetail MonitorTypeOfJobAction { get; set; }
        public virtual Plant Plant { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual UserGroup UserGroup { get; set; }
        public virtual ICollection<JobProgressTemplateLineLineTaskLink> JobProgressTemplateLineLineTaskLink { get; set; }
        public virtual ICollection<JobProgressTemplateLineProcessPia> JobProgressTemplateLineProcessPia { get; set; }
    }
}
