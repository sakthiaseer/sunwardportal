﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Documents
    {
        public Documents()
        {
            Acentry = new HashSet<Acentry>();
            AppIpirentry = new HashSet<AppIpirentry>();
            AppWikiDraftDoc = new HashSet<AppWikiDraftDoc>();
            AppWikiReleaseDoc = new HashSet<AppWikiReleaseDoc>();
            BlisterMouldInformationDocument = new HashSet<BlisterMouldInformationDocument>();
            CommentAttachment = new HashSet<CommentAttachment>();
            CompanyCalandarDocumentPermission = new HashSet<CompanyCalandarDocumentPermission>();
            CompanyCalendarLineLinkPrintUser = new HashSet<CompanyCalendarLineLinkPrintUser>();
            DocumentAlert = new HashSet<DocumentAlert>();
            DocumentDmsshare = new HashSet<DocumentDmsshare>();
            DocumentFolder = new HashSet<DocumentFolder>();
            DocumentInvitation = new HashSet<DocumentInvitation>();
            DocumentLinkDocument = new HashSet<DocumentLink>();
            DocumentLinkLinkDocument = new HashSet<DocumentLink>();
            DocumentPermission = new HashSet<DocumentPermission>();
            DocumentRights = new HashSet<DocumentRights>();
            DocumentShare = new HashSet<DocumentShare>();
            DocumentShareDocuments = new HashSet<DocumentShareDocuments>();
            DocumentUserRole = new HashSet<DocumentUserRole>();
            EmailAttachment = new HashSet<EmailAttachment>();
            EmployeeAdditionalInformation = new HashSet<EmployeeAdditionalInformation>();
            EmployeeResignation = new HashSet<EmployeeResignation>();
            FileProfileTypeDynamicForm = new HashSet<FileProfileTypeDynamicForm>();
            FileProfileTypeSetAccess = new HashSet<FileProfileTypeSetAccess>();
            FinishProductGeneralInfo = new HashSet<FinishProductGeneralInfo>();
            FolderDiscussionAttachment = new HashSet<FolderDiscussionAttachment>();
            IctlayoutPlanTypes = new HashSet<IctlayoutPlanTypes>();
            Ictmaster = new HashSet<Ictmaster>();
            IpirAppSupportDoc = new HashSet<IpirAppSupportDoc>();
            LinkFileProfileTypeDocument = new HashSet<LinkFileProfileTypeDocument>();
            ManufacturingProcess = new HashSet<ManufacturingProcess>();
            ManufacturingProcessLine = new HashSet<ManufacturingProcessLine>();
            Notes = new HashSet<Notes>();
            NotifyDocument = new HashSet<NotifyDocument>();
            PortfolioAttachment = new HashSet<PortfolioAttachment>();
            ProductionActivityAppLineDoc = new HashSet<ProductionActivityAppLineDoc>();
            ProductionActivityPlanningAppLineDoc = new HashSet<ProductionActivityPlanningAppLineDoc>();
            ProductionActivityRoutineAppLineDoc = new HashSet<ProductionActivityRoutineAppLineDoc>();
            ProductionTicket = new HashSet<ProductionTicket>();
            ProfileDocument = new HashSet<ProfileDocument>();
            ReceiveEmail = new HashSet<ReceiveEmail>();
            TaskAttachment = new HashSet<TaskAttachment>();
            TaskComment = new HashSet<TaskComment>();
        }

        public long DocumentId { get; set; }
        public string FileName { get; set; }
        public string DisplayName { get; set; }
        public string Extension { get; set; }
        public string ContentType { get; set; }
        public int? DocumentType { get; set; }
        public byte[] FileData { get; set; }
        public long? FileSize { get; set; }
        public DateTime? UploadDate { get; set; }
        public Guid? SessionId { get; set; }
        public long? LinkId { get; set; }
        public bool? IsSpecialFile { get; set; }
        public bool? IsTemp { get; set; }
        public long? DepartmentId { get; set; }
        public long? WikiId { get; set; }
        public long? CategoryId { get; set; }
        public int? StatusCodeId { get; set; }
        public int? ReferenceNumber { get; set; }
        public string Description { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? FilterProfileTypeId { get; set; }
        public string ProfileNo { get; set; }
        public string TableName { get; set; }
        public long? DocumentParentId { get; set; }
        public string ScreenId { get; set; }
        public bool? IsLatest { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public bool? IsLocked { get; set; }
        public DateTime? LockedDate { get; set; }
        public long? LockedByUserId { get; set; }
        public bool? IsWikiDraft { get; set; }
        public bool? IsWikiDraftDelete { get; set; }
        public bool? IsMobileUpload { get; set; }
        public bool? IsCompressed { get; set; }
        public bool? IsHeaderImage { get; set; }
        public int? FileIndex { get; set; }
        public bool? IsVideoFile { get; set; }
        public int? CloseDocumentId { get; set; }
        public int? ArchiveStatusId { get; set; }
        public bool? IsPublichFolder { get; set; }
        public long? FolderId { get; set; }
        public long? TaskId { get; set; }
        public bool? IsMainTask { get; set; }
        public bool? IsPrint { get; set; }
        public bool? IsWiki { get; set; }
        public string SubjectName { get; set; }
        public string FilePath { get; set; }
        public bool? IsNewPath { get; set; }
        public bool? IsDelete { get; set; }
        public long? DeleteByUserId { get; set; }
        public DateTime? DeleteByDate { get; set; }
        public long? RestoreByUserId { get; set; }
        public DateTime? RestoreByDate { get; set; }
        public string SourceFrom { get; set; }
        public Guid? UniqueSessionId { get; set; }
        public long? EmailToDms { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster ArchiveStatus { get; set; }
        public virtual CodeMaster CloseDocument { get; set; }
        public virtual ApplicationUser DeleteByUser { get; set; }
        public virtual FileProfileType FilterProfileType { get; set; }
        public virtual ApplicationUser LockedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationUser RestoreByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<Acentry> Acentry { get; set; }
        public virtual ICollection<AppIpirentry> AppIpirentry { get; set; }
        public virtual ICollection<AppWikiDraftDoc> AppWikiDraftDoc { get; set; }
        public virtual ICollection<AppWikiReleaseDoc> AppWikiReleaseDoc { get; set; }
        public virtual ICollection<BlisterMouldInformationDocument> BlisterMouldInformationDocument { get; set; }
        public virtual ICollection<CommentAttachment> CommentAttachment { get; set; }
        public virtual ICollection<CompanyCalandarDocumentPermission> CompanyCalandarDocumentPermission { get; set; }
        public virtual ICollection<CompanyCalendarLineLinkPrintUser> CompanyCalendarLineLinkPrintUser { get; set; }
        public virtual ICollection<DocumentAlert> DocumentAlert { get; set; }
        public virtual ICollection<DocumentDmsshare> DocumentDmsshare { get; set; }
        public virtual ICollection<DocumentFolder> DocumentFolder { get; set; }
        public virtual ICollection<DocumentInvitation> DocumentInvitation { get; set; }
        public virtual ICollection<DocumentLink> DocumentLinkDocument { get; set; }
        public virtual ICollection<DocumentLink> DocumentLinkLinkDocument { get; set; }
        public virtual ICollection<DocumentPermission> DocumentPermission { get; set; }
        public virtual ICollection<DocumentRights> DocumentRights { get; set; }
        public virtual ICollection<DocumentShare> DocumentShare { get; set; }
        public virtual ICollection<DocumentShareDocuments> DocumentShareDocuments { get; set; }
        public virtual ICollection<DocumentUserRole> DocumentUserRole { get; set; }
        public virtual ICollection<EmailAttachment> EmailAttachment { get; set; }
        public virtual ICollection<EmployeeAdditionalInformation> EmployeeAdditionalInformation { get; set; }
        public virtual ICollection<EmployeeResignation> EmployeeResignation { get; set; }
        public virtual ICollection<FileProfileTypeDynamicForm> FileProfileTypeDynamicForm { get; set; }
        public virtual ICollection<FileProfileTypeSetAccess> FileProfileTypeSetAccess { get; set; }
        public virtual ICollection<FinishProductGeneralInfo> FinishProductGeneralInfo { get; set; }
        public virtual ICollection<FolderDiscussionAttachment> FolderDiscussionAttachment { get; set; }
        public virtual ICollection<IctlayoutPlanTypes> IctlayoutPlanTypes { get; set; }
        public virtual ICollection<Ictmaster> Ictmaster { get; set; }
        public virtual ICollection<IpirAppSupportDoc> IpirAppSupportDoc { get; set; }
        public virtual ICollection<LinkFileProfileTypeDocument> LinkFileProfileTypeDocument { get; set; }
        public virtual ICollection<ManufacturingProcess> ManufacturingProcess { get; set; }
        public virtual ICollection<ManufacturingProcessLine> ManufacturingProcessLine { get; set; }
        public virtual ICollection<Notes> Notes { get; set; }
        public virtual ICollection<NotifyDocument> NotifyDocument { get; set; }
        public virtual ICollection<PortfolioAttachment> PortfolioAttachment { get; set; }
        public virtual ICollection<ProductionActivityAppLineDoc> ProductionActivityAppLineDoc { get; set; }
        public virtual ICollection<ProductionActivityPlanningAppLineDoc> ProductionActivityPlanningAppLineDoc { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLineDoc> ProductionActivityRoutineAppLineDoc { get; set; }
        public virtual ICollection<ProductionTicket> ProductionTicket { get; set; }
        public virtual ICollection<ProfileDocument> ProfileDocument { get; set; }
        public virtual ICollection<ReceiveEmail> ReceiveEmail { get; set; }
        public virtual ICollection<TaskAttachment> TaskAttachment { get; set; }
        public virtual ICollection<TaskComment> TaskComment { get; set; }
    }
}
