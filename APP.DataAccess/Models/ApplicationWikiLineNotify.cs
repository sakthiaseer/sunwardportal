﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApplicationWikiLineNotify
    {
        public long ApplicationWikiLineNotifyId { get; set; }
        public long? ApplicationWikiLineId { get; set; }
        public long? NotifyUserId { get; set; }

        public virtual ApplicationWikiLine ApplicationWikiLine { get; set; }
        public virtual ApplicationUser NotifyUser { get; set; }
    }
}
