﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SampleRequestForm
    {
        public SampleRequestForm()
        {
            SampleRequestFormLine = new HashSet<SampleRequestFormLine>();
        }

        public long SampleRequestFormId { get; set; }
        public string ProfileNo { get; set; }
        public long? CompanyId { get; set; }
        public long? PersonId { get; set; }
        public long? PurposeSampleRequestId { get; set; }
        public string AdditionalRequirement { get; set; }
        public DateTime? DueDate { get; set; }
        public Guid? SessionId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? NavLocationId { get; set; }
        public long? TypeOfRequestId { get; set; }
        public DateTime? RequireDate { get; set; }
        public long? InventoryTypeId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual InventoryType InventoryType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Navlocation NavLocation { get; set; }
        public virtual Employee Person { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationMasterDetail TypeOfRequest { get; set; }
        public virtual ICollection<SampleRequestFormLine> SampleRequestFormLine { get; set; }
    }
}
