﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class JobProgressTemplate
    {
        public JobProgressTemplate()
        {
            JobProgressTemplateLineProcess = new HashSet<JobProgressTemplateLineProcess>();
            JobProgressTemplateLineProcessStatus = new HashSet<JobProgressTemplateLineProcessStatus>();
            JobProgressTemplateNavItem = new HashSet<JobProgressTemplateNavItem>();
            JobProgressTemplateNotify = new HashSet<JobProgressTemplateNotify>();
            JobProgressTemplateRecurrence = new HashSet<JobProgressTemplateRecurrence>();
            JobProgressTempletateLine = new HashSet<JobProgressTempletateLine>();
        }

        public long JobProgressTemplateId { get; set; }
        public long? TemplateId { get; set; }
        public long? JobCategoryId { get; set; }
        public long? JobTypeId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public string Objective { get; set; }
        public long? Pic { get; set; }
        public long? NotificationId { get; set; }
        public int? SoftwareId { get; set; }
        public long? ModuleId { get; set; }
        public long? ItemId { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileNo { get; set; }
        public string ProfileCode { get; set; }
        public string TemplateName { get; set; }
        public bool? IsUpdatePersonalCalandar { get; set; }
        public long? CountryId { get; set; }
        public string JobOrderNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Country { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationMasterChild JobCategory { get; set; }
        public virtual ApplicationMasterChild JobType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationMasterDetail Module { get; set; }
        public virtual ApplicationUser PicNavigation { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster Software { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationMasterDetail Template { get; set; }
        public virtual ICollection<JobProgressTemplateLineProcess> JobProgressTemplateLineProcess { get; set; }
        public virtual ICollection<JobProgressTemplateLineProcessStatus> JobProgressTemplateLineProcessStatus { get; set; }
        public virtual ICollection<JobProgressTemplateNavItem> JobProgressTemplateNavItem { get; set; }
        public virtual ICollection<JobProgressTemplateNotify> JobProgressTemplateNotify { get; set; }
        public virtual ICollection<JobProgressTemplateRecurrence> JobProgressTemplateRecurrence { get; set; }
        public virtual ICollection<JobProgressTempletateLine> JobProgressTempletateLine { get; set; }
    }
}
