﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionBatchInformationLine
    {
        public long ProductionBatchInformationLineId { get; set; }
        public long? ProductionBatchInformationId { get; set; }
        public long? ProductCodeId { get; set; }
        public string BatchNo { get; set; }
        public long? ItemId { get; set; }
        public long? QtyPack { get; set; }
        public long? QtyPackUnitsId { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual NavproductCode ProductCode { get; set; }
        public virtual ProductionBatchInformation ProductionBatchInformation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
