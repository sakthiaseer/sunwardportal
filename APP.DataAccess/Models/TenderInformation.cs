﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TenderInformation
    {
        public TenderInformation()
        {
            ExtensionInformationLine = new HashSet<ExtensionInformationLine>();
            NovateInformationLine = new HashSet<NovateInformationLine>();
            PacketInformationLine = new HashSet<PacketInformationLine>();
        }

        public long TenderInformationId { get; set; }
        public long? CustomerId { get; set; }
        public long? ContractId { get; set; }
        public long? ProductId { get; set; }
        public decimal? TenderQty { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public string CustomerPonumber { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual SalesOrderEntry Contract { get; set; }
        public virtual CompanyListing Customer { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual SocustomersItemCrossReference Product { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ExtensionInformationLine> ExtensionInformationLine { get; set; }
        public virtual ICollection<NovateInformationLine> NovateInformationLine { get; set; }
        public virtual ICollection<PacketInformationLine> PacketInformationLine { get; set; }
    }
}
