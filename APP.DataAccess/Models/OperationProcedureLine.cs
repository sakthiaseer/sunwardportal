﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class OperationProcedureLine
    {
        public OperationProcedureLine()
        {
            ReAssignmentJob = new HashSet<ReAssignmentJob>();
        }

        public long OperationProcedureLineId { get; set; }
        public long? OperationProcedureId { get; set; }
        public long? OprocedureId { get; set; }
        public string Description { get; set; }
        public string Wilink { get; set; }
        public bool? IsUpdateNavision { get; set; }
        public Guid? SessionId { get; set; }

        public virtual OperationProcedure OperationProcedure { get; set; }
        public virtual ICollection<ReAssignmentJob> ReAssignmentJob { get; set; }
    }
}
