﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormFilter
    {
        public DynamicFormFilter()
        {
            AttributeHeader = new HashSet<AttributeHeader>();
        }

        public long DynamicFormFilterId { get; set; }
        public long DynamicFilterId { get; set; }
        public string TableName { get; set; }
        public string DisplayName { get; set; }

        public virtual ICollection<AttributeHeader> AttributeHeader { get; set; }
    }
}
