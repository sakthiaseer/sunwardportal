﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductGroupingManufacture
    {
        public ProductGroupingManufacture()
        {
            ProductGroupingNav = new HashSet<ProductGroupingNav>();
        }

        public long ProductGroupingManufactureId { get; set; }
        public long? ProductGroupingId { get; set; }
        public long? ManufactureById { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? SupplyToId { get; set; }
        public long? DosageFormId { get; set; }
        public long? DrugClassificationId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterDetail DosageForm { get; set; }
        public virtual ApplicationMasterDetail DrugClassification { get; set; }
        public virtual CompanyListing ManufactureBy { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual GenericCodes ProductGrouping { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CompanyListing SupplyTo { get; set; }
        public virtual ICollection<ProductGroupingNav> ProductGroupingNav { get; set; }
    }
}
