﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewEmployeeIctinformation
    {
        public long EmployeeIctinformationId { get; set; }
        public long? EmployeeId { get; set; }
        public long? SoftwareId { get; set; }
        public string LoginId { get; set; }
        public string Password { get; set; }
        public long? RoleId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public int? StatusCodeId { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsPortal { get; set; }
        public string StatusCode { get; set; }
        public string AddedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string SoftwareName { get; set; }
        public int? CodeId { get; set; }
        public string RoleName { get; set; }
    }
}
