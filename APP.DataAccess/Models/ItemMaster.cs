﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ItemMaster
    {
        public ItemMaster()
        {
            QuotationLine = new HashSet<QuotationLine>();
        }

        public long ItemId { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public long? BaseUomid { get; set; }
        public string InternalRef { get; set; }
        public long? ManufactureAtId { get; set; }
        public string Fpdescription { get; set; }
        public string Fpbom { get; set; }
        public long? CompanyId { get; set; }
        public bool? IsSync { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual BaseUom BaseUom { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ItemManufacture ManufactureAt { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<QuotationLine> QuotationLine { get; set; }
    }
}
