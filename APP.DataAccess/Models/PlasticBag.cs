﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PlasticBag
    {
        public PlasticBag()
        {
            BlisterScrapEntry = new HashSet<BlisterScrapEntry>();
        }

        public long PlasticBagId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal? TarWeight { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<BlisterScrapEntry> BlisterScrapEntry { get; set; }
    }
}
