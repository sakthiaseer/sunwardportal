﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SobyCustomersTenderAgency
    {
        public long SobyCustomersTenderAgencyId { get; set; }
        public long? SocustomersIssueId { get; set; }
        public long? TenderAgencyId { get; set; }

        public virtual SocustomersIssue SocustomersIssue { get; set; }
    }
}
