﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SocustomersIssue
    {
        public SocustomersIssue()
        {
            SobyCustomersManner = new HashSet<SobyCustomersManner>();
            SobyCustomersTenderAgency = new HashSet<SobyCustomersTenderAgency>();
        }

        public long SocustomersIssueId { get; set; }
        public long? SocustomersId { get; set; }
        public bool? MustSupplyInFull { get; set; }
        public bool? MustFollowDeliveryDate { get; set; }
        public DateTime? ShelfLifeRequirementDate { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? BuyingThroughId { get; set; }
        public long? TenderAgencyId { get; set; }
        public long? CentralizedWithId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CompanyListing BuyingThrough { get; set; }
        public virtual CompanyListing CentralizedWith { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual SobyCustomers Socustomers { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CompanyListing TenderAgency { get; set; }
        public virtual ICollection<SobyCustomersManner> SobyCustomersManner { get; set; }
        public virtual ICollection<SobyCustomersTenderAgency> SobyCustomersTenderAgency { get; set; }
    }
}
