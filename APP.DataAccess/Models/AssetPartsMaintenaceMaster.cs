﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AssetPartsMaintenaceMaster
    {
        public AssetPartsMaintenaceMaster()
        {
            AssetEquipmentMaintenaceMaster = new HashSet<AssetEquipmentMaintenaceMaster>();
        }

        public long AssetPartsMaintenaceMasterId { get; set; }
        public long? AssetCatalogMasterId { get; set; }
        public long? AssetCatalogId { get; set; }
        public long? SectionId { get; set; }
        public long? SubSectionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterChild AssetCatalog { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationMasterChild Section { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationMasterChild SubSection { get; set; }
        public virtual ICollection<AssetEquipmentMaintenaceMaster> AssetEquipmentMaintenaceMaster { get; set; }
    }
}
