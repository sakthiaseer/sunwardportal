﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PerUnitFormulationLine
    {
        public PerUnitFormulationLine()
        {
            ExcipientInformationLineByProcess = new HashSet<ExcipientInformationLineByProcess>();
        }

        public long PerUnitFormulationLineId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public long? ManufacturingStepsId { get; set; }
        public long? FinishProductExcipintId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual FinishProductExcipient FinishProductExcipint { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ExcipientInformationLineByProcess> ExcipientInformationLineByProcess { get; set; }
    }
}
