﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class RegistrationFormatMultiple
    {
        public long RegistrationFormatId { get; set; }
        public long? RegistrationFormatInformationId { get; set; }
        public long? FormatId { get; set; }

        public virtual RegistrationFormatInformation RegistrationFormatInformation { get; set; }
    }
}
