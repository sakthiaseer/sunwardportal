﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class BmrmovementLine
    {
        public long BmrmovementLineId { get; set; }
        public long? BmrmovementId { get; set; }
        public string Bmrname { get; set; }
        public string ProductionOrderNo { get; set; }
        public int? LineNo { get; set; }
        public string LocationCode { get; set; }
        public string Description1 { get; set; }
        public string Description2 { get; set; }
        public decimal? Quantity { get; set; }
        public string BatchNo { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Bmrmovement Bmrmovement { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
