﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CloseDocumentPermission
    {
        public long CloseDocumentPermissionId { get; set; }
        public long? UserId { get; set; }
        public long? UserGroupId { get; set; }
        public long? FileProfileTypeId { get; set; }
        public bool? IsCloseDocumentPermission { get; set; }

        public virtual FileProfileType FileProfileType { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual UserGroup UserGroup { get; set; }
    }
}
