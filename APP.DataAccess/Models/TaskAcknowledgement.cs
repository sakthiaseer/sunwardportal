﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TaskAcknowledgement
    {
        public long AcknowledgementId { get; set; }
        public long WorkListId { get; set; }
        public DateTime? CompletedDate { get; set; }
        public bool? IsDocumentUnderstand { get; set; }
        public bool? IsContentInformation { get; set; }
        public string Signature { get; set; }

        public virtual WorkList WorkList { get; set; }
    }
}
