﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class InventoryType
    {
        public InventoryType()
        {
            AssetMaster = new HashSet<AssetMaster>();
            DisposalItem = new HashSet<DisposalItem>();
            InventoryTypeOpeningStockBalance = new HashSet<InventoryTypeOpeningStockBalance>();
            MedMasterLine = new HashSet<MedMasterLine>();
            SampleRequestForDoctorHeader = new HashSet<SampleRequestForDoctorHeader>();
            SampleRequestForm = new HashSet<SampleRequestForm>();
        }

        public long InventoryTypeId { get; set; }
        public long? CompanyId { get; set; }
        public long? OwnerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? ItemSourceId { get; set; }
        public long? SourceTreeId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual CodeMaster ItemSource { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Department Owner { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<AssetMaster> AssetMaster { get; set; }
        public virtual ICollection<DisposalItem> DisposalItem { get; set; }
        public virtual ICollection<InventoryTypeOpeningStockBalance> InventoryTypeOpeningStockBalance { get; set; }
        public virtual ICollection<MedMasterLine> MedMasterLine { get; set; }
        public virtual ICollection<SampleRequestForDoctorHeader> SampleRequestForDoctorHeader { get; set; }
        public virtual ICollection<SampleRequestForm> SampleRequestForm { get; set; }
    }
}
