﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WikiSearch
    {
        public WikiSearch()
        {
            WikiPage = new HashSet<WikiPage>();
        }

        public long WikiSearchId { get; set; }
        public long? WikiPageId { get; set; }
        public string Name { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual WikiPage WikiPageNavigation { get; set; }
        public virtual ICollection<WikiPage> WikiPage { get; set; }
    }
}
