﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProcessManPowerSpecialNotes
    {
        public long ProcessManPowerSpecialNotesId { get; set; }
        public long? ProcessMachineTimeLineId { get; set; }
        public long? DepartmentId { get; set; }
        public string Information { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Department Department { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProcessMachineTimeProductionLine ProcessMachineTimeLine { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
