﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Ccfainformation
    {
        public long CcfainformationId { get; set; }
        public bool? IsInternalChanges { get; set; }
        public bool? IsAuthorityDirectedChanges { get; set; }
        public long? Pic { get; set; }
        public long? Pia { get; set; }
        public long? DepartmentId { get; set; }
        public bool? IsSiteTransfer { get; set; }
        public bool? IsProduct { get; set; }
        public bool? IsEquipment { get; set; }
        public bool? IsComposition { get; set; }
        public bool? IsFacility { get; set; }
        public bool? IsLayout { get; set; }
        public bool? IsDocument { get; set; }
        public bool? IsProcess { get; set; }
        public bool? IsControlParameter { get; set; }
        public bool? IsBatchSize { get; set; }
        public bool? IsHoldingTime { get; set; }
        public bool? IsRawMeterial { get; set; }
        public bool? IsArtwork { get; set; }
        public bool? IsPackagingMaterial { get; set; }
        public bool? IsVendor { get; set; }
        public bool? IsShelfLife { get; set; }
        public bool? IsRegulatory { get; set; }
        public bool? IsReTestPeriod { get; set; }
        public string Others { get; set; }
        public string DescriptionOfProposedChange { get; set; }
        public string Justification { get; set; }
        public string ProposedImplementationAction { get; set; }
        public string RelatedDeviation { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? InitiatorDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Department Department { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationUser PiaNavigation { get; set; }
        public virtual ApplicationUser PicNavigation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
