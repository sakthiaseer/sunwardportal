﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewInpprodPlanning
    {
        public long ItemId { get; set; }
        public string Description { get; set; }
        public string BatchNos { get; set; }
        public string SafetyLeadTime { get; set; }
        public string BaseUnitofMeasure { get; set; }
        public decimal? UnitCost { get; set; }
        public string DistName { get; set; }
        public string ItemGroup { get; set; }
        public string Steriod { get; set; }
        public string ShelfLife { get; set; }
        public string Quota { get; set; }
        public string Status { get; set; }
        public string AcitemDesc { get; set; }
        public int? PackSize { get; set; }
        public string PackUom { get; set; }
        public decimal? Navqty { get; set; }
        public decimal? Acqty { get; set; }
        public DateTime? Acmonth { get; set; }
        public decimal? BatchQty { get; set; }
        public string ProdTime { get; set; }
    }
}
