﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class RegistrationFormatInformationLineUsers
    {
        public long RegistrationFormatInformationLineUsersId { get; set; }
        public long? RegistrationFormatInformationLineId { get; set; }
        public long? RoleId { get; set; }
        public long? UserId { get; set; }
        public long? UserGroupId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public string ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual RegistrationFormatInformationLine RegistrationFormatInformationLine { get; set; }
        public virtual DocumentRole Role { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual UserGroup UserGroup { get; set; }
    }
}
