﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SalesOrderMasterPricingLine
    {
        public SalesOrderMasterPricingLine()
        {
            SalesOrderMasterPricingLineSellingMethod = new HashSet<SalesOrderMasterPricingLineSellingMethod>();
        }

        public long SalesOrderMasterPricingLineId { get; set; }
        public long? SalesOrderMasterPricingId { get; set; }
        public long? ItemId { get; set; }
        public long? SellingMethodId { get; set; }
        public decimal? SellingPrice { get; set; }
        public decimal? SmAllowPriceByPercent { get; set; }
        public decimal? SmAllowPriceBy { get; set; }
        public long? SmAllowPriceId { get; set; }
        public decimal? NewAllowPrice { get; set; }
        public bool? IsSalesManagerApprovePrice { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual SalesOrderMasterPricing SalesOrderMasterPricing { get; set; }
        public virtual ApplicationMasterDetail SellingMethod { get; set; }
        public virtual ApplicationMasterDetail SmAllowPrice { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<SalesOrderMasterPricingLineSellingMethod> SalesOrderMasterPricingLineSellingMethod { get; set; }
    }
}
