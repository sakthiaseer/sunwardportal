﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Table1
    {
        public long? ProductionActivityMultipleId { get; set; }
        public long? ProductionActivityAppLineId { get; set; }
        public long? ActivityMasterId { get; set; }

        public virtual ApplicationMasterDetail ActivityMaster { get; set; }
        public virtual ProductionActivityAppLine ProductionActivityAppLine { get; set; }
    }
}
