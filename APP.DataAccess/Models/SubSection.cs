﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SubSection
    {
        public SubSection()
        {
            ApplicationWiki = new HashSet<ApplicationWiki>();
            Designation = new HashSet<Designation>();
            DraftApplicationWiki = new HashSet<DraftApplicationWiki>();
            Employee = new HashSet<Employee>();
            EmployeeInformation = new HashSet<EmployeeInformation>();
            EmployeeReportPerson = new HashSet<EmployeeReportPerson>();
            ProfileAutoNumber = new HashSet<ProfileAutoNumber>();
            SubSectionTwo = new HashSet<SubSectionTwo>();
            WikiAccess = new HashSet<WikiAccess>();
        }

        public long SubSectionId { get; set; }
        public long? SectionId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int? HeadCount { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileCode { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Section Section { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ApplicationWiki> ApplicationWiki { get; set; }
        public virtual ICollection<Designation> Designation { get; set; }
        public virtual ICollection<DraftApplicationWiki> DraftApplicationWiki { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
        public virtual ICollection<EmployeeInformation> EmployeeInformation { get; set; }
        public virtual ICollection<EmployeeReportPerson> EmployeeReportPerson { get; set; }
        public virtual ICollection<ProfileAutoNumber> ProfileAutoNumber { get; set; }
        public virtual ICollection<SubSectionTwo> SubSectionTwo { get; set; }
        public virtual ICollection<WikiAccess> WikiAccess { get; set; }
    }
}
