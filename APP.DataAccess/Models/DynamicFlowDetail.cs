﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFlowDetail
    {
        public DynamicFlowDetail()
        {
            ActiveFlowDetails = new HashSet<ActiveFlowDetails>();
            DynamicFlowProgress = new HashSet<DynamicFlowProgress>();
            DynamicFlowStep = new HashSet<DynamicFlowStep>();
            FlowDistributionDetail = new HashSet<FlowDistributionDetail>();
        }

        public long DynamicFlowDetailId { get; set; }
        public long? DynamicFlowId { get; set; }
        public string FlowNo { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? FlowTypeId { get; set; }
        public long? WorkFlowPageId { get; set; }
        public string LoopTo { get; set; }

        public virtual DynamicFlow DynamicFlow { get; set; }
        public virtual CodeMaster FlowType { get; set; }
        public virtual WorkFlowPages WorkFlowPage { get; set; }
        public virtual ICollection<ActiveFlowDetails> ActiveFlowDetails { get; set; }
        public virtual ICollection<DynamicFlowProgress> DynamicFlowProgress { get; set; }
        public virtual ICollection<DynamicFlowStep> DynamicFlowStep { get; set; }
        public virtual ICollection<FlowDistributionDetail> FlowDistributionDetail { get; set; }
    }
}
