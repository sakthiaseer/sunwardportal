﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FileProfileTypeDynamicForm
    {
        public long FileProfileTypeDynamicFormId { get; set; }
        public long? FileProfileTypeId { get; set; }
        public string DynamicFormData { get; set; }
        public string ProfileNo { get; set; }
        public long? DocumentId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Documents Document { get; set; }
        public virtual FileProfileType FileProfileType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
