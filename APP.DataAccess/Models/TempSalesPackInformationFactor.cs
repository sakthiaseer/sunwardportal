﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TempSalesPackInformationFactor
    {
        public long TempSalesPackInformationFactorId { get; set; }
        public long? TempSalesPackInformationId { get; set; }
        public int? SalesFactor { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileNo { get; set; }
        public string Fpname { get; set; }
        public long? ItemId { get; set; }
        public long? QtypackPerCarton { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? SalesPackSize { get; set; }
        public int? SellingPackUnit { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual TempSalesPackInformation TempSalesPackInformation { get; set; }
    }
}
