﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WorkOrderLine
    {
        public WorkOrderLine()
        {
            WorkOrderComment = new HashSet<WorkOrderComment>();
            WorkOrderCommentUser = new HashSet<WorkOrderCommentUser>();
        }

        public long WorkOrderLineId { get; set; }
        public long? WorkOrderId { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public int? PriorityId { get; set; }
        public int? SunwardStatusId { get; set; }
        public DateTime? StatusUpdateDate { get; set; }
        public string TaskLink { get; set; }
        public string ResultLink { get; set; }
        public int? CrtstatusId { get; set; }
        public long? RequirementId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public DateTime? ExpectedReleaseDate { get; set; }
        public string LastIndex { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster Crtstatus { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster Priority { get; set; }
        public virtual ApplicationMasterDetail Requirement { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CodeMaster SunwardStatus { get; set; }
        public virtual WorkOrder WorkOrder { get; set; }
        public virtual ICollection<WorkOrderComment> WorkOrderComment { get; set; }
        public virtual ICollection<WorkOrderCommentUser> WorkOrderCommentUser { get; set; }
    }
}
