﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ChatGroupUsers
    {
        public long ChatGroupUserId { get; set; }
        public long ChatGroupId { get; set; }
        public long UserId { get; set; }

        public virtual ChatGroup ChatGroup { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
