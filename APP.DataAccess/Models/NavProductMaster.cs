﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavProductMaster
    {
        public NavProductMaster()
        {
            NavMarkingMultiple = new HashSet<NavMarkingMultiple>();
            NavProductMultiple = new HashSet<NavProductMultiple>();
            NavProductShapeMultiple = new HashSet<NavProductShapeMultiple>();
        }

        public long ProductMasterId { get; set; }
        public long? MethodCodeId { get; set; }
        public long? SalesCategoryId { get; set; }
        public long? ColorId { get; set; }
        public long? ShapeId { get; set; }
        public long? MarkingId { get; set; }
        public decimal? LengthMax { get; set; }
        public decimal? LengthMin { get; set; }
        public decimal? WidthMax { get; set; }
        public decimal? WidthMin { get; set; }
        public decimal? ThicknessMax { get; set; }
        public decimal? ThicknessMin { get; set; }
        public decimal? WeightMax { get; set; }
        public decimal? WeightMin { get; set; }
        public int? TypeOfCoating { get; set; }
        public decimal? CoreSizeLengthMax { get; set; }
        public decimal? CoreSizeLengthMin { get; set; }
        public decimal? CoreSizeWidthMax { get; set; }
        public decimal? CoreSizeWidthMin { get; set; }
        public decimal? CoreThicknessMax { get; set; }
        public decimal? CoreThicknessMin { get; set; }
        public decimal? CoatedSizeLengthMax { get; set; }
        public decimal? CoatedSizeLengthMin { get; set; }
        public decimal? CoatedSizeWidthMax { get; set; }
        public decimal? CoatedSizeWidthMin { get; set; }
        public decimal? CoreWeightMin { get; set; }
        public decimal? CoreWeightMax { get; set; }
        public int? ProductCreationType { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public decimal? CoatedThicknessMin { get; set; }
        public decimal? CoatedThicknessMax { get; set; }
        public decimal? CoatedWeightMin { get; set; }
        public decimal? CoatedWeightMax { get; set; }
        public long? FinishProductId { get; set; }
        public bool? IsMasterDocument { get; set; }
        public bool? IsInformationvsMaster { get; set; }
        public long? FlavourId { get; set; }
        public decimal? LengthVariationId { get; set; }
        public decimal? LengthVariationValue { get; set; }
        public decimal? WidthVariationId { get; set; }
        public decimal? WidthVariationValue { get; set; }
        public decimal? ThicknessVariationId { get; set; }
        public decimal? ThicknessVariationValue { get; set; }
        public decimal? WeightVariationId { get; set; }
        public decimal? WeightVariationValue { get; set; }
        public byte[] Picture { get; set; }
        public decimal? CoreSizeLengthVariationId { get; set; }
        public decimal? CoreSizeLengthVariationValue { get; set; }
        public decimal? CoreSizeWidthVariationId { get; set; }
        public decimal? CoreSizeWidthVariationValue { get; set; }
        public decimal? CoreSizeThicknessVariationId { get; set; }
        public decimal? CoreSizeThicknessVariationValue { get; set; }
        public decimal? CoreSizeWeightVariationId { get; set; }
        public decimal? CoreSizeWeightVariationValue { get; set; }
        public decimal? CoatedSizeLengthVariationId { get; set; }
        public decimal? CoatedSizeLengthVariationValue { get; set; }
        public decimal? CoatedSizeWidthVariationId { get; set; }
        public decimal? CoatedSizeWidthVariationValue { get; set; }
        public decimal? CoatedSizeThicknessVariationId { get; set; }
        public decimal? CoatedSizeThicknessVariationValue { get; set; }
        public decimal? CoatedSizeWeightVariationId { get; set; }
        public decimal? CoatedSizeWeightVariationValue { get; set; }
        public long? ClarityId { get; set; }
        public decimal? Phmin { get; set; }
        public decimal? Phmax { get; set; }
        public decimal? Sgmin { get; set; }
        public decimal? Sgmax { get; set; }
        public decimal? ViscosityMin { get; set; }
        public decimal? ViscosityMax { get; set; }
        public decimal? PhvariationId { get; set; }
        public decimal? PhvariationValue { get; set; }
        public decimal? SgvariationId { get; set; }
        public decimal? SgvariationValue { get; set; }
        public decimal? ViscosityVariationId { get; set; }
        public decimal? ViscosityVariationValue { get; set; }
        public long? TextureId { get; set; }
        public long? ManufacturingSiteId { get; set; }
        public decimal? Phstandard { get; set; }
        public decimal? Sgstandard { get; set; }
        public decimal? ViscosityStandard { get; set; }
        public decimal? CreamPhmin { get; set; }
        public decimal? CreamPhmax { get; set; }
        public decimal? CreamSgmin { get; set; }
        public decimal? CreamSgmax { get; set; }
        public decimal? CreamViscosityMin { get; set; }
        public decimal? CreamViscosityMax { get; set; }
        public decimal? CreamPhvariationId { get; set; }
        public decimal? CreamPhvariationValue { get; set; }
        public decimal? CreamSgvariationId { get; set; }
        public decimal? CreamSgvariationValue { get; set; }
        public decimal? CreamViscosityVariationId { get; set; }
        public decimal? CreamViscosityVariationValue { get; set; }
        public decimal? CreamPhstandard { get; set; }
        public decimal? CreamSgstandard { get; set; }
        public decimal? CreamViscosityStandard { get; set; }
        public decimal? LengthStandard { get; set; }
        public decimal? WidthStandard { get; set; }
        public decimal? ThicknessStandard { get; set; }
        public decimal? WeightStandard { get; set; }
        public decimal? CoreSizeLengthStandard { get; set; }
        public decimal? CoreSizeWidthStandard { get; set; }
        public decimal? CoreThicknessStandard { get; set; }
        public decimal? CoatedSizeLengthStandard { get; set; }
        public decimal? CoatedSizeWidthStandard { get; set; }
        public decimal? CoreWeightStandard { get; set; }
        public decimal? CoatedThicknessStandard { get; set; }
        public decimal? CoatedWeightStandard { get; set; }
        public long? FinishProductGeneralInfoId { get; set; }
        public int? RegistrationReferenceId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual FinishProduct FinishProduct { get; set; }
        public virtual FinishProductGeneralInfo FinishProductGeneralInfo { get; set; }
        public virtual NavMethodCode MethodCode { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster ProductCreationTypeNavigation { get; set; }
        public virtual CodeMaster RegistrationReference { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CodeMaster TypeOfCoatingNavigation { get; set; }
        public virtual ICollection<NavMarkingMultiple> NavMarkingMultiple { get; set; }
        public virtual ICollection<NavProductMultiple> NavProductMultiple { get; set; }
        public virtual ICollection<NavProductShapeMultiple> NavProductShapeMultiple { get; set; }
    }
}
