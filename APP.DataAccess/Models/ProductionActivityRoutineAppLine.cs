﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionActivityRoutineAppLine
    {
        public ProductionActivityRoutineAppLine()
        {
            ProductionActivityNonCompliance = new HashSet<ProductionActivityNonCompliance>();
            ProductionActivityRoutineAppLineDoc = new HashSet<ProductionActivityRoutineAppLineDoc>();
            ProductionActivityRoutineAppLineQaChecker = new HashSet<ProductionActivityRoutineAppLineQaChecker>();
            ProductionActivityRoutineCheckedDetails = new HashSet<ProductionActivityRoutineCheckedDetails>();
            RoutineInfo = new HashSet<RoutineInfo>();
            RoutineInfoMultiple = new HashSet<RoutineInfoMultiple>();
        }

        public long ProductionActivityRoutineAppLineId { get; set; }
        public long? ProductionActivityRoutineAppId { get; set; }
        public string ActionDropdown { get; set; }
        public long? ProdActivityActionId { get; set; }
        public long? ProdActivityCategoryId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public bool? IsTemplateUpload { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public long? ProductActivityCaseLineId { get; set; }
        public long? NavprodOrderLineId { get; set; }
        public string Comment { get; set; }
        public bool? QaCheck { get; set; }
        public bool? IsOthersOptions { get; set; }
        public long? ProdActivityResultId { get; set; }
        public long? ManufacturingProcessChildId { get; set; }
        public long? ProdActivityCategoryChildId { get; set; }
        public long? ProdActivityActionChildD { get; set; }
        public string TopicId { get; set; }
        public long? QaCheckUserId { get; set; }
        public DateTime? QaCheckDate { get; set; }
        public long? LocationId { get; set; }
        public long? ProductActivityCaseId { get; set; }
        public long? VisaMasterId { get; set; }
        public long? RoutineStatusId { get; set; }
        public byte[] CommentImage { get; set; }
        public string CommentImageType { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileNo { get; set; }
        public bool? IsCheckNoIssue { get; set; }
        public long? CheckedById { get; set; }
        public DateTime? CheckedDate { get; set; }
        public string CheckedRemark { get; set; }
        public bool? IsCheckReferSupportDocument { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Ictmaster Location { get; set; }
        public virtual ApplicationMasterDetail ManufacturingProcess { get; set; }
        public virtual ApplicationMasterChild ManufacturingProcessChild { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual NavprodOrderLine NavprodOrderLine { get; set; }
        public virtual ApplicationMasterDetail RoutineStatus { get; set; }
        public virtual ApplicationMasterDetail ProdActivityResult { get; set; }
        public virtual ApplicationMasterDetail ProdActivityAction { get; set; }
        public virtual ApplicationMasterChild ProdActivityActionChildDNavigation { get; set; }
        public virtual ApplicationMasterDetail ProdActivityCategory { get; set; }
        public virtual ApplicationMasterChild ProdActivityCategoryChild { get; set; }
        public virtual ProductActivityCase ProductActivityCase { get; set; }
        public virtual ProductActivityCaseLine ProductActivityCaseLine { get; set; }
        public virtual ProductionActivityRoutineApp ProductionActivityRoutineApp { get; set; }
        public virtual ApplicationUser QaCheckUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationMasterDetail VisaMaster { get; set; }
        public virtual ICollection<ProductionActivityNonCompliance> ProductionActivityNonCompliance { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLineDoc> ProductionActivityRoutineAppLineDoc { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLineQaChecker> ProductionActivityRoutineAppLineQaChecker { get; set; }
        public virtual ICollection<ProductionActivityRoutineCheckedDetails> ProductionActivityRoutineCheckedDetails { get; set; }
        public virtual ICollection<RoutineInfo> RoutineInfo { get; set; }
        public virtual ICollection<RoutineInfoMultiple> RoutineInfoMultiple { get; set; }
    }
}
