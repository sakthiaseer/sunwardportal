﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class MedMaster
    {
        public MedMaster()
        {
            MedMasterLine = new HashSet<MedMasterLine>();
        }

        public long MedMasterId { get; set; }
        public long? CompanyId { get; set; }
        public long? MetCatalogId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long? UnitsId { get; set; }
        public string ModelNo { get; set; }
        public bool? IsCalibrationRequire { get; set; }
        public int? TotalNumber { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual MedCatalogClassification MetCatalog { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationMasterDetail Units { get; set; }
        public virtual ICollection<MedMasterLine> MedMasterLine { get; set; }
    }
}
