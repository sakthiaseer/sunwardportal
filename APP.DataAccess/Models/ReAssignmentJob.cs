﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ReAssignmentJob
    {
        public ReAssignmentJob()
        {
            ReAssignementManpowerMultiple = new HashSet<ReAssignementManpowerMultiple>();
        }

        public long ReassignmentJobId { get; set; }
        public long? MobileShiftId { get; set; }
        public long? CompanyDbid { get; set; }
        public long? ProdTaskId { get; set; }
        public long? SpecificProcedureId { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int? StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual MobileShift MobileShift { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual OperationProcedureLine ProdTask { get; set; }
        public virtual ApplicationMasterDetail SpecificProcedure { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ReAssignementManpowerMultiple> ReAssignementManpowerMultiple { get; set; }
    }
}
