﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class MemoUser
    {
        public long MemoUserId { get; set; }
        public long? MemoId { get; set; }
        public long? UserId { get; set; }
        public long? UserGroupId { get; set; }
        public long? LevelId { get; set; }
        public string UserType { get; set; }
        public bool? IsAcknowledgement { get; set; }
        public DateTime? AcknowledgementDate { get; set; }

        public virtual LevelMaster Level { get; set; }
        public virtual Memo Memo { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual UserGroup UserGroup { get; set; }
    }
}
