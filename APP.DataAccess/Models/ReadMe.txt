﻿// Run below comment to update entity framework

Scaffold-DbContext "Server=218.208.20.237\sunward;Database=CRT_SWTMS;User ID=sunward;Password=SWp0rt@l;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Models -force

Scaffold-DbContext "Server=portal.sunwardpharma.com;Database=SWUAT;User ID=crt;Password=P@ssw0rd;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Models -force

Please add this  on  OnModelCreating method in Model.
 modelBuilder.Entity<SpApplicationPermission>().HasNoKey();
            modelBuilder.Entity<SpUserPermission>().HasNoKey();
            modelBuilder.Entity<SpApplicationRolePermission>().HasNoKey();
            modelBuilder.Entity<SpNotificationList>().HasNoKey();
            modelBuilder.Entity<View_UserPermission>().HasNoKey();
            modelBuilder.Entity<view_INPProdPlanning>().HasNoKey();
            modelBuilder.Entity<view_AcByDistributor>().HasNoKey();
            modelBuilder.Entity<view_ForecastPivot>().HasNoKey();
            modelBuilder.Entity<View_SageAttendance>().HasNoKey();
            modelBuilder.Entity<view_GetRecordVariation>().HasNoKey();
            modelBuilder.Entity<view_GetWorkOrderReport>().HasNoKey();
            modelBuilder.Entity<view_GetEmployee>().HasNoKey();
            modelBuilder.Entity<View_GetApplicationWiki>().HasNoKey();
            modelBuilder.Entity<View_GetDraftApplicationWiki>().HasNoKey();
            modelBuilder.Entity<HistoricalDetailsGraph>().HasNoKey();
            modelBuilder.Entity<SpNotifyDocumentModel>().HasNoKey();

To capture audit trail add the below Namespace and override saveChanges method.

using System.Linq;
using System.Collections.Generic;

          public override int SaveChanges()
        {
            ChangeTracker.DetectChanges();

            var modified = ChangeTracker.Entries().Where(x => x.State == EntityState.Added || x.State == EntityState.Modified || x.State == EntityState.Deleted);
            var auditTrailList = new List<AuditLog>();
            foreach (var entry in modified)
            {
                long auditbyUser = -1;
                var tableName = entry.Metadata.GetTableName();
                var primaryKeyProperty = entry.Properties.Where(f => f.Metadata.IsPrimaryKey() == true);
                var addedByUser = entry.Properties.FirstOrDefault(f => f.Metadata.Name == "AddedByUserId");
                if (addedByUser != null)
                {
                    auditbyUser = addedByUser.CurrentValue != null ?  long.Parse(addedByUser.CurrentValue.ToString()) : -1;
                }
                var modifiedByUser = entry.Properties.FirstOrDefault(f => f.Metadata.Name == "ModifiedByUserId");
                if (modifiedByUser != null)
                {
                    auditbyUser = modifiedByUser.CurrentValue !=null ? long.Parse(modifiedByUser.CurrentValue.ToString()) :-1;
                }
                var primarykeyValue = primaryKeyProperty.FirstOrDefault().OriginalValue;
                foreach (var property in entry.Properties)
                {
                    var newValue = property.CurrentValue;
                    var oldValue = property.OriginalValue;
                    var isModified = property.IsModified;
                    var action = entry.State.ToString();
                    var propertyName = property.Metadata.Name;
                    var isPrimaryKey = property.Metadata.IsPrimaryKey();
                    var isForeignKey = property.Metadata.IsForeignKey();
                    primarykeyValue = action == "Added" ? -1 : primarykeyValue;
                    var auditTrail = new AuditLog
                    {
                        TableName = tableName,
                        AuditDate = DateTime.Now,
                        AuditType = action,
                        ColumnName = propertyName,
                        ForeignKeyName = isForeignKey == true ? propertyName : string.Empty,
                        IsForeignKey = isForeignKey,
                        PrimaryKeyName = isPrimaryKey == true ? propertyName : string.Empty,
                        PrimaryKeyValue = primarykeyValue != null ? long.Parse(primarykeyValue.ToString()) : -1,
                        IsPrimaryKey = isPrimaryKey,
                        NewValue = newValue?.ToString(),
                        OldValue = oldValue?.ToString(),
                        IsModified = property.IsModified,
                        AuditByUserId = auditbyUser,
                    };
                    auditTrailList.Add(auditTrail);
                }
            }
            AuditLog.AddRange(auditTrailList);
            return base.SaveChanges();
        }