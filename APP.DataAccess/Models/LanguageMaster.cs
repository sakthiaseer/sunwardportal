﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class LanguageMaster
    {
        public LanguageMaster()
        {
            Employee = new HashSet<Employee>();
            WikiTranslation = new HashSet<WikiTranslation>();
        }

        public long LanguageId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Employee> Employee { get; set; }
        public virtual ICollection<WikiTranslation> WikiTranslation { get; set; }
    }
}
