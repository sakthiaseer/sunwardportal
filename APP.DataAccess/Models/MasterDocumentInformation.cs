﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class MasterDocumentInformation
    {
        public long MasterDocumentInformationId { get; set; }
        public long? DocumentGroupingId { get; set; }
        public long? OwnershipId { get; set; }
        public string DocumentDescription { get; set; }
        public long? LocationId { get; set; }
        public bool? IsTransferPickupPoint { get; set; }
        public long? DisposalInformationId { get; set; }
        public string DisposalWithAdditional { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Locations Location { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
