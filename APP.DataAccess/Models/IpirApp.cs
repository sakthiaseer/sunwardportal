﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class IpirApp
    {
        public IpirApp()
        {
            IpirAppCheckedDetails = new HashSet<IpirAppCheckedDetails>();
            IpirAppIssueDep = new HashSet<IpirAppIssueDep>();
            IpirAppSupportDoc = new HashSet<IpirAppSupportDoc>();
        }

        public long IpirAppId { get; set; }
        public long? CompanyId { get; set; }
        public string ProdOrderNo { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public string Comment { get; set; }
        public long? LocationId { get; set; }
        public long? NavprodOrderLineId { get; set; }
        public string FixedAssetNo { get; set; }
        public long? ReportingPersonal { get; set; }
        public string RefNo { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileNo { get; set; }
        public long? DetectedBy { get; set; }
        public string MachineName { get; set; }
        public long? ActivityStatusId { get; set; }

        public virtual ApplicationMasterDetail ActivityStatus { get; set; }
        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationUser DetectedByNavigation { get; set; }
        public virtual Ictmaster Location { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual NavprodOrderLine NavprodOrderLine { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual ApplicationUser ReportingPersonalNavigation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<IpirAppCheckedDetails> IpirAppCheckedDetails { get; set; }
        public virtual ICollection<IpirAppIssueDep> IpirAppIssueDep { get; set; }
        public virtual ICollection<IpirAppSupportDoc> IpirAppSupportDoc { get; set; }
    }
}
