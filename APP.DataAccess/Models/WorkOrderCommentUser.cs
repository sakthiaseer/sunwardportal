﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WorkOrderCommentUser
    {
        public long WorkOrderCommentUserId { get; set; }
        public long? WorkOrderCommentId { get; set; }
        public long? WorkOrderLineId { get; set; }
        public bool? IsRead { get; set; }
        public int? StatusCodeId { get; set; }
        public bool? IsAssignedTo { get; set; }
        public long? UserId { get; set; }
        public bool? IsClosed { get; set; }

        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual WorkOrderComment WorkOrderComment { get; set; }
        public virtual WorkOrderLine WorkOrderLine { get; set; }
    }
}
