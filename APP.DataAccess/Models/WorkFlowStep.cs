﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WorkFlowStep
    {
        public long WorkFlowStepId { get; set; }
        public string WorkFlowId { get; set; }
        public long? WhenEventId { get; set; }
        public long? ConditionId { get; set; }
        public long? ResposeId { get; set; }
    }
}
