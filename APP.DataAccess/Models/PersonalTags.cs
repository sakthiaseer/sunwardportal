﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PersonalTags
    {
        public long PersonalTagId { get; set; }
        public long? TaskId { get; set; }
        public string Tag { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
    }
}
