﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PrintInfo
    {
        public long PrintInfoId { get; set; }
        public long? PageId { get; set; }
        public long? UserId { get; set; }
        public string Reason { get; set; }
        public DateTime? AddedDate { get; set; }

        public virtual WikiPage Page { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
