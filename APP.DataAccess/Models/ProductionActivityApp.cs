﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionActivityApp
    {
        public ProductionActivityApp()
        {
            ProductionActivityAppLine = new HashSet<ProductionActivityAppLine>();
            ProductionActivityCheckedDetails = new HashSet<ProductionActivityCheckedDetails>();
        }

        public long ProductionActivityAppId { get; set; }
        public long? CompanyId { get; set; }
        public string ProdOrderNo { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public string Comment { get; set; }
        public string TopicId { get; set; }
        public long? LocationId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual Ictmaster Location { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ProductionActivityAppLine> ProductionActivityAppLine { get; set; }
        public virtual ICollection<ProductionActivityCheckedDetails> ProductionActivityCheckedDetails { get; set; }
    }
}
