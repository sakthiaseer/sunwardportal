﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class IssueRequestSampleLine
    {
        public long IssueRequestSampleLineId { get; set; }
        public long? SampleRequestLineId { get; set; }
        public decimal? IssueQuantity { get; set; }
        public string BatchNo { get; set; }
        public string SpecialNotes { get; set; }
        public long? NavCompanyId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Navlocation NavCompany { get; set; }
        public virtual SampleRequestFormLine SampleRequestLine { get; set; }
    }
}
