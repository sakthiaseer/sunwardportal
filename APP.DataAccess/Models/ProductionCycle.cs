﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionCycle
    {
        public ProductionCycle()
        {
            ProdRecipeCycle = new HashSet<ProdRecipeCycle>();
            ProductionCycleTicket = new HashSet<ProductionCycleTicket>();
        }

        public long ProdCycleId { get; set; }
        public long? CompanyId { get; set; }
        public long? MasterFileId { get; set; }
        public string FileName { get; set; }
        public decimal? DraftVersion { get; set; }
        public decimal? ReleaseVersion { get; set; }
        public DateTime? ValidPeriodFromDate { get; set; }
        public DateTime? ValidPeriodToDate { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? VersionSessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ProdRecipeCycle> ProdRecipeCycle { get; set; }
        public virtual ICollection<ProductionCycleTicket> ProductionCycleTicket { get; set; }
    }
}
