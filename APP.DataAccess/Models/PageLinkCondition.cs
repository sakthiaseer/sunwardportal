﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PageLinkCondition
    {
        public PageLinkCondition()
        {
            LinkPages = new HashSet<LinkPages>();
        }

        public long PageLinkConditionId { get; set; }
        public string Code { get; set; }
        public string LinkCondition { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<LinkPages> LinkPages { get; set; }
    }
}
