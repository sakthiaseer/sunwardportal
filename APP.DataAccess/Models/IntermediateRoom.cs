﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class IntermediateRoom
    {
        public long Id { get; set; }
        public DateTime? Date { get; set; }
        public string InputPersonName { get; set; }
        public string IntermediateRoomNo { get; set; }
        public string Movement { get; set; }
        public string ProductionOrder { get; set; }
        public string BatchNo { get; set; }
        public string Process { get; set; }
        public string _1stDrumNo { get; set; }
        public decimal? _1stQty { get; set; }
        public string _2ndDrumNo { get; set; }
        public decimal? _2ndQty { get; set; }
        public string _3rdDrumNo { get; set; }
        public decimal? _3rdQty { get; set; }
        public string _4thDrumNo { get; set; }
        public decimal? _4thQty { get; set; }
        public string _5thDrumNo { get; set; }
        public decimal? _5thQty { get; set; }
        public string _6thDrumNo { get; set; }
        public decimal? _6thQty { get; set; }
        public string _7thDrumNo { get; set; }
        public decimal? _7thQty { get; set; }
        public string _8thDrumNo { get; set; }
        public decimal? _8thQty { get; set; }
        public string _9thDrumNo { get; set; }
        public decimal? _9thQty { get; set; }
        public string _10thDrumNo { get; set; }
        public decimal? _10thQty { get; set; }
        public string _11thDrumNo { get; set; }
        public decimal? _11thQty { get; set; }
        public string _12thDrumNo { get; set; }
        public decimal? _12thQty { get; set; }
        public string _13thDrumNo { get; set; }
        public decimal? _13thQty { get; set; }
        public string _14thDrumNo { get; set; }
        public decimal? _14thQty { get; set; }
        public string _15thDrumNo { get; set; }
        public decimal? _15thQty { get; set; }
    }
}
