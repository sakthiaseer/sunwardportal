﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class GenericCodes
    {
        public GenericCodes()
        {
            CommitmentOrdersLine = new HashSet<CommitmentOrdersLine>();
            DistributorReplenishmentLine = new HashSet<DistributorReplenishmentLine>();
            GenericCodeCountry = new HashSet<GenericCodeCountry>();
            GenericCodeSupplyToMultiple = new HashSet<GenericCodeSupplyToMultiple>();
            MarginInformationLine = new HashSet<MarginInformationLine>();
            Navitems = new HashSet<Navitems>();
            OrderRequirement = new HashSet<OrderRequirement>();
            ProductGroupingManufacture = new HashSet<ProductGroupingManufacture>();
            QuotationAward = new HashSet<QuotationAward>();
            QuotationHistoryLine = new HashSet<QuotationHistoryLine>();
            SalesBorrowLine = new HashSet<SalesBorrowLine>();
            SalesMainPromotionList = new HashSet<SalesMainPromotionList>();
            SellingPriceInformation = new HashSet<SellingPriceInformation>();
            SobyCustomerSunwardEquivalent = new HashSet<SobyCustomerSunwardEquivalent>();
            SocustomersItemCrossReference = new HashSet<SocustomersItemCrossReference>();
        }

        public long GenericCodeId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Description2 { get; set; }
        public string ProfileNo { get; set; }
        public string PackingCode { get; set; }
        public long? Uom { get; set; }
        public long? ManufacringCountry { get; set; }
        public long? ProductNameId { get; set; }
        public long? PackingUnitsId { get; set; }
        public long? SupplyToId { get; set; }
        public bool? IsSimulation { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CompanyListing SupplyTo { get; set; }
        public virtual ICollection<CommitmentOrdersLine> CommitmentOrdersLine { get; set; }
        public virtual ICollection<DistributorReplenishmentLine> DistributorReplenishmentLine { get; set; }
        public virtual ICollection<GenericCodeCountry> GenericCodeCountry { get; set; }
        public virtual ICollection<GenericCodeSupplyToMultiple> GenericCodeSupplyToMultiple { get; set; }
        public virtual ICollection<MarginInformationLine> MarginInformationLine { get; set; }
        public virtual ICollection<Navitems> Navitems { get; set; }
        public virtual ICollection<OrderRequirement> OrderRequirement { get; set; }
        public virtual ICollection<ProductGroupingManufacture> ProductGroupingManufacture { get; set; }
        public virtual ICollection<QuotationAward> QuotationAward { get; set; }
        public virtual ICollection<QuotationHistoryLine> QuotationHistoryLine { get; set; }
        public virtual ICollection<SalesBorrowLine> SalesBorrowLine { get; set; }
        public virtual ICollection<SalesMainPromotionList> SalesMainPromotionList { get; set; }
        public virtual ICollection<SellingPriceInformation> SellingPriceInformation { get; set; }
        public virtual ICollection<SobyCustomerSunwardEquivalent> SobyCustomerSunwardEquivalent { get; set; }
        public virtual ICollection<SocustomersItemCrossReference> SocustomersItemCrossReference { get; set; }
    }
}
