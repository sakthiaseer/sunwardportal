﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Employee
    {
        public Employee()
        {
            ApplicationUser = new HashSet<ApplicationUser>();
            ApplicationUserPlant = new HashSet<ApplicationUserPlant>();
            ApplicationWiki = new HashSet<ApplicationWiki>();
            ApplicationWikiLine = new HashSet<ApplicationWikiLine>();
            ApplicationWikiLineDuty = new HashSet<ApplicationWikiLineDuty>();
            ApproveMaster = new HashSet<ApproveMaster>();
            Department = new HashSet<Department>();
            DraftApplicationWiki = new HashSet<DraftApplicationWiki>();
            DraftApplicationWikiLine = new HashSet<DraftApplicationWikiLine>();
            DraftApplicationWikiLineDuty = new HashSet<DraftApplicationWikiLineDuty>();
            DraftWikiResponsible = new HashSet<DraftWikiResponsible>();
            EmployeeAdditionalInformation = new HashSet<EmployeeAdditionalInformation>();
            EmployeeEmailInfo = new HashSet<EmployeeEmailInfo>();
            EmployeeIcthardInformation = new HashSet<EmployeeIcthardInformation>();
            EmployeeIctinformation = new HashSet<EmployeeIctinformation>();
            EmployeeInformation = new HashSet<EmployeeInformation>();
            EmployeeLeaveInformation = new HashSet<EmployeeLeaveInformation>();
            EmployeeOtherDutyInformation = new HashSet<EmployeeOtherDutyInformation>();
            EmployeeReportPerson = new HashSet<EmployeeReportPerson>();
            EmployeeReportTo = new HashSet<EmployeeReportTo>();
            EmployeeResignation = new HashSet<EmployeeResignation>();
            EmployeeSageInformation = new HashSet<EmployeeSageInformation>();
            EmployeeSign = new HashSet<EmployeeSign>();
            HumanMovement = new HashSet<HumanMovement>();
            HumanMovementActionHrlogin = new HashSet<HumanMovementAction>();
            HumanMovementActionPharmacist = new HashSet<HumanMovementAction>();
            HumanMovementCompanyRelated = new HashSet<HumanMovementCompanyRelated>();
            HumanMovementPrivate = new HashSet<HumanMovementPrivate>();
            JobProgressTemplateNotify = new HashSet<JobProgressTemplateNotify>();
            NotifyDocument = new HashSet<NotifyDocument>();
            ProductActivityCaseResponsDuty = new HashSet<ProductActivityCaseResponsDuty>();
            ProductActivityCaseResponsResponsible = new HashSet<ProductActivityCaseResponsResponsible>();
            ReAssignementManpowerMultiple = new HashSet<ReAssignementManpowerMultiple>();
            SalesTargetByStaff = new HashSet<SalesTargetByStaff>();
            SalesTargetByStaffLine = new HashSet<SalesTargetByStaffLine>();
            SampleRequestForm = new HashSet<SampleRequestForm>();
            SelfTest = new HashSet<SelfTest>();
            SelfTestCompanyRelated = new HashSet<SelfTestCompanyRelated>();
            SelfTestPrivate = new HashSet<SelfTestPrivate>();
            StartOfDayManPowerMultiple = new HashSet<StartOfDayManPowerMultiple>();
            TemplateTestCaseCheckListResponse = new HashSet<TemplateTestCaseCheckListResponse>();
            TemplateTestCaseCheckListResponseDuty = new HashSet<TemplateTestCaseCheckListResponseDuty>();
            TemplateTestCaseCheckListResponseResponsible = new HashSet<TemplateTestCaseCheckListResponseResponsible>();
            WikiResponsible = new HashSet<WikiResponsible>();
        }

        public long EmployeeId { get; set; }
        public long? UserId { get; set; }
        public string SageId { get; set; }
        public long? PlantId { get; set; }
        public long? LevelId { get; set; }
        public long? DepartmentId { get; set; }
        public long? DesignationId { get; set; }
        public string FirstName { get; set; }
        public string NickName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string JobTitle { get; set; }
        public string Email { get; set; }
        public int? TypeOfEmployeement { get; set; }
        public long? LanguageId { get; set; }
        public long? SectionId { get; set; }
        public long? SubSectionId { get; set; }
        public long? CityId { get; set; }
        public long? RegionId { get; set; }
        public string Signature { get; set; }
        public string ImageUrl { get; set; }
        public DateTime? DateOfEmployeement { get; set; }
        public DateTime? LastWorkingDate { get; set; }
        public string Extension { get; set; }
        public string SpeedDial { get; set; }
        public string Mobile { get; set; }
        public string SkypeAddress { get; set; }
        public long? ReportId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsActive { get; set; }
        public long? SubSectionTid { get; set; }
        public int? HeadCount { get; set; }
        public long? DivisionId { get; set; }
        public byte[] AcceptanceLetter { get; set; }
        public DateTime? ExpectedJoiningDate { get; set; }
        public long? AcceptanceStatus { get; set; }
        public DateTime? AcceptanceStatusDate { get; set; }
        public string Nricno { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CityMaster City { get; set; }
        public virtual Department DepartmentNavigation { get; set; }
        public virtual Designation Designation { get; set; }
        public virtual Division Division { get; set; }
        public virtual LanguageMaster Language { get; set; }
        public virtual LevelMaster Level { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Plant Plant { get; set; }
        public virtual RegionMaster Region { get; set; }
        public virtual ApplicationUser Report { get; set; }
        public virtual Section Section { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual SubSection SubSection { get; set; }
        public virtual SubSectionTwo SubSectionT { get; set; }
        public virtual CodeMaster TypeOfEmployeementNavigation { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual ICollection<ApplicationUser> ApplicationUser { get; set; }
        public virtual ICollection<ApplicationUserPlant> ApplicationUserPlant { get; set; }
        public virtual ICollection<ApplicationWiki> ApplicationWiki { get; set; }
        public virtual ICollection<ApplicationWikiLine> ApplicationWikiLine { get; set; }
        public virtual ICollection<ApplicationWikiLineDuty> ApplicationWikiLineDuty { get; set; }
        public virtual ICollection<ApproveMaster> ApproveMaster { get; set; }
        public virtual ICollection<Department> Department { get; set; }
        public virtual ICollection<DraftApplicationWiki> DraftApplicationWiki { get; set; }
        public virtual ICollection<DraftApplicationWikiLine> DraftApplicationWikiLine { get; set; }
        public virtual ICollection<DraftApplicationWikiLineDuty> DraftApplicationWikiLineDuty { get; set; }
        public virtual ICollection<DraftWikiResponsible> DraftWikiResponsible { get; set; }
        public virtual ICollection<EmployeeAdditionalInformation> EmployeeAdditionalInformation { get; set; }
        public virtual ICollection<EmployeeEmailInfo> EmployeeEmailInfo { get; set; }
        public virtual ICollection<EmployeeIcthardInformation> EmployeeIcthardInformation { get; set; }
        public virtual ICollection<EmployeeIctinformation> EmployeeIctinformation { get; set; }
        public virtual ICollection<EmployeeInformation> EmployeeInformation { get; set; }
        public virtual ICollection<EmployeeLeaveInformation> EmployeeLeaveInformation { get; set; }
        public virtual ICollection<EmployeeOtherDutyInformation> EmployeeOtherDutyInformation { get; set; }
        public virtual ICollection<EmployeeReportPerson> EmployeeReportPerson { get; set; }
        public virtual ICollection<EmployeeReportTo> EmployeeReportTo { get; set; }
        public virtual ICollection<EmployeeResignation> EmployeeResignation { get; set; }
        public virtual ICollection<EmployeeSageInformation> EmployeeSageInformation { get; set; }
        public virtual ICollection<EmployeeSign> EmployeeSign { get; set; }
        public virtual ICollection<HumanMovement> HumanMovement { get; set; }
        public virtual ICollection<HumanMovementAction> HumanMovementActionHrlogin { get; set; }
        public virtual ICollection<HumanMovementAction> HumanMovementActionPharmacist { get; set; }
        public virtual ICollection<HumanMovementCompanyRelated> HumanMovementCompanyRelated { get; set; }
        public virtual ICollection<HumanMovementPrivate> HumanMovementPrivate { get; set; }
        public virtual ICollection<JobProgressTemplateNotify> JobProgressTemplateNotify { get; set; }
        public virtual ICollection<NotifyDocument> NotifyDocument { get; set; }
        public virtual ICollection<ProductActivityCaseResponsDuty> ProductActivityCaseResponsDuty { get; set; }
        public virtual ICollection<ProductActivityCaseResponsResponsible> ProductActivityCaseResponsResponsible { get; set; }
        public virtual ICollection<ReAssignementManpowerMultiple> ReAssignementManpowerMultiple { get; set; }
        public virtual ICollection<SalesTargetByStaff> SalesTargetByStaff { get; set; }
        public virtual ICollection<SalesTargetByStaffLine> SalesTargetByStaffLine { get; set; }
        public virtual ICollection<SampleRequestForm> SampleRequestForm { get; set; }
        public virtual ICollection<SelfTest> SelfTest { get; set; }
        public virtual ICollection<SelfTestCompanyRelated> SelfTestCompanyRelated { get; set; }
        public virtual ICollection<SelfTestPrivate> SelfTestPrivate { get; set; }
        public virtual ICollection<StartOfDayManPowerMultiple> StartOfDayManPowerMultiple { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponse> TemplateTestCaseCheckListResponse { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponseDuty> TemplateTestCaseCheckListResponseDuty { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponseResponsible> TemplateTestCaseCheckListResponseResponsible { get; set; }
        public virtual ICollection<WikiResponsible> WikiResponsible { get; set; }
    }
}
