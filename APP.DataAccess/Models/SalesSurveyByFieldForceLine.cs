﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SalesSurveyByFieldForceLine
    {
        public long SalesSurveyByFieldForceLineId { get; set; }
        public long? SalesSurveyByFieldForceId { get; set; }
        public long? ProductGroupSurveyId { get; set; }
        public bool? SpecificCombinationDrug { get; set; }
        public string Symptoms { get; set; }
        public string IsUseSwproduct { get; set; }
        public string DoctorBuyFrom { get; set; }
        public string PossibleIndicatePrice { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProductGroupSurvey ProductGroupSurvey { get; set; }
        public virtual SalesSurveyByFieldForce SalesSurveyByFieldForce { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
