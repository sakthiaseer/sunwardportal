﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PkgregisteredPackingInformation
    {
        public PkgregisteredPackingInformation()
        {
            PkginformationByPackSize = new HashSet<PkginformationByPackSize>();
        }

        public long PkgregisteredPackingInformationId { get; set; }
        public long? PackagingItemId { get; set; }
        public long? PkgapprovalInformationId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual PkgapprovalInformation PkgapprovalInformation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<PkginformationByPackSize> PkginformationByPackSize { get; set; }
    }
}
