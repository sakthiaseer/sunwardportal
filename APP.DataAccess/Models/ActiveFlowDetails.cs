﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ActiveFlowDetails
    {
        public ActiveFlowDetails()
        {
            ActiveFlowFormValue = new HashSet<ActiveFlowFormValue>();
        }

        public long ActieFlowDetailId { get; set; }
        public long ActiveFlowId { get; set; }
        public long DynamicFlowDetailId { get; set; }
        public long DynamicFlowStepId { get; set; }
        public long InitiatorId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? StatusCodeId { get; set; }

        public virtual ActiveFlow ActiveFlow { get; set; }
        public virtual DynamicFlowDetail DynamicFlowDetail { get; set; }
        public virtual DynamicFlowStep DynamicFlowStep { get; set; }
        public virtual ApplicationUser Initiator { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ActiveFlowFormValue> ActiveFlowFormValue { get; set; }
    }
}
