﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TaskMembers
    {
        public long TaskMemberId { get; set; }
        public long? TaskId { get; set; }
        public long? AssignedCc { get; set; }
        public DateTime? DueDate { get; set; }
        public bool? IsRead { get; set; }
        public int? StatusCodeId { get; set; }
        public DateTime? NewDueDate { get; set; }

        public virtual ApplicationUser AssignedCcNavigation { get; set; }
        public virtual TaskMaster Task { get; set; }
    }
}
