﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WikiAccessRight
    {
        public long WikiAccessRightId { get; set; }
        public long? GroupId { get; set; }
        public long? UserId { get; set; }
        public long? PageId { get; set; }
        public int? TypeOfRights { get; set; }
        public int? Duty { get; set; }
        public bool? IsPrintable { get; set; }
        public bool? IsEdit { get; set; }
        public string SessionId { get; set; }
        public bool? IsTemp { get; set; }

        public virtual UserGroup Group { get; set; }
        public virtual WikiPage Page { get; set; }
        public virtual CodeMaster TypeOfRightsNavigation { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
