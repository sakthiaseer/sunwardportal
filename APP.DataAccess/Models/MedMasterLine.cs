﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class MedMasterLine
    {
        public MedMasterLine()
        {
            MedMasterLineMachine = new HashSet<MedMasterLineMachine>();
        }

        public long MedMasterLineId { get; set; }
        public long? MedMasterId { get; set; }
        public long? ItemId { get; set; }
        public string SerialNo { get; set; }
        public long? InventoryTypeId { get; set; }
        public long? LocationId { get; set; }
        public long? AreaId { get; set; }
        public long? SpecificAreaId { get; set; }
        public bool? IsFixPartMed { get; set; }
        public bool? IsDedicatedUsage { get; set; }
        public int? DedicatedUsageId { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public long? DedicatedUsageLocationId { get; set; }
        public long? DedicatedUsageAreaId { get; set; }
        public long? DedicatedUsagespecificAreaId { get; set; }
        public long? ManufacturingProcessId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Ictmaster Area { get; set; }
        public virtual CodeMaster DedicatedUsage { get; set; }
        public virtual Ictmaster DedicatedUsageArea { get; set; }
        public virtual Ictmaster DedicatedUsageLocation { get; set; }
        public virtual Ictmaster DedicatedUsagespecificArea { get; set; }
        public virtual InventoryType InventoryType { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual Ictmaster Location { get; set; }
        public virtual ApplicationMasterDetail ManufacturingProcess { get; set; }
        public virtual MedMaster MedMaster { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Ictmaster SpecificArea { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<MedMasterLineMachine> MedMasterLineMachine { get; set; }
    }
}
