﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ChangeControlForm
    {
        public long ChangeControlFormId { get; set; }
        public string VersionNo { get; set; }
        public string Ccnumber { get; set; }
        public string DocNo { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
