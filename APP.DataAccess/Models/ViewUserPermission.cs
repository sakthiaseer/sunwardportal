﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewUserPermission
    {
        public long PermissionId { get; set; }
        public string PermissionName { get; set; }
        public long? ParentId { get; set; }
        public byte? PermissionLevel { get; set; }
        public string PermissionUrl { get; set; }
        public string PermissionGroup { get; set; }
        public string PermissionOrder { get; set; }
        public string PermissionCode { get; set; }
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public bool? IsPermissionUrl { get; set; }
        public Guid? UniqueSessionId { get; set; }
        public bool? IsDisplay { get; set; }
        public bool? IsMobile { get; set; }
        public bool? IsNewPortal { get; set; }
        public string Component { get; set; }
        public string Name { get; set; }
        public long UserId { get; set; }
        public string Icon { get; set; }
        public string MenuId { get; set; }
        public string ScreenId { get; set; }
        public bool? IsProductionApp { get; set; }
        public bool? IsCmsApp { get; set; }
    }
}
