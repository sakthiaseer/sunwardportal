﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WorkListItem
    {
        public long WorkListItemId { get; set; }
        public long? WorkListId { get; set; }
        public long? DocumentId { get; set; }

        public virtual WorkList WorkList { get; set; }
    }
}
