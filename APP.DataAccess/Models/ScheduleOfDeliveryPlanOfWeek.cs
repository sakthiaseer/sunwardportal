﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ScheduleOfDeliveryPlanOfWeek
    {
        public long ScheduleOfDeliveryPlanOfWeekId { get; set; }
        public long? ScheduleOfDeliveryId { get; set; }
        public int? DayOfTheWeekId { get; set; }

        public virtual CodeMaster DayOfTheWeek { get; set; }
        public virtual ScheduleOfDelivery ScheduleOfDelivery { get; set; }
    }
}
