﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavMarkingMultiple
    {
        public long NavMarkingMultipleId { get; set; }
        public long? ProductMasterId { get; set; }
        public long? ApplicationMasterDetailId { get; set; }

        public virtual NavProductMaster ProductMaster { get; set; }
    }
}
