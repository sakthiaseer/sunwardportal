﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavplannedProdOrder
    {
        public long PlannedProdOrderId { get; set; }
        public long? ItemId { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string Rpono { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public decimal? Quantity { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? InpreportId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual NavinpitemReport Inpreport { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
