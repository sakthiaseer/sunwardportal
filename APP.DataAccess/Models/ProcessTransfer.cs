﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProcessTransfer
    {
        public ProcessTransfer()
        {
            ProcessTransferLine = new HashSet<ProcessTransferLine>();
        }

        public long ProcessTransferId { get; set; }
        public string LocationFrom { get; set; }
        public string LocationTo { get; set; }
        public string TrolleyPalletNo { get; set; }
        public int? StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string TransferAction { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ProcessTransferLine> ProcessTransferLine { get; set; }
    }
}
