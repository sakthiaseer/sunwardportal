﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class InventoryTypeOpeningStockBalance
    {
        public InventoryTypeOpeningStockBalance()
        {
            InventoryTypeOpeningStockBalanceLine = new HashSet<InventoryTypeOpeningStockBalanceLine>();
        }

        public long OpeningBalanceId { get; set; }
        public long? InventoryTypeId { get; set; }
        public long? LocationId { get; set; }
        public long? AreaId { get; set; }
        public long? SpecificAreaId { get; set; }
        public string ItemCategory { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Ictmaster Area { get; set; }
        public virtual InventoryType InventoryType { get; set; }
        public virtual Ictmaster Location { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Ictmaster SpecificArea { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<InventoryTypeOpeningStockBalanceLine> InventoryTypeOpeningStockBalanceLine { get; set; }
    }
}
