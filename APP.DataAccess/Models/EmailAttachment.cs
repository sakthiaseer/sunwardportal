﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmailAttachment
    {
        public long EmailAttachmentId { get; set; }
        public long? EmailsId { get; set; }
        public long? DocumentId { get; set; }

        public virtual Documents Document { get; set; }
        public virtual Emails Emails { get; set; }
    }
}
