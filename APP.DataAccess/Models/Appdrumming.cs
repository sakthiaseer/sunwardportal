﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Appdrumming
    {
        public long DrummingId { get; set; }
        public string WrokOrderNo { get; set; }
        public string ProdOrderNo { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string SublotNo { get; set; }
        public string DrumNo { get; set; }
        public decimal? DrumWeight { get; set; }
        public string BagNo { get; set; }
        public decimal? BagWeight { get; set; }
        public decimal? TotalWeight { get; set; }
        public bool? PostedtoNav { get; set; }
        public string Company { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
