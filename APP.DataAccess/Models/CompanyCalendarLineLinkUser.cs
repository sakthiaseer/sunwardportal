﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CompanyCalendarLineLinkUser
    {
        public long CompanyCalendarLineLinkUserId { get; set; }
        public long? CompanyCalendarLineLinkId { get; set; }
        public long? UserId { get; set; }

        public virtual CompanyCalendarLineLink CompanyCalendarLineLink { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
