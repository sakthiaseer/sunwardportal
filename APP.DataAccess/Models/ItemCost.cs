﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ItemCost
    {
        public ItemCost()
        {
            ItemCostLine = new HashSet<ItemCostLine>();
        }

        public long ItemCostId { get; set; }
        public long? PurposeOfCostingId { get; set; }
        public long? CompanyId { get; set; }
        public DateTime? ValidityFrom { get; set; }
        public DateTime? ValidityTo { get; set; }
        public long? ItemId { get; set; }
        public string CostMethod { get; set; }
        public string CostCurrency { get; set; }
        public decimal? Cost { get; set; }
        public string Uom { get; set; }
        public Guid? SessionId { get; set; }
        public bool? IsSyncWithNavision { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? VersionSessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ItemCostLine> ItemCostLine { get; set; }
    }
}
