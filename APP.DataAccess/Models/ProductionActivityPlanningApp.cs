﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionActivityPlanningApp
    {
        public ProductionActivityPlanningApp()
        {
            ProductionActivityPlanningAppLine = new HashSet<ProductionActivityPlanningAppLine>();
        }

        public long ProductionActivityPlanningAppId { get; set; }
        public long? CompanyId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public string Comment { get; set; }
        public string TopicId { get; set; }
        public long? ManufacturingProcessChildId { get; set; }
        public long? LocationId { get; set; }
        public string ProdOrderNo { get; set; }
        public long? ProductActivityCaseId { get; set; }
        public bool? IsOthersOptions { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual Ictmaster Location { get; set; }
        public virtual ApplicationMasterChild ManufacturingProcessChild { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProductActivityCase ProductActivityCase { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ProductionActivityPlanningAppLine> ProductionActivityPlanningAppLine { get; set; }
    }
}
