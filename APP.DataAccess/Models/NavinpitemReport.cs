﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavinpitemReport
    {
        public NavinpitemReport()
        {
            NavplannedProdOrder = new HashSet<NavplannedProdOrder>();
        }

        public long InpreportId { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? QuantityBase { get; set; }
        public decimal? DemandQuantity { get; set; }
        public decimal? SupplyQuantity { get; set; }
        public DateTime? CompletionDate { get; set; }
        public DateTime? ShipmentDate { get; set; }
        public string Company { get; set; }
        public string Uom { get; set; }
        public string ItemCategory { get; set; }
        public string PlannerComment { get; set; }
        public decimal? Inventory { get; set; }
        public decimal? SaftyStock { get; set; }
        public long? ItemId { get; set; }
        public long? CustomerId { get; set; }
        public decimal? SuppliedQuantity { get; set; }
        public string DocumentNo { get; set; }
        public long? CompanyId { get; set; }

        public virtual ICollection<NavplannedProdOrder> NavplannedProdOrder { get; set; }
    }
}
