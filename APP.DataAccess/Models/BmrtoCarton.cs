﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class BmrtoCarton
    {
        public BmrtoCarton()
        {
            BmrtoCartonLine = new HashSet<BmrtoCartonLine>();
        }

        public long BmrtoCartonId { get; set; }
        public string Displaybox { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<BmrtoCartonLine> BmrtoCartonLine { get; set; }
    }
}
