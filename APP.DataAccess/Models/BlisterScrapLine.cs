﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class BlisterScrapLine
    {
        public long BlisterScrapLineId { get; set; }
        public long? BlisterScrapId { get; set; }
        public string Type { get; set; }
        public string ProdOrderNo { get; set; }
        public string Description { get; set; }
        public int? CalculationType { get; set; }
        public decimal? AddonQty { get; set; }
        public decimal? QtyPer { get; set; }
        public string Uom { get; set; }
        public decimal? ScrapPercentage { get; set; }
        public bool? Qcinheritance { get; set; }
        public bool? Inheritance { get; set; }

        public virtual BlisterScrapMaster BlisterScrap { get; set; }
    }
}
