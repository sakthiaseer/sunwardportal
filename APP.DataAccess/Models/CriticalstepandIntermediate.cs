﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CriticalstepandIntermediate
    {
        public CriticalstepandIntermediate()
        {
            CriticalstepandIntermediateLine = new HashSet<CriticalstepandIntermediateLine>();
        }

        public long CriticalstepandIntermediateId { get; set; }
        public long? FinishProductiId { get; set; }
        public int? RegisterationCodeId { get; set; }
        public bool? IsMasterDocument { get; set; }
        public bool? IsInformationvsmaster { get; set; }
        public long? FinishProductGeneralInfoId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual FinishProductGeneralInfo FinishProductGeneralInfo { get; set; }
        public virtual FinishProduct FinishProducti { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster RegisterationCode { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<CriticalstepandIntermediateLine> CriticalstepandIntermediateLine { get; set; }
    }
}
