﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FinishProductGeneralInfo
    {
        public FinishProductGeneralInfo()
        {
            CountryInformationStatus = new HashSet<CountryInformationStatus>();
            CriticalstepandIntermediate = new HashSet<CriticalstepandIntermediate>();
            FinishProdcutGeneralInfoLine = new HashSet<FinishProdcutGeneralInfoLine>();
            FinishProductExcipient = new HashSet<FinishProductExcipient>();
            FpmanufacturingRecipe = new HashSet<FpmanufacturingRecipe>();
            Fpproduct = new HashSet<Fpproduct>();
            ManufacturingProcess = new HashSet<ManufacturingProcess>();
            NavProductMaster = new HashSet<NavProductMaster>();
            PhramacologicalProperties = new HashSet<PhramacologicalProperties>();
            RecordVariation = new HashSet<RecordVariation>();
            SalesItemPackingInfo = new HashSet<SalesItemPackingInfo>();
            TempSalesPackInformation = new HashSet<TempSalesPackInformation>();
        }

        public long FinishProductGeneralInfoId { get; set; }
        public long? FinishProductId { get; set; }
        public int? RegisterationCodeId { get; set; }
        public long RegisterCountry { get; set; }
        public string RegisterCountryProductName { get; set; }
        public long? RegisterProductOwnerId { get; set; }
        public long? DrugClassificationId { get; set; }
        public string CallNo { get; set; }
        public string RegistrationReferenceNo { get; set; }
        public string RegistrationNo { get; set; }
        public DateTime? RegistrationDateOfSubmission { get; set; }
        public DateTime? RegistrationDateOfIssue { get; set; }
        public DateTime? RegistrationDateOfExpiry { get; set; }
        public DateTime? RegistrationDateOfApproval { get; set; }
        public long? RegistrationDocumentId { get; set; }
        public string LinkComment { get; set; }
        public string LinkDmscomment { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ProductRegistrationHolderId { get; set; }
        public long? ProductionRegistrationHolderId { get; set; }
        public long? PrhspecificProductId { get; set; }
        public long? RegisterationReferenceId { get; set; }
        public long? RegistrationProductCategoryId { get; set; }
        public long? RegistrationCategoryClassId { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual FinishProduct FinishProduct { get; set; }
        public virtual ApplicationUser ModifiedUser { get; set; }
        public virtual CompanyListing ProductionRegistrationHolder { get; set; }
        public virtual CodeMaster RegisterationCode { get; set; }
        public virtual Documents RegistrationDocument { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<CountryInformationStatus> CountryInformationStatus { get; set; }
        public virtual ICollection<CriticalstepandIntermediate> CriticalstepandIntermediate { get; set; }
        public virtual ICollection<FinishProdcutGeneralInfoLine> FinishProdcutGeneralInfoLine { get; set; }
        public virtual ICollection<FinishProductExcipient> FinishProductExcipient { get; set; }
        public virtual ICollection<FpmanufacturingRecipe> FpmanufacturingRecipe { get; set; }
        public virtual ICollection<Fpproduct> Fpproduct { get; set; }
        public virtual ICollection<ManufacturingProcess> ManufacturingProcess { get; set; }
        public virtual ICollection<NavProductMaster> NavProductMaster { get; set; }
        public virtual ICollection<PhramacologicalProperties> PhramacologicalProperties { get; set; }
        public virtual ICollection<RecordVariation> RecordVariation { get; set; }
        public virtual ICollection<SalesItemPackingInfo> SalesItemPackingInfo { get; set; }
        public virtual ICollection<TempSalesPackInformation> TempSalesPackInformation { get; set; }
    }
}
