﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PageLikes
    {
        public long PageLikeId { get; set; }
        public long? PageId { get; set; }
        public long? UserId { get; set; }
        public int? Likes { get; set; }
        public DateTime? AddedDate { get; set; }

        public virtual WikiPage Page { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
