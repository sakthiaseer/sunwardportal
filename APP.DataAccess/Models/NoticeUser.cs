﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NoticeUser
    {
        public long NoticeUserId { get; set; }
        public long? NoticeId { get; set; }
        public long? UserId { get; set; }
        public long? UserGroupId { get; set; }

        public virtual Notice Notice { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual UserGroup UserGroup { get; set; }
    }
}
