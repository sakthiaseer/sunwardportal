﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TaskAttachment
    {
        public TaskAttachment()
        {
            TaskMeetingNote = new HashSet<TaskMeetingNote>();
        }

        public long TaskAttachmentId { get; set; }
        public long? TaskMasterId { get; set; }
        public long? DocumentId { get; set; }
        public long? PreviousDocumentId { get; set; }
        public bool? IsLatest { get; set; }
        public bool? IsLocked { get; set; }
        public string VersionNo { get; set; }
        public bool? IsMajorChange { get; set; }
        public long? LockedByUserId { get; set; }
        public DateTime? LockedDate { get; set; }
        public long? UploadedByUserId { get; set; }
        public DateTime? UploadedDate { get; set; }
        public bool? IsMeetingNotes { get; set; }
        public bool? IsDiscussionNotes { get; set; }
        public bool? IsSubTask { get; set; }
        public string ActualVersionNo { get; set; }
        public bool? IsComment { get; set; }
        public long? TaskCommentId { get; set; }
        public bool? IsReleaseVersion { get; set; }
        public bool? IsNoChange { get; set; }
        public string DraftingVersionNo { get; set; }
        public string CheckInDescription { get; set; }
        public bool? IsLatestCommentAttachment { get; set; }
        public long? FileProfieTypeId { get; set; }
        public long? FolderId { get; set; }

        public virtual Documents Document { get; set; }
        public virtual FileProfileType FileProfieType { get; set; }
        public virtual Folders Folder { get; set; }
        public virtual ApplicationUser LockedByUser { get; set; }
        public virtual TaskComment TaskComment { get; set; }
        public virtual TaskMaster TaskMaster { get; set; }
        public virtual ApplicationUser UploadedByUser { get; set; }
        public virtual ICollection<TaskMeetingNote> TaskMeetingNote { get; set; }
    }
}
