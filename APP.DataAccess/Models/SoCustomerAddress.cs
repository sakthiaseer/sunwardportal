﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SoCustomerAddress
    {
        public SoCustomerAddress()
        {
            SoSalesOrderSoCustomerBillingAddress = new HashSet<SoSalesOrder>();
            SoSalesOrderSoCustomerShipingAddress = new HashSet<SoSalesOrder>();
        }

        public long SoCustomerAddressId { get; set; }
        public long CustomerId { get; set; }
        public long AddressId { get; set; }
        public string AddressType { get; set; }
        public bool? IsBilling { get; set; }
        public bool? IsShipping { get; set; }

        public virtual ICollection<SoSalesOrder> SoSalesOrderSoCustomerBillingAddress { get; set; }
        public virtual ICollection<SoSalesOrder> SoSalesOrderSoCustomerShipingAddress { get; set; }
    }
}
