﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ChatMessage
    {
        public long ChatMessageId { get; set; }
        public long? ChatGroupId { get; set; }
        public long? SentUserId { get; set; }
        public long? ReceiveUserId { get; set; }
        public string Message { get; set; }
        public DateTime? CreatedDateTime { get; set; }
        public long? ParentMessageId { get; set; }
        public bool? IsDelivered { get; set; }
        public int? MessageType { get; set; }
        public string FileName { get; set; }

        public virtual ChatGroup ChatGroup { get; set; }
        public virtual ApplicationUser ReceiveUser { get; set; }
        public virtual ApplicationUser SentUser { get; set; }
    }
}
