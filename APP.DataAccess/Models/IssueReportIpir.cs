﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class IssueReportIpir
    {
        public IssueReportIpir()
        {
            IssueReportAssignTo = new HashSet<IssueReportAssignTo>();
            IssueReportCcto = new HashSet<IssueReportCcto>();
        }

        public long Ipirid { get; set; }
        public string Location { get; set; }
        public string ProdOrderNo { get; set; }
        public string Description { get; set; }
        public int? BatchSize { get; set; }
        public string Uom { get; set; }
        public string BatchNo { get; set; }
        public string IssueTitle { get; set; }
        public Guid? DocSessionId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileNo { get; set; }
        public long? CompanyId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<IssueReportAssignTo> IssueReportAssignTo { get; set; }
        public virtual ICollection<IssueReportCcto> IssueReportCcto { get; set; }
    }
}
