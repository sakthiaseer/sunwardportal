﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SobyCustomerModeDelivery
    {
        public long SobyCustomerModeDeliveryId { get; set; }
        public long? SocustomersDeliveryId { get; set; }
        public long? ModeOfDeliveryId { get; set; }

        public virtual SocustomersDelivery SocustomersDelivery { get; set; }
    }
}
