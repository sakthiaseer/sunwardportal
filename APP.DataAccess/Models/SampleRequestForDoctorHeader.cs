﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SampleRequestForDoctorHeader
    {
        public SampleRequestForDoctorHeader()
        {
            SampleRequestForDoctor = new HashSet<SampleRequestForDoctor>();
        }

        public long SampleRequestForDoctorHeaderId { get; set; }
        public long? TypeOfStockId { get; set; }
        public long? ProfileId { get; set; }
        public string SampleChitNo { get; set; }
        public DateTime? DateBy { get; set; }
        public long? RequestById { get; set; }
        public long? InventoryTypeId { get; set; }
        public long? ClassComCustomerId { get; set; }
        public string Purpose { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsConfirm { get; set; }
        public string Signature { get; set; }
        public string Remarks { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CompanyListing ClassComCustomer { get; set; }
        public virtual InventoryType InventoryType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual ApplicationUser RequestBy { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationMasterDetail TypeOfStock { get; set; }
        public virtual ICollection<SampleRequestForDoctor> SampleRequestForDoctor { get; set; }
    }
}
