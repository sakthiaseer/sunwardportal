﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApplicationRole
    {
        public ApplicationRole()
        {
            ApplicationFormPermission = new HashSet<ApplicationFormPermission>();
            ApplicationRolePermission = new HashSet<ApplicationRolePermission>();
            ApplicationUserRole = new HashSet<ApplicationUserRole>();
            EmployeeIctinformation = new HashSet<EmployeeIctinformation>();
            EmployeeIctrole = new HashSet<EmployeeIctrole>();
            FolderStorage = new HashSet<FolderStorage>();
        }

        public long RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ApplicationFormPermission> ApplicationFormPermission { get; set; }
        public virtual ICollection<ApplicationRolePermission> ApplicationRolePermission { get; set; }
        public virtual ICollection<ApplicationUserRole> ApplicationUserRole { get; set; }
        public virtual ICollection<EmployeeIctinformation> EmployeeIctinformation { get; set; }
        public virtual ICollection<EmployeeIctrole> EmployeeIctrole { get; set; }
        public virtual ICollection<FolderStorage> FolderStorage { get; set; }
    }
}
