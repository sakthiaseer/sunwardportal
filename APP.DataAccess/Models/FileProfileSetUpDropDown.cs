﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FileProfileSetUpDropDown
    {
        public long FileProfileSetUpDropDownId { get; set; }
        public long? FileProfileSetupFormId { get; set; }
        public string DisplayValue { get; set; }
    }
}
