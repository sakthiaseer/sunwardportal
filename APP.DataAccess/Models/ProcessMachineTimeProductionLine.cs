﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProcessMachineTimeProductionLine
    {
        public ProcessMachineTimeProductionLine()
        {
            MachineTimeManHourInfo = new HashSet<MachineTimeManHourInfo>();
            ProcessMachineTimeProductionLineLine = new HashSet<ProcessMachineTimeProductionLineLine>();
            ProcessManPowerSpecialNotes = new HashSet<ProcessManPowerSpecialNotes>();
        }

        public long ProcessMachineTimeLineId { get; set; }
        public long? ProcessMachineTimeId { get; set; }
        public long? ProductionCentreId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ItemId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProcessMachineTime ProcessMachineTime { get; set; }
        public virtual ApplicationMasterChild ProductionCentre { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<MachineTimeManHourInfo> MachineTimeManHourInfo { get; set; }
        public virtual ICollection<ProcessMachineTimeProductionLineLine> ProcessMachineTimeProductionLineLine { get; set; }
        public virtual ICollection<ProcessManPowerSpecialNotes> ProcessManPowerSpecialNotes { get; set; }
    }
}
