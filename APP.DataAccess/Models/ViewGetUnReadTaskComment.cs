﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewGetUnReadTaskComment
    {
        public long TaskCommentId { get; set; }
        public long? TaskMasterId { get; set; }
        public long? MainTaskId { get; set; }
        public string Comment { get; set; }
        public long? CommentedBy { get; set; }
        public DateTime? CommentedDate { get; set; }
        public long? ParentCommentId { get; set; }
        public bool? IsRead { get; set; }
        public string TaskSubject { get; set; }
        public string CommentedByName { get; set; }
        public string TaskName { get; set; }
        public bool? IsNoDueDate { get; set; }
        public DateTime? TaskDueDate { get; set; }
        public DateTime? SubjectDueDate { get; set; }
        public DateTime? LastCommentedDate { get; set; }
    }
}
