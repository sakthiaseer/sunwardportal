﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductChemicalSubGroup
    {
        public long ProductChemicalSubGroupId { get; set; }
        public long? FinishProductId { get; set; }
        public long? ChemicalSubGroupId { get; set; }

        public virtual FinishProduct FinishProduct { get; set; }
    }
}
