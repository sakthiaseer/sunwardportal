﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PlantMaintenanceEntryMaster
    {
        public PlantMaintenanceEntryMaster()
        {
            PlantMaintenanceEntryLineCategory = new HashSet<PlantMaintenanceEntryLine>();
            PlantMaintenanceEntryLineNameGroup = new HashSet<PlantMaintenanceEntryLine>();
            PlantMaintenanceEntryLinePartGroup = new HashSet<PlantMaintenanceEntryLine>();
            PlantMaintenanceEntryLinePlantItemGroup = new HashSet<PlantMaintenanceEntryLine>();
            PlantMaintenanceEntryLineSubPartGroup = new HashSet<PlantMaintenanceEntryLine>();
            PlantMaintenanceMasterDetail = new HashSet<PlantMaintenanceMasterDetail>();
        }

        public long PlantMaintenanceEntryMasterId { get; set; }
        public string Name { get; set; }
        public int? MasterType { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster MasterTypeNavigation { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<PlantMaintenanceEntryLine> PlantMaintenanceEntryLineCategory { get; set; }
        public virtual ICollection<PlantMaintenanceEntryLine> PlantMaintenanceEntryLineNameGroup { get; set; }
        public virtual ICollection<PlantMaintenanceEntryLine> PlantMaintenanceEntryLinePartGroup { get; set; }
        public virtual ICollection<PlantMaintenanceEntryLine> PlantMaintenanceEntryLinePlantItemGroup { get; set; }
        public virtual ICollection<PlantMaintenanceEntryLine> PlantMaintenanceEntryLineSubPartGroup { get; set; }
        public virtual ICollection<PlantMaintenanceMasterDetail> PlantMaintenanceMasterDetail { get; set; }
    }
}
