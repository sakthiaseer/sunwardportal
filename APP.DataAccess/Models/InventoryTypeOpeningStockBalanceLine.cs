﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class InventoryTypeOpeningStockBalanceLine
    {
        public long OpeningStockLineId { get; set; }
        public long? OpeningStockId { get; set; }
        public long? ItemId { get; set; }
        public string BatchNo { get; set; }
        public decimal? QtyOnHand { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual InventoryTypeOpeningStockBalance OpeningStock { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
