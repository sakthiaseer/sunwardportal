﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormSectionSecurity
    {
        public DynamicFormSectionSecurity()
        {
            DynamicFormSectionWorkFlow = new HashSet<DynamicFormSectionWorkFlow>();
        }

        public long DynamicFormSectionSecurityId { get; set; }
        public long? DynamicFormSectionId { get; set; }
        public long? UserId { get; set; }
        public long? UserGroupId { get; set; }
        public long? LevelId { get; set; }
        public bool? IsReadWrite { get; set; }
        public bool? IsReadOnly { get; set; }
        public bool? IsVisible { get; set; }

        public virtual DynamicFormSection DynamicFormSection { get; set; }
        public virtual LevelMaster Level { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual UserGroup UserGroup { get; set; }
        public virtual ICollection<DynamicFormSectionWorkFlow> DynamicFormSectionWorkFlow { get; set; }
    }
}
