﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DocumentCheckOut
    {
        public long DocumentCheckOutId { get; set; }
        public long? DocumentId { get; set; }
        public long? CheckOutUserId { get; set; }
        public DateTime? CheckOutDate { get; set; }
        public DateTime? CheckInDate { get; set; }
        public bool? Status { get; set; }
    }
}
