﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SobyCustomersAddress
    {
        public SobyCustomersAddress()
        {
            SalesOrder = new HashSet<SalesOrder>();
            ScheduleOfDelivery = new HashSet<ScheduleOfDelivery>();
            SowithOutBlanketOrder = new HashSet<SowithOutBlanketOrder>();
        }

        public long SobyCustomersAddressId { get; set; }
        public long? SobyCustomersId { get; set; }
        public string LocationNo { get; set; }
        public string LocationName { get; set; }
        public bool? InvoiceAddress { get; set; }
        public string DeliverySchedule { get; set; }
        public long? SobyCustomersMasterAddressId { get; set; }
        public int? TypeOfAddressId { get; set; }
        public string NavisionNo { get; set; }
        public long? CustomerCodeId { get; set; }

        public virtual ApplicationMasterDetail CustomerCode { get; set; }
        public virtual SobyCustomers SobyCustomers { get; set; }
        public virtual SobyCustomersMasterAddress SobyCustomersMasterAddress { get; set; }
        public virtual CodeMaster TypeOfAddress { get; set; }
        public virtual ICollection<SalesOrder> SalesOrder { get; set; }
        public virtual ICollection<ScheduleOfDelivery> ScheduleOfDelivery { get; set; }
        public virtual ICollection<SowithOutBlanketOrder> SowithOutBlanketOrder { get; set; }
    }
}
