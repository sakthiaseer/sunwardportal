﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TempSalesPackInformation
    {
        public TempSalesPackInformation()
        {
            TempSalesPackInformationFactor = new HashSet<TempSalesPackInformationFactor>();
        }

        public long TempSalesPackInformationId { get; set; }
        public long? FinishproductGeneralInfoId { get; set; }
        public long? FinishProductGeneralInfoLineId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual FinishProdcutGeneralInfoLine FinishProductGeneralInfoLine { get; set; }
        public virtual FinishProductGeneralInfo FinishproductGeneralInfo { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<TempSalesPackInformationFactor> TempSalesPackInformationFactor { get; set; }
    }
}
