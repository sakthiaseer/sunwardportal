﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Tag
    {
        public Tag()
        {
            WikiTag = new HashSet<WikiTag>();
        }

        public long TagId { get; set; }
        public string Tag1 { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ICollection<WikiTag> WikiTag { get; set; }
    }
}
