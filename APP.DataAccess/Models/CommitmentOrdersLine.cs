﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CommitmentOrdersLine
    {
        public long CommitmentOrdersLineId { get; set; }
        public long? CommitmentOrdersId { get; set; }
        public long? ProductId { get; set; }
        public string Source { get; set; }
        public decimal? Quantity { get; set; }
        public long? Uomid { get; set; }
        public long? PackingId { get; set; }
        public long? ShippingTermsId { get; set; }
        public bool? IsTenderExceed { get; set; }
        public bool? IsPlanned { get; set; }
        public DateTime? CommitmentDate { get; set; }
        public Guid? SessionId { get; set; }
        public string Remarks { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CommitmentOrders CommitmentOrders { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual GenericCodes Product { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
