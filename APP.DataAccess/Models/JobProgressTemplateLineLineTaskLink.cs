﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class JobProgressTemplateLineLineTaskLink
    {
        public long JobProgressTemplateLineLineTaskLinkId { get; set; }
        public long? JobProgressTemplateLineLineId { get; set; }
        public string Subject { get; set; }
        public string TaskLink { get; set; }
        public bool? IsWiki { get; set; }
        public bool? IsLatest { get; set; }
        public string IsRequireUploadFolder { get; set; }
        public string IsWithCompleteCheck { get; set; }
        public string IsEachSubjectHeadingUpload { get; set; }

        public virtual JobProgressTemplateLineLineProcess JobProgressTemplateLineLine { get; set; }
    }
}
