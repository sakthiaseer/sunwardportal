﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CcfdimplementationDetails
    {
        public long CcfdimplementationDetailsId { get; set; }
        public long? CcfdimplementationId { get; set; }
        public long? ClassOfdocumentId { get; set; }
        public bool? IsRequired { get; set; }
        public long? ResponsibiltyTo { get; set; }
        public long? DoneBy { get; set; }
        public DateTime? DoneByDate { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterDetail ClassOfdocument { get; set; }
        public virtual ApplicationUser DoneByNavigation { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationUser ResponsibiltyToNavigation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
