﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PackagingPvcfoilBlister
    {
        public long PvcFoilBlisterId { get; set; }
        public long? PvcFoilId { get; set; }
        public long? PvcFoilBlisterTypeId { get; set; }

        public virtual PackagingPvcfoil PvcFoil { get; set; }
    }
}
