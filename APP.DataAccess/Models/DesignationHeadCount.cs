﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DesignationHeadCount
    {
        public DesignationHeadCount()
        {
            EmployeeInformation = new HashSet<EmployeeInformation>();
            EmployeeResignation = new HashSet<EmployeeResignation>();
        }

        public long DesignationHeadCountId { get; set; }
        public long? DesignationId { get; set; }
        public int? HeadCount { get; set; }

        public virtual Designation Designation { get; set; }
        public virtual ICollection<EmployeeInformation> EmployeeInformation { get; set; }
        public virtual ICollection<EmployeeResignation> EmployeeResignation { get; set; }
    }
}
