﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PurchaseOrder
    {
        public PurchaseOrder()
        {
            PurchaseOrderLine = new HashSet<PurchaseOrderLine>();
        }

        public long PurchaseOrderId { get; set; }
        public long? TypeId { get; set; }
        public long? TypeOfPoId { get; set; }
        public DateTime? DateOfPo { get; set; }
        public long? FromCompanyId { get; set; }
        public long? VendorNameId { get; set; }
        public string VersionNo { get; set; }
        public string Ponumber { get; set; }
        public Guid? SessionId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? StatusCodeId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant FromCompany { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CompanyListing VendorName { get; set; }
        public virtual ICollection<PurchaseOrderLine> PurchaseOrderLine { get; set; }
    }
}
