﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PostSalesOrder
    {
        public long PostSalesOrderId { get; set; }
        public long? SoSalesOrderId { get; set; }
        public string ItemSerialNo { get; set; }
        public string SignOrderNo { get; set; }
        public string SalesOrderNo { get; set; }
        public DateTime? OrderDate { get; set; }
        public string OrderBy { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? StatusCodeId { get; set; }
    }
}
