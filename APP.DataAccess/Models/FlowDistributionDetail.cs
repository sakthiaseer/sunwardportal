﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FlowDistributionDetail
    {
        public FlowDistributionDetail()
        {
            FlowDistributionStep = new HashSet<FlowDistributionStep>();
        }

        public long FlowDistributionDetailId { get; set; }
        public long FlowDistributionId { get; set; }
        public long DynamicFlowDetailId { get; set; }
        public int? DistributionTypeId { get; set; }
        public string BlockNo { get; set; }
        public string LoopTo { get; set; }

        public virtual CodeMaster DistributionType { get; set; }
        public virtual DynamicFlowDetail DynamicFlowDetail { get; set; }
        public virtual FlowDistribution FlowDistribution { get; set; }
        public virtual ICollection<FlowDistributionStep> FlowDistributionStep { get; set; }
    }
}
