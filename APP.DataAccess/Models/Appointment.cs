﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Appointment
    {
        public long Id { get; set; }
        public int? AppointmentType { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Caption { get; set; }
        public int? Label { get; set; }
        public int? Status { get; set; }
        public bool? AllDay { get; set; }
        public string Recurrence { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
    }
}
