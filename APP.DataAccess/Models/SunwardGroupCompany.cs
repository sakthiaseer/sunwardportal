﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SunwardGroupCompany
    {
        public long SunwardGroupCompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCode { get; set; }
        public long? RegisteredCountryId { get; set; }
        public string CompanyRegistrationNo { get; set; }
        public DateTime? CompanyEstablishedDate { get; set; }
        public string Gstno { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
