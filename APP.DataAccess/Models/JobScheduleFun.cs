﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class JobScheduleFun
    {
        public JobScheduleFun()
        {
            JobSchedule = new HashSet<JobSchedule>();
        }

        public long JobScheduleFunId { get; set; }
        public long JobScheduleFunUniqueId { get; set; }
        public string ScheduleFunctionName { get; set; }
        public string DisplayName { get; set; }

        public virtual ICollection<JobSchedule> JobSchedule { get; set; }
    }
}
