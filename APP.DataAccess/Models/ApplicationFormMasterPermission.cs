﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApplicationFormMasterPermission
    {
        public ApplicationFormMasterPermission()
        {
            InverseParent = new HashSet<ApplicationFormMasterPermission>();
            ItemClassificationPermission = new HashSet<ItemClassificationPermission>();
        }

        public long ApplicationFormMasterPermissionId { get; set; }
        public long ApplicationCodeId { get; set; }
        public string ScreenId { get; set; }
        public int? ApplicationFormCodeId { get; set; }
        public long? ParentId { get; set; }

        public virtual CodeMaster ApplicationFormCode { get; set; }
        public virtual ApplicationFormMasterPermission Parent { get; set; }
        public virtual ICollection<ApplicationFormMasterPermission> InverseParent { get; set; }
        public virtual ICollection<ItemClassificationPermission> ItemClassificationPermission { get; set; }
    }
}
