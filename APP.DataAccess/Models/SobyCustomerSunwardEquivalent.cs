﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SobyCustomerSunwardEquivalent
    {
        public long SobyCustomerSunwardEquivalentId { get; set; }
        public long? SobyCustomersId { get; set; }
        public long? OrderPlacetoCompanyId { get; set; }
        public long? NavItemId { get; set; }
        public string FactorVsCustomerPacking { get; set; }
        public long? SocustomersItemCrossReferenceId { get; set; }
        public bool? IsThisDefault { get; set; }
        public int? StatusCodeId { get; set; }
        public string FactorVsPurchasePacking { get; set; }
        public string DefaultSupply { get; set; }
        public long? SupplyToId { get; set; }
        public Guid? SessionId { get; set; }

        public virtual GenericCodes NavItem { get; set; }
        public virtual Plant OrderPlacetoCompany { get; set; }
        public virtual SobyCustomers SobyCustomers { get; set; }
        public virtual SocustomersItemCrossReference SocustomersItemCrossReference { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual GenericCodeSupplyToMultiple SupplyTo { get; set; }
    }
}
