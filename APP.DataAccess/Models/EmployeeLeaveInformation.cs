﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmployeeLeaveInformation
    {
        public long EmployeeLeaveInformatonId { get; set; }
        public long? EmployeeId { get; set; }
        public DateTime? Date { get; set; }
        public int? NonPresenceId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster NonPresence { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
