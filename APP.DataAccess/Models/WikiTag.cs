﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WikiTag
    {
        public long WikiTagId { get; set; }
        public long TagId { get; set; }
        public long WikiPageId { get; set; }

        public virtual Tag Tag { get; set; }
        public virtual WikiPage WikiPage { get; set; }
    }
}
