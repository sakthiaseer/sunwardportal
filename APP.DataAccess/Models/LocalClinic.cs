﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class LocalClinic
    {
        public LocalClinic()
        {
            SalesSurveyByFieldForce = new HashSet<SalesSurveyByFieldForce>();
        }

        public long LocalClinicId { get; set; }
        public long? CountryId { get; set; }
        public string Company { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public long? StateId { get; set; }
        public long? CityId { get; set; }
        public string PostalCode { get; set; }
        public long? SpecialityId { get; set; }
        public long? SalesmanCodeId { get; set; }
        public int? CompanyStatusId { get; set; }
        public int? ToSwstatusId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster CompanyStatus { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster ToSwstatus { get; set; }
        public virtual ICollection<SalesSurveyByFieldForce> SalesSurveyByFieldForce { get; set; }
    }
}
