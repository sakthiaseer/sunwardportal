﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Fmglobal
    {
        public Fmglobal()
        {
            FmglobalAddess = new HashSet<FmglobalAddess>();
            FmglobalLine = new HashSet<FmglobalLine>();
            FmglobalMove = new HashSet<FmglobalMove>();
        }

        public long FmglobalId { get; set; }
        public long? CompanyId { get; set; }
        public DateTime? ExpectedShipmentDate { get; set; }
        public string Pono { get; set; }
        public long? LocationToId { get; set; }
        public long? LocationFromId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public long? FmglobalStausId { get; set; }
        public long? SoCustomerId { get; set; }
        public long? SoCustomerShipingAddressId { get; set; }
        public string ShippedBy { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationMasterDetail FmglobalStaus { get; set; }
        public virtual Ictmaster LocationFrom { get; set; }
        public virtual Ictmaster LocationTo { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<FmglobalAddess> FmglobalAddess { get; set; }
        public virtual ICollection<FmglobalLine> FmglobalLine { get; set; }
        public virtual ICollection<FmglobalMove> FmglobalMove { get; set; }
    }
}
