﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TaskTag
    {
        public long TaskTagId { get; set; }
        public long TaskMasterId { get; set; }
        public long? TagId { get; set; }

        public virtual TagMaster Tag { get; set; }
        public virtual TaskMaster TaskMaster { get; set; }
    }
}
