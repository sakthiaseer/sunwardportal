﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionTicket
    {
        public long ProductionTicketId { get; set; }
        public long? ComapnyId { get; set; }
        public long? BaseUnitId { get; set; }
        public long? Coaid { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual NavbaseUnit BaseUnit { get; set; }
        public virtual Documents Coa { get; set; }
        public virtual Plant Comapny { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
