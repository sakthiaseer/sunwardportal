﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DeviceGroupCatalog
    {
        public long DeviceGroupCatalogId { get; set; }
        public long? DeviceCatalogId { get; set; }
        public long? DeviceGroupId { get; set; }

        public virtual DeviceCatalogMaster DeviceCatalog { get; set; }
        public virtual DeviceGroupList DeviceGroup { get; set; }
    }
}
