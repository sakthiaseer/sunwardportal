﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionMaterial
    {
        public ProductionMaterial()
        {
            FinishProductExcipientLine = new HashSet<FinishProductExcipientLine>();
            InverseProcessUseSpec = new HashSet<ProductionMaterial>();
            ProductionFuntionOfMaterialItems = new HashSet<ProductionFuntionOfMaterialItems>();
            ProductionMaterialIngredientItems = new HashSet<ProductionMaterialIngredientItems>();
            ProductionMaterialLine = new HashSet<ProductionMaterialLine>();
        }

        public long ProductionMaterialId { get; set; }
        public string RawMaterialName { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? AddedByDate { get; set; }
        public DateTime? ModifiedByDate { get; set; }
        public long? GenericItemNameListingId { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public bool? IsSameAsMaster { get; set; }
        public string MaterialName { get; set; }
        public int? SpecNo { get; set; }
        public long? PurposeId { get; set; }
        public long? ProcessUseSpecId { get; set; }
        public long? RndpurposeId { get; set; }
        public string StudyLink { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual GenericItemNameListing GenericItemNameListing { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProductionMaterial ProcessUseSpec { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<FinishProductExcipientLine> FinishProductExcipientLine { get; set; }
        public virtual ICollection<ProductionMaterial> InverseProcessUseSpec { get; set; }
        public virtual ICollection<ProductionFuntionOfMaterialItems> ProductionFuntionOfMaterialItems { get; set; }
        public virtual ICollection<ProductionMaterialIngredientItems> ProductionMaterialIngredientItems { get; set; }
        public virtual ICollection<ProductionMaterialLine> ProductionMaterialLine { get; set; }
    }
}
