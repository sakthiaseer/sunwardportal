﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PurchaseItemSalesEntryLine
    {
        public PurchaseItemSalesEntryLine()
        {
            ContractDistributionSalesEntryLine = new HashSet<ContractDistributionSalesEntryLine>();
            ProjectedDeliveryBlanketOthers = new HashSet<ProjectedDeliveryBlanketOthers>();
            SalesOrderEntryNavigation = new HashSet<SalesOrderEntry>();
            SalesOrderProduct = new HashSet<SalesOrderProduct>();
        }

        public long PurchaseItemSalesEntryLineId { get; set; }
        public string QuotationNo { get; set; }
        public long? SalesOrderEntryId { get; set; }
        public long? SobyCustomersId { get; set; }
        public long? ItemId { get; set; }
        public int? OrderQty { get; set; }
        public long? OrderCurrencyId { get; set; }
        public decimal? SellingPrice { get; set; }
        public bool? IsFoc { get; set; }
        public string Foc { get; set; }
        public string SchdAgreementNo { get; set; }
        public string TenderAgencyNo { get; set; }
        public long? ShipToAddressId { get; set; }
        public long? Uomid { get; set; }
        public long? UomfirstId { get; set; }
        public long? UomsecondId { get; set; }
        public long? UomthirdId { get; set; }
        public bool? IsAllowExtension { get; set; }
        public int? ExtensionMonth { get; set; }
        public int? ExtensionQuantity { get; set; }
        public bool? IsLotDeliveries { get; set; }
        public DateTime? DeliveryDateOfOrder { get; set; }
        public decimal? OptionalQty { get; set; }
        public decimal? PricePerQty { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual SalesOrderEntry SalesOrderEntry { get; set; }
        public virtual SocustomersItemCrossReference SobyCustomers { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ContractDistributionSalesEntryLine> ContractDistributionSalesEntryLine { get; set; }
        public virtual ICollection<ProjectedDeliveryBlanketOthers> ProjectedDeliveryBlanketOthers { get; set; }
        public virtual ICollection<SalesOrderEntry> SalesOrderEntryNavigation { get; set; }
        public virtual ICollection<SalesOrderProduct> SalesOrderProduct { get; set; }
    }
}
