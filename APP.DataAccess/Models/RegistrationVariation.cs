﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class RegistrationVariation
    {
        public RegistrationVariation()
        {
            RecordVariationLine = new HashSet<RecordVariationLine>();
            RegistrationFormatInformation = new HashSet<RegistrationFormatInformation>();
            RegistrationVariationTypeNavigation = new HashSet<RegistrationVariationType>();
        }

        public long RegistrationVariationId { get; set; }
        public int? RegisterationCodeId { get; set; }
        public long? RegistrationCountryId { get; set; }
        public long? RegistrationClassificationId { get; set; }
        public long? RegistrationVariationCodeId { get; set; }
        public string VariationTitle { get; set; }
        public long? RegistrationVariationTypeId { get; set; }
        public long? RegistrationCategory { get; set; }
        public int? StatustCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster RegisterationCode { get; set; }
        public virtual ApplicationMasterDetail RegistrationCategoryNavigation { get; set; }
        public virtual ApplicationMasterDetail RegistrationClassification { get; set; }
        public virtual ApplicationMasterDetail RegistrationCountry { get; set; }
        public virtual ApplicationMasterDetail RegistrationVariationCode { get; set; }
        public virtual ApplicationMasterDetail RegistrationVariationType { get; set; }
        public virtual CodeMaster StatustCode { get; set; }
        public virtual ICollection<RecordVariationLine> RecordVariationLine { get; set; }
        public virtual ICollection<RegistrationFormatInformation> RegistrationFormatInformation { get; set; }
        public virtual ICollection<RegistrationVariationType> RegistrationVariationTypeNavigation { get; set; }
    }
}
