﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class GroupPlaningTicket
    {
        public long GroupPlanningId { get; set; }
        public int? CompanyId { get; set; }
        public string BatchName { get; set; }
        public string ProductGroupCode { get; set; }
        public DateTime? StartDate { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string Description1 { get; set; }
        public string BatchSize { get; set; }
        public decimal? Quantity { get; set; }
        public string Uom { get; set; }
        public int? NoOfTicket { get; set; }
        public decimal? TotalQuantity { get; set; }
    }
}
