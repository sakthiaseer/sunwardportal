﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FpproductPacksizeMultiple
    {
        public long FpproductPackSizeMultipleId { get; set; }
        public long? FpproductId { get; set; }
        public long? FinishProductGeneralInforLineId { get; set; }

        public virtual FinishProdcutGeneralInfoLine FinishProductGeneralInforLine { get; set; }
        public virtual Fpproduct Fpproduct { get; set; }
    }
}
