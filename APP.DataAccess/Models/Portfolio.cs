﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Portfolio
    {
        public Portfolio()
        {
            PortfolioLine = new HashSet<PortfolioLine>();
            PortfolioOnBehalf = new HashSet<PortfolioOnBehalf>();
        }

        public long PortfolioId { get; set; }
        public long? CompanyId { get; set; }
        public long? OnBehalfId { get; set; }
        public long? PortfolioTypeId { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationUser OnBehalf { get; set; }
        public virtual ApplicationMasterDetail PortfolioType { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<PortfolioLine> PortfolioLine { get; set; }
        public virtual ICollection<PortfolioOnBehalf> PortfolioOnBehalf { get; set; }
    }
}
