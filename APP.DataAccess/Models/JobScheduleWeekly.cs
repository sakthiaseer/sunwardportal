﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class JobScheduleWeekly
    {
        public long JobScheduleWeeklyId { get; set; }
        public long? JobScheduleId { get; set; }
        public int? WeeklyId { get; set; }
        public string CustomType { get; set; }

        public virtual JobSchedule JobSchedule { get; set; }
        public virtual CodeMaster Weekly { get; set; }
    }
}
