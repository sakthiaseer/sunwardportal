﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CommonMasterToolingClassification
    {
        public long CommonMasterToolingClassificationId { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public int? ToolInfoId { get; set; }
        public string ToolName { get; set; }
        public long? LocationId { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileReferenceNo { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ItemClassificationMaster ItemClassificationMaster { get; set; }
        public virtual Ictmaster Location { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CodeMaster ToolInfo { get; set; }
    }
}
