﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DocumentProfile
    {
        public long DocumentProfileId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? AutoGenerateReferenceNo { get; set; }
        public int? RunningReferenceNo { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
