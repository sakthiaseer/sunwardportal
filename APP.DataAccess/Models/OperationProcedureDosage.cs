﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class OperationProcedureDosage
    {
        public long OperationProcedureDosageId { get; set; }
        public long? OperationProcedureId { get; set; }
        public long? DosageFormId { get; set; }

        public virtual OperationProcedure OperationProcedure { get; set; }
    }
}
