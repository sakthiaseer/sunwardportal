﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ForumNotifications
    {
        public long Id { get; set; }
        public long? ConversationId { get; set; }
        public long? TopicId { get; set; }
        public long? UserId { get; set; }
        public bool? IsRead { get; set; }
        public int? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
    }
}
