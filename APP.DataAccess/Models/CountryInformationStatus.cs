﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CountryInformationStatus
    {
        public long CountryInformationId { get; set; }
        public long? FinishProductGeneralInfoId { get; set; }
        public int? StatusCodeId { get; set; }
        public string Remarks { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual FinishProductGeneralInfo FinishProductGeneralInfo { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
