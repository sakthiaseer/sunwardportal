﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class StartOfDayProdTaskMultiple
    {
        public long StartOfDayProdTaskMultipleId { get; set; }
        public long? ProdTaskPerformId { get; set; }
        public long? StartOfDayId { get; set; }
        public long? ProductionEntryId { get; set; }

        public virtual ProductionEntry ProductionEntry { get; set; }
        public virtual StartOfDay StartOfDay { get; set; }
    }
}
