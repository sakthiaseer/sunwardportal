﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavSalesOrderLine
    {
        public NavSalesOrderLine()
        {
            NonDeliverSo = new HashSet<NonDeliverSo>();
        }

        public long NavSalesOrderLineId { get; set; }
        public string DocumentType { get; set; }
        public string DocumentNo { get; set; }
        public int? OrderLineNo { get; set; }
        public string SelltoCustomerNo { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string Description1 { get; set; }
        public decimal? OutstandingQuantity { get; set; }
        public DateTime? PromisedDeliveryDate { get; set; }
        public DateTime? ShipmentDate { get; set; }
        public string UnitofMeasureCode { get; set; }
        public DateTime? LastSyncDate { get; set; }
        public long? LastSyncUserId { get; set; }
        public bool? IsNonDeliverSo { get; set; }
        public string Sostatus { get; set; }
        public long? CompanyId { get; set; }
        public string VersionNo { get; set; }
        public decimal? TotalOrderQuantity { get; set; }
        public decimal? DeliverQuantity { get; set; }

        public virtual Plant Company { get; set; }
        public virtual ICollection<NonDeliverSo> NonDeliverSo { get; set; }
    }
}
