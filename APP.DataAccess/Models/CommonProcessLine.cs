﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CommonProcessLine
    {
        public CommonProcessLine()
        {
            CommonProcessMachineGroupingMultiple = new HashSet<CommonProcessMachineGroupingMultiple>();
            Icbmpline = new HashSet<Icbmpline>();
            IcmasterOperation = new HashSet<IcmasterOperation>();
        }

        public long CommonProcessLineId { get; set; }
        public long? CommonProcessId { get; set; }
        public long? ManufacturingStepsId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? NavisionNoSeriesId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CommonProcess CommonProcess { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<CommonProcessMachineGroupingMultiple> CommonProcessMachineGroupingMultiple { get; set; }
        public virtual ICollection<Icbmpline> Icbmpline { get; set; }
        public virtual ICollection<IcmasterOperation> IcmasterOperation { get; set; }
    }
}
