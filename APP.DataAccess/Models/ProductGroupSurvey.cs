﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductGroupSurvey
    {
        public ProductGroupSurvey()
        {
            ProductGroupSurveyLine = new HashSet<ProductGroupSurveyLine>();
            SalesSurveyByFieldForceLine = new HashSet<SalesSurveyByFieldForceLine>();
        }

        public long ProductGroupSurveyId { get; set; }
        public long? CompanyId { get; set; }
        public DateTime? SurveyDate { get; set; }
        public long? PharmacologicalCategoryId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ProductGroupSurveyLine> ProductGroupSurveyLine { get; set; }
        public virtual ICollection<SalesSurveyByFieldForceLine> SalesSurveyByFieldForceLine { get; set; }
    }
}
