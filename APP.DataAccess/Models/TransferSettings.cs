﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TransferSettings
    {
        public long TransferConfigId { get; set; }
        public long? FromLocationId { get; set; }
        public long? ToLocationId { get; set; }
        public bool? IsTransfer { get; set; }
        public bool? IsReclass { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Ictmaster FromLocation { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual Ictmaster ToLocation { get; set; }
    }
}
