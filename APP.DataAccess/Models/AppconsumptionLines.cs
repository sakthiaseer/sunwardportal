﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AppconsumptionLines
    {
        public long ConsumptionLineId { get; set; }
        public long? ConsumptionEntryId { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public int? ProdLineNo { get; set; }
        public int? ProdComLine { get; set; }
        public string LotNo { get; set; }
        public string QcrefNo { get; set; }
        public string BatchNo { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public decimal? Quantity { get; set; }
        public string Uom { get; set; }
        public decimal? BaseQuantity { get; set; }
        public string BaseUom { get; set; }
        public bool? PostedtoNav { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? AddedUserId { get; set; }
        public bool? IsFullConsume { get; set; }
        public string ProductionOrderNo { get; set; }
        public string SubLotNo { get; set; }
        public int? DrumNo { get; set; }

        public virtual ApplicationUser AddedUser { get; set; }
        public virtual AppconsumptionEntry ConsumptionEntry { get; set; }
    }
}
