﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class BlanketOrderAttachment
    {
        public long BlanketOrderAttachmentId { get; set; }
        public long? BlanketOrderId { get; set; }
        public string DocumentType { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] FileData { get; set; }
        public long? FileSize { get; set; }
        public DateTime? UploadDate { get; set; }
        public Guid? SessionId { get; set; }
        public long? FileProfileTypeId { get; set; }
        public string ProfileNo { get; set; }
        public string ScreenId { get; set; }

        public virtual BlanketOrder BlanketOrder { get; set; }
        public virtual FileProfileType FileProfileType { get; set; }
    }
}
