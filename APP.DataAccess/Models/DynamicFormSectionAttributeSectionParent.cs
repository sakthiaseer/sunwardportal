﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormSectionAttributeSectionParent
    {
        public DynamicFormSectionAttributeSectionParent()
        {
            DynamicFormSectionAttributeSection = new HashSet<DynamicFormSectionAttributeSection>();
        }

        public long DynamicFormSectionAttributeSectionParentId { get; set; }
        public long? DynamicFormSectionAttributeId { get; set; }
        public int? SequenceNo { get; set; }

        public virtual DynamicFormSectionAttribute DynamicFormSectionAttribute { get; set; }
        public virtual ICollection<DynamicFormSectionAttributeSection> DynamicFormSectionAttributeSection { get; set; }
    }
}
