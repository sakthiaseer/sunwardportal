﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProcessMachineTime
    {
        public ProcessMachineTime()
        {
            ProcessMachineTimeProductionLine = new HashSet<ProcessMachineTimeProductionLine>();
        }

        public long ProcessMachineTimeId { get; set; }
        public long? CompanyId { get; set; }
        public long? ProductionAreaId { get; set; }
        public bool? IsRunSteroidCompaign { get; set; }
        public long? NavMethodCodeId { get; set; }
        public long? NavBatchSizeId { get; set; }
        public long? NavProductCodeId { get; set; }
        public bool? IsInterCompanyProduction { get; set; }
        public long? NavItemCategoryId { get; set; }
        public long? ItemId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationMasterDetail NavBatchSize { get; set; }
        public virtual NavMethodCode NavMethodCode { get; set; }
        public virtual NavproductCode NavProductCode { get; set; }
        public virtual ApplicationMasterDetail ProductionArea { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ProcessMachineTimeProductionLine> ProcessMachineTimeProductionLine { get; set; }
    }
}
