﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApplicationPermission
    {
        public ApplicationPermission()
        {
            ApplicationRolePermission = new HashSet<ApplicationRolePermission>();
            ApplicationWikiLineFunctionLinkNavigation = new HashSet<ApplicationWikiLine>();
            ApplicationWikiLinePageLinkNavigation = new HashSet<ApplicationWikiLine>();
            DraftApplicationWikiLineFunctionLinkNavigation = new HashSet<DraftApplicationWikiLine>();
            DraftApplicationWikiLinePageLinkNavigation = new HashSet<DraftApplicationWikiLine>();
            ProductActivityCaseResponsFunctionLinkNavigation = new HashSet<ProductActivityCaseRespons>();
            ProductActivityCaseResponsPageLinkNavigation = new HashSet<ProductActivityCaseRespons>();
            ProductionActivityMasterResponsFunctionLinkNavigation = new HashSet<ProductionActivityMasterRespons>();
            ProductionActivityMasterResponsPageLinkNavigation = new HashSet<ProductionActivityMasterRespons>();
            TemplateTestCaseCheckListResponseFunctionLinkNavigation = new HashSet<TemplateTestCaseCheckListResponse>();
            TemplateTestCaseCheckListResponsePageLinkNavigation = new HashSet<TemplateTestCaseCheckListResponse>();
            WorkOrder = new HashSet<WorkOrder>();
        }

        public long PermissionId { get; set; }
        public string PermissionName { get; set; }
        public string PermissionCode { get; set; }
        public long? ParentId { get; set; }
        public byte? PermissionLevel { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string Icon { get; set; }
        public string MenuId { get; set; }
        public string PermissionUrl { get; set; }
        public string PermissionGroup { get; set; }
        public string PermissionOrder { get; set; }
        public bool? IsDisplay { get; set; }
        public long AppplicationPermissionId { get; set; }
        public bool? IsHeader { get; set; }
        public bool? IsNewPortal { get; set; }
        public string Component { get; set; }
        public string Name { get; set; }
        public bool? IsMobile { get; set; }
        public string ScreenId { get; set; }
        public bool? IsProductionApp { get; set; }
        public bool? IsCmsApp { get; set; }
        public bool? IsPermissionUrl { get; set; }
        public Guid? UniqueSessionId { get; set; }

        public virtual ICollection<ApplicationRolePermission> ApplicationRolePermission { get; set; }
        public virtual ICollection<ApplicationWikiLine> ApplicationWikiLineFunctionLinkNavigation { get; set; }
        public virtual ICollection<ApplicationWikiLine> ApplicationWikiLinePageLinkNavigation { get; set; }
        public virtual ICollection<DraftApplicationWikiLine> DraftApplicationWikiLineFunctionLinkNavigation { get; set; }
        public virtual ICollection<DraftApplicationWikiLine> DraftApplicationWikiLinePageLinkNavigation { get; set; }
        public virtual ICollection<ProductActivityCaseRespons> ProductActivityCaseResponsFunctionLinkNavigation { get; set; }
        public virtual ICollection<ProductActivityCaseRespons> ProductActivityCaseResponsPageLinkNavigation { get; set; }
        public virtual ICollection<ProductionActivityMasterRespons> ProductionActivityMasterResponsFunctionLinkNavigation { get; set; }
        public virtual ICollection<ProductionActivityMasterRespons> ProductionActivityMasterResponsPageLinkNavigation { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponse> TemplateTestCaseCheckListResponseFunctionLinkNavigation { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponse> TemplateTestCaseCheckListResponsePageLinkNavigation { get; set; }
        public virtual ICollection<WorkOrder> WorkOrder { get; set; }
    }
}
