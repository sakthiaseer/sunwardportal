﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionActivityMasterResponsWeekly
    {
        public long ProductionActivityMasterResponsWeeklyId { get; set; }
        public long? ProductionActivityMasterResponsId { get; set; }
        public int? WeeklyId { get; set; }
        public string CustomType { get; set; }

        public virtual ProductionActivityMasterRespons ProductionActivityMasterRespons { get; set; }
        public virtual CodeMaster Weekly { get; set; }
    }
}
