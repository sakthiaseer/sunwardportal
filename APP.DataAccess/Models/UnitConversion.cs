﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class UnitConversion
    {
        public long UnitConversionId { get; set; }
        public decimal? UnitNumber { get; set; }
        public int? Units { get; set; }
        public decimal? IsEqualValue { get; set; }
        public int? Units1 { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
