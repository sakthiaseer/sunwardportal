﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NpraformulationActiveIngredient
    {
        public long NpraformulationActiveIngredientId { get; set; }
        public long? NpraformulationId { get; set; }
        public long? ActiveIngredientId { get; set; }
        public long? SaltFormId { get; set; }
        public decimal? Strength { get; set; }
        public long? UnitsId { get; set; }
        public long? StrengthSaltFreeId { get; set; }
        public long? SourceId { get; set; }
        public long? FormOfSubstanceId { get; set; }
        public string Remarks { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationMasterDetail ActiveIngredient { get; set; }
        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterDetail FormOfSubstance { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Npraformulation Npraformulation { get; set; }
        public virtual ApplicationMasterDetail SaltForm { get; set; }
        public virtual ApplicationMasterDetail Source { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationMasterDetail StrengthSaltFree { get; set; }
        public virtual ApplicationMasterDetail Units { get; set; }
    }
}
