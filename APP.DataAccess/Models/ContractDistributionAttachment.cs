﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ContractDistributionAttachment
    {
        public long ContractDistributionAttachmentId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] FileData { get; set; }
        public long? FileSize { get; set; }
        public DateTime? UploadDate { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? FileProfileTypeId { get; set; }
        public string ProfileNo { get; set; }
        public string ScreenId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual FileProfileType FileProfileType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
