﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewProductionActivityFilterReport
    {
        public long ProductionActivityAppLineId { get; set; }
        public string ProdOrderNo { get; set; }
        public string ProfileNo { get; set; }
        public string TicketNo { get; set; }
        public string LocationName { get; set; }
        public DateTime? AddedDate { get; set; }
        public string Process { get; set; }
        public string Category { get; set; }
        public string ActionList { get; set; }
        public string Comment { get; set; }
        public string Result { get; set; }
        public string ActivityMaster { get; set; }
        public string ActivityStatus { get; set; }
        public Guid? SessionId { get; set; }
    }
}
