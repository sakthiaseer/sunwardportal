﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NotifyLongNotes
    {
        public long NotifyLongNotesId { get; set; }
        public long? NotifyDocumentId { get; set; }
        public string LongNotes { get; set; }
        public long? ParentId { get; set; }

        public virtual NotifyDocument NotifyDocument { get; set; }
    }
}
