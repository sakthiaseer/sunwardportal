﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class IctcontactDetails
    {
        public long ContactDetailsId { get; set; }
        public string Department { get; set; }
        public string Salutation { get; set; }
        public string ContactName { get; set; }
        public int? Phone { get; set; }
        public long? VendorsListId { get; set; }
        public long? SourceListId { get; set; }
        public int? ContactTypeId { get; set; }
        public string Email { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster ContactType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual SourceList SourceList { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual VendorsList VendorsList { get; set; }
    }
}
