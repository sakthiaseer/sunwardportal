﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FolderDiscussion
    {
        public FolderDiscussion()
        {
            FolderDiscussionAttachment = new HashSet<FolderDiscussionAttachment>();
            FolderDiscussionUser = new HashSet<FolderDiscussionUser>();
            InverseParentDiscussionNotes = new HashSet<FolderDiscussion>();
        }

        public long DiscussionNotesId { get; set; }
        public long? FolderId { get; set; }
        public long? ParentDiscussionNotesId { get; set; }
        public string DiscussionNotes { get; set; }
        public long? DiscussedBy { get; set; }
        public DateTime? DiscussionDate { get; set; }
        public bool? IsRead { get; set; }
        public bool? IsEdited { get; set; }
        public long? EditedBy { get; set; }
        public DateTime? EditedDate { get; set; }
        public bool? IsDocument { get; set; }
        public long? DocumentId { get; set; }
        public int? StatusCodeId { get; set; }
        public string DiscussionSubject { get; set; }

        public virtual ApplicationUser DiscussedByNavigation { get; set; }
        public virtual ApplicationUser EditedByNavigation { get; set; }
        public virtual Folders Folder { get; set; }
        public virtual FolderDiscussion ParentDiscussionNotes { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<FolderDiscussionAttachment> FolderDiscussionAttachment { get; set; }
        public virtual ICollection<FolderDiscussionUser> FolderDiscussionUser { get; set; }
        public virtual ICollection<FolderDiscussion> InverseParentDiscussionNotes { get; set; }
    }
}
