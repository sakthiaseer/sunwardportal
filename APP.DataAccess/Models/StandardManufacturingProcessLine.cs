﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class StandardManufacturingProcessLine
    {
        public long StandardManufacturingProcessLineId { get; set; }
        public long? StandardManufacturingProcessId { get; set; }
        public long? MethodTemplateRoutineLineId { get; set; }
        public string No { get; set; }
        public long? ProcesssStepId { get; set; }
        public long? OperationId { get; set; }
        public long? DetermineFactorId { get; set; }
        public long? MachineGroupingId { get; set; }
        public long? MachineNameId { get; set; }
        public string Location { get; set; }
        public string TimeRequire { get; set; }
        public string TimeGapMin { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CommonFieldsProductionMachine MachineGrouping { get; set; }
        public virtual CommonFieldsProductionMachine MachineName { get; set; }
        public virtual MethodTemplateRoutineLine MethodTemplateRoutineLine { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual StandardManufacturingProcess StandardManufacturingProcess { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
