﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WorkFlowUserGroup
    {
        public WorkFlowUserGroup()
        {
            WorkFlowApproverSetup = new HashSet<WorkFlowApproverSetup>();
            WorkFlowUserGroupUsers = new HashSet<WorkFlowUserGroupUsers>();
        }

        public long WorkFlowUserGroupId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<WorkFlowApproverSetup> WorkFlowApproverSetup { get; set; }
        public virtual ICollection<WorkFlowUserGroupUsers> WorkFlowUserGroupUsers { get; set; }
    }
}
