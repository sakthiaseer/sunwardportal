﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductActivityCaseLine
    {
        public ProductActivityCaseLine()
        {
            ProductionActivityAppLine = new HashSet<ProductionActivityAppLine>();
            ProductionActivityPlanningAppLine = new HashSet<ProductionActivityPlanningAppLine>();
            ProductionActivityRoutineAppLine = new HashSet<ProductionActivityRoutineAppLine>();
        }

        public long ProductActivityCaseLineId { get; set; }
        public long? ProductActivityCaseId { get; set; }
        public string NameOfTemplate { get; set; }
        public string Subject { get; set; }
        public string Link { get; set; }
        public string LocationName { get; set; }
        public string Naming { get; set; }
        public bool? IsAutoNumbering { get; set; }
        public long? TemplateProfileId { get; set; }
        public string DocumentNo { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ProdActivityCategoryId { get; set; }
        public long? ProdActivityActionId { get; set; }
        public Guid? SessionId { get; set; }
        public long? LocationToSaveId { get; set; }
        public long? ProdActivityCategoryChildId { get; set; }
        public long? ProdActivityActionChildId { get; set; }
        public string TopicId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual FileProfileType LocationToSave { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationMasterDetail ProdActivityAction { get; set; }
        public virtual ApplicationMasterChild ProdActivityActionChild { get; set; }
        public virtual ApplicationMasterDetail ProdActivityCategory { get; set; }
        public virtual ApplicationMasterChild ProdActivityCategoryChild { get; set; }
        public virtual ProductActivityCase ProductActivityCase { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual DocumentProfileNoSeries TemplateProfile { get; set; }
        public virtual ICollection<ProductionActivityAppLine> ProductionActivityAppLine { get; set; }
        public virtual ICollection<ProductionActivityPlanningAppLine> ProductionActivityPlanningAppLine { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLine> ProductionActivityRoutineAppLine { get; set; }
    }
}
