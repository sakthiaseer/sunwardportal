﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FlowDistribution
    {
        public FlowDistribution()
        {
            FlowDistributionDelivery = new HashSet<FlowDistributionDelivery>();
            FlowDistributionDetail = new HashSet<FlowDistributionDetail>();
            FlowDistributionStep = new HashSet<FlowDistributionStep>();
            WorkFlowInteraction = new HashSet<WorkFlowInteraction>();
        }

        public long FlowDistributionId { get; set; }
        public long? DynamicFlowId { get; set; }
        public long? WikiPageId { get; set; }
        public long? Picsection { get; set; }
        public long? Piasection { get; set; }
        public string Recurrence { get; set; }
        public DateTime? DueDate { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual DynamicFlow DynamicFlow { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationUser PiasectionNavigation { get; set; }
        public virtual ApplicationUser PicsectionNavigation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual WikiPage WikiPage { get; set; }
        public virtual ICollection<FlowDistributionDelivery> FlowDistributionDelivery { get; set; }
        public virtual ICollection<FlowDistributionDetail> FlowDistributionDetail { get; set; }
        public virtual ICollection<FlowDistributionStep> FlowDistributionStep { get; set; }
        public virtual ICollection<WorkFlowInteraction> WorkFlowInteraction { get; set; }
    }
}
