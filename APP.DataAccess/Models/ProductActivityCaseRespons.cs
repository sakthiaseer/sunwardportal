﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductActivityCaseRespons
    {
        public ProductActivityCaseRespons()
        {
            ProductActivityCaseResponsDuty = new HashSet<ProductActivityCaseResponsDuty>();
            ProductActivityCaseResponsRecurrence = new HashSet<ProductActivityCaseResponsRecurrence>();
            ProductActivityCaseResponsWeekly = new HashSet<ProductActivityCaseResponsWeekly>();
        }

        public long ProductActivityCaseResponsId { get; set; }
        public long? ProductActivityCaseId { get; set; }
        public long? DutyId { get; set; }
        public string Responsibility { get; set; }
        public long? PageLink { get; set; }
        public long? FunctionLink { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? NotificationAdvice { get; set; }
        public int? NotificationAdviceTypeId { get; set; }
        public int? RepeatId { get; set; }
        public int? CustomId { get; set; }
        public DateTime? DueDate { get; set; }
        public int? Monthly { get; set; }
        public int? Yearly { get; set; }
        public string EventDescription { get; set; }
        public bool? DaysOfWeek { get; set; }
        public int? NotificationStatusId { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public long? NotifyTo { get; set; }
        public string ScreenId { get; set; }
        public DateTime? NotifyEndDate { get; set; }
        public bool? IsAllowDocAccess { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster Custom { get; set; }
        public virtual ApplicationMasterDetail Duty { get; set; }
        public virtual ApplicationPermission FunctionLinkNavigation { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster NotificationAdviceType { get; set; }
        public virtual ApplicationPermission PageLinkNavigation { get; set; }
        public virtual ProductActivityCase ProductActivityCase { get; set; }
        public virtual CodeMaster Repeat { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ProductActivityCaseResponsDuty> ProductActivityCaseResponsDuty { get; set; }
        public virtual ICollection<ProductActivityCaseResponsRecurrence> ProductActivityCaseResponsRecurrence { get; set; }
        public virtual ICollection<ProductActivityCaseResponsWeekly> ProductActivityCaseResponsWeekly { get; set; }
    }
}
