﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ForumTypes
    {
        public ForumTypes()
        {
            ForumCategorys = new HashSet<ForumCategorys>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int StatusCodeId { get; set; }
        public int AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public int? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ICollection<ForumCategorys> ForumCategorys { get; set; }
    }
}
