﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewSalesOrderMasterPricing
    {
        public long SalesOrderMasterPricingId { get; set; }
        public long? CompanyId { get; set; }
        public long? SalesPricingForId { get; set; }
        public DateTime? PriceValidaityFrom { get; set; }
        public DateTime? PriceValidaityTo { get; set; }
        public long? ReasonForChangeId { get; set; }
        public string MasterType { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string StatusCode { get; set; }
        public string AddedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string PlantCode { get; set; }
        public string CompanyName { get; set; }
        public string NavCompanyName { get; set; }
        public string NavCompany { get; set; }
        public string ReasonForChangeName { get; set; }
        public string SalesPricingForName { get; set; }
    }
}
