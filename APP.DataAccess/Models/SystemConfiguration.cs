﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SystemConfiguration
    {
        public long SystemConfigId { get; set; }
        public string CodeName { get; set; }
        public string CodeValue { get; set; }
    }
}
