﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ManpowerInformation
    {
        public ManpowerInformation()
        {
            ManpowerInformationLine = new HashSet<ManpowerInformationLine>();
        }

        public long ManpowerInformationId { get; set; }
        public long? StandardManufacturingProcessId { get; set; }
        public string ProcessFromNo { get; set; }
        public string ProcessToNo { get; set; }
        public long? ManpowerNoId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual StandardManufacturingProcess StandardManufacturingProcess { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ManpowerInformationLine> ManpowerInformationLine { get; set; }
    }
}
