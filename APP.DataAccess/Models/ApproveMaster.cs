﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApproveMaster
    {
        public ApproveMaster()
        {
            ApprovalDetails = new HashSet<ApprovalDetails>();
        }

        public long ApproveMasterId { get; set; }
        public long? UserId { get; set; }
        public int? ApproveLevelId { get; set; }
        public Guid? SessionId { get; set; }
        public int? ApproveTypeId { get; set; }
        public string ScreenId { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster ApproveLevel { get; set; }
        public virtual CodeMaster ApproveType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual Employee User { get; set; }
        public virtual ICollection<ApprovalDetails> ApprovalDetails { get; set; }
    }
}
