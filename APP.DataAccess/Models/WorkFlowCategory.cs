﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WorkFlowCategory
    {
        public WorkFlowCategory()
        {
            WorkFlow = new HashSet<WorkFlow>();
        }

        public long WorkFlowCategoryId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<WorkFlow> WorkFlow { get; set; }
    }
}
