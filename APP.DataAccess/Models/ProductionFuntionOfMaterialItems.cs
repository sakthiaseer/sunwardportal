﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionFuntionOfMaterialItems
    {
        public long ProductionFunctionOfMaterialId { get; set; }
        public long? FunctionOfMaterialId { get; set; }
        public long? ProdutionMaterialId { get; set; }

        public virtual ProductionMaterial ProdutionMaterial { get; set; }
    }
}
