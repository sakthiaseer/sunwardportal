﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewAcbyDistributor
    {
        public long AcentryLineId { get; set; }
        public string AcfromDate { get; set; }
        public DateTime? FromDate { get; set; }
        public string ActoDate { get; set; }
        public DateTime? ToDate { get; set; }
        public long? CustomerId { get; set; }
        public long? ItemId { get; set; }
        public decimal? Acqty { get; set; }
        public string No { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public long MethodCodeId { get; set; }
        public string MethodName { get; set; }
        public string MethodDescription { get; set; }
        public decimal? SellingPrice { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal? SaleAmount { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
