﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewGetWorkOrderReport
    {
        public long? NavisionModuleId { get; set; }
        public int? AssignmentId { get; set; }
        public long? PermissionId { get; set; }
        public long? AssignTo { get; set; }
        public long WorkOrderLineId { get; set; }
        public long? WorkOrderId { get; set; }
        public string Subject { get; set; }
        public int? PriorityId { get; set; }
        public long? RequirementId { get; set; }
        public int? CrtstatusId { get; set; }
        public int? SunwardStatusId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public long? ModifiedByUserId { get; set; }
        public long? UserGroupId { get; set; }
        public DateTime? ExpectedReleaseDate { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public string ResultLink { get; set; }
        public string TaskLink { get; set; }
        public DateTime? StatusUpdateDate { get; set; }
        public string ProfileNo { get; set; }
        public string Description { get; set; }
        public string RequirementName { get; set; }
        public string AddedByUser { get; set; }
        public string ModifiedByUser { get; set; }
        public string StatusCode { get; set; }
        public string AssignmentName { get; set; }
        public string NavisionModuleName { get; set; }
        public string PermissionName { get; set; }
        public string PriorityName { get; set; }
        public string SunwardStatus { get; set; }
        public string Crtstatus { get; set; }
    }
}
