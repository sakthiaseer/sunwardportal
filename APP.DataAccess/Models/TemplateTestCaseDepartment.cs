﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TemplateTestCaseDepartment
    {
        public long TemplateTestCaseDepartmentId { get; set; }
        public long? TemplateTestCaseId { get; set; }
        public long? DepartmentId { get; set; }

        public virtual Department Department { get; set; }
        public virtual TemplateTestCase TemplateTestCase { get; set; }
    }
}
