﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ReleaseVersion
    {
        public ReleaseVersion()
        {
            ReleaseDetails = new HashSet<ReleaseDetails>();
        }

        public long VersionId { get; set; }
        public string VersionNo { get; set; }
        public string Description { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public int? ReleaseType { get; set; }

        public virtual ICollection<ReleaseDetails> ReleaseDetails { get; set; }
    }
}
