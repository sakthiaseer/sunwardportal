﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApplicationRolePermission
    {
        public long RolePermissionId { get; set; }
        public long RoleId { get; set; }
        public long PermissionId { get; set; }

        public virtual ApplicationPermission Permission { get; set; }
        public virtual ApplicationRole Role { get; set; }
    }
}
