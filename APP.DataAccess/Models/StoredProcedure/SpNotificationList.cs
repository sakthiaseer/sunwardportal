﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.DataAccess.Models
{
    public class SpNotificationList
    {

        public int? RequestPerson { get; set; }
        public int? SendNotifyDoc { get; set; }
        public int? ReceiveNotifyDoc { get; set; }
        public int? RequestAC { get; set; }
        public int? Template { get; set; }
        public int? WorkUnread { get; set; }
        public int? AssignedWorkOrderUnread { get; set; }
        public int? CCUserNotify { get; set; }
        public int? SharedDocument { get; set; }
        public int? CalendarDocument { get; set; }
        public int? CalendarPrint { get; set; }
        public int? ItemCrossReferenceReport { get; set; }
        public int? TotalCount { get; set; }
        public List<DashboardNotificationList> DashboardNotificationLists { get; set; } = new List<DashboardNotificationList>();
    }
    public class DashboardNotificationList
    {
        public string Title { get; set; }
        public int? Count { get; set; }
       
    }
}
