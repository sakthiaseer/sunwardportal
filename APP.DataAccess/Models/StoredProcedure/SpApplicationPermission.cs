﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.DataAccess.Models
{
    public class SpApplicationPermission
    {
        public long PermissionID { get; set; }
        public string PermissionName { get; set; }
        public long? ParentID { get; set; }
        public string PermissionGroup { get; set; }
        public string Icon { get; set; }
        public int IsSelected { get; set; }
        public bool? IsNewPortal { get; set; }
        public bool? IsMobile { get; set; }
    }

    public class SpApplicationRolePermission
    {
    }

    public class view_INPProdPlanning
    {
        public long ItemId { get; set; }
        public string Description { get; set; }
        public string BatchNos { get; set; }
        public string SafetyLeadTime { get; set; }
        public string BaseUnitofMeasure { get; set; }
        public decimal UnitCost { get; set; }
        public string DistName { get; set; }
        public string ItemGroup { get; set; }
        public string Steriod { get; set; }
        public string ShelfLife { get; set; }
        public string Quota { get; set; }
        public string Status { get; set; }
        public string ACItemDesc { get; set; }
        public int PackSize { get; set; }
        public string PackUOM { get; set; }
        public decimal NAVQty { get; set; }
        public decimal ACQty { get; set; }
        public DateTime ACMonth { get; set; }
        public decimal BatchQty { get; set; }
        public string ProdTime { get; set; }
    }
}
