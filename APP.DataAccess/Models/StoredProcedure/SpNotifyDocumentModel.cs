﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APP.DataAccess.Models
{
    public class SpNotifyDocumentModel
    {
        public long NotifyDocumentID { get; set; }
        public long? UserId { get; set; }
        public string Remarks { get; set; }
        public long? DocumentID { get; set; }
        public int StatusCodeID { get; set; }
        public long? AddedByUserID { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserID { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionID { get; set; }
        public bool? IsUrgent { get; set; }
        public bool? IsQuote { get; set; }
        public long? QuoteNotifyID { get; set; }
        public bool? IsSenderClose { get; set; } = false;
        public DateTime? DueDate { get; set; }
        public long? NotifyDocumentUserGroupID { get; set; }
        public long? GUserId { get; set; }
        public long? GUserGroupId { get; set; }
        public bool? GIsClosed { get; set; }
        public bool? GIsCcuser { get; set; }
        public bool? GIsRead { get; set; }
        public long? NotifyUserGroupUserID { get; set; }
        public long? UUserId { get; set; }
        public long? UUserGroupId { get; set; }
        public bool? UIsClosed { get; set; }
        public bool? UIsCcuser { get; set; }
        public bool? UIsRead { get; set; }
    }
}
