﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class IpirAppIssueDep
    {
        public long IpirAppIssueDepId { get; set; }
        public long? IpirAppId { get; set; }
        public long? DepartmentId { get; set; }
        public long? ActivityInfoIssueId { get; set; }
        public string Type { get; set; }

        public virtual ApplicationMasterDetail ActivityInfoIssue { get; set; }
        public virtual Department Department { get; set; }
        public virtual IpirApp IpirApp { get; set; }
    }
}
