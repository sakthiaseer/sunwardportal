﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductActivityCaseCompany
    {
        public long ProductActivityCaseCompanyId { get; set; }
        public long? CompanyId { get; set; }
        public DateTime? ImplementationDate { get; set; }
        public int? StatusCodeId { get; set; }
        public long? ProductActivityCaseId { get; set; }
        public long? VersionCodeStatusId { get; set; }
        public string VersionNo { get; set; }

        public virtual Plant Company { get; set; }
        public virtual ProductActivityCase ProductActivityCase { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationMasterDetail VersionCodeStatus { get; set; }
    }
}
