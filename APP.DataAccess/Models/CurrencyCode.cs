﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CurrencyCode
    {
        public CurrencyCode()
        {
            QuotationHeader = new HashSet<QuotationHeader>();
        }

        public long CurrencyCodeId { get; set; }
        public string Code { get; set; }
        public string Descirption { get; set; }

        public virtual ICollection<QuotationHeader> QuotationHeader { get; set; }
    }
}
