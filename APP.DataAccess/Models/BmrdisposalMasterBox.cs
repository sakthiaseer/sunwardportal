﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class BmrdisposalMasterBox
    {
        public long BmrdisposalMasterBoxId { get; set; }
        public long? CompanyId { get; set; }
        public long? TypeOfDisposalBoxId { get; set; }
        public string BoxNo { get; set; }
        public DateTime? DisposalDate { get; set; }
        public long? EstimateNumberOfBmr { get; set; }
        public long? LocationId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual Ictmaster Location { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
