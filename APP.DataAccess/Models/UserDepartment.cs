﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class UserDepartment
    {
        public long UserDepartmentId { get; set; }
        public long? UserId { get; set; }
        public long? DepartmentId { get; set; }
        public bool? IsDefaultDepartment { get; set; }

        public virtual Department Department { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
