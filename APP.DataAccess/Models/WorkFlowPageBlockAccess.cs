﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WorkFlowPageBlockAccess
    {
        public long WorkFlowPageBlockAccessId { get; set; }
        public long? WorkFlowPageBlockId { get; set; }
        public int? PermissionId { get; set; }
        public long? UserGroupId { get; set; }

        public virtual CodeMaster Permission { get; set; }
        public virtual UserGroup UserGroup { get; set; }
        public virtual WorkFlowPageBlock WorkFlowPageBlock { get; set; }
    }
}
