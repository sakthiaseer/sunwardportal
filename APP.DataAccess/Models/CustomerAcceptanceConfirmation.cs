﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CustomerAcceptanceConfirmation
    {
        public CustomerAcceptanceConfirmation()
        {
            CustomerAcceptanceQtyOrder = new HashSet<CustomerAcceptanceQtyOrder>();
        }

        public long CustomerAcceptanceConfirmationId { get; set; }
        public bool? IsCustomerName { get; set; }
        public bool? IsContractNo { get; set; }
        public bool? IsProductName { get; set; }
        public bool? IsUom { get; set; }
        public bool? IsTenderExtensionFrom { get; set; }
        public bool? IsTenderExtensionTo { get; set; }
        public bool? IsNoOfLot { get; set; }
        public bool? IsQuotationNo { get; set; }
        public string ConfirmationPono { get; set; }
        public long? BlanketOrderId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual BlanketOrder BlanketOrder { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<CustomerAcceptanceQtyOrder> CustomerAcceptanceQtyOrder { get; set; }
    }
}
