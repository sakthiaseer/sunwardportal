﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DistributorPricing
    {
        public long DistributorPricingId { get; set; }
        public long? BusinessCategoryId { get; set; }
        public long? CustomerId { get; set; }
        public decimal? DistributorPrice { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CompanyListing Customer { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
