﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavSaleCategory
    {
        public NavSaleCategory()
        {
            OrderRequirement = new HashSet<OrderRequirement>();
            PackagingItemHistory = new HashSet<PackagingItemHistory>();
            ProdRecipeCycle = new HashSet<ProdRecipeCycle>();
        }

        public long SaleCategoryId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string NoSeries { get; set; }
        public long? LocationId { get; set; }
        public string SgnoSeries { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<OrderRequirement> OrderRequirement { get; set; }
        public virtual ICollection<PackagingItemHistory> PackagingItemHistory { get; set; }
        public virtual ICollection<ProdRecipeCycle> ProdRecipeCycle { get; set; }
    }
}
