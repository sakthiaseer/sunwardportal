﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ExtensionInformationLine
    {
        public long ExtensionInformationLineId { get; set; }
        public long? TenderInformationId { get; set; }
        public string QuotationNo { get; set; }
        public decimal? Quantity { get; set; }
        public DateTime? ExtendedPeriodTo { get; set; }
        public long? CurrencyId { get; set; }
        public decimal? SellingPrice { get; set; }
        public Guid? SessionId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual TenderInformation TenderInformation { get; set; }
    }
}
