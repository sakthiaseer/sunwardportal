﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WikiChangeControl
    {
        public long ChangeControlId { get; set; }
        public string ChangeControlNo { get; set; }
        public string ChangeControlFile { get; set; }
        public long? WikiPageId { get; set; }
        public string SessionId { get; set; }

        public virtual WikiPage WikiPage { get; set; }
    }
}
