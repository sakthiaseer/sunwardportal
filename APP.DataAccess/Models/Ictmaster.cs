﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Ictmaster
    {
        public Ictmaster()
        {
            ApptransferRelcassEntryReceiveLocation = new HashSet<ApptransferRelcassEntry>();
            ApptransferRelcassEntryTransferFromLocation = new HashSet<ApptransferRelcassEntry>();
            ApptransferRelcassEntryTransferToLocation = new HashSet<ApptransferRelcassEntry>();
            AssetMaster = new HashSet<AssetMaster>();
            BmrdisposalMasterBox = new HashSet<BmrdisposalMasterBox>();
            CommonFieldsProductionMachine = new HashSet<CommonFieldsProductionMachine>();
            CommonMasterToolingClassification = new HashSet<CommonMasterToolingClassification>();
            CompanyCalendar = new HashSet<CompanyCalendar>();
            CompanyCalendarLineLocation = new HashSet<CompanyCalendarLineLocation>();
            FmglobalLocationFrom = new HashSet<Fmglobal>();
            FmglobalLocationTo = new HashSet<Fmglobal>();
            FmglobalMoveLocation = new HashSet<FmglobalMove>();
            FmglobalMoveLocationTo = new HashSet<FmglobalMove>();
            IctlayoutPlanTypes = new HashSet<IctlayoutPlanTypes>();
            InventoryTypeOpeningStockBalanceArea = new HashSet<InventoryTypeOpeningStockBalance>();
            InventoryTypeOpeningStockBalanceLocation = new HashSet<InventoryTypeOpeningStockBalance>();
            InventoryTypeOpeningStockBalanceSpecificArea = new HashSet<InventoryTypeOpeningStockBalance>();
            InverseParentIct = new HashSet<Ictmaster>();
            Ipir = new HashSet<Ipir>();
            IpirApp = new HashSet<IpirApp>();
            IpirReport = new HashSet<IpirReport>();
            MedMasterLineArea = new HashSet<MedMasterLine>();
            MedMasterLineDedicatedUsageArea = new HashSet<MedMasterLine>();
            MedMasterLineDedicatedUsageLocation = new HashSet<MedMasterLine>();
            MedMasterLineDedicatedUsagespecificArea = new HashSet<MedMasterLine>();
            MedMasterLineLocation = new HashSet<MedMasterLine>();
            MedMasterLineSpecificArea = new HashSet<MedMasterLine>();
            PlantMaintenanceEntry = new HashSet<PlantMaintenanceEntry>();
            ProductionActivityApp = new HashSet<ProductionActivityApp>();
            ProductionActivityAppLine = new HashSet<ProductionActivityAppLine>();
            ProductionActivityPlanningApp = new HashSet<ProductionActivityPlanningApp>();
            ProductionActivityPlanningAppLine = new HashSet<ProductionActivityPlanningAppLine>();
            ProductionActivityRoutineApp = new HashSet<ProductionActivityRoutineApp>();
            ProductionActivityRoutineAppLine = new HashSet<ProductionActivityRoutineAppLine>();
            ProductionMethodTemplate = new HashSet<ProductionMethodTemplate>();
            SunwardAssetList = new HashSet<SunwardAssetList>();
            TransferSettingsFromLocation = new HashSet<TransferSettings>();
            TransferSettingsToLocation = new HashSet<TransferSettings>();
        }

        public long IctmasterId { get; set; }
        public long? CompanyId { get; set; }
        public long? ParentIctid { get; set; }
        public int? MasterType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long? LayoutPlanId { get; set; }
        public string VersionNo { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public Guid? SessionId { get; set; }
        public long? SiteId { get; set; }
        public long? LocationId { get; set; }
        public long? ZoneId { get; set; }
        public long? AreaId { get; set; }
        public long? SpecificAreaId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string LocationDescription { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual Documents LayoutPlan { get; set; }
        public virtual CodeMaster MasterTypeNavigation { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Ictmaster ParentIct { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ApptransferRelcassEntry> ApptransferRelcassEntryReceiveLocation { get; set; }
        public virtual ICollection<ApptransferRelcassEntry> ApptransferRelcassEntryTransferFromLocation { get; set; }
        public virtual ICollection<ApptransferRelcassEntry> ApptransferRelcassEntryTransferToLocation { get; set; }
        public virtual ICollection<AssetMaster> AssetMaster { get; set; }
        public virtual ICollection<BmrdisposalMasterBox> BmrdisposalMasterBox { get; set; }
        public virtual ICollection<CommonFieldsProductionMachine> CommonFieldsProductionMachine { get; set; }
        public virtual ICollection<CommonMasterToolingClassification> CommonMasterToolingClassification { get; set; }
        public virtual ICollection<CompanyCalendar> CompanyCalendar { get; set; }
        public virtual ICollection<CompanyCalendarLineLocation> CompanyCalendarLineLocation { get; set; }
        public virtual ICollection<Fmglobal> FmglobalLocationFrom { get; set; }
        public virtual ICollection<Fmglobal> FmglobalLocationTo { get; set; }
        public virtual ICollection<FmglobalMove> FmglobalMoveLocation { get; set; }
        public virtual ICollection<FmglobalMove> FmglobalMoveLocationTo { get; set; }
        public virtual ICollection<IctlayoutPlanTypes> IctlayoutPlanTypes { get; set; }
        public virtual ICollection<InventoryTypeOpeningStockBalance> InventoryTypeOpeningStockBalanceArea { get; set; }
        public virtual ICollection<InventoryTypeOpeningStockBalance> InventoryTypeOpeningStockBalanceLocation { get; set; }
        public virtual ICollection<InventoryTypeOpeningStockBalance> InventoryTypeOpeningStockBalanceSpecificArea { get; set; }
        public virtual ICollection<Ictmaster> InverseParentIct { get; set; }
        public virtual ICollection<Ipir> Ipir { get; set; }
        public virtual ICollection<IpirApp> IpirApp { get; set; }
        public virtual ICollection<IpirReport> IpirReport { get; set; }
        public virtual ICollection<MedMasterLine> MedMasterLineArea { get; set; }
        public virtual ICollection<MedMasterLine> MedMasterLineDedicatedUsageArea { get; set; }
        public virtual ICollection<MedMasterLine> MedMasterLineDedicatedUsageLocation { get; set; }
        public virtual ICollection<MedMasterLine> MedMasterLineDedicatedUsagespecificArea { get; set; }
        public virtual ICollection<MedMasterLine> MedMasterLineLocation { get; set; }
        public virtual ICollection<MedMasterLine> MedMasterLineSpecificArea { get; set; }
        public virtual ICollection<PlantMaintenanceEntry> PlantMaintenanceEntry { get; set; }
        public virtual ICollection<ProductionActivityApp> ProductionActivityApp { get; set; }
        public virtual ICollection<ProductionActivityAppLine> ProductionActivityAppLine { get; set; }
        public virtual ICollection<ProductionActivityPlanningApp> ProductionActivityPlanningApp { get; set; }
        public virtual ICollection<ProductionActivityPlanningAppLine> ProductionActivityPlanningAppLine { get; set; }
        public virtual ICollection<ProductionActivityRoutineApp> ProductionActivityRoutineApp { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLine> ProductionActivityRoutineAppLine { get; set; }
        public virtual ICollection<ProductionMethodTemplate> ProductionMethodTemplate { get; set; }
        public virtual ICollection<SunwardAssetList> SunwardAssetList { get; set; }
        public virtual ICollection<TransferSettings> TransferSettingsFromLocation { get; set; }
        public virtual ICollection<TransferSettings> TransferSettingsToLocation { get; set; }
    }
}
