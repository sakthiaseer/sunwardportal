﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CommonFieldsProductionMachine
    {
        public CommonFieldsProductionMachine()
        {
            BlisterMachineCommonFieldMultiple = new HashSet<BlisterMachineCommonFieldMultiple>();
            CommonFieldsProductionMachineLine = new HashSet<CommonFieldsProductionMachineLine>();
            CommonMasterMouldChangePartsPallowInterchangeMachine = new HashSet<CommonMasterMouldChangeParts>();
            CommonMasterMouldChangePartsPinterCompanyMachine = new HashSet<CommonMasterMouldChangeParts>();
            CompanyCalendarLine = new HashSet<CompanyCalendarLine>();
            Icbmpline = new HashSet<Icbmpline>();
            Ipir = new HashSet<Ipir>();
            MedMasterLineMachine = new HashSet<MedMasterLineMachine>();
            ProcessMachineTimeProductionMachineProcess = new HashSet<ProcessMachineTimeProductionMachineProcess>();
            ProductionMethodTemplate = new HashSet<ProductionMethodTemplate>();
            PunchesMachine = new HashSet<PunchesMachine>();
            StandardManufacturingProcessLineMachineGrouping = new HashSet<StandardManufacturingProcessLine>();
            StandardManufacturingProcessLineMachineName = new HashSet<StandardManufacturingProcessLine>();
        }

        public long CommonFieldsProductionMachineId { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public long? ManufacturingSiteId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public long? ManufacturingStepsId { get; set; }
        public long? MachineGroupingId { get; set; }
        public string MachineNo { get; set; }
        public string NameOfTheMachine { get; set; }
        public long? Location { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileReferenceNo { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ItemClassificationMaster ItemClassificationMaster { get; set; }
        public virtual Ictmaster LocationNavigation { get; set; }
        public virtual ApplicationMasterDetail MachineGrouping { get; set; }
        public virtual ApplicationMasterDetail ManufacturingProcess { get; set; }
        public virtual ApplicationMasterDetail ManufacturingSite { get; set; }
        public virtual ApplicationMasterDetail ManufacturingSteps { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<BlisterMachineCommonFieldMultiple> BlisterMachineCommonFieldMultiple { get; set; }
        public virtual ICollection<CommonFieldsProductionMachineLine> CommonFieldsProductionMachineLine { get; set; }
        public virtual ICollection<CommonMasterMouldChangeParts> CommonMasterMouldChangePartsPallowInterchangeMachine { get; set; }
        public virtual ICollection<CommonMasterMouldChangeParts> CommonMasterMouldChangePartsPinterCompanyMachine { get; set; }
        public virtual ICollection<CompanyCalendarLine> CompanyCalendarLine { get; set; }
        public virtual ICollection<Icbmpline> Icbmpline { get; set; }
        public virtual ICollection<Ipir> Ipir { get; set; }
        public virtual ICollection<MedMasterLineMachine> MedMasterLineMachine { get; set; }
        public virtual ICollection<ProcessMachineTimeProductionMachineProcess> ProcessMachineTimeProductionMachineProcess { get; set; }
        public virtual ICollection<ProductionMethodTemplate> ProductionMethodTemplate { get; set; }
        public virtual ICollection<PunchesMachine> PunchesMachine { get; set; }
        public virtual ICollection<StandardManufacturingProcessLine> StandardManufacturingProcessLineMachineGrouping { get; set; }
        public virtual ICollection<StandardManufacturingProcessLine> StandardManufacturingProcessLineMachineName { get; set; }
    }
}
