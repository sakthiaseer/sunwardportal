﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Fpproduct
    {
        public Fpproduct()
        {
            FpproductLine = new HashSet<FpproductLine>();
            FpproductPacksizeMultiple = new HashSet<FpproductPacksizeMultiple>();
        }

        public long FpproductId { get; set; }
        public long? FpproductGeneralInfoId { get; set; }
        public bool? IsActiveSalesPack { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual FinishProductGeneralInfo FpproductGeneralInfo { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<FpproductLine> FpproductLine { get; set; }
        public virtual ICollection<FpproductPacksizeMultiple> FpproductPacksizeMultiple { get; set; }
    }
}
