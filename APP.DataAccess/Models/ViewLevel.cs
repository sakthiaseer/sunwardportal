﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewLevel
    {
        public long? CompanyId { get; set; }
        public long LevelId { get; set; }
        public string Name { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string StatusCode { get; set; }
        public string AddedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string PlantCode { get; set; }
        public string CompanyName { get; set; }
        public int? CodeId { get; set; }
        public string DivisionName { get; set; }
        public long? DivisionId { get; set; }
    }
}
