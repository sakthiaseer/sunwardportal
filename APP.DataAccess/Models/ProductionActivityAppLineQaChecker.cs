﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionActivityAppLineQaChecker
    {
        public long ProductionActivityAppLineQaCheckerId { get; set; }
        public long? ProductionActivityAppLineId { get; set; }
        public bool? QaCheck { get; set; }
        public long? QaCheckUserId { get; set; }
        public DateTime? QaCheckDate { get; set; }

        public virtual ProductionActivityAppLine ProductionActivityAppLine { get; set; }
        public virtual ApplicationUser QaCheckUser { get; set; }
    }
}
