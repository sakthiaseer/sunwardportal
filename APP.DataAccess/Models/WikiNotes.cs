﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WikiNotes
    {
        public long WikiNoteId { get; set; }
        public long? WikiPageId { get; set; }
        public string Notes { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? AddedBy { get; set; }

        public virtual ApplicationUser AddedByNavigation { get; set; }
        public virtual WikiPage WikiPage { get; set; }
    }
}
