﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class RequestAc
    {
        public RequestAc()
        {
            RequestAcline = new HashSet<RequestAcline>();
        }

        public long RequestAcid { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileNo { get; set; }
        public long? CompanyId { get; set; }
        public long? CustomerId { get; set; }
        public DateTime? DateChange { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? Date { get; set; }
        public string Remarks { get; set; }
        public DateTime? ReferenceMonth { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual CompanyListing Customer { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<RequestAcline> RequestAcline { get; set; }
    }
}
