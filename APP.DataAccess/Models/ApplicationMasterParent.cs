﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApplicationMasterParent
    {
        public ApplicationMasterParent()
        {
            ApplicationMasterAccess = new HashSet<ApplicationMasterAccess>();
            ApplicationMasterChild = new HashSet<ApplicationMasterChild>();
            InverseParent = new HashSet<ApplicationMasterParent>();
        }

        public long ApplicationMasterParentId { get; set; }
        public long ApplicationMasterParentCodeId { get; set; }
        public string ApplicationMasterName { get; set; }
        public string Description { get; set; }
        public long? ParentId { get; set; }
        public bool? IsDisplay { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationMasterParent Parent { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ApplicationMasterAccess> ApplicationMasterAccess { get; set; }
        public virtual ICollection<ApplicationMasterChild> ApplicationMasterChild { get; set; }
        public virtual ICollection<ApplicationMasterParent> InverseParent { get; set; }
    }
}
