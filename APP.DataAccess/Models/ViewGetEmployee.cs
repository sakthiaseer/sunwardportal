﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewGetEmployee
    {
        public string Name { get; set; }
        public long EmployeeId { get; set; }
        public long? UserId { get; set; }
        public string SageId { get; set; }
        public long? PlantId { get; set; }
        public long? LevelId { get; set; }
        public long? LanguageId { get; set; }
        public long? CityId { get; set; }
        public long? RegionId { get; set; }
        public long? ReportId { get; set; }
        public string FirstName { get; set; }
        public string NickName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string JobTitle { get; set; }
        public string Email { get; set; }
        public string LoginId { get; set; }
        public string LoginPassword { get; set; }
        public string UserCode { get; set; }
        public long? UserGroupId { get; set; }
        public long? RoleId { get; set; }
        public int? TypeOfEmployeement { get; set; }
        public string Signature { get; set; }
        public string ImageUrl { get; set; }
        public DateTime? DateOfEmployeement { get; set; }
        public DateTime? LastWorkingDate { get; set; }
        public string Extension { get; set; }
        public string SpeedDial { get; set; }
        public string SkypeAddress { get; set; }
        public string Mobile { get; set; }
        public bool? IsActive { get; set; }
        public long? SectionId { get; set; }
        public long? DivisionId { get; set; }
        public long? DesignationId { get; set; }
        public long? DepartmentId { get; set; }
        public long? SubSectionId { get; set; }
        public long? SubSectionTid { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? AcceptanceStatus { get; set; }
        public DateTime? ExpectedJoiningDate { get; set; }
        public DateTime? AcceptanceStatusDate { get; set; }
        public int? HeadCount { get; set; }
        public string SectionName { get; set; }
        public string CompanyName { get; set; }
        public string DivisionName { get; set; }
        public string DepartmentName { get; set; }
        public string SubSectionName { get; set; }
        public string SubSectionTwoName { get; set; }
        public string DesignationName { get; set; }
        public string StatusCode { get; set; }
        public string AddedByUser { get; set; }
        public string ModifiedByUser { get; set; }
        public string Status { get; set; }
        public Guid? SessionId { get; set; }
        public int? InvalidAttempts { get; set; }
        public bool? Locked { get; set; }
    }
}
