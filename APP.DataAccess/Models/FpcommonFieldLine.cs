﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FpcommonFieldLine
    {
        public long FpcommonFieldLineId { get; set; }
        public long? FpcommonFiledId { get; set; }
        public long? MaterialNoId { get; set; }
        public string MaterialName { get; set; }
        public decimal? DosageNo { get; set; }
        public long? DosageUnitId { get; set; }
        public long? PerDosageId { get; set; }
        public string Overage { get; set; }
        public long? OverageUnitsId { get; set; }
        public long? OverageDosageUnitsId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual FpcommonField FpcommonFiled { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
