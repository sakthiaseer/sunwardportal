﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionOrderMaster
    {
        public long ProductionOrderMasterId { get; set; }
        public long? ProdutionOrderId { get; set; }
        public long? ProductionOrderNumberingId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public DateTime? ExpectedReleaseDate { get; set; }
        public DateTime? TargetFullFillDate { get; set; }
        public bool? IsExactQty { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProductionOrder ProdutionOrder { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
