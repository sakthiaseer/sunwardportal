﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class HumanMovementDocument
    {
        public long HumanMovementDocumentId { get; set; }
        public Guid? SessionId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] FileData { get; set; }
        public long? FileSize { get; set; }
    }
}
