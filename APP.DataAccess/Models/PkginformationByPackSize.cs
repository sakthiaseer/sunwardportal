﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PkginformationByPackSize
    {
        public long PkginformationByPackSizeId { get; set; }
        public long? PkgregisteredPackingInformationId { get; set; }
        public string ProfileRefNo { get; set; }
        public long? PackSizeId { get; set; }
        public DateTime? DateOfApproval { get; set; }
        public DateTime? DateOfDownload { get; set; }
        public string VersionNo { get; set; }
        public string Link { get; set; }
        public long? ProfileId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual PkgregisteredPackingInformation PkgregisteredPackingInformation { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
