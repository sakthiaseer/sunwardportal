﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CalibrationVendorInfo
    {
        public long CalibrationVendorInformationId { get; set; }
        public string VendorName { get; set; }
        public string MethodOfCalibration { get; set; }
        public DateTime? CalibrationLeadTime { get; set; }
        public string MethodOfQuotation { get; set; }
        public string Rating { get; set; }
        public long? CalibrationServiceInformationId { get; set; }
        public int? Status { get; set; }
        public long? DeviceCatalogMasterId { get; set; }

        public virtual CalibrationServiceInfo CalibrationServiceInformation { get; set; }
        public virtual DeviceCatalogMaster DeviceCatalogMaster { get; set; }
        public virtual CodeMaster StatusNavigation { get; set; }
    }
}
