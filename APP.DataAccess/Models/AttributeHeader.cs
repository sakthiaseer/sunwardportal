﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AttributeHeader
    {
        public AttributeHeader()
        {
            AttributeDetails = new HashSet<AttributeDetails>();
            DynamicFormSectionAttribute = new HashSet<DynamicFormSectionAttribute>();
            InverseSubAttribute = new HashSet<AttributeHeader>();
        }

        public int AttributeId { get; set; }
        public string AttributeName { get; set; }
        public string Description { get; set; }
        public string ControlType { get; set; }
        public string EntryMask { get; set; }
        public string RegExp { get; set; }
        public string ListDefault { get; set; }
        public bool? IsInternal { get; set; }
        public bool? ContainsPersonalData { get; set; }
        public Guid? SessionId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ControlTypeId { get; set; }
        public bool? IsMultiple { get; set; }
        public bool? IsRequired { get; set; }
        public string RequiredMessage { get; set; }
        public int? FormUsedCount { get; set; }
        public string DropDownTypeId { get; set; }
        public long? DataSourceId { get; set; }
        public long? AttributeCompanyId { get; set; }
        public long? DynamicFormId { get; set; }
        public bool? IsDynamicFormDropTagBox { get; set; }
        public bool? IsSubForm { get; set; }
        public int? SubAttributeId { get; set; }
        public int? SubAttributeDetailId { get; set; }
        public string IsAttributeSpinEditType { get; set; }
        public bool? IsAttributeDisplayTableHeader { get; set; }
        public string AttributeFormToolTips { get; set; }
        public bool? AttributeIsVisible { get; set; }
        public string AttributeRadioLayout { get; set; }
        public long? ApplicationMasterSubFormId { get; set; }
        public bool? IsDeleted { get; set; }
        public string SubApplicationMasterIds { get; set; }
        public string AttrDescription { get; set; }
        public int? AttributeSortBy { get; set; }
        public bool? IsFilterDataSource { get; set; }
        public long? FilterDataSocurceId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMaster ApplicationMasterSubForm { get; set; }
        public virtual Plant AttributeCompany { get; set; }
        public virtual CodeMaster ControlTypeNavigation { get; set; }
        public virtual AttributeHeaderDataSource DataSource { get; set; }
        public virtual DynamicForm DynamicForm { get; set; }
        public virtual DynamicFormFilter FilterDataSocurce { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual AttributeHeader SubAttribute { get; set; }
        public virtual AttributeDetails SubAttributeDetail { get; set; }
        public virtual ICollection<AttributeDetails> AttributeDetails { get; set; }
        public virtual ICollection<DynamicFormSectionAttribute> DynamicFormSectionAttribute { get; set; }
        public virtual ICollection<AttributeHeader> InverseSubAttribute { get; set; }
    }
}
