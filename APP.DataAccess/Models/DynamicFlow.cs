﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFlow
    {
        public DynamicFlow()
        {
            ActiveFlow = new HashSet<ActiveFlow>();
            DynamicFlowDetail = new HashSet<DynamicFlowDetail>();
            DynamicFlowProgress = new HashSet<DynamicFlowProgress>();
            FlowDistribution = new HashSet<FlowDistribution>();
        }

        public long DynamicFlowId { get; set; }
        public long? CompanyId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string TotalProjectTime { get; set; }
        public long? Owner { get; set; }
        public int? CategoryId { get; set; }
        public string Recurrance { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster Category { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationUser OwnerNavigation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ActiveFlow> ActiveFlow { get; set; }
        public virtual ICollection<DynamicFlowDetail> DynamicFlowDetail { get; set; }
        public virtual ICollection<DynamicFlowProgress> DynamicFlowProgress { get; set; }
        public virtual ICollection<FlowDistribution> FlowDistribution { get; set; }
    }
}
