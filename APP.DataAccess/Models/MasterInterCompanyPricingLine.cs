﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class MasterInterCompanyPricingLine
    {
        public long MasterInterCompanyPricingLineId { get; set; }
        public long? MasterInterCompanyPricingId { get; set; }
        public long? SellingToId { get; set; }
        public long? CurrencyId { get; set; }
        public decimal? BillingPercentage { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual MasterInterCompanyPricing MasterInterCompanyPricing { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CompanyListing SellingTo { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
