﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DraftApplicationWikiLine
    {
        public DraftApplicationWikiLine()
        {
            DraftApplicationWikiLineDuty = new HashSet<DraftApplicationWikiLineDuty>();
            DraftApplicationWikiLineNotify = new HashSet<DraftApplicationWikiLineNotify>();
            DraftApplicationWikiRecurrence = new HashSet<DraftApplicationWikiRecurrence>();
            DraftApplicationWikiWeekly = new HashSet<DraftApplicationWikiWeekly>();
        }

        public long ApplicationWikiLineId { get; set; }
        public long? ApplicationWikiId { get; set; }
        public long? DutyId { get; set; }
        public string Responsibility { get; set; }
        public long? PageLink { get; set; }
        public long? FunctionLink { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? NotificationAdvice { get; set; }
        public int? NotificationAdviceTypeId { get; set; }
        public int? RepeatId { get; set; }
        public int? CustomId { get; set; }
        public DateTime? DueDate { get; set; }
        public int? Monthly { get; set; }
        public int? Yearly { get; set; }
        public string EventDescription { get; set; }
        public bool? DaysOfWeek { get; set; }
        public int? NotificationStatusId { get; set; }
        public Guid? SessionId { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public long? NotifyTo { get; set; }
        public string ScreenId { get; set; }
        public DateTime? NotifyEndDate { get; set; }
        public bool? IsAllowDocAccess { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual DraftApplicationWiki ApplicationWiki { get; set; }
        public virtual CodeMaster Custom { get; set; }
        public virtual ApplicationPermission FunctionLinkNavigation { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster NotificationAdviceType { get; set; }
        public virtual Employee NotifyToNavigation { get; set; }
        public virtual ApplicationPermission PageLinkNavigation { get; set; }
        public virtual CodeMaster Repeat { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<DraftApplicationWikiLineDuty> DraftApplicationWikiLineDuty { get; set; }
        public virtual ICollection<DraftApplicationWikiLineNotify> DraftApplicationWikiLineNotify { get; set; }
        public virtual ICollection<DraftApplicationWikiRecurrence> DraftApplicationWikiRecurrence { get; set; }
        public virtual ICollection<DraftApplicationWikiWeekly> DraftApplicationWikiWeekly { get; set; }
    }
}
