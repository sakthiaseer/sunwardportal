﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class MarginInformationLine
    {
        public long MarginInformationLineId { get; set; }
        public long? MarginInformationId { get; set; }
        public long? ManufacturingSiteId { get; set; }
        public long? ItemId { get; set; }
        public decimal? SpecialMarginPercentage { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual GenericCodes Item { get; set; }
        public virtual MarginInformation MarginInformation { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
