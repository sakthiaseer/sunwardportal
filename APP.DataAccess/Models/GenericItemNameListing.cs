﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class GenericItemNameListing
    {
        public GenericItemNameListing()
        {
            FinishProductExcipientLine = new HashSet<FinishProductExcipientLine>();
            ProductionMaterial = new HashSet<ProductionMaterial>();
        }

        public long GenericItemNameListingId { get; set; }
        public string GenericName { get; set; }
        public string AlsoKownAs { get; set; }
        public long? ItemClassificationId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<FinishProductExcipientLine> FinishProductExcipientLine { get; set; }
        public virtual ICollection<ProductionMaterial> ProductionMaterial { get; set; }
    }
}
