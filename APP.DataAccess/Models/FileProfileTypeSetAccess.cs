﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FileProfileTypeSetAccess
    {
        public long FileProfileTypeSetAccessId { get; set; }
        public long? UserId { get; set; }
        public long? DocumentId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Documents Document { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
