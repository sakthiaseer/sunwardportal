﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class IssueReportAssignTo
    {
        public long IpirassignToId { get; set; }
        public long? Ipirid { get; set; }
        public long? AssignToId { get; set; }
        public long? ReportinginformationId { get; set; }
    }
}
