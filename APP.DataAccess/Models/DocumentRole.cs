﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DocumentRole
    {
        public DocumentRole()
        {
            DocumentPermission = new HashSet<DocumentPermission>();
            DocumentRolePermission = new HashSet<DocumentRolePermission>();
            DocumentUserRole = new HashSet<DocumentUserRole>();
            ItemClassificationAccess = new HashSet<ItemClassificationAccess>();
            RegistrationFormatInformationLineUsers = new HashSet<RegistrationFormatInformationLineUsers>();
        }

        public long DocumentRoleId { get; set; }
        public string DocumentRoleName { get; set; }
        public string DocumentRoleDescription { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<DocumentPermission> DocumentPermission { get; set; }
        public virtual ICollection<DocumentRolePermission> DocumentRolePermission { get; set; }
        public virtual ICollection<DocumentUserRole> DocumentUserRole { get; set; }
        public virtual ICollection<ItemClassificationAccess> ItemClassificationAccess { get; set; }
        public virtual ICollection<RegistrationFormatInformationLineUsers> RegistrationFormatInformationLineUsers { get; set; }
    }
}
