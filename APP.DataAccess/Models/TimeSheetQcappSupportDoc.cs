﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TimeSheetQcappSupportDoc
    {
        public long QctimeSheetSupportDocId { get; set; }
        public long? QctimesheetId { get; set; }
        public Guid? SessionId { get; set; }
        public long? DocumentId { get; set; }
        public string Type { get; set; }
    }
}
