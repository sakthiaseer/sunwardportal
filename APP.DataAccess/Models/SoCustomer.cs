﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SoCustomer
    {
        public SoCustomer()
        {
            NavCrossReference = new HashSet<NavCrossReference>();
            SoSalesOrder = new HashSet<SoSalesOrder>();
        }

        public long SoCustomerId { get; set; }
        public string ShipCode { get; set; }
        public string CustomerName { get; set; }
        public string AssignToRep { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string PostCode { get; set; }
        public string Channel { get; set; }
        public string Type { get; set; }

        public virtual ICollection<NavCrossReference> NavCrossReference { get; set; }
        public virtual ICollection<SoSalesOrder> SoSalesOrder { get; set; }
    }
}
