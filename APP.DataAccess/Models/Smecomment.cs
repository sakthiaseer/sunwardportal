﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Smecomment
    {
        public long SmecommentId { get; set; }
        public long UserId { get; set; }
        public long? SmegroupId { get; set; }
        public long WorkListId { get; set; }
        public bool? NoComment { get; set; }
        public string ChangeControlComment { get; set; }
        public string Comment { get; set; }
        public bool? IsAgreed { get; set; }
        public string SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public DateTime AddedDate { get; set; }

        public virtual UserGroup Smegroup { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual WorkList WorkList { get; set; }
    }
}
