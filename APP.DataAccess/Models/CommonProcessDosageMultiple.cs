﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CommonProcessDosageMultiple
    {
        public long CommonProcessDosageFormId { get; set; }
        public long? DosageFormId { get; set; }
        public long? CommonProcessId { get; set; }

        public virtual CommonProcess CommonProcess { get; set; }
        public virtual ApplicationMasterDetail DosageForm { get; set; }
    }
}
