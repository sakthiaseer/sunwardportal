﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductActivityPermission
    {
        public long ProductActivityPermissionId { get; set; }
        public long? ProductActivityCaseId { get; set; }
        public long? ProductActivityCaseResponsDutyId { get; set; }
        public string Type { get; set; }
        public bool? IsChecker { get; set; }
        public bool? IsUpdateStatus { get; set; }
        public bool? IsCheckOut { get; set; }
        public bool? IsMail { get; set; }
        public bool? IsSupportDocuments { get; set; }
        public bool? IsCopyLink { get; set; }
        public bool? IsViewHistory { get; set; }
        public bool? IsNonCompliance { get; set; }
        public bool? IsActivityInfo { get; set; }
        public bool? IsViewFile { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? StatusCodeId { get; set; }
        public long? UserId { get; set; }
        public long? UserGroupId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual UserGroup UserGroup { get; set; }
    }
}
