﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SellingCatalogue
    {
        public SellingCatalogue()
        {
            SellingPriceInformation = new HashSet<SellingPriceInformation>();
        }

        public long SellingCatalogueId { get; set; }
        public long? CountryId { get; set; }
        public long? CompanyId { get; set; }
        public DateTime? FromValidity { get; set; }
        public DateTime? ToValidity { get; set; }
        public bool? NeedToKeepVersionInfo { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? CustomerId { get; set; }
        public Guid? VersionSessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CompanyListing Company { get; set; }
        public virtual ApplicationMasterDetail Country { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<SellingPriceInformation> SellingPriceInformation { get; set; }
    }
}
