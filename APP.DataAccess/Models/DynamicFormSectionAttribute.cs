﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormSectionAttribute
    {
        public DynamicFormSectionAttribute()
        {
            DynamicFormData = new HashSet<DynamicFormData>();
            DynamicFormSectionAttributeSectionParent = new HashSet<DynamicFormSectionAttributeSectionParent>();
            DynamicFormSectionAttributeSecurity = new HashSet<DynamicFormSectionAttributeSecurity>();
        }

        public long DynamicFormSectionAttributeId { get; set; }
        public long? DynamicFormSectionId { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? AttributeId { get; set; }
        public int? SortOrderBy { get; set; }
        public int? ColSpan { get; set; }
        public string DisplayName { get; set; }
        public bool? IsMultiple { get; set; }
        public bool? IsRequired { get; set; }
        public string RequiredMessage { get; set; }
        public string IsSpinEditType { get; set; }
        public int? FormUsedCount { get; set; }
        public bool? IsDisplayTableHeader { get; set; }
        public string FormToolTips { get; set; }
        public bool? IsVisible { get; set; }
        public string RadioLayout { get; set; }
        public string IsRadioCheckRemarks { get; set; }
        public string RemarksLabelName { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsPlantLoadDependency { get; set; }
        public long? PlantDropDownWithOtherDataSourceId { get; set; }
        public string PlantDropDownWithOtherDataSourceLabelName { get; set; }
        public string PlantDropDownWithOtherDataSourceIds { get; set; }
        public bool? IsSetDefaultValue { get; set; }
        public bool? IsDefaultReadOnly { get; set; }
        public long? ApplicationMasterId { get; set; }
        public string ApplicationMasterIds { get; set; }
        public bool? IsDisplayDropDownHeader { get; set; }
        public bool? IsDependencyMultiple { get; set; }
        public bool? IsDynamicFormGridDropdown { get; set; }
        public long? GridDropDownDynamicFormId { get; set; }
        public bool? IsDynamicFormGridDropdownMultiple { get; set; }
        public string FormulaTextBox { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMaster ApplicationMaster { get; set; }
        public virtual AttributeHeader Attribute { get; set; }
        public virtual DynamicFormSection DynamicFormSection { get; set; }
        public virtual DynamicForm GridDropDownDynamicForm { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<DynamicFormData> DynamicFormData { get; set; }
        public virtual ICollection<DynamicFormSectionAttributeSectionParent> DynamicFormSectionAttributeSectionParent { get; set; }
        public virtual ICollection<DynamicFormSectionAttributeSecurity> DynamicFormSectionAttributeSecurity { get; set; }
    }
}
