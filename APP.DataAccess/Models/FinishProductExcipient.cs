﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FinishProductExcipient
    {
        public FinishProductExcipient()
        {
            BomproductionGroup = new HashSet<BomproductionGroup>();
            FinishProductExcipientLine = new HashSet<FinishProductExcipientLine>();
            FpbomInformationMultiple = new HashSet<FpbomInformationMultiple>();
            PerUnitFormulationLine = new HashSet<PerUnitFormulationLine>();
        }

        public long FinishProductExcipientId { get; set; }
        public long? FinishProductId { get; set; }
        public int? RegisterationCodeId { get; set; }
        public bool? IsMasterDocument { get; set; }
        public bool? IsInformaionvsmaster { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual FinishProductGeneralInfo FinishProduct { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster RegisterationCode { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<BomproductionGroup> BomproductionGroup { get; set; }
        public virtual ICollection<FinishProductExcipientLine> FinishProductExcipientLine { get; set; }
        public virtual ICollection<FpbomInformationMultiple> FpbomInformationMultiple { get; set; }
        public virtual ICollection<PerUnitFormulationLine> PerUnitFormulationLine { get; set; }
    }
}
