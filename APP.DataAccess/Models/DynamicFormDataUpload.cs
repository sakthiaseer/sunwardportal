﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormDataUpload
    {
        public long DynamicFormDataUploadId { get; set; }
        public long? DynamicFormDataId { get; set; }
        public long? DynamicFormSectionId { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDmsLink { get; set; }
        public long? LinkFileProfileTypeDocumentId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual DynamicFormData DynamicFormData { get; set; }
        public virtual DynamicFormSection DynamicFormSection { get; set; }
        public virtual LinkFileProfileTypeDocument LinkFileProfileTypeDocument { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
