﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AssetTransfer
    {
        public long AssetTransferId { get; set; }
        public long? AssetId { get; set; }
        public long? LocationId { get; set; }
        public long? TransferTo { get; set; }
        public decimal? TransferQty { get; set; }
        public string Remarks { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual AssetMaster Asset { get; set; }
        public virtual Locations Location { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual Locations TransferToNavigation { get; set; }
    }
}
