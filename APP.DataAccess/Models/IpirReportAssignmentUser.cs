﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class IpirReportAssignmentUser
    {
        public long IpirReportAssignmentUserId { get; set; }
        public long? IpirReportAssignmentId { get; set; }
        public long? UserId { get; set; }
        public long? UserGroupId { get; set; }
        public string Type { get; set; }

        public virtual IpirReportAssignment IpirReportAssignment { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual UserGroup UserGroup { get; set; }
    }
}
