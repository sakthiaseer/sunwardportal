﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AttributeHeaderDataSource
    {
        public AttributeHeaderDataSource()
        {
            AttributeHeader = new HashSet<AttributeHeader>();
            DynamicFormFilterBy = new HashSet<DynamicFormFilterBy>();
        }

        public long HeaderDataSourceId { get; set; }
        public long AttributeHeaderDataSourceId { get; set; }
        public string DisplayName { get; set; }
        public string DataSourceTable { get; set; }

        public virtual ICollection<AttributeHeader> AttributeHeader { get; set; }
        public virtual ICollection<DynamicFormFilterBy> DynamicFormFilterBy { get; set; }
    }
}
