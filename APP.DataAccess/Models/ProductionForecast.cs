﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionForecast
    {
        public long ProductionForecastId { get; set; }
        public long? MethodCodeId { get; set; }
        public long? ItemId { get; set; }
        public DateTime? ProductionMonth { get; set; }
        public string BatchSize { get; set; }
        public decimal? PackQuantity { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual NavMethodCode MethodCode { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
