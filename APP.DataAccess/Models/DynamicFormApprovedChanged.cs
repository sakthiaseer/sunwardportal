﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormApprovedChanged
    {
        public long DynamicFormApprovedChangedId { get; set; }
        public long? DynamicFormApprovedId { get; set; }
        public long? UserId { get; set; }
        public bool? IsApprovedStatus { get; set; }

        public virtual DynamicFormApproved DynamicFormApproved { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
