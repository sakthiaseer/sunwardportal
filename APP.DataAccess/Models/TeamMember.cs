﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TeamMember
    {
        public long TeamMemberId { get; set; }
        public long? MemberId { get; set; }
        public long? TeamId { get; set; }

        public virtual ApplicationUser Member { get; set; }
        public virtual TeamMaster Team { get; set; }
    }
}
