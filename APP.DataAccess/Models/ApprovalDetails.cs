﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApprovalDetails
    {
        public long ApprovalId { get; set; }
        public long? ApproverId { get; set; }
        public Guid? SessionId { get; set; }
        public string Remarks { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApproveMaster Approver { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
