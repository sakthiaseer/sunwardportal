﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TaskComment
    {
        public TaskComment()
        {
            CommentAttachment = new HashSet<CommentAttachment>();
            InverseParentComment = new HashSet<TaskComment>();
            InverseQuoteComment = new HashSet<TaskComment>();
            TaskAttachment = new HashSet<TaskAttachment>();
            TaskCommentUser = new HashSet<TaskCommentUser>();
            TaskUnReadNotes = new HashSet<TaskUnReadNotes>();
        }

        public long TaskCommentId { get; set; }
        public long? TaskMasterId { get; set; }
        public long? ParentCommentId { get; set; }
        public string Comment { get; set; }
        public long? CommentedBy { get; set; }
        public DateTime? CommentedDate { get; set; }
        public bool? IsRead { get; set; }
        public bool? IsEdited { get; set; }
        public long? EditedBy { get; set; }
        public DateTime? EditedDate { get; set; }
        public bool? IsDocument { get; set; }
        public long? DocumentId { get; set; }
        public Guid? SessionId { get; set; }
        public string TaskSubject { get; set; }
        public long? TaskId { get; set; }
        public long? MainTaskId { get; set; }
        public bool? IsQuote { get; set; }
        public long? QuoteCommentId { get; set; }

        public virtual ApplicationUser CommentedByNavigation { get; set; }
        public virtual Documents Document { get; set; }
        public virtual ApplicationUser EditedByNavigation { get; set; }
        public virtual TaskComment ParentComment { get; set; }
        public virtual TaskComment QuoteComment { get; set; }
        public virtual TaskMaster TaskMaster { get; set; }
        public virtual ICollection<CommentAttachment> CommentAttachment { get; set; }
        public virtual ICollection<TaskComment> InverseParentComment { get; set; }
        public virtual ICollection<TaskComment> InverseQuoteComment { get; set; }
        public virtual ICollection<TaskAttachment> TaskAttachment { get; set; }
        public virtual ICollection<TaskCommentUser> TaskCommentUser { get; set; }
        public virtual ICollection<TaskUnReadNotes> TaskUnReadNotes { get; set; }
    }
}
