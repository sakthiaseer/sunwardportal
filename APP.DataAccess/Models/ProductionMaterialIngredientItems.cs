﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionMaterialIngredientItems
    {
        public long ProductMaterialIngredientId { get; set; }
        public long? IngredientId { get; set; }
        public long? ProductionMaterialId { get; set; }

        public virtual ProductionMaterial ProductionMaterial { get; set; }
    }
}
