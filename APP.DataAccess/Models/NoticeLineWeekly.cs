﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NoticeLineWeekly
    {
        public long NoticeLineWeeklyId { get; set; }
        public long? NoticeLineId { get; set; }
        public int? WeeklyId { get; set; }
        public string CustomType { get; set; }

        public virtual NoticeLine NoticeLine { get; set; }
        public virtual CodeMaster Weekly { get; set; }
    }
}
