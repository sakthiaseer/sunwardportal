﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmailDynamicFormSection
    {
        public long Id { get; set; }
        public long? DynamicFormId { get; set; }
        public Guid? FormDataSessionId { get; set; }
        public Guid? FormSectionSessionId { get; set; }
        public Guid? EmailSessionId { get; set; }
    }
}
