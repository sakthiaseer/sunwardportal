﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductActivityCaseActionMultiple
    {
        public long ProductActivityCaseActionMultipleId { get; set; }
        public long? ProductActivityCaseId { get; set; }
        public long? ActionId { get; set; }

        public virtual ApplicationMasterChild Action { get; set; }
        public virtual ProductActivityCase ProductActivityCase { get; set; }
    }
}
