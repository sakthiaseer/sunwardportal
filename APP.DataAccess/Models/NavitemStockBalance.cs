﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavitemStockBalance
    {
        public long NavStockBalanceId { get; set; }
        public long? ItemId { get; set; }
        public DateTime? StockBalMonth { get; set; }
        public int? StockBalWeek { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? RejectQuantity { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public decimal? ReworkQty { get; set; }
        public decimal? Wipqty { get; set; }
        public decimal? GlobalQty { get; set; }
        public decimal? Kivqty { get; set; }
        public decimal? SupplyWipqty { get; set; }
        public decimal? Supply1ProcessQty { get; set; }
        public decimal? NotStartInvQty { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
