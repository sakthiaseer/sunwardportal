﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CommonProcess
    {
        public CommonProcess()
        {
            CommonProcessDosageMultiple = new HashSet<CommonProcessDosageMultiple>();
            CommonProcessLine = new HashSet<CommonProcessLine>();
        }

        public long CommonProcessId { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public long? ProfileId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileReferenceNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ItemClassificationMaster ItemClassificationMaster { get; set; }
        public virtual ApplicationMasterDetail ManufacturingProcess { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<CommonProcessDosageMultiple> CommonProcessDosageMultiple { get; set; }
        public virtual ICollection<CommonProcessLine> CommonProcessLine { get; set; }
    }
}
