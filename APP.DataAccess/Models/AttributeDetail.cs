﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AttributeDetail
    {
        public int AttributeDetailId { get; set; }
        public int? AttributeId { get; set; }
        public string Description { get; set; }
        public int? SortOrder { get; set; }
        public bool? Disabled { get; set; }
        public Guid? SessionId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Attribute Attribute { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
