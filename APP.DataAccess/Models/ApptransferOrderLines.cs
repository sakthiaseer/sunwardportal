﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApptransferOrderLines
    {
        public long TransferOrderLineId { get; set; }
        public long? TransferOrderId { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string LotNo { get; set; }
        public string QcrefNo { get; set; }
        public string BatchNo { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public decimal? Quantity { get; set; }
        public string Uom { get; set; }
        public decimal? BaseQuantity { get; set; }
        public string BaseUom { get; set; }

        public virtual ApptransferOrder TransferOrder { get; set; }
    }
}
