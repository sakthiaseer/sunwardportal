﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFlowProgressValue
    {
        public long DynamicFlowProgressValueId { get; set; }
        public long DynamicFlowProgressId { get; set; }
        public string ColumnName { get; set; }
        public string ColumnValue { get; set; }
        public string Type { get; set; }

        public virtual DynamicFlowProgress DynamicFlowProgress { get; set; }
    }
}
