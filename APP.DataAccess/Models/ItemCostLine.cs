﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ItemCostLine
    {
        public long ItemCostLineId { get; set; }
        public long? ItemCostId { get; set; }
        public long? ItemId { get; set; }
        public string CostMethod { get; set; }
        public string CostCurrency { get; set; }
        public decimal? Cost { get; set; }
        public string Uom { get; set; }
        public string ReasonForChange { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ItemCost ItemCost { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
