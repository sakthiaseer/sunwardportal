﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CompanyCalendarLineLinkPrintUser
    {
        public CompanyCalendarLineLinkPrintUser()
        {
            CompanyCalendarLineLinkUserLink = new HashSet<CompanyCalendarLineLinkUserLink>();
        }

        public long CompanyCalendarLineLinkUserId { get; set; }
        public long? CompanyCalendarLineId { get; set; }
        public long? CompanyCalendarLineLinkId { get; set; }
        public long? UserId { get; set; }
        public long? DocumentId { get; set; }

        public virtual CompanyCalendarLine CompanyCalendarLine { get; set; }
        public virtual CompanyCalendarLineLink CompanyCalendarLineLink { get; set; }
        public virtual Documents Document { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual ICollection<CompanyCalendarLineLinkUserLink> CompanyCalendarLineLinkUserLink { get; set; }
    }
}
