﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionCapsuleLine
    {
        public long ProductionCapsuleLineId { get; set; }
        public long? ProductionCapsuleId { get; set; }
        public long? DatabseRequireId { get; set; }
        public long? NavisionId { get; set; }
        public string Buom { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProductionCapsule ProductionCapsule { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
