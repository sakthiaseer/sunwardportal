﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TaskAssigned
    {
        public long TaskAssignedId { get; set; }
        public long? TaskOwnerId { get; set; }
        public long? UserId { get; set; }
        public long? TaskId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? AssignedDate { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? CompletedDate { get; set; }
        public long? ClosedByUserId { get; set; }
        public bool? IsRead { get; set; }
        public int? StatusCodeId { get; set; }
        public long? OnBehalfId { get; set; }
        public DateTime? NewDueDate { get; set; }

        public virtual CodeMaster StatusCode { get; set; }
        public virtual TaskMaster Task { get; set; }
        public virtual ApplicationUser TaskOwner { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
