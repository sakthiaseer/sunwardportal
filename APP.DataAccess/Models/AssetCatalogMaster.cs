﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AssetCatalogMaster
    {
        public long AssetCatalogMasterId { get; set; }
        public long? CompanyId { get; set; }
        public long? AssetCatalogId { get; set; }
        public long? AssetSectionId { get; set; }
        public long? AssetSubSectionId { get; set; }
        public string AssetDescription { get; set; }
        public long? AssetModelId { get; set; }
        public string ModelNo { get; set; }
        public long? AssetUomid { get; set; }
        public string FinanaceFixAssetNo { get; set; }
        public bool? NoFinanceFixAssetNo { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? AssetGroupingId { get; set; }
        public long? AssetTypeId { get; set; }
        public long? AssetCategoryId { get; set; }
        public string AssetCatalogNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterChild AssetCategory { get; set; }
        public virtual ApplicationMasterChild AssetGrouping { get; set; }
        public virtual ApplicationMasterDetail AssetModel { get; set; }
        public virtual ApplicationMasterChild AssetType { get; set; }
        public virtual ApplicationMasterDetail AssetUom { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
