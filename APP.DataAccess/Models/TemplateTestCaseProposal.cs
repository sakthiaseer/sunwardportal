﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TemplateTestCaseProposal
    {
        public long TemplateTestCaseProposalId { get; set; }
        public long? TemplateTestCaseId { get; set; }
        public long? UserId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsByCaseCompany { get; set; }
        public string UserType { get; set; }
        public long? TemplateCaseFormId { get; set; }
        public long? ReassignUserId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationUser ReassignUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual TemplateTestCaseForm TemplateCaseForm { get; set; }
        public virtual TemplateTestCase TemplateTestCase { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
