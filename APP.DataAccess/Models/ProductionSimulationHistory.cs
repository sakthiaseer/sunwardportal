﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionSimulationHistory
    {
        public long ProductionSimulationHistoryId { get; set; }
        public long? ProductionSimulationId { get; set; }
        public long? CompanyId { get; set; }
        public string ProdOrderNo { get; set; }
        public long ItemId { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string PackSize { get; set; }
        public decimal Quantity { get; set; }
        public string Uom { get; set; }
        public decimal PerQuantity { get; set; }
        public string PerQtyUom { get; set; }
        public string BatchNo { get; set; }
        public decimal? PlannedQty { get; set; }
        public decimal? OutputQty { get; set; }
        public bool? IsOutput { get; set; }
        public DateTime StartingDate { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
    }
}
