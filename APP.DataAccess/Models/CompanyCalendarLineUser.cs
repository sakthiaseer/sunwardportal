﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CompanyCalendarLineUser
    {
        public long CompanyCalendarUserId { get; set; }
        public long? CompanyCalendarLineId { get; set; }
        public long? UserId { get; set; }
        public string TypeName { get; set; }

        public virtual CompanyCalendarLine CompanyCalendarLine { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
