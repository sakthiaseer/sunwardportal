﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionOutput
    {
        public long ProductionOutputId { get; set; }
        public string LocationName { get; set; }
        public long? ProductionEntryId { get; set; }
        public string ProductionOrderNo { get; set; }
        public string SubLotNo { get; set; }
        public string DrumNo { get; set; }
        public string Description { get; set; }
        public string BatchNo { get; set; }
        public decimal? OutputQty { get; set; }
        public string Buom { get; set; }
        public string ProductionLineNo { get; set; }
        public bool? IsPostedToNav { get; set; }
        public bool? IsLotComplete { get; set; }
        public bool? IsProdutionOrderComplete { get; set; }
        public int? StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ItemId { get; set; }
        public string ItemNo { get; set; }
        public decimal? NetWeight { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
