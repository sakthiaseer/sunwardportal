﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Ccfdimplementation
    {
        public long CcfdimplementationId { get; set; }
        public long? ClassOfDocumentId { get; set; }
        public string Hodcomments { get; set; }
        public string Hodsignature { get; set; }
        public DateTime? Hoddate { get; set; }
        public bool? IsAcceptable { get; set; }
        public bool? IsNotAcceptable { get; set; }
        public long? VerifiedBy { get; set; }
        public DateTime? VerifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterDetail ClassOfDocument { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationUser VerifiedByNavigation { get; set; }
    }
}
