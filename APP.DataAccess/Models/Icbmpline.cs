﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Icbmpline
    {
        public Icbmpline()
        {
            IcbmplineDetermineFactor = new HashSet<IcbmplineDetermineFactor>();
        }

        public long IcbmplineId { get; set; }
        public long? Icbmpid { get; set; }
        public long? ManufacturingStepsId { get; set; }
        public long? MachineGroupingId { get; set; }
        public long? MachineId { get; set; }
        public string Wilink { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Icbmp Icbmp { get; set; }
        public virtual CommonFieldsProductionMachine MachineGrouping { get; set; }
        public virtual CommonProcessLine ManufacturingSteps { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<IcbmplineDetermineFactor> IcbmplineDetermineFactor { get; set; }
    }
}
