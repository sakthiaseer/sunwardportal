﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewGetCcfimplementation
    {
        public long ApplicationMasterId { get; set; }
        public long ApplicationMasterDetailId { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public long? DoneBy { get; set; }
        public bool? IsRequired { get; set; }
        public long? ResponsibiltyTo { get; set; }
        public long? ClassOfdocumentId { get; set; }
        public DateTime? DoneByDate { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public string ResponsibilityName { get; set; }
        public string DoneByName { get; set; }
        public Guid? SessionId { get; set; }
        public long? ApplicationmasterCodeid { get; set; }
    }
}
