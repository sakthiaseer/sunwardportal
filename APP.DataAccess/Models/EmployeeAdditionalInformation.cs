﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmployeeAdditionalInformation
    {
        public long EmployeeAdditionalInfotmationId { get; set; }
        public long? EmployeeId { get; set; }
        public DateTime? ReportingDate { get; set; }
        public string SageNoSeries { get; set; }
        public string OfficeExtensionNo { get; set; }
        public string SpeedDialNo { get; set; }
        public byte[] Photo { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanySkype { get; set; }
        public string PortalLogin { get; set; }
        public string SageLogin { get; set; }
        public long? DocumentId { get; set; }

        public virtual Documents Document { get; set; }
        public virtual Employee Employee { get; set; }
    }
}
