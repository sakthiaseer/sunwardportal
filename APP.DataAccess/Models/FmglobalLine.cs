﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FmglobalLine
    {
        public FmglobalLine()
        {
            FmglobalLineItem = new HashSet<FmglobalLineItem>();
            FmglobalMoveFmglobalLine = new HashSet<FmglobalMove>();
            FmglobalMoveFmglobalLinePrevious = new HashSet<FmglobalMove>();
            InversePalletEntryNo = new HashSet<FmglobalLine>();
        }

        public long FmglobalLineId { get; set; }
        public long? FmglobalId { get; set; }
        public long? PalletEntryId { get; set; }
        public string PalletNoYear { get; set; }
        public long? PalletNoAuto { get; set; }
        public string PalletNo { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public string TempPalletNo { get; set; }
        public long? PalletEntryNoId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Fmglobal Fmglobal { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationMasterDetail PalletEntry { get; set; }
        public virtual FmglobalLine PalletEntryNo { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<FmglobalLineItem> FmglobalLineItem { get; set; }
        public virtual ICollection<FmglobalMove> FmglobalMoveFmglobalLine { get; set; }
        public virtual ICollection<FmglobalMove> FmglobalMoveFmglobalLinePrevious { get; set; }
        public virtual ICollection<FmglobalLine> InversePalletEntryNo { get; set; }
    }
}
