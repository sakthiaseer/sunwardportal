﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SalesOrderProduct
    {
        public long SalesOrderProductId { get; set; }
        public long? SalesOrderId { get; set; }
        public long? CrossReferenceProductId { get; set; }
        public long? PurchaseItemId { get; set; }
        public decimal? BalanceQty { get; set; }
        public string SpecialRemarks { get; set; }
        public decimal? QtyOrder { get; set; }
        public long? Uom { get; set; }
        public DateTime? RequestShipmentDate { get; set; }
        public decimal? QtyIntend { get; set; }
        public decimal? Kivqty { get; set; }
        public long? SalesOrderEntryId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual SocustomersItemCrossReference CrossReferenceProduct { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual PurchaseItemSalesEntryLine PurchaseItem { get; set; }
        public virtual SalesOrder SalesOrder { get; set; }
        public virtual SalesOrderEntry SalesOrderEntry { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
