﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PackagingUnitBox
    {
        public long UnitBoxId { get; set; }
        public bool? IsPrinted { get; set; }
        public int? NoOfColor { get; set; }
        public long? PantoneColorId { get; set; }
        public long? ColorId { get; set; }
        public long? BgPantoneColorId { get; set; }
        public long? BgColorId { get; set; }
        public long? LetteringId { get; set; }
        public decimal? SizeLength { get; set; }
        public decimal? SizeWidth { get; set; }
        public decimal? SizeHeight { get; set; }
        public long? PackagingMaterialId { get; set; }
        public long? PackagingItemId { get; set; }
        public long? PackingUnitId { get; set; }
        public int? UnitBoxTypeId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public decimal? Grammage { get; set; }
        public int? FinishingId { get; set; }
        public bool? IsVersion { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster Finishing { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CodeMaster UnitBoxType { get; set; }
    }
}
