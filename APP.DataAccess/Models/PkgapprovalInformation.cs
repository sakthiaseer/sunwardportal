﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PkgapprovalInformation
    {
        public PkgapprovalInformation()
        {
            PkgregisteredPackingInformation = new HashSet<PkgregisteredPackingInformation>();
        }

        public long PkgapprovalInformationId { get; set; }
        public long? PlantId { get; set; }
        public long? ProductGroupCodeId { get; set; }
        public long? ItemId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Plant Plant { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<PkgregisteredPackingInformation> PkgregisteredPackingInformation { get; set; }
    }
}
