﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmailTransferHistory
    {
        public long TransferId { get; set; }
        public long FromUserId { get; set; }
        public long ToUserId { get; set; }
        public long? EmailConversationId { get; set; }
        public long? TopicId { get; set; }
        public DateTime? TransferDate { get; set; }
        public int? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
    }
}
