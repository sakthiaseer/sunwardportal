﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WorkList
    {
        public WorkList()
        {
            Smecomment = new HashSet<Smecomment>();
            TaskAcknowledgement = new HashSet<TaskAcknowledgement>();
            WorkListItem = new HashSet<WorkListItem>();
        }

        public long WorkListId { get; set; }
        public string Title { get; set; }
        public int? RequestType { get; set; }
        public DateTime? AssignedDate { get; set; }
        public long? AssignedBy { get; set; }
        public long? AssignedTo { get; set; }
        public int? WorkListType { get; set; }
        public DateTime? DueDate { get; set; }
        public string Notes { get; set; }
        public string SearchDocument { get; set; }
        public string Recurrance { get; set; }
        public long? DocumentId { get; set; }
        public DateTime? ExpectedCompetionDate { get; set; }
        public int? AssignmentMethod { get; set; }
        public string ReasonforDiscontinue { get; set; }
        public int? WorkListStatus { get; set; }
        public string Version { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser AssignedByNavigation { get; set; }
        public virtual ApplicationUser AssignedToNavigation { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster RequestTypeNavigation { get; set; }
        public virtual CodeMaster WorkListStatusNavigation { get; set; }
        public virtual CodeMaster WorkListTypeNavigation { get; set; }
        public virtual ICollection<Smecomment> Smecomment { get; set; }
        public virtual ICollection<TaskAcknowledgement> TaskAcknowledgement { get; set; }
        public virtual ICollection<WorkListItem> WorkListItem { get; set; }
    }
}
