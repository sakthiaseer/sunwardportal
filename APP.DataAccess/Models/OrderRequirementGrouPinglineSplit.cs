﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class OrderRequirementGrouPinglineSplit
    {
        public long OrderRequirementGrouPinglineSplitId { get; set; }
        public long? OrderRequirementGroupingLineId { get; set; }
        public decimal? ProductQty { get; set; }
        public long? SplitProductId { get; set; }
        public decimal? SplitProductQty { get; set; }
        public string Remarks { get; set; }
        public bool? IsNavSync { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual OrderRequirementGroupingLine OrderRequirementGroupingLine { get; set; }
        public virtual Navitems SplitProduct { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
