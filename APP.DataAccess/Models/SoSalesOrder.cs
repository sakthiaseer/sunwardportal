﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SoSalesOrder
    {
        public SoSalesOrder()
        {
            SoSalesOrderLine = new HashSet<SoSalesOrderLine>();
        }

        public long SoSalesOrderId { get; set; }
        public DateTime? DocumentDate { get; set; }
        public string SignOrderNo { get; set; }
        public DateTime? OrderDate { get; set; }
        public long? SoCustomerId { get; set; }
        public DateTime? PrioityDeliveryDate { get; set; }
        public string Remark { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? StatusCodeId { get; set; }
        public Guid? SessionId { get; set; }
        public long? SoCustomerBillingAddressId { get; set; }
        public long? SoCustomerShipingAddressId { get; set; }
        public long? CompanyId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual SoCustomer SoCustomer { get; set; }
        public virtual SoCustomerAddress SoCustomerBillingAddress { get; set; }
        public virtual SoCustomerAddress SoCustomerShipingAddress { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<SoSalesOrderLine> SoSalesOrderLine { get; set; }
    }
}
