﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PortfolioLine
    {
        public long PortfolioLineId { get; set; }
        public long? PortfolioId { get; set; }
        public string NameOfReferenceType { get; set; }
        public long? WikiReferenceId { get; set; }
        public string VersionNo { get; set; }
        public Guid? SessionId { get; set; }
        public string Remarks { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ChileMasterId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterDetail ChileMaster { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Portfolio Portfolio { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationWiki WikiReference { get; set; }
    }
}
