﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TemplateTestCaseCheckListResponseWeekly
    {
        public long TemplateTestCaseCheckListResponseWeeklyId { get; set; }
        public long? TemplateTestCaseCheckListResponseId { get; set; }
        public int? WeeklyId { get; set; }
        public string CustomType { get; set; }

        public virtual TemplateTestCaseCheckListResponse TemplateTestCaseCheckListResponse { get; set; }
        public virtual CodeMaster Weekly { get; set; }
    }
}
