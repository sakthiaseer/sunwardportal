﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class StandardManufacturingProcess
    {
        public StandardManufacturingProcess()
        {
            ManpowerInformation = new HashSet<ManpowerInformation>();
            OperationSequence = new HashSet<OperationSequence>();
            StandardManufacturingProcessLine = new HashSet<StandardManufacturingProcessLine>();
            StandardProcedure = new HashSet<StandardProcedure>();
        }

        public long StandardManufacturingProcessId { get; set; }
        public long? ManufacturingSiteId { get; set; }
        public long? ProfileId { get; set; }
        public long? MethodTempalteId { get; set; }
        public string MethodTemplateName { get; set; }
        public string MachineGroupName { get; set; }
        public string TemplateName { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual MethodTemplateRoutine MethodTempalte { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ManpowerInformation> ManpowerInformation { get; set; }
        public virtual ICollection<OperationSequence> OperationSequence { get; set; }
        public virtual ICollection<StandardManufacturingProcessLine> StandardManufacturingProcessLine { get; set; }
        public virtual ICollection<StandardProcedure> StandardProcedure { get; set; }
    }
}
