﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionCycleTicket
    {
        public long ProdCycleAllocationId { get; set; }
        public long? ProdCycleId { get; set; }
        public string ItemCardNo { get; set; }
        public string ItemName { get; set; }
        public string PerItemUnit { get; set; }
        public decimal? Allocation { get; set; }
        public decimal? QtyPerTicket { get; set; }
        public string Navbom { get; set; }

        public virtual ProductionCycle ProdCycle { get; set; }
    }
}
