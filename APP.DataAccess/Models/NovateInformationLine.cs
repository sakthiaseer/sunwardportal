﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NovateInformationLine
    {
        public long NovateInformationLineId { get; set; }
        public long? TenderInformationId { get; set; }
        public long? NovateFromId { get; set; }
        public long? NovateToId { get; set; }
        public decimal? Quantity { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CompanyListing NovateFrom { get; set; }
        public virtual CompanyListing NovateTo { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual TenderInformation TenderInformation { get; set; }
    }
}
