﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AppsupervisorDispensingEntry
    {
        public long SupervisorDispensingId { get; set; }
        public bool? PreLineClearance { get; set; }
        public string WeighingMachine { get; set; }
        public string WorkOrderNo { get; set; }
        public string ProdOrderNo { get; set; }
        public int? ProdLineNo { get; set; }
        public string ItemName { get; set; }
        public string LotNo { get; set; }
        public string SubLotNo { get; set; }
        public string JobNo { get; set; }
        public string Uom { get; set; }
        public string QcrefNo { get; set; }
        public string DrumNo { get; set; }
        public decimal? DrumWeight { get; set; }
        public bool? PostedtoNav { get; set; }
        public string Company { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
