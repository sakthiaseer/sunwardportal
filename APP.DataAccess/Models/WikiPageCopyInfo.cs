﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WikiPageCopyInfo
    {
        public long PageCopyId { get; set; }
        public long? PageId { get; set; }
        public string CopyNo { get; set; }
        public long? Department { get; set; }
        public long? LocationId { get; set; }
        public string SessionId { get; set; }

        public virtual Department DepartmentNavigation { get; set; }
        public virtual Locations Location { get; set; }
        public virtual WikiPage Page { get; set; }
    }
}
