﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Division
    {
        public Division()
        {
            ApplicationWiki = new HashSet<ApplicationWiki>();
            Department = new HashSet<Department>();
            DraftApplicationWiki = new HashSet<DraftApplicationWiki>();
            Employee = new HashSet<Employee>();
            EmployeeInformation = new HashSet<EmployeeInformation>();
            LevelMaster = new HashSet<LevelMaster>();
        }

        public long DivisionId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public long? CompanyId { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ApplicationWiki> ApplicationWiki { get; set; }
        public virtual ICollection<Department> Department { get; set; }
        public virtual ICollection<DraftApplicationWiki> DraftApplicationWiki { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
        public virtual ICollection<EmployeeInformation> EmployeeInformation { get; set; }
        public virtual ICollection<LevelMaster> LevelMaster { get; set; }
    }
}
