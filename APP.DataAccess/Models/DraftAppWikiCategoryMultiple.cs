﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DraftAppWikiCategoryMultiple
    {
        public long AppWikiCategoryMultipleId { get; set; }
        public long? ApplicationWikiId { get; set; }
        public long? WikiCategoryId { get; set; }

        public virtual DraftApplicationWiki ApplicationWiki { get; set; }
        public virtual ApplicationMasterChild WikiCategory { get; set; }
    }
}
