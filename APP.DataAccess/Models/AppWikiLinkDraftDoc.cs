﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AppWikiLinkDraftDoc
    {
        public long AppWikiLinkDraftDocId { get; set; }
        public long? DocumentLinkId { get; set; }
        public string Type { get; set; }
        public long? ApplicationWikiDraftId { get; set; }
        public long? ApplicationWikiId { get; set; }

        public virtual ApplicationWiki ApplicationWiki { get; set; }
        public virtual DraftApplicationWiki ApplicationWikiDraft { get; set; }
        public virtual DocumentLink DocumentLink { get; set; }
    }
}
