﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WorkFlowUserGroupUsers
    {
        public long WorkFlowGroupUserId { get; set; }
        public long? WorkFlowUserGroupId { get; set; }
        public long? UserId { get; set; }
        public int? Sequence { get; set; }

        public virtual ApplicationUser User { get; set; }
        public virtual WorkFlowUserGroup WorkFlowUserGroup { get; set; }
    }
}
