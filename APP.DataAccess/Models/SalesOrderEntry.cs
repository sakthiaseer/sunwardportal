﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SalesOrderEntry
    {
        public SalesOrderEntry()
        {
            BlanketOrder = new HashSet<BlanketOrder>();
            InverseBlanketOrderNo = new HashSet<SalesOrderEntry>();
            InverseTagAlongTenderNo = new HashSet<SalesOrderEntry>();
            PurchaseItemSalesEntryLine = new HashSet<PurchaseItemSalesEntryLine>();
            SaleAdhocNovateOrder = new HashSet<SaleAdhocNovateOrder>();
            SalesAdhocNovate = new HashSet<SalesAdhocNovate>();
            SalesOrderProduct = new HashSet<SalesOrderProduct>();
            TenderInformation = new HashSet<TenderInformation>();
            TenderPeriodPricing = new HashSet<TenderPeriodPricing>();
            TransferBalanceQtyContract = new HashSet<TransferBalanceQty>();
            TransferBalanceQtyTransferToContract = new HashSet<TransferBalanceQty>();
        }

        public long SalesOrderEntryId { get; set; }
        public long? ProfileId { get; set; }
        public long? CompanyId { get; set; }
        public int? OrderById { get; set; }
        public long? CustomerId { get; set; }
        public long? TenderAgencyId { get; set; }
        public int? TypeOfOrderId { get; set; }
        public string TagAlongOrderNo { get; set; }
        public string OrderNo { get; set; }
        public DateTime? OrderPeriodFrom { get; set; }
        public DateTime? OrderPeriodTo { get; set; }
        public bool? IsMultipleQuatationNo { get; set; }
        public bool? IsExtensionInfoMultiple { get; set; }
        public bool? IsAllowExtension { get; set; }
        public bool? IsEachDistribution { get; set; }
        public int? ExtensionMonth { get; set; }
        public int? ExtensionQuantity { get; set; }
        public bool? IsTagAlongWithTenderAgency { get; set; }
        public bool? IsWithOrderPeriod { get; set; }
        public Guid? SessionId { get; set; }
        public string OrderEntryType { get; set; }
        public long? TagAlongCustomerId { get; set; }
        public long? TagAlongTenderId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileReferenceNo { get; set; }
        public int? TypeOfSalesOrderId { get; set; }
        public string QuotationNo { get; set; }
        public long? BlanketOrderNoId { get; set; }
        public DateTime? DateOfOrder { get; set; }
        public string Ponumber { get; set; }
        public long? TagAlongTenderNoId { get; set; }
        public long? BlanketItemId { get; set; }
        public long? CrossReferenceId { get; set; }
        public long? ProductItemId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems BlanketItem { get; set; }
        public virtual SalesOrderEntry BlanketOrderNo { get; set; }
        public virtual Plant Company { get; set; }
        public virtual SocustomersItemCrossReference CrossReference { get; set; }
        public virtual CompanyListing Customer { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster OrderBy { get; set; }
        public virtual PurchaseItemSalesEntryLine ProductItem { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CompanyListing TagAlongCustomer { get; set; }
        public virtual SalesOrderEntry TagAlongTenderNo { get; set; }
        public virtual CodeMaster TypeOfOrder { get; set; }
        public virtual CodeMaster TypeOfSalesOrder { get; set; }
        public virtual ICollection<BlanketOrder> BlanketOrder { get; set; }
        public virtual ICollection<SalesOrderEntry> InverseBlanketOrderNo { get; set; }
        public virtual ICollection<SalesOrderEntry> InverseTagAlongTenderNo { get; set; }
        public virtual ICollection<PurchaseItemSalesEntryLine> PurchaseItemSalesEntryLine { get; set; }
        public virtual ICollection<SaleAdhocNovateOrder> SaleAdhocNovateOrder { get; set; }
        public virtual ICollection<SalesAdhocNovate> SalesAdhocNovate { get; set; }
        public virtual ICollection<SalesOrderProduct> SalesOrderProduct { get; set; }
        public virtual ICollection<TenderInformation> TenderInformation { get; set; }
        public virtual ICollection<TenderPeriodPricing> TenderPeriodPricing { get; set; }
        public virtual ICollection<TransferBalanceQty> TransferBalanceQtyContract { get; set; }
        public virtual ICollection<TransferBalanceQty> TransferBalanceQtyTransferToContract { get; set; }
    }
}
