﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CompanyHolidayDetail
    {
        public long CompanyHolidayDetailId { get; set; }
        public long? CompanyHolidayId { get; set; }
        public long? HolidayId { get; set; }
        public int? HolidayTypeId { get; set; }
        public int? AllowEarlyReleaseId { get; set; }
        public DateTime? EarlyRelease { get; set; }
        public bool? IsCompulsory { get; set; }
        public bool? IsCompanyChoose { get; set; }
        public DateTime? HolidayDate { get; set; }

        public virtual CodeMaster AllowEarlyRelease { get; set; }
        public virtual CompanyHolidays CompanyHoliday { get; set; }
        public virtual HolidayMaster Holiday { get; set; }
        public virtual CodeMaster HolidayType { get; set; }
    }
}
