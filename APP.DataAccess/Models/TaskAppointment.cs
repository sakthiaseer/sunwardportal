﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TaskAppointment
    {
        public long TaskAppointmentId { get; set; }
        public long? TaskMasterId { get; set; }
        public DateTime? DiscussionDate { get; set; }
        public string DiscussionNotes { get; set; }
        public long? SubTaskId { get; set; }
        public long? AppointmentBy { get; set; }
        public DateTime? ClosedDate { get; set; }
        public int? StatusCodeId { get; set; }

        public virtual ApplicationUser AppointmentByNavigation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual TaskMaster SubTask { get; set; }
        public virtual TaskMaster TaskMaster { get; set; }
    }
}
