﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DocumentShareUser
    {
        public long DocumentShareUserId { get; set; }
        public long? DocumentShareId { get; set; }
        public long? UserId { get; set; }

        public virtual DocumentShare DocumentShare { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
