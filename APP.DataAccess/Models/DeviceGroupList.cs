﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DeviceGroupList
    {
        public DeviceGroupList()
        {
            DeviceCatalogMaster = new HashSet<DeviceCatalogMaster>();
            DeviceGroupCatalog = new HashSet<DeviceGroupCatalog>();
        }

        public long DeviceGroupListId { get; set; }
        public string DeviceGroupNo { get; set; }
        public string DeviceGroupName { get; set; }
        public string Appreviation { get; set; }
        public string Uom { get; set; }
        public bool? IsRequire { get; set; }
        public long? CalibrationTypeId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CalibrationType CalibrationType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<DeviceCatalogMaster> DeviceCatalogMaster { get; set; }
        public virtual ICollection<DeviceGroupCatalog> DeviceGroupCatalog { get; set; }
    }
}
