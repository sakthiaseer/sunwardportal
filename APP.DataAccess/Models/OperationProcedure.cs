﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class OperationProcedure
    {
        public OperationProcedure()
        {
            OperationProcedureDosage = new HashSet<OperationProcedureDosage>();
            OperationProcedureLine = new HashSet<OperationProcedureLine>();
        }

        public long OperationProcedureId { get; set; }
        public long? DosageFormId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<OperationProcedureDosage> OperationProcedureDosage { get; set; }
        public virtual ICollection<OperationProcedureLine> OperationProcedureLine { get; set; }
    }
}
