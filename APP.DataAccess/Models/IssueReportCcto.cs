﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class IssueReportCcto
    {
        public long IpircctoId { get; set; }
        public long? Ipirid { get; set; }
        public long? CctoId { get; set; }

        public virtual UserGroup Ccto { get; set; }
        public virtual IssueReportIpir Ipir { get; set; }
    }
}
