﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApplicationMasterChild
    {
        public ApplicationMasterChild()
        {
            AddressCityNavigation = new HashSet<Address>();
            AddressCountryNavigation = new HashSet<Address>();
            AddressState = new HashSet<Address>();
            AppWikiCategoryMultiple = new HashSet<AppWikiCategoryMultiple>();
            AppWikiTopicMultiple = new HashSet<AppWikiTopicMultiple>();
            AssetCatalogMasterAssetCategory = new HashSet<AssetCatalogMaster>();
            AssetCatalogMasterAssetGrouping = new HashSet<AssetCatalogMaster>();
            AssetCatalogMasterAssetType = new HashSet<AssetCatalogMaster>();
            AssetMasterCategory = new HashSet<AssetMaster>();
            AssetMasterMake = new HashSet<AssetMaster>();
            AssetMasterSubCategory = new HashSet<AssetMaster>();
            AssetPartsMaintenaceMasterAssetCatalog = new HashSet<AssetPartsMaintenaceMaster>();
            AssetPartsMaintenaceMasterSection = new HashSet<AssetPartsMaintenaceMaster>();
            AssetPartsMaintenaceMasterSubSection = new HashSet<AssetPartsMaintenaceMaster>();
            DraftAppWikiCategoryMultiple = new HashSet<DraftAppWikiCategoryMultiple>();
            DraftAppWikiTopicMultiple = new HashSet<DraftAppWikiTopicMultiple>();
            DynamicFormItemDepartmentAppParent = new HashSet<DynamicFormItem>();
            DynamicFormItemItemGroupAppParent = new HashSet<DynamicFormItem>();
            DynamicFormItemStockGroupAppParent = new HashSet<DynamicFormItem>();
            InverseParent = new HashSet<ApplicationMasterChild>();
            IpirReport = new HashSet<IpirReport>();
            JobProgressTemplateJobCategory = new HashSet<JobProgressTemplate>();
            JobProgressTemplateJobType = new HashSet<JobProgressTemplate>();
            ProcessMachineTimeProductionLine = new HashSet<ProcessMachineTimeProductionLine>();
            ProcessMachineTimeProductionLineLine = new HashSet<ProcessMachineTimeProductionLineLine>();
            ProductActivityCaseAction = new HashSet<ProductActivityCase>();
            ProductActivityCaseActionMultiple = new HashSet<ProductActivityCaseActionMultiple>();
            ProductActivityCaseCategoryAction = new HashSet<ProductActivityCase>();
            ProductActivityCaseCategoryMultiple = new HashSet<ProductActivityCaseCategoryMultiple>();
            ProductActivityCaseLineProdActivityActionChild = new HashSet<ProductActivityCaseLine>();
            ProductActivityCaseLineProdActivityCategoryChild = new HashSet<ProductActivityCaseLine>();
            ProductActivityCaseManufacturingProcessChild = new HashSet<ProductActivityCase>();
            ProductionActivityAppLineManufacturingProcessChild = new HashSet<ProductionActivityAppLine>();
            ProductionActivityAppLineProdActivityActionChildDNavigation = new HashSet<ProductionActivityAppLine>();
            ProductionActivityAppLineProdActivityCategoryChild = new HashSet<ProductionActivityAppLine>();
            ProductionActivityMasterAction = new HashSet<ProductionActivityMaster>();
            ProductionActivityMasterCategoryAction = new HashSet<ProductionActivityMaster>();
            ProductionActivityMasterLineProdActivityActionChild = new HashSet<ProductionActivityMasterLine>();
            ProductionActivityMasterLineProdActivityCategoryChild = new HashSet<ProductionActivityMasterLine>();
            ProductionActivityMasterManufacturingProcess = new HashSet<ProductionActivityMaster>();
            ProductionActivityPlanningApp = new HashSet<ProductionActivityPlanningApp>();
            ProductionActivityPlanningAppLineManufacturingProcessChild = new HashSet<ProductionActivityPlanningAppLine>();
            ProductionActivityPlanningAppLineProdActivityActionChildDNavigation = new HashSet<ProductionActivityPlanningAppLine>();
            ProductionActivityPlanningAppLineProdActivityCategoryChild = new HashSet<ProductionActivityPlanningAppLine>();
            ProductionActivityRoutineApp = new HashSet<ProductionActivityRoutineApp>();
            ProductionActivityRoutineAppLineManufacturingProcessChild = new HashSet<ProductionActivityRoutineAppLine>();
            ProductionActivityRoutineAppLineProdActivityActionChildDNavigation = new HashSet<ProductionActivityRoutineAppLine>();
            ProductionActivityRoutineAppLineProdActivityCategoryChild = new HashSet<ProductionActivityRoutineAppLine>();
        }

        public long ApplicationMasterChildId { get; set; }
        public long? ApplicationMasterParentId { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public long? ParentId { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterParent ApplicationMasterParent { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationMasterChild Parent { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<Address> AddressCityNavigation { get; set; }
        public virtual ICollection<Address> AddressCountryNavigation { get; set; }
        public virtual ICollection<Address> AddressState { get; set; }
        public virtual ICollection<AppWikiCategoryMultiple> AppWikiCategoryMultiple { get; set; }
        public virtual ICollection<AppWikiTopicMultiple> AppWikiTopicMultiple { get; set; }
        public virtual ICollection<AssetCatalogMaster> AssetCatalogMasterAssetCategory { get; set; }
        public virtual ICollection<AssetCatalogMaster> AssetCatalogMasterAssetGrouping { get; set; }
        public virtual ICollection<AssetCatalogMaster> AssetCatalogMasterAssetType { get; set; }
        public virtual ICollection<AssetMaster> AssetMasterCategory { get; set; }
        public virtual ICollection<AssetMaster> AssetMasterMake { get; set; }
        public virtual ICollection<AssetMaster> AssetMasterSubCategory { get; set; }
        public virtual ICollection<AssetPartsMaintenaceMaster> AssetPartsMaintenaceMasterAssetCatalog { get; set; }
        public virtual ICollection<AssetPartsMaintenaceMaster> AssetPartsMaintenaceMasterSection { get; set; }
        public virtual ICollection<AssetPartsMaintenaceMaster> AssetPartsMaintenaceMasterSubSection { get; set; }
        public virtual ICollection<DraftAppWikiCategoryMultiple> DraftAppWikiCategoryMultiple { get; set; }
        public virtual ICollection<DraftAppWikiTopicMultiple> DraftAppWikiTopicMultiple { get; set; }
        public virtual ICollection<DynamicFormItem> DynamicFormItemDepartmentAppParent { get; set; }
        public virtual ICollection<DynamicFormItem> DynamicFormItemItemGroupAppParent { get; set; }
        public virtual ICollection<DynamicFormItem> DynamicFormItemStockGroupAppParent { get; set; }
        public virtual ICollection<ApplicationMasterChild> InverseParent { get; set; }
        public virtual ICollection<IpirReport> IpirReport { get; set; }
        public virtual ICollection<JobProgressTemplate> JobProgressTemplateJobCategory { get; set; }
        public virtual ICollection<JobProgressTemplate> JobProgressTemplateJobType { get; set; }
        public virtual ICollection<ProcessMachineTimeProductionLine> ProcessMachineTimeProductionLine { get; set; }
        public virtual ICollection<ProcessMachineTimeProductionLineLine> ProcessMachineTimeProductionLineLine { get; set; }
        public virtual ICollection<ProductActivityCase> ProductActivityCaseAction { get; set; }
        public virtual ICollection<ProductActivityCaseActionMultiple> ProductActivityCaseActionMultiple { get; set; }
        public virtual ICollection<ProductActivityCase> ProductActivityCaseCategoryAction { get; set; }
        public virtual ICollection<ProductActivityCaseCategoryMultiple> ProductActivityCaseCategoryMultiple { get; set; }
        public virtual ICollection<ProductActivityCaseLine> ProductActivityCaseLineProdActivityActionChild { get; set; }
        public virtual ICollection<ProductActivityCaseLine> ProductActivityCaseLineProdActivityCategoryChild { get; set; }
        public virtual ICollection<ProductActivityCase> ProductActivityCaseManufacturingProcessChild { get; set; }
        public virtual ICollection<ProductionActivityAppLine> ProductionActivityAppLineManufacturingProcessChild { get; set; }
        public virtual ICollection<ProductionActivityAppLine> ProductionActivityAppLineProdActivityActionChildDNavigation { get; set; }
        public virtual ICollection<ProductionActivityAppLine> ProductionActivityAppLineProdActivityCategoryChild { get; set; }
        public virtual ICollection<ProductionActivityMaster> ProductionActivityMasterAction { get; set; }
        public virtual ICollection<ProductionActivityMaster> ProductionActivityMasterCategoryAction { get; set; }
        public virtual ICollection<ProductionActivityMasterLine> ProductionActivityMasterLineProdActivityActionChild { get; set; }
        public virtual ICollection<ProductionActivityMasterLine> ProductionActivityMasterLineProdActivityCategoryChild { get; set; }
        public virtual ICollection<ProductionActivityMaster> ProductionActivityMasterManufacturingProcess { get; set; }
        public virtual ICollection<ProductionActivityPlanningApp> ProductionActivityPlanningApp { get; set; }
        public virtual ICollection<ProductionActivityPlanningAppLine> ProductionActivityPlanningAppLineManufacturingProcessChild { get; set; }
        public virtual ICollection<ProductionActivityPlanningAppLine> ProductionActivityPlanningAppLineProdActivityActionChildDNavigation { get; set; }
        public virtual ICollection<ProductionActivityPlanningAppLine> ProductionActivityPlanningAppLineProdActivityCategoryChild { get; set; }
        public virtual ICollection<ProductionActivityRoutineApp> ProductionActivityRoutineApp { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLine> ProductionActivityRoutineAppLineManufacturingProcessChild { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLine> ProductionActivityRoutineAppLineProdActivityActionChildDNavigation { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLine> ProductionActivityRoutineAppLineProdActivityCategoryChild { get; set; }
    }
}
