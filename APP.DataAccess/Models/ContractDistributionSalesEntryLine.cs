﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ContractDistributionSalesEntryLine
    {
        public ContractDistributionSalesEntryLine()
        {
            ProjectedDeliverySalesOrderLine = new HashSet<ProjectedDeliverySalesOrderLine>();
        }

        public long ContractDistributionSalesEntryLineId { get; set; }
        public long? SocustomerId { get; set; }
        public long? PurchaseItemSalesEntryLineId { get; set; }
        public decimal? TotalQty { get; set; }
        public long? Uomid { get; set; }
        public int? NoOfLots { get; set; }
        public string Ponumber { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? FrequencyId { get; set; }
        public int? PerFrequencyQty { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual PurchaseItemSalesEntryLine PurchaseItemSalesEntryLine { get; set; }
        public virtual CompanyListing Socustomer { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ProjectedDeliverySalesOrderLine> ProjectedDeliverySalesOrderLine { get; set; }
    }
}
