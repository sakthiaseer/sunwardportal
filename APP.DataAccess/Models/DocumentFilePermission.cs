﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DocumentFilePermission
    {
        public long FilePermissionId { get; set; }
        public long? FileProfileTypeId { get; set; }
        public Guid? SessionId { get; set; }
        public long DocumentRoleId { get; set; }
    }
}
