﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Attribute
    {
        public Attribute()
        {
            AttributeDetail = new HashSet<AttributeDetail>();
        }

        public int AttributeId { get; set; }
        public string Description { get; set; }
        public int? ControlType { get; set; }
        public string EntryMask { get; set; }
        public string RegExp { get; set; }
        public string ListDefault { get; set; }
        public bool? IsInternal { get; set; }
        public bool? ContainsPersonalData { get; set; }
        public Guid? SessionId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<AttributeDetail> AttributeDetail { get; set; }
    }
}
