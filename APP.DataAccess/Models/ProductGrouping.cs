﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductGrouping
    {
        public long ProductGroupingId { get; set; }
        public long? CompanyId { get; set; }
        public string ProductGroupCode { get; set; }
        public long? ProductNameId { get; set; }
        public long? PackingInforId { get; set; }
        public long? QuotationUnitsId { get; set; }
        public long? SupplyToId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CompanyListing SupplyTo { get; set; }
    }
}
