﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewGetRecordVariation
    {
        public long RecordVariationId { get; set; }
        public string RelatedChangeControlNo { get; set; }
        public DateTime? SubmissionDate { get; set; }
        public long? RegistrationHolderId { get; set; }
        public DateTime? SubmittedPaidDate { get; set; }
        public DateTime? EstimateApprovalDate { get; set; }
        public long? ProductId { get; set; }
        public string VariationNo { get; set; }
        public int? RegisterationCodeId { get; set; }
        public int? SubmissionStatusId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public long? ModifiedByUserId { get; set; }
        public string DocumentLink { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? EstimateSubmissionDate { get; set; }
        public Guid? SessionId { get; set; }
        public long? RegisterCountryId { get; set; }
        public string ProductName { get; set; }
        public long? ManufacturingSiteId { get; set; }
        public string ManufacturingSite { get; set; }
        public long? RegisterCountry { get; set; }
        public string RegisterCountryName { get; set; }
        public long? PrhspecificProductId { get; set; }
        public string PrhspecificName { get; set; }
        public long? RegisterProductOwnerId { get; set; }
        public string RegisterProductOwner { get; set; }
        public string RegistrationHolderName { get; set; }
        public string AddedByUser { get; set; }
        public string ModifiedByUser { get; set; }
        public string StatusCode { get; set; }
        public string RegisterationCode { get; set; }
        public string SubmissionStatus { get; set; }
        public string VariationStatus { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public string CorrespondenceLink { get; set; }
        public string VariationForm { get; set; }
    }
}
