﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormReport
    {
        public long DynamicFormReportId { get; set; }
        public long? DynamicFormId { get; set; }
        public string FileName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid? SessionId { get; set; }
        public string FilePath { get; set; }
        public string ContentType { get; set; }
        public long? FileSize { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDeleted { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual DynamicForm DynamicForm { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
