﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionActivityMaster
    {
        public ProductionActivityMaster()
        {
            ProductionActivityMasterLine = new HashSet<ProductionActivityMasterLine>();
            ProductionActivityMasterRespons = new HashSet<ProductionActivityMasterRespons>();
        }

        public long ProductionActivityMasterId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public long? CategoryActionId { get; set; }
        public long? ActionId { get; set; }
        public string Reference { get; set; }
        public string CheckerNotes { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public string Upload { get; set; }
        public int? ProductionTypeId { get; set; }

        public virtual ApplicationMasterChild Action { get; set; }
        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterChild CategoryAction { get; set; }
        public virtual ApplicationMasterChild ManufacturingProcess { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster ProductionType { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ProductionActivityMasterLine> ProductionActivityMasterLine { get; set; }
        public virtual ICollection<ProductionActivityMasterRespons> ProductionActivityMasterRespons { get; set; }
    }
}
