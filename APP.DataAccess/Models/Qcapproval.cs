﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Qcapproval
    {
        public Qcapproval()
        {
            QcapprovalLine = new HashSet<QcapprovalLine>();
        }

        public long QcapprovalId { get; set; }
        public string InspectionNo { get; set; }
        public bool? IsRetest { get; set; }
        public long? TestNameId { get; set; }
        public int? StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<QcapprovalLine> QcapprovalLine { get; set; }
    }
}
