﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TaskNotes
    {
        public long TaskNotesId { get; set; }
        public long? TaskId { get; set; }
        public string Notes { get; set; }
        public DateTime? RemainderDate { get; set; }
        public long? TaskUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsNoReminderDate { get; set; }

        public virtual TaskMaster Task { get; set; }
        public virtual ApplicationUser TaskUser { get; set; }
    }
}
