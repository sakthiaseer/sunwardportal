﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NonDeliverSo
    {
        public long NonDeliverSoid { get; set; }
        public long? NavSalesOrderLineId { get; set; }
        public bool? IsChangeBalanceQty { get; set; }
        public decimal? NewBalanceQty { get; set; }
        public long? ReasonId { get; set; }
        public string ReasonDescription { get; set; }
        public DateTime? NewShipmentDate { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsLatest { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual NavSalesOrderLine NavSalesOrderLine { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
