﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DiscussionNotes
    {
        public long DiscussionNotesId { get; set; }
        public long? TaskId { get; set; }
        public string DiscussionNotes1 { get; set; }
        public DateTime? DiscussionDate { get; set; }
        public long? TaskUserId { get; set; }

        public TaskMaster Task { get; set; }
        public ApplicationUser TaskUser { get; set; }
    }
}
