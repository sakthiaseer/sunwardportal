﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SellingPriceInformation
    {
        public SellingPriceInformation()
        {
            SellingPricingTiers = new HashSet<SellingPricingTiers>();
        }

        public long SellingPriceInformationId { get; set; }
        public long? SellingCatalogueId { get; set; }
        public long? ManufacturingId { get; set; }
        public long? ProductId { get; set; }
        public long? PricingMethodId { get; set; }
        public long? BonusCurrencyId { get; set; }
        public decimal? BonusSellingPrice { get; set; }
        public long? Quantity { get; set; }
        public decimal? Bonus { get; set; }
        public long? MinCurrencyId { get; set; }
        public decimal? MinPrice { get; set; }
        public decimal? MinQty { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? GenericCodeSupplyToMultipleId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterDetail BonusCurrency { get; set; }
        public virtual GenericCodeSupplyToMultiple GenericCodeSupplyToMultiple { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationMasterDetail PricingMethod { get; set; }
        public virtual GenericCodes Product { get; set; }
        public virtual SellingCatalogue SellingCatalogue { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<SellingPricingTiers> SellingPricingTiers { get; set; }
    }
}
