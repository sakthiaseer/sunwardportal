﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class VendorSourceList
    {
        public long VendorSourceListId { get; set; }
        public long? VendorsListId { get; set; }
        public long? SourceListId { get; set; }

        public virtual SourceList SourceList { get; set; }
        public virtual VendorsList VendorsList { get; set; }
    }
}
