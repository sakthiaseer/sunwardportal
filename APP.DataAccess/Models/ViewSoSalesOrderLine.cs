﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewSoSalesOrderLine
    {
        public long SoSalesOrderLineId { get; set; }
        public long? SoSalesOrderId { get; set; }
        public string ItemSerialNo { get; set; }
        public decimal? Qty { get; set; }
        public decimal? UnitPrice { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string StatusCode { get; set; }
        public string AddedBy { get; set; }
        public string ModifiedBy { get; set; }
        public int? CodeId { get; set; }
        public string No { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string BaseUnitofMeasure { get; set; }
        public long? ItemId { get; set; }
        public string PlantCode { get; set; }
        public string NavCompanyName { get; set; }
        public string NavCompany { get; set; }
        public string ShipCode { get; set; }
        public long? SellingMethodId { get; set; }
        public decimal? OrderBounsQty { get; set; }
        public bool? IsManual { get; set; }
        public decimal? ManualPrice { get; set; }
        public string PricingType { get; set; }
        public string SellingMethod { get; set; }
    }
}
