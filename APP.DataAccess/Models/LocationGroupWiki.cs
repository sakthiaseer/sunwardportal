﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class LocationGroupWiki
    {
        public long LocationGroupWikiId { get; set; }
        public long? LocationGroupId { get; set; }
        public long LocationWikiId { get; set; }

        public virtual LocationGroup LocationGroup { get; set; }
        public virtual WikiPage LocationWiki { get; set; }
    }
}
