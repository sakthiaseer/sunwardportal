﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class RecordVariationLine
    {
        public RecordVariationLine()
        {
            RecordVariationDocumentLink = new HashSet<RecordVariationDocumentLink>();
        }

        public long RecordVariationLineId { get; set; }
        public long? RecordVariationId { get; set; }
        public long? VariationCodeId { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public DateTime? ProjectApprovalDate { get; set; }
        public int? StatusCodeId { get; set; }
        public string VariationForm { get; set; }
        public string CorrespondenceLink { get; set; }
        public long? RegistrationVariationId { get; set; }

        public virtual RecordVariation RecordVariation { get; set; }
        public virtual RegistrationVariation RegistrationVariation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<RecordVariationDocumentLink> RecordVariationDocumentLink { get; set; }
    }
}
