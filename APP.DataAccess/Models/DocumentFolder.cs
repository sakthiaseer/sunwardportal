﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DocumentFolder
    {
        public long DocumentFolderId { get; set; }
        public long? FolderId { get; set; }
        public long? DocumentId { get; set; }
        public long? PreviousDocumentId { get; set; }
        public bool? IsLatest { get; set; }
        public bool? IsLocked { get; set; }
        public string VersionNo { get; set; }
        public bool? IsMajorChange { get; set; }
        public long? LockedByUserId { get; set; }
        public DateTime? LockedDate { get; set; }
        public long? UploadedByUserId { get; set; }
        public DateTime? UploadedDate { get; set; }
        public string ActualVersionNo { get; set; }
        public bool? IsMeetingNotes { get; set; }
        public bool? IsDiscussionNotes { get; set; }
        public bool? IsReleaseVersion { get; set; }
        public bool? IsNoChange { get; set; }
        public string DraftingVersionNo { get; set; }
        public string CheckInDescription { get; set; }

        public virtual Documents Document { get; set; }
        public virtual Folders Folder { get; set; }
    }
}
