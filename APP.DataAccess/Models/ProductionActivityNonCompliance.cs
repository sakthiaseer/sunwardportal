﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionActivityNonCompliance
    {
        public ProductionActivityNonCompliance()
        {
            ProductionActivityNonComplianceUser = new HashSet<ProductionActivityNonComplianceUser>();
        }

        public long ProductionActivityNonComplianceId { get; set; }
        public long? ProductionActivityAppLineId { get; set; }
        public long? IpirReportId { get; set; }
        public long? ProductionActivityRoutineAppLineId { get; set; }
        public string Type { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? StatusCodeId { get; set; }
        public string ActionType { get; set; }
        public long? ProductionActivityPlanningAppLineId { get; set; }
        public string Notes { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual IpirReport IpirReport { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProductionActivityAppLine ProductionActivityAppLine { get; set; }
        public virtual ProductionActivityPlanningAppLine ProductionActivityPlanningAppLine { get; set; }
        public virtual ProductionActivityRoutineAppLine ProductionActivityRoutineAppLine { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ProductionActivityNonComplianceUser> ProductionActivityNonComplianceUser { get; set; }
    }
}
