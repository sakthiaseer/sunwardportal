﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WikiPageGroup
    {
        public long WikiPageGroupId { get; set; }
        public long PageId { get; set; }
        public string GroupName { get; set; }
        public long? ParentId { get; set; }

        public virtual WikiPage Page { get; set; }
        public virtual WikiPage Parent { get; set; }
    }
}
