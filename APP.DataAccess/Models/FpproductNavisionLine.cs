﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FpproductNavisionLine
    {
        public long FpproductNavisionLineId { get; set; }
        public long? DatabaseId { get; set; }
        public long? NavisionId { get; set; }
        public string LinkProfileRefNo { get; set; }
        public string MasterProfileRefNo { get; set; }
        public string ProfileRefNo { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Navitems Navision { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
