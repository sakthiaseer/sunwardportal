﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Npraformulation
    {
        public Npraformulation()
        {
            NpraformulationActiveIngredient = new HashSet<NpraformulationActiveIngredient>();
            NpraformulationExcipient = new HashSet<NpraformulationExcipient>();
        }

        public long NpraformulationId { get; set; }
        public int? RegistrationReferenceId { get; set; }
        public long? ProductNameId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationMasterDetail ProductName { get; set; }
        public virtual CodeMaster RegistrationReference { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<NpraformulationActiveIngredient> NpraformulationActiveIngredient { get; set; }
        public virtual ICollection<NpraformulationExcipient> NpraformulationExcipient { get; set; }
    }
}
