﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormWorkFlowApproval
    {
        public long DynamicFormWorkFlowApprovalId { get; set; }
        public long? DynamicFormWorkFlowId { get; set; }
        public long? UserId { get; set; }
        public int? SortBy { get; set; }

        public virtual DynamicFormWorkFlow DynamicFormWorkFlow { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
