﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class BomproductionGroup
    {
        public long BomproductionGroupId { get; set; }
        public long? ManufacturingRecipeId { get; set; }
        public long? FpexcipientId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string ProfileReferenceNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual FinishProductExcipient Fpexcipient { get; set; }
        public virtual FpmanufacturingRecipe ManufacturingRecipe { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
