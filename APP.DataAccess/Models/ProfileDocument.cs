﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProfileDocument
    {
        public long ProfileDocumentId { get; set; }
        public long? DocumentId { get; set; }
        public long? NumberSeriesId { get; set; }
        public bool? IsLatest { get; set; }
        public string VersionNo { get; set; }
        public long? UploadedByUserId { get; set; }
        public DateTime? UploadedDate { get; set; }

        public virtual Documents Document { get; set; }
        public virtual DocumentNoSeries NumberSeries { get; set; }
        public virtual ApplicationUser UploadedByUser { get; set; }
    }
}
