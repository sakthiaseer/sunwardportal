﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ItemClassificationAccess
    {
        public long ItemClassificationAccessId { get; set; }
        public long? ItemClassificationId { get; set; }
        public long? UserId { get; set; }
        public long? UserGroupId { get; set; }
        public long? RoleId { get; set; }
        public long? FormId { get; set; }

        public virtual ApplicationForm Form { get; set; }
        public virtual ItemClassificationMaster ItemClassification { get; set; }
        public virtual DocumentRole Role { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual UserGroup UserGroup { get; set; }
    }
}
