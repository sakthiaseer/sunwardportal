﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ReleaseDetails
    {
        public long ReleaseDetailId { get; set; }
        public long? VersionId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ReleaseVersion Version { get; set; }
    }
}
