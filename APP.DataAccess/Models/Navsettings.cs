﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Navsettings
    {
        public long NavcompanyId { get; set; }
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public string SoapUrl { get; set; }
        public string OdataUrl { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DomainName { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
