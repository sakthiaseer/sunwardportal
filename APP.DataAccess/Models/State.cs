﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class State
    {
        public State()
        {
            CompanyHolidays = new HashSet<CompanyHolidays>();
            CompanyWorkDayMaster = new HashSet<CompanyWorkDayMaster>();
            Contacts = new HashSet<Contacts>();
            HolidayMaster = new HashSet<HolidayMaster>();
        }

        public long StateId { get; set; }
        public long? CountryId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Country Country { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<CompanyHolidays> CompanyHolidays { get; set; }
        public virtual ICollection<CompanyWorkDayMaster> CompanyWorkDayMaster { get; set; }
        public virtual ICollection<Contacts> Contacts { get; set; }
        public virtual ICollection<HolidayMaster> HolidayMaster { get; set; }
    }
}
