﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SalesOrderMasterPricingLineSellingMethod
    {
        public long SalesOrderMasterPricingLineSellingMethodId { get; set; }
        public long? SalesOrderMasterPricingLineId { get; set; }
        public decimal? TierFromQty { get; set; }
        public decimal? TierToQty { get; set; }
        public decimal? TierPrice { get; set; }
        public decimal? BounsPrice { get; set; }
        public decimal? BounsQty { get; set; }
        public decimal? BounsFocQty { get; set; }

        public virtual SalesOrderMasterPricingLine SalesOrderMasterPricingLine { get; set; }
    }
}
