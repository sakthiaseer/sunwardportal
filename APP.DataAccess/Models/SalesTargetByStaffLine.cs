﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SalesTargetByStaffLine
    {
        public SalesTargetByStaffLine()
        {
            SalesTargetByStaffSalesCover = new HashSet<SalesTargetByStaffSalesCover>();
        }

        public long SalesTargetByStaffLineId { get; set; }
        public long? SalesTargetByStaffId { get; set; }
        public long? SalesPersonId { get; set; }
        public string SalesPersonName { get; set; }
        public decimal? Target { get; set; }
        public DateTime? EffectiveFrom { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Employee SalesPerson { get; set; }
        public virtual SalesTargetByStaff SalesTargetByStaff { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<SalesTargetByStaffSalesCover> SalesTargetByStaffSalesCover { get; set; }
    }
}
