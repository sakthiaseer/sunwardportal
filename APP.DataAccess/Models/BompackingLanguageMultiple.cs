﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class BompackingLanguageMultiple
    {
        public long BompackingLanguageId { get; set; }
        public long? BompackingId { get; set; }
        public long? LanguageId { get; set; }

        public virtual BompackingMaster Bompacking { get; set; }
    }
}
