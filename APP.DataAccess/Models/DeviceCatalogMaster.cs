﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DeviceCatalogMaster
    {
        public DeviceCatalogMaster()
        {
            AcceptableCalibrationInfo = new HashSet<AcceptableCalibrationInfo>();
            CalibrationExpireCalculation = new HashSet<CalibrationExpireCalculation>();
            CalibrationVendorInfo = new HashSet<CalibrationVendorInfo>();
            CatalogCalibrationService = new HashSet<CatalogCalibrationService>();
            DeviceGroupCatalog = new HashSet<DeviceGroupCatalog>();
            DeviceManufacturerGroup = new HashSet<DeviceManufacturerGroup>();
            InstructionType = new HashSet<InstructionType>();
            RangeCalibration = new HashSet<RangeCalibration>();
            SunwardAssetList = new HashSet<SunwardAssetList>();
        }

        public long DeviceCatalogMasterId { get; set; }
        public long? SourceListId { get; set; }
        public long? DeviceGroupListId { get; set; }
        public string ModelName { get; set; }
        public long? CalibrationTypeId { get; set; }
        public string Cataloglink { get; set; }
        public string Status { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CalibrationType CalibrationType { get; set; }
        public virtual DeviceGroupList DeviceGroupList { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual SourceList SourceList { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<AcceptableCalibrationInfo> AcceptableCalibrationInfo { get; set; }
        public virtual ICollection<CalibrationExpireCalculation> CalibrationExpireCalculation { get; set; }
        public virtual ICollection<CalibrationVendorInfo> CalibrationVendorInfo { get; set; }
        public virtual ICollection<CatalogCalibrationService> CatalogCalibrationService { get; set; }
        public virtual ICollection<DeviceGroupCatalog> DeviceGroupCatalog { get; set; }
        public virtual ICollection<DeviceManufacturerGroup> DeviceManufacturerGroup { get; set; }
        public virtual ICollection<InstructionType> InstructionType { get; set; }
        public virtual ICollection<RangeCalibration> RangeCalibration { get; set; }
        public virtual ICollection<SunwardAssetList> SunwardAssetList { get; set; }
    }
}
