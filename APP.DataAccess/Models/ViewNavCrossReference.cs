﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewNavCrossReference
    {
        public long NavCrossReferenceId { get; set; }
        public long? ItemId { get; set; }
        public string TypeOfCompany { get; set; }
        public long? CompanyId { get; set; }
        public long? NavVendorId { get; set; }
        public long? NavCustomerId { get; set; }
        public string CrossReferenceNo { get; set; }
        public string VendorName { get; set; }
        public string VendorItemNo { get; set; }
        public string VendorNo { get; set; }
        public string NavcustomerName { get; set; }
        public string NavcustomerCode { get; set; }
        public string PlantCode { get; set; }
        public string PlantDescription { get; set; }
        public string CustomerName { get; set; }
        public long? SoCustomerId { get; set; }
        public string ShipCode { get; set; }
    }
}
