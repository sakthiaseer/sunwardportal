﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmployeeSageInformation
    {
        public long SageInformationId { get; set; }
        public long? EmployeeId { get; set; }
        public string SageId { get; set; }
        public DateTime? AttendenceDate { get; set; }
        public DateTime? CheckinDateTime { get; set; }
        public string Remarks { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
