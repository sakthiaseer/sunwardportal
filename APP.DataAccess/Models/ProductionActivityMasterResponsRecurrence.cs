﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionActivityMasterResponsRecurrence
    {
        public long ProductionActivityMasterResponsRecurrenceId { get; set; }
        public long? ProductionActivityMasterResponsId { get; set; }
        public int? TypeId { get; set; }
        public int? RepeatNos { get; set; }
        public bool? Sunday { get; set; }
        public bool? Monday { get; set; }
        public bool? Tuesday { get; set; }
        public bool? Wednesday { get; set; }
        public bool? Thursday { get; set; }
        public bool? Friday { get; set; }
        public bool? Saturyday { get; set; }
        public int? OccurenceOptionId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? NoOfOccurences { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster OccurenceOption { get; set; }
        public virtual ProductionActivityMasterRespons ProductionActivityMasterRespons { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CodeMaster Type { get; set; }
    }
}
