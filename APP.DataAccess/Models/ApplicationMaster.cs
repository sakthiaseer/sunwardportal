﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApplicationMaster
    {
        public ApplicationMaster()
        {
            ApplicationMasterAccess = new HashSet<ApplicationMasterAccess>();
            ApplicationMasterDetail = new HashSet<ApplicationMasterDetail>();
            AttributeHeader = new HashSet<AttributeHeader>();
            DynamicFormSectionAttribute = new HashSet<DynamicFormSectionAttribute>();
        }

        public long ApplicationMasterId { get; set; }
        public string ApplicationMasterName { get; set; }
        public string ApplicationMasterDescription { get; set; }
        public long? ApplicationMasterCodeId { get; set; }
        public bool? IsApplyProfile { get; set; }
        public bool? IsApplyFileProfile { get; set; }

        public virtual ICollection<ApplicationMasterAccess> ApplicationMasterAccess { get; set; }
        public virtual ICollection<ApplicationMasterDetail> ApplicationMasterDetail { get; set; }
        public virtual ICollection<AttributeHeader> AttributeHeader { get; set; }
        public virtual ICollection<DynamicFormSectionAttribute> DynamicFormSectionAttribute { get; set; }
    }
}
