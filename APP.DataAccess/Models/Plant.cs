﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Plant
    {
        public Plant()
        {
            Acentry = new HashSet<Acentry>();
            Acitems = new HashSet<Acitems>();
            ApplicationUserPlant = new HashSet<ApplicationUserPlant>();
            ApplicationWikiLineDuty = new HashSet<ApplicationWikiLineDuty>();
            ApplicationWikiPlant = new HashSet<ApplicationWiki>();
            ApplicationWikiProfilePlant = new HashSet<ApplicationWiki>();
            AssetCatalogMaster = new HashSet<AssetCatalogMaster>();
            AttributeHeader = new HashSet<AttributeHeader>();
            BmrdisposalMasterBox = new HashSet<BmrdisposalMasterBox>();
            ClassificationBmr = new HashSet<ClassificationBmr>();
            CommitmentOrders = new HashSet<CommitmentOrders>();
            CompanyCalendar = new HashSet<CompanyCalendar>();
            Contact = new HashSet<Contact>();
            Department = new HashSet<Department>();
            Designation = new HashSet<Designation>();
            DistributorReplenishment = new HashSet<DistributorReplenishment>();
            Division = new HashSet<Division>();
            DocumentProfileNoSeries = new HashSet<DocumentProfileNoSeries>();
            DraftApplicationWikiLineDuty = new HashSet<DraftApplicationWikiLineDuty>();
            DraftApplicationWikiPlant = new HashSet<DraftApplicationWiki>();
            DraftApplicationWikiProfilePlant = new HashSet<DraftApplicationWiki>();
            DynamicFlow = new HashSet<DynamicFlow>();
            DynamicForm = new HashSet<DynamicForm>();
            DynamicFormItem = new HashSet<DynamicFormItem>();
            Employee = new HashSet<Employee>();
            EmployeeInformation = new HashSet<EmployeeInformation>();
            FinishedProdOrderLine = new HashSet<FinishedProdOrderLine>();
            Fmglobal = new HashSet<Fmglobal>();
            GenericCodeCountry = new HashSet<GenericCodeCountry>();
            Ictmaster = new HashSet<Ictmaster>();
            InterCompanyPurchaseOrder = new HashSet<InterCompanyPurchaseOrder>();
            InventoryType = new HashSet<InventoryType>();
            Ipir = new HashSet<Ipir>();
            IpirApp = new HashSet<IpirApp>();
            IpirReport = new HashSet<IpirReport>();
            Ipirmobile = new HashSet<Ipirmobile>();
            IssueReportIpir = new HashSet<IssueReportIpir>();
            ItemBatchInfo = new HashSet<ItemBatchInfo>();
            ItemClassificationTransport = new HashSet<ItemClassificationTransport>();
            ItemCost = new HashSet<ItemCost>();
            ItemManufacture = new HashSet<ItemManufacture>();
            ItemMaster = new HashSet<ItemMaster>();
            ItemSalesPrice = new HashSet<ItemSalesPrice>();
            JobProgressTemplate = new HashSet<JobProgressTemplate>();
            JobProgressTemplateLineLineProcess = new HashSet<JobProgressTemplateLineLineProcess>();
            JobSchedule = new HashSet<JobSchedule>();
            LevelMaster = new HashSet<LevelMaster>();
            Locations = new HashSet<Locations>();
            MarginInformation = new HashSet<MarginInformation>();
            MasterBlanketOrder = new HashSet<MasterBlanketOrder>();
            MasterInterCompanyPricingFromCompany = new HashSet<MasterInterCompanyPricing>();
            MasterInterCompanyPricingToCompany = new HashSet<MasterInterCompanyPricing>();
            MedMaster = new HashSet<MedMaster>();
            MobileShift = new HashSet<MobileShift>();
            NavCrossReference = new HashSet<NavCrossReference>();
            NavManufacturingProcess = new HashSet<NavManufacturingProcess>();
            NavMethodCode = new HashSet<NavMethodCode>();
            NavSalesOrderLine = new HashSet<NavSalesOrderLine>();
            NavbaseUnit = new HashSet<NavbaseUnit>();
            Navcustomer = new HashSet<Navcustomer>();
            NavisionCompany = new HashSet<NavisionCompany>();
            Navitems = new HashSet<Navitems>();
            Navlocation = new HashSet<Navlocation>();
            NavprodOrderLine = new HashSet<NavprodOrderLine>();
            NavproductCode = new HashSet<NavproductCode>();
            Navrecipes = new HashSet<Navrecipes>();
            Navvendor = new HashSet<Navvendor>();
            NoticeLine = new HashSet<NoticeLine>();
            OrderRequirement = new HashSet<OrderRequirement>();
            PkgapprovalInformation = new HashSet<PkgapprovalInformation>();
            PlantMaintenanceEntry = new HashSet<PlantMaintenanceEntry>();
            Portfolio = new HashSet<Portfolio>();
            ProductActivityCase = new HashSet<ProductActivityCase>();
            ProductActivityCaseCompany = new HashSet<ProductActivityCaseCompany>();
            ProductActivityCaseCompanyMultiple = new HashSet<ProductActivityCaseCompanyMultiple>();
            ProductActivityCaseResponsDuty = new HashSet<ProductActivityCaseResponsDuty>();
            ProductGroupSurvey = new HashSet<ProductGroupSurvey>();
            ProductGrouping = new HashSet<ProductGrouping>();
            ProductionActivityApp = new HashSet<ProductionActivityApp>();
            ProductionActivityPlanningApp = new HashSet<ProductionActivityPlanningApp>();
            ProductionActivityRoutineApp = new HashSet<ProductionActivityRoutineApp>();
            ProductionBatchInformation = new HashSet<ProductionBatchInformation>();
            ProductionCycle = new HashSet<ProductionCycle>();
            ProductionSimulation = new HashSet<ProductionSimulation>();
            ProductionSimulationGrouping = new HashSet<ProductionSimulationGrouping>();
            ProductionTicket = new HashSet<ProductionTicket>();
            ProfileAutoNumber = new HashSet<ProfileAutoNumber>();
            PurchaseOrder = new HashSet<PurchaseOrder>();
            PurchaseOrderLine = new HashSet<PurchaseOrderLine>();
            QuotationHeader = new HashSet<QuotationHeader>();
            QuotationHistory = new HashSet<QuotationHistory>();
            RawMatItemList = new HashSet<RawMatItemList>();
            RequestAc = new HashSet<RequestAc>();
            SalesOrder = new HashSet<SalesOrder>();
            SalesOrderEntry = new HashSet<SalesOrderEntry>();
            SalesOrderMasterPricing = new HashSet<SalesOrderMasterPricing>();
            SalesPromotion = new HashSet<SalesPromotion>();
            SampleRequestForDoctor = new HashSet<SampleRequestForDoctor>();
            SampleRequestForm = new HashSet<SampleRequestForm>();
            SoSalesOrder = new HashSet<SoSalesOrder>();
            SobyCustomerSunwardEquivalent = new HashSet<SobyCustomerSunwardEquivalent>();
            TemplateTestCase = new HashSet<TemplateTestCase>();
            TemplateTestCaseCheckListResponseDuty = new HashSet<TemplateTestCaseCheckListResponseDuty>();
            TemplateTestCaseForm = new HashSet<TemplateTestCaseForm>();
            WikiCompany = new HashSet<WikiCompany>();
            WorkFlowPages = new HashSet<WorkFlowPages>();
        }

        public long PlantId { get; set; }
        public long CompanyId { get; set; }
        public string PlantCode { get; set; }
        public string Description { get; set; }
        public long? RegisteredCountryId { get; set; }
        public string RegistrationNo { get; set; }
        public DateTime? EstablishedDate { get; set; }
        public string Gstno { get; set; }
        public string NavSoapAddress { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsLinkNav { get; set; }
        public string NavCompanyName { get; set; }
        public string NavUserName { get; set; }
        public string NavPassword { get; set; }
        public string NavOdataAddress { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public string NavCompany { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Company Company { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<Acentry> Acentry { get; set; }
        public virtual ICollection<Acitems> Acitems { get; set; }
        public virtual ICollection<ApplicationUserPlant> ApplicationUserPlant { get; set; }
        public virtual ICollection<ApplicationWikiLineDuty> ApplicationWikiLineDuty { get; set; }
        public virtual ICollection<ApplicationWiki> ApplicationWikiPlant { get; set; }
        public virtual ICollection<ApplicationWiki> ApplicationWikiProfilePlant { get; set; }
        public virtual ICollection<AssetCatalogMaster> AssetCatalogMaster { get; set; }
        public virtual ICollection<AttributeHeader> AttributeHeader { get; set; }
        public virtual ICollection<BmrdisposalMasterBox> BmrdisposalMasterBox { get; set; }
        public virtual ICollection<ClassificationBmr> ClassificationBmr { get; set; }
        public virtual ICollection<CommitmentOrders> CommitmentOrders { get; set; }
        public virtual ICollection<CompanyCalendar> CompanyCalendar { get; set; }
        public virtual ICollection<Contact> Contact { get; set; }
        public virtual ICollection<Department> Department { get; set; }
        public virtual ICollection<Designation> Designation { get; set; }
        public virtual ICollection<DistributorReplenishment> DistributorReplenishment { get; set; }
        public virtual ICollection<Division> Division { get; set; }
        public virtual ICollection<DocumentProfileNoSeries> DocumentProfileNoSeries { get; set; }
        public virtual ICollection<DraftApplicationWikiLineDuty> DraftApplicationWikiLineDuty { get; set; }
        public virtual ICollection<DraftApplicationWiki> DraftApplicationWikiPlant { get; set; }
        public virtual ICollection<DraftApplicationWiki> DraftApplicationWikiProfilePlant { get; set; }
        public virtual ICollection<DynamicFlow> DynamicFlow { get; set; }
        public virtual ICollection<DynamicForm> DynamicForm { get; set; }
        public virtual ICollection<DynamicFormItem> DynamicFormItem { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
        public virtual ICollection<EmployeeInformation> EmployeeInformation { get; set; }
        public virtual ICollection<FinishedProdOrderLine> FinishedProdOrderLine { get; set; }
        public virtual ICollection<Fmglobal> Fmglobal { get; set; }
        public virtual ICollection<GenericCodeCountry> GenericCodeCountry { get; set; }
        public virtual ICollection<Ictmaster> Ictmaster { get; set; }
        public virtual ICollection<InterCompanyPurchaseOrder> InterCompanyPurchaseOrder { get; set; }
        public virtual ICollection<InventoryType> InventoryType { get; set; }
        public virtual ICollection<Ipir> Ipir { get; set; }
        public virtual ICollection<IpirApp> IpirApp { get; set; }
        public virtual ICollection<IpirReport> IpirReport { get; set; }
        public virtual ICollection<Ipirmobile> Ipirmobile { get; set; }
        public virtual ICollection<IssueReportIpir> IssueReportIpir { get; set; }
        public virtual ICollection<ItemBatchInfo> ItemBatchInfo { get; set; }
        public virtual ICollection<ItemClassificationTransport> ItemClassificationTransport { get; set; }
        public virtual ICollection<ItemCost> ItemCost { get; set; }
        public virtual ICollection<ItemManufacture> ItemManufacture { get; set; }
        public virtual ICollection<ItemMaster> ItemMaster { get; set; }
        public virtual ICollection<ItemSalesPrice> ItemSalesPrice { get; set; }
        public virtual ICollection<JobProgressTemplate> JobProgressTemplate { get; set; }
        public virtual ICollection<JobProgressTemplateLineLineProcess> JobProgressTemplateLineLineProcess { get; set; }
        public virtual ICollection<JobSchedule> JobSchedule { get; set; }
        public virtual ICollection<LevelMaster> LevelMaster { get; set; }
        public virtual ICollection<Locations> Locations { get; set; }
        public virtual ICollection<MarginInformation> MarginInformation { get; set; }
        public virtual ICollection<MasterBlanketOrder> MasterBlanketOrder { get; set; }
        public virtual ICollection<MasterInterCompanyPricing> MasterInterCompanyPricingFromCompany { get; set; }
        public virtual ICollection<MasterInterCompanyPricing> MasterInterCompanyPricingToCompany { get; set; }
        public virtual ICollection<MedMaster> MedMaster { get; set; }
        public virtual ICollection<MobileShift> MobileShift { get; set; }
        public virtual ICollection<NavCrossReference> NavCrossReference { get; set; }
        public virtual ICollection<NavManufacturingProcess> NavManufacturingProcess { get; set; }
        public virtual ICollection<NavMethodCode> NavMethodCode { get; set; }
        public virtual ICollection<NavSalesOrderLine> NavSalesOrderLine { get; set; }
        public virtual ICollection<NavbaseUnit> NavbaseUnit { get; set; }
        public virtual ICollection<Navcustomer> Navcustomer { get; set; }
        public virtual ICollection<NavisionCompany> NavisionCompany { get; set; }
        public virtual ICollection<Navitems> Navitems { get; set; }
        public virtual ICollection<Navlocation> Navlocation { get; set; }
        public virtual ICollection<NavprodOrderLine> NavprodOrderLine { get; set; }
        public virtual ICollection<NavproductCode> NavproductCode { get; set; }
        public virtual ICollection<Navrecipes> Navrecipes { get; set; }
        public virtual ICollection<Navvendor> Navvendor { get; set; }
        public virtual ICollection<NoticeLine> NoticeLine { get; set; }
        public virtual ICollection<OrderRequirement> OrderRequirement { get; set; }
        public virtual ICollection<PkgapprovalInformation> PkgapprovalInformation { get; set; }
        public virtual ICollection<PlantMaintenanceEntry> PlantMaintenanceEntry { get; set; }
        public virtual ICollection<Portfolio> Portfolio { get; set; }
        public virtual ICollection<ProductActivityCase> ProductActivityCase { get; set; }
        public virtual ICollection<ProductActivityCaseCompany> ProductActivityCaseCompany { get; set; }
        public virtual ICollection<ProductActivityCaseCompanyMultiple> ProductActivityCaseCompanyMultiple { get; set; }
        public virtual ICollection<ProductActivityCaseResponsDuty> ProductActivityCaseResponsDuty { get; set; }
        public virtual ICollection<ProductGroupSurvey> ProductGroupSurvey { get; set; }
        public virtual ICollection<ProductGrouping> ProductGrouping { get; set; }
        public virtual ICollection<ProductionActivityApp> ProductionActivityApp { get; set; }
        public virtual ICollection<ProductionActivityPlanningApp> ProductionActivityPlanningApp { get; set; }
        public virtual ICollection<ProductionActivityRoutineApp> ProductionActivityRoutineApp { get; set; }
        public virtual ICollection<ProductionBatchInformation> ProductionBatchInformation { get; set; }
        public virtual ICollection<ProductionCycle> ProductionCycle { get; set; }
        public virtual ICollection<ProductionSimulation> ProductionSimulation { get; set; }
        public virtual ICollection<ProductionSimulationGrouping> ProductionSimulationGrouping { get; set; }
        public virtual ICollection<ProductionTicket> ProductionTicket { get; set; }
        public virtual ICollection<ProfileAutoNumber> ProfileAutoNumber { get; set; }
        public virtual ICollection<PurchaseOrder> PurchaseOrder { get; set; }
        public virtual ICollection<PurchaseOrderLine> PurchaseOrderLine { get; set; }
        public virtual ICollection<QuotationHeader> QuotationHeader { get; set; }
        public virtual ICollection<QuotationHistory> QuotationHistory { get; set; }
        public virtual ICollection<RawMatItemList> RawMatItemList { get; set; }
        public virtual ICollection<RequestAc> RequestAc { get; set; }
        public virtual ICollection<SalesOrder> SalesOrder { get; set; }
        public virtual ICollection<SalesOrderEntry> SalesOrderEntry { get; set; }
        public virtual ICollection<SalesOrderMasterPricing> SalesOrderMasterPricing { get; set; }
        public virtual ICollection<SalesPromotion> SalesPromotion { get; set; }
        public virtual ICollection<SampleRequestForDoctor> SampleRequestForDoctor { get; set; }
        public virtual ICollection<SampleRequestForm> SampleRequestForm { get; set; }
        public virtual ICollection<SoSalesOrder> SoSalesOrder { get; set; }
        public virtual ICollection<SobyCustomerSunwardEquivalent> SobyCustomerSunwardEquivalent { get; set; }
        public virtual ICollection<TemplateTestCase> TemplateTestCase { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponseDuty> TemplateTestCaseCheckListResponseDuty { get; set; }
        public virtual ICollection<TemplateTestCaseForm> TemplateTestCaseForm { get; set; }
        public virtual ICollection<WikiCompany> WikiCompany { get; set; }
        public virtual ICollection<WorkFlowPages> WorkFlowPages { get; set; }
    }
}
