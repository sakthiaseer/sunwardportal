﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class UserGroup
    {
        public UserGroup()
        {
            ApplicationMasterAccess = new HashSet<ApplicationMasterAccess>();
            CloseDocumentPermission = new HashSet<CloseDocumentPermission>();
            CompanyCalandarDocumentPermission = new HashSet<CompanyCalandarDocumentPermission>();
            DocumentUserRoleReferencedGroup = new HashSet<DocumentUserRole>();
            DocumentUserRoleUserGroup = new HashSet<DocumentUserRole>();
            DraftWikiResponsible = new HashSet<DraftWikiResponsible>();
            DynamicFlowProgress = new HashSet<DynamicFlowProgress>();
            DynamicFlowStep = new HashSet<DynamicFlowStep>();
            DynamicFlowStepDetail = new HashSet<DynamicFlowStepDetail>();
            DynamicFormSectionAttributeSecurity = new HashSet<DynamicFormSectionAttributeSecurity>();
            DynamicFormSectionSecurity = new HashSet<DynamicFormSectionSecurity>();
            DynamicFormWorkFlow = new HashSet<DynamicFormWorkFlow>();
            FolderStorage = new HashSet<FolderStorage>();
            IpirReportAssignmentUser = new HashSet<IpirReportAssignmentUser>();
            IssueReportCcto = new HashSet<IssueReportCcto>();
            ItemClassificationAccess = new HashSet<ItemClassificationAccess>();
            ItemClassificationPermission = new HashSet<ItemClassificationPermission>();
            JobProgressTemplateLineLineProcess = new HashSet<JobProgressTemplateLineLineProcess>();
            JobProgressTemplateNotify = new HashSet<JobProgressTemplateNotify>();
            MemoUser = new HashSet<MemoUser>();
            NoticeUser = new HashSet<NoticeUser>();
            NotifyDocumentUserGroup = new HashSet<NotifyDocumentUserGroup>();
            NotifyUserGroupUser = new HashSet<NotifyUserGroupUser>();
            OpenAccessUserLink = new HashSet<OpenAccessUserLink>();
            ProductActivityCaseResponsResponsible = new HashSet<ProductActivityCaseResponsResponsible>();
            ProductActivityPermission = new HashSet<ProductActivityPermission>();
            RegistrationFormatInformationLineUsers = new HashSet<RegistrationFormatInformationLineUsers>();
            Smecomment = new HashSet<Smecomment>();
            TemplateTestCaseCheckListResponseResponsible = new HashSet<TemplateTestCaseCheckListResponseResponsible>();
            UserGroupUser = new HashSet<UserGroupUser>();
            WikiAccessRight = new HashSet<WikiAccessRight>();
            WikiResponsible = new HashSet<WikiResponsible>();
            WorkFlowPageBlockAccess = new HashSet<WorkFlowPageBlockAccess>();
            WorkFlowPageFieldAccess = new HashSet<WorkFlowPageFieldAccess>();
            WorkOrder = new HashSet<WorkOrder>();
        }

        public long UserGroupId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsTms { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ApplicationMasterAccess> ApplicationMasterAccess { get; set; }
        public virtual ICollection<CloseDocumentPermission> CloseDocumentPermission { get; set; }
        public virtual ICollection<CompanyCalandarDocumentPermission> CompanyCalandarDocumentPermission { get; set; }
        public virtual ICollection<DocumentUserRole> DocumentUserRoleReferencedGroup { get; set; }
        public virtual ICollection<DocumentUserRole> DocumentUserRoleUserGroup { get; set; }
        public virtual ICollection<DraftWikiResponsible> DraftWikiResponsible { get; set; }
        public virtual ICollection<DynamicFlowProgress> DynamicFlowProgress { get; set; }
        public virtual ICollection<DynamicFlowStep> DynamicFlowStep { get; set; }
        public virtual ICollection<DynamicFlowStepDetail> DynamicFlowStepDetail { get; set; }
        public virtual ICollection<DynamicFormSectionAttributeSecurity> DynamicFormSectionAttributeSecurity { get; set; }
        public virtual ICollection<DynamicFormSectionSecurity> DynamicFormSectionSecurity { get; set; }
        public virtual ICollection<DynamicFormWorkFlow> DynamicFormWorkFlow { get; set; }
        public virtual ICollection<FolderStorage> FolderStorage { get; set; }
        public virtual ICollection<IpirReportAssignmentUser> IpirReportAssignmentUser { get; set; }
        public virtual ICollection<IssueReportCcto> IssueReportCcto { get; set; }
        public virtual ICollection<ItemClassificationAccess> ItemClassificationAccess { get; set; }
        public virtual ICollection<ItemClassificationPermission> ItemClassificationPermission { get; set; }
        public virtual ICollection<JobProgressTemplateLineLineProcess> JobProgressTemplateLineLineProcess { get; set; }
        public virtual ICollection<JobProgressTemplateNotify> JobProgressTemplateNotify { get; set; }
        public virtual ICollection<MemoUser> MemoUser { get; set; }
        public virtual ICollection<NoticeUser> NoticeUser { get; set; }
        public virtual ICollection<NotifyDocumentUserGroup> NotifyDocumentUserGroup { get; set; }
        public virtual ICollection<NotifyUserGroupUser> NotifyUserGroupUser { get; set; }
        public virtual ICollection<OpenAccessUserLink> OpenAccessUserLink { get; set; }
        public virtual ICollection<ProductActivityCaseResponsResponsible> ProductActivityCaseResponsResponsible { get; set; }
        public virtual ICollection<ProductActivityPermission> ProductActivityPermission { get; set; }
        public virtual ICollection<RegistrationFormatInformationLineUsers> RegistrationFormatInformationLineUsers { get; set; }
        public virtual ICollection<Smecomment> Smecomment { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponseResponsible> TemplateTestCaseCheckListResponseResponsible { get; set; }
        public virtual ICollection<UserGroupUser> UserGroupUser { get; set; }
        public virtual ICollection<WikiAccessRight> WikiAccessRight { get; set; }
        public virtual ICollection<WikiResponsible> WikiResponsible { get; set; }
        public virtual ICollection<WorkFlowPageBlockAccess> WorkFlowPageBlockAccess { get; set; }
        public virtual ICollection<WorkFlowPageFieldAccess> WorkFlowPageFieldAccess { get; set; }
        public virtual ICollection<WorkOrder> WorkOrder { get; set; }
    }
}
