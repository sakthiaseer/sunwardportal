﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SalesBorrowHeader
    {
        public SalesBorrowHeader()
        {
            SalesBorrowLine = new HashSet<SalesBorrowLine>();
        }

        public long SalesBorrowHeaderId { get; set; }
        public long? OnBehalfId { get; set; }
        public long? BorrowFromId { get; set; }
        public long? CustomerToSendId { get; set; }
        public string PurposeOfBorrow { get; set; }
        public bool? IsReturn { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CompanyListing BorrowFrom { get; set; }
        public virtual CompanyListing CustomerToSend { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationUser OnBehalf { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<SalesBorrowLine> SalesBorrowLine { get; set; }
    }
}
