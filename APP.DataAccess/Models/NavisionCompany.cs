﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavisionCompany
    {
        public long NavisionCompanyId { get; set; }
        public long? DatabaseId { get; set; }
        public long? CompanyTypeId { get; set; }
        public long? NavCustomerId { get; set; }
        public long? NavVendorId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Database { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Navcustomer NavCustomer { get; set; }
        public virtual Navvendor NavVendor { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
