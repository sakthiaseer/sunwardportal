﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SunwardAssetList
    {
        public long SunwardAssetListId { get; set; }
        public int? No { get; set; }
        public long? CompanyId { get; set; }
        public string Description { get; set; }
        public string UnitOfMeasure { get; set; }
        public long? DeviceCatalogMasterId { get; set; }
        public string DeviceModelName { get; set; }
        public string SerialNo { get; set; }
        public string OldId { get; set; }
        public string NewId { get; set; }
        public string PhotoOfDevice { get; set; }
        public long? SiteId { get; set; }
        public long? ZoneId { get; set; }
        public long? LocationId { get; set; }
        public long? AreaId { get; set; }
        public long? SpecificAreaId { get; set; }
        public int? DeviceStatusId { get; set; }
        public string DevicePhysicalCondition { get; set; }
        public int? CalibrationStatus { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster CalibrationStatusNavigation { get; set; }
        public virtual Company Company { get; set; }
        public virtual DeviceCatalogMaster DeviceCatalogMaster { get; set; }
        public virtual CodeMaster DeviceStatus { get; set; }
        public virtual Ictmaster Location { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
