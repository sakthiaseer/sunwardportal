﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProposedNoOfTickets
    {
        public long ProposedItemId { get; set; }
        public long? ItemId { get; set; }
        public decimal? FbStockQty { get; set; }
        public decimal? AdhocStockQty { get; set; }
        public DateTime? StartMonth { get; set; }
        public DateTime? EndMonth { get; set; }
        public long? CompanyId { get; set; }
        public DateTime? ReportDate { get; set; }
        public string VersionNo { get; set; }
    }
}
