﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ServerActivityLog
    {
        public long ServerActivityLogId { get; set; }
        public string ApplicationName { get; set; }
        public string ModuleName { get; set; }
        public string FunctionName { get; set; }
        public string LogType { get; set; }
        public DateTime? LogTime { get; set; }
        public string LogMessage { get; set; }
        public string Sqlcode { get; set; }
        public string ErrorMessage { get; set; }
        public long? LoginSessionId { get; set; }
        public int? StatusCodeId { get; set; }
    }
}
