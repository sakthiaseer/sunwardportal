﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class RangeCalibration
    {
        public long RangeCalibrationId { get; set; }
        public string ParameterRange { get; set; }
        public string Max { get; set; }
        public string Min { get; set; }
        public string UnitOfMeasure { get; set; }
        public string Remarks { get; set; }
        public long? CalibrationServiceInformationId { get; set; }
        public long? DeviceCatalogMasterId { get; set; }

        public virtual CalibrationServiceInfo CalibrationServiceInformation { get; set; }
        public virtual DeviceCatalogMaster DeviceCatalogMaster { get; set; }
    }
}
