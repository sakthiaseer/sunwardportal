﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class OpenAccessUser
    {
        public OpenAccessUser()
        {
            OpenAccessUserLink = new HashSet<OpenAccessUserLink>();
        }

        public long OpenAccessUserId { get; set; }
        public string AccessType { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<OpenAccessUserLink> OpenAccessUserLink { get; set; }
    }
}
