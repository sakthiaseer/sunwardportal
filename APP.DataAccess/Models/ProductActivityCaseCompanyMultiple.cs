﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductActivityCaseCompanyMultiple
    {
        public long ProductActivityCaseCompanyMultipleId { get; set; }
        public long? ProductActivityCaseId { get; set; }
        public long? CompanyId { get; set; }

        public virtual Plant Company { get; set; }
        public virtual ProductActivityCase ProductActivityCase { get; set; }
    }
}
