﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FileProfileType
    {
        public FileProfileType()
        {
            ApplicationMasterDetail = new HashSet<ApplicationMasterDetail>();
            BlanketOrderAttachment = new HashSet<BlanketOrderAttachment>();
            CloseDocumentPermission = new HashSet<CloseDocumentPermission>();
            CommonFieldsProductionMachineDocument = new HashSet<CommonFieldsProductionMachineDocument>();
            ContractDistributionAttachment = new HashSet<ContractDistributionAttachment>();
            DocumentNoSeries = new HashSet<DocumentNoSeries>();
            DocumentUserRole = new HashSet<DocumentUserRole>();
            Documents = new HashSet<Documents>();
            DynamicForm = new HashSet<DynamicForm>();
            DynamicFormSection = new HashSet<DynamicFormSection>();
            FileProfileSetupForm = new HashSet<FileProfileSetupForm>();
            FileProfileTypeDynamicForm = new HashSet<FileProfileTypeDynamicForm>();
            InverseParent = new HashSet<FileProfileType>();
            IpirlineDocument = new HashSet<IpirlineDocument>();
            LinkFileProfileTypeDocument = new HashSet<LinkFileProfileTypeDocument>();
            PackagingHistoryItemLineDocument = new HashSet<PackagingHistoryItemLineDocument>();
            ProductActivityCase = new HashSet<ProductActivityCase>();
            ProductActivityCaseLine = new HashSet<ProductActivityCaseLine>();
            ProductRecipeDocument = new HashSet<ProductRecipeDocument>();
            ProductionActivityMasterLine = new HashSet<ProductionActivityMasterLine>();
            ProductionDocument = new HashSet<ProductionDocument>();
            PurchaseOrderDocument = new HashSet<PurchaseOrderDocument>();
            QuotationHistoryDocument = new HashSet<QuotationHistoryDocument>();
            ReceiveEmail = new HashSet<ReceiveEmail>();
            SalesAdhocNovateAttachment = new HashSet<SalesAdhocNovateAttachment>();
            SalesOrderAttachment = new HashSet<SalesOrderAttachment>();
            TaskAttachment = new HashSet<TaskAttachment>();
            TemplateTestCaseForm = new HashSet<TemplateTestCaseForm>();
            TemplateTestCaseLink = new HashSet<TemplateTestCaseLink>();
            TransferBalanceQtyDocument = new HashSet<TransferBalanceQtyDocument>();
        }

        public long FileProfileTypeId { get; set; }
        public long ProfileId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long? ParentId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsExpiryDate { get; set; }
        public bool? IsAllowMobileUpload { get; set; }
        public bool? IsDocumentAccess { get; set; }
        public int? ShelfLifeDuration { get; set; }
        public int? ShelfLifeDurationId { get; set; }
        public string Hints { get; set; }
        public bool? IsEnableCreateTask { get; set; }
        public string ProfileTypeInfo { get; set; }
        public bool? IsCreateByYear { get; set; }
        public bool? IsCreateByMonth { get; set; }
        public bool? IsHidden { get; set; }
        public string ProfileInfo { get; set; }
        public bool? IsTemplateCaseNo { get; set; }
        public long? TemplateTestCaseId { get; set; }
        public Guid? SessionId { get; set; }
        public bool? IsDelete { get; set; }
        public long? DeleteByUserId { get; set; }
        public DateTime? DeleteByDate { get; set; }
        public long? RestoreByUserId { get; set; }
        public DateTime? RestoreByDate { get; set; }
        public long? DynamicFormId { get; set; }
        public bool? IsDuplicateUpload { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser DeleteByUser { get; set; }
        public virtual DynamicForm DynamicFormNavigation { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual FileProfileType Parent { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual ApplicationUser RestoreByUser { get; set; }
        public virtual CodeMaster ShelfLifeDurationNavigation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual TemplateTestCase TemplateTestCase { get; set; }
        public virtual ICollection<ApplicationMasterDetail> ApplicationMasterDetail { get; set; }
        public virtual ICollection<BlanketOrderAttachment> BlanketOrderAttachment { get; set; }
        public virtual ICollection<CloseDocumentPermission> CloseDocumentPermission { get; set; }
        public virtual ICollection<CommonFieldsProductionMachineDocument> CommonFieldsProductionMachineDocument { get; set; }
        public virtual ICollection<ContractDistributionAttachment> ContractDistributionAttachment { get; set; }
        public virtual ICollection<DocumentNoSeries> DocumentNoSeries { get; set; }
        public virtual ICollection<DocumentUserRole> DocumentUserRole { get; set; }
        public virtual ICollection<Documents> Documents { get; set; }
        public virtual ICollection<DynamicForm> DynamicForm { get; set; }
        public virtual ICollection<DynamicFormSection> DynamicFormSection { get; set; }
        public virtual ICollection<FileProfileSetupForm> FileProfileSetupForm { get; set; }
        public virtual ICollection<FileProfileTypeDynamicForm> FileProfileTypeDynamicForm { get; set; }
        public virtual ICollection<FileProfileType> InverseParent { get; set; }
        public virtual ICollection<IpirlineDocument> IpirlineDocument { get; set; }
        public virtual ICollection<LinkFileProfileTypeDocument> LinkFileProfileTypeDocument { get; set; }
        public virtual ICollection<PackagingHistoryItemLineDocument> PackagingHistoryItemLineDocument { get; set; }
        public virtual ICollection<ProductActivityCase> ProductActivityCase { get; set; }
        public virtual ICollection<ProductActivityCaseLine> ProductActivityCaseLine { get; set; }
        public virtual ICollection<ProductRecipeDocument> ProductRecipeDocument { get; set; }
        public virtual ICollection<ProductionActivityMasterLine> ProductionActivityMasterLine { get; set; }
        public virtual ICollection<ProductionDocument> ProductionDocument { get; set; }
        public virtual ICollection<PurchaseOrderDocument> PurchaseOrderDocument { get; set; }
        public virtual ICollection<QuotationHistoryDocument> QuotationHistoryDocument { get; set; }
        public virtual ICollection<ReceiveEmail> ReceiveEmail { get; set; }
        public virtual ICollection<SalesAdhocNovateAttachment> SalesAdhocNovateAttachment { get; set; }
        public virtual ICollection<SalesOrderAttachment> SalesOrderAttachment { get; set; }
        public virtual ICollection<TaskAttachment> TaskAttachment { get; set; }
        public virtual ICollection<TemplateTestCaseForm> TemplateTestCaseForm { get; set; }
        public virtual ICollection<TemplateTestCaseLink> TemplateTestCaseLink { get; set; }
        public virtual ICollection<TransferBalanceQtyDocument> TransferBalanceQtyDocument { get; set; }
    }
}
