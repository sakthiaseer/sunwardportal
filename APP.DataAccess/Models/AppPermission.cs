﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AppPermission
    {
        public AppPermission()
        {
            InverseParent = new HashSet<AppPermission>();
        }

        public long AppPermissionId { get; set; }
        public long AppId { get; set; }
        public long? ParentId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }

        public virtual AppPermission Parent { get; set; }
        public virtual ICollection<AppPermission> InverseParent { get; set; }
    }
}
