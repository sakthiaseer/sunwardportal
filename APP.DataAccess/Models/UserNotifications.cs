﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class UserNotifications
    {
        public long UserNotificationId { get; set; }
        public long UserId { get; set; }
        public string DeviceType { get; set; }
        public string TokenId { get; set; }
    }
}
