﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class MasterBlanketOrderLine
    {
        public long MasterBlanketOrderLineId { get; set; }
        public long? MasterBlanketOrderId { get; set; }
        public int? NoOfWeeksPerMonth { get; set; }
        public DateTime? WeekOneStartDate { get; set; }
        public long? WeekOneLargestOrder { get; set; }
        public DateTime? WeekTwoStartDate { get; set; }
        public long? WeekTwoLargestOrder { get; set; }
        public DateTime? WeekThreeStartDate { get; set; }
        public long? WeekThreeLargestOrder { get; set; }
        public DateTime? WeekFourStartDate { get; set; }
        public long? WeekFourLargestOrder { get; set; }
        public long? BalanceQtyForCalculate { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual MasterBlanketOrder MasterBlanketOrder { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
