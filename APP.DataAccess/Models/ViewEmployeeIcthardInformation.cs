﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewEmployeeIcthardInformation
    {
        public long EmployeeIcthardInformationId { get; set; }
        public long? EmployeeId { get; set; }
        public long? HardwareId { get; set; }
        public long? CompanyId { get; set; }
        public string LoginId { get; set; }
        public string Password { get; set; }
        public string Instruction { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public int? StatusCodeId { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Name { get; set; }
        public string StatusCode { get; set; }
        public string AddedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string HardwareName { get; set; }
        public int? CodeId { get; set; }
        public string PlantCode { get; set; }
        public string PlantDescription { get; set; }
    }
}
