﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormApproval
    {
        public DynamicFormApproval()
        {
            DynamicFormApproved = new HashSet<DynamicFormApproved>();
        }

        public long DynamicFormApprovalId { get; set; }
        public long? DynamicFormId { get; set; }
        public long? ApprovalUserId { get; set; }
        public int? SortOrderBy { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsApproved { get; set; }
        public string Description { get; set; }
        public int? ApprovedCountUsed { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ApprovalUser { get; set; }
        public virtual DynamicForm DynamicForm { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ICollection<DynamicFormApproved> DynamicFormApproved { get; set; }
    }
}
