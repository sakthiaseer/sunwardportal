﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ManpowerInformationLine
    {
        public long ManpowerInformationLineId { get; set; }
        public long? ManpowerInformationId { get; set; }
        public string OperationNo { get; set; }
        public bool? IsOperatorSkilled { get; set; }
        public bool? IsQcapproval { get; set; }
        public bool? IsPicinPerson { get; set; }
        public string OperationFromNo { get; set; }
        public string OperationToNo { get; set; }
        public string AddedByHuman { get; set; }
        public string MaxNumberOfHuman { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ManpowerInformation ManpowerInformation { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
