﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionActivityRoutineCheckedDetails
    {
        public long ProductionActivityRoutineCheckedDetailsId { get; set; }
        public long? ProductionActivityRoutineAppLineId { get; set; }
        public long? ProductionActivityRoutineAppId { get; set; }
        public int? ActivityInfoId { get; set; }
        public bool? IsCheckNoIssue { get; set; }
        public long? CheckedById { get; set; }
        public DateTime? CheckedDate { get; set; }
        public string CheckedComment { get; set; }
        public bool? IsCheckReferSupportDocument { get; set; }
        public byte[] CommentImage { get; set; }
        public string CommentImageType { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public long? RoutineStatusId { get; set; }
        public long? RoutineResultId { get; set; }

        public virtual CodeMaster ActivityInfo { get; set; }
        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser CheckedBy { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProductionActivityRoutineApp ProductionActivityRoutineApp { get; set; }
        public virtual ProductionActivityRoutineAppLine ProductionActivityRoutineAppLine { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
