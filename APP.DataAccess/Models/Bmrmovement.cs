﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Bmrmovement
    {
        public Bmrmovement()
        {
            BmrmovementLine = new HashSet<BmrmovementLine>();
        }

        public long BmrmovementId { get; set; }
        public bool? ShipReceivedStatus { get; set; }
        public long? ShipToId { get; set; }
        public long? BmrstatusId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterDetail Bmrstatus { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Department ShipTo { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<BmrmovementLine> BmrmovementLine { get; set; }
    }
}
