﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmailActivityCatgorys
    {
        public long Id { get; set; }
        public long TopicId { get; set; }
        public string Name { get; set; }
        public long? GroupTag { get; set; }
        public long? CategoryTag { get; set; }
        public long? ActionTag { get; set; }
        public string Description { get; set; }
        public int? StatusCodeId { get; set; }
        public int? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public int? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public bool? IsTagDelete { get; set; }
        public string ActivityType { get; set; }
        public string UserTag { get; set; }
    }
}
