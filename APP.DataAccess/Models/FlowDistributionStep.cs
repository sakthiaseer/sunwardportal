﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FlowDistributionStep
    {
        public long FlowDistributionStepId { get; set; }
        public long FlowDistributionDetailId { get; set; }
        public long FlowDistributionId { get; set; }
        public long DynamicFlowStepId { get; set; }
        public string LoopToTypeId { get; set; }
        public string LoopToId { get; set; }

        public virtual DynamicFlowStep DynamicFlowStep { get; set; }
        public virtual FlowDistribution FlowDistribution { get; set; }
        public virtual FlowDistributionDetail FlowDistributionDetail { get; set; }
    }
}
