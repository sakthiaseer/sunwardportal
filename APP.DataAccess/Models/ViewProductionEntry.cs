﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewProductionEntry
    {
        public string LocationName { get; set; }
        public string ProductionOrderNo { get; set; }
        public string Deascription { get; set; }
        public string RoomStatus { get; set; }
        public string RoomStatus1 { get; set; }
        public string BatchNo { get; set; }
        public DateTime? StartDate { get; set; }
        public string Name { get; set; }
        public DateTime? ActualStartDate { get; set; }
        public string AddedBy { get; set; }
        public long ProductionEntryId { get; set; }
    }
}
