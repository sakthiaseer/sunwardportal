﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class IpirissueTitleMultiple
    {
        public long IpirissueTitleMultipleId { get; set; }
        public long? Ipirid { get; set; }
        public long? IssueTitleId { get; set; }
        public long? IpirissueMobileActionId { get; set; }

        public virtual Ipir Ipir { get; set; }
        public virtual IpirmobileAction IpirissueMobileAction { get; set; }
    }
}
