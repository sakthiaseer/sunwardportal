﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class BaseUom
    {
        public BaseUom()
        {
            ItemMaster = new HashSet<ItemMaster>();
            QuotationLine = new HashSet<QuotationLine>();
        }

        public long Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ItemMaster> ItemMaster { get; set; }
        public virtual ICollection<QuotationLine> QuotationLine { get; set; }
    }
}
