﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TransferPermissionLog
    {
        public long TransferPermissionLogId { get; set; }
        public string TableName { get; set; }
        public long? PrimaryTableId { get; set; }
        public long? PreviousUserId { get; set; }
        public long? CurrentUserId { get; set; }
        public long? AddedByUserId { get; set; }
        public string Type { get; set; }
        public DateTime? AddedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser CurrentUser { get; set; }
        public virtual ApplicationUser PreviousUser { get; set; }
    }
}
