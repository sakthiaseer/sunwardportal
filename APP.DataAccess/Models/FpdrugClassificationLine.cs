﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FpdrugClassificationLine
    {
        public long FpdrugClassificationLineId { get; set; }
        public long? FpdrugClassificationId { get; set; }
        public int? PurposeId { get; set; }
        public long? RequirementId { get; set; }
        public string TimeRequire { get; set; }
        public long? CalculationPointId { get; set; }
        public string WiLink { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual FpdrugClassification FpdrugClassification { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster Purpose { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
