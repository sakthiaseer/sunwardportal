﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewSoSalesOrder
    {
        public long SoSalesOrderId { get; set; }
        public DateTime? DocumentDate { get; set; }
        public long? CompanyId { get; set; }
        public string SignOrderNo { get; set; }
        public long? SoCustomerId { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? PrioityDeliveryDate { get; set; }
        public string Remark { get; set; }
        public int? StatusCodeId { get; set; }
        public Guid? SessionId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string StatusCode { get; set; }
        public string AddedBy { get; set; }
        public string ModifiedBy { get; set; }
        public int CodeId { get; set; }
        public string ShipCode { get; set; }
        public string CustomerName { get; set; }
        public string AssignToRep { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string PostCode { get; set; }
        public string Channel { get; set; }
        public long? SoCustomerBillingAddressId { get; set; }
        public long? SoCustomerShipingAddressId { get; set; }
        public string PlantCode { get; set; }
        public string PlantDescription { get; set; }
    }
}
