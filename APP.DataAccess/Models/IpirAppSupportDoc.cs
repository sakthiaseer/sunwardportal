﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class IpirAppSupportDoc
    {
        public long IpirAppSupportDocId { get; set; }
        public long? IpirAppId { get; set; }
        public Guid? SessionId { get; set; }
        public long? DocumentId { get; set; }
        public string Type { get; set; }

        public virtual Documents Document { get; set; }
        public virtual IpirApp IpirApp { get; set; }
    }
}
