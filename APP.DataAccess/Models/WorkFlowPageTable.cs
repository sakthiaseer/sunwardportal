﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WorkFlowPageTable
    {
        public long WorkFlowPageTableId { get; set; }
        public long WorkFlowPageFieldId { get; set; }
        public string FieldName { get; set; }
        public int? DataType { get; set; }
        public string FieldValue { get; set; }
        public bool? IsRequired { get; set; }

        public virtual WorkFlowPageField WorkFlowPageField { get; set; }
    }
}
