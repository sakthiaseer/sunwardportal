﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SocustomersDelivery
    {
        public SocustomersDelivery()
        {
            SobyCustomerModeDelivery = new HashSet<SobyCustomerModeDelivery>();
            SobyCustomersTypeOfPermit = new HashSet<SobyCustomersTypeOfPermit>();
        }

        public long SocustomersDeliveryId { get; set; }
        public long? SocustomersId { get; set; }
        public bool? RequireSpecialPermit { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual SobyCustomers Socustomers { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<SobyCustomerModeDelivery> SobyCustomerModeDelivery { get; set; }
        public virtual ICollection<SobyCustomersTypeOfPermit> SobyCustomersTypeOfPermit { get; set; }
    }
}
