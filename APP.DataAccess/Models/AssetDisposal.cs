﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AssetDisposal
    {
        public long AssetDisposalId { get; set; }
        public long? AssetId { get; set; }
        public int? DisposalTypeId { get; set; }
        public decimal? DisposalQty { get; set; }
        public DateTime? DisposalDate { get; set; }
        public decimal? Amount { get; set; }
        public string Reason { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual AssetMaster Asset { get; set; }
        public virtual CodeMaster DisposalType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
