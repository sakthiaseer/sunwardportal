﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SelfTestPrivate
    {
        public long PrivateId { get; set; }
        public long? SelfTestId { get; set; }
        public long? EmployeeId { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual SelfTest SelfTest { get; set; }
    }
}
