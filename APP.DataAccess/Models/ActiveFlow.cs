﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ActiveFlow
    {
        public ActiveFlow()
        {
            ActiveFlowDetails = new HashSet<ActiveFlowDetails>();
        }

        public long ActiveFlowId { get; set; }
        public long DynamicFlowId { get; set; }
        public long InitiatorId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? StatusCodeId { get; set; }

        public virtual DynamicFlow DynamicFlow { get; set; }
        public virtual ApplicationUser Initiator { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ActiveFlowDetails> ActiveFlowDetails { get; set; }
    }
}
