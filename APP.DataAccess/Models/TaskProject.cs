﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TaskProject
    {
        public long TaskProjectId { get; set; }
        public long? TaskId { get; set; }
        public long? ProjectId { get; set; }

        public virtual Project Project { get; set; }
        public virtual TaskMaster Task { get; set; }
    }
}
