﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AssetTicket
    {
        public long AssetTicketId { get; set; }
        public long? AssetId { get; set; }
        public int? UrgencyStatus { get; set; }
        public int? ImpactStatus { get; set; }
        public int? PriorityStatus { get; set; }
        public string Description { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual AssetMaster Asset { get; set; }
        public virtual CodeMaster ImpactStatusNavigation { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster PriorityStatusNavigation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CodeMaster UrgencyStatusNavigation { get; set; }
    }
}
