﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewConsumptionEntry
    {
        public string ProdOrderNo { get; set; }
        public string Description { get; set; }
        public string SubLotNo { get; set; }
        public string AddedBy { get; set; }
        public DateTime? AddedDate { get; set; }
        public long ConsumptionEntryId { get; set; }
    }
}
