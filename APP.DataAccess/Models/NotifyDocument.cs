﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NotifyDocument
    {
        public NotifyDocument()
        {
            InverseQuoteNotify = new HashSet<NotifyDocument>();
            NotifyDocumentUserGroup = new HashSet<NotifyDocumentUserGroup>();
            NotifyLongNotes = new HashSet<NotifyLongNotes>();
        }

        public long NotifyDocumentId { get; set; }
        public long? UserId { get; set; }
        public string Remarks { get; set; }
        public long? DocumentId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public bool? IsUrgent { get; set; }
        public bool? IsQuote { get; set; }
        public long? QuoteNotifyId { get; set; }
        public bool? IsSenderClose { get; set; }
        public DateTime? DueDate { get; set; }
        public string NotifyType { get; set; }
        public long? ProductionActivityAppLineId { get; set; }
        public long? IpirReportId { get; set; }
        public long? IpirReportAssignmentId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Documents Document { get; set; }
        public virtual IpirReport IpirReport { get; set; }
        public virtual IpirReportAssignment IpirReportAssignment { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProductionActivityAppLine ProductionActivityAppLine { get; set; }
        public virtual NotifyDocument QuoteNotify { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual Employee User { get; set; }
        public virtual ICollection<NotifyDocument> InverseQuoteNotify { get; set; }
        public virtual ICollection<NotifyDocumentUserGroup> NotifyDocumentUserGroup { get; set; }
        public virtual ICollection<NotifyLongNotes> NotifyLongNotes { get; set; }
    }
}
