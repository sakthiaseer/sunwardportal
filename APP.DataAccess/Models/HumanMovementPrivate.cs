﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class HumanMovementPrivate
    {
        public long PrivateId { get; set; }
        public long? HumanMovementId { get; set; }
        public long? EmployeeId { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual HumanMovement HumanMovement { get; set; }
    }
}
