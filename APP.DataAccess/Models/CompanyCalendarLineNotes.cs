﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CompanyCalendarLineNotes
    {
        public CompanyCalendarLineNotes()
        {
            CompanyCalendarLineNotesUser = new HashSet<CompanyCalendarLineNotesUser>();
            InverseCalendarNotes = new HashSet<CompanyCalendarLineNotes>();
        }

        public long CompanyCalendarLineNotesId { get; set; }
        public long? CompanyCalendarLineId { get; set; }
        public string NotesDetails { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? CalendarNotesId { get; set; }
        public bool? IsUrgent { get; set; }
        public bool? IsQuote { get; set; }
        public Guid? SessionId { get; set; }
        public long? UserId { get; set; }
        public bool? IsShowInfoAllUser { get; set; }
        public int? StatusCodeId { get; set; }
        public string Subject { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CompanyCalendarLineNotes CalendarNotes { get; set; }
        public virtual CompanyCalendarLine CompanyCalendarLine { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual ICollection<CompanyCalendarLineNotesUser> CompanyCalendarLineNotesUser { get; set; }
        public virtual ICollection<CompanyCalendarLineNotes> InverseCalendarNotes { get; set; }
    }
}
