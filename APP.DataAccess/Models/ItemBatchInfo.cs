﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ItemBatchInfo
    {
        public ItemBatchInfo()
        {
            FmglobalLineItem = new HashSet<FmglobalLineItem>();
        }

        public long ItemBatchId { get; set; }
        public long ItemId { get; set; }
        public long? CompanyId { get; set; }
        public string LocationCode { get; set; }
        public string BatchNo { get; set; }
        public string LotNo { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime? ManufacturingDate { get; set; }
        public decimal? QuantityOnHand { get; set; }
        public decimal? NavQuantity { get; set; }
        public decimal? IssueQuantity { get; set; }
        public decimal? BalanceQuantity { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public decimal? StockOutBatchConfirm { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<FmglobalLineItem> FmglobalLineItem { get; set; }
    }
}
