﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SalesAdhocNovate
    {
        public SalesAdhocNovate()
        {
            SaleAdhocNovateOrder = new HashSet<SaleAdhocNovateOrder>();
        }

        public long SalesAdhocNovateId { get; set; }
        public long? SalesOrderEntryId { get; set; }
        public DateTime? Date { get; set; }
        public long? ItemId { get; set; }
        public long? FromCustomerId { get; set; }
        public DateTime? FromStartMonth { get; set; }
        public decimal? FromTotalQty { get; set; }
        public long? ToCustomerId { get; set; }
        public DateTime? ToStartMonth { get; set; }
        public decimal? ToTotalQty { get; set; }
        public decimal? IntoNoOfLots { get; set; }
        public string NewPono { get; set; }
        public Guid? SessionId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CompanyListing FromCustomer { get; set; }
        public virtual SocustomersItemCrossReference Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual SalesOrderEntry SalesOrderEntry { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CompanyListing ToCustomer { get; set; }
        public virtual ICollection<SaleAdhocNovateOrder> SaleAdhocNovateOrder { get; set; }
    }
}
