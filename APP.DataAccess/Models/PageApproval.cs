﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PageApproval
    {
        public long PageApproverId { get; set; }
        public long PageId { get; set; }
        public long ApproverId { get; set; }
        public DateTime ApprovedDate { get; set; }
        public int StatusCodeId { get; set; }

        public virtual ApplicationUser Approver { get; set; }
        public virtual WikiPage Page { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
