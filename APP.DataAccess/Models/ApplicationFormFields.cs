﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApplicationFormFields
    {
        public ApplicationFormFields()
        {
            ApplicationFormPermission = new HashSet<ApplicationFormPermission>();
        }

        public long ApplicationFormFieldId { get; set; }
        public long ApplicationFormId { get; set; }
        public string ColumnName { get; set; }
        public string DisplayName { get; set; }
        public int? StatusCodeId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedByUserId { get; set; }

        public virtual ApplicationForm ApplicationForm { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ApplicationFormPermission> ApplicationFormPermission { get; set; }
    }
}
