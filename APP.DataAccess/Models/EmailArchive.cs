﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmailArchive
    {
        public long Id { get; set; }
        public long? TopicId { get; set; }
        public long? UserId { get; set; }
        public long? IsArchive { get; set; }
    }
}
