﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ChatUser
    {
        public long ChatUserId { get; set; }
        public long UserId { get; set; }
        public bool? IsOnline { get; set; }
        public Guid? SessionId { get; set; }
        public string ConnectionId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedData { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
