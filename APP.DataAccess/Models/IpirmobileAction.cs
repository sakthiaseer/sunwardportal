﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class IpirmobileAction
    {
        public IpirmobileAction()
        {
            IpirissueRelatedMultiple = new HashSet<IpirissueRelatedMultiple>();
            IpirissueTitleMultiple = new HashSet<IpirissueTitleMultiple>();
        }

        public long IpirmobileActionId { get; set; }
        public long? IpirmobileId { get; set; }
        public int? IntendedActionId { get; set; }
        public bool? IsConfirmIpir { get; set; }
        public string IssueDescription { get; set; }
        public string Photo { get; set; }
        public long? AssignmentTo { get; set; }
        public int? AssignmentStatusId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster AssignmentStatus { get; set; }
        public virtual ApplicationUser AssignmentToNavigation { get; set; }
        public virtual CodeMaster IntendedAction { get; set; }
        public virtual Ipirmobile Ipirmobile { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<IpirissueRelatedMultiple> IpirissueRelatedMultiple { get; set; }
        public virtual ICollection<IpirissueTitleMultiple> IpirissueTitleMultiple { get; set; }
    }
}
