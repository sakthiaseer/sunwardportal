﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewGetEmployeeByName
    {
        public long EmployeeId { get; set; }
        public long? UserId { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }
        public string Status { get; set; }
        public string FirstName { get; set; }
    }
}
