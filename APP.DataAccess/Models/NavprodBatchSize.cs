﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavprodBatchSize
    {
        public long NavbatchSize { get; set; }
        public string RecipeName { get; set; }
        public string ItemDesc { get; set; }
        public int? ProdBatchSize { get; set; }
        public int? UnitQty { get; set; }
        public string Uom { get; set; }
        public int? ProdTime { get; set; }
    }
}
