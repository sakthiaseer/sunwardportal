﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormWorkFlowApprovedForm
    {
        public DynamicFormWorkFlowApprovedForm()
        {
            DynamicFormWorkFlowApprovedFormChanged = new HashSet<DynamicFormWorkFlowApprovedFormChanged>();
        }

        public long DynamicFormWorkFlowApprovedFormId { get; set; }
        public long? DynamicFormWorkFlowFormId { get; set; }
        public long? UserId { get; set; }
        public int? SortBy { get; set; }
        public bool? IsApproved { get; set; }
        public string ApprovedDescription { get; set; }
        public long? ApprovedByUserId { get; set; }
        public DateTime? ApprovedDate { get; set; }

        public virtual ApplicationUser ApprovedByUser { get; set; }
        public virtual DynamicFormWorkFlowForm DynamicFormWorkFlowForm { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual ICollection<DynamicFormWorkFlowApprovedFormChanged> DynamicFormWorkFlowApprovedFormChanged { get; set; }
    }
}
