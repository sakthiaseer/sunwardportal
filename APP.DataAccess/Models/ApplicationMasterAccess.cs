﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApplicationMasterAccess
    {
        public long ApplicationMasterAccessId { get; set; }
        public long? ApplicationMasterId { get; set; }
        public long? UserId { get; set; }
        public long? UserGroupId { get; set; }
        public long? LevelId { get; set; }
        public string AccessTypeFrom { get; set; }
        public string UserType { get; set; }
        public long? ApplicationMasterParentId { get; set; }

        public virtual ApplicationMaster ApplicationMaster { get; set; }
        public virtual ApplicationMasterParent ApplicationMasterParent { get; set; }
        public virtual LevelMaster Level { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual UserGroup UserGroup { get; set; }
    }
}
