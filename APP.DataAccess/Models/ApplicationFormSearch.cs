﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApplicationFormSearch
    {
        public long ApplicationSearchFormId { get; set; }
        public long? MainFormId { get; set; }
        public long? ApplicationFormId { get; set; }
        public long? ParentId { get; set; }
        public string ColumnName { get; set; }
        public string DisplayName { get; set; }
        public bool? IsReadOnly { get; set; }
        public bool? IsVisible { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationForm ApplicationForm { get; set; }
        public virtual ApplicationForm MainForm { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationForm Parent { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
