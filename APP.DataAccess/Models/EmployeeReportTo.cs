﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmployeeReportTo
    {
        public long EmployeeReportToId { get; set; }
        public long? EmployeeId { get; set; }
        public long? ReportToId { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual ApplicationUser ReportTo { get; set; }
    }
}
