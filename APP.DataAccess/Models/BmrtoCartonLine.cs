﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class BmrtoCartonLine
    {
        public long BmrtoCartonLineId { get; set; }
        public long? BmrtoCartonId { get; set; }
        public string Bmrno { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual BmrtoCarton BmrtoCarton { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
