﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WorkFlowPageBlock
    {
        public WorkFlowPageBlock()
        {
            ActiveFlowFormValue = new HashSet<ActiveFlowFormValue>();
            DynamicFlowProgress = new HashSet<DynamicFlowProgress>();
            DynamicFlowStep = new HashSet<DynamicFlowStep>();
            WorkFlowPageBlockAccess = new HashSet<WorkFlowPageBlockAccess>();
            WorkFlowPageField = new HashSet<WorkFlowPageField>();
        }

        public long WorkFlowPageBlockId { get; set; }
        public long? WorkFlowPageId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long? TableId { get; set; }
        public string TableName { get; set; }
        public string Code { get; set; }
        public int? CategoryId { get; set; }

        public virtual CodeMaster Category { get; set; }
        public virtual WorkFlowPages WorkFlowPage { get; set; }
        public virtual ICollection<ActiveFlowFormValue> ActiveFlowFormValue { get; set; }
        public virtual ICollection<DynamicFlowProgress> DynamicFlowProgress { get; set; }
        public virtual ICollection<DynamicFlowStep> DynamicFlowStep { get; set; }
        public virtual ICollection<WorkFlowPageBlockAccess> WorkFlowPageBlockAccess { get; set; }
        public virtual ICollection<WorkFlowPageField> WorkFlowPageField { get; set; }
    }
}
