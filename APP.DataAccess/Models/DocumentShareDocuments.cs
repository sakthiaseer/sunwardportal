﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DocumentShareDocuments
    {
        public long DocumentShareDocumentsId { get; set; }
        public long? DocumentShareId { get; set; }
        public long? DocumentId { get; set; }

        public virtual Documents Document { get; set; }
        public virtual DocumentShare DocumentShare { get; set; }
    }
}
