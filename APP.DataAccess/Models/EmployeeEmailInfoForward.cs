﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmployeeEmailInfoForward
    {
        public long EmployeeEmailInfoForwardId { get; set; }
        public long? EmployeeEmailInfoId { get; set; }
        public string EmailAddress { get; set; }

        public virtual EmployeeEmailInfo EmployeeEmailInfo { get; set; }
    }
}
