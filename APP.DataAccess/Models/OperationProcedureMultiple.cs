﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class OperationProcedureMultiple
    {
        public long OperationProcedureMultipleId { get; set; }
        public long? ProductionMachineId { get; set; }
        public long? OperationProcedureId { get; set; }

        public virtual ProductionMachine ProductionMachine { get; set; }
    }
}
