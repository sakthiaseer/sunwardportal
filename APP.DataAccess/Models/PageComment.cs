﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PageComment
    {
        public long PageCommentId { get; set; }
        public long PageId { get; set; }
        public long UserId { get; set; }
        public string Comment { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsEdit { get; set; }

        public virtual WikiPage Page { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
