﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FinishProduct
    {
        public FinishProduct()
        {
            CriticalstepandIntermediate = new HashSet<CriticalstepandIntermediate>();
            FinishProductEquivalentBrandCategory = new HashSet<FinishProductEquivalentBrandCategory>();
            FinishProductGeneralInfo = new HashSet<FinishProductGeneralInfo>();
            FinishProductLine = new HashSet<FinishProductLine>();
            FinisihProductGenericCategory = new HashSet<FinisihProductGenericCategory>();
            ManufacturingProcess = new HashSet<ManufacturingProcess>();
            NavProductMaster = new HashSet<NavProductMaster>();
            PharmacologicalCategory = new HashSet<PharmacologicalCategory>();
            ProductChemicalSubGroup = new HashSet<ProductChemicalSubGroup>();
        }

        public long FinishProductId { get; set; }
        public int? RegisterationCodeId { get; set; }
        public long? ManufacturingSiteId { get; set; }
        public long? DosageFormId { get; set; }
        public long? ProductId { get; set; }
        public string RegistrationFileName { get; set; }
        public long? GenericNameId { get; set; }
        public string EquvalentBrandName { get; set; }
        public long? ChemicalSubgroup { get; set; }
        public long? PharmacologicalCategoryId { get; set; }
        public long? RegistrationProductCategory { get; set; }
        public long? DrugClassificationId { get; set; }
        public string Atccode { get; set; }
        public string Gtinno { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster RegisterationCode { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<CriticalstepandIntermediate> CriticalstepandIntermediate { get; set; }
        public virtual ICollection<FinishProductEquivalentBrandCategory> FinishProductEquivalentBrandCategory { get; set; }
        public virtual ICollection<FinishProductGeneralInfo> FinishProductGeneralInfo { get; set; }
        public virtual ICollection<FinishProductLine> FinishProductLine { get; set; }
        public virtual ICollection<FinisihProductGenericCategory> FinisihProductGenericCategory { get; set; }
        public virtual ICollection<ManufacturingProcess> ManufacturingProcess { get; set; }
        public virtual ICollection<NavProductMaster> NavProductMaster { get; set; }
        public virtual ICollection<PharmacologicalCategory> PharmacologicalCategory { get; set; }
        public virtual ICollection<ProductChemicalSubGroup> ProductChemicalSubGroup { get; set; }
    }
}
