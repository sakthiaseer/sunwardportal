﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmailDueDateHistory
    {
        public long Id { get; set; }
        public long? ConversationId { get; set; }
        public long? UserId { get; set; }
        public DateTime? DueDate { get; set; }
        public int? NoOfDays { get; set; }
        public DateTime? ExpiryDueDate { get; set; }
        public DateTime? ExtendDueDate { get; set; }
        public int? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
    }
}
