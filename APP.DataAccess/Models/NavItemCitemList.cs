﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavItemCitemList
    {
        public long NavItemCitemId { get; set; }
        public long? NavItemId { get; set; }
        public long? NavItemCustomerItemId { get; set; }

        public virtual Navitems NavItem { get; set; }
        public virtual Acitems NavItemCustomerItem { get; set; }
    }
}
