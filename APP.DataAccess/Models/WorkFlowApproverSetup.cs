﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WorkFlowApproverSetup
    {
        public long ApproverSetupId { get; set; }
        public long? UserId { get; set; }
        public long? ApproverId { get; set; }
        public long? WorkFlowUserGroupId { get; set; }
        public long? ModuleId { get; set; }
        public long? DepartmentId { get; set; }
        public long? CategoryId { get; set; }
        public bool? ApprovalAdmin { get; set; }
        public int Sequence { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser Approver { get; set; }
        public virtual Category Category { get; set; }
        public virtual Department Department { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Modules Module { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual WorkFlowUserGroup WorkFlowUserGroup { get; set; }
    }
}
