﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TenderPeriodPricing
    {
        public TenderPeriodPricing()
        {
            TenderPeriodPricingLine = new HashSet<TenderPeriodPricingLine>();
        }

        public long TenderPeriodPricingId { get; set; }
        public long? CustomerId { get; set; }
        public long? ContractId { get; set; }
        public DateTime? EffectiveMonth { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual SalesOrderEntry Contract { get; set; }
        public virtual CompanyListing Customer { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<TenderPeriodPricingLine> TenderPeriodPricingLine { get; set; }
    }
}
