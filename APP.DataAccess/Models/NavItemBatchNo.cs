﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavItemBatchNo
    {
        public NavItemBatchNo()
        {
            FmglobalLineItem = new HashSet<FmglobalLineItem>();
        }

        public long NavItemBatchNoId { get; set; }
        public long? ItemId { get; set; }
        public string ItemNo { get; set; }
        public string BatchNo { get; set; }
        public string BatchSize { get; set; }
        public string Description { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<FmglobalLineItem> FmglobalLineItem { get; set; }
    }
}
