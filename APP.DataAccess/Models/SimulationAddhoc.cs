﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SimulationAddhoc
    {
        public long SimualtionAddhocId { get; set; }
        public string DocumantType { get; set; }
        public string SelltoCustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string Categories { get; set; }
        public string DocumentNo { get; set; }
        public string ExternalDocNo { get; set; }
        public long? ItemId { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string Description1 { get; set; }
        public decimal? OutstandingQty { get; set; }
        public DateTime? PromisedDate { get; set; }
        public DateTime? ShipmentDate { get; set; }
        public string Uomcode { get; set; }

        public virtual Navitems Item { get; set; }
    }
}
