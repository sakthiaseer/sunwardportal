﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SobyCustomersSalesAddress
    {
        public SobyCustomersSalesAddress()
        {
            SalesOrder = new HashSet<SalesOrder>();
        }

        public long SobyCustomersSalesAddressId { get; set; }
        public long? SobyCustomersId { get; set; }
        public long? SobyCustomersMasterAddressId { get; set; }

        public virtual SobyCustomers SobyCustomers { get; set; }
        public virtual SobyCustomersMasterAddress SobyCustomersMasterAddress { get; set; }
        public virtual ICollection<SalesOrder> SalesOrder { get; set; }
    }
}
