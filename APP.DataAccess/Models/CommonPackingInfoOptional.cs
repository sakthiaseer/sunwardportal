﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CommonPackingInfoOptional
    {
        public long CommonPackingInfoOptionalId { get; set; }
        public long? PackingInformationId { get; set; }
        public long? SetInformationId { get; set; }

        public virtual CommonPackingInformation PackingInformation { get; set; }
        public virtual CommonPackagingSetInfo SetInformation { get; set; }
    }
}
