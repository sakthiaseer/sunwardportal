﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProjectedDeliveryBlanketOthers
    {
        public long ProjectedDeliverySalesOrderLineId { get; set; }
        public long? PurchaseItemSalesEntryLineId { get; set; }
        public long? FrequencyId { get; set; }
        public int? PerFrequencyQty { get; set; }
        public DateTime? StartDate { get; set; }
        public bool? IsQtyReference { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? UomId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual PurchaseItemSalesEntryLine PurchaseItemSalesEntryLine { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
