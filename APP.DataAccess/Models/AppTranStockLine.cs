﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AppTranStockLine
    {
        public int Id { get; set; }
        public int? AppTranStockId { get; set; }
        public string ScanActionStatus { get; set; }
        public string DocRequisitionNo { get; set; }
        public string ActionStatus { get; set; }
        public string LabelNo { get; set; }
        public string ItemNo { get; set; }
        public string LotNo { get; set; }
        public decimal? Qty { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual AppTranStock AppTranStock { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
