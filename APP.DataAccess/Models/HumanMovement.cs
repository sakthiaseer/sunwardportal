﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class HumanMovement
    {
        public HumanMovement()
        {
            HumanMovementAction = new HashSet<HumanMovementAction>();
            HumanMovementCompanyRelated = new HashSet<HumanMovementCompanyRelated>();
            HumanMovementPrivate = new HashSet<HumanMovementPrivate>();
        }

        public long HumanMovementId { get; set; }
        public DateTime? DateAndTime { get; set; }
        public string Language { get; set; }
        public string Location { get; set; }
        public bool? IsSwstaff { get; set; }
        public long? EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string ContactNo { get; set; }
        public string VisitingPurpose { get; set; }
        public string PurposeOfVisit { get; set; }
        public long? CompanyId { get; set; }
        public string NameOfCompany { get; set; }
        public string CompanyRelatedpurposeOfVisit { get; set; }
        public bool? HignFever { get; set; }
        public bool? MildFever { get; set; }
        public bool? SoreThroat { get; set; }
        public bool? MuscleJointPain { get; set; }
        public bool? Headache { get; set; }
        public bool? ShortnessOfBread { get; set; }
        public bool? Diarrhea { get; set; }
        public bool? LossOfVoice { get; set; }
        public bool? Phlegm { get; set; }
        public bool? Flu { get; set; }
        public bool? Gathering { get; set; }
        public string GatheringDetails { get; set; }
        public bool? TravellingCountry { get; set; }
        public string TravellingCountryDetails { get; set; }
        public bool? ContactPatients { get; set; }
        public string ContactPatientsDetails { get; set; }
        public decimal? Temperature { get; set; }
        public Guid? SessionId { get; set; }
        public bool? NauseaVomiting { get; set; }
        public bool? Fatigue { get; set; }
        public bool? Conjunctivits { get; set; }
        public bool? LossOfTasteSmell { get; set; }
        public bool? LossOfApetite { get; set; }
        public bool? RunnyStuffyNose { get; set; }
        public bool? Sneezing { get; set; }
        public bool? Chills { get; set; }
        public bool? NoForAllAbove { get; set; }
        public bool? Coughing { get; set; }
        public int? MovementStatusId { get; set; }

        public virtual CompanyListing Company { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual CodeMaster MovementStatus { get; set; }
        public virtual ICollection<HumanMovementAction> HumanMovementAction { get; set; }
        public virtual ICollection<HumanMovementCompanyRelated> HumanMovementCompanyRelated { get; set; }
        public virtual ICollection<HumanMovementPrivate> HumanMovementPrivate { get; set; }
    }
}
