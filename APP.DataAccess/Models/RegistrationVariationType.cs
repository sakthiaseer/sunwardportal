﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class RegistrationVariationType
    {
        public long RegistrationVariationTypeId { get; set; }
        public long? RegistrationVariationId { get; set; }
        public long? VariationTypeId { get; set; }

        public virtual RegistrationVariation RegistrationVariation { get; set; }
    }
}
