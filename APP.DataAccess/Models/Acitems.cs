﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Acitems
    {
        public Acitems()
        {
            DistStockBalance = new HashSet<DistStockBalance>();
            NavItemCitemList = new HashSet<NavItemCitemList>();
            PsbreportCommentDetail = new HashSet<PsbreportCommentDetail>();
            RequestAcline = new HashSet<RequestAcline>();
        }

        public long DistAcid { get; set; }
        public long? CompanyId { get; set; }
        public long? CustomerId { get; set; }
        public string DistName { get; set; }
        public string ItemGroup { get; set; }
        public string Steriod { get; set; }
        public string ShelfLife { get; set; }
        public string Quota { get; set; }
        public string Status { get; set; }
        public string ItemDesc { get; set; }
        public string PackSize { get; set; }
        public decimal? Acqty { get; set; }
        public DateTime? Acmonth { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ItemNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual Navcustomer Customer { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<DistStockBalance> DistStockBalance { get; set; }
        public virtual ICollection<NavItemCitemList> NavItemCitemList { get; set; }
        public virtual ICollection<PsbreportCommentDetail> PsbreportCommentDetail { get; set; }
        public virtual ICollection<RequestAcline> RequestAcline { get; set; }
    }
}
