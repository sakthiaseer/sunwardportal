﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class MethodTemplateRoutineLanguage
    {
        public long MethodTemplateRoutineLanguageId { get; set; }
        public long? MethodTemplateRoutineId { get; set; }
        public long? LanguageId { get; set; }

        public virtual MethodTemplateRoutine MethodTemplateRoutine { get; set; }
    }
}
