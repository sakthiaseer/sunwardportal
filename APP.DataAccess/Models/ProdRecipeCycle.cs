﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProdRecipeCycle
    {
        public long ProdRecipeCycleId { get; set; }
        public long? ProdCycleId { get; set; }
        public long? ItemTypeId { get; set; }
        public long? SalesCategoryId { get; set; }
        public long? MethodCodeId { get; set; }
        public long? BatchSizeId { get; set; }
        public int? NoOfTickets { get; set; }
        public string PlanningMonth { get; set; }
        public long? ResonForChangeId { get; set; }
        public Guid? SessionId { get; set; }
        public string RreasonForChange { get; set; }
        public int? CycleMethodId { get; set; }
        public DateTime? SpecificMonth { get; set; }
        public string ByMonth { get; set; }
        public DateTime? SpecificMonthOfTheYear { get; set; }
        public bool? MustFollow { get; set; }
        public string Notes { get; set; }

        public virtual NavmethodCodeBatch BatchSize { get; set; }
        public virtual CodeMaster CycleMethod { get; set; }
        public virtual NavMethodCode MethodCode { get; set; }
        public virtual ProductionCycle ProdCycle { get; set; }
        public virtual NavSaleCategory SalesCategory { get; set; }
    }
}
