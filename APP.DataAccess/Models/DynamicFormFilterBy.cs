﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormFilterBy
    {
        public long DynamicFormFilterById { get; set; }
        public long DynamicFormFilterId { get; set; }
        public string FilterDisplayName { get; set; }
        public string FromFilterFieldName { get; set; }
        public string FilterTableName { get; set; }
        public string ToDropDownFieldId { get; set; }
        public string ToDisplayDropDownName { get; set; }
        public string ToDisplayDropDownDescription { get; set; }
        public string FilterDataType { get; set; }
        public string ApplicationMasterCodeId { get; set; }
        public long? AttributeHeaderDataSourceId { get; set; }
        public long? DynamicFormFilterParentId { get; set; }
        public int? SortBy { get; set; }

        public virtual AttributeHeaderDataSource AttributeHeaderDataSource { get; set; }
    }
}
