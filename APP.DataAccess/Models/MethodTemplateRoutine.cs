﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class MethodTemplateRoutine
    {
        public MethodTemplateRoutine()
        {
            MethodTemplateRoutineLanguage = new HashSet<MethodTemplateRoutineLanguage>();
            MethodTemplateRoutineLine = new HashSet<MethodTemplateRoutineLine>();
            StandardManufacturingProcess = new HashSet<StandardManufacturingProcess>();
        }

        public long MethodTemplateRoutineId { get; set; }
        public int? ManufacturingTypeId { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public string MethodTemplateNo { get; set; }
        public string TemplateName { get; set; }
        public string MachineGrouping { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster ManufacturingType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<MethodTemplateRoutineLanguage> MethodTemplateRoutineLanguage { get; set; }
        public virtual ICollection<MethodTemplateRoutineLine> MethodTemplateRoutineLine { get; set; }
        public virtual ICollection<StandardManufacturingProcess> StandardManufacturingProcess { get; set; }
    }
}
