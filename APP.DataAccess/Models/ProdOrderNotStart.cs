﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProdOrderNotStart
    {
        public long ProdNotStartId { get; set; }
        public long? CompanyId { get; set; }
        public string ProdOrderNo { get; set; }
        public string ItemNo { get; set; }
        public string PackSize { get; set; }
        public decimal? Quantity { get; set; }
        public DateTime? StartDate { get; set; }
    }
}
