﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CompanyCalendarLineLocation
    {
        public long CompanyCalendarLineLocationId { get; set; }
        public long? CompanyCalendarLineId { get; set; }
        public long? LocationId { get; set; }

        public virtual CompanyCalendarLine CompanyCalendarLine { get; set; }
        public virtual Ictmaster Location { get; set; }
    }
}
