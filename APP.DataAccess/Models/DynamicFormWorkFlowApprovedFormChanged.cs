﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormWorkFlowApprovedFormChanged
    {
        public long DynamicFormWorkFlowApprovedFormChangedId { get; set; }
        public long? DynamicFormWorkFlowApprovedFormId { get; set; }
        public long? UserId { get; set; }
        public bool? IsApprovedStatus { get; set; }

        public virtual DynamicFormWorkFlowApprovedForm DynamicFormWorkFlowApprovedForm { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
