﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FinishProductExcipientLine
    {
        public long FinishProductExcipientLineId { get; set; }
        public long? FinishProductExcipientId { get; set; }
        public long? ProcessId { get; set; }
        public long? MaterialId { get; set; }
        public decimal? Strength { get; set; }
        public long? Uomid { get; set; }
        public long? FunctionId { get; set; }
        public string Source { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? OperationProcessId { get; set; }
        public long? GenericItemNameId { get; set; }
        public long? GeneralRawMaterialNameId { get; set; }
        public long? NavisionDataBaseId { get; set; }
        public decimal? Dosage { get; set; }
        public long? DosageUnitsId { get; set; }
        public long? PerDosageId { get; set; }
        public decimal? Overage { get; set; }
        public long? OverageUnitsId { get; set; }
        public long? DosageUnitsId1 { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual FinishProductExcipient FinishProductExcipient { get; set; }
        public virtual ProductionMaterial GeneralRawMaterialName { get; set; }
        public virtual GenericItemNameListing GenericItemName { get; set; }
        public virtual Navitems Material { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
