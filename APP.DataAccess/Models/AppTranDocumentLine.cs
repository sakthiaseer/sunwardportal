﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AppTranDocumentLine
    {
        public AppTranDocumentLine()
        {
            AppTranLfromLtoLine = new HashSet<AppTranLfromLtoLine>();
        }

        public long Id { get; set; }
        public long? AppTranLfromLtoId { get; set; }
        public long? DocumentTypeId { get; set; }
        public string DocumentNo { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual AppTranLfromLto AppTranLfromLto { get; set; }
        public virtual ApplicationMasterDetail DocumentType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<AppTranLfromLtoLine> AppTranLfromLtoLine { get; set; }
    }
}
