﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ItemClassificationPermission
    {
        public long ItemClassificationPermissionId { get; set; }
        public long? ApplicationCodeId { get; set; }
        public long? ItemClassificationId { get; set; }
        public long? UserGroupId { get; set; }
        public long? UserId { get; set; }
        public string UserType { get; set; }

        public virtual ApplicationFormMasterPermission ApplicationCode { get; set; }
        public virtual ItemClassificationMaster ItemClassification { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual UserGroup UserGroup { get; set; }
    }
}
