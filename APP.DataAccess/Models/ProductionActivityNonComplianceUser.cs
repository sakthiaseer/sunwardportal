﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionActivityNonComplianceUser
    {
        public long ProductionActivityNonComplianceUserId { get; set; }
        public long? ProductionActivityNonComplianceId { get; set; }
        public long? UserId { get; set; }

        public virtual ProductionActivityNonCompliance ProductionActivityNonCompliance { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
