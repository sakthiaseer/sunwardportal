﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormData
    {
        public DynamicFormData()
        {
            DynamicFormApproved = new HashSet<DynamicFormApproved>();
            DynamicFormDataSectionLock = new HashSet<DynamicFormDataSectionLock>();
            DynamicFormDataUpload = new HashSet<DynamicFormDataUpload>();
            DynamicFormItemLine = new HashSet<DynamicFormItemLine>();
            DynamicFormWorkFlowForm = new HashSet<DynamicFormWorkFlowForm>();
            DynamicFormWorkFlowSection = new HashSet<DynamicFormWorkFlowSection>();
            InverseDynamicFormDataGrid = new HashSet<DynamicFormData>();
        }

        public long DynamicFormDataId { get; set; }
        public long? DynamicFormId { get; set; }
        public Guid? SessionId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string DynamicFormItem { get; set; }
        public bool? IsSendApproval { get; set; }
        public Guid? FileProfileSessionId { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileNo { get; set; }
        public long? DynamicFormDataGridId { get; set; }
        public bool? IsDeleted { get; set; }
        public long? SortOrderByNo { get; set; }
        public long? GridSortOrderByNo { get; set; }
        public long? DynamicFormSectionGridAttributeId { get; set; }
        public bool? IsLocked { get; set; }
        public long? LockedUserId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual DynamicForm DynamicForm { get; set; }
        public virtual DynamicFormData DynamicFormDataGrid { get; set; }
        public virtual DynamicFormSectionAttribute DynamicFormSectionGridAttribute { get; set; }
        public virtual ApplicationUser LockedUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<DynamicFormApproved> DynamicFormApproved { get; set; }
        public virtual ICollection<DynamicFormDataSectionLock> DynamicFormDataSectionLock { get; set; }
        public virtual ICollection<DynamicFormDataUpload> DynamicFormDataUpload { get; set; }
        public virtual ICollection<DynamicFormItemLine> DynamicFormItemLine { get; set; }
        public virtual ICollection<DynamicFormWorkFlowForm> DynamicFormWorkFlowForm { get; set; }
        public virtual ICollection<DynamicFormWorkFlowSection> DynamicFormWorkFlowSection { get; set; }
        public virtual ICollection<DynamicFormData> InverseDynamicFormDataGrid { get; set; }
    }
}
