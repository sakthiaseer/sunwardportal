﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CommonProcessMachineGroupingMultiple
    {
        public long CommonProcessMachineGroupingId { get; set; }
        public long? MachineGroupingId { get; set; }
        public long? CommonProcessLineId { get; set; }

        public virtual CommonProcessLine CommonProcessLine { get; set; }
    }
}
