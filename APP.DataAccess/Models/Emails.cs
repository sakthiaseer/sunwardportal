﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Emails
    {
        public Emails()
        {
            EmailAttachment = new HashSet<EmailAttachment>();
        }

        public long EmailsId { get; set; }
        public string FormId { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string FromMails { get; set; }
        public string ToMails { get; set; }
        public string Ccmails { get; set; }
        public string Bccmails { get; set; }
        public Guid? SessionId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? StatusCodeId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<EmailAttachment> EmailAttachment { get; set; }
    }
}
