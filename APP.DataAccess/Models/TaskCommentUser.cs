﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TaskCommentUser
    {
        public long TaskCommentUserId { get; set; }
        public long TaskCommentId { get; set; }
        public long TaskMasterId { get; set; }
        public long UserId { get; set; }
        public bool IsRead { get; set; }
        public bool? IsPin { get; set; }
        public long? PinnedBy { get; set; }
        public bool? IsAssignedTo { get; set; }
        public DateTime? DueDate { get; set; }
        public int? StatusCodeId { get; set; }
        public bool? IsClosed { get; set; }

        public virtual ApplicationUser PinnedByNavigation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual TaskComment TaskComment { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
