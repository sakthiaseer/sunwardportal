﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProjectedDeliverySalesOrderLine
    {
        public long ProjectedDeliverySalesOrderLineId { get; set; }
        public long? ContractDistributionSalesEntryLineId { get; set; }
        public long? FrequencyId { get; set; }
        public int? PerFrequencyQty { get; set; }
        public DateTime? StartDate { get; set; }
        public bool? IsQtyReference { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? EndDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ContractDistributionSalesEntryLine ContractDistributionSalesEntryLine { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
