﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AppWikiTaskLink
    {
        public long AppWikiTaskLinkId { get; set; }
        public string TaskLink { get; set; }
        public long? ApplicationWikiId { get; set; }
        public string Subject { get; set; }

        public virtual ApplicationWiki ApplicationWiki { get; set; }
    }
}
