﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DisposalItem
    {
        public long DisposalItemId { get; set; }
        public long? InventoryTypeId { get; set; }
        public long? ItemId { get; set; }
        public string BatchNo { get; set; }
        public decimal? DisposalQty { get; set; }
        public string Remarks { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual InventoryType InventoryType { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
