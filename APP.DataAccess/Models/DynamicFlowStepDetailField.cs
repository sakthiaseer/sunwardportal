﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFlowStepDetailField
    {
        public long DynamicFlowStepDetailFieldId { get; set; }
        public long? DynamicFlowStepDetailId { get; set; }
        public string LableText { get; set; }
        public string FieldName { get; set; }
        public string FieldType { get; set; }
        public string FieldValue { get; set; }
        public bool? IsRequired { get; set; }

        public virtual DynamicFlowStepDetail DynamicFlowStepDetail { get; set; }
    }
}
