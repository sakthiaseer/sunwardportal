﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NpraformulationExcipient
    {
        public long NpraformulationExcipientId { get; set; }
        public long? NpraformulationId { get; set; }
        public long? ExcipientId { get; set; }
        public decimal? Strength { get; set; }
        public long? UnitsId { get; set; }
        public long? FunctionId { get; set; }
        public long? SourceId { get; set; }
        public long? RemarksFunctionId { get; set; }
        public string Remarks { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterDetail Excipient { get; set; }
        public virtual ApplicationMasterDetail Function { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Npraformulation Npraformulation { get; set; }
        public virtual ApplicationMasterDetail RemarksFunction { get; set; }
        public virtual ApplicationMasterDetail Source { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationMasterDetail Units { get; set; }
    }
}
