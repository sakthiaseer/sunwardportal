﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class OperationSequence
    {
        public long OperationSequenceId { get; set; }
        public long? StandardManufacturingProcessId { get; set; }
        public string ProcessType { get; set; }
        public string StartRepeatProcessNo { get; set; }
        public string EndRepeatProcessNo { get; set; }
        public string TimeGapOfNextProcess { get; set; }
        public string StartAfterProcessNo { get; set; }
        public bool? Qcapproval { get; set; }
        public string TimeGapOperand { get; set; }
        public string TimeGap { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual StandardManufacturingProcess StandardManufacturingProcess { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
