﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Notice
    {
        public Notice()
        {
            NoticeLine = new HashSet<NoticeLine>();
            NoticeUser = new HashSet<NoticeUser>();
        }

        public long NoticeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Link { get; set; }
        public bool? IsLogin { get; set; }
        public int? ModuleId { get; set; }
        public long? TypeId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster Module { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationMasterDetail Type { get; set; }
        public virtual ICollection<NoticeLine> NoticeLine { get; set; }
        public virtual ICollection<NoticeUser> NoticeUser { get; set; }
    }
}
