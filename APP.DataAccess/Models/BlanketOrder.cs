﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class BlanketOrder
    {
        public BlanketOrder()
        {
            BlanketOrderAttachment = new HashSet<BlanketOrderAttachment>();
            CustomerAcceptanceConfirmation = new HashSet<CustomerAcceptanceConfirmation>();
        }

        public long BlanketOrderId { get; set; }
        public long? CustomerId { get; set; }
        public long? ContractId { get; set; }
        public long? ProductId { get; set; }
        public int? TypeOfRequestId { get; set; }
        public decimal? QtyRequest { get; set; }
        public int? ExtensionInformationId { get; set; }
        public DateTime? FromMonth { get; set; }
        public DateTime? ToMonth { get; set; }
        public int? NoOfMonth { get; set; }
        public bool? LotInformation { get; set; }
        public string HowManyLot { get; set; }
        public long? CurrencyId { get; set; }
        public decimal? Price { get; set; }
        public decimal? PriceToQuoteForExceedQty { get; set; }
        public string SpecialNotes { get; set; }
        public string Validity { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? DateOfOrder { get; set; }
        public int? ExtensionStatusId { get; set; }
        public int? QtyExtensionStatusId { get; set; }
        public decimal? SellingPrice { get; set; }
        public string SpecialRemarks { get; set; }
        public decimal? ExceedQty { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual SalesOrderEntry Contract { get; set; }
        public virtual CompanyListing Customer { get; set; }
        public virtual CodeMaster ExtensionInformation { get; set; }
        public virtual CodeMaster ExtensionStatus { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual SocustomersItemCrossReference Product { get; set; }
        public virtual CodeMaster QtyExtensionStatus { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CodeMaster TypeOfRequest { get; set; }
        public virtual ICollection<BlanketOrderAttachment> BlanketOrderAttachment { get; set; }
        public virtual ICollection<CustomerAcceptanceConfirmation> CustomerAcceptanceConfirmation { get; set; }
    }
}
