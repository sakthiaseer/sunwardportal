﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class GenericCodeCountry
    {
        public long GenericCodeCountryId { get; set; }
        public long? CountryId { get; set; }
        public long? GenericCodeId { get; set; }
        public bool IsSellingToCountry { get; set; }

        public virtual Plant Country { get; set; }
        public virtual GenericCodes GenericCode { get; set; }
    }
}
