﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApptransferRelcassEntry
    {
        public ApptransferRelcassEntry()
        {
            ApptransferRelcassLines = new HashSet<ApptransferRelcassLines>();
        }

        public long TransferReclassId { get; set; }
        public long? TransferFromLocationId { get; set; }
        public long? TransferFromAreaId { get; set; }
        public long? TransferFromSpecificAreaId { get; set; }
        public long? TransferToLocationId { get; set; }
        public long? TransferToAreaId { get; set; }
        public long? TransferToSpecificAreaId { get; set; }
        public int? TransferTypeId { get; set; }
        public string Company { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string TransferOrderNo { get; set; }
        public long? ReceiveLocationId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Ictmaster ReceiveLocation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual Ictmaster TransferFromLocation { get; set; }
        public virtual Ictmaster TransferToLocation { get; set; }
        public virtual CodeMaster TransferType { get; set; }
        public virtual ICollection<ApptransferRelcassLines> ApptransferRelcassLines { get; set; }
    }
}
