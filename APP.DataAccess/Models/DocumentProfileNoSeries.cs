﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DocumentProfileNoSeries
    {
        public DocumentProfileNoSeries()
        {
            ApplicationForm = new HashSet<ApplicationForm>();
            ApplicationMasterDetail = new HashSet<ApplicationMasterDetail>();
            ApplicationWiki = new HashSet<ApplicationWiki>();
            BlisterMouldInformation = new HashSet<BlisterMouldInformation>();
            BompackingMaster = new HashSet<BompackingMaster>();
            ClassificationBmr = new HashSet<ClassificationBmr>();
            ClassificationProduction = new HashSet<ClassificationProduction>();
            CommonFieldsProductionMachine = new HashSet<CommonFieldsProductionMachine>();
            CommonMasterToolingClassification = new HashSet<CommonMasterToolingClassification>();
            CommonPackagingItemHeader = new HashSet<CommonPackagingItemHeader>();
            CommonPackingInformationLine = new HashSet<CommonPackingInformationLine>();
            CommonProcess = new HashSet<CommonProcess>();
            CompanyListing = new HashSet<CompanyListing>();
            DocumentNoSeries = new HashSet<DocumentNoSeries>();
            DraftApplicationWiki = new HashSet<DraftApplicationWiki>();
            DynamicForm = new HashSet<DynamicForm>();
            DynamicFormData = new HashSet<DynamicFormData>();
            FileProfileType = new HashSet<FileProfileType>();
            FpcommonField = new HashSet<FpcommonField>();
            FpdrugClassification = new HashSet<FpdrugClassification>();
            HandlingOfShipmentClassification = new HashSet<HandlingOfShipmentClassification>();
            IpirApp = new HashSet<IpirApp>();
            IssueReportIpir = new HashSet<IssueReportIpir>();
            ItemClassificationHeader = new HashSet<ItemClassificationHeader>();
            ItemClassificationMaster = new HashSet<ItemClassificationMaster>();
            ItemClassificationTransport = new HashSet<ItemClassificationTransport>();
            JobProgressTemplate = new HashSet<JobProgressTemplate>();
            JobProgressTempletateLine = new HashSet<JobProgressTempletateLine>();
            MedCatalogClassification = new HashSet<MedCatalogClassification>();
            PkginformationByPackSize = new HashSet<PkginformationByPackSize>();
            ProductActivityCase = new HashSet<ProductActivityCase>();
            ProductActivityCaseLine = new HashSet<ProductActivityCaseLine>();
            ProductionActivityClassification = new HashSet<ProductionActivityClassification>();
            ProductionActivityMasterLine = new HashSet<ProductionActivityMasterLine>();
            ProductionMethodTemplate = new HashSet<ProductionMethodTemplate>();
            ProfileAutoNumber = new HashSet<ProfileAutoNumber>();
            RequestAc = new HashSet<RequestAc>();
            SalesItemPackingInfo = new HashSet<SalesItemPackingInfo>();
            SalesOrder = new HashSet<SalesOrder>();
            SalesOrderEntry = new HashSet<SalesOrderEntry>();
            SalesOrderProfile = new HashSet<SalesOrderProfile>();
            SalesPromotion = new HashSet<SalesPromotion>();
            SampleRequestForDoctorHeader = new HashSet<SampleRequestForDoctorHeader>();
            StandardManufacturingProcess = new HashSet<StandardManufacturingProcess>();
            TempSalesPackInformationFactor = new HashSet<TempSalesPackInformationFactor>();
            TemplateTestCase = new HashSet<TemplateTestCase>();
            TemplateTestCaseLink = new HashSet<TemplateTestCaseLink>();
            WorkOrder = new HashSet<WorkOrder>();
        }

        public long ProfileId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Abbreviation { get; set; }
        public string Abbreviation1 { get; set; }
        public string Abbreviation2 { get; set; }
        public bool? AbbreviationRequired { get; set; }
        public string SpecialWording { get; set; }
        public string StartingNo { get; set; }
        public bool? StartWithYear { get; set; }
        public int? NoOfDigit { get; set; }
        public int? IncrementalNo { get; set; }
        public bool? TranslationRequired { get; set; }
        public DateTime? LastCreatedDate { get; set; }
        public string LastNoUsed { get; set; }
        public string Note { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? CompanyId { get; set; }
        public long? DeparmentId { get; set; }
        public long? GroupId { get; set; }
        public string GroupAbbreviation { get; set; }
        public long? CategoryId { get; set; }
        public string CategoryAbbreviation { get; set; }
        public bool? IsGroupAbbreviation { get; set; }
        public bool? IsCategoryAbbreviation { get; set; }
        public int? SeperatorToUse { get; set; }
        public int? LinkId { get; set; }
        public int? ProfileTypeId { get; set; }
        public string SampleDocumentNo { get; set; }
        public bool? IsEnableTask { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterDetail Category { get; set; }
        public virtual Plant Company { get; set; }
        public virtual Department Deparment { get; set; }
        public virtual ApplicationMasterDetail Group { get; set; }
        public virtual CodeMaster Link { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster ProfileType { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ApplicationForm> ApplicationForm { get; set; }
        public virtual ICollection<ApplicationMasterDetail> ApplicationMasterDetail { get; set; }
        public virtual ICollection<ApplicationWiki> ApplicationWiki { get; set; }
        public virtual ICollection<BlisterMouldInformation> BlisterMouldInformation { get; set; }
        public virtual ICollection<BompackingMaster> BompackingMaster { get; set; }
        public virtual ICollection<ClassificationBmr> ClassificationBmr { get; set; }
        public virtual ICollection<ClassificationProduction> ClassificationProduction { get; set; }
        public virtual ICollection<CommonFieldsProductionMachine> CommonFieldsProductionMachine { get; set; }
        public virtual ICollection<CommonMasterToolingClassification> CommonMasterToolingClassification { get; set; }
        public virtual ICollection<CommonPackagingItemHeader> CommonPackagingItemHeader { get; set; }
        public virtual ICollection<CommonPackingInformationLine> CommonPackingInformationLine { get; set; }
        public virtual ICollection<CommonProcess> CommonProcess { get; set; }
        public virtual ICollection<CompanyListing> CompanyListing { get; set; }
        public virtual ICollection<DocumentNoSeries> DocumentNoSeries { get; set; }
        public virtual ICollection<DraftApplicationWiki> DraftApplicationWiki { get; set; }
        public virtual ICollection<DynamicForm> DynamicForm { get; set; }
        public virtual ICollection<DynamicFormData> DynamicFormData { get; set; }
        public virtual ICollection<FileProfileType> FileProfileType { get; set; }
        public virtual ICollection<FpcommonField> FpcommonField { get; set; }
        public virtual ICollection<FpdrugClassification> FpdrugClassification { get; set; }
        public virtual ICollection<HandlingOfShipmentClassification> HandlingOfShipmentClassification { get; set; }
        public virtual ICollection<IpirApp> IpirApp { get; set; }
        public virtual ICollection<IssueReportIpir> IssueReportIpir { get; set; }
        public virtual ICollection<ItemClassificationHeader> ItemClassificationHeader { get; set; }
        public virtual ICollection<ItemClassificationMaster> ItemClassificationMaster { get; set; }
        public virtual ICollection<ItemClassificationTransport> ItemClassificationTransport { get; set; }
        public virtual ICollection<JobProgressTemplate> JobProgressTemplate { get; set; }
        public virtual ICollection<JobProgressTempletateLine> JobProgressTempletateLine { get; set; }
        public virtual ICollection<MedCatalogClassification> MedCatalogClassification { get; set; }
        public virtual ICollection<PkginformationByPackSize> PkginformationByPackSize { get; set; }
        public virtual ICollection<ProductActivityCase> ProductActivityCase { get; set; }
        public virtual ICollection<ProductActivityCaseLine> ProductActivityCaseLine { get; set; }
        public virtual ICollection<ProductionActivityClassification> ProductionActivityClassification { get; set; }
        public virtual ICollection<ProductionActivityMasterLine> ProductionActivityMasterLine { get; set; }
        public virtual ICollection<ProductionMethodTemplate> ProductionMethodTemplate { get; set; }
        public virtual ICollection<ProfileAutoNumber> ProfileAutoNumber { get; set; }
        public virtual ICollection<RequestAc> RequestAc { get; set; }
        public virtual ICollection<SalesItemPackingInfo> SalesItemPackingInfo { get; set; }
        public virtual ICollection<SalesOrder> SalesOrder { get; set; }
        public virtual ICollection<SalesOrderEntry> SalesOrderEntry { get; set; }
        public virtual ICollection<SalesOrderProfile> SalesOrderProfile { get; set; }
        public virtual ICollection<SalesPromotion> SalesPromotion { get; set; }
        public virtual ICollection<SampleRequestForDoctorHeader> SampleRequestForDoctorHeader { get; set; }
        public virtual ICollection<StandardManufacturingProcess> StandardManufacturingProcess { get; set; }
        public virtual ICollection<TempSalesPackInformationFactor> TempSalesPackInformationFactor { get; set; }
        public virtual ICollection<TemplateTestCase> TemplateTestCase { get; set; }
        public virtual ICollection<TemplateTestCaseLink> TemplateTestCaseLink { get; set; }
        public virtual ICollection<WorkOrder> WorkOrder { get; set; }
    }
}
