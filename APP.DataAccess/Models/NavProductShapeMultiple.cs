﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavProductShapeMultiple
    {
        public long NavProductShapeMultipleId { get; set; }
        public long? ProductMasterId { get; set; }
        public long? ApplicationMasterDetailId { get; set; }

        public virtual NavProductMaster ProductMaster { get; set; }
    }
}
