﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FpcommonField
    {
        public FpcommonField()
        {
            FpcommonFieldLine = new HashSet<FpcommonFieldLine>();
        }

        public long FpcommonFieldId { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public long FpnumberId { get; set; }
        public string Fpnumber { get; set; }
        public long? FinishProductId { get; set; }
        public long? DosageFormId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterDetail DosageForm { get; set; }
        public virtual ApplicationMasterDetail FinishProduct { get; set; }
        public virtual DocumentProfileNoSeries FpnumberNavigation { get; set; }
        public virtual ItemClassificationMaster ItemClassificationMaster { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<FpcommonFieldLine> FpcommonFieldLine { get; set; }
    }
}
