﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PageAttachment
    {
        public long AttachmentId { get; set; }
        public long? PageId { get; set; }
        public string DocumentType { get; set; }
        public string Size { get; set; }
        public byte[] Attachement { get; set; }

        public virtual WikiPage Page { get; set; }
    }
}
