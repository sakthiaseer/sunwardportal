﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class RegistrationFormatInformationLine
    {
        public RegistrationFormatInformationLine()
        {
            RegistrationFormatInformationLineUsers = new HashSet<RegistrationFormatInformationLineUsers>();
        }

        public long RegistrationFormatInformationLineId { get; set; }
        public long? RegistrationFormatInformationId { get; set; }
        public long? PartNameId { get; set; }
        public long? SectionNameId { get; set; }
        public string Numbering { get; set; }
        public string Description { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? FieldTypeId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster FieldType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual RegistrationFormatInformation RegistrationFormatInformation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<RegistrationFormatInformationLineUsers> RegistrationFormatInformationLineUsers { get; set; }
    }
}
