﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DistStockBalanceKiv
    {
        public long DistKivId { get; set; }
        public string ItemNo { get; set; }
        public long? ItemId { get; set; }
        public long? CompanyId { get; set; }
        public string CustomerNo { get; set; }
        public long? CustomerId { get; set; }
        public DateTime? StockBalMonth { get; set; }
        public int? StockBalWeek { get; set; }
        public decimal? Quantity { get; set; }
    }
}
