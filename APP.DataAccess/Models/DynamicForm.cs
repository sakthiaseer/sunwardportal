﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicForm
    {
        public DynamicForm()
        {
            AttributeHeader = new HashSet<AttributeHeader>();
            DynamicFormApproval = new HashSet<DynamicFormApproval>();
            DynamicFormData = new HashSet<DynamicFormData>();
            DynamicFormItemLine = new HashSet<DynamicFormItemLine>();
            DynamicFormReport = new HashSet<DynamicFormReport>();
            DynamicFormSection = new HashSet<DynamicFormSection>();
            DynamicFormSectionAttribute = new HashSet<DynamicFormSectionAttribute>();
            DynamicFormWorkFlow = new HashSet<DynamicFormWorkFlow>();
            FileProfileTypeNavigation = new HashSet<FileProfileType>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string ScreenId { get; set; }
        public Guid? SessionId { get; set; }
        public string AttributeId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsApproval { get; set; }
        public bool? IsUpload { get; set; }
        public long? FileProfileTypeId { get; set; }
        public bool? IsProfileNoGenerate { get; set; }
        public bool? IsMultipleUpload { get; set; }
        public long? CompanyId { get; set; }
        public long? ProfileId { get; set; }
        public bool? IsGridForm { get; set; }
        public bool? IsDeleted { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual FileProfileType FileProfileType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<AttributeHeader> AttributeHeader { get; set; }
        public virtual ICollection<DynamicFormApproval> DynamicFormApproval { get; set; }
        public virtual ICollection<DynamicFormData> DynamicFormData { get; set; }
        public virtual ICollection<DynamicFormItemLine> DynamicFormItemLine { get; set; }
        public virtual ICollection<DynamicFormReport> DynamicFormReport { get; set; }
        public virtual ICollection<DynamicFormSection> DynamicFormSection { get; set; }
        public virtual ICollection<DynamicFormSectionAttribute> DynamicFormSectionAttribute { get; set; }
        public virtual ICollection<DynamicFormWorkFlow> DynamicFormWorkFlow { get; set; }
        public virtual ICollection<FileProfileType> FileProfileTypeNavigation { get; set; }
    }
}
