﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TemplateTestCaseLink
    {
        public TemplateTestCaseLink()
        {
            TemplateTestCaseCheckList = new HashSet<TemplateTestCaseCheckList>();
            TemplateTestCaseLinkDoc = new HashSet<TemplateTestCaseLinkDoc>();
        }

        public long TemplateTestCaseLinkId { get; set; }
        public long? TemplateTestCaseId { get; set; }
        public string NameOfTemplate { get; set; }
        public string Subject { get; set; }
        public string Link { get; set; }
        public string LocationName { get; set; }
        public string Naming { get; set; }
        public bool? IsAutoNumbering { get; set; }
        public long? TemplateProfileId { get; set; }
        public string DocumentNo { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? LocationToSaveId { get; set; }
        public long? TemplateCaseFormId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual FileProfileType LocationToSave { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual TemplateTestCaseForm TemplateCaseForm { get; set; }
        public virtual DocumentProfileNoSeries TemplateProfile { get; set; }
        public virtual TemplateTestCase TemplateTestCase { get; set; }
        public virtual ICollection<TemplateTestCaseCheckList> TemplateTestCaseCheckList { get; set; }
        public virtual ICollection<TemplateTestCaseLinkDoc> TemplateTestCaseLinkDoc { get; set; }
    }
}
