﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FinishProductLine
    {
        public long FinishProductLineId { get; set; }
        public long? FinishProductId { get; set; }
        public long? MaterialId { get; set; }
        public decimal? DosageForm { get; set; }
        public long? Uom { get; set; }
        public string Overage { get; set; }
        public long? DosageUnits { get; set; }
        public long? OverageUnits { get; set; }
        public long? PerDosage { get; set; }

        public virtual FinishProduct FinishProduct { get; set; }
    }
}
