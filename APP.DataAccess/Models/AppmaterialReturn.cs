﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AppmaterialReturn
    {
        public long MaterialReturnId { get; set; }
        public string ProdOrderNo { get; set; }
        public int? ProdLineNo { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string DrumNo { get; set; }
        public string LotNo { get; set; }
        public string QcrefNo { get; set; }
        public string BatchNo { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string Uom { get; set; }
        public decimal? BalanceWeight { get; set; }
        public bool? PostedtoNav { get; set; }
        public string Photo { get; set; }
        public string Company { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
