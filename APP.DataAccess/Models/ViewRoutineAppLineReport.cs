﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewRoutineAppLineReport
    {
        public string Date { get; set; }
        public long? CompanyId { get; set; }
        public string DisciplineType { get; set; }
        public string Routine { get; set; }
        public string Category { get; set; }
        public string Action { get; set; }
        public string Comment { get; set; }
        public string Result { get; set; }
        public string Status { get; set; }
        public string ReadBy { get; set; }
        public string Name { get; set; }
        public string ModifyBy { get; set; }
        public long ProductionActivityRoutineAppLineId { get; set; }
        public Guid? SessionId { get; set; }
        public string TicketNo { get; set; }
        public string BatchNo { get; set; }
        public string LocationName { get; set; }
    }
}
