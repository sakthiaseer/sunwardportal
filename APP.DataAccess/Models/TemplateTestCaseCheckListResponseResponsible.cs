﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TemplateTestCaseCheckListResponseResponsible
    {
        public long WikiResponsibilityId { get; set; }
        public long? EmployeeId { get; set; }
        public long? UserId { get; set; }
        public long? UserGroupId { get; set; }
        public long? TemplateTestCaseCheckListResponseDutyId { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual TemplateTestCaseCheckListResponseDuty TemplateTestCaseCheckListResponseDuty { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual UserGroup UserGroup { get; set; }
    }
}
