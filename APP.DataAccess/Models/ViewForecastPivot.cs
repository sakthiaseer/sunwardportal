﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewForecastPivot
    {
        public long AcentryLineId { get; set; }
        public string AcfromDate { get; set; }
        public DateTime? FromDate { get; set; }
        public string ActoDate { get; set; }
        public DateTime? ToDate { get; set; }
        public long? CustomerId { get; set; }
        public long? ItemId { get; set; }
        public decimal? Acqty { get; set; }
        public string No { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public long MethodCodeId { get; set; }
        public string MethodName { get; set; }
        public string MethodDescription { get; set; }
        public string TableName { get; set; }
        public string ProdOrder { get; set; }
        public decimal? ProdQty { get; set; }
        public string DocumentNo { get; set; }
        public decimal? Soqty { get; set; }
        public decimal? Quantity { get; set; }
    }
}
