﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmployeeOtherDutyInformation
    {
        public long EmployeeOtherDutyInformationId { get; set; }
        public long? EmployeeId { get; set; }
        public long? DesignationTypeId { get; set; }
        public long? SubSectionTid { get; set; }
        public long? DesignationId { get; set; }
        public int? HeadCount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? DutyTypeId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Designation Designation { get; set; }
        public virtual ApplicationMasterDetail DutyType { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual SubSectionTwo SubSectionT { get; set; }
    }
}
