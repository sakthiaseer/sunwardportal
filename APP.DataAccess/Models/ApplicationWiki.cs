﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApplicationWiki
    {
        public ApplicationWiki()
        {
            AppWikiCategoryMultiple = new HashSet<AppWikiCategoryMultiple>();
            AppWikiLinkDraftDoc = new HashSet<AppWikiLinkDraftDoc>();
            AppWikiReleaseDoc = new HashSet<AppWikiReleaseDoc>();
            AppWikiTaskLink = new HashSet<AppWikiTaskLink>();
            AppWikiTopicMultiple = new HashSet<AppWikiTopicMultiple>();
            ApplicationWikiLine = new HashSet<ApplicationWikiLine>();
            DraftApplicationWiki = new HashSet<DraftApplicationWiki>();
            PortfolioLine = new HashSet<PortfolioLine>();
        }

        public long ApplicationWikiId { get; set; }
        public DateTime? WikiDate { get; set; }
        public long? WikiEntryById { get; set; }
        public long? WikiTypeId { get; set; }
        public long? WikiOwnerId { get; set; }
        public long? WikiCategoryId { get; set; }
        public long? WikiTopicId { get; set; }
        public string Title { get; set; }
        public int? TranslationRequiredId { get; set; }
        public string Objective { get; set; }
        public string Scope { get; set; }
        public string PreRequisition { get; set; }
        public string Content { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsMalay { get; set; }
        public bool? IsChinese { get; set; }
        public string VersionNo { get; set; }
        public long? DepartmentId { get; set; }
        public long? TaskLinkId { get; set; }
        public long? ProfileNo { get; set; }
        public Guid? SessionId { get; set; }
        public string Description { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? NewReviewDate { get; set; }
        public string Remarks { get; set; }
        public string ProfileReferenceNo { get; set; }
        public long? ProfileId { get; set; }
        public bool? IsContentEntry { get; set; }
        public string TaskLink { get; set; }
        public long? ProfilePlantId { get; set; }
        public long? ProfileDepartmentId { get; set; }
        public long? ProfileSectionId { get; set; }
        public long? ProfileSubSectionId { get; set; }
        public long? ProfileDivisionId { get; set; }
        public decimal? EstimationPreparationTimeMonth { get; set; }
        public int? WikiOwnerTypeId { get; set; }
        public long? PlantId { get; set; }
        public long? EmployeeId { get; set; }
        public string ContentInfo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Department Department { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Plant Plant { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual Department ProfileDepartment { get; set; }
        public virtual Division ProfileDivision { get; set; }
        public virtual Plant ProfilePlant { get; set; }
        public virtual Section ProfileSection { get; set; }
        public virtual SubSection ProfileSubSection { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual TaskMaster TaskLinkNavigation { get; set; }
        public virtual CodeMaster TranslationRequired { get; set; }
        public virtual ApplicationUser WikiEntryBy { get; set; }
        public virtual Department WikiOwner { get; set; }
        public virtual CodeMaster WikiOwnerType { get; set; }
        public virtual ApplicationMasterDetail WikiType { get; set; }
        public virtual ICollection<AppWikiCategoryMultiple> AppWikiCategoryMultiple { get; set; }
        public virtual ICollection<AppWikiLinkDraftDoc> AppWikiLinkDraftDoc { get; set; }
        public virtual ICollection<AppWikiReleaseDoc> AppWikiReleaseDoc { get; set; }
        public virtual ICollection<AppWikiTaskLink> AppWikiTaskLink { get; set; }
        public virtual ICollection<AppWikiTopicMultiple> AppWikiTopicMultiple { get; set; }
        public virtual ICollection<ApplicationWikiLine> ApplicationWikiLine { get; set; }
        public virtual ICollection<DraftApplicationWiki> DraftApplicationWiki { get; set; }
        public virtual ICollection<PortfolioLine> PortfolioLine { get; set; }
    }
}
