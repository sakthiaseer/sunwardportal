﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SocustomersItemCrossReference
    {
        public SocustomersItemCrossReference()
        {
            BlanketOrder = new HashSet<BlanketOrder>();
            PurchaseItemSalesEntryLine = new HashSet<PurchaseItemSalesEntryLine>();
            SalesAdhocNovate = new HashSet<SalesAdhocNovate>();
            SalesOrderEntry = new HashSet<SalesOrderEntry>();
            SalesOrderProduct = new HashSet<SalesOrderProduct>();
            SobyCustomerSunwardEquivalent = new HashSet<SobyCustomerSunwardEquivalent>();
            SobyCustomersTypeOfPermit = new HashSet<SobyCustomersTypeOfPermit>();
            SowithOutBlanketOrder = new HashSet<SowithOutBlanketOrder>();
            TenderInformation = new HashSet<TenderInformation>();
            TenderPeriodPricingLine = new HashSet<TenderPeriodPricingLine>();
            TransferBalanceQty = new HashSet<TransferBalanceQty>();
        }

        public long SocustomersItemCrossReferenceId { get; set; }
        public long? SobyCustomersId { get; set; }
        public string CustomerReferenceNo { get; set; }
        public string Description { get; set; }
        public long? CustomerPackingUomId { get; set; }
        public long? PerUomId { get; set; }
        public DateTime? ShelfLifeDate { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string MinShelfLife { get; set; }
        public long? NavItemId { get; set; }
        public string CustomerReferenceNo2 { get; set; }
        public long? PurchasePerUomId { get; set; }
        public bool? IsSameAsSunward { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterDetail CustomerPackingUom { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual GenericCodes NavItem { get; set; }
        public virtual ApplicationMasterDetail PerUom { get; set; }
        public virtual ApplicationMasterDetail PurchasePerUom { get; set; }
        public virtual SobyCustomers SobyCustomers { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<BlanketOrder> BlanketOrder { get; set; }
        public virtual ICollection<PurchaseItemSalesEntryLine> PurchaseItemSalesEntryLine { get; set; }
        public virtual ICollection<SalesAdhocNovate> SalesAdhocNovate { get; set; }
        public virtual ICollection<SalesOrderEntry> SalesOrderEntry { get; set; }
        public virtual ICollection<SalesOrderProduct> SalesOrderProduct { get; set; }
        public virtual ICollection<SobyCustomerSunwardEquivalent> SobyCustomerSunwardEquivalent { get; set; }
        public virtual ICollection<SobyCustomersTypeOfPermit> SobyCustomersTypeOfPermit { get; set; }
        public virtual ICollection<SowithOutBlanketOrder> SowithOutBlanketOrder { get; set; }
        public virtual ICollection<TenderInformation> TenderInformation { get; set; }
        public virtual ICollection<TenderPeriodPricingLine> TenderPeriodPricingLine { get; set; }
        public virtual ICollection<TransferBalanceQty> TransferBalanceQty { get; set; }
    }
}
