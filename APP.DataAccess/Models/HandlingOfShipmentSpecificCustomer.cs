﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class HandlingOfShipmentSpecificCustomer
    {
        public long HandlingOfShipmentSpecificCustomerId { get; set; }
        public long? HandlingOfShipmentId { get; set; }
        public long? SpecificCustomerId { get; set; }

        public virtual HandlingOfShipmentClassification HandlingOfShipment { get; set; }
        public virtual CompanyListing SpecificCustomer { get; set; }
    }
}
