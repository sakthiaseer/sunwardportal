﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Navinpcategory
    {
        public Navinpcategory()
        {
            NavMethodCode = new HashSet<NavMethodCode>();
        }

        public long NavinpcategoryId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public virtual ICollection<NavMethodCode> NavMethodCode { get; set; }
    }
}
