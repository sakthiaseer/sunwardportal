﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class IpirReport
    {
        public IpirReport()
        {
            IpirReportAssignment = new HashSet<IpirReportAssignment>();
            NotifyDocument = new HashSet<NotifyDocument>();
            ProductionActivityAppLineDoc = new HashSet<ProductionActivityAppLineDoc>();
            ProductionActivityNonCompliance = new HashSet<ProductionActivityNonCompliance>();
            ProductionActivityPlanningAppLineDoc = new HashSet<ProductionActivityPlanningAppLineDoc>();
            ProductionActivityRoutineAppLineDoc = new HashSet<ProductionActivityRoutineAppLineDoc>();
        }

        public long IpirReportId { get; set; }
        public string Location { get; set; }
        public long? ManufacturingProcessId { get; set; }
        public string ProdOrder { get; set; }
        public string IssueTitle { get; set; }
        public string Description { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public long? CompanyId { get; set; }
        public string ProdOrderDescription { get; set; }
        public string ProdOrderData { get; set; }
        public string TopicId { get; set; }
        public string Subject { get; set; }
        public bool? IsOthersOptions { get; set; }
        public string IpirReportNo { get; set; }
        public long? IpirissueId { get; set; }
        public string IpirissueDescription { get; set; }
        public long? LocationId { get; set; }
        public long? ProductActivityCaseId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationMasterDetail Ipirissue { get; set; }
        public virtual Ictmaster LocationNavigation { get; set; }
        public virtual ApplicationMasterChild ManufacturingProcess { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProductActivityCase ProductActivityCase { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<IpirReportAssignment> IpirReportAssignment { get; set; }
        public virtual ICollection<NotifyDocument> NotifyDocument { get; set; }
        public virtual ICollection<ProductionActivityAppLineDoc> ProductionActivityAppLineDoc { get; set; }
        public virtual ICollection<ProductionActivityNonCompliance> ProductionActivityNonCompliance { get; set; }
        public virtual ICollection<ProductionActivityPlanningAppLineDoc> ProductionActivityPlanningAppLineDoc { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLineDoc> ProductionActivityRoutineAppLineDoc { get; set; }
    }
}
