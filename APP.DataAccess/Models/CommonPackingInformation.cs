﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CommonPackingInformation
    {
        public CommonPackingInformation()
        {
            CommonPackingInfoOptional = new HashSet<CommonPackingInfoOptional>();
            CommonPackingInformationLine = new HashSet<CommonPackingInformationLine>();
        }

        public long PackingInformationId { get; set; }
        public long? SetInformationId { get; set; }
        public long? PackagingMethodId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CommonPackagingSetInfo SetInformation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<CommonPackingInfoOptional> CommonPackingInfoOptional { get; set; }
        public virtual ICollection<CommonPackingInformationLine> CommonPackingInformationLine { get; set; }
    }
}
