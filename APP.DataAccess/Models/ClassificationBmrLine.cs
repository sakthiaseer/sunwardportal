﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ClassificationBmrLine
    {
        public long ClassificationBmrLineId { get; set; }
        public long? ClassificationBmrId { get; set; }
        public string BatchNo { get; set; }
        public string SupplyTo { get; set; }
        public string ProductNo { get; set; }
        public string ProductName { get; set; }
        public string ProductUom { get; set; }
        public decimal? OutputQty { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ClassificationBmr ClassificationBmr { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
