﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SobyCustomerContactInfo
    {
        public long SobyCustomerContactInfoId { get; set; }
        public long? SobyCustomersId { get; set; }
        public long? SobyCustomersMasterAddressId { get; set; }
        public long? StationLocationId { get; set; }

        public virtual SobyCustomers SobyCustomers { get; set; }
        public virtual SobyCustomersMasterAddress SobyCustomersMasterAddress { get; set; }
        public virtual SobyCustomersMasterAddress StationLocation { get; set; }
    }
}
