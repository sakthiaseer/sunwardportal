﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TemplateTestCaseCheckListResponseDuty
    {
        public TemplateTestCaseCheckListResponseDuty()
        {
            TemplateTestCaseCheckListResponseResponsible = new HashSet<TemplateTestCaseCheckListResponseResponsible>();
        }

        public long TemplateTestCaseCheckListResponseDutyId { get; set; }
        public long? TemplateTestCaseCheckListResponseId { get; set; }
        public long? DutyNo { get; set; }
        public long? EmployeeId { get; set; }
        public long? CompanyId { get; set; }
        public long? DepartmantId { get; set; }
        public long? DesignationId { get; set; }
        public string Description { get; set; }
        public string DesignationNumber { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Type { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual Department Departmant { get; set; }
        public virtual Designation Designation { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual TemplateTestCaseCheckListResponse TemplateTestCaseCheckListResponse { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListResponseResponsible> TemplateTestCaseCheckListResponseResponsible { get; set; }
    }
}
