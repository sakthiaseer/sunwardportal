﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DistStockBalance
    {
        public long DistStockBalanceId { get; set; }
        public long? DistItemId { get; set; }
        public DateTime? StockBalMonth { get; set; }
        public int? StockBalWeek { get; set; }
        public decimal? Quantity { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public decimal? Poquantity { get; set; }
        public decimal? Avg6MonthQty { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Acitems DistItem { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
