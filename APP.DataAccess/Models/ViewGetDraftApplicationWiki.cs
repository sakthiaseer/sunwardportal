﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewGetDraftApplicationWiki
    {
        public long ApplicationWikiId { get; set; }
        public DateTime? WikiDate { get; set; }
        public long? WikiEntryById { get; set; }
        public string WikiEntryBy { get; set; }
        public long? WikiTypeId { get; set; }
        public string WikiType { get; set; }
        public long? WikiOwnerId { get; set; }
        public string WikiOwner { get; set; }
        public long? WikiCategoryId { get; set; }
        public long? WikiTopicId { get; set; }
        public string Title { get; set; }
        public int? TranslationRequiredId { get; set; }
        public string TranslationRequired { get; set; }
        public string Objective { get; set; }
        public string Scope { get; set; }
        public string PreRequisition { get; set; }
        public string Content { get; set; }
        public string VersionNo { get; set; }
        public int StatusCodeId { get; set; }
        public string StatusCode { get; set; }
        public long? AddedByUserId { get; set; }
        public string AddedByUser { get; set; }
        public long? ModifiedByUserId { get; set; }
        public string ModifiedByUser { get; set; }
        public DateTime? AddedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsMalay { get; set; }
        public bool? IsChinese { get; set; }
        public long? DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public long? ProfileNo { get; set; }
        public string Description { get; set; }
        public string Remarks { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? NewReviewDate { get; set; }
        public Guid? SessionId { get; set; }
        public string ProfileReferenceNo { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileName { get; set; }
        public string ProfileDescription { get; set; }
        public bool? IsContentEntry { get; set; }
        public string TaskLink { get; set; }
        public long? ProfilePlantId { get; set; }
        public long? ProfileDepartmentId { get; set; }
        public long? ProfileSubSectionId { get; set; }
        public long? ProfileSectionId { get; set; }
        public long? ProfileDivisionId { get; set; }
        public decimal? EstimationPreparationTimeMonth { get; set; }
        public int? WikiOwnerTypeId { get; set; }
        public long? PlantId { get; set; }
        public long? EmployeeId { get; set; }
        public long? ReleaseApplicationWikiId { get; set; }
        public string FirstName { get; set; }
        public string PlantName { get; set; }
        public string ContentInfo { get; set; }
        public string WikiCategoryIds { get; set; }
        public string WikiTopicIds { get; set; }
    }
}
