﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SalesOrder
    {
        public SalesOrder()
        {
            ReceiveEmail = new HashSet<ReceiveEmail>();
            SalesOrderProduct = new HashSet<SalesOrderProduct>();
            SowithOutBlanketOrder = new HashSet<SowithOutBlanketOrder>();
        }

        public long SalesOrderId { get; set; }
        public long? ProfileId { get; set; }
        public string DocumentNo { get; set; }
        public DateTime? DateOfOrder { get; set; }
        public string Ponumber { get; set; }
        public long? PurchaseOrderIssueId { get; set; }
        public long? CustomerId { get; set; }
        public DateTime? RequestShipmentDate { get; set; }
        public long? ShipToCodeId { get; set; }
        public string ShipingCodeType { get; set; }
        public long? SobyCustomersSalesAddressId { get; set; }
        public DateTime? VanDeliveryDate { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CompanyListing Customer { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual Plant PurchaseOrderIssue { get; set; }
        public virtual SobyCustomersAddress ShipToCode { get; set; }
        public virtual SobyCustomersSalesAddress SobyCustomersSalesAddress { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ReceiveEmail> ReceiveEmail { get; set; }
        public virtual ICollection<SalesOrderProduct> SalesOrderProduct { get; set; }
        public virtual ICollection<SowithOutBlanketOrder> SowithOutBlanketOrder { get; set; }
    }
}
