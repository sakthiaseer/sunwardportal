﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TodoNotesUsers
    {
        public long Id { get; set; }
        public long NotesHistoryId { get; set; }
        public long UserId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public string Status { get; set; }
    }
}
