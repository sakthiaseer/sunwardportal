﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class GenericCodeSupplyToMultiple
    {
        public GenericCodeSupplyToMultiple()
        {
            ProductGroupingNav = new HashSet<ProductGroupingNav>();
            SellingPriceInformation = new HashSet<SellingPriceInformation>();
            SobyCustomerSunwardEquivalent = new HashSet<SobyCustomerSunwardEquivalent>();
        }

        public long GenericCodeSupplyToMultipleId { get; set; }
        public long? GenericCodeId { get; set; }
        public long? SupplyToId { get; set; }
        public string GenericCodeSupplyDescription { get; set; }

        public virtual GenericCodes GenericCode { get; set; }
        public virtual CompanyListing SupplyTo { get; set; }
        public virtual ICollection<ProductGroupingNav> ProductGroupingNav { get; set; }
        public virtual ICollection<SellingPriceInformation> SellingPriceInformation { get; set; }
        public virtual ICollection<SobyCustomerSunwardEquivalent> SobyCustomerSunwardEquivalent { get; set; }
    }
}
