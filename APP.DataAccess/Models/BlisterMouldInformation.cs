﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class BlisterMouldInformation
    {
        public BlisterMouldInformation()
        {
            BlisterMachineCommonFieldMultiple = new HashSet<BlisterMachineCommonFieldMultiple>();
            BlisterMouldInformationDocument = new HashSet<BlisterMouldInformationDocument>();
            CommonMasterMouldChangePartsBcutter = new HashSet<CommonMasterMouldChangeParts>();
            CommonMasterMouldChangePartsBformingMould = new HashSet<CommonMasterMouldChangeParts>();
            CommonMasterMouldChangePartsBsealing = new HashSet<CommonMasterMouldChangeParts>();
            CommonMasterMouldChangePartsBtrackMould = new HashSet<CommonMasterMouldChangeParts>();
        }

        public long BlisterMouldInformationId { get; set; }
        public long? BlistedMouldPartsId { get; set; }
        public long? ProfileId { get; set; }
        public string EngraveId { get; set; }
        public string NameOfMould { get; set; }
        public long? VendorId { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public decimal? FmdiameterOfMould { get; set; }
        public decimal? FmdepthOfMould { get; set; }
        public decimal? SmdiameterOfMould { get; set; }
        public decimal? SmdepthOfMould { get; set; }
        public int? NoOfTrack { get; set; }
        public decimal? CblisterLength { get; set; }
        public decimal? CblisterWidth { get; set; }
        public int? CcuNo { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public decimal? FmUpperCupDiameter { get; set; }
        public decimal? FmUpperCupDepth { get; set; }
        public decimal? FmLowerCupShape { get; set; }
        public long? FeederId { get; set; }
        public long? BlisterKnurlingPatternId { get; set; }
        public int? UpperKnurlingCoverageId { get; set; }
        public decimal? LowerCupDiameter { get; set; }
        public decimal? LowerCupDepth { get; set; }
        public decimal? Widthtrack { get; set; }
        public long? FormatId { get; set; }
        public long? CuNoFormatId { get; set; }
        public bool? Perforation { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterDetail BlistedMouldParts { get; set; }
        public virtual ApplicationMasterDetail BlisterKnurlingPattern { get; set; }
        public virtual ApplicationMasterDetail CuNoFormat { get; set; }
        public virtual ApplicationMasterDetail Feeder { get; set; }
        public virtual ApplicationMasterDetail Format { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CodeMaster UpperKnurlingCoverage { get; set; }
        public virtual CompanyListing Vendor { get; set; }
        public virtual ICollection<BlisterMachineCommonFieldMultiple> BlisterMachineCommonFieldMultiple { get; set; }
        public virtual ICollection<BlisterMouldInformationDocument> BlisterMouldInformationDocument { get; set; }
        public virtual ICollection<CommonMasterMouldChangeParts> CommonMasterMouldChangePartsBcutter { get; set; }
        public virtual ICollection<CommonMasterMouldChangeParts> CommonMasterMouldChangePartsBformingMould { get; set; }
        public virtual ICollection<CommonMasterMouldChangeParts> CommonMasterMouldChangePartsBsealing { get; set; }
        public virtual ICollection<CommonMasterMouldChangeParts> CommonMasterMouldChangePartsBtrackMould { get; set; }
    }
}
