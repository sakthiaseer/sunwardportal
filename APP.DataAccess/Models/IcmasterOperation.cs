﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class IcmasterOperation
    {
        public IcmasterOperation()
        {
            ProductionMethodTemplate = new HashSet<ProductionMethodTemplate>();
            StandardProcedure = new HashSet<StandardProcedure>();
        }

        public long IcmasterOperationId { get; set; }
        public long? ManufacturingStepsId { get; set; }
        public string MasterOperation { get; set; }
        public string WiLink { get; set; }
        public bool? Training { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CommonProcessLine ManufacturingSteps { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ProductionMethodTemplate> ProductionMethodTemplate { get; set; }
        public virtual ICollection<StandardProcedure> StandardProcedure { get; set; }
    }
}
