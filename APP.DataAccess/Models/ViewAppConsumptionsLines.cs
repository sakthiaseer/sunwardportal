﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewAppConsumptionsLines
    {
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public int? ProdLineNo { get; set; }
        public string LotNo { get; set; }
        public string SubLotNo { get; set; }
        public int? DrumNo { get; set; }
        public string QcrefNo { get; set; }
        public string BatchNo { get; set; }
        public decimal? Quantity { get; set; }
        public string Uom { get; set; }
        public bool? PostedtoNav { get; set; }
        public string AddedBy { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ConsumptionEntryId { get; set; }
    }
}
