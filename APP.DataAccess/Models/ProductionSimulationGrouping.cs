﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionSimulationGrouping
    {
        public ProductionSimulationGrouping()
        {
            OrderRequirementGroupingLine = new HashSet<OrderRequirementGroupingLine>();
        }

        public long ProductionSimulationGroupingId { get; set; }
        public long? ProductionSimulationId { get; set; }
        public string ProdOrderNo { get; set; }
        public long? ItemId { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string PackSize { get; set; }
        public decimal? Quantity { get; set; }
        public string Uom { get; set; }
        public decimal? PerQuantity { get; set; }
        public string PerQtyUom { get; set; }
        public string BatchNo { get; set; }
        public DateTime? StartingDate { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? CompanyId { get; set; }
        public decimal? PlannedQty { get; set; }
        public decimal? OutputQty { get; set; }
        public bool? IsOutput { get; set; }
        public DateTime? ProcessDate { get; set; }
        public string RePlanRefNo { get; set; }
        public string BatchSize { get; set; }
        public bool? IsBmrticket { get; set; }
        public string Description2 { get; set; }
        public long? TypeOfProdOrderId { get; set; }
        public long? SellingStatusId { get; set; }
        public string PortalStatusNo { get; set; }
        public long? MethodCodeId { get; set; }
        public string Dispense { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual NavMethodCode MethodCode { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProductionSimulation ProductionSimulation { get; set; }
        public virtual ApplicationMasterDetail SellingStatus { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationMasterDetail TypeOfProdOrder { get; set; }
        public virtual ICollection<OrderRequirementGroupingLine> OrderRequirementGroupingLine { get; set; }
    }
}
