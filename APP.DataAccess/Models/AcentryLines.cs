﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AcentryLines
    {
        public long AcentryLineId { get; set; }
        public long? AcentryId { get; set; }
        public long? ItemId { get; set; }
        public decimal? Quantity { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual Acentry Acentry { get; set; }
        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
