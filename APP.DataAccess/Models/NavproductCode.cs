﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavproductCode
    {
        public NavproductCode()
        {
            ProcessMachineTime = new HashSet<ProcessMachineTime>();
            ProductionBatchInformationLine = new HashSet<ProductionBatchInformationLine>();
        }

        public long NavproductCodeId { get; set; }
        public string ProductCode { get; set; }
        public string ProductCodeName { get; set; }
        public long? CompanyId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ProcessMachineTime> ProcessMachineTime { get; set; }
        public virtual ICollection<ProductionBatchInformationLine> ProductionBatchInformationLine { get; set; }
    }
}
