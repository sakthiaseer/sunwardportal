﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AppWikiDraftDoc
    {
        public long AppWikiDraftDocId { get; set; }
        public long? DocumentId { get; set; }
        public long? ApplicationWikiId { get; set; }

        public virtual DraftApplicationWiki ApplicationWiki { get; set; }
        public virtual Documents Document { get; set; }
    }
}
