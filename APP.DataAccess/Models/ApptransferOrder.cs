﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApptransferOrder
    {
        public ApptransferOrder()
        {
            ApptransferOrderLines = new HashSet<ApptransferOrderLines>();
        }

        public long TransferId { get; set; }
        public string TransferFrom { get; set; }
        public long? TransferFromId { get; set; }
        public string TransferTo { get; set; }
        public long? TransferToId { get; set; }
        public string ProdOrderNo { get; set; }
        public string ReplanRefNo { get; set; }
        public string SubLotNo { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ApptransferOrderLines> ApptransferOrderLines { get; set; }
    }
}
