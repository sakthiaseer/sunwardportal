﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Memo
    {
        public Memo()
        {
            MemoUser = new HashSet<MemoUser>();
        }

        public long MemoId { get; set; }
        public string Subject { get; set; }
        public string MemoContent { get; set; }
        public bool? IsAttachment { get; set; }
        public Guid? SessionId { get; set; }
        public DateTime? StartDate { get; set; }
        public int? StatusCodeId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedByUserId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<MemoUser> MemoUser { get; set; }
    }
}
