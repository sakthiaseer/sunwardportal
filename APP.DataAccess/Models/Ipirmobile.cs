﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Ipirmobile
    {
        public Ipirmobile()
        {
            IpirmobileAction = new HashSet<IpirmobileAction>();
        }

        public long IpirmobileId { get; set; }
        public long? CompanyId { get; set; }
        public int? IntendedActionId { get; set; }
        public string IpirreportNo { get; set; }
        public string Location { get; set; }
        public string ProductionOrderNo { get; set; }
        public string ProductName { get; set; }
        public string BatchNo { get; set; }
        public string IssueDescription { get; set; }
        public string Photo { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual CodeMaster IntendedAction { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<IpirmobileAction> IpirmobileAction { get; set; }
    }
}
