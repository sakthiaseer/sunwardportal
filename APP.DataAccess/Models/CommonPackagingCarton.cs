﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CommonPackagingCarton
    {
        public CommonPackagingCarton()
        {
            BottleCapUseForItems = new HashSet<BottleCapUseForItems>();
        }

        public long CartonSpecificationId { get; set; }
        public long? PackagingItemSetInfoId { get; set; }
        public bool? Set { get; set; }
        public bool? VersionControl { get; set; }
        public decimal? OuterMeasurementLength { get; set; }
        public decimal? OuterMeasurementWidth { get; set; }
        public decimal? OuterMeasurementHeight { get; set; }
        public decimal? InnerMeasurementLength { get; set; }
        public decimal? InnerMeasurementWidth { get; set; }
        public decimal? InnerMeasurementHeight { get; set; }
        public long? TypeWallId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<BottleCapUseForItems> BottleCapUseForItems { get; set; }
    }
}
