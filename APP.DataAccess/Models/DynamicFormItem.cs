﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormItem
    {
        public DynamicFormItem()
        {
            DynamicFormItemLine = new HashSet<DynamicFormItemLine>();
        }

        public long DynamicFormItemId { get; set; }
        public string AutoNumber { get; set; }
        public long? CompanyId { get; set; }
        public int? TransactionTypeId { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string Description { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Reason { get; set; }
        public long? StockGroupAppParentId { get; set; }
        public long? DepartmentAppParentId { get; set; }
        public long? ItemGroupAppParentId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationMasterChild DepartmentAppParent { get; set; }
        public virtual ApplicationMasterChild ItemGroupAppParent { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationMasterChild StockGroupAppParent { get; set; }
        public virtual CodeMaster TransactionType { get; set; }
        public virtual ICollection<DynamicFormItemLine> DynamicFormItemLine { get; set; }
    }
}
