﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ShiftMaster
    {
        public ShiftMaster()
        {
            WorkDayShift = new HashSet<WorkDayShift>();
        }

        public long ShiftId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<WorkDayShift> WorkDayShift { get; set; }
    }
}
