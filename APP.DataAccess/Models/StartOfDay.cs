﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class StartOfDay
    {
        public StartOfDay()
        {
            StartOfDayManPowerMultiple = new HashSet<StartOfDayManPowerMultiple>();
            StartOfDayProdTaskMultiple = new HashSet<StartOfDayProdTaskMultiple>();
        }

        public long StartOfDayId { get; set; }
        public long? CompanyDbid { get; set; }
        public long? WorkingBlockId { get; set; }
        public int? StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<StartOfDayManPowerMultiple> StartOfDayManPowerMultiple { get; set; }
        public virtual ICollection<StartOfDayProdTaskMultiple> StartOfDayProdTaskMultiple { get; set; }
    }
}
