﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionActivityCheckedDetails
    {
        public long ProductionActivityCheckedDetailsId { get; set; }
        public long? ProductionActivityAppLineId { get; set; }
        public long? ProductionActivityAppId { get; set; }
        public int? ActivityInfoId { get; set; }
        public bool? IsCheckNoIssue { get; set; }
        public long? CheckedById { get; set; }
        public DateTime? CheckedDate { get; set; }
        public string CheckedComment { get; set; }
        public bool? IsCheckReferSupportDocument { get; set; }
        public byte[] CommentImage { get; set; }
        public string CommentImageType { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public long? ActivityStatusId { get; set; }
        public long? ActivityResultId { get; set; }

        public virtual CodeMaster ActivityInfo { get; set; }
        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser CheckedBy { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProductionActivityApp ProductionActivityApp { get; set; }
        public virtual ProductionActivityAppLine ProductionActivityAppLine { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
