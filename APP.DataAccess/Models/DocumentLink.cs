﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DocumentLink
    {
        public DocumentLink()
        {
            AppWikiLinkDraftDoc = new HashSet<AppWikiLinkDraftDoc>();
        }

        public long DocumentLinkId { get; set; }
        public long? DocumentId { get; set; }
        public long? LinkDocumentId { get; set; }
        public string DocumentPath { get; set; }
        public long? FileProfieTypeId { get; set; }
        public long? FolderId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Documents Document { get; set; }
        public virtual Documents LinkDocument { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<AppWikiLinkDraftDoc> AppWikiLinkDraftDoc { get; set; }
    }
}
