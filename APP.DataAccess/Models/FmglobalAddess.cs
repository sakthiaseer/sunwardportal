﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FmglobalAddess
    {
        public long FmglobalAddessId { get; set; }
        public long FmglobalId { get; set; }
        public long AddressId { get; set; }
        public string AddressType { get; set; }
        public bool? IsBilling { get; set; }
        public bool? IsShipping { get; set; }

        public virtual Address Address { get; set; }
        public virtual Fmglobal Fmglobal { get; set; }
    }
}
