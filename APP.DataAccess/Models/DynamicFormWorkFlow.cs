﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormWorkFlow
    {
        public DynamicFormWorkFlow()
        {
            DynamicFormWorkFlowApproval = new HashSet<DynamicFormWorkFlowApproval>();
            DynamicFormWorkFlowForm = new HashSet<DynamicFormWorkFlowForm>();
            DynamicFormWorkFlowSection = new HashSet<DynamicFormWorkFlowSection>();
        }

        public long DynamicFormWorkFlowId { get; set; }
        public long? DynamicFormId { get; set; }
        public long? UserId { get; set; }
        public long? UserGroupId { get; set; }
        public long? LevelId { get; set; }
        public string Type { get; set; }
        public int? SequenceNo { get; set; }
        public bool? IsAllowDelegateUser { get; set; }

        public virtual DynamicForm DynamicForm { get; set; }
        public virtual LevelMaster Level { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual UserGroup UserGroup { get; set; }
        public virtual ICollection<DynamicFormWorkFlowApproval> DynamicFormWorkFlowApproval { get; set; }
        public virtual ICollection<DynamicFormWorkFlowForm> DynamicFormWorkFlowForm { get; set; }
        public virtual ICollection<DynamicFormWorkFlowSection> DynamicFormWorkFlowSection { get; set; }
    }
}
