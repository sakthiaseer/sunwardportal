﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmployeeEmailInfoAuthority
    {
        public long EmployeeEmailInfoAuthorityId { get; set; }
        public long? EmployeeEmailInfoId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Purpose { get; set; }

        public virtual EmployeeEmailInfo EmployeeEmailInfo { get; set; }
    }
}
