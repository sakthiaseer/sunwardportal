﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ItemSalesPrice
    {
        public long ItemSalesPriceId { get; set; }
        public long? CustomerId { get; set; }
        public long? ItemId { get; set; }
        public long? CompanyId { get; set; }
        public decimal? SellingPrice { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual Navcustomer Customer { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
