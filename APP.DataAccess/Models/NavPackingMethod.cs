﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavPackingMethod
    {
        public long PackingMethodId { get; set; }
        public long? ItemId { get; set; }
        public long? NoOfUnitsPerShipperCarton { get; set; }
        public long? PalletSize { get; set; }
        public long? NoOfShipperCartorPerPallet { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
