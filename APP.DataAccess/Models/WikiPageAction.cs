﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WikiPageAction
    {
        public WikiPageAction()
        {
            WikiPageActionReply = new HashSet<WikiPageActionReply>();
        }

        public long PageActionId { get; set; }
        public long? PageId { get; set; }
        public string Notes { get; set; }
        public string FileName { get; set; }
        public string SessionId { get; set; }
        public long? AddedUserId { get; set; }
        public DateTime? AddedDate { get; set; }

        public virtual ApplicationUser AddedUser { get; set; }
        public virtual WikiPage Page { get; set; }
        public virtual ICollection<WikiPageActionReply> WikiPageActionReply { get; set; }
    }
}
