﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApplicationFormSub
    {
        public long ApplicationFormSubId { get; set; }
        public long? FormId { get; set; }
        public long? ApplicationFormId { get; set; }

        public virtual ApplicationForm ApplicationForm { get; set; }
        public virtual ApplicationForm Form { get; set; }
    }
}
