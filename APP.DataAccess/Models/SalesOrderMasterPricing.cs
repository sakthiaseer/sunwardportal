﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SalesOrderMasterPricing
    {
        public SalesOrderMasterPricing()
        {
            SalesOrderMasterPricingLine = new HashSet<SalesOrderMasterPricingLine>();
        }

        public long SalesOrderMasterPricingId { get; set; }
        public long? CompanyId { get; set; }
        public long? SalesPricingForId { get; set; }
        public DateTime? PriceValidaityFrom { get; set; }
        public DateTime? PriceValidaityTo { get; set; }
        public long? ReasonForChangeId { get; set; }
        public string MasterType { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationMasterDetail ReasonForChange { get; set; }
        public virtual ApplicationMasterDetail SalesPricingFor { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<SalesOrderMasterPricingLine> SalesOrderMasterPricingLine { get; set; }
    }
}
