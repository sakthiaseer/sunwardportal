﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Ictcertificate
    {
        public long IctcertificateId { get; set; }
        public int? CertificateType { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public bool? IsExpired { get; set; }
        public DateTime? IssueDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public long? VendorsListId { get; set; }
        public long? SourceListId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster CertificateTypeNavigation { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual SourceList SourceList { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual VendorsList VendorsList { get; set; }
    }
}
