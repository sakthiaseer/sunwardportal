﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class MedCatalogClassification
    {
        public MedCatalogClassification()
        {
            MedMaster = new HashSet<MedMaster>();
        }

        public long MedCatalogId { get; set; }
        public long? ItemClassificationMasterId { get; set; }
        public long? GroupId { get; set; }
        public long? CategoryId { get; set; }
        public string CategoryNo { get; set; }
        public long? ProfileId { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string LastNo { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterDetail Category { get; set; }
        public virtual ApplicationMasterDetail Group { get; set; }
        public virtual ItemClassificationMaster ItemClassificationMaster { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<MedMaster> MedMaster { get; set; }
    }
}
