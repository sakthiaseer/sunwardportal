﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class IctlayoutPlanTypes
    {
        public long IctlayoutTypeId { get; set; }
        public long? IctmasterId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long? DocumentId { get; set; }
        public long? LayoutTypeId { get; set; }
        public string VersionNo { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Documents Document { get; set; }
        public virtual Ictmaster Ictmaster { get; set; }
        public virtual LayoutPlanType LayoutType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
