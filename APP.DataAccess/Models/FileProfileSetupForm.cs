﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FileProfileSetupForm
    {
        public long FileProfileSetupFormId { get; set; }
        public long? FileProfileTypeId { get; set; }
        public int? ControlTypeId { get; set; }
        public string ControlTypeValue { get; set; }
        public string DefaultValue { get; set; }
        public DateTime? Date { get; set; }
        public bool? IsDefault { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsMultiple { get; set; }
        public bool? IsRequired { get; set; }
        public string Placeholder { get; set; }
        public string RequiredMessage { get; set; }
        public string PropertyName { get; set; }
        public string DropDownTypeId { get; set; }
        public string DataSourceId { get; set; }
        public long? ProductActivityCaseId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster ControlType { get; set; }
        public virtual FileProfileType FileProfileType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProductActivityCase ProductActivityCase { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
