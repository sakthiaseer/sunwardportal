﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class MachineTimeManHourInfo
    {
        public long MachineTimeManHourInfoId { get; set; }
        public long? ProcessMachineTimeLineId { get; set; }
        public decimal? Manpower { get; set; }
        public int? PreparationTimeId { get; set; }
        public string PreparationTime { get; set; }
        public int? ProductionTimeId { get; set; }
        public string ProductionTime { get; set; }
        public int? Level1CleaningId { get; set; }
        public string Level1Cleaning { get; set; }
        public int? Level2CleaningId { get; set; }
        public string Level2Cleaning { get; set; }
        public int? IntermittentCleaningAtId { get; set; }
        public string IntermittentCleaningAt { get; set; }
        public int? NextStepMinTimeGapId { get; set; }
        public string NextStepMinTimeGap { get; set; }
        public int? NextStepMaxTimeGapId { get; set; }
        public string NextStepMaxTimeGap { get; set; }
        public bool? NextStepManpowerOverlapping { get; set; }
        public decimal? OverlapNumber { get; set; }
        public int? NextProdItemMinTimeId { get; set; }
        public string NextProdItemMinTime { get; set; }
        public int? NextProdItemMaxTimeId { get; set; }
        public string NextProdItemMaxTime { get; set; }
        public string QctestingTime { get; set; }
        public string QcmaxCollection { get; set; }
        public string MaxCampaign { get; set; }
        public string RecommendedCycle { get; set; }
        public string NoOfCutUse { get; set; }
        public int? TypeOfFeedId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster IntermittentCleaningAtNavigation { get; set; }
        public virtual CodeMaster Level1CleaningNavigation { get; set; }
        public virtual CodeMaster Level2CleaningNavigation { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster NextProdItemMaxTimeNavigation { get; set; }
        public virtual CodeMaster NextProdItemMinTimeNavigation { get; set; }
        public virtual CodeMaster NextStepMaxTimeGapNavigation { get; set; }
        public virtual CodeMaster NextStepMinTimeGapNavigation { get; set; }
        public virtual CodeMaster PreparationTimeNavigation { get; set; }
        public virtual ProcessMachineTimeProductionLine ProcessMachineTimeLine { get; set; }
        public virtual CodeMaster ProductionTimeNavigation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual CodeMaster TypeOfFeed { get; set; }
    }
}
