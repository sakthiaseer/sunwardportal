﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WorkFlowInteraction
    {
        public WorkFlowInteraction()
        {
            WorkFlowInteractionRecipient = new HashSet<WorkFlowInteractionRecipient>();
        }

        public long InteractionId { get; set; }
        public long? FlowDistributionId { get; set; }
        public long? ParentInteractionId { get; set; }
        public string Title { get; set; }
        public DateTime? ReminderDate { get; set; }
        public string MessageBody { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual FlowDistribution FlowDistribution { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<WorkFlowInteractionRecipient> WorkFlowInteractionRecipient { get; set; }
    }
}
