﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class VendorsList
    {
        public VendorsList()
        {
            Ictcertificate = new HashSet<Ictcertificate>();
            IctcontactDetails = new HashSet<IctcontactDetails>();
            VendorInvoice = new HashSet<VendorInvoice>();
            VendorPayment = new HashSet<VendorPayment>();
            VendorSourceList = new HashSet<VendorSourceList>();
        }

        public long VendorsListId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string VendorNo { get; set; }
        public long? OfficeAddressId { get; set; }
        public long? SiteAddressId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Address OfficeAddress { get; set; }
        public virtual Address SiteAddress { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<Ictcertificate> Ictcertificate { get; set; }
        public virtual ICollection<IctcontactDetails> IctcontactDetails { get; set; }
        public virtual ICollection<VendorInvoice> VendorInvoice { get; set; }
        public virtual ICollection<VendorPayment> VendorPayment { get; set; }
        public virtual ICollection<VendorSourceList> VendorSourceList { get; set; }
    }
}
