﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class LocationGroup
    {
        public LocationGroup()
        {
            GroupLocationLink = new HashSet<GroupLocationLink>();
            LocationGroupWiki = new HashSet<LocationGroupWiki>();
        }

        public long LocationGroupId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<GroupLocationLink> GroupLocationLink { get; set; }
        public virtual ICollection<LocationGroupWiki> LocationGroupWiki { get; set; }
    }
}
