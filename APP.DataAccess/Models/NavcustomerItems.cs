﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavcustomerItems
    {
        public long CustomerItemId { get; set; }
        public long? ItemId { get; set; }
        public long? CustomerId { get; set; }

        public virtual Navcustomer Customer { get; set; }
        public virtual Navitems Item { get; set; }
    }
}
