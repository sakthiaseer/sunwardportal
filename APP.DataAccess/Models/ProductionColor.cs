﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionColor
    {
        public ProductionColor()
        {
            InverseProcessUseSpec = new HashSet<ProductionColor>();
            ProductionColorLine = new HashSet<ProductionColorLine>();
        }

        public long ProductionColorId { get; set; }
        public long? ItemNo { get; set; }
        public long? ColorId { get; set; }
        public long? GenericName { get; set; }
        public long? ColorIndexNumberId { get; set; }
        public decimal? DyeContent { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }
        public bool? IsSameAsMaster { get; set; }
        public string MaterialName { get; set; }
        public int? SpecNo { get; set; }
        public long? PurposeId { get; set; }
        public long? ProcessUseSpecId { get; set; }
        public long? RndpurposeId { get; set; }
        public string StudyLink { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterDetail Color { get; set; }
        public virtual ApplicationMasterDetail ColorIndexNumber { get; set; }
        public virtual ApplicationMasterDetail GenericNameNavigation { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProductionColor ProcessUseSpec { get; set; }
        public virtual ApplicationMasterDetail Purpose { get; set; }
        public virtual ApplicationMasterDetail Rndpurpose { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ProductionColor> InverseProcessUseSpec { get; set; }
        public virtual ICollection<ProductionColorLine> ProductionColorLine { get; set; }
    }
}
