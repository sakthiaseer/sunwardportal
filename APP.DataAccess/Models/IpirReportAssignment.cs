﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class IpirReportAssignment
    {
        public IpirReportAssignment()
        {
            IpirReportAssignmentUser = new HashSet<IpirReportAssignmentUser>();
            NotifyDocument = new HashSet<NotifyDocument>();
        }

        public long IpirReportAssignmentId { get; set; }
        public long? IpirReportId { get; set; }
        public string Description { get; set; }
        public bool? Urgent { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public int? IntendActionId { get; set; }
        public string OthersDescription { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public string TopicId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster IntendAction { get; set; }
        public virtual IpirReport IpirReport { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<IpirReportAssignmentUser> IpirReportAssignmentUser { get; set; }
        public virtual ICollection<NotifyDocument> NotifyDocument { get; set; }
    }
}
