﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavMethodCodeLines
    {
        public long MethodCodeLineId { get; set; }
        public long? MethodCodeId { get; set; }
        public long? ItemId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual NavMethodCode MethodCode { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
