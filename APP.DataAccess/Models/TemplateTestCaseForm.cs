﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TemplateTestCaseForm
    {
        public TemplateTestCaseForm()
        {
            TemplateCaseFormNotes = new HashSet<TemplateCaseFormNotes>();
            TemplateTestCaseCheckList = new HashSet<TemplateTestCaseCheckList>();
            TemplateTestCaseCheckListForm = new HashSet<TemplateTestCaseCheckListForm>();
            TemplateTestCaseLink = new HashSet<TemplateTestCaseLink>();
            TemplateTestCaseProposal = new HashSet<TemplateTestCaseProposal>();
        }

        public long TemplateTestCaseFormId { get; set; }
        public long? TemplateTestCaseId { get; set; }
        public long? CompanyId { get; set; }
        public string CaseNo { get; set; }
        public string SubjectName { get; set; }
        public string VersionNo { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public string Instruction { get; set; }
        public long? FileProfileTypeId { get; set; }
        public string TopicId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual FileProfileType FileProfileType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual TemplateTestCase TemplateTestCase { get; set; }
        public virtual ICollection<TemplateCaseFormNotes> TemplateCaseFormNotes { get; set; }
        public virtual ICollection<TemplateTestCaseCheckList> TemplateTestCaseCheckList { get; set; }
        public virtual ICollection<TemplateTestCaseCheckListForm> TemplateTestCaseCheckListForm { get; set; }
        public virtual ICollection<TemplateTestCaseLink> TemplateTestCaseLink { get; set; }
        public virtual ICollection<TemplateTestCaseProposal> TemplateTestCaseProposal { get; set; }
    }
}
