﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class LevelMaster
    {
        public LevelMaster()
        {
            ApplicationMasterAccess = new HashSet<ApplicationMasterAccess>();
            Designation = new HashSet<Designation>();
            DocumentUserRole = new HashSet<DocumentUserRole>();
            DynamicFormSectionAttributeSecurity = new HashSet<DynamicFormSectionAttributeSecurity>();
            DynamicFormSectionSecurity = new HashSet<DynamicFormSectionSecurity>();
            DynamicFormWorkFlow = new HashSet<DynamicFormWorkFlow>();
            Employee = new HashSet<Employee>();
            EmployeeReportPerson = new HashSet<EmployeeReportPerson>();
            MemoUser = new HashSet<MemoUser>();
            OpenAccessUserLink = new HashSet<OpenAccessUserLink>();
        }

        public long LevelId { get; set; }
        public long? CompanyId { get; set; }
        public string Name { get; set; }
        public int? Priority { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? DivisionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual Division Division { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ApplicationMasterAccess> ApplicationMasterAccess { get; set; }
        public virtual ICollection<Designation> Designation { get; set; }
        public virtual ICollection<DocumentUserRole> DocumentUserRole { get; set; }
        public virtual ICollection<DynamicFormSectionAttributeSecurity> DynamicFormSectionAttributeSecurity { get; set; }
        public virtual ICollection<DynamicFormSectionSecurity> DynamicFormSectionSecurity { get; set; }
        public virtual ICollection<DynamicFormWorkFlow> DynamicFormWorkFlow { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
        public virtual ICollection<EmployeeReportPerson> EmployeeReportPerson { get; set; }
        public virtual ICollection<MemoUser> MemoUser { get; set; }
        public virtual ICollection<OpenAccessUserLink> OpenAccessUserLink { get; set; }
    }
}
