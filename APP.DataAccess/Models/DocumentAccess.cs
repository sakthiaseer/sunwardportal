﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DocumentAccess
    {
        public long DocumentAccessId { get; set; }
        public long? UserId { get; set; }
        public long? DocumentId { get; set; }

        public Documents Document { get; set; }
        public ApplicationUser User { get; set; }
    }
}
