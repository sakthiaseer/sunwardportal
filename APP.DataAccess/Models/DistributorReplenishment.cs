﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DistributorReplenishment
    {
        public DistributorReplenishment()
        {
            DistributorReplenishmentLine = new HashSet<DistributorReplenishmentLine>();
        }

        public long DistributorReplenishmentId { get; set; }
        public long? CompanyId { get; set; }
        public DateTime? FromPeriod { get; set; }
        public DateTime? ToPeriod { get; set; }
        public decimal? ReplenishmentTiming { get; set; }
        public decimal? NoOfMonthToReplenish { get; set; }
        public bool? NeedVersionChecking { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public long? CustomerId { get; set; }
        public long? DosageFormId { get; set; }
        public long? DrugClassificationId { get; set; }
        public decimal? MaxHoldingStock { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual Navcustomer Customer { get; set; }
        public virtual ApplicationMasterDetail DosageForm { get; set; }
        public virtual ApplicationMasterDetail DrugClassification { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<DistributorReplenishmentLine> DistributorReplenishmentLine { get; set; }
    }
}
