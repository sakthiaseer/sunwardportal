﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Project
    {
        public Project()
        {
            TaskProject = new HashSet<TaskProject>();
        }

        public long ProjectId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid? SessionId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? TeamId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual TeamMaster Team { get; set; }
        public virtual ICollection<TaskProject> TaskProject { get; set; }
    }
}
