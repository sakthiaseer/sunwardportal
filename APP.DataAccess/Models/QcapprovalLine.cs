﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class QcapprovalLine
    {
        public long QcapprovalLineId { get; set; }
        public long? QcapprovalId { get; set; }
        public string ProductionOrderNo { get; set; }
        public string SubLotNo { get; set; }
        public string DrumNo { get; set; }
        public int? MoisturePercent { get; set; }
        public int? StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string MoistureStatus { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Qcapproval Qcapproval { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
