﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DocumentShare
    {
        public DocumentShare()
        {
            DocumentShareDocuments = new HashSet<DocumentShareDocuments>();
            DocumentShareUser = new HashSet<DocumentShareUser>();
        }

        public long DocumentShareId { get; set; }
        public bool? IsShareAnyOne { get; set; }
        public long? DocumentId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Documents Document { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<DocumentShareDocuments> DocumentShareDocuments { get; set; }
        public virtual ICollection<DocumentShareUser> DocumentShareUser { get; set; }
    }
}
