﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SampleRequestForDoctor
    {
        public long SampleRequestForDoctorId { get; set; }
        public long? SampleRequestForDoctorHeaderId { get; set; }
        public long? CompanyId { get; set; }
        public long? ItemId { get; set; }
        public string BatchNo { get; set; }
        public DateTime? MfgDate { get; set; }
        public DateTime? ExpDate { get; set; }
        public decimal? InStockQty { get; set; }
        public decimal? OutStockQty { get; set; }
        public decimal? BalanceStockQty { get; set; }
        public decimal? TotalQuantity { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsOutOfStock { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual Navitems Item { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual SampleRequestForDoctorHeader SampleRequestForDoctorHeader { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
