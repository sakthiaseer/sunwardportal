﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SalesTargetByStaff
    {
        public SalesTargetByStaff()
        {
            SalesTargetByStaffLine = new HashSet<SalesTargetByStaffLine>();
        }

        public long SalesTargetByStaffId { get; set; }
        public long? CompanyId { get; set; }
        public long? SalesGroupId { get; set; }
        public long? LeadById { get; set; }
        public int StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMasterDetail Company { get; set; }
        public virtual Employee LeadBy { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationMasterDetail SalesGroup { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<SalesTargetByStaffLine> SalesTargetByStaffLine { get; set; }
    }
}
