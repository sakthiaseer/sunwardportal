﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AppdispenserDispensingDrumDetails
    {
        public long DispenserDispensingDrumDetailsId { get; set; }
        public long DispenserDispensingLineId { get; set; }
        public string MaterialNo { get; set; }
        public string LotNo { get; set; }
        public string QcrefNo { get; set; }
        public decimal? Weight { get; set; }
        public string WeighingPhoto { get; set; }

        public virtual AppdispenserDispensingLine DispenserDispensingLine { get; set; }
    }
}
