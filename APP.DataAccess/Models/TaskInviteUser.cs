﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TaskInviteUser
    {
        public long TaskInviteUserId { get; set; }
        public long? TaskId { get; set; }
        public long? InviteUserId { get; set; }
        public DateTime? InvitedDate { get; set; }
        public DateTime? DueDate { get; set; }
        public bool? IsRead { get; set; }
        public int? StatusCodeId { get; set; }

        public virtual ApplicationUser InviteUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual TaskMaster Task { get; set; }
    }
}
