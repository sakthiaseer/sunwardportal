﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WorkFlowInteractionRecipient
    {
        public long InteractionRecipientId { get; set; }
        public long InteractionId { get; set; }
        public long? RecipientId { get; set; }
        public long? RecipientGroupId { get; set; }
        public bool? IsRead { get; set; }

        public virtual WorkFlowInteraction Interaction { get; set; }
        public virtual ApplicationUser Recipient { get; set; }
    }
}
