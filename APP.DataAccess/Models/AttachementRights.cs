﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AttachementRights
    {
        public long AttachmentRights { get; set; }
        public long? UserId { get; set; }
        public long? GroupId { get; set; }
    }
}
