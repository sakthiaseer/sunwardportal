﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class GroupLocationLink
    {
        public long GroupLocationId { get; set; }
        public long? LocationId { get; set; }
        public long? LocationGroupId { get; set; }

        public virtual Locations Location { get; set; }
        public virtual LocationGroup LocationGroup { get; set; }
    }
}
