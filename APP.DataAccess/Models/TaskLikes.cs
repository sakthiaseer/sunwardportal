﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TaskLikes
    {
        public long TaskLikesId { get; set; }
        public long? TaskMasterId { get; set; }
        public long? LikedBy { get; set; }

        public virtual TaskMaster TaskMaster { get; set; }
    }
}
