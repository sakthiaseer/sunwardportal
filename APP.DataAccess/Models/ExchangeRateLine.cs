﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ExchangeRateLine
    {
        public long ExchangeRateLineId { get; set; }
        public long? ExchangeRateId { get; set; }
        public decimal? Units { get; set; }
        public long? CurrencyId { get; set; }
        public decimal? EquivalentUnitId { get; set; }
        public long? EquivalebtCurrencyId { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ExchangeRate ExchangeRate { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
