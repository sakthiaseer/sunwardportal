﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmailConversationAssignToUserGroup
    {
        public long Id { get; set; }
        public long GroupId { get; set; }
        public long ConversationId { get; set; }
        public long TopicId { get; set; }
        public int AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public int? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
