﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormWorkFlowFormDelegate
    {
        public long DynamicFormWorkFlowFormDelegateId { get; set; }
        public long? DynamicFormWorkFlowFormId { get; set; }
        public long? UserId { get; set; }

        public virtual DynamicFormWorkFlowForm DynamicFormWorkFlowForm { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
