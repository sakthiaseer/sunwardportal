﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewGetRoles
    {
        public long RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string StatusCode { get; set; }
        public int StatusCodeId { get; set; }
        public string CodeDescription { get; set; }
        public string AddedByUser { get; set; }
        public string AddedByUseName { get; set; }
        public string ModifyiedByUser { get; set; }
        public string ModifyiedByUserName { get; set; }
    }
}
