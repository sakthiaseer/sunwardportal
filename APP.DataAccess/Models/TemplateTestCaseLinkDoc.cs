﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TemplateTestCaseLinkDoc
    {
        public long TemplateTestCaseLinkDocId { get; set; }
        public long? TemplateTestCaseLinkId { get; set; }
        public string DocumentNo { get; set; }
        public Guid? SessionId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? StatusCodeId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual TemplateTestCaseLink TemplateTestCaseLink { get; set; }
    }
}
