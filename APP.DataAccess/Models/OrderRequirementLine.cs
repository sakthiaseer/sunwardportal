﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class OrderRequirementLine
    {
        public OrderRequirementLine()
        {
            OrderRequirementLineSplit = new HashSet<OrderRequirementLineSplit>();
        }

        public long OrderRequirementLineId { get; set; }
        public long? OrderRequirementId { get; set; }
        public long? ProductId { get; set; }
        public string TicketBatchSizeId { get; set; }
        public decimal? NoOfTicket { get; set; }
        public long? NavLocationId { get; set; }
        public long? NavUomid { get; set; }
        public DateTime? ExpectedStartDate { get; set; }
        public decimal? ProductQty { get; set; }
        public bool? RequireToSplit { get; set; }
        public string Remarks { get; set; }
        public bool? IsNavSync { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual OrderRequirement OrderRequirement { get; set; }
        public virtual Navitems Product { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<OrderRequirementLineSplit> OrderRequirementLineSplit { get; set; }
    }
}
