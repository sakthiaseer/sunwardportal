﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SalesItemPackingRequirement
    {
        public long SalesItemPackingRequirementId { get; set; }
        public long? SalesItemPackingInfoId { get; set; }
        public long? PackingRequirementId { get; set; }
        public long? SalesItemPackingInfoLineId { get; set; }

        public virtual SalesItemPackingInfo SalesItemPackingInfo { get; set; }
        public virtual SalesItemPackingInforLine SalesItemPackingInfoLine { get; set; }
    }
}
