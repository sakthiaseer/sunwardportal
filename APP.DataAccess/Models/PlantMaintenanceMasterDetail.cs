﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PlantMaintenanceMasterDetail
    {
        public long PlantMaintenanceMasterDetailId { get; set; }
        public long? PlantEntryLineId { get; set; }
        public long? PlantMaintenanceEntryMasterId { get; set; }

        public virtual PlantMaintenanceEntryLine PlantEntryLine { get; set; }
        public virtual PlantMaintenanceEntryMaster PlantMaintenanceEntryMaster { get; set; }
    }
}
