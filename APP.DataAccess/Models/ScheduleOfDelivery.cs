﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ScheduleOfDelivery
    {
        public ScheduleOfDelivery()
        {
            ScheduleOfDeliveryPlanOfWeek = new HashSet<ScheduleOfDeliveryPlanOfWeek>();
        }

        public long ScheduleOfDeliveryId { get; set; }
        public long? DeliveryCompanyId { get; set; }
        public long? ShipmentNo { get; set; }
        public long? PlanweekId { get; set; }
        public int? DayOfTheWeekId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ProfileLinkReferenceNo { get; set; }
        public string LinkProfileReferenceNo { get; set; }
        public string MasterProfileReferenceNo { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster DayOfTheWeek { get; set; }
        public virtual CompanyListing DeliveryCompany { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual SobyCustomersAddress ShipmentNoNavigation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ScheduleOfDeliveryPlanOfWeek> ScheduleOfDeliveryPlanOfWeek { get; set; }
    }
}
