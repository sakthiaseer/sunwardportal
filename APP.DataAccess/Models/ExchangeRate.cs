﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ExchangeRate
    {
        public ExchangeRate()
        {
            ExchangeRateLine = new HashSet<ExchangeRateLine>();
        }

        public long ExchangeRateId { get; set; }
        public long? PurposeId { get; set; }
        public string Description { get; set; }
        public DateTime? ValidPeriodFrom { get; set; }
        public DateTime? ValidPeriodTo { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ExchangeRateLine> ExchangeRateLine { get; set; }
    }
}
