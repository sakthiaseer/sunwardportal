﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApplicationMasterDetail
    {
        public ApplicationMasterDetail()
        {
            ActivityMasterMultiple = new HashSet<ActivityMasterMultiple>();
            ActivityResultMultiple = new HashSet<ActivityResultMultiple>();
            AppSamplingLine = new HashSet<AppSamplingLine>();
            AppTranDocumentLine = new HashSet<AppTranDocumentLine>();
            ApplicationWiki = new HashSet<ApplicationWiki>();
            AssetCatalogMasterAssetModel = new HashSet<AssetCatalogMaster>();
            AssetCatalogMasterAssetUom = new HashSet<AssetCatalogMaster>();
            AssetEquipmentMaintenaceMaster = new HashSet<AssetEquipmentMaintenaceMaster>();
            AssetEquipmentMaintenaceMasterAssetDocument = new HashSet<AssetEquipmentMaintenaceMasterAssetDocument>();
            AssetMaster = new HashSet<AssetMaster>();
            BlisterMouldInformationBlistedMouldParts = new HashSet<BlisterMouldInformation>();
            BlisterMouldInformationBlisterKnurlingPattern = new HashSet<BlisterMouldInformation>();
            BlisterMouldInformationCuNoFormat = new HashSet<BlisterMouldInformation>();
            BlisterMouldInformationFeeder = new HashSet<BlisterMouldInformation>();
            BlisterMouldInformationFormat = new HashSet<BlisterMouldInformation>();
            Bmrmovement = new HashSet<Bmrmovement>();
            Ccfdimplementation = new HashSet<Ccfdimplementation>();
            CcfdimplementationDetails = new HashSet<CcfdimplementationDetails>();
            CommonFieldsProductionMachineMachineGrouping = new HashSet<CommonFieldsProductionMachine>();
            CommonFieldsProductionMachineManufacturingProcess = new HashSet<CommonFieldsProductionMachine>();
            CommonFieldsProductionMachineManufacturingSite = new HashSet<CommonFieldsProductionMachine>();
            CommonFieldsProductionMachineManufacturingSteps = new HashSet<CommonFieldsProductionMachine>();
            CommonMasterMouldChangePartsMachineGrouping = new HashSet<CommonMasterMouldChangeParts>();
            CommonMasterMouldChangePartsPmarking = new HashSet<CommonMasterMouldChangeParts>();
            CommonMasterMouldChangePartsPshape = new HashSet<CommonMasterMouldChangeParts>();
            CommonPackagingItemHeaderPackagingItemCategory = new HashSet<CommonPackagingItemHeader>();
            CommonPackagingItemHeaderUom = new HashSet<CommonPackagingItemHeader>();
            CommonProcess = new HashSet<CommonProcess>();
            CommonProcessDosageMultiple = new HashSet<CommonProcessDosageMultiple>();
            CompanyCalendarLineCalenderStatus = new HashSet<CompanyCalendarLine>();
            CompanyCalendarLineTypeOfEvent = new HashSet<CompanyCalendarLine>();
            CompanyListingCustomerCode = new HashSet<CompanyListingCustomerCode>();
            DistributorReplenishmentDosageForm = new HashSet<DistributorReplenishment>();
            DistributorReplenishmentDrugClassification = new HashSet<DistributorReplenishment>();
            DocumentProfileNoSeriesCategory = new HashSet<DocumentProfileNoSeries>();
            DocumentProfileNoSeriesGroup = new HashSet<DocumentProfileNoSeries>();
            DraftApplicationWiki = new HashSet<DraftApplicationWiki>();
            EmployeeEmailInfoEmailGuide = new HashSet<EmployeeEmailInfo>();
            EmployeeEmailInfoSubscription = new HashSet<EmployeeEmailInfo>();
            EmployeeIcthardInformation = new HashSet<EmployeeIcthardInformation>();
            EmployeeOtherDutyInformation = new HashSet<EmployeeOtherDutyInformation>();
            FinishProdcutGeneralInfoLineCapacity = new HashSet<FinishProdcutGeneralInfoLine>();
            FinishProdcutGeneralInfoLineClosure = new HashSet<FinishProdcutGeneralInfoLine>();
            FinishProdcutGeneralInfoLineLiner = new HashSet<FinishProdcutGeneralInfoLine>();
            Fmglobal = new HashSet<Fmglobal>();
            FmglobalLine = new HashSet<FmglobalLine>();
            FmglobalLineItem = new HashSet<FmglobalLineItem>();
            FpcommonFieldDosageForm = new HashSet<FpcommonField>();
            FpcommonFieldFinishProduct = new HashSet<FpcommonField>();
            FpdrugClassificationCountry = new HashSet<FpdrugClassification>();
            FpdrugClassificationDrugClassification = new HashSet<FpdrugClassification>();
            GeneralEquivalaentNavision = new HashSet<GeneralEquivalaentNavision>();
            HandlingOfShipmentClassificationShipmentMethod = new HashSet<HandlingOfShipmentClassification>();
            HandlingOfShipmentClassificationSpecificTypeOfOrder = new HashSet<HandlingOfShipmentClassification>();
            IpirApp = new HashSet<IpirApp>();
            IpirAppIssueDep = new HashSet<IpirAppIssueDep>();
            IpirReport = new HashSet<IpirReport>();
            ItemClassificationHeaderMaterial = new HashSet<ItemClassificationHeader>();
            ItemClassificationHeaderUom = new HashSet<ItemClassificationHeader>();
            JobProgressTemplateLineLineProcess = new HashSet<JobProgressTemplateLineLineProcess>();
            JobProgressTemplateModule = new HashSet<JobProgressTemplate>();
            JobProgressTemplateTemplate = new HashSet<JobProgressTemplate>();
            MedCatalogClassificationCategory = new HashSet<MedCatalogClassification>();
            MedCatalogClassificationGroup = new HashSet<MedCatalogClassification>();
            MedMaster = new HashSet<MedMaster>();
            MedMasterLine = new HashSet<MedMasterLine>();
            NavisionSpecification = new HashSet<NavisionSpecification>();
            NavitemsPackSizeNavigation = new HashSet<Navitems>();
            NavitemsSupplyTo = new HashSet<Navitems>();
            NavitemsUom = new HashSet<Navitems>();
            Notice = new HashSet<Notice>();
            Npraformulation = new HashSet<Npraformulation>();
            NpraformulationActiveIngredientActiveIngredient = new HashSet<NpraformulationActiveIngredient>();
            NpraformulationActiveIngredientFormOfSubstance = new HashSet<NpraformulationActiveIngredient>();
            NpraformulationActiveIngredientSaltForm = new HashSet<NpraformulationActiveIngredient>();
            NpraformulationActiveIngredientSource = new HashSet<NpraformulationActiveIngredient>();
            NpraformulationActiveIngredientStrengthSaltFree = new HashSet<NpraformulationActiveIngredient>();
            NpraformulationActiveIngredientUnits = new HashSet<NpraformulationActiveIngredient>();
            NpraformulationExcipientExcipient = new HashSet<NpraformulationExcipient>();
            NpraformulationExcipientFunction = new HashSet<NpraformulationExcipient>();
            NpraformulationExcipientRemarksFunction = new HashSet<NpraformulationExcipient>();
            NpraformulationExcipientSource = new HashSet<NpraformulationExcipient>();
            NpraformulationExcipientUnits = new HashSet<NpraformulationExcipient>();
            Portfolio = new HashSet<Portfolio>();
            PortfolioLine = new HashSet<PortfolioLine>();
            ProcessMachineTimeNavBatchSize = new HashSet<ProcessMachineTime>();
            ProcessMachineTimeProductionArea = new HashSet<ProcessMachineTime>();
            ProcessMachineTimeProductionMachineProcessClassificationCompany = new HashSet<ProcessMachineTimeProductionMachineProcess>();
            ProcessMachineTimeProductionMachineProcessManufacturingProcess = new HashSet<ProcessMachineTimeProductionMachineProcess>();
            ProcessMachineTimeProductionMachineProcessManufacturingSteps = new HashSet<ProcessMachineTimeProductionMachineProcess>();
            ProductActivityCaseCompany = new HashSet<ProductActivityCaseCompany>();
            ProductActivityCaseLineProdActivityAction = new HashSet<ProductActivityCaseLine>();
            ProductActivityCaseLineProdActivityCategory = new HashSet<ProductActivityCaseLine>();
            ProductActivityCaseManufacturingProcess = new HashSet<ProductActivityCase>();
            ProductActivityCaseRespons = new HashSet<ProductActivityCaseRespons>();
            ProductActivityCaseVersionCodeStatus = new HashSet<ProductActivityCase>();
            ProductGroupingManufactureDosageForm = new HashSet<ProductGroupingManufacture>();
            ProductGroupingManufactureDrugClassification = new HashSet<ProductGroupingManufacture>();
            ProductionActivityActivityDocument = new HashSet<ProductionActivity>();
            ProductionActivityAppLineActivityMaster = new HashSet<ProductionActivityAppLine>();
            ProductionActivityAppLineActivityStatus = new HashSet<ProductionActivityAppLine>();
            ProductionActivityAppLineManufacturingProcess = new HashSet<ProductionActivityAppLine>();
            ProductionActivityAppLineProdActivityAction = new HashSet<ProductionActivityAppLine>();
            ProductionActivityAppLineProdActivityCategory = new HashSet<ProductionActivityAppLine>();
            ProductionActivityAppLineProdActivityResult = new HashSet<ProductionActivityAppLine>();
            ProductionActivityMasterLineProdActivityAction = new HashSet<ProductionActivityMasterLine>();
            ProductionActivityMasterLineProdActivityCategory = new HashSet<ProductionActivityMasterLine>();
            ProductionActivityMasterRespons = new HashSet<ProductionActivityMasterRespons>();
            ProductionActivityPlanningAppLineManufacturingProcess = new HashSet<ProductionActivityPlanningAppLine>();
            ProductionActivityPlanningAppLineProdActivityAction = new HashSet<ProductionActivityPlanningAppLine>();
            ProductionActivityPlanningAppLineProdActivityCategory = new HashSet<ProductionActivityPlanningAppLine>();
            ProductionActivityPlanningAppLineProdActivityResult = new HashSet<ProductionActivityPlanningAppLine>();
            ProductionActivityProcess = new HashSet<ProductionActivity>();
            ProductionActivityRoutineAppLineManufacturingProcess = new HashSet<ProductionActivityRoutineAppLine>();
            ProductionActivityRoutineAppLineProdActivityAction = new HashSet<ProductionActivityRoutineAppLine>();
            ProductionActivityRoutineAppLineProdActivityCategory = new HashSet<ProductionActivityRoutineAppLine>(); 
            ProductionActivityRoutineAppLineProdActivityResult = new HashSet<ProductionActivityRoutineAppLine>();
            ProductionActivityRoutineAppLineRoutineStatus = new HashSet<ProductionActivityRoutineAppLine>();
            ProductionActivityRoutineAppLineVisaMaster = new HashSet<ProductionActivityRoutineAppLine>();
            ProductionCapsuleColor = new HashSet<ProductionCapsule>();
            ProductionCapsuleItemNo = new HashSet<ProductionCapsule>();
            ProductionCapsulePurpose = new HashSet<ProductionCapsule>();
            ProductionCapsuleRndpurpose = new HashSet<ProductionCapsule>();
            ProductionCapsuleSize = new HashSet<ProductionCapsule>();
            ProductionColorColor = new HashSet<ProductionColor>();
            ProductionColorColorIndexNumber = new HashSet<ProductionColor>();
            ProductionColorGenericNameNavigation = new HashSet<ProductionColor>();
            ProductionColorPurpose = new HashSet<ProductionColor>();
            ProductionColorRndpurpose = new HashSet<ProductionColor>();
            ProductionFlavourFlavour = new HashSet<ProductionFlavour>();
            ProductionFlavourPurpose = new HashSet<ProductionFlavour>();
            ProductionFlavourRndpurpose = new HashSet<ProductionFlavour>();
            ProductionFlavourSupplier = new HashSet<ProductionFlavour>();
            ProductionSimulationGroupingSellingStatus = new HashSet<ProductionSimulationGrouping>();
            ProductionSimulationGroupingTypeOfProdOrder = new HashSet<ProductionSimulationGrouping>();
            QuotationAward = new HashSet<QuotationAward>();
            ReAssignmentJob = new HashSet<ReAssignmentJob>();
            ReceiveEmail = new HashSet<ReceiveEmail>();
            RegistrationVariationRegistrationCategoryNavigation = new HashSet<RegistrationVariation>();
            RegistrationVariationRegistrationClassification = new HashSet<RegistrationVariation>();
            RegistrationVariationRegistrationCountry = new HashSet<RegistrationVariation>();
            RegistrationVariationRegistrationVariationCode = new HashSet<RegistrationVariation>();
            RegistrationVariationRegistrationVariationType = new HashSet<RegistrationVariation>();
            SalesOrderMasterPricingLineSellingMethod = new HashSet<SalesOrderMasterPricingLine>();
            SalesOrderMasterPricingLineSmAllowPrice = new HashSet<SalesOrderMasterPricingLine>();
            SalesOrderMasterPricingReasonForChange = new HashSet<SalesOrderMasterPricing>();
            SalesOrderMasterPricingSalesPricingFor = new HashSet<SalesOrderMasterPricing>();
            SalesTargetByStaffCompany = new HashSet<SalesTargetByStaff>();
            SalesTargetByStaffSalesCover = new HashSet<SalesTargetByStaffSalesCover>();
            SalesTargetByStaffSalesGroup = new HashSet<SalesTargetByStaff>();
            SampleRequestForDoctorHeader = new HashSet<SampleRequestForDoctorHeader>();
            SampleRequestForm = new HashSet<SampleRequestForm>();
            SellingCatalogue = new HashSet<SellingCatalogue>();
            SellingPriceInformationBonusCurrency = new HashSet<SellingPriceInformation>();
            SellingPriceInformationPricingMethod = new HashSet<SellingPriceInformation>();
            SellingPricingTiersPricingMethod = new HashSet<SellingPricingTiers>();
            SellingPricingTiersSellingCurrency = new HashSet<SellingPricingTiers>();
            SoSalesOrderLine = new HashSet<SoSalesOrderLine>();
            SobyCustomersAddress = new HashSet<SobyCustomersAddress>();
            SobyCustomersMasterAddressCity = new HashSet<SobyCustomersMasterAddress>();
            SobyCustomersMasterAddressCountry = new HashSet<SobyCustomersMasterAddress>();
            SobyCustomersMasterAddressState = new HashSet<SobyCustomersMasterAddress>();
            SocustomersItemCrossReferenceCustomerPackingUom = new HashSet<SocustomersItemCrossReference>();
            SocustomersItemCrossReferencePerUom = new HashSet<SocustomersItemCrossReference>();
            SocustomersItemCrossReferencePurchasePerUom = new HashSet<SocustomersItemCrossReference>();
            WorkOrderLine = new HashSet<WorkOrderLine>();
            WorkOrderModule = new HashSet<WorkOrder>();
            WorkOrderNavisionModule = new HashSet<WorkOrder>();
            WorkOrderRequirement = new HashSet<WorkOrder>();
        }

        public long ApplicationMasterDetailId { get; set; }
        public long ApplicationMasterId { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ProfileId { get; set; }
        public long? FileProfileTypeId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationMaster ApplicationMaster { get; set; }
        public virtual FileProfileType FileProfileType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ActivityMasterMultiple> ActivityMasterMultiple { get; set; }
        public virtual ICollection<ActivityResultMultiple> ActivityResultMultiple { get; set; }
        public virtual ICollection<AppSamplingLine> AppSamplingLine { get; set; }
        public virtual ICollection<AppTranDocumentLine> AppTranDocumentLine { get; set; }
        public virtual ICollection<ApplicationWiki> ApplicationWiki { get; set; }
        public virtual ICollection<AssetCatalogMaster> AssetCatalogMasterAssetModel { get; set; }
        public virtual ICollection<AssetCatalogMaster> AssetCatalogMasterAssetUom { get; set; }
        public virtual ICollection<AssetEquipmentMaintenaceMaster> AssetEquipmentMaintenaceMaster { get; set; }
        public virtual ICollection<AssetEquipmentMaintenaceMasterAssetDocument> AssetEquipmentMaintenaceMasterAssetDocument { get; set; }
        public virtual ICollection<AssetMaster> AssetMaster { get; set; }
        public virtual ICollection<BlisterMouldInformation> BlisterMouldInformationBlistedMouldParts { get; set; }
        public virtual ICollection<BlisterMouldInformation> BlisterMouldInformationBlisterKnurlingPattern { get; set; }
        public virtual ICollection<BlisterMouldInformation> BlisterMouldInformationCuNoFormat { get; set; }
        public virtual ICollection<BlisterMouldInformation> BlisterMouldInformationFeeder { get; set; }
        public virtual ICollection<BlisterMouldInformation> BlisterMouldInformationFormat { get; set; }
        public virtual ICollection<Bmrmovement> Bmrmovement { get; set; }
        public virtual ICollection<Ccfdimplementation> Ccfdimplementation { get; set; }
        public virtual ICollection<CcfdimplementationDetails> CcfdimplementationDetails { get; set; }
        public virtual ICollection<CommonFieldsProductionMachine> CommonFieldsProductionMachineMachineGrouping { get; set; }
        public virtual ICollection<CommonFieldsProductionMachine> CommonFieldsProductionMachineManufacturingProcess { get; set; }
        public virtual ICollection<CommonFieldsProductionMachine> CommonFieldsProductionMachineManufacturingSite { get; set; }
        public virtual ICollection<CommonFieldsProductionMachine> CommonFieldsProductionMachineManufacturingSteps { get; set; }
        public virtual ICollection<CommonMasterMouldChangeParts> CommonMasterMouldChangePartsMachineGrouping { get; set; }
        public virtual ICollection<CommonMasterMouldChangeParts> CommonMasterMouldChangePartsPmarking { get; set; }
        public virtual ICollection<CommonMasterMouldChangeParts> CommonMasterMouldChangePartsPshape { get; set; }
        public virtual ICollection<CommonPackagingItemHeader> CommonPackagingItemHeaderPackagingItemCategory { get; set; }
        public virtual ICollection<CommonPackagingItemHeader> CommonPackagingItemHeaderUom { get; set; }
        public virtual ICollection<CommonProcess> CommonProcess { get; set; }
        public virtual ICollection<CommonProcessDosageMultiple> CommonProcessDosageMultiple { get; set; }
        public virtual ICollection<CompanyCalendarLine> CompanyCalendarLineCalenderStatus { get; set; }
        public virtual ICollection<CompanyCalendarLine> CompanyCalendarLineTypeOfEvent { get; set; }
        public virtual ICollection<CompanyListingCustomerCode> CompanyListingCustomerCode { get; set; }
        public virtual ICollection<DistributorReplenishment> DistributorReplenishmentDosageForm { get; set; }
        public virtual ICollection<DistributorReplenishment> DistributorReplenishmentDrugClassification { get; set; }
        public virtual ICollection<DocumentProfileNoSeries> DocumentProfileNoSeriesCategory { get; set; }
        public virtual ICollection<DocumentProfileNoSeries> DocumentProfileNoSeriesGroup { get; set; }
        public virtual ICollection<DraftApplicationWiki> DraftApplicationWiki { get; set; }
        public virtual ICollection<EmployeeEmailInfo> EmployeeEmailInfoEmailGuide { get; set; }
        public virtual ICollection<EmployeeEmailInfo> EmployeeEmailInfoSubscription { get; set; }
        public virtual ICollection<EmployeeIcthardInformation> EmployeeIcthardInformation { get; set; }
        public virtual ICollection<EmployeeOtherDutyInformation> EmployeeOtherDutyInformation { get; set; }
        public virtual ICollection<FinishProdcutGeneralInfoLine> FinishProdcutGeneralInfoLineCapacity { get; set; }
        public virtual ICollection<FinishProdcutGeneralInfoLine> FinishProdcutGeneralInfoLineClosure { get; set; }
        public virtual ICollection<FinishProdcutGeneralInfoLine> FinishProdcutGeneralInfoLineLiner { get; set; }
        public virtual ICollection<Fmglobal> Fmglobal { get; set; }
        public virtual ICollection<FmglobalLine> FmglobalLine { get; set; }
        public virtual ICollection<FmglobalLineItem> FmglobalLineItem { get; set; }
        public virtual ICollection<FpcommonField> FpcommonFieldDosageForm { get; set; }
        public virtual ICollection<FpcommonField> FpcommonFieldFinishProduct { get; set; }
        public virtual ICollection<FpdrugClassification> FpdrugClassificationCountry { get; set; }
        public virtual ICollection<FpdrugClassification> FpdrugClassificationDrugClassification { get; set; }
        public virtual ICollection<GeneralEquivalaentNavision> GeneralEquivalaentNavision { get; set; }
        public virtual ICollection<HandlingOfShipmentClassification> HandlingOfShipmentClassificationShipmentMethod { get; set; }
        public virtual ICollection<HandlingOfShipmentClassification> HandlingOfShipmentClassificationSpecificTypeOfOrder { get; set; }
        public virtual ICollection<IpirApp> IpirApp { get; set; }
        public virtual ICollection<IpirAppIssueDep> IpirAppIssueDep { get; set; }
        public virtual ICollection<IpirReport> IpirReport { get; set; }
        public virtual ICollection<ItemClassificationHeader> ItemClassificationHeaderMaterial { get; set; }
        public virtual ICollection<ItemClassificationHeader> ItemClassificationHeaderUom { get; set; }
        public virtual ICollection<JobProgressTemplateLineLineProcess> JobProgressTemplateLineLineProcess { get; set; }
        public virtual ICollection<JobProgressTemplate> JobProgressTemplateModule { get; set; }
        public virtual ICollection<JobProgressTemplate> JobProgressTemplateTemplate { get; set; }
        public virtual ICollection<MedCatalogClassification> MedCatalogClassificationCategory { get; set; }
        public virtual ICollection<MedCatalogClassification> MedCatalogClassificationGroup { get; set; }
        public virtual ICollection<MedMaster> MedMaster { get; set; }
        public virtual ICollection<MedMasterLine> MedMasterLine { get; set; }
        public virtual ICollection<NavisionSpecification> NavisionSpecification { get; set; }
        public virtual ICollection<Navitems> NavitemsPackSizeNavigation { get; set; }
        public virtual ICollection<Navitems> NavitemsSupplyTo { get; set; }
        public virtual ICollection<Navitems> NavitemsUom { get; set; }
        public virtual ICollection<Notice> Notice { get; set; }
        public virtual ICollection<Npraformulation> Npraformulation { get; set; }
        public virtual ICollection<NpraformulationActiveIngredient> NpraformulationActiveIngredientActiveIngredient { get; set; }
        public virtual ICollection<NpraformulationActiveIngredient> NpraformulationActiveIngredientFormOfSubstance { get; set; }
        public virtual ICollection<NpraformulationActiveIngredient> NpraformulationActiveIngredientSaltForm { get; set; }
        public virtual ICollection<NpraformulationActiveIngredient> NpraformulationActiveIngredientSource { get; set; }
        public virtual ICollection<NpraformulationActiveIngredient> NpraformulationActiveIngredientStrengthSaltFree { get; set; }
        public virtual ICollection<NpraformulationActiveIngredient> NpraformulationActiveIngredientUnits { get; set; }
        public virtual ICollection<NpraformulationExcipient> NpraformulationExcipientExcipient { get; set; }
        public virtual ICollection<NpraformulationExcipient> NpraformulationExcipientFunction { get; set; }
        public virtual ICollection<NpraformulationExcipient> NpraformulationExcipientRemarksFunction { get; set; }
        public virtual ICollection<NpraformulationExcipient> NpraformulationExcipientSource { get; set; }
        public virtual ICollection<NpraformulationExcipient> NpraformulationExcipientUnits { get; set; }
        public virtual ICollection<Portfolio> Portfolio { get; set; }
        public virtual ICollection<PortfolioLine> PortfolioLine { get; set; }
        public virtual ICollection<ProcessMachineTime> ProcessMachineTimeNavBatchSize { get; set; }
        public virtual ICollection<ProcessMachineTime> ProcessMachineTimeProductionArea { get; set; }
        public virtual ICollection<ProcessMachineTimeProductionMachineProcess> ProcessMachineTimeProductionMachineProcessClassificationCompany { get; set; }
        public virtual ICollection<ProcessMachineTimeProductionMachineProcess> ProcessMachineTimeProductionMachineProcessManufacturingProcess { get; set; }
        public virtual ICollection<ProcessMachineTimeProductionMachineProcess> ProcessMachineTimeProductionMachineProcessManufacturingSteps { get; set; }
        public virtual ICollection<ProductActivityCaseCompany> ProductActivityCaseCompany { get; set; }
        public virtual ICollection<ProductActivityCaseLine> ProductActivityCaseLineProdActivityAction { get; set; }
        public virtual ICollection<ProductActivityCaseLine> ProductActivityCaseLineProdActivityCategory { get; set; }
        public virtual ICollection<ProductActivityCase> ProductActivityCaseManufacturingProcess { get; set; }
        public virtual ICollection<ProductActivityCaseRespons> ProductActivityCaseRespons { get; set; }
        public virtual ICollection<ProductActivityCase> ProductActivityCaseVersionCodeStatus { get; set; }
        public virtual ICollection<ProductGroupingManufacture> ProductGroupingManufactureDosageForm { get; set; }
        public virtual ICollection<ProductGroupingManufacture> ProductGroupingManufactureDrugClassification { get; set; }
        public virtual ICollection<ProductionActivity> ProductionActivityActivityDocument { get; set; }
        public virtual ICollection<ProductionActivityAppLine> ProductionActivityAppLineActivityMaster { get; set; }
        public virtual ICollection<ProductionActivityAppLine> ProductionActivityAppLineActivityStatus { get; set; }
        public virtual ICollection<ProductionActivityAppLine> ProductionActivityAppLineManufacturingProcess { get; set; }
        public virtual ICollection<ProductionActivityAppLine> ProductionActivityAppLineProdActivityAction { get; set; }
        public virtual ICollection<ProductionActivityAppLine> ProductionActivityAppLineProdActivityCategory { get; set; }
        public virtual ICollection<ProductionActivityAppLine> ProductionActivityAppLineProdActivityResult { get; set; }
        public virtual ICollection<ProductionActivityMasterLine> ProductionActivityMasterLineProdActivityAction { get; set; }
        public virtual ICollection<ProductionActivityMasterLine> ProductionActivityMasterLineProdActivityCategory { get; set; }
        public virtual ICollection<ProductionActivityMasterRespons> ProductionActivityMasterRespons { get; set; }
        public virtual ICollection<ProductionActivityPlanningAppLine> ProductionActivityPlanningAppLineManufacturingProcess { get; set; }
        public virtual ICollection<ProductionActivityPlanningAppLine> ProductionActivityPlanningAppLineProdActivityAction { get; set; }
        public virtual ICollection<ProductionActivityPlanningAppLine> ProductionActivityPlanningAppLineProdActivityCategory { get; set; }
        public virtual ICollection<ProductionActivityPlanningAppLine> ProductionActivityPlanningAppLineProdActivityResult { get; set; }
        public virtual ICollection<ProductionActivity> ProductionActivityProcess { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLine> ProductionActivityRoutineAppLineManufacturingProcess { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLine> ProductionActivityRoutineAppLineProdActivityAction { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLine> ProductionActivityRoutineAppLineProdActivityCategory { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLine> ProductionActivityRoutineAppLineProdActivityResult { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLine> ProductionActivityRoutineAppLineRoutineStatus { get; set; }
        public virtual ICollection<ProductionActivityRoutineAppLine> ProductionActivityRoutineAppLineVisaMaster { get; set; }
        public virtual ICollection<ProductionCapsule> ProductionCapsuleColor { get; set; }
        public virtual ICollection<ProductionCapsule> ProductionCapsuleItemNo { get; set; }
        public virtual ICollection<ProductionCapsule> ProductionCapsulePurpose { get; set; }
        public virtual ICollection<ProductionCapsule> ProductionCapsuleRndpurpose { get; set; }
        public virtual ICollection<ProductionCapsule> ProductionCapsuleSize { get; set; }
        public virtual ICollection<ProductionColor> ProductionColorColor { get; set; }
        public virtual ICollection<ProductionColor> ProductionColorColorIndexNumber { get; set; }
        public virtual ICollection<ProductionColor> ProductionColorGenericNameNavigation { get; set; }
        public virtual ICollection<ProductionColor> ProductionColorPurpose { get; set; }
        public virtual ICollection<ProductionColor> ProductionColorRndpurpose { get; set; }
        public virtual ICollection<ProductionFlavour> ProductionFlavourFlavour { get; set; }
        public virtual ICollection<ProductionFlavour> ProductionFlavourPurpose { get; set; }
        public virtual ICollection<ProductionFlavour> ProductionFlavourRndpurpose { get; set; }
        public virtual ICollection<ProductionFlavour> ProductionFlavourSupplier { get; set; }
        public virtual ICollection<ProductionSimulationGrouping> ProductionSimulationGroupingSellingStatus { get; set; }
        public virtual ICollection<ProductionSimulationGrouping> ProductionSimulationGroupingTypeOfProdOrder { get; set; }
        public virtual ICollection<QuotationAward> QuotationAward { get; set; }
        public virtual ICollection<ReAssignmentJob> ReAssignmentJob { get; set; }
        public virtual ICollection<ReceiveEmail> ReceiveEmail { get; set; }
        public virtual ICollection<RegistrationVariation> RegistrationVariationRegistrationCategoryNavigation { get; set; }
        public virtual ICollection<RegistrationVariation> RegistrationVariationRegistrationClassification { get; set; }
        public virtual ICollection<RegistrationVariation> RegistrationVariationRegistrationCountry { get; set; }
        public virtual ICollection<RegistrationVariation> RegistrationVariationRegistrationVariationCode { get; set; }
        public virtual ICollection<RegistrationVariation> RegistrationVariationRegistrationVariationType { get; set; }
        public virtual ICollection<SalesOrderMasterPricingLine> SalesOrderMasterPricingLineSellingMethod { get; set; }
        public virtual ICollection<SalesOrderMasterPricingLine> SalesOrderMasterPricingLineSmAllowPrice { get; set; }
        public virtual ICollection<SalesOrderMasterPricing> SalesOrderMasterPricingReasonForChange { get; set; }
        public virtual ICollection<SalesOrderMasterPricing> SalesOrderMasterPricingSalesPricingFor { get; set; }
        public virtual ICollection<SalesTargetByStaff> SalesTargetByStaffCompany { get; set; }
        public virtual ICollection<SalesTargetByStaffSalesCover> SalesTargetByStaffSalesCover { get; set; }
        public virtual ICollection<SalesTargetByStaff> SalesTargetByStaffSalesGroup { get; set; }
        public virtual ICollection<SampleRequestForDoctorHeader> SampleRequestForDoctorHeader { get; set; }
        public virtual ICollection<SampleRequestForm> SampleRequestForm { get; set; }
        public virtual ICollection<SellingCatalogue> SellingCatalogue { get; set; }
        public virtual ICollection<SellingPriceInformation> SellingPriceInformationBonusCurrency { get; set; }
        public virtual ICollection<SellingPriceInformation> SellingPriceInformationPricingMethod { get; set; }
        public virtual ICollection<SellingPricingTiers> SellingPricingTiersPricingMethod { get; set; }
        public virtual ICollection<SellingPricingTiers> SellingPricingTiersSellingCurrency { get; set; }
        public virtual ICollection<SoSalesOrderLine> SoSalesOrderLine { get; set; }
        public virtual ICollection<SobyCustomersAddress> SobyCustomersAddress { get; set; }
        public virtual ICollection<SobyCustomersMasterAddress> SobyCustomersMasterAddressCity { get; set; }
        public virtual ICollection<SobyCustomersMasterAddress> SobyCustomersMasterAddressCountry { get; set; }
        public virtual ICollection<SobyCustomersMasterAddress> SobyCustomersMasterAddressState { get; set; }
        public virtual ICollection<SocustomersItemCrossReference> SocustomersItemCrossReferenceCustomerPackingUom { get; set; }
        public virtual ICollection<SocustomersItemCrossReference> SocustomersItemCrossReferencePerUom { get; set; }
        public virtual ICollection<SocustomersItemCrossReference> SocustomersItemCrossReferencePurchasePerUom { get; set; }
        public virtual ICollection<WorkOrderLine> WorkOrderLine { get; set; }
        public virtual ICollection<WorkOrder> WorkOrderModule { get; set; }
        public virtual ICollection<WorkOrder> WorkOrderNavisionModule { get; set; }
        public virtual ICollection<WorkOrder> WorkOrderRequirement { get; set; }
    }
}
