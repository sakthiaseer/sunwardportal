﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FinisihProductGenericCategory
    {
        public long GenericId { get; set; }
        public long? FinishProductId { get; set; }
        public long? GenericCategoryId { get; set; }

        public virtual FinishProduct FinishProduct { get; set; }
    }
}
