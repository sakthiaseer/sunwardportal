﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormApproved
    {
        public DynamicFormApproved()
        {
            DynamicFormApprovedChanged = new HashSet<DynamicFormApprovedChanged>();
        }

        public long DynamicFormApprovedId { get; set; }
        public long? DynamicFormApprovalId { get; set; }
        public long? DynamicFormDataId { get; set; }
        public bool? IsApproved { get; set; }
        public string ApprovedDescription { get; set; }
        public long? UserId { get; set; }
        public long? ApprovedByUserId { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public int? ApprovedSortBy { get; set; }

        public virtual ApplicationUser ApprovedByUser { get; set; }
        public virtual DynamicFormApproval DynamicFormApproval { get; set; }
        public virtual DynamicFormData DynamicFormData { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual ICollection<DynamicFormApprovedChanged> DynamicFormApprovedChanged { get; set; }
    }
}
