﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavProductMultiple
    {
        public long NavProductMultipleId { get; set; }
        public long? ProductMasterId { get; set; }
        public long? ApplicationMasterDetailId { get; set; }

        public virtual NavProductMaster ProductMaster { get; set; }
    }
}
