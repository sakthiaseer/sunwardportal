﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class OrderRequirement
    {
        public OrderRequirement()
        {
            OrderRequirementLine = new HashSet<OrderRequirementLine>();
        }

        public long OrderRequirementId { get; set; }
        public long? ManufacturingSiteId { get; set; }
        public long? SalesCategoryId { get; set; }
        public long? MethodCodeId { get; set; }
        public long? NavItemId { get; set; }
        public long? GenericCodeId { get; set; }
        public long? DistributorItemId { get; set; }
        public bool? IsNavSync { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual GenericCodes GenericCode { get; set; }
        public virtual Plant ManufacturingSite { get; set; }
        public virtual NavMethodCode MethodCode { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Navitems NavItem { get; set; }
        public virtual NavSaleCategory SalesCategory { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<OrderRequirementLine> OrderRequirementLine { get; set; }
    }
}
