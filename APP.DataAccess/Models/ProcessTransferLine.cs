﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProcessTransferLine
    {
        public long ProcessTransferLineId { get; set; }
        public long ProcessTransferId { get; set; }
        public string DrumInfo { get; set; }
        public string ProcessInfo { get; set; }
        public string Weight { get; set; }
        public string LocationTo { get; set; }
        public int? StatusCodeId { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ProcessTransfer ProcessTransfer { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
