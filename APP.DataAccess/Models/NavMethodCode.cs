﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavMethodCode
    {
        public NavMethodCode()
        {
            NavMethodCodeLines = new HashSet<NavMethodCodeLines>();
            NavMethodCodePlanning = new HashSet<NavMethodCodePlanning>();
            NavProductMaster = new HashSet<NavProductMaster>();
            NavmethodCodeBatch = new HashSet<NavmethodCodeBatch>();
            OrderRequirement = new HashSet<OrderRequirement>();
            ProcessMachineTime = new HashSet<ProcessMachineTime>();
            ProdRecipeCycle = new HashSet<ProdRecipeCycle>();
            ProductionForecast = new HashSet<ProductionForecast>();
            ProductionSimulationGrouping = new HashSet<ProductionSimulationGrouping>();
            ProductionSimulationGroupingTicket = new HashSet<ProductionSimulationGroupingTicket>();
        }

        public long MethodCodeId { get; set; }
        public string MethodName { get; set; }
        public string MethodDescription { get; set; }
        public long? NavinpcategoryId { get; set; }
        public long? CompanyId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public decimal? ProdFrequency { get; set; }
        public decimal? DistReplenishHs { get; set; }
        public decimal? DistAcmonth { get; set; }
        public decimal? AdhocReplenishHs { get; set; }
        public int? AdhocMonthStandAlone { get; set; }
        public decimal? AdhocPlanQty { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Navinpcategory Navinpcategory { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<NavMethodCodeLines> NavMethodCodeLines { get; set; }
        public virtual ICollection<NavMethodCodePlanning> NavMethodCodePlanning { get; set; }
        public virtual ICollection<NavProductMaster> NavProductMaster { get; set; }
        public virtual ICollection<NavmethodCodeBatch> NavmethodCodeBatch { get; set; }
        public virtual ICollection<OrderRequirement> OrderRequirement { get; set; }
        public virtual ICollection<ProcessMachineTime> ProcessMachineTime { get; set; }
        public virtual ICollection<ProdRecipeCycle> ProdRecipeCycle { get; set; }
        public virtual ICollection<ProductionForecast> ProductionForecast { get; set; }
        public virtual ICollection<ProductionSimulationGrouping> ProductionSimulationGrouping { get; set; }
        public virtual ICollection<ProductionSimulationGroupingTicket> ProductionSimulationGroupingTicket { get; set; }
    }
}
