﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TaskUnReadNotes
    {
        public long TaskUnReadNotesId { get; set; }
        public string Notes { get; set; }
        public DateTime? MyDueDate { get; set; }
        public long? UserId { get; set; }
        public long? TaskId { get; set; }
        public long? TaskCommentId { get; set; }
        public DateTime? AddedDate { get; set; }

        public virtual TaskMaster Task { get; set; }
        public virtual TaskComment TaskComment { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
