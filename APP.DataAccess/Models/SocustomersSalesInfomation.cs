﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SocustomersSalesInfomation
    {
        public long SocustomersSalesInfomationId { get; set; }
        public long? SobyCustomersId { get; set; }
        public long? PaymentModeId { get; set; }
        public long? ActivationOfOrderId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual SobyCustomers SobyCustomers { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
