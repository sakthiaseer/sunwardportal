﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AppSamplingLine
    {
        public int AppSamplingLineId { get; set; }
        public int? AppSamplingId { get; set; }
        public long? SamplingPurposeId { get; set; }
        public string DrumNo { get; set; }
        public string QcsampleNo { get; set; }
        public int? Qty { get; set; }
        public string Uom { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual AppSampling AppSampling { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationMasterDetail SamplingPurpose { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
