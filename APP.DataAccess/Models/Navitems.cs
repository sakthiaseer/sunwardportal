﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Navitems
    {
        public Navitems()
        {
            AcentryLines = new HashSet<AcentryLines>();
            ClassificationBmr = new HashSet<ClassificationBmr>();
            DisposalItem = new HashSet<DisposalItem>();
            FinishProductExcipientLine = new HashSet<FinishProductExcipientLine>();
            FinishedProdOrderLine = new HashSet<FinishedProdOrderLine>();
            FmglobalLineItem = new HashSet<FmglobalLineItem>();
            FpproductLine = new HashSet<FpproductLine>();
            FpproductNavisionLine = new HashSet<FpproductNavisionLine>();
            GeneralEquivalaentNavision = new HashSet<GeneralEquivalaentNavision>();
            InterCompanyPurchaseOrderLine = new HashSet<InterCompanyPurchaseOrderLine>();
            InventoryTypeOpeningStockBalanceLine = new HashSet<InventoryTypeOpeningStockBalanceLine>();
            Ipir = new HashSet<Ipir>();
            ItemBatchInfo = new HashSet<ItemBatchInfo>();
            ItemCost = new HashSet<ItemCost>();
            ItemCostLine = new HashSet<ItemCostLine>();
            ItemSalesPrice = new HashSet<ItemSalesPrice>();
            ItemStockInfo = new HashSet<ItemStockInfo>();
            JobProgressTemplate = new HashSet<JobProgressTemplate>();
            JobProgressTemplateNavItem = new HashSet<JobProgressTemplateNavItem>();
            MedMasterLine = new HashSet<MedMasterLine>();
            NavCrossReference = new HashSet<NavCrossReference>();
            NavItemBatchNo = new HashSet<NavItemBatchNo>();
            NavItemCitemList = new HashSet<NavItemCitemList>();
            NavManufacturingProcess = new HashSet<NavManufacturingProcess>();
            NavMethodCodeLines = new HashSet<NavMethodCodeLines>();
            NavPackingMethod = new HashSet<NavPackingMethod>();
            NavProductionInformation = new HashSet<NavProductionInformation>();
            NavcustomerItems = new HashSet<NavcustomerItems>();
            NavisionSpecification = new HashSet<NavisionSpecification>();
            NavitemLinksMyItem = new HashSet<NavitemLinks>();
            NavitemLinksSgItem = new HashSet<NavitemLinks>();
            NavitemStockBalance = new HashSet<NavitemStockBalance>();
            Navlocation = new HashSet<Navlocation>();
            NavplannedProdOrder = new HashSet<NavplannedProdOrder>();
            OrderRequirement = new HashSet<OrderRequirement>();
            OrderRequirementGrouPinglineSplit = new HashSet<OrderRequirementGrouPinglineSplit>();
            OrderRequirementGroupingLine = new HashSet<OrderRequirementGroupingLine>();
            OrderRequirementLine = new HashSet<OrderRequirementLine>();
            OrderRequirementLineSplit = new HashSet<OrderRequirementLineSplit>();
            PackagingItemHistory = new HashSet<PackagingItemHistory>();
            PackagingItemHistoryLine = new HashSet<PackagingItemHistoryLine>();
            PkgapprovalInformation = new HashSet<PkgapprovalInformation>();
            ProcessMachineTime = new HashSet<ProcessMachineTime>();
            ProcessMachineTimeProductionLine = new HashSet<ProcessMachineTimeProductionLine>();
            ProductGroupingNav = new HashSet<ProductGroupingNav>();
            ProductionBatchInformationLine = new HashSet<ProductionBatchInformationLine>();
            ProductionForecast = new HashSet<ProductionForecast>();
            ProductionOrder = new HashSet<ProductionOrder>();
            ProductionOutput = new HashSet<ProductionOutput>();
            ProductionSimulation = new HashSet<ProductionSimulation>();
            ProductionSimulationGrouping = new HashSet<ProductionSimulationGrouping>();
            ProductionSimulationGroupingTicket = new HashSet<ProductionSimulationGroupingTicket>();
            PsbreportCommentDetail = new HashSet<PsbreportCommentDetail>();
            PurchaseItemSalesEntryLine = new HashSet<PurchaseItemSalesEntryLine>();
            PurchaseOrderLine = new HashSet<PurchaseOrderLine>();
            RequestAcline = new HashSet<RequestAcline>();
            SalesOrderEntry = new HashSet<SalesOrderEntry>();
            SalesOrderMasterPricingLine = new HashSet<SalesOrderMasterPricingLine>();
            SampleRequestForDoctor = new HashSet<SampleRequestForDoctor>();
            SampleRequestFormLineInventoryItem = new HashSet<SampleRequestFormLine>();
            SampleRequestFormLineItem = new HashSet<SampleRequestFormLine>();
            SimulationAddhoc = new HashSet<SimulationAddhoc>();
            SoSalesOrderLine = new HashSet<SoSalesOrderLine>();
            TempSalesPackInformationFactor = new HashSet<TempSalesPackInformationFactor>();
        }

        public long ItemId { get; set; }
        public long? CompanyId { get; set; }
        public string No { get; set; }
        public string RelatedItemNo { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string ItemType { get; set; }
        public decimal? Inventory { get; set; }
        public string InternalRef { get; set; }
        public string ItemRegistration { get; set; }
        public string ExpirationCalculation { get; set; }
        public string BatchNos { get; set; }
        public string ProductionRecipeNo { get; set; }
        public bool? Qcenabled { get; set; }
        public string SafetyLeadTime { get; set; }
        public string ProductionBomno { get; set; }
        public string RoutingNo { get; set; }
        public string BaseUnitofMeasure { get; set; }
        public decimal? UnitCost { get; set; }
        public decimal? UnitPrice { get; set; }
        public string VendorNo { get; set; }
        public string VendorItemNo { get; set; }
        public string ItemCategoryCode { get; set; }
        public string ItemTrackingCode { get; set; }
        public string Qclocation { get; set; }
        public string Company { get; set; }
        public DateTime? LastSyncDate { get; set; }
        public long? LastSyncBy { get; set; }
        public long? CategoryId { get; set; }
        public bool? Steroid { get; set; }
        public string ShelfLife { get; set; }
        public string Quota { get; set; }
        public int? PackSize { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string PackUom { get; set; }
        public long? GenericCodeId { get; set; }
        public bool? IsDifferentAcuom { get; set; }
        public decimal? PackQty { get; set; }
        public string PurchaseUom { get; set; }
        public long? ReplenishmentMethodId { get; set; }
        public string ImageUrl { get; set; }
        public bool? IsPortal { get; set; }
        public string ItemSerialNo { get; set; }
        public long? PackSizeId { get; set; }
        public long? SupplyToId { get; set; }
        public long? UomId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant CompanyNavigation { get; set; }
        public virtual GenericCodes GenericCode { get; set; }
        public virtual ApplicationUser LastSyncByNavigation { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationMasterDetail PackSizeNavigation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationMasterDetail SupplyTo { get; set; }
        public virtual ApplicationMasterDetail Uom { get; set; }
        public virtual ICollection<AcentryLines> AcentryLines { get; set; }
        public virtual ICollection<ClassificationBmr> ClassificationBmr { get; set; }
        public virtual ICollection<DisposalItem> DisposalItem { get; set; }
        public virtual ICollection<FinishProductExcipientLine> FinishProductExcipientLine { get; set; }
        public virtual ICollection<FinishedProdOrderLine> FinishedProdOrderLine { get; set; }
        public virtual ICollection<FmglobalLineItem> FmglobalLineItem { get; set; }
        public virtual ICollection<FpproductLine> FpproductLine { get; set; }
        public virtual ICollection<FpproductNavisionLine> FpproductNavisionLine { get; set; }
        public virtual ICollection<GeneralEquivalaentNavision> GeneralEquivalaentNavision { get; set; }
        public virtual ICollection<InterCompanyPurchaseOrderLine> InterCompanyPurchaseOrderLine { get; set; }
        public virtual ICollection<InventoryTypeOpeningStockBalanceLine> InventoryTypeOpeningStockBalanceLine { get; set; }
        public virtual ICollection<Ipir> Ipir { get; set; }
        public virtual ICollection<ItemBatchInfo> ItemBatchInfo { get; set; }
        public virtual ICollection<ItemCost> ItemCost { get; set; }
        public virtual ICollection<ItemCostLine> ItemCostLine { get; set; }
        public virtual ICollection<ItemSalesPrice> ItemSalesPrice { get; set; }
        public virtual ICollection<ItemStockInfo> ItemStockInfo { get; set; }
        public virtual ICollection<JobProgressTemplate> JobProgressTemplate { get; set; }
        public virtual ICollection<JobProgressTemplateNavItem> JobProgressTemplateNavItem { get; set; }
        public virtual ICollection<MedMasterLine> MedMasterLine { get; set; }
        public virtual ICollection<NavCrossReference> NavCrossReference { get; set; }
        public virtual ICollection<NavItemBatchNo> NavItemBatchNo { get; set; }
        public virtual ICollection<NavItemCitemList> NavItemCitemList { get; set; }
        public virtual ICollection<NavManufacturingProcess> NavManufacturingProcess { get; set; }
        public virtual ICollection<NavMethodCodeLines> NavMethodCodeLines { get; set; }
        public virtual ICollection<NavPackingMethod> NavPackingMethod { get; set; }
        public virtual ICollection<NavProductionInformation> NavProductionInformation { get; set; }
        public virtual ICollection<NavcustomerItems> NavcustomerItems { get; set; }
        public virtual ICollection<NavisionSpecification> NavisionSpecification { get; set; }
        public virtual ICollection<NavitemLinks> NavitemLinksMyItem { get; set; }
        public virtual ICollection<NavitemLinks> NavitemLinksSgItem { get; set; }
        public virtual ICollection<NavitemStockBalance> NavitemStockBalance { get; set; }
        public virtual ICollection<Navlocation> Navlocation { get; set; }
        public virtual ICollection<NavplannedProdOrder> NavplannedProdOrder { get; set; }
        public virtual ICollection<OrderRequirement> OrderRequirement { get; set; }
        public virtual ICollection<OrderRequirementGrouPinglineSplit> OrderRequirementGrouPinglineSplit { get; set; }
        public virtual ICollection<OrderRequirementGroupingLine> OrderRequirementGroupingLine { get; set; }
        public virtual ICollection<OrderRequirementLine> OrderRequirementLine { get; set; }
        public virtual ICollection<OrderRequirementLineSplit> OrderRequirementLineSplit { get; set; }
        public virtual ICollection<PackagingItemHistory> PackagingItemHistory { get; set; }
        public virtual ICollection<PackagingItemHistoryLine> PackagingItemHistoryLine { get; set; }
        public virtual ICollection<PkgapprovalInformation> PkgapprovalInformation { get; set; }
        public virtual ICollection<ProcessMachineTime> ProcessMachineTime { get; set; }
        public virtual ICollection<ProcessMachineTimeProductionLine> ProcessMachineTimeProductionLine { get; set; }
        public virtual ICollection<ProductGroupingNav> ProductGroupingNav { get; set; }
        public virtual ICollection<ProductionBatchInformationLine> ProductionBatchInformationLine { get; set; }
        public virtual ICollection<ProductionForecast> ProductionForecast { get; set; }
        public virtual ICollection<ProductionOrder> ProductionOrder { get; set; }
        public virtual ICollection<ProductionOutput> ProductionOutput { get; set; }
        public virtual ICollection<ProductionSimulation> ProductionSimulation { get; set; }
        public virtual ICollection<ProductionSimulationGrouping> ProductionSimulationGrouping { get; set; }
        public virtual ICollection<ProductionSimulationGroupingTicket> ProductionSimulationGroupingTicket { get; set; }
        public virtual ICollection<PsbreportCommentDetail> PsbreportCommentDetail { get; set; }
        public virtual ICollection<PurchaseItemSalesEntryLine> PurchaseItemSalesEntryLine { get; set; }
        public virtual ICollection<PurchaseOrderLine> PurchaseOrderLine { get; set; }
        public virtual ICollection<RequestAcline> RequestAcline { get; set; }
        public virtual ICollection<SalesOrderEntry> SalesOrderEntry { get; set; }
        public virtual ICollection<SalesOrderMasterPricingLine> SalesOrderMasterPricingLine { get; set; }
        public virtual ICollection<SampleRequestForDoctor> SampleRequestForDoctor { get; set; }
        public virtual ICollection<SampleRequestFormLine> SampleRequestFormLineInventoryItem { get; set; }
        public virtual ICollection<SampleRequestFormLine> SampleRequestFormLineItem { get; set; }
        public virtual ICollection<SimulationAddhoc> SimulationAddhoc { get; set; }
        public virtual ICollection<SoSalesOrderLine> SoSalesOrderLine { get; set; }
        public virtual ICollection<TempSalesPackInformationFactor> TempSalesPackInformationFactor { get; set; }
    }
}
