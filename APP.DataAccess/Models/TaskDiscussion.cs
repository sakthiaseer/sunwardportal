﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TaskDiscussion
    {
        public long DiscussionNotesId { get; set; }
        public long? TaskId { get; set; }
        public long? UserId { get; set; }
        public DateTime? DiscussionDate { get; set; }
        public string DiscussionNotes { get; set; }
        public int? StatusCodeId { get; set; }

        public virtual CodeMaster StatusCode { get; set; }
        public virtual TaskMaster Task { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
