﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class AssetAssignment
    {
        public long AssetAssignId { get; set; }
        public long? AssetId { get; set; }
        public long? AssignedTo { get; set; }
        public DateTime? AssignedOn { get; set; }
        public int? AssetStatus { get; set; }
        public long? AssetManagedBy { get; set; }
        public bool? IsLatest { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual AssetMaster Asset { get; set; }
        public virtual ApplicationUser AssetManagedByNavigation { get; set; }
        public virtual CodeMaster AssetStatusNavigation { get; set; }
        public virtual ApplicationUser AssignedToNavigation { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
