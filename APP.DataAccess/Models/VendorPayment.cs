﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class VendorPayment
    {
        public long PaymentId { get; set; }
        public string Description { get; set; }
        public string PaymentCode { get; set; }
        public long? VendorListId { get; set; }

        public virtual VendorsList VendorList { get; set; }
    }
}
