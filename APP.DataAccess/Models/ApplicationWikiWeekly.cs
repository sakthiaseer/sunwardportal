﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApplicationWikiWeekly
    {
        public long ApplicationWikiWeeklyId { get; set; }
        public long? ApplicationWikiLineId { get; set; }
        public int? WeeklyId { get; set; }
        public string CustomType { get; set; }

        public virtual ApplicationWikiLine ApplicationWikiLine { get; set; }
        public virtual CodeMaster Weekly { get; set; }
    }
}
