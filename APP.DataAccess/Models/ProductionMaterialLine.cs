﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductionMaterialLine
    {
        public ProductionMaterialLine()
        {
            ProductionMaterialLineClassification = new HashSet<ProductionMaterialLineClassification>();
        }

        public long ProductionMaterialLineId { get; set; }
        public long? ProductionMaterialId { get; set; }
        public int? SaltOrBaseId { get; set; }
        public decimal? XofGeneralItemName { get; set; }
        public bool? IsWaterMalecule { get; set; }
        public long? HydrationId { get; set; }
        public int? MicronisedOrMeshId { get; set; }
        public decimal? ParticleSize { get; set; }
        public decimal? MeshNo { get; set; }
        public bool? IsDensity { get; set; }
        public int? Density { get; set; }
        public decimal? DyeContentFrom { get; set; }
        public decimal? DyeContentTo { get; set; }
        public long? DatabaseRequireId { get; set; }
        public string SuggestNameBySystem { get; set; }
        public long? NavisionSinid { get; set; }
        public long? ItemCategorySinid { get; set; }
        public long? NavisionMalId { get; set; }
        public long? ItemCategorMalId { get; set; }
        public bool? IsChangeNavisonToPropose { get; set; }

        public virtual CodeMaster DensityNavigation { get; set; }
        public virtual CodeMaster MicronisedOrMesh { get; set; }
        public virtual ProductionMaterial ProductionMaterial { get; set; }
        public virtual CodeMaster SaltOrBase { get; set; }
        public virtual ICollection<ProductionMaterialLineClassification> ProductionMaterialLineClassification { get; set; }
    }
}
