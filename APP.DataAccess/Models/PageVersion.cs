﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PageVersion
    {
        public long PageVersionIid { get; set; }
        public long PageId { get; set; }
        public decimal VersionNo { get; set; }
        public string VersionMath { get; set; }
        public string PageContent { get; set; }
        public string VersionComment { get; set; }
        public long AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual WikiPage Page { get; set; }
    }
}
