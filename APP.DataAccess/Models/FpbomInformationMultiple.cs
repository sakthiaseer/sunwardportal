﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FpbomInformationMultiple
    {
        public long BomInformationMultipleId { get; set; }
        public long? BomInformationId { get; set; }
        public long? FinishProductExcipientId { get; set; }

        public virtual FinishProductExcipient FinishProductExcipient { get; set; }
    }
}
