﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class Acentry
    {
        public Acentry()
        {
            AcentryLines = new HashSet<AcentryLines>();
        }

        public long AcentryId { get; set; }
        public long? CompanyId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public long? CustomerId { get; set; }
        public long? DocumentId { get; set; }
        public decimal? Quantity { get; set; }
        public string Remark { get; set; }
        public string Version { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual Navcustomer Customer { get; set; }
        public virtual Documents Document { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<AcentryLines> AcentryLines { get; set; }
    }
}
