﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class WikiTranslation
    {
        public long WikiTranslationId { get; set; }
        public long? WikiPageId { get; set; }
        public long? LanguageId { get; set; }
        public long? TranslationPageId { get; set; }

        public virtual LanguageMaster Language { get; set; }
        public virtual WikiPage WikiPage { get; set; }
    }
}
