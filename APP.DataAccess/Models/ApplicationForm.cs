﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ApplicationForm
    {
        public ApplicationForm()
        {
            ApplicationFormFields = new HashSet<ApplicationFormFields>();
            ApplicationFormSearchApplicationForm = new HashSet<ApplicationFormSearch>();
            ApplicationFormSearchMainForm = new HashSet<ApplicationFormSearch>();
            ApplicationFormSearchParent = new HashSet<ApplicationFormSearch>();
            ApplicationFormSubApplicationForm = new HashSet<ApplicationFormSub>();
            ApplicationFormSubForm = new HashSet<ApplicationFormSub>();
            ItemClassificationAccess = new HashSet<ItemClassificationAccess>();
            ItemClassificationForms = new HashSet<ItemClassificationForms>();
            ItemClassificationMaster = new HashSet<ItemClassificationMaster>();
        }

        public long FormId { get; set; }
        public string FormType { get; set; }
        public string Name { get; set; }
        public string PathUrl { get; set; }
        public string ModuleName { get; set; }
        public string TableName { get; set; }
        public long? ProfileId { get; set; }
        public int? ClassificationTypeId { get; set; }
        public string ScreenId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? PersonInchargeId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CodeMaster ClassificationType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationUser PersonIncharge { get; set; }
        public virtual DocumentProfileNoSeries Profile { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ApplicationFormFields> ApplicationFormFields { get; set; }
        public virtual ICollection<ApplicationFormSearch> ApplicationFormSearchApplicationForm { get; set; }
        public virtual ICollection<ApplicationFormSearch> ApplicationFormSearchMainForm { get; set; }
        public virtual ICollection<ApplicationFormSearch> ApplicationFormSearchParent { get; set; }
        public virtual ICollection<ApplicationFormSub> ApplicationFormSubApplicationForm { get; set; }
        public virtual ICollection<ApplicationFormSub> ApplicationFormSubForm { get; set; }
        public virtual ICollection<ItemClassificationAccess> ItemClassificationAccess { get; set; }
        public virtual ICollection<ItemClassificationForms> ItemClassificationForms { get; set; }
        public virtual ICollection<ItemClassificationMaster> ItemClassificationMaster { get; set; }
    }
}
