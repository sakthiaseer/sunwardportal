﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class InstructionType
    {
        public long InstructionTypeId { get; set; }
        public string DocumentName { get; set; }
        public int? TypeId { get; set; }
        public string No { get; set; }
        public string InstructionNo { get; set; }
        public string VersionNo { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string Link { get; set; }
        public long? DeviceCatalogMasterId { get; set; }
        public long? CalibrationServiceInformationId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual CalibrationServiceInfo CalibrationServiceInformation { get; set; }
        public virtual DeviceCatalogMaster DeviceCatalogMaster { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
