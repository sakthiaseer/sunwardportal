﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class PurchaseInfo
    {
        public long PurchaseInformationId { get; set; }
        public string Purpose { get; set; }
        public string DepartmentInCharge { get; set; }
        public string DateOfPurchase { get; set; }
        public string Vendor { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public string InvoiceNo { get; set; }
        public decimal? Price { get; set; }
    }
}
