﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DynamicFormWorkFlowSectionForm
    {
        public long DynamicFormWorkFlowSectionFormId { get; set; }
        public long? DynamicFormWorkFlowFormId { get; set; }
        public long? DynamicFormSectionId { get; set; }

        public virtual DynamicFormSection DynamicFormSection { get; set; }
        public virtual DynamicFormWorkFlowForm DynamicFormWorkFlowForm { get; set; }
    }
}
