﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavbaseUnit
    {
        public NavbaseUnit()
        {
            ProductionTicket = new HashSet<ProductionTicket>();
        }

        public long NavBaseUnitId { get; set; }
        public long? CompanyId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<ProductionTicket> ProductionTicket { get; set; }
    }
}
