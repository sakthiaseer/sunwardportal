﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class CommonPackingInformationLine
    {
        public CommonPackingInformationLine()
        {
            CommonPackingItemListLine = new HashSet<CommonPackingItemListLine>();
        }

        public long PackingInformationLineId { get; set; }
        public long? PackingInformationId { get; set; }
        public long? PackagingItemCategoryId { get; set; }
        public long? PackagingItemNameId { get; set; }
        public long? UomId { get; set; }
        public decimal? NoOfUnits { get; set; }
        public string Link { get; set; }
        public long? PackagingMethodId { get; set; }
        public string NameOfPackingMethod { get; set; }
        public decimal? UnitsPackingContent { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CommonPackagingItemHeader PackagingItemName { get; set; }
        public virtual DocumentProfileNoSeries PackagingMethod { get; set; }
        public virtual CommonPackingInformation PackingInformation { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<CommonPackingItemListLine> CommonPackingItemListLine { get; set; }
    }
}
