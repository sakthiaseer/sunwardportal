﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class RawMatItemList
    {
        public long Id { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public decimal? Inventory { get; set; }
        public string InternalRef { get; set; }
        public string ItemRegistration { get; set; }
        public string BatchNos { get; set; }
        public string PsoitemNo { get; set; }
        public string ProductionRecipeNo { get; set; }
        public string SafetyLeadTime { get; set; }
        public string ProductionBomno { get; set; }
        public string RoutingNo { get; set; }
        public string BaseUnitofMeasure { get; set; }
        public decimal? StandardCost { get; set; }
        public decimal? UnitCost { get; set; }
        public decimal? LastDirectCost { get; set; }
        public string ItemCategoryCode { get; set; }
        public string ProductGroupCode { get; set; }
        public long? CompanyId { get; set; }
        public string Type { get; set; }

        public virtual Plant Company { get; set; }
    }
}
