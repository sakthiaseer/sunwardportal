﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class SobyCustomersManner
    {
        public long SobyCustomersMannerId { get; set; }
        public long? SocustomersIssueId { get; set; }
        public int? MannerId { get; set; }
        public long? MannersId { get; set; }

        public virtual CodeMaster Manner { get; set; }
        public virtual SocustomersIssue SocustomersIssue { get; set; }
    }
}
