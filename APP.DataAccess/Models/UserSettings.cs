﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class UserSettings
    {
        public long UserSettingsId { get; set; }
        public long? UserId { get; set; }
        public int? PageSize { get; set; }
        public string SearchQuery { get; set; }
        public string UserTheme { get; set; }
        public string TitleCaption { get; set; }
        public bool? IsEnableFolderPermission { get; set; }

        public virtual ApplicationUser User { get; set; }
    }
}
