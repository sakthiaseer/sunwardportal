﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class TemplateCaseFormNotes
    {
        public long TemplateCaseFormNotesId { get; set; }
        public long? TemplateTestCaseFormId { get; set; }
        public string Notes { get; set; }
        public Guid? SessionId { get; set; }
        public string Link { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual TemplateTestCaseForm TemplateTestCaseForm { get; set; }
    }
}
