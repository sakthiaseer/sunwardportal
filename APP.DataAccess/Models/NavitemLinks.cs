﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavitemLinks
    {
        public long ItemLinkId { get; set; }
        public long? MyItemId { get; set; }
        public long? SgItemId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual Navitems MyItem { get; set; }
        public virtual Navitems SgItem { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
    }
}
