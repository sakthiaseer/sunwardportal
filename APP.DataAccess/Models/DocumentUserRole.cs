﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class DocumentUserRole
    {
        public long DocumentUserRoleId { get; set; }
        public long? UserId { get; set; }
        public long? RoleId { get; set; }
        public long? UserGroupId { get; set; }
        public long? DocumentId { get; set; }
        public long? FolderId { get; set; }
        public bool? IsFolderLevel { get; set; }
        public long? ReferencedGroupId { get; set; }
        public long? FileProfileTypeId { get; set; }
        public long? LevelId { get; set; }

        public virtual Documents Document { get; set; }
        public virtual FileProfileType FileProfileType { get; set; }
        public virtual Folders Folder { get; set; }
        public virtual LevelMaster Level { get; set; }
        public virtual UserGroup ReferencedGroup { get; set; }
        public virtual DocumentRole Role { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual UserGroup UserGroup { get; set; }
    }
}
