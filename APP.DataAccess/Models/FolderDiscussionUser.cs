﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FolderDiscussionUser
    {
        public long FolderDiscussionNotesUserId { get; set; }
        public long FolderDiscussionNotesId { get; set; }
        public long FolderId { get; set; }
        public long UserId { get; set; }
        public bool IsRead { get; set; }

        public virtual FolderDiscussion FolderDiscussionNotes { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
