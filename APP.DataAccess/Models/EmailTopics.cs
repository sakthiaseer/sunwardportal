﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class EmailTopics
    {
        public EmailTopics()
        {
            EmailTopicCc = new HashSet<EmailTopicCc>();
            EmailTopicTo = new HashSet<EmailTopicTo>();
        }

        public long Id { get; set; }
        public string TicketNo { get; set; }
        public string TopicName { get; set; }
        public long? TypeId { get; set; }
        public long? CategoryId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? DueDate { get; set; }
        public long TopicFrom { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public string Description { get; set; }
        public byte[] FileData { get; set; }
        public string Type { get; set; }
        public int? SeqNo { get; set; }
        public string SubjectName { get; set; }
        public int StatusCodeId { get; set; }
        public int AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public int? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? SessionId { get; set; }
        public string To { get; set; }
        public string Cc { get; set; }
        public string Participants { get; set; }
        public string Label { get; set; }
        public string TypeName { get; set; }
        public string Follow { get; set; }
        public long? OnBehalf { get; set; }
        public bool? Urgent { get; set; }
        public bool? OverDue { get; set; }
        public int? OnDraft { get; set; }
        public bool? IsAllowParticipants { get; set; }
        public bool? IsArchive { get; set; }
        public string PinStatus { get; set; }
        public bool? NotifyUser { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public string UserType { get; set; }
        public int? NoOfDays { get; set; }
        public DateTime? ExpiryDueDate { get; set; }
        public bool? TagLock { get; set; }

        public virtual ICollection<EmailTopicCc> EmailTopicCc { get; set; }
        public virtual ICollection<EmailTopicTo> EmailTopicTo { get; set; }
    }
}
