﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NavmethodCodeBatch
    {
        public NavmethodCodeBatch()
        {
            ProdRecipeCycle = new HashSet<ProdRecipeCycle>();
            ProductionOrder = new HashSet<ProductionOrder>();
        }

        public long MethodCodeBatchId { get; set; }
        public long NavMethodCodeId { get; set; }
        public long? BatchSize { get; set; }
        public long? DefaultBatchSize { get; set; }
        public decimal? BatchUnitSize { get; set; }

        public virtual NavMethodCode NavMethodCode { get; set; }
        public virtual ICollection<ProdRecipeCycle> ProdRecipeCycle { get; set; }
        public virtual ICollection<ProductionOrder> ProductionOrder { get; set; }
    }
}
