﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class MasterBlanketOrder
    {
        public MasterBlanketOrder()
        {
            MasterBlanketOrderLine = new HashSet<MasterBlanketOrderLine>();
        }

        public long MasterBlanketOrderId { get; set; }
        public long? CompanyId { get; set; }
        public DateTime? FromPeriod { get; set; }
        public DateTime? ToPeriod { get; set; }
        public long? CustomerId { get; set; }
        public bool? IsRequireVersionInformation { get; set; }
        public int StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? VersionSessionId { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual CompanyListing Customer { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<MasterBlanketOrderLine> MasterBlanketOrderLine { get; set; }
    }
}
