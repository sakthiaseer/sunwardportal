﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class FolderStorage
    {
        public FolderStorage()
        {
            FolderStorageUser = new HashSet<FolderStorageUser>();
        }

        public long FolderStorageId { get; set; }
        public long? FolderId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid? SessionId { get; set; }
        public int? FolderTypeId { get; set; }
        public string FolderLocation { get; set; }
        public string StorageLimit { get; set; }
        public long? UserId { get; set; }
        public long? UserGroupId { get; set; }
        public long? UserGroupUserId { get; set; }
        public long? RoleId { get; set; }
        public long? UserRoleId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Folders Folder { get; set; }
        public virtual CodeMaster FolderType { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual ApplicationRole Role { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual UserGroup UserGroup { get; set; }
        public virtual UserGroupUser UserGroupUser { get; set; }
        public virtual ICollection<FolderStorageUser> FolderStorageUser { get; set; }
    }
}
