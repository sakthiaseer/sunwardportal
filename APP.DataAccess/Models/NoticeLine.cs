﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class NoticeLine
    {
        public NoticeLine()
        {
            NoticeLineWeekly = new HashSet<NoticeLineWeekly>();
        }

        public long NoticeLineId { get; set; }
        public long? NoticeId { get; set; }
        public string Link { get; set; }
        public string Wilink { get; set; }
        public int? ModuleId { get; set; }
        public int? FrequencyId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? MonthlyDay { get; set; }
        public bool? DaysOfWeek { get; set; }
        public long? CompanyId { get; set; }
        public int? StatusCodeId { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ApplicationUser AddedByUser { get; set; }
        public virtual Plant Company { get; set; }
        public virtual CodeMaster Frequency { get; set; }
        public virtual ApplicationUser ModifiedByUser { get; set; }
        public virtual CodeMaster Module { get; set; }
        public virtual Notice Notice { get; set; }
        public virtual CodeMaster StatusCode { get; set; }
        public virtual ICollection<NoticeLineWeekly> NoticeLineWeekly { get; set; }
    }
}
