﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ProductActivityCaseCategoryMultiple
    {
        public long ProductActivityCaseCategoryMultipleId { get; set; }
        public long? CategoryActionId { get; set; }
        public long? ProductActivityCaseId { get; set; }

        public virtual ApplicationMasterChild CategoryAction { get; set; }
        public virtual ProductActivityCase ProductActivityCase { get; set; }
    }
}
