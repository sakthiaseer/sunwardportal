﻿using System;
using System.Collections.Generic;

namespace APP.DataAccess.Models
{
    public partial class ViewGetFileProfileTypeDocument
    {
        public string Profile { get; set; }
        public string FileProfileTypeName { get; set; }
        public string FileProfileDescription { get; set; }
        public long ProfileId { get; set; }
        public long FileProfileTypeId { get; set; }
        public int StatusCodeId { get; set; }
        public int? IsExpiryDate { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? AddedDate { get; set; }
        public long? ModifiedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsDocumentAccess { get; set; }
        public string StatusCode { get; set; }
        public string AddedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string DocumentName { get; set; }
        public long? DocFileSize { get; set; }
        public long? DocumentId { get; set; }
        public string DocumentDescription { get; set; }
        public string ProfileNo { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string ShelfLifeDurationStatus { get; set; }
        public DateTime? DocAddedDate { get; set; }
        public string ContentType { get; set; }
        public long? FolderId { get; set; }
        public string DocAddedBy { get; set; }
        public Guid? DocSessionId { get; set; }
        public int? CloseDocumentId { get; set; }
        public long? DocumentParentId { get; set; }
        public bool? IsMobileUpload { get; set; }
        public string TableName { get; set; }
        public bool? IsCompressed { get; set; }
        public bool? IsLatest { get; set; }
        public bool? IsLocked { get; set; }
        public string FilePath { get; set; }
        public int? ArchiveStatusId { get; set; }
        public long? DynamicFormId { get; set; }
    }
}
