/**
 * plugin.js
 *
 * Copyright, Moxiecode Systems AB
 * Released under LGPL License.
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

/*global tinymce:true */

tinymce.PluginManager.add('advlist', function(editor) {
	var olMenuItems, ulMenuItems, lastStyles = {};
	function setStart() {
		return function() {
			var win, v, list, data = {}, dom = editor.dom, sel = editor.selection;

			// Check for existing list element
			list = dom.getParent(sel.getNode(), 'ol');
			if (list) {
				v = editor.dom.getAttrib(list, "start", 1);
			} else {
				v = 1;
			}

			data = {start: v,type:1};
			/* var myListItems =[];
			myListItems = buildMenuItemsList('OL', editor.getParam(
				"advlist_number_styles",
				"default,lower-alpha,lower-greek,lower-roman,upper-alpha,upper-roman"
			)); */
			win = editor.windowManager.open({
				title: "List numbering",
				data: data,
				body: [
					{
						label:'Type From A-Z,a-Z,1 to ∞',
						type: 'label',
						align: 'center',
					},
					/* {
						label: 'Type From',
						name: 'type',
						type: 'listbox',
						values: myListItems,
						fixedWidth: true,
					}, */
					{
						label: 'Start From',
						name: 'start',
						type: 'textbox',
						
					}
				],
				buttons: [
					{
						text: "Ok",
						subtype: "primary",
						onclick: function() {
							var list = dom.getParent(editor.selection.getNode(), 'ol');
							if (list) {
                                data = tinymce.extend(data, win.toJSON());
								//editor.dom.setAttrib(list, "start", data.start);
								//var style="list-style-type: "+data.type+";"
								//editor.dom.setAttrib(list, "style",style);
								var caps=genCharArray('A','Z');
								var small=genCharArray('a','z');
								if(caps.indexOf(data.start)>0)
								{
									data.type="A";
									data.start=parseInt(caps.indexOf(data.start)) + 1;
								}
								if(small.indexOf(data.start)>0)
								{
									data.type="a";
									data.start=parseInt(small.indexOf(data.start)) + 1;
								}
								editor.dom.setAttrib(list, "start", data.start);
								editor.dom.setAttrib(list, "type",data.type);
							}
							win.close();
						}
					},
					{
						text: "Cancel",
						onclick: function() {
							win.close();
						}
					}
				],
				//width: 320,
				//height: 100,
			});
		};
	}
	function genCharArray(charA, charZ) {
		var a = [], i = charA.charCodeAt(0), j = charZ.charCodeAt(0);
		for (; i <= j; ++i) {
			a.push(String.fromCharCode(i));
		}
		return a;
	}
	/* function buildMenuItemsList(listName,styleValues) {
		var items = [];

		tinymce.each(styleValues.split(/[ ,]/), function(styleValue) {
			items.push({
				text: styleValue.replace(/\-/g, ' ').replace(/\b\w/g, function(chr) {
					return chr.toUpperCase();
				}),
				value: styleValue == 'default' ? '' : styleValue
			});
		});

		return items;
	} */
	function buildMenuItems(listName, styleValues) {
		var items = [];

		tinymce.each(styleValues.split(/[ ,]/), function(styleValue) {
			items.push({
				text: styleValue.replace(/\-/g, ' ').replace(/\b\w/g, function(chr) {
					return chr.toUpperCase();
				}),
				data: styleValue == 'default' ? '' : styleValue
			});
		});

		return items;
	}
 
	olMenuItems = buildMenuItems('OL', editor.getParam(
		"advlist_number_styles",
		"default,lower-alpha,lower-greek,lower-roman,upper-alpha,upper-roman"
	)); 
	olMenuItems.push({
		text: 'Start From',
		onclick: setStart()
	});

	ulMenuItems = buildMenuItems('UL', editor.getParam("advlist_bullet_styles", "default,circle,disc,square"));

	function applyListFormat(listName, styleValue) {
		editor.undoManager.transact(function() {
			var list, dom = editor.dom, sel = editor.selection;

			// Check for existing list element
			list = dom.getParent(sel.getNode(), 'ol,ul');

			// Switch/add list type if needed
			if (!list || list.nodeName != listName || styleValue === false) {
				editor.execCommand(listName == 'UL' ? 'InsertUnorderedList' : 'InsertOrderedList');
			}

			// Set style
			styleValue = styleValue === false ? lastStyles[listName] : styleValue;
			lastStyles[listName] = styleValue;

			list = dom.getParent(sel.getNode(), 'ol,ul');
			if (list) {
				dom.setStyle(list, 'listStyleType', styleValue ? styleValue : null);
				list.removeAttribute('data-mce-style');
			}

			editor.focus();
		});
	}

	function updateSelection(e) {
		var listStyleType = editor.dom.getStyle(editor.dom.getParent(editor.selection.getNode(), 'ol,ul'), 'listStyleType') || '';

		e.control.items().each(function(ctrl) {
			ctrl.active(ctrl.settings.data === listStyleType);
		});
	}

	editor.addButton('numlist', {
		type: 'splitbutton',
		tooltip: 'Numbered list',
		menu: olMenuItems,
		onshow: updateSelection,
		onselect: function(e) {
			applyListFormat('OL', e.control.settings.data);
		},
		onclick: function() {
			applyListFormat('OL', false);
		}
	});

	editor.addButton('bullist', {
		type: 'splitbutton',
		tooltip: 'Bullet list',
		menu: ulMenuItems,
		onshow: updateSelection,
		onselect: function(e) {
			applyListFormat('UL', e.control.settings.data);
		},
		onclick: function() {
			applyListFormat('UL', false);
		}
	});
});