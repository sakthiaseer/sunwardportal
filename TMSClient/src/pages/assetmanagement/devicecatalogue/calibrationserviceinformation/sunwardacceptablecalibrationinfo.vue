<template>
  <div>
    <v-toolbar flat color="white">
      <v-text-field v-model="search" prepend-icon="search" label="Search" clearable single-line hide-details></v-text-field>
      <v-spacer></v-spacer>
      <download-excel
        class="btn btn-default"
        :data="gridItems"
        :fields="json_fields"
        worksheet="My Worksheet"
        type="csv"
        name="swcalibrationInfo.xls"
      >
        <v-btn icon>
          <v-icon>cloud_download</v-icon>
        </v-btn>
      </download-excel>  
      <v-dialog v-model="dialog" max-width="600px">
        <v-btn slot="activator" icon @click.native="openDialog">
          <v-icon>add_circle_outline</v-icon>
        </v-btn>
        <v-card>
          <v-card-title>
            <span class="headline">{{ formTitle }}</span>
          </v-card-title>

          <v-card-text>
            <v-form v-model="editedItem.valid" ref="form" lazy-validation>
              <v-container grid-list-md>
                <v-layout wrap>
                  <v-flex xs12 sm12>
                    <v-text-field
                      v-model="editedItem.parameter"
                      label="Parameter"
                      :rules="editedItem.nameRules"
                      required
                      prepend-icon="info"
                    ></v-text-field>
                  </v-flex>
                  <v-flex xs12 sm6>
                    <v-text-field
                      v-model="editedItem.standardReference"
                      label="StandardReference"
                     
                      prepend-icon="select_all"
                    ></v-text-field>
                  </v-flex>
                  <v-flex xs12 sm6>
                    <v-text-field
                      v-model="editedItem.max"
                      label="Max"
                     
                      prepend-icon="select_all"
                    ></v-text-field>
                  </v-flex>
                  <v-flex xs12 sm6>
                    <v-text-field
                      v-model="editedItem.min"
                      label="Min"
                      
                      prepend-icon="select_all"
                    ></v-text-field>
                  </v-flex>
                  <v-flex xs12 sm6>
                    <v-text-field
                      v-model="editedItem.uom"
                      label="Unit Of Measure"
                     
                      prepend-icon="select_all"
                    ></v-text-field>
                  </v-flex>
                  <v-flex xs12 sm12 md12>
                    <v-text-field
                      v-model="editedItem.remarks"
                      label="Remarks"
                      
                      prepend-icon="info"
                    ></v-text-field>
                  </v-flex>
                </v-layout>
              </v-container>
            </v-form>
          </v-card-text>
            <v-card-actions>
           <v-btn
                outline
                color="primary"
                slot="activator"
                @click.native="saveacceptable"
              >
                SAVE
              </v-btn> 
              <v-btn
                outline
                color="primary"
                slot="activator"
                @click.native="close"
              >
                Close
              </v-btn> 
          </v-card-actions>   
          <delete-confirmation-dialog
            ref="formdeleteConfirmationDialog"
            heading="Are You Sure You Want To Delete?"
            message="Are You Sure You Want To Delete This Item Permanently?"
            @onConfirm="formDeleteItem"
          ></delete-confirmation-dialog>
        </v-card>
      </v-dialog>
    </v-toolbar>
    <app-section-loader :status="loader"></app-section-loader>
    <v-data-table
      :headers="headers"
      :items="gridItems"
      :search="search"
      class="elevation-1"
      default-sort="acceptableCalibrationInformationID:desc"
      :rows-per-page-items="rowsPerPageItems"
      :pagination.sync="pagination"
    >
      <template slot="items" slot-scope="props">
        <td class="text-xs-left">{{ props.item.parameter }}</td>
        <td class="text-xs-left">{{ props.item.standardReference }}</td>
        <td class="text-xs-left">{{ props.item.max }}</td>
        <td class="text-xs-left">{{ props.item.min }}</td>
        <td class="text-xs-left">{{ props.item.uom }}</td>
        <td class="text-xs-left">{{ props.item.remarks }}</td>

        <td class="justify-center layout px-0">
          <v-icon small class="mr-2" @click="editItem(props.item)">edit</v-icon>
          <v-icon small @click="onDeleteItem(props.item)">delete</v-icon>
        </td>
      </template>
      <template slot="no-data">
        <v-btn
          color="primary"
          outline
          @click="initialize"
        >Record not exist.Refresh again in while time.</v-btn>
      </template>
    </v-data-table>
    <v-snackbar :top="y === 'top'" :timeout="timeout" v-model="snackbar">{{ snackbarMessage }}</v-snackbar>
    <delete-confirmation-dialog
      ref="deleteConfirmationDialog"
      heading="Are You Sure You Want To Delete?"
      message="Are You Sure You Want To Delete This Item Permanently?"
      @onConfirm="deleteItem"
    ></delete-confirmation-dialog>
  </div>
</template>
<script>
import {
  getAcceptableCalibrationInformations,
  getAcceptableCalibrationInformation,
  createAcceptableCalibrationInformation,
  updateAcceptableCalibrationInformation,
  deleteAcceptableCalibrationInformation,  
} from "@/api/accepTableCalibration";
import GlobalMessage from "@/constant";
export default {
  data: () => ({
    dialog: false,
    loader: false,
     search: "",    
    statusOptions: [
      { text: "Active", value: 1 },
      { text: "InActive", value: 2 }
    ],
    snackbar: false,
    snackbarMessage: "",
    timeout: 2000,
    y: "top",
    headers: [
      { text: "Parameter", value: "parameter", align: "left" },
      { text: "Standard Reference", value: "standardReference", align: "left" },
      { text: "Max", value: "max", align: "left" },
      { text: "Min", value: "min", align: "left" },
      { text: "Unif Of Measure", value: "uom", align: "left" },
      { text: "Remarks", value: "remarks", align: "left" },
      { text: "Actions", value: "name", sortable: false, align: "left" }
    ],
    json_fields: {
      'Parameter': 'parameter',
      'Standard Reference': 'standardReference',
      'Max': 'max',
      'Min': 'min',
      'Unif Of Measure': 'uom',
      'Remarks': 'remarks'
    },
    gridItems: [],
    userItems: [],
    editedIndex: -1,
    selectedItem: null,
    rowsPerPageItems: [10, 20, 30, 40, 50, 100],
    pagination: {
      rowsPerPage: 10
    },
    editedItem: {
      valid: false,
      acceptableCalibrationInformationID: -1,
      parameter: "",
      standardReference: "",
      max: "",
      min: "",
      unitOfMeasure: "",
      remarks: "",
      calibrationServiceInformationID: null,
      nameRules: [v => !!v || "Name is required"],   
     
      
    },
    defaultItem: {
       valid: false,
      acceptableCalibrationInformationID: -1,
      parameter: "",
      standardReference: "",
       max: "",
      min: "",
      unitOfMeasure: "",
      remarks: "",
      calibrationServiceInformationID: null,
      nameRules: [v => !!v || "Name is required"],
    },   
    
  }),

  computed: {
    formTitle() {
      return this.editedIndex === -1 ? "Add AcceptableCalibration" : "Edit AcceptableCalibration";
    }
  },
 
  created() {
    this.initialize();
  },

  methods: {
    initialize() {},
    editItem(item) {
      this.editedIndex = this.gridItems.indexOf(item);
      this.editedItem = Object.assign({}, item);

      this.dialog = true;
    },
    onDeleteItem(item) {
      this.$refs.deleteConfirmationDialog.openDialog();
      this.selectedItem = item;
    },
    deleteItem() {
      this.$refs.deleteConfirmationDialog.close();
      this.loader = true;
      let index = this.gridItems.indexOf(this.selectedItem);
     
      if(this.selectedItem.acceptableCalibrationInformationID <= 0){
      this.gridItems.splice(index, 1);
       this.loader = false;
      
      }
      else{
      this.loader = false; 
      deleteAcceptableCalibrationInformation(this.selectedItem)
        .then(() => {
          this.loader = false;
          this.selectedItem = null;
          this.gridItems.splice(index, 1);
          window.getApp.$emit('APP_API_SUCCESS', GlobalMessage.DELETE);
        })
        .catch(error => {
          this.loader = false;
          window.getApp.$emit('APP_API_ERROR', error);
        });
      }
    },  
    saveAndClose() {
      this.saveacceptable();
      this.close();
    },
   
      openDialog() {
      this.add();
      this.dialog = true;
    },
    add() {
      this.editedItem.valid = false;
      this.editedItem.acceptableCalibrationInformationID = -1;  
      this.editedItem.parameter = "";
      this.editedItem.standardReference = "",
       this.editedItem.max = "",
      this.editedItem.min = "",
      this.editedItem.remarks = "",
      this.editedItem.calibrationServiceInformationID = null,   
      this.editedItem.nameRules = [v => !!v || "Name is required"];  
     
      this.editedIndex = -1;
    },
    deleterow() {
      this.$refs.formdeleteConfirmationDialog.openDialog();
    },
    formDeleteItem() {
      this.$refs.formdeleteConfirmationDialog.close();
      // this.loader = true;
      let index = this.editedIndex;
      console.log(this.editedItem);
      deleteAcceptableCalibrationInformation(this.editedItem)
        .then(() => {
          //this.loader = false;
          //this.editedItem = null;
          this.add();
          this.gridItems.splice(index, 1);
          window.getApp.$emit("APP_API_SUCCESS", GlobalMessage.DELETE);
        })
        .catch(error => {
          //this.loader = false;
          window.getApp.$emit("APP_API_ERROR", error);
        });
    },
    close() {
      this.dialog = false;
      this.initialize();
      setTimeout(() => {
        this.editedItem = Object.assign({}, this.defaultItem);
        this.editedIndex = -1;
      }, 300);
    },
     saveacceptable(){
       if (this.$refs.form.validate()) {
       if (this.editedIndex > -1) {
         
        Object.assign(this.gridItems[this.editedIndex], this.editedItem);
          setTimeout(() => {
        this.editedItem = Object.assign({}, this.defaultItem);
        this.add();
      }, 400);
      } else {
        
        this.gridItems.push(this.editedItem);
          setTimeout(() => {
        this.editedItem = Object.assign({}, this.defaultItem);
        this.add();
      }, 400);
      }     
      
       }
    },
  }
};
</script>