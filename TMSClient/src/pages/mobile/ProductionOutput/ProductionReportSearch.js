export default {
    action:"",
    descending: false,
    searchString: "",
    userID: -1,
    pageCount: 1,
    pageSize: 10,
    methodName: "",
    sortCol: "AddedDate",
    sortBy: "DESC",
    masterTypeID: -1,
    classificationTypeId: null,
    classificationId: null,
    isHeader: false,
    profileReferenceNo: "",
    primaryId: null,
    primaryColumn: "",
    folderId:null,
    filterFields: [
    ],
    fileProfileTypeId: null,
    contentType: null,
    fromMonth: null,
    toMonth: null,
    taskSearch: {
      title: "",
      assignTypeIds: -1,
      statusIds: -1,
      workTypeIds: -1,
      requestTypeIds: -1,
      projectIds: -1,
      tagIds: [],
      personaltagIds: [],
      userId: -1,
      taskStatusIds: [],
      taskAssigneeIds: [],
      taskType: "",
      barType: "",
      userIds: [],
      unreadSubject: false,
      addedDate: null,
      createdToDate: null,
      isBefore: false,
      startDate: null,
          endDate: null,
          locationId: [],
          companyId: [],
          isSWStaff: null,
          temperature: null,
          allLocation: false,
          allVisitorCompany: false,
          tesKitResult:null,
          isDuplicate:null,
    },
  };
  
  
  // filterFields: [
  //     "RoleName",
  //     "RoleDescription",
  //     "AddedByUser.UserName",
  //     "StatusCode.CodeValue",
  //   ],