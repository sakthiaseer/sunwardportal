import DeleteConfirmationDialog from "@/components/DeleteConfirmationDialog/DeleteConfirmationDialog";
import ConfirmationDialog from "@/components/ConfirmationDialog/ConfirmationDialog";
import AppSectionLoader from "@/Components/AppSectionLoader/AppSectionLoader";
import VuePerfectScrollbar from 'vue-perfect-scrollbar';

import {
    RotateSquare2
} from "vue-loading-spinner";
const GlobalComponents = {
    install(Vue) {
        Vue.component('deleteConfirmationDialog', DeleteConfirmationDialog);
        Vue.component('confirmationDialog',ConfirmationDialog);
        Vue.component('appSectionLoader', AppSectionLoader);
        Vue.component('rotateSquare2', RotateSquare2);
        Vue.component('vuePerfectScrollbar', VuePerfectScrollbar);
    }
};
export default GlobalComponents;