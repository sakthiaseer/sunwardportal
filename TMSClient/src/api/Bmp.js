import myApi from '@/util/api'

export function getIcbmp () {

  return myApi.getAll('Icbmp/GetIcbmp')
}
export function createIcbmp (data) {
  return myApi.create(data, "Icbmp/InsertIcbmp")
}
export function updateIcbmp (data) {

  return myApi.update(data.icBmpId, data, "Icbmp/UpdateIcbmp")
}
export function deleteIcbmp (data) {

  return myApi.delete(data.icBmpId, 'Icbmp/DeleteIcbmp')
}