import myApi from '@/util/api'

export function getCalibrationTypes() {

    return myApi.getAll('CalibrationType/GetCalibrationTypes')
}
export function getCalibrationType(data) {

    return myApi.getItem(data, "CalibrationType/GetData")
}

export function createCalibrationType(data) {
    return myApi.create(data, "CalibrationType/InsertCalibrationType")
}

export function updateCalibrationType(data) {

    return myApi.update(data.calibrationTypeID, data, "CalibrationType/UpdateCalibrationType")
}
export function deleteCalibrationType(data) {

    return myApi.delete(data.calibrationTypeID, 'CalibrationType/DeleteCalibrationType')
}
export function getApplicationUsers() {

    return myApi.getAll('ApplicationUser/GetApplicationUsers')
}