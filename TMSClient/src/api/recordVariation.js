import myApi from '@/util/api'

export function getRecordVariation() {

    return myApi.getAll('RecordVariation/GetRecordVariation')
}
export function getRecordVariationDetail() {

  return myApi.getAll('RecordVariation/GetRecordVariationDetail')
}
export function getRecordVariationDetailSearch(data) {

  return myApi.getItem(data,'RecordVariation/GetRecordVariationDetailSearch')
}
export function getDataRecordVariation(data) {

  return myApi.getItem(data, "RecordVariation/GetData")
}
export function getRegisterCountry(id) {

    return myApi.getByID(id,'RecordVariation/GetRegisterCountry')
}
export function getFinishProduct(id) {

    return myApi.getByID(id,'RecordVariation/GetFinishProduct')
}
export function createRecordVariation(data) {
    return myApi.create(data, "RecordVariation/InsertRecordVariation")
  }

  export function updateRecordVariation(data) {

    return myApi.update(data.recordVariationId, data, "RecordVariation/UpdateRecordVariation")
  }
  export function deleteRecordVariation(data) {
  
    return myApi.delete(data.recordVariationId, 'RecordVariation/DeleteRecordVariation')
  }
  

export function getRecordVariationLine (recordVariationId) {
    return myApi.getAll('RecordVariation/GetRecordVariationLine/?id='+recordVariationId)
  }
  export function getRegistrationVariationCountry(id) {

    return myApi.getByID(id,'RecordVariation/GetRegistrationVariationCountry')
}
export function createRecordVariationLine(data) {
    return myApi.create(data, "RecordVariation/InsertRecordVariationLine")
  }
  export function updateRecordVariationLine(data) {

    return myApi.update(data.recordVariationLineId, data, "RecordVariation/UpdateRecordVariationLine")
  }
  export function deleteRecordVariationLine(data) {

    return myApi.delete(data.recordVariationLineId, 'RecordVariation/DeleteRecordVariationLine')
  }

  export function createRecordVariationDocumentLink(data) {
    return myApi.create(data, "RecordVariation/InsertRecordVariationDocumentLink")
  }
  export function deleteRecordDocumentLink(data) {

    return myApi.delete(data.recordVariationDocumentLinkId, 'RecordVariation/DeleteRecordDocumentLink')
  }
  export function getRecordVariationDocument(id) {

    return myApi.getByID(id,'RecordVariation/GetRecordVariationDocument')
}
export function getRecordVariationLineDocument(id) {

  return myApi.getByID(id,'RecordVariation/GetRecordVariationLineDocument')
}
export function getRecordVariationLineItems () {
  return myApi.getAll('RecordVariation/GetRecordVariationLineItems')
}