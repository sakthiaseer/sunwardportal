import myApi from '@/util/api'

export function getTagMaster() {

    return myApi.getAll('TagMaster/GetTagMaster')
}
export function getTagMasters(data) {

    return myApi.getItem(data, "TagMaster/GetData")
}
export function createTagMaster(data) {
    return myApi.create(data, "TagMaster/InsertTagMaster")
}

export function updateTagMaster(data) {

    return myApi.update(data.tagMasterID, data, "TagMaster/UpdateTagMaster")
}
export function updateTaskTag(data) {

    return myApi.update(data.taskTagId, data, "TagMaster/UpdateTaskTag")
}
export function deleteTagMaster(data) {

    return myApi.delete(data.tagMasterID, 'TagMaster/DeleteTagMaster')
}
export function getTasksByTag(id) {

    return myApi.getByID(id, 'TagMaster/GetTasksByTag');
}