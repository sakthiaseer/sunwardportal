import myApi from '@/util/api'



export function getPackagingTube(data) {

    return myApi.getItem(data, "PackagingTube/GetData")
}
export function getPackagingTubes(){
    return myApi.getAll("PackagingTube/GetPackagingTubes")
}
export function createPackagingTube(data) {
    return myApi.create(data, "PackagingTube/InsertPackagingTube")
}

export function updatePackagingTube(data) {

    return myApi.update(data.tubeId, data, "PackagingTube/UpdatePackagingTube")
}
export function deletePackagingTube(data) {

    return myApi.delete(data.tubeId, 'PackagingTube/DeletePackagingTube')
}
export function getPackagingTubesByRefNo(data) {
    return myApi.getItems(data, 'PackagingTube/GetPackagingTubesByRefNo')
  }
