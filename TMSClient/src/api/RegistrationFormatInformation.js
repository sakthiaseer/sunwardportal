import myApi from '@/util/api'

export function getRegistrationFormatInformation() {

    return myApi.getAll('RegistrationFormatInformation/GetRegistrationFormatInformation')
}

export function getRegistrationFormatInformationItems() {

  return myApi.getAll('RegistrationFormatInformation/GetRegistrationFormatInformationItems')
}
export function getDatas(data) {

  return myApi.getItem(data, "RegistrationFormatInformation/GetData")
}
export function createRegistrationFormatInformation(data) {
    return myApi.create(data, "RegistrationFormatInformation/InsertRegistrationFormatInformation")
  }

  export function updateRegistrationFormatInformation(data) {

    return myApi.update(data.registrationFormatInformationId, data, "RegistrationFormatInformation/UpdateRegistrationFormatInformation")
  }
  export function deleteRegistrationFormatInformation(data) {
  
    return myApi.delete(data.registrationFormatInformationId, 'RegistrationFormatInformation/DeleteRegistrationFormatInformation')
  }

export function getRegistrationFormatInformationLine (registrationFormatInformationId,userId) {
    return myApi.getAll('RegistrationFormatInformation/GetRegistrationFormatInformationLine/?id='+registrationFormatInformationId+"&&userId="+userId)
  }

export function createRegistrationFormatInformationLine(data) {
    return myApi.create(data, "RegistrationFormatInformation/InsertRegistrationFormatInformationLine")
  }
  export function updateRegistrationFormatInformationLine(data) {

    return myApi.update(data.registrationFormatInformationLineId, data, "RegistrationFormatInformation/UpdateRegistrationFormatInformationLine")
  }
  export function deleteRegistrationFormatInformationLine(data) {

    return myApi.delete(data.registrationFormatInformationLineId, 'RegistrationFormatInformation/DeleteRegistrationFormatInformationLine')
  }

  export function getRegistrationFormatInformationLineUsers(id) {
    return myApi.getByID(id, 'RegistrationFormatInformation/GetRegistrationFormatInformationLineUsers')
  }
  export function deleteApplicationFormAccess(data) {

    return myApi.update(data.registrationFormatInformationLineUsersId, data,'RegistrationFormatInformation/DeleteApplicationFormAccess')
  }
  export function updateApplicationFormAccess(data) {

    return myApi.update(data.registrationFormatInformationLineUsersId, data,'RegistrationFormatInformation/updateApplicationFormAccess')
  }