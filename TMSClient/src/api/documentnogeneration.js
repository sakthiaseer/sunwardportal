import myApi from '@/util/api'

export function getDocumentNoSeriesItems() {

    return myApi.getAll('DocumentNoSeries/GetDocumentNoSeriesItems')
}
export function getDocumentNoSeries(data) {

    return myApi.getItem(data, "DocumentNoSeries/GetData")
}
export function createDocumentNoSeries(data) {
    return myApi.create(data, "DocumentNoSeries/InsertDocumentNoSeries")
}
export function updateDocumentNoSeries(data) {
    return myApi.update(data.numberSeriesId,data, "DocumentNoSeries/UpdateDocumentNoSeries")
}
export function deleteDocumentNoSeries(data) {

    return myApi.delete(data.numberSeriesId, 'DocumentNoSeries/DeleteDocumentNoSeries')
}
export function isProfileNoGenerated(id) {

    return myApi.getByID(id, 'DocumentNoSeries/IsProfileNoGenerated')
}
export function getDocumentNoSeriesSearch(data) {

    return myApi.getItem(data, "DocumentNoSeries/GetDocumentNoSeriesSearch")
}
export function getDocumentNoSeriesBySearch(data) {

    return myApi.getItem(data, "DocumentNoSeries/GetDocumentNoSeriesBySearch")
}
export function getDocumentNoSeriesItemsByProfileId(id) {

    return myApi.getByID(id, 'DocumentNoSeries/GetDocumentNoSeriesItemsByProfileId')
}

export function getDocumentReserveNoSeriesByProfileId(data) {

    return myApi.getItem(data, 'DocumentNoSeries/GetDocumentReserveNoSeriesByProfileId')
}
export function getFileProfilePathByDocumentNo(data) {

    return myApi.getItem(data, 'DocumentLink/GetFileProfilePathByDocumentNo')
}
export function updateDescriptionNoSeries(data) {
    return myApi.update(data.numberSeriesId,data, "DocumentNoSeries/UpdateDescriptionNoSeries")
}