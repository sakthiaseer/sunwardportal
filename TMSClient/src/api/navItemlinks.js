import myApi from '@/util/api'


export function getNavItems() {
  return myApi.getAll('NavItem/GetNavItems')
}

//

export function getDescriptionCategory() {
  return myApi.getAll('NavItem/GetDescriptionCategory')
}


export function getNavItemBasedRelatedItemsNos(id) {

  return myApi.getByID(id,'NavItem/GetNavItemBasedRelatedItemsNos')
}


export function getRelatedItemNo() {
  return myApi.getAll('NavItem/GetRelatedItemNo')
}

export function getnavItemspkg() {
    return myApi.getAll('NavItem/GetNavItemspkg')
  }



export function getItems() {
  return myApi.getAll('NAVFunc/GetItems')
}
export function getNAVItemLinks() {

    return myApi.getAll('NAVItemLinks/GetNAVItemLinkss')
}

export function getNAVItemLinkdata(data) {

    return myApi.getItem(data, "NAVItemLinks/GetData")
}

export function createNAVItemLinks(data) {
    return myApi.create(data, "NAVItemLinks/InsertNAVItemLinks")
}

export function updateNAVItemLinks(data) {

    return myApi.update(data.itemLinkId, data, "NAVItemLinks/UpdateNAVItemLinks")
}
export function deleteNAVItemLinks(data) {

    return myApi.delete(data.itemLinkId, 'NAVItemLinks/DeleteNAVItemLinks')
}
