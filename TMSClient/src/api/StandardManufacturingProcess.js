import myApi from '@/util/api'

export function getStandardManufacturingProcess () {

  return myApi.getAll('StandardManufacturingProcess/GetStandardManufacturingProcess')
}
export function getData (data) {

  return myApi.getItem(data, "StandardManufacturingProcess/GetData")
}
export function createStandardManufacturingProcess (data) {
  return myApi.create(data, "StandardManufacturingProcess/InsertStandardManufacturingProcess")
}
export function updateStandardManufacturingProcess (data) {

  return myApi.update(data.standardManufacturingProcessId, data, "StandardManufacturingProcess/UpdateStandardManufacturingProcess")
}
export function deleteStandardManufacturingProcess (data) {

  return myApi.delete(data.standardManufacturingProcessId, 'StandardManufacturingProcess/DeleteStandardManufacturingProcess')
}
export function getStandardManufacturingProcessLine (id) {

  return myApi.getByID(id, 'StandardManufacturingProcess/GetStandardManufacturingProcessLine')
}
export function createStandardManufacturingProcessLine (data) {
  return myApi.create(data, "StandardManufacturingProcess/InsertStandardManufacturingProcess")
}
export function updateStandardManufacturingProcessLine (data) {

  return myApi.update(data.standardManufacturingProcessLineId, data, "StandardManufacturingProcess/UpdateStandardManufacturingProcessLine")
}
export function deleteStandardManufacturingProcessLine (data) {

  return myApi.delete(data.standardManufacturingProcessLineId, 'StandardManufacturingProcess/DeleteStandardManufacturingProcessLine')
}