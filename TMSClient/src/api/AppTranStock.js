import myApi from '@/util/api'

export function getAppTranStocks () {

    return myApi.getAll('AppTranStock/GetAppTranStocks')
}
export function getDatas (data) {

    return myApi.getItem(data, "AppTranStock/GetData")
}
export function createAppTranStock (data) {
    return myApi.create(data, "AppTranStock/InsertAppTranStock")
}
export function updateAppTranStock (data) {

    return myApi.update(data.id, data, "AppTranStock/UpdateAppTranStock")
}
export function deleteAppTranStock (data) {

    return myApi.delete(data.id, 'AppTranStock/DeleteAppTranStock')
}

export function getAppTranStockLinesById (id) {

    return myApi.getByID(id,'AppTranStock/GetAPPTranStockLinesById')
}
export function createAppTranStockLine (data) {
    return myApi.create(data, "AppTranStock/InsertAppTranStockLine")
}
export function updateAppTranStockLine (data) {

    return myApi.update(data.id, data, "AppTranStock/UpdateTranStockLine")
}
export function deleteAppTranStockLine (data) {

    return myApi.delete(data.id, 'AppTranStock/DeleteTranStockLine')
}