import myApi from '@/util/api'

export function getExchangeRates() {

    return myApi.getAll('ExchangeRate/GetExchangeRate')
}
export function getExchangeRate(data) {

    return myApi.getItem(data, "ExchangeRate/GetData")
}
export function getExchangeRateLineID(id) {

    return myApi.getByID(id, 'ExchangeRate/GetExchangeRateLineID')
}
export function createExchangeRate(data) {
    return myApi.create(data, "ExchangeRate/InsertExchangeRate")
}

export function updateExchangeRate(data) {

    return myApi.update(data.exchangeRateId, data, "ExchangeRate/UpdateExchangeRate")
}
export function deleteExchangeRate(data) {

    return myApi.delete(data.exchangeRateId, 'ExchangeRate/DeleteExchangeRate')
}



export function createExchangeRateLine(data) {

    return myApi.create(data, "ExchangeRate/InsertExchangeRateLine")
}

export function updateExchangeRateLine(data) {

    return myApi.update(data.exchangeRateLineId, data, "ExchangeRate/UpdateExchangeRateLine")
}

export function deleteExchangeRateLine(data) {

    return myApi.delete(data.exchangeRateLineId, 'ExchangeRate/DeleteExchangeRateLine')
}



export function getApplicationMasterCurrency() {

    return myApi.getAll('ExchangeRate/GetApplicationMasterCurrency')
}

export function getApplicationMasterPurpose() {

    return myApi.getAll('ExchangeRate/GetApplicationMasterPurpose')
}


export function getExchangeVersion(id) {

    return myApi.getByID(id,'ExchangeRate/GetExchangeVersion')
}
export function getExchangeLineVersion(id) {

    return myApi.getByID(id,'ExchangeRate/GetExchangeLineVersion')
}


export function updateVersionExchangeRate(data) {

    return myApi.update(data.exchangeRateId, data, "ExchangeRate/UpdateVersionExchangeRate")
}
export function updateVersionExchangeRateLine(data) {

    return myApi.update(data.exchangeRateLineId, data, "ExchangeRate/UpdateVersionExchangeRateLine")
}
export function createVersion (data) {

    return myApi.update(data.exchangeRateId, data, "ExchangeRate/CreateVersion")
}
export function applyVersion (data) {

    return myApi.update(data.exchangeRateId, data, "ExchangeRate/ApplyVersion")
}
export function tempVersion(data) {

    return myApi.update(data.exchangeRateId, data, "ExchangeRate/TempVersion")
}
export function undoVersion(id) {

    return myApi.getByID(id, "ExchangeRate/UndoVersion")
}
export function deleteVersion(id) {

    return myApi.delete(id, 'ExchangeRate/DeleteVersion')
}