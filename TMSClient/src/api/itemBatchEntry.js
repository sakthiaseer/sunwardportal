import myApi from '@/util/api'

export function getItemBatchInfo () {

    return myApi.getAll('ItemBatchInfo/GetItemBatchInfo')
}
export function getItemBatchInfoData (data) {

    return myApi.getItem(data, "ItemBatchInfo/GetItemBatchInfoData")
}
export function createItemBatchInfo (data) {
    return myApi.create(data, "ItemBatchInfo/InsertItemBatchInfo")
}

export function updateItemBatchInfo (data) {

    return myApi.update(data.itemBatchId, data, "ItemBatchInfo/UpdateItemBatchInfo")
}
export function deleteItemBatchInfo (data) {

    return myApi.delete(data.itemBatchId, 'ItemBatchInfo/DeleteItemBatchInfo')
}
export function getNavItemsByCountryDataBase (id) {
    return myApi.getByID(id, "NavItem/GetNavItemsByCountryDataBase");
}
export function getItemBatchInfoByItemId (id) {
    return myApi.getByID(id, "ItemBatchInfo/GetItemBatchInfoByItemId");
}
export function getItemBatchInfoByOutStockQty (id) {
    return myApi.getByID(id, "ItemBatchInfo/GetItemBatchInfoByOutStockQty");
}
export function getItemBatchInfoByBatchInfoId (id) {
    return myApi.getByID(id, "ItemBatchInfo/GetItemBatchInfoByBatchInfoId");
}
export function getItemByBatchNo (data) {

    return myApi.getItem(data, "ItemBatchInfo/GetItemByBatchNo")
}

export function getItemsByCountryDataBase (id) {
    return myApi.getByID(id, "NavItem/GetItemsByCountryDataBase");
}
export function getItemStockInfo () {

    return myApi.getAll('ItemBatchInfo/GetItemStockInfo')
}
export function getItemBatchInfoForSampleLine (id) {
    return myApi.getByID(id, "ItemBatchInfo/GetItemBatchInfoForSampleLine");
}

export function getItemStockInfoByInventoryType (id) {
    return myApi.getByID(id, "ItemBatchInfo/GetItemStockInfoByInventoryType");
}
export function getItemBatchInfoById (id) {
    return myApi.getByID(id, "ItemBatchInfo/GetItemBatchInfoById");
}
export function getItemStockReport () {

    return myApi.getAll('ItemBatchInfo/GetItemStockReport')
}
export function getBalanceBatchInfoByItemId (data) {
    return myApi.getItem(data, "ItemBatchInfo/GetBalanceBatchInfoByItemId");
}
export function getItemStockReportByClosingDate (data) {

    return myApi.getItem(data,'ItemBatchInfo/GetItemStockReportByClosingDate')
}