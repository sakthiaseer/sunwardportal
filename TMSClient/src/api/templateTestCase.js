import myApi from '@/util/api'

export function getTemplateTestCase() {

  return myApi.getAll('TemplateTestCase/GetTemplateTestCase')
}
export function getSearchData(data) {

  return myApi.getItem(data, "TemplateTestCase/GetData")
}

export function createTemplateTestCase(data) {
  return myApi.create(data, "TemplateTestCase/InsertTemplateTestCase")
}
export function createTemplateTestCaseInherit(data) {
  return myApi.create(data, "TemplateTestCase/InsertTemplateTestCaseInherit")
}
export function updateTemplateTestCase(data) {

  return myApi.update(data.templateTestCaseId, data, "TemplateTestCase/UpdateTemplateTestCase")
}
export function deleteTemplateTestCase(data) {

  return myApi.delete(data.templateTestCaseId, 'TemplateTestCase/DeleteTemplateTestCase')
}




export function getTemplateTestCaseForm() {

  return myApi.getAll('TemplateTestCase/GetTemplateTestCaseForm')
}
export function getTemplateTestCaseFormData(data) {

  return myApi.getItem(data, "TemplateTestCase/GetTemplateTestCaseFormData")
}

export function createTemplateTestCaseForm(data) {
  return myApi.create(data, "TemplateTestCase/InsertTemplateTestCaseForm")
}
export function updateTemplateTestCaseForm(data) {

  return myApi.update(data.templateTestCaseFormId, data, "TemplateTestCase/UpdateTemplateTestCaseForm")
}
export function deleteTemplateTestCaseForm(data) {

  return myApi.delete(data.templateTestCaseFormId, 'TemplateTestCase/DeleteTemplateTestCaseForm')
}
export function updateTemplateTestCaseFormTopic(data) {

  return myApi.update(data.templateTestCaseFormId, data, "TemplateTestCase/updateTemplateTestCaseFormTopic")
}


export function getTemplateTestCaseLinkDocNo(id) {

  return myApi.getByID(id, "TemplateTestCase/GetTemplateTestCaseLinkDocNo")
}
export function getTemplateTestCaseLink(id, type, typeId) {

  return myApi.getAll("TemplateTestCase/getTemplateTestCaseLink?id=" + id + "&&type=" + type + "&&typeId=" + typeId)
}
export function createTemplateTestCaseLink(data) {
  return myApi.create(data, "TemplateTestCase/InsertTemplateTestCaseLink")
}
export function updateTemplateTestCaseLink(data) {

  return myApi.update(data.templateTestCaseLinkId, data, "TemplateTestCase/UpdateTemplateTestCaseLink")
}
export function deleteTemplateTestCaseLink(data) {

  return myApi.delete(data.templateTestCaseLinkId, 'TemplateTestCase/DeleteTemplateTestCaseLink')
}

export function generateTemplateTestCaseDocNo(data) {
  return myApi.create(data, "TemplateTestCase/GenerateTemplateTestCaseDocNo")
}



export function getTemplateTestCaseCheckList(id, type, typeId) {

  return myApi.getAll("TemplateTestCase/getTemplateTestCaseCheckList?id=" + id + "&&type=" + type + "&&typeId=" + typeId)
}
export function createTemplateTestCaseCheckList(data) {
  return myApi.create(data, "TemplateTestCase/InsertTemplateTestCaseCheckList")
}
export function updateTemplateTestCaseCheckList(data) {

  return myApi.update(data.templateTestCaseCheckListId, data, "TemplateTestCase/UpdateTemplateTestCaseCheckList")
}
export function updateTemplateTestCaseCheckListSeqNo(data) {

  return myApi.update(data.templateTestCaseCheckListId, data, "TemplateTestCase/updateTemplateTestCaseCheckListSeqNo")
}
export function deleteTemplateTestCaseCheckList(data) {

  return myApi.delete(data.templateTestCaseCheckListId, 'TemplateTestCase/DeleteTemplateTestCaseCheckList')
}




export function getTemplateTestCaseCheckListResponse(id) {

  return myApi.getByID(id, "TemplateTestCase/GetTemplateTestCaseCheckListResponse")
}
export function createTemplateTestCaseCheckListResponse(data) {
  return myApi.create(data, "TemplateTestCase/InsertTemplateTestCaseCheckListResponse")
}
export function updateTemplateTestCaseCheckListResponse(data) {

  return myApi.update(data.templateTestCaseCheckListResponseId, data, "TemplateTestCase/updateTemplateTestCaseCheckListResponse")
}
export function deleteTemplateTestCaseCheckListResponse(data) {

  return myApi.delete(data.templateTestCaseCheckListResponseId, 'TemplateTestCase/deleteTemplateTestCaseCheckListResponse')
}


export function createTemplateTestCaseCheckListResponseRecurrence(data) {
  return myApi.create(data, "TemplateTestCase/InsertTemplateTestCaseCheckListResponseRecurrence")
}

export function getTemplateTestCaseCheckListResponseRecurrence(id) {

  return myApi.getByID(id, "TemplateTestCase/GetTemplateTestCaseCheckListResponseRecurrence")
}


export function updateTemplateTestCaseCheckListTopic(data) {

  return myApi.update(data.templateTestCaseCheckListId, data, "TemplateTestCase/UpdateTemplateTestCaseCheckListTopic")
}





export function getApplicationWikiLineDuty(applicationWikiLineId) {

  return myApi.getAll("TemplateTestCase/getApplicationWikiLineDuty?id=" + applicationWikiLineId);
}

export function createApplicationWikiLineDuty(data) {
  return myApi.create(data, "TemplateTestCase/InsertApplicationWikiLineDuty")
}

export function updateApplicationWikiLineDuty(data) {

  return myApi.update(data.templateTestCaseCheckListResponseDutyId, data, "TemplateTestCase/UpdateApplicationWikiLineDuty")
}
export function deleteApplicationWikiLineDuty(data) {

  return myApi.delete(data.templateTestCaseCheckListResponseDutyId, 'TemplateTestCase/DeleteApplicationWikiLineDuty')
}
export function deleteWikiResponsible(data) {

  return myApi.delete(data.wikiResponsibilityID, 'TemplateTestCase/DeleteWikiResponsible')
}
export function getWikiResponsibleByID(id) {

  return myApi.getByID(id, 'TemplateTestCase/GetWikiResponsibleByID')
}
export function deleteWikiResponsibles(data) {

  return myApi.update(data.wikiResponsibilityID, data, "TemplateTestCase/DeleteWikiResponsibles")
}


export function getTemplateTestCaseProposal(id, type, typeId) {

  return myApi.getAll("TemplateTestCase/GetTemplateTestCaseProposal?id=" + id + "&&type=" + type + "&&typeId=" + typeId)
}

export function createTemplateTestCaseProposal(data) {
  return myApi.create(data, "TemplateTestCase/InsertTemplateTestCaseProposal")
}
export function deleteTemplateTestCaseProposal(data) {

  return myApi.delete(data.templateTestCaseProposalId, 'TemplateTestCase/DeleteTemplateTestCaseProposal')
}
export function getTemplateTestCaseProposalUsers(id) {

  return myApi.getByID(id, "TemplateTestCase/GetTemplateTestCaseProposalUsers")
}
export function updateTemplateTestCaseProposal(data) {

  return myApi.update(data.templateTestCaseProposalId, data, "TemplateTestCase/updateTemplateTestCaseProposal")
}
export function getTemplateTestCaseByCompanyProposalUsers(id, caseFormId) {

  return myApi.getAll("TemplateTestCase/GetTemplateTestCaseByCompanyProposalUsers?id=" + id + "&&caseFormId=" + caseFormId)
}
export function getWikiResponsiblesByExits(id, caseFormId) {

  return myApi.getAll("TemplateTestCase/getWikiResponsiblesByExits?id=" + id + "&&caseFormId=" + caseFormId)
}
export function getTemplateTestChecListInherit(data) {

  return myApi.getItem(data, "TemplateTestCase/TemplateTestChecListInherit")
}