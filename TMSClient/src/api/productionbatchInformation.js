import myApi from '@/util/api'

export function getProductionBatchInformations () {

    return myApi.getAll('ProductionBatchInformation/GetProductionBatchInformations')
}
export function getProductionBatchInformation (data) {

    return myApi.getItem(data, "ProductionBatchInformation/GetData")
}

export function createProductionBatchInformation (data) {
    return myApi.create(data, "ProductionBatchInformation/InsertProductionBatchInformation")
}

export function updateProductionBatchInformation (data) {

    return myApi.update(data.productionBatchInformationID, data, "ProductionBatchInformation/UpdateProductionBatchInformation")
}
export function deleteProductionBatchInformation (data) {

    return myApi.delete(data.productionBatchInformationID, 'ProductionBatchInformation/DeleteProductionBatchInformation')
}

export function getProductionBatchInformationLinesByID (id) {

    return myApi.getByID(id, "ProductionBatchInformationLine/GetProductionBatchInformationLinesByID")
}

export function createProductionBatchInformationLine (data) {
    return myApi.create(data, "ProductionBatchInformationLine/InsertProductionBatchInformationLine")
}

export function updateProductionBatchInformationLine (data) {

    return myApi.update(data.productionBatchInformationLineID, data, "ProductionBatchInformationLine/UpdateProductionBatchInformationLine")
}
export function deleteProductionBatchInformationLine (data) {

    return myApi.delete(data.productionBatchInformationLineID, 'ProductionBatchInformationLine/DeleteProductionBatchInformationLine')
}
export function getNavProductCode () {

    return myApi.getAll('ProductionBatchInformationLine/GetNavProductCode')
}
export function getProductionBatchSize () {

    return myApi.getAll('ProductionBatchInformation/GetProductionBatchSize')
}
export function getProductionBatchInformationDetail () {

    return myApi.getAll('ProductionBatchInformation/GetProductionBatchInformationDetail')
}
export function getProductionBatchNos () {

    return myApi.getAll('ProductionBatchInformationLine/GetProductionBatchNos')
}
export function getProductionBatchInformationReport (data) {
    return myApi.getItems(data, 'ProductionBatchInformationLine/GetProductionBatchInformationReport')
}
export function getProductionBatchQRCodeReport (data) {

    return myApi.getItems(data, 'ProductionBatchInformationLine/GetProductionBatchQRCodeReport')
}
export function getProductionBatchInformationSyncBMR () {

    return myApi.getAll('ProductionBatchInformation/ProductionBatchInformationSyncBMR')
}