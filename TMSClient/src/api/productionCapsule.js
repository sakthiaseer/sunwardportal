import myApi from '@/util/api'

export function getProductionCapsules() {

    return myApi.getAll('ProductionCapsule/GetProductionCapsules')
}

export function getProductionCapsule(data) {

    return myApi.getItem(data, "ProductionCapsule/GetData")
}

export function createProductionCapsule(data) {
    return myApi.create(data, "ProductionCapsule/InsertProductionCapsule")
}

export function updateProductionCapsule(data) {

    return myApi.update(data.productionCapsuleID, data, "ProductionCapsule/UpdateProductionCapsule")
}
export function deleteProductionCapsule(data) {

    return myApi.delete(data.productionCapsuleID, 'ProductionCapsule/DeleteProductionCapsule')
}

export function getProductionCapsuleLinesByID(id) {
    return myApi.getByID(id, "ProductionCapsuleLine/ProductionCapsuleLinesByID")
}

export function createProductionCapsuleLine(data) {
    return myApi.create(data, "ProductionCapsuleLine/InsertProductionCapsuleLine")
}

export function updateProductionCapsuleLine(data) {

    return myApi.update(data.productionCapsuleLineID, data, "ProductionCapsuleLine/UpdateProductionCapsuleLine")
}
export function deleteProductionCapsuleLine(data) {

    return myApi.delete(data.productionCapsuleLineID, 'ProductionCapsuleLine/DeleteProductionCapsuleLine')
}
export function getDescriptionByCountryItems(id) {
    return myApi.getByID(id, 'NavItem/GetDescriptionByCountryItems')

}
export function getProductionCapsulesByRefNo(data) {
    return myApi.getItems(data, 'ProductionCapsule/GetProductionCapsulesByRefNo')
}