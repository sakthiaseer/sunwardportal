import myApi from '@/util/api'

export function getNavProductMasters() {

    return myApi.getAll('NavProductMaster/GetNavProductMaster')
}
export function getNavProductMaster(data) {

    return myApi.getItem(data, "NavProductMaster/GetData")
}
export function createNavProductMaster(data) {
    return myApi.create(data, "NavProductMaster/InsertNavProductMaster")
}
export function updateNavProductMaster(data) {

    return myApi.update(data.productMasterId, data, "NavProductMaster/UpdateNavProductMaster")
}
export function deleteNavProductMaster(data) {

    return myApi.delete(data.productMasterId, 'NavProductMaster/DeleteNavProductMaster')
}
export function getNavProductMasterByFinishProduct(id,productCreationType) {  
    return myApi.getAll("NavProductMaster/GetNavProductMasterByFinishProduct/?id="+id+"&&type="+productCreationType);
}
export function isMasterExist(id) {  
    return myApi.getByID(id, "NavProductMaster/IsMasterExist")
}
