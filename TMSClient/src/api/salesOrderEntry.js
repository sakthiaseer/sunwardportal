import myApi from '@/util/api'

export function getSalesOrderEntryItems() {

    return myApi.getAll('SalesOrderEntry/GetSalesOrderEntryItems')
}

export function getSalesOrderEntryItemsByType(id) {

    return myApi.getByID(id, 'SalesOrderEntry/GetSalesOrderEntryItemsByType')
}
export function getTagAlongOrderCustomer() {

    return myApi.getAll('SalesOrderEntry/GetTagAlongOrderCustomer')
}
export function getSalesOrderEntryData(data) {

    return myApi.getItem(data, "SalesOrderEntry/GetSalesOrderEntryData")
}

export function createSalesOrderEntry(data) {
    return myApi.create(data, "SalesOrderEntry/InsertSalesOrderEntry")
}

export function updateSalesOrderEntry(data) {

    return myApi.update(data.salesOrderEntryID, data, "SalesOrderEntry/UpdateSalesOrderEntry")
}
export function deleteSalesOrderEntry(data) {

    return myApi.delete(data.salesOrderEntryID, 'SalesOrderEntry/DeleteSalesOrderEntry')
}
export function uploadSalesOrderEntryDocuments(data, sessionId) {

    return myApi.uploadFile(data, "SalesOrderEntry/UploadSalesOrderEntryDocuments?sessionId=" + sessionId)
}

export function downLoadSalesOrderEntryDocument(data) {

    return myApi.downLoadDocument(data, "SalesOrderEntry/DownLoadSalesOrderEntryLineDocument")
}


export function getSalesOrderEntryDocument(sessionId) {

    return myApi.getAll("SalesOrderEntry/GetSalesOrderEntryDocument?sessionId=" + sessionId)
}
export function deleteDocuments(id) {

    return myApi.delete(id, 'SalesOrderEntry/DeleteDocuments')
}


export function getSalesOrderProfile() {

    return myApi.getAll('SalesOrderProfile/GetSalesOrderProfile')
}
export function updateSalesOrderProfile(data) {

    return myApi.create(data, "SalesOrderProfile/UpdateSalesOrderProfile")
}
export function getSalesOrderEntryBySearch(data) {

    return myApi.getItem(data, "SalesOrderEntry/GetSalesOrderEntryBySearch")
}

export function getSalesOrderReport() {

    return myApi.getAll('SalesOrderEntry/GetSalesOrderReport')
}

export function getContractNos() {

    return myApi.getAll('SalesOrderEntry/GetContractNos')
}

export function getTenderAgencySalesOrders() {

    return myApi.getAll('SalesOrderEntry/GetTenderAgencySalesOrders')
}


export function getContractNoSearch(data) {

    return myApi.getItem(data, "SalesOrderEntry/GetContractNoSearch")
}
export function getSalesOrderReportBySearch(data) {

    return myApi.getItem(data, "SalesOrderEntry/GetSalesOrderReportBySearch")
}

export function getSalesOrderEntryItemById(id) {

    return myApi.getByID(id, 'SalesOrderEntry/GetSalesOrderEntryItemById')
}
export function getOrderNoDropDown() {

    return myApi.getAll('SalesOrderEntry/GetOrderNoDropDown')
}

export function getNavSalesOrderLines(data) {

    return myApi.getItem(data, "SalesOrderEntry/GetNavSalesOrderLines")
}

export function createNonDeliverSO(data) {
    return myApi.create(data, "SalesOrderEntry/InsertNonDeliverSO")
}
export function getCompanyListingForBlanketTenderAgency() {

    return myApi.getAll('CompanyListing/GetCompanyListingForBlanketTenderAgency')
}
export function getContractDistributionItemsAll() {

    return myApi.getAll('ContractDistributionSalesEntryLine/GetContractDistributionItemsAll')
}
export function getSalesOrderEntryUpdateSearch(data) {

    return myApi.getItem(data, "SalesOrderEntry/GetSalesOrderEntryUpdateSearch")
}
export function getContractDistributionItemsBySalesId(id) {

    return myApi.getByID(id, 'SalesOrderEntry/GetContractDistributionItemsBySalesId')
}
export function getProjectedDeliveryBySalesId(id) {

    return myApi.getByID(id, 'SalesOrderEntry/GetProjectedDeliveryBySalesId')
}
export function getCompanyListingsAll() {

    return myApi.getAll('CompanyListing/GetCompanyListingsAll')
}

export function getSalesOrdersByCustomer(data) {

    return myApi.getItem(data, "SalesOrderEntry/GetSalesOrdersByCustomer")
}

export function getProductsByContractNo(id) {

    return myApi.getByID(id, 'SalesOrderEntry/GetProductsByContractNo')
}
export function getSalesOrderEntryItemsForDropDown() {

    return myApi.getAll('SalesOrderEntry/GetSalesOrderEntryItemsForDropDown')
}
export function getContractNosById(id) {

    return myApi.getByID(id, 'SalesOrderEntry/GetContractNosById')
}
export function getProductsContractNo(data) {

    return myApi.getItem(data, "SalesOrderEntry/GetProductsContractNo")
}