import myApi from '@/util/api'


export function getNotesBySessionId(sessionID) {

    return myApi.getBySessionID(sessionID, "Notes/GetNotesBySessionId")
}
export function getNotesByDocumentId(id) {

    return myApi.getByID(id, "Notes/GetNotesByDocumentId")
}

export function getPersonalNotes(data) {

    return myApi.getItem(data, "Notes/GetPersonalNotes")
  }
export function createNotes(data) {

    return myApi.create(data, "Notes/InsertNotes")
}
export function updateNotes(data) {

    return myApi.update(data.notesId, data, "Notes/UpdateNotes")
}
export function createPersonalNotes(data) {

    return myApi.create(data, "Notes/InsertPersonalNotes")
}
export function updatePersonalNotes(data) {

    return myApi.update(data.notesId, data, "Notes/UpdatePersonalNotes")
}
export function deleteNotes(data) {

    return myApi.delete(data.notesId, 'Notes/DeleteNotes')
}