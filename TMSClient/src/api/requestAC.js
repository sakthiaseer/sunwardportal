import myApi from '@/util/api'

export function getRequestACItems() {

    return myApi.getAll('RequestAC/GetRequestACItems')
}

export function getRequestAC(data) {

    return myApi.getItem(data, "RequestAC/GetData")
}

export function createRequestAC(data) {
    return myApi.create(data, "RequestAC/InsertRequestAC")
}

export function updateRequestAC(data) {

    return myApi.update(data.requestAcid, data, "RequestAC/UpdateRequestAC")
}
export function deleteRequestAC(data) {

    return myApi.delete(data.requestAcid, 'RequestAC/DeleteRequestAC')
}
export function getCustomers() {
    return myApi.getAll('NavCustomer/getCustomers')
}

export function getRequestACLinesByID(id) {

    return myApi.getByID(id, "RequestAC/GetRequestACLinesByID")
}
export function getRequestACItemsByStatus(id) {

    return myApi.getByID(id, "RequestAC/GetRequestACItemsByStatus")
}

export function getRequestACLines() {

    return myApi.getAll("RequestACLine/GetRequestACLines")
}
export function createRequestACLine(data) {
    return myApi.create(data, "RequestACLine/InsertRequestACLine")
}

export function updateRequestACLine(data) {

    return myApi.update(data.requestACLineId, data, "RequestACLine/UpdateRequestACLine")
}
export function deleteRequestACLine(data) {

    return myApi.delete(data.requestACLineId, 'RequestACLine/DeleteRequestACLine')
}
export function getIncreaseDecreaseLines(id) {

    return myApi.getByID(id, 'RequestACLine/GetIncreaseDecreaseLines')
}