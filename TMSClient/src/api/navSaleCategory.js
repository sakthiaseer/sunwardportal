import myApi from '@/util/api'

export function getNavSaleCategory() {

    return myApi.getAll('NavSaleCategory/GetNavCategory')
}
export function getNavProductMasterByType(id) {

    return myApi.getByID(id, 'NavProductMaster/GetNavProductMasterByType')
}
export function getSearchData(data) {

    return myApi.getItem(data, "NavSaleCategory/GetData")
}
export function updateNavSaleCategory(data) {

    return myApi.update(data.saleCategoryId, data, "NavSaleCategory/UpdateNavSaleCategory")
}
