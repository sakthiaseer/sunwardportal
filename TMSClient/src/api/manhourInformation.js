import myApi from '@/util/api'


export function getMachineTimeManHourInfo(id){
    return myApi.getByID(id,"ProcessMachineTimeProductionLine/GetMachineTimeManHourInfo")
}
export function createMachineTimeManHourInfo(data) {
    return myApi.create(data, "ProcessMachineTimeProductionLine/InsertMachineTimeManHourInfo")
}

export function updateMachineTimeManHourInfo(data) {

    return myApi.update(data.machineTimeManHourInfoId, data, "ProcessMachineTimeProductionLine/UpdateMachineTimeManHourInfo")
}
export function deleteMachineTimeManHourInfo(data) {

    return myApi.delete(data.machineTimeManHourInfoId, 'ProcessMachineTimeProductionLine/DeleteMachineTimeManHourInfo')
}

export function getProcessManPowerSpecialNotes(id){
    return myApi.getByID(id,"ProcessMachineTimeProductionLine/GetProcessManPowerSpecialNotes")
}
export function createProcessManPowerSpecialNotes(data) {
    return myApi.create(data, "ProcessMachineTimeProductionLine/InsertProcessManPowerSpecialNotes")
}

export function updateProcessManPowerSpecialNotes(data) {

    return myApi.update(data.processManPowerSpecialNotesId, data, "ProcessMachineTimeProductionLine/UpdateProcessManPowerSpecialNotes")
}
export function deleteProcessManPowerSpecialNotes(data) {

    return myApi.delete(data.processManPowerSpecialNotesId, 'ProcessMachineTimeProductionLine/DeleteProcessManPowerSpecialNotes')
}
export function getSpecialNotesItem(id){
    return myApi.getByID(id,"ProcessMachineTimeProductionLine/GetSpecialNotesItem")
}