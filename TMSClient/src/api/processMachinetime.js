import myApi from '@/util/api'



export function getProcessMachineTime(data) {

    return myApi.getItem(data, "ProcessMachineTime/GetData")
}
export function getProcessMachineTimes(){
    return myApi.getAll("ProcessMachineTime/GetProcessMachineTimes")
}
export function createProcessMachineTime(data) {
    return myApi.create(data, "ProcessMachineTime/InsertProcessMachineTime")
}

export function updateProcessMachineTime(data) {

    return myApi.update(data.processMachineTimeId, data, "ProcessMachineTime/UpdateProcessMachineTime")
}
export function deleteProcessMachineTime(data) {

    return myApi.delete(data.processMachineTimeId, 'ProcessMachineTime/DeleteProcessMachineTime')
}

export function getProcessMachineTimeProductionLine(data){
    return myApi.getItem(data,"ProcessMachineTimeProductionLine/GetProcessMachineTimeProductionLine")
}
export function createProcessMachineTimeProductionLine(data) {
    return myApi.create(data, "ProcessMachineTimeProductionLine/InsertProcessMachineTimeProductionLine")
}

export function updateProcessMachineTimeProductionLine(data) {

    return myApi.update(data.processMachineTimeLineId, data, "ProcessMachineTimeProductionLine/UpdateProcessMachineTimeProductionLine")
}
export function deleteProcessMachineTimeProductionLine(data) {

    return myApi.delete(data.processMachineTimeLineId, 'ProcessMachineTimeProductionLine/DeleteProcessMachineTimeProductionLine')
}


export function getProcessMachineTimeProductionLineLine(id){
    return myApi.getByID(id,"ProcessMachineTimeProductionLine/GetProcessMachineTimeProductionLineLine")
}
export function createProcessMachineTimeProductionLineLine(data) {
    return myApi.create(data, "ProcessMachineTimeProductionLine/InsertProcessMachineTimeProductionLineLine")
}

export function updateProcessMachineTimeProductionLineLine(data) {

    return myApi.update(data.processMachineTimeProductionLineLineId, data, "ProcessMachineTimeProductionLine/UpdateProcessMachineTimeProductionLineLine")
}
export function deleteProcessMachineTimeProductionLineLine(data) {

    return myApi.delete(data.processMachineTimeProductionLineLineId, 'ProcessMachineTimeProductionLine/DeleteProcessMachineTimeProductionLineLine')
}



export function getProcessMachineTimeProductionMachineProcess(id){
    return myApi.getByID(id,"ProcessMachineTimeProductionLine/GetProcessMachineTimeProductionMachineProcess")
}
export function createProcessMachineTimeProductionMachineProcess(data) {
    return myApi.create(data, "ProcessMachineTimeProductionLine/InsertProcessMachineTimeProductionMachineProcess")
}

export function updateProcessMachineTimeProductionMachineProcess(data) {

    return myApi.update(data.processMachineTimeProductionMachineProcessId, data, "ProcessMachineTimeProductionLine/UpdateProcessMachineTimeProductionMachineProcess")
}
export function deleteProcessMachineTimeProductionMachineProcess(data) {

    return myApi.delete(data.processMachineTimeProductionMachineProcessId, 'ProcessMachineTimeProductionLine/DeleteProcessMachineTimeProductionMachineProcess')
}

export function getNavItemForProcessMachineTime(id){
    return myApi.getByID(id,"NavItem/GetNavItemForProcessMachineTime")
}

export function getItemClassificationMachineDropdown(data) {

    return myApi.getItem(data, "ItemClassificationHeader/GetItemClassificationMachineDropdown")
}
export function getNavBatchSizeForProcessMachineTime(id){
    return myApi.getByID(id,"NavItem/GetNavBatchSizeForProcessMachineTime")
}