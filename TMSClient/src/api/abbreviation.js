import myApi from '@/util/api'

export function getAbbreviations() {

    return myApi.getAll('Abbreviation/GetAbbreviations')
}
export function getAbbreviation(data) {

    return myApi.getItem(data, "Abbreviation/GetData")
}
export function createAbbreviation(data) {
    return myApi.create(data, "Abbreviation/InsertAbbreviations")
}

export function updateAbbreviation(data) {

    return myApi.update(data.applicationAbbreviationId, data, "Abbreviation/UpdateAbbreviations")
}
export function deleteAbbreviation(data) {

    return myApi.delete(data.applicationAbbreviationId, 'Abbreviation/DeleteAbbreviations')
}
export function getDepartments() {

    return myApi.getAll('department/GetDepartments')
}
export function GetApplicationAbbreviationSearch(data)
{
    return myApi.getAll("ApplicationGlossary/GetApplicationGloAbbrSearch?data="+data+"&&type=glossary");
}