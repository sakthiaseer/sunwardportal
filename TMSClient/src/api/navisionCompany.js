import myApi from '@/util/api'

export function getNavisionCompanyByRefNo (data) {
    return myApi.getItems(data, 'NavisionCompany/GetNavisionCompanyByRefNo')
}
export function createNavisionCompany (data) {
    return myApi.create(data, "NavisionCompany/InsertNavisionCompany")
}

export function updateNavisionCompany (data) {

    return myApi.update(data.navisionCompanyId, data, "NavisionCompany/UpdateNavisionCompany")
}
export function deleteNavisionCompany (data) {

    return myApi.delete(data.navisionCompanyId, 'NavisionCompany/DeleteNavisionCompany')
}
export function getNavCustomer () {
    return myApi.getAll('NavCustomer/GetNavCustomers')
}
export function getNavVendor () {
    return myApi.getAll('Navvendor/GetNavVendors')
}
export function getNavCustomersByCountryID (id) {
    return myApi.getByID(id, 'NavCustomer/GetNavCustomersByCountryID')
}
export function getNavVendorsByCountry (id) {
    return myApi.getByID(id, 'Navvendor/GetNavVendorsByCountry')
}

