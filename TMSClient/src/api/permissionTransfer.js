import myApi from '@/util/api'
export function getUserGroupsByUserId(data) {

    return myApi.getItem(data, "UserPermissionTransfer/GetUserGroupsByUserId")
}

export function getDocumentUserRoleByUserId(data) {

    return myApi.getItem(data, "UserPermissionTransfer/GetDocumentUserRoleByUserId")
}
export function getWikiResponsibleByUserId(data) {

    return myApi.getItem(data, "UserPermissionTransfer/GetWikiResponsibleByUserId")
}
export function getDraftWikiResponsibleByUserId(data) {

    return myApi.getItem(data, "UserPermissionTransfer/GetDraftWikiResponsibleByUserId")
}
export function getApplicationFormByUserId(data) {

    return myApi.getItem(data, "UserPermissionTransfer/GetApplicationFormByUserId")
}
export function getItemClassificationPermissionByUserId(data) {

    return myApi.getItem(data, "UserPermissionTransfer/GetItemClassificationPermissionByUserId")
}
export function getDocumentRightsByUserId(data) {

    return myApi.getItem(data, "UserPermissionTransfer/GetDocumentRightsByUserId")
}
export function getDocumentShareUserByUserId(data) {

    return myApi.getItem(data, "UserPermissionTransfer/GetDocumentShareUserByUserId")
}
export function getFileProfileTypeSetAccessByUserId(data) {

    return myApi.getItem(data, "UserPermissionTransfer/GetFileProfileTypeSetAccessByUserId")
}
export function getFolderDiscussionUserByUserId(data) {

    return myApi.getItem(data, "UserPermissionTransfer/GetFolderDiscussionUserByUserId")
}
export function getTaskAppointmentUserByUserId(data) {

    return myApi.getItem(data, "UserPermissionTransfer/GetTaskAppointmentUserByUserId")
}
export function getTaskCommentUserByUserId(data) {

    return myApi.getItem(data, "UserPermissionTransfer/GetTaskCommentUserByUserId")
}
export function getTaskDiscussionByUserId(data) {

    return myApi.getItem(data, "UserPermissionTransfer/GetTaskDiscussionByUserId")
}
export function getTaskInviteUserByUserId(data) {

    return myApi.getItem(data, "UserPermissionTransfer/GetTaskInviteUserByUserId")
}
export function getTaskMembersByUserId(data) {

    return myApi.getItem(data, "UserPermissionTransfer/GetTaskMembersByUserId")
}
export function getTaskNotesByUserId(data) {

    return myApi.getItem(data, "UserPermissionTransfer/GetTaskNotesByUserId")
}
export function getTeamMemberByUserId(data) {

    return myApi.getItem(data, "UserPermissionTransfer/GetTeamMemberByUserId")
}
export function getNoticeUserByUserId(data) {

    return myApi.getItem(data, "UserPermissionTransfer/GetNoticeUserByUserId")
}
export function getTaskMasterByUserId(data) {

    return myApi.getItem(data, "UserPermissionTransfer/GetTaskMasterByUserId")
}






export function updateUserGroupsByUser(data) {

    return myApi.update(data.transferFromUserId, data, "UserPermissionTransfer/UpdateUserGroupsByUser")
}
export function updatedocumentUserRole(data) {

    return myApi.update(data.transferFromUserId, data, "UserPermissionTransfer/UpdatedocumentUserRole")
}
export function updateAppWikiResponsibleByUser(data) {

    return myApi.update(data.transferFromUserId, data, "UserPermissionTransfer/UpdateAppWikiResponsibleByUser")
}
export function updateDraftWikiResponsibleByUser(data) {

    return myApi.update(data.transferFromUserId, data, "UserPermissionTransfer/UpdateDraftWikiResponsibleByUser")
}
export function updateApplicationFormByUser(data) {

    return myApi.update(data.transferFromUserId, data, "UserPermissionTransfer/UpdateApplicationFormByUser")
}
export function updateItemClassificationPermissionByUser(data) {

    return myApi.update(data.transferFromUserId, data, "UserPermissionTransfer/UpdateItemClassificationPermissionByUser")
}
export function updateDocumentRights(data) {

    return myApi.update(data.transferFromUserId, data, "UserPermissionTransfer/UpdateDocumentRights")
}
export function updateDocumentShareUser(data) {

    return myApi.update(data.transferFromUserId, data, "UserPermissionTransfer/UpdateDocumentShareUser")
}
export function updateFileProfileTypeSetAccess(data) {

    return myApi.update(data.transferFromUserId, data, "UserPermissionTransfer/UpdateFileProfileTypeSetAccess")
}
export function updateFolderDiscussionUser(data) {

    return myApi.update(data.transferFromUserId, data, "UserPermissionTransfer/UpdateFolderDiscussionUser")
}
export function updateTaskAppointmentUser(data) {

    return myApi.update(data.transferFromUserId, data, "UserPermissionTransfer/UpdateTaskAppointmentUser")
}
export function updateTaskCommentUser(data) {

    return myApi.update(data.transferFromUserId, data, "UserPermissionTransfer/UpdateTaskCommentUser")
}
export function updateTaskDiscussion(data) {

    return myApi.update(data.transferFromUserId, data, "UserPermissionTransfer/UpdateTaskDiscussion")
}
export function updateTaskInviteUser(data) {

    return myApi.update(data.transferFromUserId, data, "UserPermissionTransfer/UpdateTaskInviteUser")
}
export function updateTaskMembers(data) {

    return myApi.update(data.transferFromUserId, data, "UserPermissionTransfer/UpdateTaskMembers")
}
export function updateTaskNotes(data) {

    return myApi.update(data.transferFromUserId, data, "UserPermissionTransfer/UpdateTaskNotes")
}
export function updateTeamMember(data) {

    return myApi.update(data.transferFromUserId, data, "UserPermissionTransfer/UpdateTeamMember")
}
export function updateNoticeUser(data) {

    return myApi.update(data.transferFromUserId, data, "UserPermissionTransfer/UpdateNoticeUser")
}
export function updateTaskMaster(data) {

    return myApi.update(data.transferFromUserId, data, "UserPermissionTransfer/UpdateTaskMaster")
}