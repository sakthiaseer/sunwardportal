import myApi from '@/util/api'

export function getSalesOrderProducts() {

    return myApi.getAll('SalesOrderProduct/GetSalesOrderProducts')
}
export function getSalesOrderProductData(data) {

    return myApi.getItem(data, "SalesOrderProduct/GetSalesOrderProductData")
}

export function createSalesOrderProduct(data) {
    return myApi.create(data, "SalesOrderProduct/InsertSalesOrderProduct")
}

export function updateSalesOrderProduct(data) {

    return myApi.update(data.salesOrderProductId, data, "SalesOrderProduct/UpdateSalesOrderProduct")
}
export function deleteSalesOrderProduct(data) {

    return myApi.delete(data.salesOrderProductId, 'SalesOrderProduct/DeleteSalesOrderProduct')
}
