import myApi from "@/util/api";

export function getNavCustomers() {
  return myApi.getAll("NavCustomer/GetNavCustomers");
}
export function getNavCustomersByCountry(item) {
  return myApi.getItem(item, "NavCustomer/GetNavCustomersByCountry");
}

export function getNavItems() {
  return myApi.getAll("NavItem/GetNavItems");
}
export function getNavItemsMat() {
  return myApi.getAll("NavItem/GetNavItemsMat");
}
export function getItems() {
  return myApi.getAll("NAVFunc/GetItems");
}

export function getCustomers(item) {
  return myApi.getItem(item, "NAVFunc/GetCustomers");
}

export function getINPItems() {
  return myApi.getAll("NavProductionScheduling/GetNavINPItems");
}

export function getNavINPItemDetails(item) {
  return myApi.getByNavItem(
    item,
    "NavProductionScheduling/GetNavINPItemDetails"
  );
}
export function getRecipes(itemNo, company) {
  return myApi.getByStrings(itemNo, company, "NAVFunc/GetRecipes");
}
export function getSyncProdBOMTree(itemNo, company) {
  return myApi.getByStrings(itemNo, company, "NAVFunc/GetSyncProdBOMTree");
}
export function getParInfoItems(item) {
  return myApi.getByNavItem(item, "NavProductionScheduling/GetParInfoItems");
}

export function getMCRCheckingItems(item) {
  return myApi.getByNavItem(
    item,
    "NavProductionScheduling/GetMCRCheckingItems"
  );
}

export function getPlannerComments() {
  return myApi.getAll("NavProductionScheduling/GetPlannerComments");
}

export function getINPItemsByDemand(item) {
  return myApi.getByNavItem(
    item,
    "NavProductionScheduling/GetINPItemsByDemand"
  );
}

export function getINPItemsBySupply(item) {
  return myApi.getByNavItem(
    item,
    "NavProductionScheduling/GetINPItemsBySupply"
  );
}

export function getINPCalendarView(item) {
  return myApi.getItem(item, "NavProductionScheduling/GetINPCalendarView");
}
export function postProdOrder(data) {
  return myApi.update(
    data.orderRequirementId,
    data,
    "Simulation/PostProdOrder"
  );
}

export function getcustomerAC(item) {
  return myApi.getByNavItem(item, "NavProductionScheduling/getcustomerAC");
}
export function getINPACInfoView(data) {
  return myApi.getItem(data, "NavProductionScheduling/GetINPACInfoView");
}
export function getProductionSchedulingFilter(item) {
  return myApi.getItem(
    item,
    "NavProductionScheduling/GetProductionSchedulingFilter"
  );
}

export function getPlannerCommentsFilter(item) {
  return myApi.getItem(
    item,
    "NavProductionScheduling/GetPlannerCommentsFilter"
  );
}

export function updatePlannerComment(item) {
  return myApi.update(
    item.inpreportId,
    item,
    "NavProductionScheduling/UpdatePlannerComment"
  );
}

export function getINPCalendarPivotView(item) {
  return myApi.getItem(item, "NavProductionScheduling/GetINPCalendarPivotView");
}
export function getSimulationPivotView(item) {
  return myApi.getItem(item, "Simulation/GetINPCalendarPivotView");
}
export function getSimulationaddhoc(item) {
  return myApi.getItem(item, "Simulation/GetSimulationAddhoc");
}
export function getSimulationMidMonth(item) {
  return myApi.getItem(item, "Simulation/GetSimulationMidMonth");
}
export function getNavItemById(item) {
  return myApi.getItem(item, "NavItem/GetNavItemById");
}

export function getPsbReport(item) {
  return myApi.getItem(item, "Simulation/GetPSB");
}
export function getPSBReportV2(item) {
  return myApi.getItem(item, "Simulation/GetPSBReportV2");
}
export function getPARReport(item) {
  return myApi.getItem(item, "Simulation/GetPARReport");
}
export function getProductionBatchSize(id) {
  return myApi.getByID(id, "NavProductionScheduling/GetProductionBatchSize");
}

export function getNAVLocationByItem(id) {
  return myApi.getByID(id, "NavLocation/GetNAVLocationByItem");
}

export function getShipment(item) {
  return myApi.getItem(item, "NAVPostedShip/GetShipment");
}
export function updateReceive(data) {
  return myApi.update(data.shipmentId, data, "NAVPostedShip/UpdateShipment");
}

export function updateACReportData(data) {
  return myApi.update(data.itemId, data, "Simulation/UpdateACReportData");
}

export function getPortalCustomers() {
  return myApi.getAll("NavCustomer/GetNavCustomers");
}
export function getPlants() {
  return myApi.getAll("Plant/GetMobilePlants");
}

export function getNavItemsByGenericID(id) {
  return myApi.getByID(id, "NavItem/GetNavItemsByGenericID");
}
export function createVersion(data) {
  return myApi.create(data, "Simulation/SavePSB");
}

export function savePSBComment(data) {
  return myApi.create(data, "Simulation/SavePSBComment");
}
export function getDescriptionByCountryItems(id) {
  return myApi.getByID(id, "NavItem/GetDescriptionByCountryItems");
}
export function getNavItemsList() {
  return myApi.getAll("NavItem/GetNavItemsList");
}

export function syncPARReport(item) {
  return myApi.getItem(item, "NAVFunc/GetPARReport");
}

export function getGroupPlan(item) {
  return myApi.getItem(item, "Simulation/GetGroupPlan");
}
export function getGroupPlanRefresh(item) {
  return myApi.getItem(item, "Simulation/GetGroupPlanRefresh");
}
export function getGroupPlanRefreshV1(item) {
  return myApi.getItem(item, "Simulation/GetGroupPlanRefreshV1");
}
export function saveProductionSimulationGroupingTicket(data) {
  return myApi.create(
    data,
    "Simulation/InsertProductionSimulationGroupingTicket"
  );
}
export function getSimulationAddhocV2(item) {
  return myApi.getItem(item, "Simulation/GetSimulationAddhocV2");
}
export function getSimulationForecastPlanning(data) {
  return myApi.getItem(data, "Simulation/GetSimulationForecastPlanning");
}
export function getNavItemsListByMethodCode(id) {
  return myApi.getByID(id, "NavItem/GetNavItemsListByMethodCode");
}

export function getForecastProductionSimulation(item) {
  return myApi.getItem(item, "Simulation/GetGroupPlanSimulationProduction");
}

export function getSimulationAddhocAlps(item) {
  return myApi.getItem(item, "Simulation/GetSimulationAddhocAlps");
}
export function getSimulationAddhocV3(item) {
  return myApi.getItem(item, "Simulation/GetSimulationAddhocV3");
}
export function saveGroupingTicketPlanning(data) {
  return myApi.create(data, "Simulation/InsertGroupingTicketPlanning");
}

export function getSimulationAddhocRefreshV3(item) {
  return myApi.getItem(item, "Simulation/GetSimulationAddhocRefreshV3");
}

export function getSimulationGroupV3(item) {
  return myApi.getItem(item, "Simulation/GetSimulationGroupV3");
}

export function proposedItemStock(data) {
  return myApi.create(data, "Simulation/ProposedItemStock");
}

export function getTicketAllocations(item) {
  return myApi.getItem(item, "Simulation/GetTicketAllocations");
}
export function getProposedNoOfTicketsByVersionNo(item) {
  return myApi.getByType(item, "Simulation/GetProposedNoOfTicketsByVersionNo");
}
export function PostNavNoOfTickets(item) {
  return myApi.getItem(item, "Simulation/PostNavNoOfTickets");
}
//ProposedItemStock
