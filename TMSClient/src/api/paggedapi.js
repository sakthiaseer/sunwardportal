import myApi from '@/util/api'

export function getData(data) {

    return myApi.getItem(data, "PagedList/GetData")
}