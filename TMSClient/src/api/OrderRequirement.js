import myApi from '@/util/api'


export function createOrderRequirement (data) {
    return myApi.create(data, "OrderRequirement/InsertOrderRequirement")
  }
  export function getOrderRequirementID(data) {

    return myApi.getItem(data, "OrderRequirement/GetOrderRequirementID")
  }
  export function updateOrderRequirement(data) {

    return myApi.update(data.orderRequirementId,data, "OrderRequirement/UpdateOrderRequirement")
  }

  export function getOrderRequirementLine (orderRequirementId) {
    return myApi.getAll('OrderRequirement/GetOrderRequirementLine/?id='+orderRequirementId)
  }
export function createOrderRequirementLine(data) {
    return myApi.create(data, "OrderRequirement/InsertOrderRequirementLine")
  }
  export function updateOrderRequirementLine(data) {

    return myApi.update(data.orderRequirementLineId, data, "OrderRequirement/UpdateOrderRequirementLine")
  }
  export function deleteOrderRequirementLine(data) {

    return myApi.delete(data.orderRequirementLineId, 'OrderRequirement/DeleteOrderRequirementLine')
  }



  export function getOrderRequirementLineSplit (orderRequirementLineId) {
    return myApi.getAll('OrderRequirement/GetOrderRequirementLineSplit/?id='+orderRequirementLineId)
  }
export function createOrderRequirementLineSplit(data) {
    return myApi.create(data, "OrderRequirement/InsertOrderRequirementLineSplit")
  }
  export function updateOrderRequirementLineSplit(data) {

    return myApi.update(data.orderRequirementLineSplitId, data, "OrderRequirement/UpdateOrderRequirementLineSplit")
  }
  export function deleteOrderRequirementLineSplit(data) {

    return myApi.delete(data.orderRequirementLineSplitId, 'OrderRequirement/DeleteOrderRequirementLineSplit')
  }
  export function getNavSaleCategoryByID(id) {  
    return myApi.getByID(id, "NavSaleCategory/GetNavSaleCategoryByID")
}
  
  