import myApi from '@/util/api'

export function createSampleRequestFormLine (data) {
    return myApi.create(data, "SampleRequestForm/InsertSampleRequestFormLine")
}

export function updateSampleRequestFormLine (data) {

    return myApi.update(data.sampleRequestFormLineId, data, "SampleRequestForm/UpdateSampleRequestFormLine")
}
export function updateSampleRequestFormLineByRequest (data) {

    return myApi.update(data.sampleRequestFormLineId, data, "SampleRequestForm/UpdateSampleRequestFormLineByRequest")
}
export function deleteSampleRequestFormLine (data) {

    return myApi.delete(data.sampleRequestFormLineId, 'SampleRequestForm/DeleteSampleRequestFormLine')
}
export function getSampleRequestFormLinesById (id) {

    return myApi.getByID(id, 'SampleRequestForm/GetSampleRequestFormLinesById')
}
export function getItemBatchInfoByBatchNo (id) {

    return myApi.getByID(id, 'ItemBatchInfo/GetItemBatchInfoByBatchNo')
}
export function getNavItemForSampleRequestform(id) {

    return myApi.getByID(id,'NavItem/GetNavItemForSampleRequestform')
}

export function createPortalNavItem(data) {

    return myApi.create(data,'InventoryType/InsertPortalNavItem')
}
export function deleteIssueRequestSampleLine (data) {

    return myApi.delete(data.issueRequestSampleLineId, 'SampleRequestForm/DeleteIssueRequestSampleLine')
}
export function getIssueLinesById(id) {

    return myApi.getByID(id,'SampleRequestForm/GetIssueLinesById')
}
export function getNavItemForSampleRequestDoctors(id) {

    return myApi.getByID(id,'NavItem/GetNavItemForSampleRequestDoctors')
}