import myApi from '@/util/api'

export function getTenderOrder() {
    return myApi.getAll('TenderOrder/GetTenderOrder')
}

export function updateTenderOrder(data) {

    return myApi.update(data.simualtionAddhocId, data, "TenderOrder/UpdateTenderOrder")
}
export function deleteTenderOrder(data) {

    return myApi.delete(data.simualtionAddhocID, 'TenderOrder/DeleteTenderOrder')
}
export function uploadDocuments(data) {

    return myApi.uploadFile(data, "TenderOrder/UploadDocuments");
}
