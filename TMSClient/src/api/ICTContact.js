import myApi from '@/util/api'

export function getICTContactDetails() {

    return myApi.getAll('ICTContactDetails/GetICTContactDetails')
}
export function getICTContactDetail(data) {

    return myApi.getItem(data,"ICTContactDetails/GetData")
}

export function createICTContactDetails(data) {
    return myApi.create(data, "ICTContactDetails/InsertICTContactDetails")
}

export function updateICTContactDetails(data) {

    return myApi.update(data.contactDetailsID, data, "ICTContactDetails/UpdateICTContactDetails")
}
export function deleteICTContactDetails(data) {

    return myApi.delete(data.contactDetailsID, 'ICTContactDetails/DeleteICTContactDetails')
}
