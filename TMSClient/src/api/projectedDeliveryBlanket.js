import myApi from '@/util/api'

export function getProjectedDeliverys(id) {

    return myApi.getByID(id,'ProjectedDeliveryBlanketOthers/GetProjectedDeliverys')
}
export function getProjectedDelivery(data) {

    return myApi.getItem(data, "ProjectedDeliveryBlanketOthers/GetData")
}
export function createProjectedDelivery(data) {
    return myApi.create(data, "ProjectedDeliveryBlanketOthers/InsertProjectedDelivery")
}

export function updateProjectedDelivery(data) {

    return myApi.update(data.projectedDeliverySalesOrderLineID, data, "ProjectedDeliveryBlanketOthers/UpdateProjectedDelivery")
}
export function deleteProjectedDelivery(data) {

    return myApi.delete(data.projectedDeliverySalesOrderLineID, 'ProjectedDeliveryBlanketOthers/DeleteProjectedDelivery')
}
