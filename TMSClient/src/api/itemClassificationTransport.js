import myApi from '@/util/api'

export function getItemClassificationTransports () {

    return myApi.getAll('ItemClassificationTransport/GetItemClassificationTransports')
}
export function getItemClassificationTransportsByRefNo (id) {

    return myApi.getByID(id, 'ItemClassificationTransport/GetItemClassificationTransportsByRefNo')
}
export function createItemClassificationTransports (data) {
    return myApi.create(data, "ItemClassificationTransport/InsertItemClassificationTransport")
}

export function updateItemClassificationTransports (data) {

    return myApi.update(data.transportId, data, "ItemClassificationTransport/UpdateItemClassificationTransport")
}
export function deleteItemClassificationTransports (data) {

    return myApi.delete(data.transportId, 'ItemClassificationTransport/DeleteItemClassificationTransport')
}