import myApi from '@/util/api'

export function getEmails () {

    return myApi.getAll('PortalEmails/GetEmails')
}
export function getEmailsAttachment (id) {

    return myApi.getByID(id,'PortalEmails/GetEmailsAttachment')
}