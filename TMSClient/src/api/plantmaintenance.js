import myApi from '@/util/api'

export function getICTMasters() {

    return myApi.getAll('ICTMaster/GetICTMasters')
  }
  export function getICTMasterByID(id) {
  
    return myApi.getByID(id, "ICTMaster/GetICTMaster")
  }
  export function getPlants() {

    return myApi.getAll('Plant/GetPlants')
  }
  export function getPlantMaintenanceEntrys() {

    return myApi.getAll('PlantMaintenanceEntry/GetPlantMaintenanceEntrys')
  }
  export function getPlantMaintenanceEntryLines() {

    return myApi.getAll('PlantMaintenanceEntryLine/GetPlantMaintenanceEntryLines')
  }
  export function getPlantMaintenanceEntryLine(data) {

    return myApi.getItem(data, "PlantMaintenanceEntryLine/GetData")
}
export function getPlantMaintenanceEntryLineByID (userId) {

  return myApi.getByID(userId, 'PlantMaintenanceEntryLine/GetPlantMaintenanceEntryLineByID')
}
export function updatePlantMaintenanceEntryLines(data) {

  return myApi.update(data.plantEntryLineID, data, "PlantMaintenanceEntryLine/UpdatePlantMaintenance")
}
export function updatePlantMaintenanceLinesMaster(data) {

  return myApi.update(data.plantEntryLineID, data, "PlantMaintenanceEntryLine/UpdatePlantMaintenanceLine")
}
export function getPlantMaintenanceEntryFilter(data) {

  return myApi.getItem(data, "PlantMaintenanceEntry/GetPlantMaintenanceEntryFilter")
}
  
