import myApi from '@/util/api'

export function getDocumentRoles() {

  return myApi.getAll('DocumentRole/GetDocumentRoles')
}
export function getDocumentRole(data) {

  return myApi.getItem(data, "DocumentRole/GetData")
}

export function createDocumentRole(data) {
  return myApi.create(data, "DocumentRole/InsertDocumentRole")
}

export function updateDocumentRole(data) {

  return myApi.update(data.documentRoleID, data, "DocumentRole/UpdateDocumentRole")
}
export function deleteDocumentRole(data) {

  return myApi.delete(data.documentRoleID, 'DocumentRole/DeleteDocumentRole')
}