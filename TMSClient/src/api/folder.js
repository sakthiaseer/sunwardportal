import myApi from '@/util/api'

export function getFolders() {

  return myApi.getAll('Folders/GetFolders')
}
export function getPublicFolders(userId) {

  return myApi.getByID(userId, 'Folders/GetPublicFolders')
}
export function getParentPublicFolders(userId) {
  return myApi.getByID(userId, 'Folders/GetParentPublicFolders')
}

export function getInActivePublicFolders(userId) {

  return myApi.getByID(userId, 'Folders/GetInActivePublicFolders')
}
export function getPersonalFolders(userId) {

  return myApi.getByID(userId, 'Folders/GetPersonalFolders')
}
export function getFoldersbyID(id) {

  return myApi.getByID(id, 'Folders/GetFoldersbyID')
}
export function getFolderDetail(id) {

  return myApi.getByID(id, 'Folders/GetFolderDetail')
}
export function getuserFolderTreeId(id, userId) {

  return myApi.getByuserId(id, userId, 'Folders/GetSubNestedUserFolders')
}
export function getuserTreefolderPermission(id, userId) {

  return myApi.getByuserId(id, userId, 'Folders/GetuserTreefolderPermission')
}
export function getSubFolders() {

  return myApi.getAll('Folders/GetSubFolders')
}
export function getFolderTypes() {

  return myApi.getAll('Folders/GetCodeMaster')
}
export function getSubFoldersId(id, userId) {

  return myApi.getByuserId(id, userId, 'Folders/GetSubFoldersId')
}
export function getFolderTreeId(id, userId) {

  return myApi.getByuserId(id, userId, 'Folders/GetSubNestedFoldersId')
}
export function getApplicationUsers() {

  return myApi.getAll('ApplicationUser/GetApplicationUsers')
}
export function getUserGroups() {

  return myApi.getAll('UserGroup/GetUserGroups')
}
export function getUsersById(id) {

  return myApi.getByID(id, "ApplicationUser/GetuserId")
}
export function getPublicFoldersByID(id) {

  return myApi.getByID(id, "Documents/GetPublicFolders")
}
// export function getSubFolderId(id, userId) {

//   return myApi.getByuserId(id, userId, 'Folders/GetSubFolderId')
// }
// export function getSubNestedFoldersId(id) {

//   return myApi.getByID(id, 'Folders/GetSubNestedFoldersId')
// }
export function getFolder(data) {

  return myApi.getItem(data, "Folders/GetData")
}

export function createFolders(data) {
  return myApi.create(data, "Folders/InsertFolders")
}
export function createDocumentUserRole(data) {
  return myApi.create(data, "Folders/InsertDocumentUserRole")
}
export function updateFolders(data) {

  return myApi.update(data.folderID, data, "Folders/UpdateFolders")
}
export function createSubFolder(data) {
  // console.log(data);
  return myApi.create(data, "Folders/InsertSubFolder")
}

export function updateSubFolder(data) {

  return myApi.update(data.folderID, data, "Folders/UpdateSubFolder")
}
export function deleteFolders(data) {

  return myApi.delete(data.folderID, 'Folders/DeleteFolders')
}
export function getFolderFiles(id, userId) {

  return myApi.getByuserId(id, userId, 'Folders/GetFolderFiles')
}
export function getDocumentFolder(id) {

  return myApi.getByID(id, 'DocumentFolder/GetDocumentFolders')
}
export function checkOutDoc(data) {

  return myApi.update(data.documentFolderID, data, 'Folders/CheckOutDoc')
}
export function checkInDoc(data) {

  return myApi.update(data.documentFolderId, data, 'Folders/CheckInDoc')
}
export function downloadFile(data) {

  return myApi.downLoadDocument(data, "Folders/DownloadDocument")
}
export function setDocAccess(data) {

  return myApi.update(data.documentFolderID, data, 'Folders/SetDocAccess')
}
export function setDocInvitation(data) {

  return myApi.update(data.documentFolderID, data, 'Folders/SetDocInvitation')
}
export function getFilesHistory(id) {

  return myApi.getByID(id, 'Folders/GetFilesHistory');
}
export function updateRenameFileName(data) {

  return myApi.update(data.documentID, data, 'Folders/UpdateRenameFileName');
}
export function updateDocumentDescription(data) {

  return myApi.update(data.documentID, data, 'Folders/UpdateDocumentDescription');
}
export function getAccessControl(id) {

  return myApi.getByID(id, 'Folders/GetAccessControl');
}
export function getInvitationControl(id) {

  return myApi.getByID(id, 'Folders/GetInvitationControl');
}
export function updateMoveFolder(data) {

  return myApi.update(data.documentFolderID, data, 'DocumentFolder/UpdateDocumentFolder');
}
export function getMovePublicFolders(id) {

  return myApi.getByID(id, 'Folders/GetMovePublicFolders')
}
export function getDocumentsByFolderID(id,userId) {

  return myApi.getByuserId(id,userId, 'Folders/GetDocumentsByFolderID')
}

export function getMovePersonalFolders() {

  return myApi.getAll('Folders/GetMovePersonalFolders')
}
export function getMoveDocumentFolderID(data) {

  return myApi.getItem(data, 'Folders/GetMoveDocumentFolderID');
}
export function getUsers(data) {

  return myApi.getItem(data, 'ApplicationUser/GetUsers')
}
export function getAlertsByID(id, userId) {

  return myApi.getByuserId(id, userId, 'FolderDiscussion/GetFolderDiscussions')
}
export function getPublicFolderPath(id, userId) {

  return myApi.getByuserId(id, userId, 'Folders/GetPublicFolderPath')
}
export function updateDescriptionField(data) {

  return myApi.update(data.documentID, data, 'Folders/UpdateDescriptionField');
}
export function dragDropFolder(data) {

  return myApi.update(data.folderID, data, "Folders/DragDropFolder");
}
export function getDocumentPermissionControl(userId) {

  return myApi.getByID(userId, 'Folders/GetDocumentPermissionControl');
}
export function getDocumentUserRoleByFolderID(id) {

  return myApi.getByID(id, 'Folders/GetDocumentUserRoleByFolderID');
}
export function getEditFolderByFolderID(id) {

  return myApi.getByID(id, 'Folders/GetEditFolderByFolderID');
}
export function getDocumentUserRoleByFolderIDDocumentID(id, userId) {

  return myApi.getByuserId(id, userId, 'Folders/GetDocumentUserRoleByFolderIDDocumentID');
}
export function getUserFolderRole(id, userId) {

  return myApi.getByuserId(id, userId, 'Folders/GetUserFolderRole');
}
export function getFileUploadByFolderID(id) {

  return myApi.getByID(id, 'Folders/GetFileUploadByFolderID');
}
export function getDocumentUserRole(id) {

  return myApi.getByID(id, 'Folders/GetDocumentUserRole');
}
export function updateDocumentUserRole(data) {

  return myApi.update(data.folderID, data, 'Folders/UpdateDocumentUserRole');
}

export function getShowonlyOrPermissionFolders(data) {
  return myApi.getItem(data, 'Folders/GetShowonlyOrPermissionFolders')
}
export function getPublicFolderMainPath(data) {
  return myApi.getByID(data, 'Folders/GetPublicFolderMainPath')
}
export function getFolderLists(id, userId) {

  return myApi.getByuserId(id, userId, 'Folders/GetFolderLists')
}

export function getPublicFoldersTreeItems(id) {

  return myApi.getByID(id, 'Folders/GetPublicFoldersTreeItems')
}
export function getFoldersDropDown() {

  return myApi.getAll('Folders/GetFoldersDropDown')
}
export function getPublicFolderPathAll(userId) {

  return myApi.getByID(userId, 'Folders/GetPublicFolderPathAll')
}
export function getPublicFolderMainPathAll(userId) {

  return myApi.getItem(userId, 'Folders/GetPublicFolderMainPathAll')
}
export function checkfolderIds(id, userId) {

  return myApi.getByuserId(id, userId, 'Folders/CheckfolderIds')
}
export function saveFolderImage(data) {

  return myApi.downLoadProfileDocument(data, "Folders/saveFolderImage")
}
export function getExcel(data) {

  return myApi.downLoadProfileDocument(data, 'Folders/GetExcel')
}