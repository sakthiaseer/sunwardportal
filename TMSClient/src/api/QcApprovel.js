import myApi from '@/util/api'

export function getQcApproval () {

    return myApi.getAll('QcApproval/GetQCApprovals')
}
export function getDatas (data) {

    return myApi.getItem(data, "QcApproval/GetData")
}
export function createQcApproval (data) {
    return myApi.create(data, "QcApproval/InsertQcApproval")
}
export function updateQcApproval (data) {

    return myApi.update(data.qcapprovalId, data, "QcApproval/UpdateQcApproval")
}
export function deleteQcApproval (data) {

    return myApi.delete(data.qcapprovalId, 'QcApproval/DeleteQcApproval')
}

export function getQCApprovalLine (id) {

    return myApi.getByID(id,'QcApproval/GetQCApprovalsLine')
}
export function updateQCApprovalLine (data) {

    return myApi.create(data, "QcApproval/updateQCApprovalLine")
}
export function deleteQCApprovalLine (data) {

    return myApi.delete(data.qcapprovalLineId, 'QcApproval/DeleteQcApprovalLine')
}