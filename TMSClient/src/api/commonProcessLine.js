import myApi from '@/util/api'
export function getCommonProcessLine(id) {

    return myApi.getByID(id, "CommonProcess/GetCommonProcessLine")
}
export function createCommonProcessLine(data) {
    return myApi.create(data, "CommonProcess/InsertCommonProcessLine")
}

export function updateCommonProcessLine(data) {

    return myApi.update(data.commonProcessLineId, data, "CommonProcess/UpdateCommonProcessLine")
}
export function deleteCommonProcessLine(data) {

    return myApi.delete(data.commonProcessLineId, 'CommonProcess/DeleteCommonProcessLine')
}