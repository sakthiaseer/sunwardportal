import myApi from '@/util/api'



export function getManpowerInformation(data) {

    return myApi.getItem(data, "ManpowerInformation/GetData")
}
export function getManpowerInformations(id){
    return myApi.getByID(id,"ManpowerInformation/GetManpowerInformations")
}
export function createManpowerInformation(data) {
    return myApi.create(data, "ManpowerInformation/InsertManpowerInformation")
}

export function updateManpowerInformation(data) {

    return myApi.update(data.ManpowerInformationID, data, "ManpowerInformation/UpdateManpowerInformation")
}
export function deleteManpowerInformation(data) {

    return myApi.delete(data.ManpowerInformationID, 'ManpowerInformation/DeleteManpowerInformation')
}

