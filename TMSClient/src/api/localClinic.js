import myApi from '@/util/api'
export function getLocalClinic () {

    return myApi.getAll('LocalClinic/GetLocalClinic')
}
export function getDataList (data) {

    return myApi.getItem(data, "LocalClinic/GetData")
}
export function createLocalClinic (data) {
    return myApi.create(data, "LocalClinic/InsertLocalClinic")
}

export function updateLocalClinic (data) {

    return myApi.update(data.localClinicId, data, "LocalClinic/UpdateLocalClinic")
}
export function deleteLocalClinic (data) {

    return myApi.delete(data.localClinicId, 'LocalClinic/DeleteLocalClinic')
}