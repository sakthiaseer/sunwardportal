import myApi from '@/util/api'

export function getCommonPackagingDivider (refNo) {

  return myApi.getByRefNo(refNo,'CommonPackagingDivider/GetCommonPackagingDivider')
}
export function getData (data) {

  return myApi.getItem(data, "CommonPackagingDivider/GetData")
}
export function createCommonPackagingDivider (data) {
  return myApi.create(data, "CommonPackagingDivider/InsertCommonPackagingDivider")
}
export function updateCommonPackagingDivider (data) {

  return myApi.update(data.dividerSpecificationId, data, "CommonPackagingDivider/UpdateCommonPackagingDivider")
}
export function deleteCommonPackagingDivider (data) {

  return myApi.delete(data.dividerSpecificationId, 'CommonPackagingDivider/DeleteCommonPackagingDivider')
}
export function getCommonPackagingDividerByRefNo(data) {
  return myApi.getItems(data, 'CommonPackagingDivider/GetCommonPackagingDividerByRefNo')
}