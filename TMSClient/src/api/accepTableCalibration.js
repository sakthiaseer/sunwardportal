import myApi from '@/util/api'

export function getAcceptableCalibrationInformations() {

    return myApi.getAll('AcceptableCalibrationInformation/GetAcceptableCalibrationInformations')
}
export function getAcceptableCalibrationInformation(data) {

    return myApi.getItem(data, "AcceptableCalibrationInformation/GetData")
}

export function createAcceptableCalibrationInformation(data) {
    return myApi.create(data, "AcceptableCalibrationInformation/InsertAcceptableCalibrationInformation")
}

export function updateAcceptableCalibrationInformation(data) {

    return myApi.update(data.acceptableCalibrationInformationID, data, "AcceptableCalibrationInformation/UpdateAcceptableCalibrationInformation")
}
export function deleteAcceptableCalibrationInformation(data) {

    return myApi.delete(data.acceptableCalibrationInformationID, 'AcceptableCalibrationInformation/DeleteAcceptableCalibrationInformation')
}
