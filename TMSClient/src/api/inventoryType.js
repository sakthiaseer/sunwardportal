import myApi from '@/util/api'

export function getInventoryType() {

    return myApi.getAll('InventoryType/GetInventoryType')
}
export function getInventoryTypeData(data) {

    return myApi.getItem(data, "InventoryType/GetInventoryTypeData")
}
export function createInventoryType(data) {
    return myApi.create(data, "InventoryType/InsertInventoryType")
}

export function updateInventoryType(data) {

    return myApi.update(data.inventoryTypeId, data, "InventoryType/UpdateInventoryType")
}
export function deleteInventoryType(data) {

    return myApi.delete(data.inventoryTypeId, 'InventoryType/DeleteInventoryType')
}
export function getItemClassificationTreePaths() {

    return myApi.getAll('ItemClassification/GetItemClassificationTreePaths')
}

export function getInventoryTypeDropDown() {

    return myApi.getAll('InventoryType/GetInventoryTypeDropDown')
}
export function getInventoryTypeForMovedStock() {

    return myApi.getAll('InventoryType/GetInventoryTypeForMovedStock')
}
export function getInventoryTypeByItem(id) {

    return myApi.getByID(id, 'InventoryType/GetInventoryTypeByItem')
}
export function getItemStockInfoByInventoryType(id) {

    return myApi.getByID(id, 'InventoryType/GetItemStockInfoByInventoryType')
}
export function getStockMovement(data) {

    return myApi.getItem(data, 'InventoryType/GetStockMovement')
}
export function getInventoryTypeDisposalItem() {

    return myApi.getAll('InventoryType/GetInventoryTypeDisposalItem')
}