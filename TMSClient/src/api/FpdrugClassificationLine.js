import myApi from '@/util/api'
export function getFpdrugClassificationLineByID (id) {

    return myApi.getByID(id, 'FpdrugClassificationLine/GetFpdrugClassificationLineByID')
}
export function getFpdrugClassificationLine (data) {

    return myApi.getItem(data, "FpdrugClassificationLine/GetData")
}
export function createFpdrugClassificationLine (data) {
    return myApi.create(data, "FpdrugClassificationLine/InsertFpdrugClassificationLine")
}

export function updateFpdrugClassificationLine (data) {

    return myApi.update(data.fpdrugClassificationLineId, data, "FpdrugClassificationLine/UpdateFpdrugClassificationLine")
}
export function deleteFpdrugClassificationLine (data) {

    return myApi.delete(data.fpdrugClassificationLineId, 'FpdrugClassificationLine/DeleteFpdrugClassificationLine')
}