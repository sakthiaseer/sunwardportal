import myApi from '@/util/api'

export function getSubSectionsTwo() {

    return myApi.getAll('SubSectionTwo/GetSubSectionsTwo')
}
export function getSubSectionTwo(data) {

    return myApi.getItem(data, "SubSectionTwo/GetData")
}
export function createSubSectionTwo(data) {
    return myApi.create(data, "SubSectionTwo/InsertSubSectionTwo")
}

export function updateSubSectionTwo(data) {

    return myApi.update(data.subSectionTID, data, "SubSectionTwo/UpdateSubSectionTwo")
}
export function deleteSubSectionTwo(data) {

    return myApi.delete(data.subSectionTID, 'SubSectionTwo/DeleteSubSectionTwo')
}
export function getSubSections() {

    return myApi.getAll('SubSection/GetSubSections')
}