import myApi from '@/util/api'

export function getWorkOrders(id) {

    return myApi.getByID(id,'WorkOrder/GetWorkOrders')
}
export function getSearchData(data) {

    return myApi.getItem(data, "WorkOrder/GetData")
}
export function createWorkOrder(data) {
    return myApi.create(data, "WorkOrder/InsertWorkOrder")
}
export function updateWorkOrder(data) {

    return myApi.update(data.workOrderId, data, "WorkOrder/UpdateWorkOrder")
}
export function deleteWorkOrder(data) {

    return myApi.delete(data.workOrderId, 'WorkOrder/DeleteWorkOrder')
}
export function getWorkOrderBySearch(data) {

    return myApi.getItem(data, "WorkOrder/GetWorkOrderBySearch")
}
export function getWorkOrderReportBySearch(data) {

    return myApi.getItem(data, "WorkOrder/GetWorkOrderReportBySearch")
}
export function getWorkOrderLines(id) {

    return myApi.getByID(id,'WorkOrder/GetWorkOrderLines')
}

export function createWorkOrderLine(data) {
    return myApi.create(data, "WorkOrder/InsertWorkOrderLine")
}
export function updateWorkOrderLine(data) {

    return myApi.update(data.workOrderLineId, data, "WorkOrder/UpdateWorkOrderLine")
}
export function deleteWorkOrderLine(data) {

    return myApi.delete(data.workOrderLineId, 'WorkOrder/DeleteWorkOrderLine')
}

export function getWorkOrderReport(id) {

    return myApi.getByID(id,'WorkOrder/GetWorkOrderReport')
}

export function getWorkOrderTypes() {

    return myApi.getAll('WorkOrder/GetWorkOrderTypes')
}

export function getApplicationPermissionPaths(id) {

    return myApi.getByID(id,'WorkOrder/GetApplicationPermissionPaths')
}

export function getWorkOrderReportById(id) {

    return myApi.getByID(id,'WorkOrder/GetWorkOrderReportById')
}

