import myApi from '@/util/api'

export function getItemCosts() {

    return myApi.getAll('ItemCost/GetItemCosts')
}
export function getItemCost(data) {

    return myApi.getItem(data, "ItemCost/GetData")
}
export function createItemCost(data) {
    return myApi.create(data, "ItemCost/InsertItemCost")
}

export function updateItemCost(data) {

    return myApi.update(data.itemCostId, data, "ItemCost/UpdateItemCost")
}
export function updateItemCostVersion(data) {

    return myApi.update(data.itemCostId, data, "ItemCost/updateItemCostVersion")
}
export function deleteItemCost(data) {

    return myApi.delete(data.itemCostId, 'ItemCost/DeleteItemCost')
}
export function geItemCostLinesById(id) {

    return myApi.getByID(id, 'ItemCost/GeItemCostLinesById')
}
export function createItemCostLine(data) {
    return myApi.create(data, "ItemCost/InsertItemCostLine")
}

export function updateItemCostLine(data) {

    return myApi.update(data.itemCostLineId, data, "ItemCost/updateItemCostLine")
}
export function deleteItemCostLine(data) {

    return myApi.delete(data.itemCostLineId, 'ItemCost/DeleteItemCostLine')
}
export function createVersion(data) {

    return myApi.update(data.itemCostId, data, "ItemCost/CreateVersion")
}
export function applyVersion(data) {

    return myApi.update(data.itemCostId, data, "ItemCost/ApplyVersion")
}
export function tempVersion(data) {

    return myApi.update(data.itemCostId, data, "ItemCost/TempVersion")
}
export function undoVersion(id) {

    return myApi.getByID(id, "ItemCost/UndoVersion")
}
export function deleteVersion(id) {

    return myApi.delete(id, 'ItemCost/DeleteVersion')
}

export function syncWithNavisionUpdateCost(data) {

    return myApi.update(data.itemCostId, data, "ItemCost/syncWithNavisionUpdateCost")
}