import myApi from '@/util/api'

export function getPurchaseOrder() {

    return myApi.getAll('PurchaseOrder/GetPurchaseOrder')
}
export function getDataLinesData(data) {

    return myApi.getItem(data, "PurchaseOrder/GetData")
}
export function createPurchaseOrder(data) {
    return myApi.create(data, "PurchaseOrder/InsertPurchaseOrder")
}
export function updatePurchaseOrder(data) {

    return myApi.update(data.purchaseOrderId, data, "PurchaseOrder/UpdatePurchaseOrder")
}
export function deletePurchaseOrder(data) {

    return myApi.delete(data.purchaseOrderId, 'PurchaseOrder/DeletePurchaseOrder')
}


export function uploadPurchaseOrderDocuments(data, sessionId) {

    return myApi.uploadFile(data, "PurchaseOrder/UploadPurchaseOrderDocuments?sessionId=" + sessionId)
}
export function getPurchaseOrderDocuments(id) {

    return myApi.getByID(id, "PurchaseOrder/GetPurchaseOrderDocument")
}
export function deletePurchaseOrderDocuments(PurchaseOrderDocumentId) {

    return myApi.delete(PurchaseOrderDocumentId, 'PurchaseOrder/DeletePurchaseOrderDocument')
}
export function downloadPurchaseOrderDocuments(data) {

    return myApi.downLoadDocument(data, "PurchaseOrder/DownLoadPurchaseOrderDocument")
}