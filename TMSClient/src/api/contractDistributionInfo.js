import myApi from '@/util/api'

export function getContractDistributionItems(id) {

    return myApi.getByID(id, 'ContractDistributionSalesEntryLine/GetContractDistributionItems')
}
export function getContractDistributionData(data) {

    return myApi.getItem(data, "ContractDistributionSalesEntryLine/GetContractDistributionData")
}

export function createContractDistribution(data) {
    return myApi.create(data, "ContractDistributionSalesEntryLine/InsertContractDistribution")
}

export function updateContractDistribution(data) {

    return myApi.update(data.contractDistributionSalesEntryLineID, data, "ContractDistributionSalesEntryLine/UpdateContractDistribution")
}
export function deleteContractDistribution(data) {

    return myApi.delete(data.contractDistributionSalesEntryLineID, 'ContractDistributionSalesEntryLine/DeleteContractDistribution')
}

export function uploadContractDistributionDocuments(data, sessionId) {

    return myApi.uploadFile(data, "ContractDistributionSalesEntryLine/UploadContractDistributionDocuments?sessionId=" + sessionId)
}

export function downLoadContractDistributionDocument(data) {

    return myApi.downLoadDocument(data, "ContractDistributionSalesEntryLine/DownLoadContractDistributionDocument")
}


export function getContractDistributionDocument(sessionId) {

    return myApi.getAll("ContractDistributionSalesEntryLine/GetContractDistributionDocument?sessionId=" + sessionId)
}
export function deleteContractDistributionDocuments(id) {

    return myApi.delete(id, 'ContractDistributionSalesEntryLine/DeleteContractDistributionDocuments')
}

export function getCompanyListingsForDistributionInformation() {

    return myApi.getAll('CompanyListing/GetCompanyListingsForDistributionInformation')
}
export function getSoBycustomersTender() {

    return myApi.getAll('SobyCustomers/GetSoBycustomersTender')
}
export function getSoBycustomersTenderAgency(id) {

    return myApi.getByID(id,'SobyCustomers/GetSoBycustomersTenderAgency')
}
export function getSoBycustomersForSalesNovate(id) {

    return myApi.getByID(id, 'SobyCustomers/GetSoBycustomersForSalesNovate')
}
export function getDistributorCustomerNameBySalesEntryId(id) {

    return myApi.getByID(id, 'CompanyListing/GetDistributorCustomerNameBySalesEntryId')
}
export function getDistributionTenderQty(data) {

    return myApi.getItem(data, "ContractDistributionSalesEntryLine/GetDistributionTenderQty")
}