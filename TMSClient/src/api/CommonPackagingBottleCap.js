import myApi from '@/util/api'

export function getCommonPackagingBottleCap (refNo) {

  return myApi.getByRefNo(refNo,'CommonPackagingBottleCap/GetCommonPackagingBottleCap')
}
export function getData (data) {

  return myApi.getItem(data, "CommonPackagingBottleCap/GetData")
}
export function createCommonPackagingBottleCap (data) {
  return myApi.create(data, "CommonPackagingBottleCap/InsertCommonPackagingBottleCap")
}
export function updateCommonPackagingBottleCap (data) {

  return myApi.update(data.bottleCapSpecificationId, data, "CommonPackagingBottleCap/UpdateCommonPackagingBottleCap")
}
export function deleteCommonPackagingBottleCap (data) {

  return myApi.delete(data.bottleCapSpecificationId, 'CommonPackagingBottleCap/DeleteCommonPackagingBottleCap')
}
export function getCommonPackagingBottleCapByRefNo(data) {
  return myApi.getItems(data, 'CommonPackagingBottleCap/GetCommonPackagingBottleCapByRefNo')
}