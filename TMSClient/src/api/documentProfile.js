import myApi from '@/util/api'

export function getDocumentProfiles () {

    return myApi.getAll('DocumentProfile/GetDocumentProfiles')
}
export function getDocumentProfilesByStatus () {

    return myApi.getAll('DocumentProfile/GetDocumentProfilesByStatus')
}
export function getDocumentProfilesForCreateTask () {

    return myApi.getAll('DocumentProfile/GetDocumentProfilesForCreateTask')
}
export function getDocumentProfile (data) {

    return myApi.getItem(data, "DocumentProfile/GetData")
}
export function getDocumentProfilesByID (id) {

    return myApi.getByID(id, 'DocumentProfile/GetDocumentProfilesByID')
}
export function createDocumentProfile (data) {
    return myApi.create(data, "DocumentProfile/InsertDocumentProfile")
}

export function updateDocumentProfile (data) {

    return myApi.update(data.profileID, data, "DocumentProfile/UpdateDocumentProfile")
}
export function deleteDocumentProfile (data) {

    return myApi.delete(data.profileID, 'DocumentProfile/DeleteDocumentProfile')
}

export function getDocumentProfileItems () {

    return myApi.getAll('DocumentProfile/GetDocumentProfileItems')
}
export function getDocumentProfileType (id) {
    return myApi.getByID(id, "DocumentProfile/GetDocumentProfileType")
}
export function getDocumentNoSeriesItemsList (id) {
    return myApi.getByID(id, "DocumentNoSeries/GetDocumentNoSeriesItemsList")
}
export function getProfileDocumentById (id) {

    return myApi.getByID(id, 'DocumentNoSeries/GetProfileDocumentById');
}

export function updateDocumentProfileStatus (data) {

    return myApi.update(data.profileID, data, "DocumentProfile/UpdateDocumentProfileStatus")
}