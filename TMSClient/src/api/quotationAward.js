import myApi from '@/util/api'

export function getQuotationAwards(id) {

    return myApi.getByID(id, "QuotationAward/GetQuotationAwards")
}
export function getData(data) {

    return myApi.getItem(data, "QuotationAward/GetQuotationAwardData")
}
export function createQuotationAward(data) {
    return myApi.create(data, "QuotationAward/InsertQuotationAward")
}
export function updateQuotationAward(data) {

    return myApi.update(data.quotationAwardId, data, "QuotationAward/UpdateQuotationAward")
}
export function deleteQuotationAward(data) {

    return myApi.delete(data.quotationAwardId, 'QuotationAward/DeleteQuotationAward')
}


export function getQuotationAwardLine(QuotationAwardId) {
    return myApi.getAll('QuotationAward/GetQuotationAwardLine/?id=' + QuotationAwardId)
}
export function createQuotationAwardLine(data) {
    return myApi.create(data, "QuotationAward/InsertQuotationAwardLine")
}
export function updateQuotationAwardLine(data) {

    return myApi.update(data.quotationAwardLineId, data, "QuotationAward/UpdateQuotationAwardLine")
}
export function deleteQuotationAwardLine(data) {

    return myApi.delete(data.quotationAwardLineId, 'QuotationAward/DeleteQuotationAwardLine')
}
export function getDataLine(data) {

    return myApi.getItem(data, "QuotationAward/GetQuotationAwardLineData")
}

export function updateQuotationAwardVersion (data) {

    return myApi.update(data.quotationAwardId, data, "QuotationAward/UpdateQuotationAwardVersion")
}

export function createVersion (data) {

    return myApi.update(data.quotationAwardId, data, "QuotationAward/CreateVersion")
}
export function applyVersion (data) {

    return myApi.update(data.quotationAwardId, data, "QuotationAward/ApplyVersion")
}
export function checkExitDataVersion (data) {

    return myApi.update(data.quotationAwardId, data, "QuotationAward/CheckExitDataVersion")
}
export function tempVersion (data) {

    return myApi.update(data.quotationAwardId, data, "QuotationAward/TempVersion")
}
export function undoVersion (id) {

    return myApi.getByID(id, "QuotationAward/UndoVersion")
}
export function deleteVersion (id) {

    return myApi.delete(id, 'QuotationAward/DeleteVersion')
}
export function getQuotationHistoryLineProducts(id) {

    return myApi.getByID(id, "QuotationHistory/GetQuotationHistoryLineProducts")
}

export function getQuotationAwardReference(data) {

    return myApi.getItem(data, "QuotationHistory/GetQuotationAwardReference")
}
export function getQuotationHistoryLineByProduct(id) {

    return myApi.getByID(id, "QuotationAward/GetQuotationHistoryLineByProduct")
}


