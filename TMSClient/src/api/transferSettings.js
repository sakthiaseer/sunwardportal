import myApi from '@/util/api'

export function getTransferSettings() {

    return myApi.getAll('TransferSettings/GetTransferSettings')
}
export function getICTFromToLocations() {

    return myApi.getAll('TransferSettings/GetICTFromToLocations')
}
export function getTransferSettingData(data) {

    return myApi.getItem(data, "TransferSettings/GetData")
}

export function createTransferSettings(data) {
    return myApi.create(data, "TransferSettings/InsertTransferSettings")
}

export function updateTransferSettings(data) {

    return myApi.update(data.transferConfigId, data, "TransferSettings/UpdateTransferSettings")
}
export function deleteTransferSettings(data) {

    return myApi.delete(data.transferConfigId, 'TransferSettings/DeleteTransferSettings')
}
