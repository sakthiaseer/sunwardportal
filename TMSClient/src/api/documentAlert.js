import myApi from '@/util/api'

export function getDocumentAlerts() {

    return myApi.getAll('DocumentAlert/GetDocumentAlerts')
}
export function getDocumentAlert(data) {

    return myApi.getItem(data, "DocumentAlert/GetData")
}
export function createDocumentAlert(data) {
    return myApi.create(data, "DocumentAlert/InsertDocumentAlert")
}

export function updateDocumentAlert(data) {

    return myApi.update(data.documentAlertId, data, "DocumentAlert/UpdateDocumentAlert")
}
export function deleteDocumentAlert(data) {

    return myApi.delete(data.documentAlertId, 'DocumentAlert/DeleteDocumentAlert')
}

