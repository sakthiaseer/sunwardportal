import myApi from '@/util/api'

export function getApplicationWikiItems (userId, superUser) {

    return myApi.getAll('Appwiki/GetApplicationWiki?userId=' + userId + "&&superUser=" + superUser)
}
export function getApplicationWikiByID (id) {

    return myApi.getByID(id, 'Appwiki/GetApplicationWikiByID')
}
export function getApplicationWiki (data) {

    return myApi.getItem(data, "Appwiki/GetData")
}

export function isWikiResponsibleByUser (data) {

    return myApi.getItem(data, "Appwiki/IsWikiResponsibleByUser")
}


export function createApplicationWiki (data) {
    return myApi.create(data, "Appwiki/InsertApplicationWiki")
}
export function createDraftApplicationWiki (data) {
    return myApi.create(data, "Appwiki/InsertDraftApplicationWiki")
}
export function updateApplicationWiki (data) {

    return myApi.update(data.applicationWikiId, data, "Appwiki/UpdateApplicationWiki")
}
export function deleteApplicationWiki (data) {

    return myApi.delete(data.applicationWikiId, 'Appwiki/DeleteApplicationWiki')
}
export function getApplicationWikiLine (applicationWikiId) {

    return myApi.getAll("Appwiki/getApplicationWikiLine?id=" + applicationWikiId);
}

export function createApplicationWikiLine (data) {
    return myApi.create(data, "Appwiki/InsertApplicationWikiLine")
}

export function updateApplicationWikiLine (data) {

    return myApi.update(data.applicationWikiLineId, data, "Appwiki/UpdateApplicationWikiLine")
}
export function deleteApplicationWikiLine (data) {

    return myApi.delete(data.applicationWikiLineId, 'Appwiki/DeleteApplicationWikiLine')
}

export function getApplicationWikiLineDuty (applicationWikiLineId) {

    return myApi.getAll("Appwiki/getApplicationWikiLineDuty?id=" + applicationWikiLineId);
}

export function createApplicationWikiLineDuty (data) {
    return myApi.create(data, "Appwiki/InsertApplicationWikiLineDuty")
}

export function updateApplicationWikiLineDuty (data) {

    return myApi.update(data.applicationWikiLineDutyId, data, "Appwiki/UpdateApplicationWikiLineDuty")
}
export function deleteApplicationWikiLineDuty (data) {

    return myApi.delete(data.applicationWikiLineDutyId, 'Appwiki/DeleteApplicationWikiLineDuty')
}

export function createApplicationWikiRecurrence (data) {
    return myApi.create(data, "Appwiki/InsertApplicationWikiRecurrence")
}
export function getApplicationWikiRecurrence (id) {

    return myApi.getByID(id, 'Appwiki/GetApplicationWikiRecurrence')
}

export function updateVersionApplicationWiki (data) {

    return myApi.update(data.applicationWikiId, data, "Appwiki/UpdateVersionApplicationWiki")
}

export function deleteWikiResponsible (data) {

    return myApi.delete(data.wikiResponsibilityID, 'Appwiki/DeleteWikiResponsible')
}

export function createWikiResponsible (data) {
    return myApi.create(data, "Appwiki/InsertWikiResponsible")
}
export function getWikiResponsibleByID (id) {

    return myApi.getByID(id, 'Appwiki/GetWikiResponsibleByID')
}
export function deleteWikiResponsibles (data) {

    return myApi.update(data.wikiResponsibilityID, data, "Appwiki/DeleteWikiResponsibles")
}
export function createVersion (data) {

    return myApi.update(data.applicationWikiId, data, "Appwiki/CreateVersion")
}
export function applyVersion (data) {

    return myApi.update(data.applicationWikiId, data, "Appwiki/ApplyVersion")
}
export function tempVersion (data) {

    return myApi.update(data.applicationWikiId, data, "Appwiki/TempVersion")
}
export function undoVersion (id) {

    return myApi.getByID(id, "Appwiki/UndoVersion")
}
export function deleteVersion (id) {

    return myApi.delete(id, 'Appwiki/DeleteVersion')
}
export function getDocumentProfiles () {

    return myApi.getAll('appwiki/GetApplicationWikiProlileList')
}

export function getProfileAutonumber (id) {

    return myApi.getByID(id, 'Appwiki/GetProfileAutonumber')
}
export function getApplicationWikiByListing (userId, superUser, id) {

    return myApi.getAll('Appwiki/GetApplicationWikiByListing?userId=' + userId + "&&superUser=" + superUser + "&&id=" + id)
}

export function getApplicationWikiBySearch (data) {

    return myApi.getItem(data, "Appwiki/GetApplicationWikiBySearch")
}
export function getApplicationWikiPdf (userId, id) {

    return myApi.getAll('Appwiki/GetApplicationWikiPdf?userId=' + userId + "&&id=" + id)
}


export function getReleaseWikiList() {

    return myApi.getAll('Appwiki/GetReleaseWikiList')
}
export function updateAppWikiReleaseDoc (id) {

    return myApi.getByID(id, 'Appwiki/UpdateAppWikiReleaseDoc')
}
export function getExcel(data) {

    return myApi.downLoadProfileDocument(data, 'Appwiki/GetExcel')
}