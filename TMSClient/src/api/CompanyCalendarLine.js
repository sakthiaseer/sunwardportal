import myApi from '@/util/api'
export function getCompanyCalendarLine (id) {

    return myApi.getByID(id, "companyCalendarLine/GetCompanyCalendarLine")
}
export function createCompanyCalendarLine (data) {
    return myApi.create(data, "companyCalendarLine/InsertCompanyCalendarLine")
}

export function updateCompanyCalendarLine (data) {

    return myApi.update(data.companyCalendarLineId, data, "companyCalendarLine/UpdateCompanyCalendarLine")
}
export function deleteCompanyCalendarLine (data) {

    return myApi.delete(data.companyCalendarLineId, 'companyCalendarLine/DeleteCompanyCalendarLine')
}
export function getCompanyCalendarDocuments (id) {

    return myApi.getByID(id, "companyCalendarLine/GetCompanyCalendarDocuments")
}
export function getCalandarNotesUsers (data) {

    return myApi.getByID(data,"CompanyCalendarLine/GetCalandarNotesUsers")
}


export function getCompanyCalendarLineLink (id) {

    return myApi.getByID(id, "companyCalendarLine/GetCompanyCalendarLineLink")
}
export function createCompanyCalendarLineLink (data) {
    return myApi.create(data, "companyCalendarLine/InsertCompanyCalendarLineLink")
}

export function updateCompanyCalendarLineLinks (data) {

    return myApi.update(data.companyCalendarLineLinkId, data, "companyCalendarLine/UpdateCompanyCalendarLineLinks")
}
export function deleteCompanyCalendarLineLink (data) {

    return myApi.delete(data.companyCalendarLineLinkId, 'companyCalendarLine/DeleteCompanyCalendarLineLink')
}
export function getNotesUsers (id) {

    return myApi.getByID(id, "companyCalendarLine/getNotesUsers")
}
export function getCalandarNotes (id,userId) {

    return myApi.getByuserId(id,userId, "companyCalendarLine/GetCalandarNotes")
}
export function createCompanyCalendarLineNotes (data) {
    return myApi.create(data, "companyCalendarLine/InsertCompanyCalendarLineNotes")
}
export function getDocumentsBySessionID(sessionId, userId) {

    return myApi.getAll('Documents/GetDocumentsBySessionID/?sessionID=' + sessionId + '&&userId=' + userId + '&&type=')
}
export function insertCompanyCalendarLineLinkPrint (data) {

    return myApi.create( data, "companyCalendarLine/InsertCompanyCalendarLineLinkPrint")
}

export function getCompanyCalendarLineLinkAll (id,sessionId,userId) {
    return myApi.getAll('companyCalendarLine/getCompanyCalendarLineLinkAll/?sessionID=' + sessionId + '&&Id=' + id+ '&&userId='+userId)
}
export function getCompanyCalendarLineLinkUserLinkByUser (id) {

    return myApi.getByID(id, "companyCalendarLine/CompanyCalendarLineLinkUserLinkByUser")
}
export function createCompanyCalendarLineMeetingNotes(data) {
    return myApi.create(data, "companyCalendarLine/InsertCompanyCalendarLineMeetingNotes")
}
export function getCompanyCalendarLineMeetingNotes(userId) {
    return myApi.getByID(userId, 'companyCalendarLine/GetCompanyCalendarLineMeetingNotes')
}
export function updatecompanyCalendarLinePrintNotes(data) {

    return myApi.update(data.companyCalendarLineLinkId, data, "companyCalendarLine/UpdatecompanyCalendarLinePrintNotes")
}
export function uploadCalandarLinkDocuments(data) {

    return myApi.uploadFile(data, "documents/UploadCalandarLinkDocuments")
}
export function getCompanyCalendarLineLinkDocuments(sessionId, userId) {

    return myApi.getAll('Documents/GetDocumentsByCalandarLink/?sessionID=' + sessionId + '&&userId=' + userId)
}
export function deleteDocuments(data) {

    return myApi.delete(data.documentID, 'Documents/DeleteDocuments')
}
export function uploadCalandarLinkAllDocuments(data,userId, sessionId) {

    return myApi.uploadFile(data, "Documents/uploadCalandarLinkAllDocuments?sessionId=" + sessionId+"&&userId="+userId);
}
export function updateCompanyCalendarLineLinkUserLinkByUserStatus(data) {

    return myApi.update(data.companyCalendarLineLinkUserLinkId, data, "companyCalendarLine/UpdateCompanyCalendarLineLinkUserLinkByUserStatus")
}
export function updateCompanyCalendarLineCloseNotes(data) {

    return myApi.update(data.companyCalendarLineNotesId, data, "companyCalendarLine/UpdateCompanyCalendarLineCloseNotes")
}
export function updateCompanyCalendarLineMeetingNotes(data) {

    return myApi.update(data.companyCalendarLineMeetingNotesId, data, "companyCalendarLine/UpdateCompanyCalendarLineMeetingNotes")
}

export function createCompanyCalendarLineLinkCalandar (data) {
    return myApi.create(data, "companyCalendarLine/InsertCompanyCalendarLineLinkCalandar")
}