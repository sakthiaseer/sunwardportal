import myApi from '@/util/api'

export function getFinishProducts() {
  return myApi.getAll('FinishProduct/GetFinishProducts')
}
export function getFinishProductsList() {
  return myApi.getAll('FinishProduct/GetFinishProductsList')
}
export function getFinishProduct(data) {

  return myApi.getItem(data, "FinishProduct/GetData")
}
export function createFinishProduct(data) {
  return myApi.create(data, "FinishProduct/InsertFinishProdut")
}
export function updatefinishProduct(data) {

  console.log(data);
  return myApi.update(data.finishProductId, data, "FinishProduct/UpdateFinishProdut")
}
export function deleteFinishProduct(data) {

  return myApi.delete(data.finishProductId, 'FinishProduct/DeleteFinishProduct')
}