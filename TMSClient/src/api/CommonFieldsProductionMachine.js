import myApi from '@/util/api'

export function getcommonFieldsProductionMachine (id) {

  return myApi.getByID(id, 'commonFieldsProductionMachine/GetcommonFieldsProductionMachine')
}
export function getData (data) {

  return myApi.getItem(data, "commonFieldsProductionMachine/GetData")
}
export function createcommonFieldsProductionMachine (data) {
  return myApi.create(data, "commonFieldsProductionMachine/InsertcommonFieldsProductionMachine")
}
export function updatecommonFieldsProductionMachine (data) {

  return myApi.update(data.commonFieldsProductionMachineId, data, "commonFieldsProductionMachine/UpdatecommonFieldsProductionMachine")
}
export function deletecommonFieldsProductionMachine (data) {

  return myApi.delete(data.commonFieldsProductionMachineId, 'commonFieldsProductionMachine/DeletecommonFieldsProductionMachine')
}
export function getICTMasterlocation () {

  return myApi.getAll('ICTMaster/GetICTMasterlocation')
}
export function getICTMasterSpecificArea(id) {

  return myApi.getByID(id, "ICTMaster/GetICTMasterSpecificArea")
}
export function getCommonFieldsProductionMachineManufactureSite (id) {

  return myApi.getByID(id, 'commonFieldsProductionMachine/GetCommonFieldsProductionMachineManufactureSite')
}
export function getCommonFieldsProductionMachineName (type) {

  return myApi.getByType(type, 'commonFieldsProductionMachine/GetCommonFieldsProductionMachineName')
}

export function getcommonFieldsProductionMachineAll () {

  return myApi.getAll('commonFieldsProductionMachine/GetCommonFieldsProductionMachineAll')
}