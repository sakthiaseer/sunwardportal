import myApi from '@/util/api'



export function getPackagingLeaflet(data) {

    return myApi.getItem(data, "PackagingLeaflet/GetData")
}
export function getPackagingLeaflets(){
    return myApi.getAll("PackagingLeaflet/GetPackagingLeaflets")
}
export function createPackagingLeaflet(data) {
    return myApi.create(data, "PackagingLeaflet/InsertPackagingLeaflet")
}

export function updatePackagingLeaflet(data) {

    return myApi.update(data.leafletId, data, "PackagingLeaflet/UpdatePackagingLeaflet")
}
export function deletePackagingLeaflet(data) {

    return myApi.delete(data.leafletId, 'PackagingLeaflet/DeletePackagingLeaflet')
}
export function getPackagingLeafletsByRefNo(data) {
    return myApi.getItems(data, 'PackagingLeaflet/GetPackagingLeafletsByRefNo')
  }

