import myApi from '@/util/api'


  export function getStartOfDays() {

    return myApi.getAll('StartOfDay/GetStartOfDays')
  }

export function updateStartOfDay(data) {

  return myApi.update(data.startOfDayId, data, "StartOfDay/UpdateStartOfDay")
}