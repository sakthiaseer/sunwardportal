import myApi from '@/util/api'

export function getItems() {

    return myApi.getAll('NAVFunc/GetItems')
}

export function getProdOrders() {

    return myApi.getAll('NAVFunc/GetProdOrders')
}

export function getProdOrderLines(id) {

    return myApi.getByID(id, 'NAVFunc/GetProdOrderLines')
}
export function getBOMHeader() {

    return myApi.getAll('NAVFunc/GetBOMHeader')
}
export function getBOMLine(id) {

    return myApi.getByID(id, 'NAVFunc/GetBOMLine')
}
export function getBOMVersion(id) {

    return myApi.getByID(id, 'NAVFunc/GetBOMVersion')
}
export function getMachine() {

    return myApi.getAll('NAVFunc/GetMachine')
}
export function getScrapCode() {

    return myApi.getAll('NAVFunc/GetScrapCode')
}
export function getBOMType() {

    return myApi.getAll('NAVFunc/GetBOMType')
}
export function getUOM() {

    return myApi.getAll('NAVFunc/GetUOM')
}
export function postScrapJournal(data) {

    return myApi.create(data, "NAVFunc/PostScrapJournal")
}
export function postProdBOM(data) {

    return myApi.create(data, "NAVFunc/PostProdBOM")
}
export function getItemUOM(id) {

    return myApi.getByID(id, 'NAVFunc/GetItemUOM')
}
export function getPackSize() {

    return myApi.getAll('NAVFunc/GetPackSize')
}
export function getScrapCodeSync() {

    return myApi.getAll('NAVFunc/GetScrapCodeSync')
}