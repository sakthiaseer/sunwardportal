import myApi from '@/util/api'

export function getAppTranLocations () {

    return myApi.getAll('AppTranLocation/GetAppTranLocations')
}
export function getDatas (data) {

    return myApi.getItem(data, "AppTranLocation/GetData")
}
export function createAppTranLocation (data) {
    return myApi.create(data, "AppTranLocation/InsertAppTranLocation")
}
export function updateAppTranLocation (data) {

    return myApi.update(data.id, data, "AppTranLocation/UpdateAppTranLocation")
}
export function deleteAppTranLocation (data) {

    return myApi.delete(data.id, 'AppTranLocation/DeleteAppTranLocation')
}

export function getAPPTranLocationLinesById (id) {

    return myApi.getByID(id,'AppTranLocation/GetAPPTranLocationLinesById')
}
export function createAppTranLocationLine (data) {
    return myApi.create(data, "AppTranLocation/InsertAppTranLocationLine")
}
export function updateAppTranLocationLine (data) {

    return myApi.update(data.id, data, "AppTranLocation/UpdateTranLocationLine")
}
export function deleteAppTranLocationLine (data) {

    return myApi.delete(data.id, 'AppTranLocation/DeleteTranLocationLine')
}