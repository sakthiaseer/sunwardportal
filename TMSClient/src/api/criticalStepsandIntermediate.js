import myApi from '@/util/api'

export function getCriticalstepandIntermediates() {

    return myApi.getAll('CriticalstepandIntermediate/GetCriticalstepandIntermediates')
  }

 
export function getCriticalstepandIntermediate(data) {

  return myApi.getItem(data, "CriticalstepandIntermediate/GetData")
}
export function isMasterExist(id){
  return myApi.getByID(id, "CriticalstepandIntermediate/isMasterExist")
}

export function getCriticalstepandIntermediatesByFinishProduct(id) {  
  return myApi.getByID(id, "CriticalstepandIntermediate/GetCriticalstepandIntermediatesByFinishProduct")
}
export function updateCriticalstepandIntermediate(data) {

  return myApi.update(data.criticalstepandIntermediateID, data, "CriticalstepandIntermediate/UpdateCriticalstepandIntermediate")
}
  
export function createCriticalstepandIntermediate(data) {

    return myApi.create(data, "CriticalstepandIntermediate/InsertCriticalstepandIntermediate")
  }
  export function deleteCriticalstepandIntermediate(data) {

    return myApi.delete(data.criticalstepandIntermediateID, 'CriticalstepandIntermediate/DeleteCriticalstepandIntermediate')
}

export function getCriticalstepandIntermediateLines(id) {

  return myApi.getByID(id,'CriticalstepandIntermediateLine/GetCriticalstepandIntermediateLines')
}


export function getDataLine(data) {

  return myApi.getItem(data, "CriticalstepandIntermediateLine/GetData")
}
export function updateCriticalstepandIntermediateLine(data) {

return myApi.update(data.criticalstepandIntermediateLineID, data, "CriticalstepandIntermediateLine/UpdateCriticalstepandIntermediateLine")
}

export function createCriticalstepandIntermediateLine(data) {

  return myApi.create(data, "CriticalstepandIntermediateLine/InsertCriticalstepandIntermediateLine")
}
export function deleteCriticalstepandIntermediateLine(data) {

  return myApi.delete(data.criticalstepandIntermediateLineID, 'CriticalstepandIntermediateLine/DeleteFinishProductGeneralInfoLine')
}
export function getFinishProductsListstepsInformation () {
  return myApi.getAll('FinishProduct/GetFinishProductsListstepsInformation')
}
