import myApi from '@/util/api'

export function getApptransferOrders() {

  return myApi.getAll('ApptransferOrder/GetApptransferOrders')
}
export function getApptransferOrderByID(id) {

  return myApi.getByID(id, "ApptransferOrder/GetApptransferOrderByID")
}
export function getApptransferOrder(data) {

  return myApi.getItem(data, "ApptransferOrder/GetData")
}

export function updateApptransferOrder(data) {

  return myApi.update(data.consumptionEntryID, data, "ApptransferOrder/UpdateApptransferOrder")
}

export function getAPPTransferOrderLiness() {

  return myApi.getAll('APPTransferOrderLines/GetAPPTransferOrderLiness')
}
export function getApptransferOrderLinesByID(id, lineNo) {

  return myApi.getByuserId(id, lineNo, "APPTransferOrderLines/GetConsumptionLinesById")
}
export function getConsumptionLinesItemsById(id,lineNo) {

  return myApi.getByuserId(id,lineNo, "APPTransferOrderLines/GetConsumptionLinesItemsById")
}
export function getAPPTransferOrderLines(data) {

  return myApi.getItem(data, "APPTransferOrderLines/GetData")
}
export function getConsumptionEntryFilter(data) {

  return myApi.getItem(data, "ApptransferOrder/GetConsumptionEntryFilter")
}

export function getByCompany(data) {
  return myApi.getItem(data, "ApptransferOrder/GetByCompany")
}

export function getConsumptionEntryByCompany(data) {
  return myApi.getItem(data, "ApptransferOrder/GetConsumptionEntryByCompany")
}

export function getConsumptionInfo(data) {
  return myApi.getItem(data, "ApptransferOrder/GetConsumptionInfo")
}
export function getConsumptionUnScannedItems(data) {
  return myApi.getItem(data, "ApptransferOrder/GetConsumptionUnScannedItems")
}