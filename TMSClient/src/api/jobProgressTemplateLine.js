import myApi from '@/util/api'
export function getJobProgressTemplateLines(id) {

    return myApi.getByID(id, 'JobProgressTemplate/GetJobProgressTemplateLines')
}
export function createJobProgressTemplateLine(data) {
    return myApi.create(data, "JobProgressTemplate/InsertJobProgressTemplateLine")
}

export function updateJobProgressTemplateLine(data) {

    return myApi.update(data.jobProgressTemplateLineId, data, "JobProgressTemplate/UpdateJobProgressTemplateLine")
}
export function deleteJobProgressTemplateLine(data) {

    return myApi.delete(data.jobProgressTemplateLineId, 'JobProgressTemplate/DeleteJobProgressTemplateLine')
}

export function getJobProgressTemplateProcessLines(id) {

    return myApi.getByID(id, 'JobProgressTemplate/GetJobProgressTemplateProcessLines')
}


export function getJobProgressTemplateLineProcess(id) {

    return myApi.getByID(id, 'JobProgressTemplate/GetJobProgressTemplateLineProcess')
}
export function createJobProgressTemplateLineProcess(data) {
    return myApi.create(data, "JobProgressTemplate/InsertJobProgressTemplateLineProcess")
}

export function updateJobProgressTemplateLineProcess(data) {

    return myApi.update(data.jobProgressTemplateLineProcessId, data, "JobProgressTemplate/UpdateJobProgressTemplateLineProcess")
}



export function getJobProgressTemplateLineLine(id) {

    return myApi.getByID(id, 'JobProgressTemplate/GetJobProgressTemplateLineLine')
}
export function createJobProgressTemplateLineLine(data) {
    return myApi.create(data, "JobProgressTemplate/InsertJobProgressTemplateLineLine")
}

export function updateJobProgressTemplateLineLine(data) {

    return myApi.update(data.jobProgressTemplateLineLineProcessId, data, "JobProgressTemplate/UpdateJobProgressTemplateLineLine")
}
export function deleteJobProgressTemplateLineLine(data) {

    return myApi.delete(data.jobProgressTemplateLineLineProcessId, 'JobProgressTemplate/DeleteJobProgressTemplateLineLine')
}