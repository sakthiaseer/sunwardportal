import myApi from '@/util/api'

export function getProductionSimulation() {
    return myApi.getAll('ProductionSimulation/GetProductionSimulation')
}
export function createProductionSimulation(data) {
    return myApi.create(data, "ProductionSimulation/InsertProductionSimulation")
}

export function updateProductionSimulation(data) {

    return myApi.update(data.productionSimulationId, data, "ProductionSimulation/UpdateProductionSimulation")
}
export function deleteProductionSimulation(data) {

    return myApi.delete(data.productionSimulationId, 'ProductionSimulation/DeleteProductionSimulation')
}
export function uploadDocuments(data) {

    return myApi.uploadFile(data, "ProductionSimulation/UploadDocuments");
}
export function getNavRefresh(id) {
    return myApi.getByID(id, 'ProductionSimulation/RefreshProductionSimulation')
}

export function getNAVProdOrderLine(id) {
    return myApi.getByID(id, "NAVFunc/GetNAVProdOrderLine")
  }

  export function getNAVProdOrderLineList() {
    return myApi.getAll('NAVFunc/GetNAVProdOrderLineList')
}