import myApi from '@/util/api'
export function getClassificationBmrByRefNo (data) {
    return myApi.getByID(data, 'ClassificationBmr/GetClassificationBmrByRefNo')
}
export function getClassificationBmrByTicketNo (data) {
    return myApi.getByID(data, 'ClassificationBmr/GetClassificationBmrByTicketNo')
}
export function createClassificationBmr (data) {
    return myApi.create(data, "ClassificationBmr/InsertClassificationBmr")
}
export function updateClassificationBmr (data) {

    return myApi.update(data.classificationBmrId, data, "ClassificationBmr/UpdateClassificationBmr")
}
export function deleteClassificationBmr (data) {

    return myApi.delete(data.classificationBmrId, 'ClassificationBmr/DeleteClassificationBmr')
}
export function getRefPlanProductionSimulation (id, productionSimulationDate) {

    return myApi.getAll('ClassificationBmr/GetRefPlanProductionSimulation?id=' + id + "&&productionSimulationDate=" + productionSimulationDate)
}
export function getClassificationBmrSearch (data) {

    return myApi.getItem(data, "ClassificationBmr/GetClassificationBmrSearch")
}
export function getClassificationBmrByProductOrderNo(type) {

    return myApi.getByType(type, 'CompanyListing/GetClassificationBmrByProductOrderNo')
}
