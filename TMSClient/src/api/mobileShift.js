import myApi from '@/util/api'

export function getMobileShift () {

    return myApi.getAll('MobileShift/GetMobileShift')
}
export function getSearchData (data) {

    return myApi.getItem(data, "MobileShift/GetData")
}

export function createMobileShift (data) {
    return myApi.create(data, "MobileShift/InsertMobileShift")
}

export function updateMobileShift (data) {

    return myApi.update(data.mobileShiftId, data, "MobileShift/UpdateMobileShift")
}
export function deleteMobileShift (data) {

    return myApi.delete(data.mobileShiftId, 'MobileShift/DeleteMobileShift')
}
