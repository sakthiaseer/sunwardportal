import myApi from '@/util/api'

export function getProductionForecasts() {

    return myApi.getAll('ProductionForecast/GetNavMethodCode')
}
export function getProductionForecast(data) {

    return myApi.getItem(data, "ProductionForecast/GetData")
}
export function createProductionForecast(data) {
    return myApi.create(data, "ProductionForecast/InsertProductionForecast")
}

export function updateProductionForecast(data) {

    return myApi.update(data.productionForecastID, data, "ProductionForecast/UpdateProductionForecast")
}
export function deleteProductionForecast(data) {

    return myApi.delete(data.productionForecastID, 'ProductionForecast/DeleteProductionForecast')
}
export function getProductionForecastsById(id) {
  
    return myApi.getByID(id, "ProductionForecast/GetProductionForecastsById")
  }


