import myApi from '@/util/api'

export function getRelease() {

    return myApi.getAll('Release/GetRelease')
}