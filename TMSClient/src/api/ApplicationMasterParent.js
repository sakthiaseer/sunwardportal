import myApi from '@/util/api'

export function getApplicationMasterParent () {

    return myApi.getAll('ApplicationMasterParent/GetApplicationMasterParent')
}
export function getData (data) {

    return myApi.getItem(data, "ApplicationMasterParent/GetData")
}
export function createApplicationMasterParent (data) {
    return myApi.create(data, "ApplicationMasterParent/InsertApplicationMasterParent")
}
export function updateApplicationMasterParent (data) {

    return myApi.update(data.applicationMasterParentCodeId, data, "ApplicationMasterParent/UpdateApplicationMasterParent")
}
export function deleteApplicationMasterParent (data) {

    return myApi.delete(data.applicationMasterParentCodeId, 'ApplicationMasterParent/DeleteApplicationMasterParent')
}
export function getApplicationMasterParentDropDown (data) {
    return myApi.getByID(data, "ApplicationMasterParent/GetApplicationMasterParentDropDown")
}

export function getApplicationMasterParentDropDownList (data) {
    return myApi.getByID(data, "ApplicationMasterParent/GetApplicationMasterParentDropDownList")
}
export function getApplicationMasterParentByParent () {

    return myApi.getAll('ApplicationMasterParent/GetApplicationMasterParentByParent')
}
