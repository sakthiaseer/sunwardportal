import myApi from '@/util/api'

export function getAPPDispenserDispensings() {

    return myApi.getAll('APPDispenserDispensing/GetAPPDispenserDispensings')
  }
  export function getAPPDispenserDispensingByID(id) {
  
    return myApi.getByID(id, "APPDispenserDispensing/GetAPPDispenserDispensingByID")
  }
  export function getAPPDispenserDispensing(data) {

    return myApi.getItem(data, "APPDispenserDispensing/GetData")
}

export function updateAPPDispenserDispensing(data) {

  return myApi.update(data.dispenserDispensingId, data, "APPDispenserDispensing/UpdateAPPDispenserDispensing")
}
  
export function getAPPDispenserDispensingLines() {

    return myApi.getAll('APPDispenserDispensingLine/GetAPPDispenserDispensingLines')
  }
  export function getAPPDispenserDispensingLineByID(id) {
  
    return myApi.getByID(id, "APPDispenserDispensingLine/GetAPPDispenserDispensingLineByID")
  }
  export function getAPPDispenserDispensingLine(data) {

    return myApi.getItem(data, "APPDispenserDispensingLine/GetData")
}

export function updateAPPDispenserDispensingLine(data) {

  return myApi.update(data.dispenserDispensingId, data, "APPDispenserDispensingLine/UpdateAPPDispenserDispensingLine")
}
export function getAPPDispenserDispensingEntryFilter (data) {

  return myApi.getItem(data, "APPDispenserDispensing/GetAPPDispenserDispensingEntryFilter")
}
