import myApi from '@/util/api'


export function getCustomerAcceptanceConfirmationByID (id) {
  return myApi.getByID(id, 'CustomerAcceptanceConfirmation/GetCustomerAcceptanceConfirmationByID')
}
export function getCustomerAcceptanceQtyOrderByID (id) {
    return myApi.getByID(id, 'CustomerAcceptanceConfirmation/GetCustomerAcceptanceQtyOrderByID')
  }

export function getData (data) {

  return myApi.getItem(data, "CustomerAcceptanceConfirmation/GetData")
}
export function createCustomerAcceptanceConfirmation (data) {
  return myApi.create(data, "CustomerAcceptanceConfirmation/InsertCustomerAcceptanceConfirmation")
}
export function updateCustomerAcceptanceConfirmation (data) {

  console.log(data);
  return myApi.update(data.customerAcceptanceConfirmationId, data, "CustomerAcceptanceConfirmation/UpdateCustomerAcceptanceConfirmation")
}
export function deleteCustomerAcceptanceConfirmation (data) {

  return myApi.delete(data.customerAcceptanceConfirmationId, 'CustomerAcceptanceConfirmation/DeleteCustomerAcceptanceConfirmation')
}

export function createCustomerAcceptanceQtyOrder (data) {
  return myApi.create(data, "CustomerAcceptanceConfirmation/InsertCustomerAcceptanceQtyOrder")
}
export function updateCustomerAcceptanceQtyOrder (data) {

  console.log(data);
  return myApi.update(data.customerAcceptanceQtyOrderId, data, "CustomerAcceptanceConfirmation/UpdateCustomerAcceptanceQtyOrder")
}
export function deleteCustomerAcceptanceQtyOrder (data) {

  return myApi.delete(data.customerAcceptanceQtyOrderId, 'CustomerAcceptanceConfirmation/DeleteCustomerAcceptanceQtyOrder')
}