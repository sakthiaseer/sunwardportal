import myApi from '@/util/api'

export function getRangeCalibrations() {

    return myApi.getAll('RangeCalibration/GetRangeCalibrations')
}
export function getRangeCalibration(data) {

    return myApi.getItem(data, "RangeCalibration/GetData")
}

export function createRangeCalibration(data) {
    return myApi.create(data, "RangeCalibration/InsertRangeCalibration")
}

export function updateRangeCalibration(data) {

    return myApi.update(data.rangeCalibrationID, data, "RangeCalibration/UpdateRangeCalibration")
}
export function deleteRangeCalibration(data) {

    return myApi.delete(data.rangeCalibrationID, 'RangeCalibration/DeleteRangeCalibration')
}
