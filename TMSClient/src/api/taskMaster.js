import myApi from '@/util/api'

export function getTaskMasters(id) {

    return myApi.getByID(id, 'TaskMaster/GetTaskMasters')
}
export function getTaskMastersbyID(id) {

    return myApi.getByID(id, 'TaskMaster/GetTaskMastersbyID')
}
export function getTaskVersionDescription(id) {

    return myApi.getByID(id, 'TaskMaster/GetTaskVersionDescription')
}
export function getNotesbyID(id, userId) {

    return myApi.getByuserId(id, userId, 'TaskNotes/GetIsReminder')
}
export function getSubTasks() {

    return myApi.getAll('TaskMaster/GetSubTasks')
}
export function getSubTasksId(id, userId) {

    return myApi.getByuserId(id, userId, 'TaskMaster/GetSubTasksId')
}
export function getTaskTreeId(id, userId) {

    return myApi.getByuserId(id, userId, 'TaskMaster/GetSubNestedTasksId');
}
export function getuserTaskTreeId(id, userId) {

    return myApi.getByuserId(id, userId, 'TaskMaster/GetSubNestedUserTasks');
}
export function getApplicationUsers() {

    return myApi.getAll('ApplicationUser/GetApplicationUsers')
}
export function getApplicationUsersForTask() {

    return myApi.getAll('ApplicationUser/GetApplicationUsersForTask')
}
export function getUsers(data) {

    return myApi.getItem(data, 'ApplicationUser/GetUsers')
}
export function getUserById(id) {

    return myApi.getByID(id, 'ApplicationUser/GetUsersById')
}
export function getUserPassword(id) {

    return myApi.getByID(id, 'ApplicationUser/GetUserPassword')
}
export function updatePassword(data) {
    console.log(data);
    return myApi.update(data.userId, data, "ApplicationUser/UpdatePassword")
}
export function getProjects() {

    return myApi.getAll('Project/GetActiveProjects')
}
export function getTagMaster() {

    return myApi.getAll('TagMaster/GetActiveTagMaster')
}

export function getTaskMaster(data) {

    return myApi.getItem(data, "TaskMaster/GetData")
}
export function getDataSubTask(data) {

    return myApi.getItem(data, "TaskMaster/GetDataSubTask")
}
export function createTaskMaster(data) {

    return myApi.create(data, "TaskMaster/InsertTaskMaster")
}
export function dragDropTask(data) {

    return myApi.update(data.taskID, data, "TaskMaster/DragDropTask");
}
export function updateTaskMaster(data) {

    return myApi.update(data.taskID, data, "TaskMaster/UpdateTaskMaster")
}
export function createTaskNotes(data) {
    return myApi.create(data, "TaskNotes/InsertTaskNotes");
}
export function deleteTaskNotes(data) {
    return myApi.create(data, "TaskNotes/DeleteTaskNotes");
}
export function createPersonalTags(data) {
    return myApi.create(data, "PersonalTags/InsertPersonalTags")
}
export function updateTaskNotes(data) {

    return myApi.update(data, data, "TaskNotes/UpdateTaskNotes")
}
export function createSubTaskMaster(data) {
    console.log(data);
    return myApi.create(data, "TaskMaster/InsertSubTaskMaster")
}

export function updateSubTaskMaster(data) {
    console.log(data);
    return myApi.update(data.taskID, data, "TaskMaster/UpdateSubTaskMaster")
}
export function deleteTaskMaster(data) {

    return myApi.delete(data.taskID, 'TaskMaster/DeleteTaskMaster')
}
export function updateCommentAttachement(data) {
    return myApi.update(data.taskCommentId, data, "TaskComment/UpdateCommentAttachement")
}
export function getTaskLinks() {

    return myApi.getAll('TaskMaster/GetTaskLinks')
}
export function getDataTaskLink(data) {

    return myApi.getItem(data, "TaskMaster/GetDataTaskLink")
}
export function createTaskLinks(data) {
    return myApi.create(data, "TaskMaster/InsertTaskLinks")
}

export function updateTaskLink(data) {

    return myApi.update(data.taskLinkID, data, "TaskMaster/UpdateTaskLink")
}
export function deleteTaskLink(data) {

    return myApi.delete(data.taskLinkID, 'TaskMaster/DeleteTaskLink')
}
export function getTaskAttachment(id) {

    return myApi.getByID(id, 'TaskAttachment/GetTaskAttachment')
}
export function getTaskComments(id, userId) {

    return myApi.getByuserId(id, userId, 'TaskComment/GetTaskComment')
}
export function createTaskComments(data) {

    return myApi.create(data, "TaskComment/InsertTaskComment")
}
export function updateTaskComment(data) {

    return myApi.update(data.taskCommentID, data, "TaskComment/UpdateTaskComment")
}
export function updateTaskCommentuser(data) {

    return myApi.update(data.taskCommentID, data, "TaskComment/UpdateTaskCommentUser")
}
export function updateReadComment(data) {
    return myApi.update(data.taskCommentID, data, "TaskComment/UpdateRead");
}
export function updateClosed(data) {
    return myApi.update(data.taskCommentID, data, "TaskComment/UpdateClosed");
}
export function updateReadFileComment(data) {
    return myApi.update(data.taskCommentID, data, "TaskComment/UpdateDocRead");
}

export function closeComments(data) {

    return myApi.update(data.taskID, data, "TaskComment/CloseComments")
}
export function reOpenClosedComments(data) {

    return myApi.update(data.taskID, data, "TaskComment/ReOpenClosedComments")
}
export function TaskLink(data) {

    return myApi.update(data.taskID, data, "TaskComment/UpdateTaskLink")
}
export function completeTask(data) {
    return myApi.update(data.taskID, data, "TaskMaster/CompleteTask")
}
export function completeTaskSubject(data) {
    return myApi.update(data.taskID, data, "TaskMaster/CompleteTaskSubject")
}

export function closeTask(data) {
    return myApi.update(data.taskID, data, "TaskMaster/CloseTask")
}
export function getSubTasksbyStatus(id, userId) {

    return myApi.getByuserId(id, userId, 'TaskMaster/GetTaskByStatus')
}
export function getTaskAssigne(id, userId) {

    return myApi.getByuserId(id, userId, 'TaskMaster/GetTaskAsignee')
}
export function getTaskList(data) {

    return myApi.getItem(data, "TaskMaster/GetTaskList");
}

export function getAssignedAndCCUsers(data) {
    console.log(JSON.stringify(data));
    return myApi.getItem(data, "TaskComment/GetAssignedAndCCUsers")
}

export function getDashTaskList(data) {

    return myApi.getItem(data, "TaskMaster/GetDashTaskList");
}
export function getRemainderTaskList(data) {

    return myApi.getItem(data, "TaskMaster/GetRemainderTaskList");
}
export function getPersonalTags(id, userId) {

    return myApi.getByuserId(id, userId, 'PersonalTags/GetPersonalTags');
}
export function removePersonalTagsItem(data) {

    return myApi.getItem(data, "PersonalTags/RemovePersonalTagsItem")
}
export function getTaskNotes(id, userId) {

    return myApi.getByuserId(id, userId, 'TaskNotes/GetTaskNotes')
}
export function getTaskSearchFilter(data) {

    return myApi.getItem(data, "TaskMaster/GetTaskFilter")
}
export function getTaskSorting(id, userId) {

    return myApi.getByuserId(id, userId, 'TaskMaster/GetTaskSorting')
}
export function duplicateTask(data) {

    return myApi.update(data.userId, data, 'TaskMaster/DuplicateTask')
}

export function getTaskFiles(id, userId) {

    return myApi.getByuserId(id, userId, 'TaskMaster/GetTaskFiles');
}
export function getFilesHistory(id) {

    return myApi.getByID(id, 'TaskMaster/GetFilesHistory');
}
export function getTeamMember(id) {

    return myApi.getByID(id, 'Project/GetTeamMember')
}
export function updateRead(id, userId) {

    return myApi.getByuserId(id, userId, 'TaskMaster/UpadateRead')
}
export function upadateAsUnRead(id, userId) {

    return myApi.getByuserId(id, userId, 'TaskMaster/UpadateAsUnRead')
}
export function deleteTaskComment(data) {

    return myApi.delete(data.taskCommentID, 'TaskComment/DeleteTaskComment')
}
export function getDocComment(id, userId) {

    return myApi.getByuserId(id, userId, 'TaskComment/GetDocComment')
}
export function getTaskSubCommentFilter(data) {

    return myApi.getItem(data, 'TaskComment/GetTaskSubCommentFilter')
}
export function getUserGroups() {

    return myApi.getAll('TaskMaster/GetUserGroups')
}
export function getTaskMeetingNote(data) {

    return myApi.getItem(data, 'TaskMeetingNote/GetTaskMeetingNote')
}
export function updateTaskMeetingNote(data) {

    return myApi.update(data.taskMeetingNoteID, data, 'TaskMeetingNote/UpdateTaskMeetingNote')
}
export function updatePersonalTags(data) {

    return myApi.update(data.personalTagID, data, 'PersonalTags/UpdateTaskMeetingNote')
}
export function createTaskMeetingNote(data) {

    return myApi.create(data, "TaskMeetingNote/InsertTaskMeetingNote")
}
export function getPersonalTagsByUser(id) {

    return myApi.getByID(id, 'PersonalTags/GetPersonalTagsByUser')
}
export function getTaskAssigned(id, userId) {

    return myApi.getByuserId(id, userId, 'TaskAssigned/GetTaskAssigned')
}
export function checkOutDoc(data) {
    return myApi.update(data.taskAttachmentID, data, 'TaskMaster/CheckOutDoc')
}
export function checkInDoc(data) {

    return myApi.update(data.taskAttachmentID, data, 'TaskMaster/CheckInDoc')
}
export function reOpenTask(data) {

    return myApi.create(data, "TaskMaster/ReOpenTask")
}

export function bulkTransferTask(data) {

    return myApi.create(data, "TaskMaster/BulkTransferTask")
}
export function updateTaskAssignCC(data) {

    return myApi.create(data, "TaskAssigned/UpdateTaskAssignCC")
}
export function deleteMeetingNote(data) {
    return myApi.delete(data.taskMeetingNoteID, 'TaskMeetingNote/DeleteMeetingNote')
}

export function getAttachmentLinks(id) {

    return myApi.getByID(id, 'TaskAttachment/GetAttachmentLinks')
}
export function getTaskListByUser(id) {

    return myApi.getByID(id, 'TaskMaster/GetTaskListByUser')
}
export function getUsersByTaskID(id) {

    return myApi.getByID(id, 'TaskMaster/GetUsersByTaskID')
}
export function getSelectedTaskFiles(id, userId) {

    return myApi.getByuserId(id, userId, 'TaskMaster/GetSelectedTaskFiles');
}
export function getTaskLinksByID(id) {

    return myApi.getByID(id, 'TaskMaster/GetTaskLinksByID')
}
export function getTaskOpenCommentByUserID(id) {
    return myApi.getByID(id, "TaskComment/GetTaskOpenCommentByUserID")
}
export function updateDescriptionTaskAttachement(data) {

    return myApi.update(data.taskAttachmentID, data, 'TaskMaster/UpdateDescriptionTaskAttachement');
}
export function getTaskMessageBySearch(data) {

    return myApi.getItem(data, 'TaskComment/GetTaskMessageBySearch')
}

export function updateInviteTaskSubject(data) {

    return myApi.update(data.taskCommentId, data, "TaskMaster/UpdateInviteTaskSubject")
}

export function getTaskCommentUnreadClick(id, userId) {

    return myApi.getByuserId(id, userId, 'TaskComment/GetTaskCommentUnreadClick')
}
export function getSubTasksList(id, userId) {

    return myApi.getByuserId(id, userId, 'TaskMaster/GetSubTasksIdsList');
}

export function updateTaskCommentDueExtendDate(data) {

    return myApi.getItem(data, 'TaskComment/UpdateTaskCommentDueExtendDate')
}

export function createTaskMasterFromDocument(data) {

    return myApi.create(data, "TaskMaster/InsertTaskMasterFromDocument")
}

export function multipleTaskUpdateAsRead(data) {

    return myApi.update(data.taskID, data, 'TaskMaster/MultipleTaskUpdateAsRead')
}
export function multipleTaskUpdateAsUnRead(data) {

    return myApi.update(data.taskID, data, 'TaskMaster/MultipleTaskUpdateAsUnRead')
}
export function generateTaskSession(id) {
    return myApi.getByID(id, "TaskMaster/GenerateTaskSession")
}
export function getTaskAttachmentByDocument(id) {

    return myApi.getByID(id, 'TaskAttachment/GetTaskAttachmentByDocument')
}

export function deleteTaskAttachment(data) {

    return myApi.update(data.taskMasterID, data, "TaskComment/DeleteTaskAttachment")
}

export function createTaskUnReadNotes(data) {
    return myApi.create(data, "TaskNotes/InsertTaskUnReadNotes");
}
export function deleteTaskUnReadNotes(data) {
    return myApi.create(data, "TaskNotes/DeleteTaskUnReadNotes");
}

export function updateTaskUnReadNotes(data) {

    return myApi.update(data, data, "TaskNotes/UpdateTaskUnReadNotes")
}
export function getTaskPersonalNotes(data) {

    return myApi.getItem(data, 'TaskNotes/GetTaskPersonalNotes')
}
export function updateTaskUnReadMyDueDate(data) {

    return myApi.update(data.taskCommentID, data, "TaskNotes/UpdateTaskUnReadMyDueDate")
}
export function createTaskMasterByTemplateCase(data) {

    return myApi.create(data, "TaskMaster/InsertTaskMasterByTemplateCase")
}