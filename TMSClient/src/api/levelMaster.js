import myApi from '@/util/api'

export function getLevelMasters() {

    return myApi.getAll('LevelMaster/GetLevelMasters')
}
export function getLevelMaster(data) {

    return myApi.getItem(data, "LevelMaster/GetData")
}
export function createLevelMaster(data) {
    return myApi.create(data, "LevelMaster/InsertLevelMaster")
}

export function updateLevelMaster(data) {

    return myApi.update(data.levelID, data, "LevelMaster/UpdateLevelMaster")
}
export function deleteLevelMaster(data) {

    return myApi.delete(data.levelID, 'LevelMaster/DeleteLevelMaster')
}
export function getCompanies() {

    return myApi.getAll('Plant/GetPlants')
}