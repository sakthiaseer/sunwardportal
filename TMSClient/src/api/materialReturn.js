import myApi from '@/util/api'

export function getAppmaterialReturns() {

    return myApi.getAll('AppmaterialReturn/GetAppmaterialReturns')
  }
  export function getAppmaterialReturnFilter(data) {

    return myApi.getItem(data, "AppmaterialReturn/GetAppmaterialReturnFilter")
  }
  
  export function getAppmaterialReturn(data) {

    return myApi.getItem(data, "AppmaterialReturn/GetData")
}
export function createAppmaterialReturn(data) {
    return myApi.create(data, "AppmaterialReturn/InsertAppmaterialReturn")
}
export function updateAppmaterialReturn(data) {

  return myApi.update(data.materialReturnID, data, "AppmaterialReturn/UpdateAppmaterialReturn")
}
export function deleteAppmaterialReturn(data) {

    return myApi.delete(data.materialReturnID, 'AppmaterialReturn/DeleteAppmaterialReturn')
}
  
