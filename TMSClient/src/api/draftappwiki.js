import myApi from '@/util/api'

export function getApplicationWikiItems (userId, superUser) {

    return myApi.getAll('DraftAppwiki/GetApplicationWiki?userId=' + userId + "&&superUser=" + superUser)
}
export function getApplicationWikiByID (id) {

    return myApi.getByID(id, 'DraftAppwiki/GetApplicationWikiByID')
}
export function getApplicationWiki (data) {

    return myApi.getItem(data, "DraftAppwiki/GetData")
}

export function updateApplicationWiki (data) {

    return myApi.update(data.applicationWikiId, data, "DraftAppwiki/DraftUpdateApplicationWiki")
}
export function deleteApplicationWiki (data) {

    return myApi.delete(data.applicationWikiId, 'DraftAppwiki/DeleteApplicationWiki')
}
export function getApplicationWikiLine (applicationWikiId) {

    return myApi.getAll("DraftAppwiki/getApplicationWikiLine?id=" + applicationWikiId);
}

export function createApplicationWikiLine (data) {
    return myApi.create(data, "DraftAppwiki/InsertApplicationWikiLine")
}
export function createReleaseApplicationWiki (data) {
    return myApi.create(data, "DraftAppwiki/InsertReleaseApplicationWiki")
}

export function updateApplicationWikiLine (data) {

    return myApi.update(data.applicationWikiLineId, data, "DraftAppwiki/UpdateApplicationWikiLine")
}
export function deleteApplicationWikiLine (data) {

    return myApi.delete(data.applicationWikiLineId, 'DraftAppwiki/DeleteApplicationWikiLine')
}

export function getApplicationWikiLineDuty (applicationWikiLineId) {

    return myApi.getAll("DraftAppwiki/getApplicationWikiLineDuty?id=" + applicationWikiLineId);
}

export function createApplicationWikiLineDuty (data) {
    return myApi.create(data, "DraftAppwiki/InsertApplicationWikiLineDuty")
}

export function updateApplicationWikiLineDuty (data) {

    return myApi.update(data.applicationWikiLineDutyId, data, "DraftAppwiki/UpdateApplicationWikiLineDuty")
}
export function deleteApplicationWikiLineDuty (data) {

    return myApi.delete(data.applicationWikiLineDutyId, 'DraftAppwiki/DeleteApplicationWikiLineDuty')
}

export function createApplicationWikiRecurrence (data) {
    return myApi.create(data, "DraftAppwiki/InsertApplicationWikiRecurrence")
}
export function getApplicationWikiRecurrence (id) {

    return myApi.getByID(id, 'DraftAppwiki/GetApplicationWikiRecurrence')
}

export function deleteWikiResponsible (data) {

    return myApi.delete(data.wikiResponsibilityID, 'DraftAppwiki/DeleteWikiResponsible')
}

export function createWikiResponsible (data) {
    return myApi.create(data, "DraftAppwiki/InsertWikiResponsible")
}
export function getWikiResponsibleByID (id) {

    return myApi.getByID(id, 'DraftAppwiki/GetWikiResponsibleByID')
}
export function deleteWikiResponsibles (data) {

    return myApi.update(data.wikiResponsibilityID, data, "DraftAppwiki/DeleteWikiResponsibles")
}
export function getDocumentProfiles () {

    return myApi.getAll('appwiki/GetApplicationWikiProlileList')
}

export function getProfileAutonumber (id) {

    return myApi.getByID(id, 'Appwiki/GetProfileAutonumber')
}
export function getExcel(data) {

    return myApi.downLoadProfileDocument(data, 'DraftAppwiki/GetExcel')
}