import myApi from '@/util/api'

export function getGroupPlanning() {
    return myApi.getAll('GroupPlanning/GetGroupPlanning')
}

export function getNavRefreshGroupPlanning(data) {
    return myApi.getItem(data, 'NAVFunc/GetGroupPlanning')
}
export function getSearchData(data) {

    return myApi.getItem(data, "GroupPlanning/GetSearchData")
}
export function deleteGroupPlanning (data) {

    return myApi.delete(data.groupPlanningId, 'GroupPlanning/DeleteGroupPlanning')
}
export function updateGroupPlanning (data) {

    return myApi.update(data.groupPlanningId, data, "GroupPlanning/UpdateGroupPlanning")
}