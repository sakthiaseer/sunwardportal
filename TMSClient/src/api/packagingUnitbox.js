import myApi from '@/util/api'



export function getPackagingUnitBox(data) {

    return myApi.getItem(data, "PackagingUnitBox/GetData")
}
export function getPackagingUnitBoxs(){
    return myApi.getAll("PackagingUnitBox/GetPackagingUnitBox")
}
export function createPackagingUnitBox(data) {
    return myApi.create(data, "PackagingUnitBox/InsertPackagingUnitBox")
}

export function updatePackagingUnitBox(data) {

    return myApi.update(data.unitBoxId, data, "PackagingUnitBox/UpdatePackagingUnitBox")
}
export function deletePackagingUnitBox(data) {

    return myApi.delete(data.unitBoxId, 'PackagingUnitBox/DeletePackagingUnitBox')
}
export function getPackagingUnitBoxByRefNo(data) {
    return myApi.getItems(data, 'PackagingUnitBox/GetPackagingUnitBoxByRefNo')
  }
