import myApi from '@/util/api'

export function getFPCommonFields() {

    return myApi.getAll('FPCommonField/GetFPCommonFields')
}
export function getFPCommonFieldsId(id) {

    return myApi.getByID(id, 'FPCommonField/GetFPCommonFields')
}
export function getFPCommonField(data) {

    return myApi.getItem(data, "FPCommonField/GetData")
}

export function createFPCommonField(data) {
    return myApi.create(data, "FPCommonField/InsertFPCommonField")
}

export function updateFPCommonField(data) {

    return myApi.update(data.fpCommonFieldID, data, "FPCommonField/UpdateFPCommonField")
}
export function deleteFPCommonField(data) {

    return myApi.delete(data.fpCommonFieldID, 'FPCommonField/DeleteFPCommonField')
}
export function getFPCommonFieldsByRefNo(data) {
    return myApi.getItems(data, 'FPCommonField/GetFPCommonFieldsByRefNo')
}
export function getFPCommonFieldsAll() {

    return myApi.getAll('FPCommonField/GetFPCommonFieldsAll')
}

export function getFPReports(data) {
    return myApi.getItems(data, 'FPCommonField/GetFPReports')
}