import myApi from '@/util/api'

export function getDivisions() {

    return myApi.getAll('Division/GetDivisions')
}
export function getDivision(data) {

    return myApi.getItem(data, "Division/GetData")
}
export function createDivision(data) {
    return myApi.create(data, "Division/InsertDivision")
}

export function updateDivision(data) {

    return myApi.update(data.DivisionID, data, "Division/UpdateDivision")
}
export function deleteDivision(data) {

    return myApi.delete(data.divisionID, 'Division/DeleteDivision')
}
export function getCompanies() {

    return myApi.getAll('Plant/GetPlants')
}
export function getEmployees() {

    return myApi.getAll('Employee/GetEmployees')
}

