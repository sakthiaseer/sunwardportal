import myApi from '@/util/api'

export function getFileProfileSetupFormsById(id) {

    return myApi.getByID(id,'FileProfileSetupForm/GetFileProfileSetupFormsById')
}
export function getFileProfileSetupForm(data) {

    return myApi.getItem(data, "FileProfileSetupForm/GetData")
}
export function createFileProfileSetupForm(data) {
    return myApi.create(data, "FileProfileSetupForm/InsertFileProfileSetupForm")
}

export function updateFileProfileSetupForm(data) {

    return myApi.update(data.fileProfileSetupFormId, data, "FileProfileSetupForm/UpdateFileProfileSetupForm")
}
export function deleteFileProfileSetupForm(data) {

    return myApi.delete(data.fileProfileSetupFormId, 'FileProfileSetupForm/DeleteFileProfileSetupForm')
}
