import myApi from '@/util/api'

export function getFinishProductGeneralInfos() {

  return myApi.getAll('FinishProductGeneralInfo/GetFinishProductGeneralInfos')
}


export function getFinishProductGeneralInfo(data) {

  return myApi.getItem(data, "FinishProductGeneralInfo/GetData")
}
export function updateFinishProductGeneralInfo(data) {

  return myApi.update(data.finishProductGeneralInfoID, data, "FinishProductGeneralInfo/UpdateFinishProductGeneralInfo")
}

export function createFinishProductGeneralInfo(data) {

  return myApi.create(data, "FinishProductGeneralInfo/InsertFinishProductGeneralInfo")
}
export function createFinishProductGeneralInfoStatus(data) {

  return myApi.create(data, "FinishProductGeneralInfo/InsertFinishProductGeneralInfoStatus")
}

export function deleteFinishProductGeneralInfo(data) {

  return myApi.delete(data.finishProductGeneralInfoID, 'FinishProductGeneralInfo/DeleteFinishProductGeneralInfo')
}

export function getFinishProductGeneralInfoLines(id) {

  return myApi.getByID(id, 'FinishProductGeneralInfo/GetFinishProductGeneralInfoLines')
}


export function updateFinishProductGeneralInfoLine(data) {

  return myApi.update(data.finishProductGeneralInfoLineID, data, "FinishProductGeneralInfo/UpdateFinishProductGeneralInfoLine")
}

export function createFinishProductGeneralInfoLine(data) {

  return myApi.create(data, "FinishProductGeneralInfo/InsertFinishProductGeneralInfoLine")
}
export function deleteFinishProductGeneralInfoLine(data) {

  return myApi.delete(data.finishProductGeneralInfoLineID, 'FinishProductGeneralInfo/DeleteFinishProductGeneralInfoLine')
}
export function getFinishProductsListForGeneralInfo() {
  return myApi.getAll('FinishProduct/GetFinishProductsListForGeneralInfo')
}
export function getDocumentLists(userId) {

  return myApi.getByID(userId, 'Folders/GetDocumentLists')
}
export function getPacksizeSearch(data) {

  return myApi.getItem(data, "FinishProductGeneralInfo/GetPacksizeSearch")
}

export function getFinishProductGeneralInfoByManufacuringId(id) {

  return myApi.getByID(id, 'FinishProductGeneralInfo/GetFinishProductGeneralInfoByManufacuringId')
}

export function getCompanyListings() {

  return myApi.getAll('CompanyListing/GetCompanyListings')
}
export function getCompanyListingsForRegistration() {

  return myApi.getAll('CompanyListing/GetCompanyListingsForRegistration')
}

export function uploadGeneralInfoDocuments(data, sessionId) {

  return myApi.uploadFile(data, "FinishProductGeneralInfo/UploadGeneralInfoDocuments?sessionId=" + sessionId)
}
export function getGeneralInfoDocument(sessionId) {

  return myApi.getAll("FinishProductGeneralInfo/GetGeneralInfoDocument?sessionId=" + sessionId)
}
export function deleteGeneralInfoDocuments(data) {

  return myApi.delete(data.finishProductGeneralInfoDocumentID, 'FinishProductGeneralInfo/DeleteGeneralInfoDocuments')
}

export function downLoadGeneralInfoDocument(data) {

  return myApi.downLoadDocument(data, "FinishProductGeneralInfo/DownLoadGeneralInfoDocument")
}