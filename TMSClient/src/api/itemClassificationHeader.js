import myApi from '@/util/api'

export function getItemClassificationHeader(id) {

  return myApi.getByID(id,'ItemClassificationHeader/GetItemClassificationHeaders')
}
export function getData(data) {

  return myApi.getItem(data, "ItemClassificationHeader/GetData")
}
export function createItemClassificationHeader(data) {
  return myApi.create(data, "ItemClassificationHeader/InsertItemClassificationHeader")
}
export function updateItemClassificationHeader(data) {

  return myApi.update(data.itemClassificationHeaderID, data, "ItemClassificationHeader/UpdateItemClassificationHeader")
}
export function deleteItemClassificationHeader(data) {

  return myApi.delete(data.itemClassificationHeaderID, 'ItemClassificationHeader/DeleteItemClassificationHeader')
}
export function getItemClassificationHeadersByRefNo(refNo) {

  return myApi.getByRefNo(refNo, 'ItemClassificationHeader/GetItemClassificationHeadersByRefNo')
}