import myApi from '@/util/api'
export function getPlants() {

    return myApi.getAll('Plant/GetPlants')
}
export function getFulfillmentofAdhocBySearch(item) {
    return myApi.getItem(item, 'FulfillmentofAdhoc/GetFulfillmentofAdhocBySearch');
}
export function syncNAVOrders(item) {
    return myApi.getItem(item, 'FulfillmentofAdhoc/SyncNAVOrders');
}