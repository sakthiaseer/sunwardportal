import myApi from '@/util/api'

export function getCommonMasterMouldChangeParts() {

  return myApi.getAll('CommonMasterMouldChangeParts/GetCommonMasterMouldChangeParts')
}
export function getData(data) {

  return myApi.getItem(data, "CommonMasterMouldChangeParts/GetData")
}
export function createCommonMasterMouldChangeParts(data) {
  return myApi.create(data, "CommonMasterMouldChangeParts/InsertCommonMasterMouldChangeParts")
}
export function updateCommonMasterMouldChangeParts(data) {

  return myApi.update(data.commonMasterMouldChangePartsId, data, "CommonMasterMouldChangeParts/UpdateCommonMasterMouldChangeParts")
}
export function deleteCommonMasterMouldChangeParts(data) {

  return myApi.delete(data.commonMasterMouldChangePartsId, 'CommonMasterMouldChangeParts/DeleteCommonMasterMouldChangeParts')
}
export function getCommonMasterMouldChangePartsByRefNo(data) {
  return myApi.getItems(data, 'CommonMasterMouldChangeParts/GetCommonMasterMouldChangePartsByRefNo')
}
export function uploadDocuments(data, sessionId, DocumentType) {

  return myApi.uploadFile(data, "CommonMasterMouldChangeParts/UploadDocuments?sessionId=" + sessionId + "&&documentType=" + DocumentType)
}
export function getDocument(sessionId, DocumentType) {

  return myApi.getAll("CommonMasterMouldChangeParts/GetDocument?sessionId=" + sessionId + "&&documentType=" + DocumentType)
}
export function deleteDocument(machineDocumentId) {

  return myApi.delete(machineDocumentId, 'CommonMasterMouldChangeParts/DeleteDocument')
}
export function downloadDocument(data) {

  return myApi.downLoadDocument(data, "CommonMasterMouldChangeParts/DownLoadDocument")
}
export function removeDocument(data) {

  return myApi.delete(data, 'CommonMasterMouldChangeParts/DeleteDocument')
}