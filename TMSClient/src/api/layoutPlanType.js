import myApi from '@/util/api'

export function getLayoutPlanTypes() {

  return myApi.getAll('LayoutPlanType/GetLayoutPlanTypes')
}
export function getLayoutPlanType(data) {

  return myApi.getItem(data, "LayoutPlanType/GetData")
}

export function createLayoutPlanType(data) {
  return myApi.create(data, "LayoutPlanType/InsertLayoutPlanType")
}

export function updateLayoutPlanType(data) {

  return myApi.update(data.layoutPlanTypeID, data, "LayoutPlanType/UpdateLayoutPlanType")
}
export function deleteLayoutPlanType(data) {

  return myApi.delete(data.layoutPlanTypeID, 'LayoutPlanType/DeleteLayoutPlanType')
}
export function deleteICTLayoutPlanType(data) {

  return myApi.delete(data.ictLayoutTypeID, 'LayoutPlanType/DeleteICTLayoutPlanType')
}


