import myApi from '@/util/api'

export function getDeviceCatalogMasters() {

    return myApi.getAll('DeviceCatalogMaster/GetDeviceCatalogMasters')
}
export function getDeviceCatalogByID(id) {

    return myApi.getByID(id, "DeviceCatalogMaster/GetDeviceCatalogByID")
  }
export function getDeviceCatalogMaster(data) {

    return myApi.getItem(data, "DeviceCatalogMaster/GetData")
}

export function createDeviceCatalogMaster(data) {
    return myApi.create(data, "DeviceCatalogMaster/InsertDeviceCatalogMaster")
}

export function updateDeviceCatalogMaster(data) {

    return myApi.update(data.deviceCatalogMasterID, data, "DeviceCatalogMaster/UpdateDeviceCatalogMaster")
}
export function deleteDeviceCatalogMaster(data) {

    return myApi.delete(data.deviceCatalogMasterID, 'DeviceCatalogMaster/DeleteDeviceCatalogMaster')
}
export function getCalibrationTypes() {

    return myApi.getAll('DeviceCatalogMaster/GetCalibrationTypes')
}