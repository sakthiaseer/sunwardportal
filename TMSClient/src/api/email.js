import myApi from '@/util/api'

export function getReceiveEmails () {

    return myApi.getAll('Email/ReceiveEmails')
}
export function getEmailTypeList () {

    return myApi.getAll('Email/GetEmailTypeList')
}
export function createReceiveEmail (data) {
    return myApi.create(data, "Email/InsertReceiveEmail")
}
export function updateReceiveEmail (data) {

    return myApi.update(data.receiveEmailId, data, "Email/UpdateReceiveEmail")
}