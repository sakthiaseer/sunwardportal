import myApi from '@/util/api'

export function getSunwardGroupCompanys() {

  return myApi.getAll('SunwardGroupCompany/GetSunwardGroupCompanys')
}
export function getSunwardGroupCompany(data) {

  return myApi.getItem(data, "SunwardGroupCompany/GetData")
}

export function createSunwardGroupCompany(data) {
  return myApi.create(data, "SunwardGroupCompany/InsertSunwardGroupCompany")
}

export function updateSunwardGroupCompany(data) {

  return myApi.update(data.sunwardGroupCompanyID, data, "SunwardGroupCompany/UpdateSunwardGroupCompany")
}
export function deleteSunwardGroupCompany(data) {

  return myApi.delete(data.sunwardGroupCompanyID, 'SunwardGroupCompany/DeleteSunwardGroupCompany')
}



