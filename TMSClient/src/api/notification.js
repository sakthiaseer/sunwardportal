import myApi from '@/util/api'

export function getNotification(id) {
  return myApi.getByID(id, 'Notification/GetNotifications')
}
export function updateNotification(data) {

  return myApi.update(data.notificationId, data, "Notification/UpdateNotification")
}

export function createPaging(data) {
  return myApi.create(data, "UserSettings/InsertUserSettings")
}

export function getNotificationHandlers(id) {
  return myApi.getByID(id, 'NotificationHandler/GetNotificationHandlers')
}
export function getNotificationList(id) {
  return myApi.getByID(id, 'NotificationHandler/GetNotificationList')
}
export function getUserSettings(id) {
  return myApi.getByID(id, 'UserSettings/GetUserSettings')
}

export function updateUserSettings(data) {

  return myApi.update(data.userID, data, "UserSettings/UpdateUserSettings")
}