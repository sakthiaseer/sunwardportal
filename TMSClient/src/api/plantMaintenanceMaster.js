import myApi from '@/util/api'

export function getPlantMaintenanceEntryMasters() {

    return myApi.getAll('PlantMaintenanceEntryMaster/GetPlantMaintenanceEntryMasters')
  }
  export function getPlantMaintenanceEntryMasterByID(id) {
  
    return myApi.getByID(id, "PlantMaintenanceEntryMaster/GetPlantMaintenanceEntryMasterByID")
  }
  export function getPlantMaintenanceEntryMaster(data) {

    return myApi.getItem(data, "PlantMaintenanceEntryMaster/GetData")
}

export function updatePlantMaintenanceEntryMaster(data) {

  return myApi.update(data.plantEntryLineID, data, "PlantMaintenanceEntryMaster/UpdatePlantMaintenance")
}
  
