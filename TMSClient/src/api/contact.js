import myApi from '@/util/api'

export function getContacts() {

    return myApi.getAll('Contacts/GetContacts')
}
export function getContact(data) {

    return myApi.getItem(data, "Contacts/GetData")
}
export function getOwner() {

    return myApi.getAll('ApplicationUser/GetApplicationUsers')
}
export function getCountries() {

    return myApi.getAll('Country/GetCountries')
}
export function getStates() {

    return myApi.getAll('State/GetStates')
}
export function createContact(data) {
    return myApi.create(data, "Contacts/InsertContacts")
}

export function updateContact(data) {
    console.log(data);
    return myApi.update(data.contactId, data, "Contacts/UpdateContacts")
}
export function deleteContact(data) {

    return myApi.delete(data.contactId, 'Contacts/DeleteContacts')
}