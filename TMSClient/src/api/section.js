import myApi from '@/util/api'

export function getSections() {

    return myApi.getAll('Section/GetSections')
}
export function getSection(data) {

    return myApi.getItem(data, "Section/GetData")
}
export function createSection(data) {
    return myApi.create(data, "Section/InsertSection")
}

export function updateSection(data) {

    return myApi.update(data.sectionID, data, "Section/UpdateSection")
}
export function deleteSection(data) {

    return myApi.delete(data.sectionID, 'Section/DeleteSection')
}
export function getDepartments() {

    return myApi.getAll('Department/GetDepartments')
}