import myApi from '@/util/api'

export function getPackagingItemHistory() {

    return myApi.getAll('PackagingItemHistory/GetPackagingItemHistorys')
}
export function getData(data) {

    return myApi.getItem(data, "PackagingItemHistory/GetPackagingItemHistory")
}
export function createPackagingItemHistory(data) {
    return myApi.create(data, "PackagingItemHistory/InsertPackagingItemHistory")
}
export function updatePackagingItemHistory(data) {

    return myApi.update(data.packagingItemHistoryId, data, "PackagingItemHistory/UpdatePackagingItemHistory")
}
export function deletePackagingItemHistory(data) {

    return myApi.delete(data.packagingItemHistoryId, 'PackagingItemHistory/DeletePackagingItemHistory')
}


export function getPackagingItemHistoryLine(packagingItemHistoryId) {
    return myApi.getAll('PackagingItemHistory/GetPackagingItemHistoryLine/?id=' + packagingItemHistoryId)
}
export function createPackagingItemHistoryLine(data) {
    return myApi.create(data, "PackagingItemHistory/InsertPackagingItemHistoryLine")
}
export function updatePackagingItemHistoryLine(data) {

    return myApi.update(data.packagingItemHistoryLineId, data, "PackagingItemHistory/UpdatePackagingItemHistoryLine")
}
export function deletePackagingItemHistoryLine(data) {

    return myApi.delete(data.packagingItemHistoryLineId, 'PackagingItemHistory/DeletePackagingItemHistoryLine')
}
export function getDataLine(data) {

    return myApi.getItem(data, "PackagingItemHistory/GetPackagingItemHistoryLineData")
}
export function getApplicationMasterDetailById(id) {
    return myApi.getAll("PackagingItemHistory/GetApplicationMasterDetailById/" + id)
}


export function uploadPackagingItemHistoryDocuments(data, sessionId) {

    return myApi.uploadFile(data, "PackagingItemHistory/UploadPackagingItemHistoryLineDocuments?sessionId=" + sessionId)
}
export function getPackagingItemHistoryDocuments(id) {

    return myApi.getByID(id, "PackagingItemHistory/GetPackagingItemHistoryLineDocument")
}
export function deletePackagingItemHistoryDocuments(packagingHistoryItemLineDocumentId) {


    return myApi.delete(packagingHistoryItemLineDocumentId, 'PackagingItemHistory/DeletePackagingItemHistoryLineDocument')
}
export function downloadPackagingItemHistoryDocuments(data) {

    return myApi.downLoadDocument(data, "PackagingItemHistory/DownLoadPackagingItemHistoryLineDocument")
}

export function getPackagingHistoryLineDocumentById(packagingHistoryItemLineDocumentId) {

    return myApi.getByID(packagingHistoryItemLineDocumentId, 'PackagingItemHistory/GetPackagingHistoryLineDocumentById')
}

