import myApi from '@/util/api'

export function getBompackingMaster() {

    return myApi.getAll('BOMPackingMaster/GetBompackingMaster')
}
export function getPackingMaster(data) {

    return myApi.getItem(data, "BOMPackingMaster/GetData")
}
export function getBompackingMasterLineID(id) {

    return myApi.getByID(id, 'BOMPackingMaster/GetBompackingMasterLineID')
}
export function createBompackingMaster(data) {
    return myApi.create(data, "BOMPackingMaster/InsertBompackingMaster")
}

export function updateBompackingMaster(data) {

    return myApi.update(data.bompackingId, data, "BOMPackingMaster/UpdateBompackingMaster")
}
export function deleteBompackingMaster(data) {

    return myApi.delete(data.bompackingId, 'BOMPackingMaster/DeleteBompackingMaster')
}



export function createBompackingMasterLine(data) {
    return myApi.create(data, "BOMPackingMaster/InsertBompackingMasterLine")
}

export function updateBompackingMasterLine(data) {

    return myApi.update(data.bompackingMasterLineId, data, "BOMPackingMaster/UpdateBompackingMasterLine")
}
export function deleteBompackingMasterLine(data) {

    return myApi.delete(data.bompackingMasterLineId, 'BOMPackingMaster/DeleteBompackingMasterLine')
}


//




