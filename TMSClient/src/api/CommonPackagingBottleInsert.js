import myApi from '@/util/api'

export function getCommonPackagingBottleInsert (refNo) {

  return myApi.getByRefNo(refNo,'CommonPackagingBottleInsert/GetCommonPackagingBottleInsert')
}
export function getData (data) {

  return myApi.getItem(data, "CommonPackagingBottleInsert/GetData")
}
export function createCommonPackagingBottleInsert (data) {
  return myApi.create(data, "CommonPackagingBottleInsert/InsertCommonPackagingBottleInsert")
}
export function updateCommonPackagingBottleInsert (data) {

  return myApi.update(data.bottleInsertSpecificationId, data, "CommonPackagingBottleInsert/UpdateCommonPackagingBottleInsert")
}
export function deleteCommonPackagingBottleInsert (data) {

  return myApi.delete(data.bottleInsertSpecificationId, 'CommonPackagingBottleInsert/DeleteCommonPackagingBottleInsert')
}
export function getCommonPackagingBottleInsertByRefNo(data) {
  return myApi.getItems(data, 'CommonPackagingBottleInsert/GetCommonPackagingBottleInsertByRefNo')
}