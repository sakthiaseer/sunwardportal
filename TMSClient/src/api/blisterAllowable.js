import myApi from '@/util/api'

export function getTeams() {

    return myApi.getAll('TeamMaster/GetTeams')
}
export function getTeam(data) {

    return myApi.getItem(data, "TeamMaster/GetData")
}

export function createTeam(data) {
    return myApi.create(data, "TeamMaster/InsertTeam")
}

export function updateTeam(data) {

    return myApi.update(data.teamMasterID, data, "TeamMaster/UpdateTeam")
}
export function deleteTeam(data) {

    return myApi.delete(data.teamMasterID, 'TeamMaster/DeleteTeam')
}