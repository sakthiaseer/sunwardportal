import myApi from '@/util/api'

export function getAddress() {

    return myApi.getAll('Address/GetAddress')
}
export function getAddresses(data) {

    return myApi.getItem(data, "Address/GetData")
}
export function getAddressesByID(id) {

    return myApi.getByID(id, 'Address/GetAddressByID')
}
export function createAddress(data) {
    return myApi.create(data, "Address/InsertAddress")
}

export function updateAddress(data) {

    return myApi.update(data.addressID, data, "Address/UpdateAddress")
}
export function deleteAddress(data) {

    return myApi.delete(data.addressID, 'Address/DeleteAddress')
}
