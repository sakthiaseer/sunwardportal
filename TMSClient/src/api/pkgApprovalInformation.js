import myApi from '@/util/api'

export function getPKGApprovalInformation() {

    return myApi.getAll('PKGApprovalInformation/GetPKGApprovalInformation')
}
export function getSearchData(data) {

    return myApi.getItem(data, "PKGApprovalInformation/GetData")
}
export function createPKGApprovalInformation(data) {
    return myApi.create(data, "PKGApprovalInformation/InsertPKGApprovalInformation")
}

export function updatePKGApprovalInformation(data) {

    return myApi.update(data.pkgapprovalInformationId, data, "PKGApprovalInformation/UpdatePKGApprovalInformation")
}
export function deletePKGApprovalInformation(data) {

    return myApi.delete(data.pkgapprovalInformationId, 'PKGApprovalInformation/DeletePKGApprovalInformation')
}

export function getPKGRegisteredPackingInformationByID(id) {

    return myApi.getByID(id, 'PKGApprovalInformation/GetPKGRegisteredPackingInformationByID')
}

export function createPKGRegisteredPackingInformation(data) {
    return myApi.create(data, "PKGApprovalInformation/InsertPKGRegisteredPackingInformation")
}

export function updatePKGRegisteredPackingInformation(data) {

    return myApi.update(data.pkgregisteredPackingInformationId, data, "PKGApprovalInformation/UpdatePKGRegisteredPackingInformation")
}
export function deletePKGRegisteredPackingInformation(data) {

    return myApi.delete(data.pkgregisteredPackingInformationId, 'PKGApprovalInformation/DeletePKGRegisteredPackingInformation')
}

export function getPKGInformationByPackSizeByID(id) {

    return myApi.getByID(id, 'PKGApprovalInformation/GetPKGInformationByPackSizeByID')
}

export function createPKGInformationByPackSize(data) {
    return myApi.create(data, "PKGApprovalInformation/InsertPKGInformationByPackSize")
}

export function updatePKGInformationByPackSize(data) {

    return myApi.update(data.pkginformationByPackSizeId, data, "PKGApprovalInformation/UpdatePKGInformationByPackSize")
}
export function deletePKGInformationByPackSize(data) {

    return myApi.delete(data.pkginformationByPackSizeId, 'PKGApprovalInformation/DeletePKGInformationByPackSize')
}


