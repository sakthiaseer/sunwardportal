import myApi from '@/util/api'

export function getDocumentRolePermissions() {

    return myApi.getAll('DocumentRolePermission/GetDocumentRolePermission')
}
export function getDocuments () {
    return myApi.getAll('Documents/GetDocuments')
  }
  export function getDocumentPermissions() {

    return myApi.getAll('DocumentPermission/GetDocumentPermissions')
}
export function getDocumentRoles() {

    return myApi.getAll('DocumentRole/GetDocumentRoles')
}
export function getDocumentRoleIDByPermission() {

    return myApi.getAll('DocumentRole/GetDocumentRoleIDByPermission')
}
export function getDocumentPermission(data) {

    return myApi.getItem(data, "DocumentPermission/GetData")
}
export function getDocumentPermissionByRoleID(id) {

    return myApi.getByID(id, 'DocumentPermission/GetDocumentPermissionByRoleID')
}
export function createDocumentPermission(data) {
    return myApi.create(data, "DocumentPermission/InsertDocumentPermission")
}
export function getDocumentUserRolesByFolderID(id) {

    return myApi.getByID(id, 'Folders/GetDocumentUserRoleByFolderID')
}
export function getDocumentUserRoleByDocumentID(id) {

    return myApi.getByID(id, 'Folders/GetDocumentUserRoleByDocumentID')
}
export function updateDocumentPermission(data) {

    return myApi.update(data.documentPermissionID, data, "DocumentPermission/UpdateDocumentPermission")
}
export function deleteDocumentUserRoles(data) {

    return myApi.update(data.documentUserRoleID, data, "DocumentUserRole/DeleteDocumentUserRoles")
}
export function deleteDocumentPermission(data) {

    return myApi.delete(data.documentPermissionID, 'DocumentPermission/DeleteDocumentPermission')
}
export function deleteDocumentUserRole(data) {

    return myApi.delete(data.documentUserRoleID, 'DocumentUserRole/DeleteDocumentUserRole')
}
// export function getDocumentRoleWithoutPermission() {

//     return myApi.getAll('DocumentRole/GetDocumentRoleWithoutPermission')
// }