import myApi from '@/util/api'
export function getCommonPackingInformation (id) {
    return myApi.getByID(id, "CommonPackingInformation/GetCommonPackingInformation")
}
export function createCommonPackingInformation (data) {
    return myApi.create(data, "CommonPackingInformation/InsertCommonPackingInformation")
}
export function updateCommonPackingInformation (data) {

    return myApi.update(data.packingInformationId, data, "CommonPackingInformation/UpdateCommonPackingInformation")
}
export function createCommonPackingInformationLine (data) {
    return myApi.create(data, "CommonPackingInformation/InsertCommonPackingInformationLine")
}
export function updateCommonPackingInformationLine (data) {

    return myApi.update(data.packingInformationLineId, data, "CommonPackingInformation/UpdateCommonPackingInformationLine")
}
export function getCommonPackingInformationLine (id) {
    return myApi.getByID(id, "CommonPackingInformation/GetCommonPackingInformationLine")
}
export function deleteCommonPackingInformationLine (data) {

    return myApi.delete(data.packingInformationLineId, 'CommonPackingInformation/DeleteCommonPackingInformationLine')
  }
