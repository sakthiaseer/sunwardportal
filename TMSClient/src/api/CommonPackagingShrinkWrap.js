import myApi from '@/util/api'

export function getCommonPackagingShrinkWrap (refNo) {

  return myApi.getByRefNo(refNo,'CommonPackagingShrinkWrap/GetCommonPackagingShrinkWrap')
}
export function getData (data) {

  return myApi.getItem(data, "CommonPackagingShrinkWrap/GetData")
}
export function createCommonPackagingShrinkWrap (data) {
  return myApi.create(data, "CommonPackagingShrinkWrap/InsertCommonPackagingShrinkWrap")
}
export function updateCommonPackagingShrinkWrap (data) {

  return myApi.update(data.shrinkWrapSpecificationId, data, "CommonPackagingShrinkWrap/UpdateCommonPackagingShrinkWrap")
}
export function deleteCommonPackagingShrinkWrap (data) {

  return myApi.delete(data.shrinkWrapSpecificationId, 'CommonPackagingShrinkWrap/DeleteCommonPackagingShrinkWrap')
}
export function getCommonPackagingShrinkWrapByRefNo(data) {
  return myApi.getItems(data, 'CommonPackagingShrinkWrap/GetCommonPackagingShrinkWrapByRefNo')
}