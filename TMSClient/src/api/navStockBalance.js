import myApi from '@/util/api'

// export function getDistMonthlyStockBalances() {

//     return myApi.getAll('NavStockBalance/GetDistMonthlyStockBalance')
// }
export function getDistMonthlyStockBalances(item) {
    return myApi.getItem(item, 'NavStockBalance/GetDistMonthlyStockBalance');
}
export function getNavMonthlyStockBalance(item) {

    return myApi.getItem(item, 'NavStockBalance/GetNavMonthlyStockBalance')
}

export function getNavStockBalance(item) {

    return myApi.getItem(item, 'NAVFunc/GetNAVStockBalance')
}

export function getDistMonthlyStockBalance(data) {

    return myApi.getItem(data, "NavStockBalance/GetData")
}
export function getDataNavMonthlyStockBalance(data) {

    return myApi.getItem(data, "NavStockBalance/GetDataNavMonthlyStockBalance")
}

export function createDistMonthlyStockBalance(data) {
    return myApi.create(data, "NavStockBalance/InsertDistMonthlyStockBalance")
}

export function updateDistMonthlyStockBalance(data) {

    return myApi.update(data.avnavStockBalID, data, "NavStockBalance/UpdateDistMonthlyStockBalance")
}
export function updateNavMonthlyStockBalance(data) {

    return myApi.update(data.avnavStockBalID, data, "NavStockBalance/UpdateNavMonthlyStockBalance")
}
export function deleteDistMonthlyStockBalance(data) {

    return myApi.delete(data.avnavStockBalID, 'NavStockBalance/DeleteDistMonthlyStockBalance')
}
export function uploadDocuments(data) {

    return myApi.uploadFile(data, "NavStockBalance/UploadDocuments");
}
export function importuploadDocuments(data) {

    return myApi.uploadFile(data, "NavStockBalance/ImportuploadDocuments");
}

export function saveNavItemStockBalance(data) {
    return myApi.create(data, "NavStockBalance/SaveNavItemStockBalance")
}

export function updateNavItemStockBalance(data) {

    return myApi.update(data.navStockBalanceId, data, "NavStockBalance/UpdateNavItemStockBalance")
}

export function deleteNavItemStockBalance(data) {

    return myApi.delete(data.navStockBalanceId, 'NavStockBalance/DeleteNavItemStockBalance')
}

export function getNavItemStockBalanceById(id) {
    return myApi.getByID(id, 'NavStockBalance/GetNavItemStockBalanceById')
}
 export function getNavCustomersDropDown() {

     return myApi.getAll('NavCustomer/GetNavCustomersDropDown')
 }