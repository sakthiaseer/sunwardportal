import myApi from '@/util/api'
export function getCommonFieldsProductionMachineLineLine(id) {

    return myApi.getByID(id, "CommonFieldsProductionMachineLineLine/GetCommonFieldsProductionMachineLineLine")
}
export function createCommonFieldsProductionMachineLineLine(data) {
    return myApi.create(data, "CommonFieldsProductionMachineLineLine/InsertCommonFieldsProductionMachineLineLine")
}

export function updateCommonFieldsProductionMachineLineLine(data) {

    return myApi.update(data.commonFieldsProductionMachineLineLineId, data, "CommonFieldsProductionMachineLineLine/UpdateCommonFieldsProductionMachineLineLine")
}
export function deleteCommonFieldsProductionMachineLineLine(data) {

    return myApi.delete(data.commonFieldsProductionMachineLineLineId, 'CommonFieldsProductionMachineLineLine/DeleteCommonFieldsProductionMachineLineLine')
}
export function getGetCommonFieldsProductionMachineLineLineByRefNo(data) {
    return myApi.getItems(data, 'CommonFieldsProductionMachineLineLine/GetCommonFieldsProductionMachineLineLineRefByNo')
  }