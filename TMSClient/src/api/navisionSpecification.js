import myApi from '@/util/api'

export function getNavisionSpecifications () {
    return myApi.getAll('NavisionSpecification/GetNavisionSpecifications')
}

export function createNavisionSpecifications (data) {
    return myApi.create(data, "NavisionSpecification/InsertNavisionSpecification")
}
export function updateNavisionSpecifications (data) {

    console.log(data);
    return myApi.update(data.navisionSpecificationId, data, "NavisionSpecification/UpdateNavisionSpecification")
}
export function deleteNavisionSpecifications (data) {

    return myApi.delete(data.navisionSpecificationId, 'NavisionSpecification/DeleteNavisionSpecification')
}
export function getNavisionSpecificationsByRefNo (data) {
    return myApi.getItems(data, 'NavisionSpecification/GetNavisionSpecificationsByRefNo')
}