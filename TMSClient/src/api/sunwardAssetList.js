import myApi from '@/util/api'

export function getSunwardAssetLists() {

  return myApi.getAll('SunwardAssetList/GetSunwardAssetLists')
}
export function getSunwardAssetListByID(id) {

  return myApi.getByID(id, "SunwardAssetList/GetSunwardAssetList")
}
export function getSunwardAssetList(data) {

    return myApi.getItem(data, "SunwardAssetList/GetData")
  }
export function createSunwardAssetList(data) {
  return myApi.create(data, "SunwardAssetList/InsertSunwardAssetList")
}

export function updateSunwardAssetList(data) {

  return myApi.update(data.sunwardAssetListID, data, "SunwardAssetList/UpdateSunwardAssetList")
}
export function deleteSunwardAssetList(data) {

  return myApi.delete(data.sunwardAssetListID, 'SunwardAssetList/DeleteSunwardAssetList')
}
export function getPlants() {

  return myApi.getAll('Plant/GetPlants')
}

