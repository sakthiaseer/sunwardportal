import myApi from '@/util/api'

export function getNoticesAll() {

    return myApi.getAll('Notice/GetNoticesAll')
}

export function getNotices(id) {

    return myApi.getByID(id,'Notice/GetNotices')
}
export function getNoticeSearch(id) {

    return myApi.getItem(id,'Notice/GetNoticeSearch')
}
export function getNoticesLogin() {

    return myApi.getAll('Notice/GetNoticesLogIn')
}
export function getNotice(data) {

    return myApi.getItem(data, "Notice/GetData")
}
export function createNotice(data) {
    return myApi.create(data, "Notice/InsertNotice")
}

export function updateNotice(data) {

    return myApi.update(data.noticeId, data, "Notice/UpdateNotice")
}
export function deleteNotice(data) {

    return myApi.delete(data.noticeId, 'Notice/DeleteNotice')
}

