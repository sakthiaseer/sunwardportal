import myApi from '@/util/api'

export function getTaskAppointments() {

    return myApi.getAll('TaskAppointment/GetTaskAppointments')
}
export function getTaskAppointment(data) {

    return myApi.getItem(data, "TaskAppointment/GetData")
}
export function createTaskAppointment(data) {
    return myApi.create(data, "TaskAppointment/InsertTaskAppointment")
}

export function updateTaskAppointment(data) {

    return myApi.update(data.taskAppointmentID, data, "TaskAppointment/UpdateTaskAppointment")
}
export function deleteTaskAppointment(data) {

    return myApi.delete(data.taskAppointmentID, 'TaskAppointment/DeleteTaskAppointment')
}
export function getTaskAppointmentsByID(id, userId) {

    return myApi.getByuserId(id, userId, 'TaskAppointment/GetTaskAppointmentsByID')
}
export function updateTaskAssigned(data) {

    return myApi.update(data.taskAssignedID, data, "TaskAssigned/UpdateTaskAssigned")
}
export function getProjects() {

    return myApi.getAll('Project/GetProjects')
}
export function getTagMaster() {

    return myApi.getAll('TagMaster/GetTagMaster')
}

export function updateInviteTaskDiscussion(data) {

    return myApi.update(data.taskAppointmentId, data, "TaskAppointment/UpdateInviteTaskDiscussion")
}