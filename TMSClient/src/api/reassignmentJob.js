import myApi from '@/util/api'

export function getReAssignmentJob () {

    return myApi.getAll('ReAssignmentJob/GetReAssignmentJob')
}
export function getSearchData (data) {

    return myApi.getItem(data, "ReAssignmentJob/GetData")
}

export function createReAssignmentJob (data) {
    return myApi.create(data, "ReAssignmentJob/InsertReAssignmentJob")
}

export function updateReAssignmentJob (data) {

    return myApi.update(data.reAssignmentJobId, data, "ReAssignmentJob/UpdateReAssignmentJob")
}
export function deleteReAssignmentJob (data) {

    return myApi.delete(data.reAssignmentJobId, 'ReAssignmentJob/DeleteReAssignmentJob')
}
