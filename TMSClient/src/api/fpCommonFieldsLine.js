import myApi from '@/util/api'

export function getFPCommonFieldLines(id) {

    return myApi.getByID(id, 'FPCommonFieldLine/GetFPCommonFieldLines')
}
export function getFPCommonFieldLine(data) {

    return myApi.getItem(data, "FPCommonFieldLine/GetData")
}

export function createFPCommonFieldLine(data) {
    return myApi.create(data, "FPCommonFieldLine/InsertFPCommonFieldLine")
}

export function updateFPCommonFieldLine(data) {

    return myApi.update(data.fpCommonFieldLineID, data, "FPCommonFieldLine/UpdateFPCommonFieldLine")
}
export function deleteFPCommonFieldLine(data) {

    return myApi.delete(data.fpCommonFieldLineID, 'FPCommonFieldLine/DeleteFPCommonFieldLine')
}

export function getItemClassificationHeaders() {

    return myApi.getAll('ItemClassificationHeader/GetItemClassificationHeaders')
}
export function getItemClassificationHeadersDropdown() {

    return myApi.getAll('ItemClassificationHeader/GetItemClassificationHeadersDropdown')
}
export function getItemClassificationHeadersDropdownMATItems() {

    return myApi.getAll('ItemClassificationHeader/GetItemClassificationHeadersDropdownMATItems')
}
export function getItemClassificationHeadersByClassificationID(id) {

    return myApi.getByID(id, 'ItemClassificationHeader/GetItemClassificationHeadersByClassificationID')
}

