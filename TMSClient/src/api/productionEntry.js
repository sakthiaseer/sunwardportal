import myApi from '@/util/api'

export function getProductionEntrys() {

  return myApi.getAll('ProductionEntry/GetProductionEntrys')
}
export function getProductionEntryByID(id) {

  return myApi.getByID(id, "ProductionEntry/GetProductionEntryByID")
}
export function getProductionEntry(data) {

  return myApi.getItem(data, "ProductionEntry/GetData")
}
export function getProductionEntryFilter(data) {

  return myApi.getItem(data, "ProductionEntry/GetProductionEntryFilter")
}
export function updateProductionEntry(data) {

  return myApi.update(data.productionEntryID, data, "ProductionEntry/UpdateProductionEntry")
}

export function getByCompany(data) {
  return myApi.getItem(data, "ProductionEntry/GetByCompany")
}
export function getByAction(data) {
  return myApi.getItem(data, "ProductionEntry/GetByAction")
}
export function getByLocation(data) {
  return myApi.getItem(data, "ProductionEntry/GetByLocation")
}
export function getProductionEntryByCompany(data) {
  return myApi.getItem(data, "ProductionEntry/GetProductionEntryByCompany")
}

export function getProductionEntryByAction(data) {
  return myApi.getItem(data, "ProductionEntry/GetProductionEntryByAction")
}






