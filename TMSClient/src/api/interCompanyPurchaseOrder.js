import myApi from '@/util/api'

export function getInterCompanyPurchaseOrder() {

    return myApi.getAll('InterCompanyPurchaseOrder/GetInterCompanyPurchaseOrder')
}
export function getData(data) {

    return myApi.getItem(data, "InterCompanyPurchaseOrder/GetDataInterCompanyPurchaseOrder")
}
export function createInterCompanyPurchaseOrder(data) {
    return myApi.create(data, "InterCompanyPurchaseOrder/InsertInterCompanyPurchaseOrder")
}
export function updateInterCompanyPurchaseOrder(data) {

    return myApi.update(data.InterCompanyPurchaseOrderId, data, "InterCompanyPurchaseOrder/UpdateInterCompanyPurchaseOrder")
}
export function deleteInterCompanyPurchaseOrder(data) {

    return myApi.delete(data.interCompanyPurchaseOrderId, 'InterCompanyPurchaseOrder/DeleteInterCompanyPurchaseOrder')
}


export function getInterCompanyPurchaseOrderLine(interCompanyPurchaseOrderId) {
    return myApi.getAll('InterCompanyPurchaseOrder/GetInterCompanyPurchaseOrderLine/?id=' + interCompanyPurchaseOrderId)
}
export function createInterCompanyPurchaseOrderLine(data) {
    return myApi.create(data, "InterCompanyPurchaseOrder/InsertInterCompanyPurchaseOrderLine")
}
export function updateInterCompanyPurchaseOrderLine(data) {

    return myApi.update(data.interCompanyPurchaseOrderLineId, data, "InterCompanyPurchaseOrder/UpdateInterCompanyPurchaseOrderLine")
}
export function deleteInterCompanyPurchaseOrderLine(data) {

    return myApi.delete(data.interCompanyPurchaseOrderLineId, 'InterCompanyPurchaseOrder/DeleteInterCompanyPurchaseOrderLine')
}

export function getDescriptionExceptItems(id) {

    return myApi.getByID(id, 'NavItem/GetDescriptionExceptItems')
}



//  export function uploadInterCompanyPurchaseOrderDocuments(data,sessionId) {

//     return myApi.uploadFile(data, "InterCompanyPurchaseOrder/UploadInterCompanyPurchaseOrderDocuments?sessionId="+sessionId)
//   }
//   export function getInterCompanyPurchaseOrderDocuments(id) {

//     return myApi.getByID(id,"InterCompanyPurchaseOrder/GetInterCompanyPurchaseOrderDocument")
// }
// export function deleteInterCompanyPurchaseOrderDocuments(InterCompanyPurchaseOrderDocumentId) {

//     return myApi.delete(InterCompanyPurchaseOrderDocumentId, 'InterCompanyPurchaseOrder/DeleteInterCompanyPurchaseOrderDocument')
// }
// export function downloadInterCompanyPurchaseOrderDocuments(data) {

//     return myApi.downLoadDocument(data, "InterCompanyPurchaseOrder/DownLoadInterCompanyPurchaseOrderDocument")
//   }


