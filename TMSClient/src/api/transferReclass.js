import myApi from '@/util/api'

// Transfer Reclass Entry JS Files Created By Aravinth

export function getApptransferRelcassEntrys() {

    return myApi.getAll('ApptransferRelcassEntry/GetApptransferReclassEntrys')
  }
  export function getApptransferRelcassEntryFilter(data) {

    return myApi.getItem(data, "ApptransferRelcassEntry/GetApptransferRelcassEntryFilter")
  }
  export function getApptransferRelcassEntryItemByID(id) {

    return myApi.getByID(id, "ApptransferRelcassEntry/GetApptransferRelcassEntryItemByID")
  }
  
  export function getApptransferRelcassEntry(data) {

    return myApi.getItem(data, "ApptransferRelcassEntry/GetData")
}
export function createApptransferRelcassEntry(data) {
    return myApi.create(data, "ApptransferRelcassEntry/InsertApptransferRelcassEntry")
}
export function updateApptransferRelcassEntry(data) {

  return myApi.update(data.transferReclassID, data, "ApptransferRelcassEntry/UpdateApptransferRelcassEntry")
}
export function deleteApptransferRelcassEntry(data) {

    return myApi.delete(data.transferReclassID, 'ApptransferRelcassEntry/DeleteApptransferRelcassEntry')
}

// Transfer Reclass Entry Lines JS Files Created By Aravinth

export function getApptransferReclassEntrysByID(id) {

  return myApi.getByID(id, "ApptransferRelcassLines/GetApptransferReclassEntrysByID")
}
export function getApptransferReclassEntrys() {

  return myApi.getAll('ApptransferRelcassLines/GetApptransferReclassEntrys')
}
// export function getApptransferReclassEntryLines() {

//     return myApi.getAll('ApptransferRelcassLines/GetApptransferReclassEntrys')
//   }
  export function getApptransferRelcassEntryLineFilter(data) {

    return myApi.getItem(data, "ApptransferRelcassLines/GetApptransferRelcassEntryLineFilter")
  }
  export function getApptransferRelcassEntryLineItemByID(id) {

    return myApi.getByID(id, "ApptransferRelcassLines/GetApptransferReclassEntrysByID")
  }
  
  export function getApptransferReclassEntry(data) {

    return myApi.getItem(data, "ApptransferRelcassLines/GetData")
}
export function createApptransferRelcassLine(data) {
    return myApi.create(data, "ApptransferRelcassLines/InsertApptransferReclassLine")
}
export function updateApptransferRelcassLine(data) {

  return myApi.update(data.apptransferRelcassLineID, data, "ApptransferRelcassLines/UpdateApptransferReclassLine")
}
export function deleteApptransferRelcassLine(data) {

    return myApi.delete(data.apptransferRelcassLineID, 'ApptransferRelcassLines/DeleteApptransferReclassEntry')
}
export function postTransferReclass(data) {

    return myApi.getItem(data, "ApptransferRelcassLines/PostTransferReclass")
}
