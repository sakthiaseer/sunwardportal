import myApi from '@/util/api'

export function getInventoryTypeOpeningBalance() {

    return myApi.getAll('InventoryTypeOpeningStockBalance/GetInventoryTypeOpeningBalance')
}
export function getInventoryTypeOpeningStockBalanceData(data) {

    return myApi.getItem(data, "InventoryTypeOpeningStockBalance/GetInventoryTypeOpeningStockBalanceData")
}
export function createInventoryTypeOpeningBalance(data) {
    return myApi.create(data, "InventoryTypeOpeningStockBalance/InsertInventoryTypeOpeningBalance")
}

export function updateInventoryTypeOpeningBalance(data) {

    return myApi.update(data.openingBalanceId, data, "InventoryTypeOpeningStockBalance/UpdateInventoryTypeOpeningBalance")
}
export function deleteInventoryTypeOpeningBalance(data) {

    return myApi.delete(data.openingBalanceId, 'InventoryTypeOpeningStockBalance/DeleteInventoryTypeOpeningBalance')
}


export function getInventoryTypeOpeningBalanceLines(id) {

    return myApi.getByID(id,'InventoryTypeOpeningStockBalance/GetInventoryTypeOpeningBalanceLines')
}

export function createInventoryTypeOpeningStockBalanceLine(data) {
    return myApi.create(data, "InventoryTypeOpeningStockBalance/InsertInventoryTypeOpeningStockBalanceLine")
}

export function updateInventoryTypeOpeningStockBalanceLine(data) {

    return myApi.update(data.openingStockLineId, data, "InventoryTypeOpeningStockBalance/UpdateInventoryTypeOpeningStockBalanceLine")
}
export function deleteInventoryTypeOpeningStockBalanceLine(data) {

    return myApi.delete(data.openingStockLineId, 'InventoryTypeOpeningStockBalance/DeleteInventoryTypeOpeningStockBalanceLine')
}

export function getNavItemsItemCategory(id) {

    return myApi.getByID(id,'NavItem/GetNavItemsItemCategory')
}
export function getNavItemsForInventoryLine(data) {

    return myApi.getItem(data, "NavItem/GetNavItemsForInventoryLine")
  }
