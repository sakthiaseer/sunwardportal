import myApi from '@/util/api'

export function getTenderPeriodPricingLine (id) {

    return myApi.getByID(id, 'TenderPeriodPricingLine/GetTenderPeriodPricingLine')
}
export function getTenderPeriodPricingLineData (data) {

    return myApi.getItem(data, "TenderPeriodPricingLine/GetData")
}

export function createTenderPeriodPricingLine (data) {
    return myApi.create(data, "TenderPeriodPricingLine/InsertTenderPeriodPricingLine")
}

export function updateTenderPeriodPricingLine (data) {

    return myApi.update(data.tenderPeriodPricingLineId, data, "TenderPeriodPricingLine/UpdateTenderPeriodPricingLine")
}
export function deleteTenderPeriodPricingLine (data) {

    return myApi.delete(data.tenderPeriodPricingLineId, 'TenderPeriodPricingLine/DeleteTenderPeriodPricingLine')
}
