import myApi from '@/util/api'

export function getPurchaseOrderLine (purchaseOrderId) {
    return myApi.getAll('PurchaseOrderLine/GetPurchaseOrderLine/?id=' + purchaseOrderId)
}
export function createPurchaseOrderLine (data) {
    return myApi.create(data, "PurchaseOrderLine/InsertPurchaseOrderLine")
}
export function updatePurchaseOrderLine (data) {

    return myApi.update(data.purchaseOrderLineId, data, "PurchaseOrderLine/UpdatePurchaseOrderLine")
}
export function deletePurchaseOrderLine (data) {

    return myApi.delete(data.purchaseOrderLineId, 'PurchaseOrderLine/DeletePurchaseOrderLine')
}
export function getDescriptionExceptFPandPRItems(id) {

    return myApi.getByID(id,'NavItem/GetDescriptionExceptFPandPRItems')
}