import myApi from '@/util/api'

export function getNavplannedProdOrders() {

    return myApi.getAll('NavplannedProdOrder/GetNavplannedProdOrder')
}
export function getNavplannedProdOrder(data) {

    return myApi.getItem(data, "NavplannedProdOrder/GetData")
}

export function createNavplannedProdOrder(data) {
    return myApi.create(data, "NavplannedProdOrder/InsertNavplannedProdOrder")
}

export function updateNavplannedProdOrder(data) {

    return myApi.update(data.plannedProdOrderId, data, "NavplannedProdOrder/UpdateNavplannedProdOrder")
}
export function deleteNavplannedProdOrder(data) {

    return myApi.delete(data.plannedProdOrderID, 'NavplannedProdOrder/DeleteNavplannedProdOrder')
}
export function getItems() {

    return myApi.getAll('NavItem/GetItems')
}
export function getINPItems(id) {
    return myApi.getByID(id, 'NavplannedProdOrder/GetNavINPItems')
}
  export function getByINPReportID(id) {

    return myApi.getByID(id, 'NavplannedProdOrder/GetByINPReportID')
}
