import myApi from '@/util/api'

export function getShiftMasters() {

    return myApi.getAll('ShiftMaster/GetShiftMasters')
}
export function getShiftMaster(data) {

    return myApi.getItem(data, "ShiftMaster/GetData")
}
export function createShiftMaster(data) {
    return myApi.create(data, "ShiftMaster/InsertShiftMaster")
}

export function updateShiftMaster(data) {

    return myApi.update(data.shiftID, data, "ShiftMaster/UpdateShiftMaster")
}
export function deleteShiftMaster(data) {

    return myApi.delete(data.shiftID, 'ShiftMaster/DeleteShiftMaster')
}