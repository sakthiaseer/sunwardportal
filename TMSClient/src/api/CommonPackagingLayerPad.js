import myApi from '@/util/api'

export function getCommonPackagingLayerPad (refNo) {

  return myApi.getByRefNo(refNo,'CommonPackagingLayerPad/GetCommonPackagingLayerPad')
}
export function getData (data) {

  return myApi.getItem(data, "CommonPackagingLayerPad/GetData")
}
export function createCommonPackagingLayerPad (data) {
  return myApi.create(data, "CommonPackagingLayerPad/InsertCommonPackagingLayerPad")
}
export function updateCommonPackagingLayerPad (data) {

  return myApi.update(data.layerPadSpecificationId, data, "CommonPackagingLayerPad/UpdateCommonPackagingLayerPad")
}
export function deleteCommonPackagingLayerPad (data) {

  return myApi.delete(data.layerPadSpecificationId, 'CommonPackagingLayerPad/DeleteCommonPackagingLayerPad')
}
export function getCommonPackagingLayerPadByRefNo(data) {
  return myApi.getItems(data, 'CommonPackagingLayerPad/GetCommonPackagingLayerPadByRefNo')
}