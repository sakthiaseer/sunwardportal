import myApi from '@/util/api'

export function getJobProgressTemplate() {

  return myApi.getAll('JobProgressTemplate/GetJobProgressTemplateItems')
}
export function getSearchData(data) {

  return myApi.getItem(data, "JobProgressTemplate/GetData")
}
export function isTemplateExist(data) {
  return myApi.getByID(data, "JobProgressTemplate/IsTemplateExist")
}

export function getJobProgressTemplateItemsByID(data) {
  return myApi.getByID(data, "JobProgressTemplate/GetJobProgressTemplateItemsByID")
}

export function getJobProgressTemplateItemsByTemplate(data) {
  return myApi.getItem(data, "JobProgressTemplate/GetJobProgressTemplateItemsByTemplate")
}
export function getJobProgressTemplateLineProcessByPIC(data) {
  return myApi.getByID(data, "JobProgressTemplate/GetJobProgressTemplateLineProcessByPIC")
}

export function createJobProgressTemplate(data) {
  return myApi.create(data, "JobProgressTemplate/InsertJobProgressTemplate")
}
export function updateJobProgressTemplate(data) {

  return myApi.update(data.jobProgressTemplateId, data, "JobProgressTemplate/UpdateJobProgressTemplate")
}
export function deleteJobProgressTemplate(data) {

  return myApi.delete(data.jobProgressTemplateId, 'JobProgressTemplate/DeleteJobProgressTemplate')
}
export function createVersion(data) {

  return myApi.update(data.jobProgressTemplateId, data, "JobProgressTemplate/CreateVersion")
}
export function applyVersion(data) {

  return myApi.update(data.jobProgressTemplateId, data, "JobProgressTemplate/ApplyVersion")
}
export function tempVersion(data) {

  return myApi.update(data.jobProgressTemplateId, data, "JobProgressTemplate/TempVersion")
}
export function undoVersion(id) {

  return myApi.getByID(id, "JobProgressTemplate/UndoVersion")
}
export function deleteVersion(id) {

  return myApi.delete(id, 'JobProgressTemplate/DeleteVersion')
}
export function getJobProgressTemplateNotify(id) {

  return myApi.getByID(id, 'JobProgressTemplate/GetJobProgressTemplateNotify')
}
export function deleteJobProgressTemplateNotify(data) {

  return myApi.getItem(data, "JobProgressTemplate/DeleteJobProgressTemplateNotify")
}

export function createJobProgressTemplateRecurrence (data) {
  return myApi.create(data, "JobProgressTemplate/InsertJobProgressTemplateRecurrence")
}
export function getJobProgressTemplateRecurrence (id) {

  return myApi.getByID(id, 'JobProgressTemplate/GetJobProgressTemplateRecurrence')
}
export function getProcessMonitoringSequence (id) {

  return myApi.getByID(id, 'JobProgressTemplate/GetProcessMonitoringSequence')
}