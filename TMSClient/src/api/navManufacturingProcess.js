import myApi from '@/util/api'

export function getNavManufacturingProcess(id) {

    return myApi.getByID(id, 'NAVManufacturingProcess/GetNavManufacturingProcess')
}
export function getProcessAvailablity(data) {

    return myApi.getItems(data, 'NAVManufacturingProcess/GetProcessAvailablity')
}
export function getNavManufacturingProcessAll(data) {

    return myApi.getItems(data, 'NAVManufacturingProcess/GetNavManufacturingProcessAll')
}
export function uploadDocuments(data) {

    return myApi.uploadFile(data, "NAVManufacturingProcess/UploadDocuments");
}
export function getNavManufacturingProcessAllItems() {

    return myApi.getAll('NAVManufacturingProcess/GetNavManufacturingProcessAllItems')
}
export function getReplanRefNoByCompany(id) {

    return myApi.getByID(id, 'NAVManufacturingProcess/GetReplanRefNoByCompany')
}
export function getNavManufacturingProcessByIds(ids) {
    return myApi.getItems(ids, 'NAVManufacturingProcess/GetNavManufacturingProcessByIds')
}