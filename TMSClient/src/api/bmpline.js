import myApi from '@/util/api'



export function getICBMPLine(data) {

    return myApi.getItem(data, "ICBMPLine/GetData")
}
export function getICBMPLines(){
    return myApi.getAll(data, "ICBMPLine/GetICBMPLine")
}
export function createICBMPLine(data) {
    return myApi.create(data, "ICBMPLine/InsertICBMPLine")
}

export function updateICBMPLine(data) {

    return myApi.update(data.icbmpLineID, data, "ICBMPLine/UpdateICBMPLine")
}
export function deleteICBMPLine(data) {

    return myApi.delete(data.icbmpLineID, 'ICBMPLine/DeleteICBMPLine')
}

