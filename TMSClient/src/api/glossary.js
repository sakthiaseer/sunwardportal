import myApi from '@/util/api'

export function getGlossarys(data='') {

    return myApi.getAll('ApplicationGlossary/GetGlossarys/'+data)
}
export function getGlossary(data) {

    return myApi.getItem(data, "ApplicationGlossary/GetData")
}
export function createGlossary(data) {
    return myApi.create(data, "ApplicationGlossary/InsertGlossary")
}

export function updateGlossary(data) {

    return myApi.update(data.applicationGlossaryID, data, "ApplicationGlossary/UpdateGlossary")
}
export function deleteGlossary(data) {

    return myApi.delete(data.applicationGlossaryID, 'ApplicationGlossary/DeleteGlossary')
}
export function GetApplicationGlossarySearch(data)
{
    return myApi.getAll("ApplicationGlossary/GetApplicationGloAbbrSearch?data="+data+"&&type=abbrevation");
}
export function getApplicationAbbreviationGlossarySearch(data)
{
    return myApi.getAll("ApplicationGlossary/GetApplicationGloAbbrSearch?data="+data+"&&type=abbrevationGlossory");
}