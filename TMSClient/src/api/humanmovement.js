import myApi from '@/util/api'

export function getHumanMovement() {
    return myApi.getAll('HumanMovement/GetHumanMovement')
}
export function getHumanMovementSearch(data) {
    return myApi.getItems(data, 'HumanMovement/GetHumanMovementSearch')
}
export function getHumanMovementLocation() {
    return myApi.getAll('HumanMovement/GetHumanMovementLocation')
}
export function getCompanyListings() {

    return myApi.getAll('CompanyListing/GetCompanyListings')
}
export function getHumanMovementSage(data) {
    return myApi.getItems(data, 'HumanMovement/GetHumanMovementSage')
}
export function updateEmployeeSageInformation(data) {

    return myApi.update(data.sageInformationId, data, "HumanMovement/UpdateEmployeeSageInformation")
}
export function getEmployees() {

    return myApi.getAll('Employee/GetEmployeesList')
}
export function getHumanMovementActionByMovement(id) {

    return myApi.getByID(id, 'HumanMovementAction/GetHumanMovementActionByMovement')
}
export function updateHumanMovementAction(data) {

    return myApi.update(data.humanMovementId, data, "HumanMovementAction/UpdateHumanMovementAction")
}
export function createHumanMovementAction(data) {

    return myApi.create(data, "HumanMovementAction/InsertHumanMovementAction")
}
export function getHumanMovementActionSearch(data) {
    return myApi.getItems(data, 'HumanMovement/GetHumanMovementActionSearch')
}

export function getGetSelfTestSearch(data) {
    return myApi.getItems(data, 'SelfTest/GetSelfTestSearch')
}
export function getSelfTestPdfSearch(data) {
    return myApi.getItems(data, 'SelfTest/GetSelfTestPdfSearch')
}
export function getSelfTestDocument(data) {
    return myApi.getItems(data, 'SelfTest/GetSelfTestDocument')
}
export function getSelfTestLocation() {
    return myApi.getAll('SelfTest/GetSelfTestLocation')
}
export function updateVoidStatusSelfTest(data) {

    return myApi.getItems(data, 'SelfTest/UpdateVoidStatusSelfTest')
}
export function updateSelfTest(data) {

    return myApi.update(data.selfTestId, data, "SelfTest/UpdateSelfTest")
}