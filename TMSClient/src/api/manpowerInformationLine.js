import myApi from '@/util/api'



export function getManpowerInformationLine(data) {

    return myApi.getItem(data, "ManpowerInformationLine/GetData")
}
export function getManpowerInformationLines(){
    return myApi.getAll("ManpowerInformationLine/GetManpowerInformationLines")
}
export function getManpowerInformationLinesByID(id) {

    return myApi.getByID(id,'ManpowerInformationLine/GetManpowerInformationLinesByID')
}
export function createManpowerInformationLine(data) {
    return myApi.create(data, "ManpowerInformationLine/InsertManpowerInformationLine")
}

export function updateManpowerInformationLine(data) {

    return myApi.update(data.manpowerInformationLineID, data, "ManpowerInformationLine/UpdateManpowerInformationLine")
}
export function deleteManpowerInformationLine(data) {

    return myApi.delete(data.manpowerInformationLineID, 'ManpowerInformationLine/DeleteManpowerInformationLine')
}

