import myApi from '@/util/api'

export function getHRExternalPersonal () {

    return myApi.getAll('HRExternalPersonal/GetHRExternalPersonal')
}
export function getData (data) {

    return myApi.getItem(data, "HRExternalPersonal/GetData")
}
export function createHRExternalPersonal (data) {
    return myApi.create(data, "HRExternalPersonal/InsertHRExternalPersonal")
}
export function updateHRExternalPersonal (data) {

    return myApi.update(data.hrexternalPersonalId, data, "HRExternalPersonal/UpdateHRExternalPersonal")
}
export function deleteHRExternalPersonal (data) {

    return myApi.delete(data.hrexternalPersonalId, 'HRExternalPersonal/DeleteHRExternalPersonal')
}