import myApi from "@/util/api";

export function getNavMethodCodes() {
  return myApi.getAll("NavMethodCode/GetNavMethodCode");
}

export function getNavItems() {
  return myApi.getAll("NavItem/GetNavItems");
}
export function getNavItemsbyCountry(id) {
  return myApi.getByID(id, "NavItem/GetNavItemsbyCountry");
}
export function getDescriptionByCountryItems(id) {
  return myApi.getByID(id, "NavItem/GetDescriptionByCountryItems");
}
export function getDescriptionByCountryMATItems(id, type) {
  return myApi.getAll(
    "NavItem/GetDescriptionByCountryMATItems?id=" + id + "&&type=" + type
  );
}
export function getDescriptionPUOMByCountryItems(id) {
  return myApi.getByID(id, "NavItem/GetDescriptionPUOMByCountryItems");
}
export function getItemBatchItems(item) {
  return myApi.getItems(item, "NAVFunc/GetItemBatchItems");
}

export function getSyncItemBatch(company) {
  return myApi.getItem(company, "NAVFunc/GetItemBatch");
}

export function getSyncNavItems(company) {
  return myApi.getItem(company, "NAVFunc/GetItems");
}
export function getSalesOrderKIVItems(item) {
  return myApi.getItems(item, "NAVFunc/GetSalesOrderKIVItems");
}
export function getSalesOrderLineKIV(item) {
  return myApi.getItem(item, "NAVFunc/GetSalesOrderLineKIV");
}
export function updateItemMaster(item) {
  return myApi.update(item.itemId, item, "NavItem/UpdateNavItem");
}
export function getDistributedItems() {
  return myApi.getAll("NavItem/GetNavDistributedItems");
}

export function getGenericCodes() {
  return myApi.getAll("NavItem/GetGenericCodes");
}

export function getGenericCodeByCountry(id) {
  return myApi.getByID(id, "NavItem/GetGenericCodeByCountry");
}


export function getGenericCodesByStatus() {
  return myApi.getAll("GenericCodes/GetGenericCodesByStatus");
}

export function getGenericCodeByPlant(id) {
  return myApi.getByID(id, "NavItem/GetGenericCodeByPlant");
}

export function getSWItems() {
  return myApi.getAll("NavItem/GetSWItems");
}
export function getCustomers(id) {
  return myApi.getByID(id, "NavCustomer/GetCustomers");
}
export function getDescriptionFPItems(id) {
  return myApi.getByID(id, "NavItem/GetDescriptionFPItems");
}
export function GetDescriptionFPItemsDetails(id) {
  return myApi.getByID(id, "NavItem/GetDescriptionFPItemsDetails");
}
