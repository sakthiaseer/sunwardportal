import myApi from '@/util/api'


export function getBomProductionGroup () {

    return myApi.getAll('BomProductionGroup/GetBomProductionGroups')
}
export function getBomProductionGroupByRefNo (data) {
    return myApi.getItems(data, 'BomProductionGroup/GetBomProductionGroupsByRefNo')
}
export function createBomProductionGroup (data) {
    return myApi.create(data, "BomProductionGroup/InsertBomProductionGroup")
}

export function updateBomProductionGroup (data) {

    return myApi.update(data.bomproductionGroupId, data, "BomProductionGroup/UpdateBomProductionGroup")
}
export function deleteBomProductionGroup (data) {

    return myApi.delete(data.bomproductionGroupId, 'BomProductionGroup/DeleteBomProductionGroup')
}
