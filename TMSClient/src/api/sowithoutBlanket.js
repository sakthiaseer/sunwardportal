import myApi from '@/util/api'

export function getSOWithoutBlanketOrders(id) {

    return myApi.getByID(id,'SOWithoutBlanket/GetSOWithoutBlanketOrders')
}
export function getSOWithoutBlanketOrdersData(data) {

    return myApi.getItem(data, "SOWithoutBlanket/GetSOWithoutBlanketOrderData")
}

export function createSOWithoutBlanketOrders(data) {
    return myApi.create(data, "SOWithoutBlanket/InsertSowithoutBlanketOrder")
}

export function updateSOWithoutBlanketOrders(data) {

    return myApi.update(data.sowithoutBlanketOrdersId, data, "SOWithoutBlanket/UpdateSOWithoutBlanketOrder")
}
export function deleteSOWithoutBlanketOrders(data) {

    return myApi.delete(data.sowithoutBlanketOrdersId, 'SOWithoutBlanket/DeleteSoWithOutBlanketOrder')
}
export function getItemCrossReferenceForSOWithoutBlanket () {

    return myApi.getAll('SobyCustomers/GetItemCrossReferenceForSOWithoutBlanket')
}