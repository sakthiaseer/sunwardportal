import myApi from '@/util/api'
export function getProductGroupSurvey () {

    return myApi.getAll('ProductGroupSurvey/GetProductGroupSurvey')
}
export function getDataList (data) {

    return myApi.getItem(data, "ProductGroupSurvey/GetData")
}
export function createProductGroupSurvey (data) {
    return myApi.create(data, "ProductGroupSurvey/InsertProductGroupSurvey")
}

export function updateProductGroupSurvey (data) {

    return myApi.update(data.productGroupSurveyId, data, "ProductGroupSurvey/UpdateProductGroupSurvey")
}
export function deleteProductGroupSurvey (data) {

    return myApi.delete(data.productGroupSurveyId, 'ProductGroupSurvey/DeleteProductGroupSurvey')
}


export function getProductGroupSurveyLine (id) {

    return myApi.getByID(id, "ProductGroupSurvey/GetProductGroupSurveyLine")
}

export function createProductGroupSurveyLine (data) {
    return myApi.create(data, "ProductGroupSurvey/InsertProductGroupSurveyLine")
}

export function updateProductGroupSurveyLine (data) {

    return myApi.update(data.productGroupSurveyLineId, data, "ProductGroupSurvey/UpdateProductGroupSurveyLine")
}
export function deleteProductGroupSurveyLine (data) {

    return myApi.delete(data.productGroupSurveyLineId, 'ProductGroupSurvey/DeleteProductGroupSurveyLine')
}

export function getSalesSurveyByFieldForce () {

    return myApi.getAll('SalesSurveyByFieldForce/GetSalesSurveyByFieldForce')
}
export function getLocalClinic () {
    return myApi.getAll("LocalClinic/GetLocalClinic")
}
export function getSalesSurveyByFieldForceLineById (id) {
    return myApi.getByID(id, "SalesSurveyByFieldForce/GetSalesSurveyByFieldForceLine")
}