import myApi from '@/util/api'

export function getPortfolio() {

    return myApi.getAll('Portfolio/GetPortfolio')
}
export function getPortfolioData(data) {

    return myApi.getItem(data, "Portfolio/GetData")
}
export function getPortfolioByID(id) {

    return myApi.getByID(id, 'Portfolio/GetPortfolioByID')
}
export function createPortfolio(data) {
    return myApi.create(data, "Portfolio/InsertPortfolio")
}

export function updatePortfolio(data) {

    return myApi.update(data.portfolioId, data, "Portfolio/UpdatePortfolio")
}
export function deletePortfolio(data) {

    return myApi.delete(data.portfolioId, 'Portfolio/DeletePortfolio')
}


export function getPortfolioLinesByID(id) {

    return myApi.getByID(id, "Portfolio/GetPortfolioLinesByID")
}
export function getPortfolioLines() {

    return myApi.getAll("Portfolio/GetPortfolioLines")
}
export function createPortfolioLine(data) {
    return myApi.create(data, "Portfolio/InsertPortfolioLine")
}

export function updatePortfolioLine(data) {

    return myApi.update(data.portfolioLineId, data, "Portfolio/UpdatePortfolioLine")
}
export function deletePortfolioLine(data) {

    return myApi.delete(data.portfolioLineId, 'Portfolio/DeletePortfolioLine')
}

export function checkOutDoc(data) {
    return myApi.update(data.documentID, data, 'Portfolio/CheckOutDoc')
}
export function checkInDoc(data) {

    return myApi.update(data.documentID, data, 'Portfolio/CheckInDoc')
}