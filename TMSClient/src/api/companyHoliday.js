import myApi from '@/util/api'

export function getCompanyHolidays() {

    return myApi.getAll('CompanyHolidays/GetCompanyHolidays')
}
export function getCompanyHoliday(data) {

    return myApi.getItem(data, "CompanyHolidays/GetData")
}
export function getStates() {

    return myApi.getAll('State/GetStates')
}
export function getCountries() {

    return myApi.getAll('Country/GetCountries')
}
export function createCompanyHolidays(data) {
    return myApi.create(data, "CompanyHolidays/InsertCompanyHolidayss")
}

export function updateCompanyHolidays(data) {

    return myApi.update(data.companyHolidayID, data, "CompanyHolidays/UpdateCompanyHolidays")
}
export function deleteCompanyHolidays(data) {

    return myApi.delete(data.companyHolidayID, 'CompanyHolidays/DeleteCompanyHolidays')
}
export function getHolidayDetails(id) {

    return myApi.getByID(id, 'CompanyHolidayDetail/GetCompanyHolidayDetails')
}