import myApi from '@/util/api'

export function getICTCertificates() {

    return myApi.getAll('ICTCertificate/GetICTCertificate')
}
export function getICTCertificate(data) {

    return myApi.getItem(data, "ICTCertificate/GetData")
}

export function createICTCertificate(data) {
    return myApi.create(data, "ICTCertificate/InsertICTCertificate")
}

export function updateICTCertificate(data) {

    return myApi.update(data.iCTCertificateID, data, "ICTCertificate/UpdateICTCertificate")
}
export function deleteICTCertificate(data) {

    return myApi.delete(data.iCTCertificateID, 'ICTCertificate/DeleteICTCertificate')
}
