import myApi from '@/util/api'

export function getItemClassifications () {

    return myApi.getAll('ItemClassification/GetItemClassifications')
}
export function getItemClassification (data) {

    return myApi.getItem(data, "ItemClassification/GetData")
}
export function getItemClassificationByID (id) {
    return myApi.getByID(id, 'ItemClassification/GetItemClassification')

}
export function createItemClassification (data) {
    return myApi.create(data, "ItemClassification/InsertItemClassification")
}

export function updateItemClassification (data) {

    return myApi.update(data.itemClassificationID, data, "ItemClassification/UpdateItemClassification")
}
export function deleteItemClassification (data) {

    return myApi.delete(data.itemClassificationID, 'ItemClassification/DeleteItemClassification')
}
export function getItemClassificationId (id, userId) {

    return myApi.getByuserId(id, userId, 'ItemClassification/GetItemClassificationId');
}

export function getItemClassificationTree (id, userId) {

    return myApi.getByuserId(id, userId, 'ItemClassification/GetItemClassificationTree');
}
export function getItemClassificationSubItems (id, userId) {

    return myApi.getByuserId(id, userId, 'ItemClassification/GetItemClassificationSubItems')
}
export function createItemClassificationAccess (data) {
    return myApi.create(data, "ItemClassification/InsertItemClassificationAccess")
}
export function deleteItemClassificationAccess (data) {

    return myApi.update(data.itemClassificationAccessID, data, "ItemClassification/DeleteItemClassificationAccess")
}
export function getItemClassificationAccess (id) {
    return myApi.getByID(id, 'ItemClassification/GetItemClassificationAccess')

}
export function getItemClassificationByUserID (id) {
    return myApi.getByID(id, 'ItemClassification/GetItemClassificationByUserID')

}

export function getItemClassificationForBMR (id) {
    return myApi.getByID(id, 'ItemClassification/GetItemClassificationForBMR')

}
export function updateItemClassificationAccess (data) {

    return myApi.update(data.itemClassificationAccessID, data, "ItemClassification/UpdateItemClassificationAccess")
}
export function getApplicationForms () {

    return myApi.getAll('ItemClassification/GetApplicationForms')
}
export function getItemForm (id, userId) {

    return myApi.getAll('ItemClassification/GetItemForm?id=' + id + "&&userId=" + userId);
}
export function getApplicationFormsType (id) {

    return myApi.getByID(id, 'ItemClassification/GetApplicationFormsType')
}
export function getItemClassificationHeaders () {

    return myApi.getAll('ItemClassificationHeader/GetItemClassificationHeadersAll')
}
export function getClassificationList (data) {
    return myApi.getItems(data, 'ItemClassificationHeader/GetClassificationList')
}
export function getApplicationFormMasterPermissionTree (id) {
    return myApi.getAll('ItemClassification/GetApplicationFormMasterPermissionTree?id=' + id);

}
export function getItemClassificationPermission (itemClassificationId, userType, userId) {
    return myApi.getAll('ItemClassification/GetItemClassificationPermission?itemClassificationId=' + itemClassificationId + "&&userType=" + userType + '&&userId=' + userId);

}
export function createItemClassificationPermission (data) {
    return myApi.create(data, "ItemClassification/InsertItemClassificationPermission")
}
export function getItemClassificationPermissionList (itemClassificationId, userId) {
    return myApi.getAll('ItemClassification/GetItemClassificationPermissionList?id=' + itemClassificationId + "&&userId=" + userId);

}

export function getItemClassificationPermissionByClass (id) {
    return myApi.getAll('ItemClassification/GetItemClassificationPermissionByClass?ItemClassificationId=' + id);

}