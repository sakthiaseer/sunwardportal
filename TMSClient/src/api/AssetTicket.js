import myApi from '@/util/api'

export function getAssetTicket () {

    return myApi.getAll('AssetTicket/GetAssetTicket')
}
export function getAssetData (data) {

    return myApi.getItem(data, "AssetTicket/GetData")
}
export function createAssetTicket (data) {
    return myApi.create(data, "AssetTicket/InsertAssetTicket")
}

export function updateAssetTicket (data) {

    return myApi.update(data.assetTicketId, data, "AssetTicket/UpdateAssetTicket")
}
export function deleteAssetTicket (data) {

    return myApi.delete(data.assetTicketId, 'AssetTicket/DeleteAssetTicket')
}
