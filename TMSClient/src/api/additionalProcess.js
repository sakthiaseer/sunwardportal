import myApi from '@/util/api'

export function getAdditionalProcesss() {

  return myApi.getAll('AdditionalProcess/GetAdditionalProcessList')
}
export function getAdditionalProcessByID(id) {

  return myApi.getByID(id, "AdditionalProcess/GetAdditionalProcess")
}
export function getAdditionalProcessActive(id) {

  return myApi.getByID(id, "AdditionalProcess/GetAdditionalProcessActive")
}
export function getAdditionalProcess(data) {

    return myApi.getItem(data, "AdditionalProcess/GetData")
  }
export function createAdditionalProcess(data) {
  return myApi.create(data, "AdditionalProcess/InsertAdditionalProcess")
}

export function updateAdditionalProcess(data) {

  return myApi.update(data.AdditionalProcessID, data, "AdditionalProcess/UpdateAdditionalProcess")
}
export function deleteAdditionalProcess(data) {

  return myApi.delete(data.AdditionalProcessID, 'AdditionalProcess/DeleteAdditionalProcess')
}


