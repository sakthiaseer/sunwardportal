import myApi from '@/util/api'
export function importuploadDocuments (data) {

    return myApi.uploadFile(data, "outlookEmail/ImportuploadDocuments");
}
export function outlookImportPublicFolder (data) {

    return myApi.getItem(data, "outlookEmail/OutlookImportPublicFolder");
}