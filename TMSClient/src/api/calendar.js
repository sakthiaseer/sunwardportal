import myApi from '@/util/api'

export function getCalendars() {

  return myApi.getAll('Calendar/GetCalendars')
}
export function getCalendar(data) {

  return myApi.getItem(data, "Calendar/GetData")
}

export function createCalendar(data) {
  return myApi.create(data, "Calendar/InsertCalendar")
}

export function updateCalendar(data) {

  return myApi.update(data.countryID, data, "Calendar/UpdateCalendar")
}
export function deleteCalendar(data) {

  return myApi.delete(data.countryID, 'Calendar/DeleteCalendar')
}