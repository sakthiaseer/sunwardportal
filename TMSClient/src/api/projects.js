import myApi from '@/util/api'

export function getProjects() {

    return myApi.getAll('Project/GetProjects')
}
export function getProject(data) {

    return myApi.getItem(data, "Project/GetData")
}
export function createProjects(data) {
    return myApi.create(data, "Project/InsertProjects")
}

export function updateProjects(data) {

    return myApi.update(data.projectID, data, "Project/UpdateProjects")
}
export function deleteProjects(data) {

    return myApi.delete(data.projectID, 'Project/DeleteProjects')
}
export function getTeams() {

    return myApi.getAll('TeamMaster/GetTeams')
}