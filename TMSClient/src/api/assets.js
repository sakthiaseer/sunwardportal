import myApi from '@/util/api'

export function getAssetMasters() {

    return myApi.getAll('AssetMaster/GetAssetMasters')
}
export function getAssetData(data) {

    return myApi.getItem(data, "AssetMaster/GetData")
}
export function createAssetMaster(data) {
    return myApi.create(data, "AssetMaster/InsertAssetMaster")
}

export function updateAssetMaster(data) {

    return myApi.update(data.assetMasterId, data, "AssetMaster/UpdateAssetMaster")
}
export function deleteAssetMaster(data) {

    return myApi.delete(data.assetMasterId, 'AssetMaster/DeleteAssetMaster')
}


export function getAssetMaintenanceById(id) {

    return myApi.getByID(id, 'AssetMaster/GetAssetMaintenanceById')
}
export function getAssetAssignmentById(id) {

    return myApi.getByID(id, 'AssetMaster/GetAssetAssignmentById')
}

export function createAssetMaintenance(data) {
    return myApi.create(data, "AssetMaster/InsertAssetMaintenance")
}

export function updateAssetMaintenance(data) {

    return myApi.update(data.assetMaintenanceId, data, "AssetMaster/UpdateAssetMaintenance")
}
export function deleteAssetMaintenance(data) {

    return myApi.delete(data.assetMaintenanceId, 'AssetMaster/DeleteAssetMaintenance')
}

export function createAssetAssignment(data) {
    return myApi.create(data, "AssetMaster/InsertAssetAssignment")
}

export function updateAssetAssignment(data) {

    return myApi.update(data.assetMasterId, data, "AssetMaster/UpdateAssetAssignment")
}
export function deleteAssetAssignment(data) {

    return myApi.delete(data.assetMasterId, 'AssetMaster/DeleteAssetAssignment')
}