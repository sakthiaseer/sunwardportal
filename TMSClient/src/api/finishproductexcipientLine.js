import myApi from '@/util/api'

export function getFinishProductExcipientLine (finishproductexcipientId) {
    return myApi.getAll('FinishProductExcipientLine/GetFinishProductExcipientLine/?id='+finishproductexcipientId)
  }
export function createFinishProductExcipientLine(data) {
    return myApi.create(data, "FinishProductExcipientLine/InsertFinishProductExcipientLine")
  }
  export function updateFinishProductExcipientLine(data) {

    return myApi.update(data.finishProductExcipientLineId, data, "FinishProductExcipientLine/UpdateFinishProductExcipientLine")
  }
  export function deleteFinishProductExcipientLine(data) {

    return myApi.delete(data.finishProductExcipientLineId, 'FinishProductExcipientLine/DeleteFinishProductExcipientLine')
  }