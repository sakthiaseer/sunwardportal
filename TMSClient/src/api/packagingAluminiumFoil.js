import myApi from '@/util/api'



export function getPackagingAluminiumFoil(data) {

    return myApi.getItem(data, "PackagingAluminiumFoil/GetData")
}
export function getPackagingAluminiumFoils(){
    return myApi.getAll("PackagingAluminiumFoil/GetPackagingAluminiumFoils")
}
export function createPackagingAluminiumFoil(data) {
    return myApi.create(data, "PackagingAluminiumFoil/InsertPackagingAluminiumFoil")
}

export function updatePackagingAluminiumFoil(data) {

    return myApi.update(data.aluminiumFoilId, data, "PackagingAluminiumFoil/UpdatePackagingAluminiumFoil")
}
export function deletePackagingAluminiumFoil(data) {

    return myApi.delete(data.aluminiumFoilId, 'PackagingAluminiumFoil/DeletePackagingAluminiumFoil')
}



export function getPackagingAluminiumFoilLine(data) {

    return myApi.getItem(data, "PackagingAluminiumFoilLine/GetData")
}
export function getPackagingAluminiumFoilLinesById(id){
    return myApi.getByID(id,"PackagingAluminiumFoilLine/GetPackagingAluminiumFoilLinesById")
}
export function createPackagingAluminiumFoilLine(data) {
    return myApi.create(data, "PackagingAluminiumFoilLine/InsertPackagingAluminiumFoilLine")
}

export function updatePackagingAluminiumFoilLine(data) {

    return myApi.update(data.packagingAluminiumFoilLineId, data, "PackagingAluminiumFoilLine/UpdatePackagingAluminiumFoilLine")
}
export function deletePackagingAluminiumFoilLine(data) {

    return myApi.delete(data.packagingAluminiumFoilLineId, 'PackagingAluminiumFoilLine/DeletePackagingAluminiumFoilLine')
}
export function getPackagingAluminiumFoilsByRefNo(data) {
    return myApi.getItems(data, 'PackagingAluminiumFoil/GetPackagingAluminiumFoilsByRefNo')
  }


