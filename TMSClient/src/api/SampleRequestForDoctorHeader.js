import myApi from '@/util/api'

export function getSampleRequestForDoctorHeader() {

    return myApi.getAll('SampleRequestForDoctor/GetSampleRequestForDoctorHeader')
}
export function getSampleRequestForDoctorHeaderData(data) {

    return myApi.getItem(data, "SampleRequestForDoctor/GetSampleRequestForDoctorHeaderData")
}
export function createSampleRequestForDoctorHeader(data) {
    return myApi.create(data, "SampleRequestForDoctor/InsertSampleRequestForDoctorHeader")
}

export function updateSampleRequestForDoctorHeader(data) {

    return myApi.update(data.sampleRequestForDoctorHeaderId, data, "SampleRequestForDoctor/UpdateSampleRequestForDoctorHeader")
}
export function deleteSampleRequestForDoctorHeader(data) {

    return myApi.delete(data.sampleRequestForDoctorHeaderId, 'SampleRequestForDoctor/DeleteSampleRequestForDoctorHeader')
}

