import myApi from '@/util/api'

export function getQuotationHistory() {

    return myApi.getAll('QuotationHistory/GetQualificationHistoryItems')
}
export function getSearchData(data) {

    return myApi.getItem(data, "QuotationHistory/GetQuotationHistoryData")
}
export function createQuotationHistory(data) {
    return myApi.create(data, "QuotationHistory/InsertQuotationHistory")
}
export function updateQuotationHistory(data) {

    return myApi.update(data.quotationHistoryId, data, "QuotationHistory/UpdateQuotationHistory")
}
export function deleteQuotationHistory(data) {

    return myApi.delete(data.quotationHistoryId, 'QuotationHistory/DeleteQuotationHistory')
}


export function getQuotationHistoryLine(quotationHistoryId) {
    return myApi.getAll('QuotationHistory/GetQuotationHistoryLine/?id=' + quotationHistoryId)
}
export function createQuotationHistoryLine(data) {
    return myApi.create(data, "QuotationHistory/InsertQuotationHistoryLine")
}
export function updateQuotationHistoryLine(data) {

    return myApi.update(data.quotationHistoryLineId, data, "QuotationHistory/UpdateQuotationHistoryLine")
}
export function deleteQuotationHistoryLine(data) {

    return myApi.delete(data.quotationHistoryLineId, 'QuotationHistory/DeleteQuotationHistoryLine')
}
export function getDataLine(data) {

    return myApi.getItem(data, "QuotationHistory/GetQuotationHistoryLineData")
}
export function getApplicationMasterDetailById(id) {
    return myApi.getAll("QuotationHistory/GetApplicationMasterDetailById/" + id)
}


export function uploadQuotationHistoryDocuments(data, sessionId) {

    return myApi.uploadFile(data, "QuotationHistory/UploadQuotationHistoryDocuments?sessionId=" + sessionId)
}
export function getQuotationHistoryDocuments(id) {

    return myApi.getByID(id, "QuotationHistory/GetQuotationHistoryDocument")
}
export function deleteQuotationHistoryDocuments(quotationHistoryDocumentId) {

    return myApi.delete(quotationHistoryDocumentId, 'QuotationHistory/DeleteQuotationHistoryDocument')
}
export function downloadQuotationHistoryDocuments(data) {

    return myApi.downLoadDocument(data, "QuotationHistory/DownLoadQuotationHistoryDocument")
}

export function getQuotationHistoryReports() {

    return myApi.getAll('QuotationHistory/GetQuotationHistoryReports')
}
export function getQuotationHistoryBySearch(data) {

    return myApi.getItem(data, "QuotationHistory/GetQuotationHistoryBySearch")
}
export function getQuotationHistoryReportBySearch(data) {

    return myApi.getItem(data, "QuotationHistory/GetQuotationHistoryReportBySearch")
}


