import myApi from '@/util/api'

export function getBlisterScrapEntrys() {

    return myApi.getAll('BlisterScrapEntry/GetBlisterScrapEntrys')
}
export function getBlisterScrapEntry(data) {

    return myApi.getItem(data, "BlisterScrapEntry/GetData")
}

export function createBlisterScrapEntry(data) {
    return myApi.create(data, "BlisterScrapEntry/InsertBlisterScrapEntry")
}

export function updateBlisterScrapEntry(data) {

    return myApi.update(data.scrapEntryId, data, "BlisterScrapEntry/UpdateBlisterScrapEntry")
}
export function deleteBlisterScrapEntry(data) {

    return myApi.delete(data.scrapEntryId, 'BlisterScrapEntry/DeleteBlisterScrapEntry')
}

export function getPlasticbags() {

    return myApi.getAll('PlasticBag/GetPlasticBags')
}
export function getScrapType() {

    return myApi.getAll('BlisterScrapCode/GetBlisterScrapCodes')
}