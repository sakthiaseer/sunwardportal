import myApi from '@/util/api'

export function getMedMaster() {

    return myApi.getAll('MedMaster/GetMedMaster')
}
export function getSearchData(data) {

  return myApi.getItem(data, "MedMaster/GetData")
}
export function createMedMaster(data) {
    return myApi.create(data, "MedMaster/InsertMedMaster")
  }
  export function updateMedMaster(data) {

    return myApi.update(data.medMasterId, data, "MedMaster/UpdateMedMaster")
  }
  export function deleteMedMaster(data) {
  
    return myApi.delete(data.medMasterId, 'MedMaster/DeleteMedMaster')
  }


  export function getMedMasterLine(id) {

    return myApi.getByID(id,'MedMaster/GetMedMasterLine')
}
export function createMedMasterLine(data) {
    return myApi.create(data, "MedMaster/InsertMedMasterLine")
  }
  export function updateMedMasterLine(data) {

    return myApi.update(data.medMasterLineId, data, "MedMaster/UpdateMedMasterLine")
  }
  export function deleteMedMasterLine(data) {
  
    return myApi.delete(data.medMasterLineId, 'MedMaster/DeleteMedMasterLine')
  }