import myApi from '@/util/api'

export function createFpdrugClassification (data) {
    return myApi.create(data, "FpdrugClassification/InsertFpdrugClassification")
}

export function updateFpdrugClassification (data) {

    return myApi.update(data.fpdrugClassificationId, data, "FpdrugClassification/UpdateFpdrugClassification")
}
export function deleteFpdrugClassification (data) {

    return myApi.delete(data.fpdrugClassificationId, 'FpdrugClassification/DeleteFpdrugClassification')
}
export function getFpdrugClassificationByRefNo (id) {

    return myApi.getByID(id, 'FpdrugClassification/GetFpdrugClassificationByRefNo')
}