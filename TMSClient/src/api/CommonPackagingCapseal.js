import myApi from '@/util/api'

export function getCommonPackagingCapseal (refNo) {

  return myApi.getByRefNo(refNo,'CommonPackagingCapseal/GetCommonPackagingCapseal')
}
export function getData (data) {

  return myApi.getItem(data, "CommonPackagingCapseal/GetData")
}
export function createCommonPackagingCapseal (data) {
  return myApi.create(data, "CommonPackagingCapseal/InsertCommonPackagingCapseal")
}
export function updateCommonPackagingCapseal (data) {

  return myApi.update(data.capsealSpecificationId, data, "CommonPackagingCapseal/UpdateCommonPackagingCapseal")
}
export function deleteCommonPackagingCapseal (data) {

  return myApi.delete(data.capsealSpecificationId, 'CommonPackagingCapseal/DeleteCommonPackagingCapseal')
}
export function getCommonPackagingCapsealByRefNo(data) {
  return myApi.getItems(data, 'CommonPackagingCapseal/GetCommonPackagingCapsealByRefByNo')
}