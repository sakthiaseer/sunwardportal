import myApi from '@/util/api'

export function getNotesByDocumentId(id) {

    return myApi.getByID(id, "TemplateTestCase/GetNotesByDocumentId")
}

export function createNotes(data) {

    return myApi.create(data, "TemplateTestCase/InsertNotes")
}
export function updateNotes(data) {

    return myApi.update(data.templateCaseFormNotesId, data, "TemplateTestCase/UpdateNotes")
}

export function deleteNotes(data) {

    return myApi.delete(data.templateCaseFormNotesId, 'TemplateTestCase/DeleteNotes')
}