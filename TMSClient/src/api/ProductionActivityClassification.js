import myApi from '@/util/api'

export function getProductionActivityClassification(id) {

    return myApi.getByID(id,'ProductionActivityClassification/GetProductionActivityClassification')
}
export function getData(data) {

    return myApi.getItem(data, "ProductionActivityClassification/GetData")
}
export function createProductionActivityClassification(data) {
    return myApi.create(data, "ProductionActivityClassification/InsertProductionActivityClassification")
}

export function updateProductionActivityClassification(data) {

    return myApi.update(data.productionActivityClassificationId, data, "ProductionActivityClassification/UpdateProductionActivityClassification")
}
export function deleteProductionActivityClassification(data) {

    return myApi.delete(data.productionActivityClassificationId, 'ProductionActivityClassification/DeleteProductionActivityClassification')
}




