import myApi from '@/util/api'

export function getGenericItemNameListing() {

    return myApi.getAll('GenericItemNameListing/GetGenericItemNameListing')
}
export function getData(data) {

  return myApi.getItem(data, "GenericItemNameListing/GetData")
}
export function createGenericItemNameListing(data) {
    return myApi.create(data, "GenericItemNameListing/InsertGenericItemNameListing")
  }
  export function updateGenericItemNameListing(data) {

    return myApi.update(data.genericItemNameListingId, data, "GenericItemNameListing/UpdateGenericItemNameListing")
  }
  export function deleteGenericItemNameListing(data) {
  
    return myApi.delete(data.genericItemNameListingId, 'GenericItemNameListing/DeleteGenericItemNameListing')
  }