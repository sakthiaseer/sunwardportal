import myApi from '@/util/api'

export function getMarginInformation () {

    return myApi.getAll('MarginInformation/GetMarginInformation')
}
export function getSearchData (data) {

    return myApi.getItem(data, "MarginInformation/GetData")
}
export function createMarginInformation (data) {
    return myApi.create(data, "MarginInformation/InsertMarginInformation")
}
export function updateMarginInformation (data) {

    return myApi.update(data.marginInformationID, data, "MarginInformation/UpdateMarginInformation")
}
export function deleteMarginInformation (data) {

    return myApi.delete(data.marginInformationID, 'MarginInformation/DeleteMarginInformation')
}


export function getMarginInformationLineLineByID (marginInformationID) {
    return myApi.getAll('MarginInformation/GetMarginInformationLineByID/?id=' + marginInformationID)
}
export function createMarginInformationLine (data) {
    return myApi.create(data, "MarginInformation/InsertMarginInformationLine")
}
export function updateMarginInformationLine (data) {

    return myApi.update(data.marginInformationLineID, data, "MarginInformation/UpdateMarginInformationLine")
}
export function deleteMarginInformationLine (data) {

    return myApi.delete(data.marginInformationLineID, 'MarginInformation/DeleteMarginInformationLine')
}
export function getDataLine (data) {

    return myApi.getItem(data, "MarginInformation/GetMarginInformationLineData")
}
export function getApplicationMasterDetailById (id) {
    return myApi.getAll("MarginInformation/GetApplicationMasterDetailById/" + id)
}


export function getProductwithName (companyID) {
    return myApi.getAll('MarginInformation/GetProductwithName/?id=' + companyID)
}

export function getProductUomID (itemID) {
    return myApi.getAll('MarginInformation/GetProductUomID/?id=' + itemID)
}

export function updateMarginInformationVersion (data) {
    return myApi.update(data.marginInformationID, data, "MarginInformation/UpdateMarginInformationVersion")
}
export function createVersion (data) {

    return myApi.update(data.marginInformationID, data, "MarginInformation/CreateVersion")
}
export function applyVersion (data) {

    return myApi.update(data.marginInformationID, data, "MarginInformation/ApplyVersion")
}
export function tempVersion(data) {

    return myApi.update(data.marginInformationID, data, "MarginInformation/TempVersion")
}
export function undoVersion(id) {

    return myApi.getByID(id, "MarginInformation/UndoVersion")
}
export function deleteVersion(id) {

    return myApi.delete(id, 'MarginInformation/DeleteVersion')
}
