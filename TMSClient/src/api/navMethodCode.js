import myApi from '@/util/api'

export function getNavMethodCodes() {

    return myApi.getAll('NavMethodCode/GetNavMethodCode')
}
export function getExistingItems() {

    return myApi.getAll('NavMethodCode/GetExistingItems')
}
export function getNavMethodCode(data) {

    return myApi.getItem(data, "NavMethodCode/GetData")
}
export function getNavMethodCodeLine(id) {
  
    return myApi.getByID(id, "NavMethodCode/GetNavMethodCodeLine")
  }
export function createNavMethodCode(data) {
    return myApi.create(data, "NavMethodCode/InsertNavMethodCode")
}

export function updateNavMethodCode(data) {

    return myApi.update(data.methodCodeID, data, "NavMethodCode/UpdateNavMethodCode")
}
export function createNavMethodCodeLine(data) {
    return myApi.create(data, "NavMethodCode/InsertNavMethodCodeLine")
}

export function updateNavMethodCodeLine(data) {

    return myApi.update(data.methodCodeLineID, data, "NavMethodCode/UpdateNavMethodCodeLine")
}
export function deleteNavMethodCode(data) {

    return myApi.delete(data.methodCodeID, 'NavMethodCode/DeleteNavMethodCode')
}
export function deleteNavMethodCodeLines(data) {

    return myApi.delete(data.methodCodeLineID, 'NavMethodCode/DeleteNavMethodCodeLines')
}
  export function getItems() {

    return myApi.getAll('NavMethodCode/GetItems')
}
export function getNAVINPCategoryItems() {

    return myApi.getAll('NavMethodCode/GetNAVINPCategoryItems')
}

