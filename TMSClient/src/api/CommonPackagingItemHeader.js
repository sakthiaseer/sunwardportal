import myApi from '@/util/api'

export function getCommonPackagingItemHeader(id) {

  return myApi.getByID(id, 'CommonPackagingItemHeader/GetCommonPackagingItemHeader')
}
export function getCommonPackagingItemHeaderAll() {

  return myApi.getAll('CommonPackagingItemHeader/GetCommonPackagingItemHeaderAll')
}
export function getData(data) {

  return myApi.getItem(data, "CommonPackagingItemHeader/GetData")
}
export function createCommonPackagingItemHeader(data) {
  return myApi.create(data, "CommonPackagingItemHeader/InsertCommonPackagingItemHeader")
}
export function updateCommonPackagingItemHeader(data) {

  return myApi.update(data.commonPackagingItemId, data, "CommonPackagingItemHeader/UpdateCommonPackagingItemHeader")
}
export function deleteCommonPackagingItemHeader(data) {

  return myApi.delete(data.commonPackagingItemId, 'CommonPackagingItemHeader/DeleteCommonPackagingItemHeader')
}

export function getCommonPackageReports(data) {

  return myApi.getItems(data, "CommonPackagingItemHeader/GetCommonPackageReports")
}