import myApi from '@/util/api'

export function getCommonPackagingNesting (refNo) {

  return myApi.getByRefNo(refNo,'CommonPackagingNesting/GetCommonPackagingNesting')
}
export function getData (data) {

  return myApi.getItem(data, "CommonPackagingNesting/GetData")
}
export function createCommonPackagingNesting (data) {
  return myApi.create(data, "CommonPackagingNesting/InsertCommonPackagingNesting")
}
export function updateCommonPackagingNesting (data) {

  return myApi.update(data.nestingSpecificationId, data, "CommonPackagingNesting/UpdateCommonPackagingNesting")
}
export function deleteCommonPackagingNesting (data) {

  return myApi.delete(data.nestingSpecificationId, 'CommonPackagingNesting/DeleteCommonPackagingNesting')
}
export function getCommonPackagingNestingByRefNo(data) {
  return myApi.getItems(data, 'CommonPackagingNesting/GetCommonPackagingNestingByRefNo')
}