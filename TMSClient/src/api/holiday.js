import myApi from '@/util/api'

export function getHolidays() {

    return myApi.getAll('HolidayMaster/GetHolidays')
}
export function getStates() {

    return myApi.getAll('State/GetStates')
}
export function getCountry() {

    return myApi.getAll('State/GetCountries')
}
export function getState() {

    return myApi.getAll('State/GetData')
}
export function getHoliday(data) {

    return myApi.getItem(data, "HolidayMaster/GetData")
}

export function createHoliday(data) {
    return myApi.create(data, "HolidayMaster/InsertHoliday")
}

export function updateHoliday(data) {

    return myApi.update(data.holidayID, data, "HolidayMaster/UpdateHoliday")
}
export function deleteHoliday(data) {

    return myApi.delete(data.holidayID, 'HolidayMaster/DeleteHoliday')
}