import myApi from '@/util/api'


export function getFinishProductExcipient() {

    return myApi.getAll('FinishProductExcipient/GetFinishProductExcipient')
}
export function getDataFinishProductExcipient(data) {

  return myApi.getItem(data, "FinishProductExcipient/GetData")
}
export function createFinishProductExcipient(data) {
    return myApi.create(data, "FinishProductExcipient/InsertFinishProductExcipient")
  }
  export function getFinishProductExcipientByFinishProduct(id) {  
    return myApi.getByID(id, "FinishProductExcipient/GetFinishProductExcipientByFinishProduct")
}
  export function isMasterExist(id){
    return myApi.getByID(id, "FinishProductExcipient/isMasterExist")
  }
  export function updateFinishProductExcipient(data) {

    return myApi.update(data.finishProductExcipientId, data, "FinishProductExcipient/UpdateFinishProductExcipient")
  }
  export function deleteFinishProductExcipient(data) {
  
    return myApi.delete(data.finishProductExcipientId, 'FinishProductExcipient/DeleteFinishProductExcipient')
  }
  export function getFinishProductExcipientByRefNo(data) {
    return myApi.getItems(data, 'FinishProductExcipient/GetFinishProductExcipientByRefNo')
}