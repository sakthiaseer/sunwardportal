import myApi from '@/util/api'

export function getcommonMasterToolingClassification (id) {

  return myApi.getByID(id,'commonMasterToolingClassification/GetcommonMasterToolingClassification')
}
export function getData(data) {

    return myApi.getItem(data, "commonMasterToolingClassification/GetData")
  }
export function createcommonMasterToolingClassification (data) {
  return myApi.create(data, "commonMasterToolingClassification/InsertCommonMasterToolingClassification")
}
export function updatecommonMasterToolingClassification (data) {

  return myApi.update(data.commonMasterToolingClassificationId, data, "commonMasterToolingClassification/UpdateCommonMasterToolingClassification")
}
export function deletecommonMasterToolingClassification (data) {

  return myApi.delete(data.commonMasterToolingClassificationId, 'commonMasterToolingClassification/DeleteCommonMasterToolingClassification')
}
export function getICTMasterlocation () {

    return myApi.getAll('ICTMaster/GetICTMasterlocation')
  }