import myApi from '@/util/api'

export function getShortcuts() {

    return myApi.getAll('Shortcut/GetShortcuts')
}
export function getShortcut(data) {

    return myApi.getItem(data, "Shortcut/GetData")
}
export function createShortcut(data) {
    return myApi.create(data, "Shortcut/InsertShortcut")
}

export function updateShortcut(data) {

    return myApi.update(data.shortCutID, data, "Shortcut/UpdateShortcut")
}
export function deleteShortcut(data) {

    return myApi.delete(data.shortCutID, 'Shortcut/DeleteShortcut')
}