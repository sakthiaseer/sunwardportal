import myApi from '@/util/api'

export function getEmployeeAdditionalInformations() {

  return myApi.getAll('EmployeeAdditionalInformation/GetEmployeeAdditionalInformations')
}
export function getEmployeeAdditionalInformation(data) {

    return myApi.getByID(data, "EmployeeAdditionalInformation/GetEmployeeAdditionalInformation")
}
export function createEmployeeAdditionalInformation(data) {
  return myApi.create(data, "EmployeeAdditionalInformation/InsertEmployeeAdditionalInformation")
}
export function createEmployeePublicInformation(data) {
  return myApi.create(data, "EmployeeAdditionalInformation/InsertPublicInformation")
}