import myApi from '@/util/api'

export function getPlants() {

    return myApi.getAll('Plant/GetPlants')
}
export function getPlant(data) {

    return myApi.getItem(data, "Plant/GetData")
}

export function createPlant(data) {
    return myApi.create(data, "Plant/InsertPlant")
}

export function updatePlant(data) {

    return myApi.update(data.plantID, data, "Plant/UpdatePlant")
}
export function deletePlant(data) {

    return myApi.delete(data.plantID, 'Plant/DeletePlant')
}
export function getApplicationUsers() {

    return myApi.getAll('ApplicationUser/GetApplicationUsers')
}