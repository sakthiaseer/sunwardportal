import myApi from '@/util/api'

export function getMasterInterCompanyPricings() {

    return myApi.getAll('MasterInterCompanyPricing/GetMasterInterCompanyPricings')
}
export function getMasterInterCompanyPricingData(data) {

    return myApi.getItem(data, "MasterInterCompanyPricing/GetMasterInterCompanyPricingData")
}
export function createMasterInterCompanyPricing(data) {
    return myApi.create(data, "MasterInterCompanyPricing/InsertMasterInterCompanyPricing")
}

export function updateMasterInterCompanyPricing(data) {
    return myApi.update(data.masterInterCompanyPricingId, data, "MasterInterCompanyPricing/UpdateMasterInterCompanyPricing")
}
export function updateMasterInterCompanyPricingVersion(data) {
    return myApi.update(data.masterInterCompanyPricingId, data, "MasterInterCompanyPricing/UpdateMasterInterCompanyPricingVersion")
}
export function deleteMasterInterCompanyPricing(data) {
    return myApi.delete(data.masterInterCompanyPricingId, 'MasterInterCompanyPricing/DeleteMasterInterCompanyPricing')
}

export function getMasterInterCompanyPricingLines(id) {

    return myApi.getByID(id, 'MasterInterCompanyPricingLine/GetMasterInterCompanyPricingLines')
}
export function getMasterInterCompanyPricingLineData(data) {

    return myApi.getItem(data, "MasterInterCompanyPricingLine/GetMasterInterCompanyPricingLineData")
}
export function createMasterInterCompanyPricingLine(data) {
    return myApi.create(data, "MasterInterCompanyPricingLine/InsertMasterInterCompanyPricingLine")
}

export function updateMasterInterCompanyPricingLine(data) {

    return myApi.update(data.masterInterCompanyPricingLineId, data, "MasterInterCompanyPricingLine/UpdateMasterInterCompanyPricingLine")
}
export function deleteMasterInterCompanyPricingLine(data) {

    return myApi.delete(data.masterInterCompanyPricingLineId, 'MasterInterCompanyPricingLine/DeleteMasterInterCompanyPricingLine')
}
export function getNavisionCompany(data) {

    return myApi.getByType(data, 'MasterInterCompanyPricingLine/GetNavisionCompany')
}
export function createVersion(data) {

    return myApi.update(data.masterInterCompanyPricingId, data, "MasterInterCompanyPricing/CreateVersion")
}
export function applyVersion(data) {

    return myApi.update(data.masterInterCompanyPricingId, data, "MasterInterCompanyPricing/ApplyVersion")
}
export function tempVersion(data) {

    return myApi.update(data.masterInterCompanyPricingId, data, "MasterInterCompanyPricing/TempVersion")
}
export function undoVersion(id) {

    return myApi.getByID(id, "MasterInterCompanyPricing/UndoVersion")
}
export function deleteVersion(id) {

    return myApi.delete(id, 'MasterInterCompanyPricing/DeleteVersion')
}