import myApi from '@/util/api'

export function getAppIPIREntrys() {

  return myApi.getAll('AppIPIREntry/GetAppIPIREntrys')
}

export function getAppIPIREntryActive(id) {

  return myApi.getByID(id, "AppIPIREntry/GetActiveAppIPIREntry")
}
export function getAppIPIREntry(data) {

    return myApi.getItem(data, "AppIPIREntry/GetData")
  }
export function createAppIPIREntry(data) {
  return myApi.create(data, "AppIPIREntry/InsertAppIPIREntry")
}

export function updateAppIPIREntry(data) {

  return myApi.update(data.AppIPIREntryID, data, "AppIPIREntry/UpdateAppIPIREntry")
}
export function deleteAppIPIREntry(data) {

  return myApi.delete(data.AppIPIREntryID, 'AppIPIREntry/DeleteAppIPIREntry')
}


