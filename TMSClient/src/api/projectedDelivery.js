import myApi from '@/util/api'

export function getProjectedDeliverys(id) {

    return myApi.getByID(id,'ProjectedDeliverySalesOrderLine/GetProjectedDeliverys')
}
export function getProjectedDelivery(data) {

    return myApi.getItem(data, "ProjectedDeliverySalesOrderLine/GetData")
}
export function createProjectedDelivery(data) {
    return myApi.create(data, "ProjectedDeliverySalesOrderLine/InsertProjectedDelivery")
}

export function updateProjectedDelivery(data) {

    return myApi.update(data.projectedDeliverySalesOrderLineID, data, "ProjectedDeliverySalesOrderLine/UpdateProjectedDelivery")
}
export function deleteProjectedDelivery(data) {

    return myApi.delete(data.projectedDeliverySalesOrderLineID, 'ProjectedDeliverySalesOrderLine/DeleteProjectedDelivery')
}
