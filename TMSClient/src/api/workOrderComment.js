import myApi from '@/util/api'
export function getWorkOrderComments(id, userId) {

    return myApi.getByuserId(id, userId, 'WorkOrderComment/GetWorkOrderComments')
}

export function createWorkOrderComment(data) {
    return myApi.create(data, "WorkOrderComment/InsertWorkOrderComment")
}
export function updateWorkOrderComment(data) {

    return myApi.update(data.workOrderCommentId, data, "WorkOrderComment/UpdateWorkOrderComment")
}
export function deleteWorkOrderComment(data) {

    return myApi.delete(data.workOrderCommentId, 'WorkOrderComment/DeleteWorkOrderComment')
}
export function updateReadComment(data) {
    return myApi.update(data.workOrderId, data, "WorkOrderComment/UpdateReadComment");
}
export function closeComment(data) {
    return myApi.update(data.workOrderCommentId, data, "WorkOrderComment/CloseComment");
}

export function getUnReadWorkOrderComments(id) {

    return myApi.getByID(id, 'WorkOrderComment/GetUnReadWorkOrderComments')
}
export function getEmployeesByDepartment() {

    return myApi.getAll('Employee/GetEmployeesByDepartment')
}
export function getWorkOrderSubject(id) {

    return myApi.getByID(id, 'WorkOrder/GetWorkOrderSubject')
}

export function getUnReadWorkOrder(id) {

    return myApi.getByID(id, 'WorkOrder/GetUnReadWorkOrder')
}
export function getUnReadWorkOrderLineDetails(id) {

    return myApi.getByID(id, 'WorkOrder/GetUnReadWorkOrderLineDetails')
}