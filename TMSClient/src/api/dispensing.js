import myApi from '@/util/api'

export function getAPPSupervisorDispensingEntrys () {

  return myApi.getAll('APPSupervisorDispensingEntry/GetAPPSupervisorDispensingEntrys')
}
export function getAPPSupervisorDispensingEntryByID (id) {

  return myApi.getByID(id, "APPSupervisorDispensingEntry/GetDispensingEntryByID")
}
export function getAPPSupervisorDispensingEntry (data) {

  return myApi.getItem(data, "APPSupervisorDispensingEntry/GetData")
}
export function getAPPSupervisorDispensingEntryFilter (data) {

  return myApi.getItem(data, "APPSupervisorDispensingEntry/GetAPPSupervisorDispensingEntryFilter")
}

