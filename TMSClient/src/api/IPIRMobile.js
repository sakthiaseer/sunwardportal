import myApi from '@/util/api'

export function getiPIRMobileFilter(data) {

  return myApi.getItem(data, "IPIRMobile/GetiPIRMobileFilter")
}

export function getIssueReportIPIRItems() {

  return myApi.getAll('IssueReportIPIR/GetIssueReportIPIRItems')
}





