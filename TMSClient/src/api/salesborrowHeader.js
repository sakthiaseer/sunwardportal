import myApi from '@/util/api'

export function getSalesBorrowHeaders() {

    return myApi.getAll('SalesBorrowHeader/GetSalesBorrowHeader')
}

export function getSalesBorrowHeader(data) {

    return myApi.getItem(data, "SalesBorrowHeader/GetData")
}
export function createSalesBorrowHeader(data) {
    return myApi.create(data, "SalesBorrowHeader/InsertSalesBorrowHeader")
}

export function updateSalesBorrowHeader(data) {

    return myApi.update(data.salesBorrowHeaderId, data, "SalesBorrowHeader/UpdateSalesBorrowHeader")
}
export function deleteSalesBorrowHeader(data) {

    return myApi.delete(data.salesBorrowHeaderId, 'SalesBorrowHeader/DeleteSalesBorrowHeader')
}
export function getSalesBorrowLinesByID(id) {

    return myApi.getByID(id, 'SalesBorrowHeader/GetSalesBorrowLinesByID')
}
export function createSalesBorrowLine(data) {
    return myApi.create(data, "SalesBorrowHeader/InsertSalesBorrowLine")
}

export function updateSalesBorrowLine(data) {

    return myApi.update(data.salesBorrowLineId, data, "SalesBorrowHeader/UpdateSalesBorrowLine")
}
export function deleteSalesBorrowLine(data) {

    return myApi.delete(data.salesBorrowLineId, 'SalesBorrowHeader/DeleteSalesBorrowLine')
}

export function getDistributorCompanyListingByName() {

    return myApi.getAll('CompanyListingLine/GetDistributorCompanyListingByName')
}
export function getGenericCodes() {

    return myApi.getAll('GenericCodes/GetGenericCodes')
}