import myApi from '@/util/api'

export function getItemSalesPrice() {

    return myApi.getAll('ItemSalesPrice/GetItemSalesPrice')
}
export function getItemSalesPricedata(data) {

    return myApi.getItem(data, "ItemSalesPrice/GetData")
}

export function createItemSalesPrice(data) {
    return myApi.create(data, "ItemSalesPrice/InsertItemSalesPrice")
}

export function updateItemSalesPrice(data) {

    return myApi.update(data.itemSalesPriceID, data, "ItemSalesPrice/UpdateItemSalesPrice")
}
export function deleteItemSalesPrice(data) {

    return myApi.delete(data.itemSalesPriceID, 'ItemSalesPrice/DeleteItemSalesPrice')
}
