import myApi from '@/util/api'

export function getClassificationBmrLine (id) {
    return myApi.getByID(id, 'ClassificationBmrLine/GetClassificationBmrLine')
}
export function createClassificationBmrLine (data) {
    return myApi.create(data, "ClassificationBmrLine/InsertClassificationBmrLine")
}
export function updateClassificationBmrLine (data) {

    return myApi.update(data.classificationBmrLineId, data, "ClassificationBmrLine/UpdateClassificationBmrLine")
}
export function deleteClassificationBmrLine (data) {

    return myApi.delete(data.classificationBmrLineId, 'ClassificationBmrLine/DeleteClassificationBmrLine')
}