import myApi from '@/util/api'

export function NoticeLineByID(id) {

    return myApi.getByID(id,'NoticeLine/GetNoticesLine')
}
export function getNoticeLine(data) {

    return myApi.getItem(data, "NoticeLine/GetData")
}
export function createNoticeLine(data) {
    return myApi.create(data, "NoticeLine/InsertNoticeLine")
}

export function updateNoticeLine(data) {

    return myApi.update(data.noticeLineId, data, "NoticeLine/UpdateNoticeLine")
}
export function deleteNoticeLine(data) {

    return myApi.delete(data.noticeLineId, 'NoticeLine/DeleteNoticeLine')
}
export function getNoticesLineByType(data) {

    return myApi.getItem(data,'NoticeLine/GetNoticesLineByType')
}


