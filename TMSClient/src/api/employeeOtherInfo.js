import myApi from '@/util/api'

export function getEmployeeOtherDutyInformationByID(id) {

    return myApi.getByID(id,'EmployeeOtherDutyInformation/GetEmployeeOtherDutyInformationByID')
}
export function getEmployeeOtherDutyInformation(data) {

    return myApi.getItem(data, "EmployeeOtherDutyInformation/GetData")
}
export function createEmployeeOtherDutyInformation(data) {
    return myApi.create(data, "EmployeeOtherDutyInformation/InsertEmployeeOtherDutyInformation")
}

export function updateEmployeeOtherDutyInformation(data) {

    return myApi.update(data.employeeOtherDutyInformationID, data, "EmployeeOtherDutyInformation/UpdateEmployeeOtherDutyInformation")
}
export function deleteEmployeeOtherDutyInformation(data) {

    return myApi.delete(data.employeeOtherDutyInformationID, 'EmployeeOtherDutyInformation/DeleteEmployeeOtherDutyInformation')
}

export function getEmployeeICTInformationByID(id) {

    return myApi.getByID(id,'EmployeeICTInformation/GetEmployeeICTInformationByID')
}
export function getEmployeeICTInformation(data) {

    return myApi.getItem(data, "EmployeeICTInformation/GetData")
}
export function createEmployeeICTInformation(data) {
    return myApi.create(data, "EmployeeICTInformation/InsertEmployeeICTInformation")
}

export function updateEmployeeICTInformation(data) {

    return myApi.update(data.employeeICTInformationID, data, "EmployeeICTInformation/UpdateEmployeeICTInformation")
}
export function deleteEmployeeICTInformation(data) {

    return myApi.delete(data.employeeICTInformationID, 'EmployeeICTInformation/DeleteEmployeeICTInformation')
}
