import myApi from '@/util/api'
export function getFinishProductReport(item) {
  return myApi.getItem(item, 'FinishProduct/GetFinishProductReport');
}

export function getFinishProductGeneralInfoReport() {
  return myApi.getAll('FinishProductGeneralInfo/GetFinishProductGeneralInfoReport');
}
export function getPharmacologicalInformationReports(item) {
  return myApi.getItem(item, 'PhramacologicalProperties/GetPharmacologicalInformationReports');
}
