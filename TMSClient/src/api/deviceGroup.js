import myApi from '@/util/api'

export function getDeviceGroupLists() {

    return myApi.getAll('DeviceGroupList/GetDeviceGroupLists')
}
export function getDeviceGroupList(data) {

    return myApi.getItem(data, "DeviceGroupList/GetData")
}

export function createDeviceGroupList(data) {
    return myApi.create(data, "DeviceGroupList/InsertDeviceGroupList")
}

export function updateDeviceGroupList(data) {

    return myApi.update(data.deviceGroupListID, data, "DeviceGroupList/UpdateDeviceGroupList")
}
export function deleteDeviceGroupList(data) {

    return myApi.delete(data.deviceGroupListID, 'DeviceGroupList/DeleteDeviceGroupList')
}
export function getCalibrationTypes() {

    return myApi.getAll('CalibrationType/GetCalibrationTypes')
}