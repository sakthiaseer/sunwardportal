import myApi from '@/util/api'

export function getDistributorReplenishment() {

    return myApi.getAll('DistributorReplenishment/GetDistributorReplenishment')
}
export function getDataList(data) {

    return myApi.getItem(data, "DistributorReplenishment/GetData")
}
export function createDistributorReplenishment(data) {
    return myApi.create(data, "DistributorReplenishment/InsertDistributorReplenishment")
}
export function updateDistributorReplenishment(data) {

    return myApi.update(data.distributorReplenishmentId, data, "DistributorReplenishment/UpdateDistributorReplenishment")
}
export function deleteDistributorReplenishment(data) {

    return myApi.delete(data.distributorReplenishmentId, 'DistributorReplenishment/DeleteDistributorReplenishment')
}
export function getDistributorCompanyListing() {

    return myApi.getAll('CompanyListingLine/GetDistributorCompanyListing')
}
//
export function updateDistributorReplenishmentversion(data) {

    return myApi.update(data.distributorReplenishmentId, data, "DistributorReplenishment/UpdateDistributorReplenishmentversion")
}


export function getDistributorReplenishmentVersion(id) {
   
    return myApi.getByID(id,'DistributorReplenishment/GetDistributorReplenishmentVersion')
}


export function updateVersionDistributorReplenishment(data) {

    return myApi.update(data.distributorReplenishmentId, data, "DistributorReplenishment/UpdateDistributorReplenishmentversion")
}

export function createVersion(data) {

    return myApi.update(data.distributorReplenishmentId, data, "DistributorReplenishment/CreateVersion")
}
export function applyVersion(data) {

    return myApi.update(data.distributorReplenishmentId, data, "DistributorReplenishment/ApplyVersion")
}
export function tempVersion(data) {

    return myApi.update(data.distributorReplenishmentId, data, "DistributorReplenishment/TempVersion")
}
export function undoVersion(id) {

    return myApi.getByID(id, "DistributorReplenishment/UndoVersion")
}
export function deleteVersion(id) {

    return myApi.delete(id, 'DistributorReplenishment/DeleteVersion')
}



export function getDistributorReplenishmentLineById(id) {

    return myApi.getByID(id,'DistributorReplenishmentLine/GetDistributorReplenishmentLineById')
}
export function getDataLine(data) {

    return myApi.getItem(data, "DistributorReplenishmentLine/GetData")
}
export function createDistributorReplenishmentLine(data) {
    return myApi.create(data, "DistributorReplenishmentLine/InsertDistributorReplenishmentLine")
}
export function updateDistributorReplenishmentLine(data) {

    return myApi.update(data.distributorReplenishmentLineId, data, "DistributorReplenishmentLine/UpdateDistributorReplenishmentLine")
}
export function deleteDistributorReplenishmentLine(data) {

    return myApi.delete(data.distributorReplenishmentLineId, 'DistributorReplenishmentLine/DeleteDistributorReplenishmentLine')
}
export function getGenericCodesDistributorReplenishment(data) {

    return myApi.getItem(data, "DistributorReplenishmentLine/GetGenericCodesDistributorReplenishment")
}