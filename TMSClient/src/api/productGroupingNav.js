import myApi from '@/util/api'

export function getProductGroupingNavItems(id) {

    return myApi.getByID(id, 'ProductGroupingNav/GetProductGroupingNavItems')
}

export function getProductGroupingNavItemsByGenericID(id) {
    return myApi.getByID(id, 'ProductGroupingNav/GetProductGroupingNavItemsByGenericID')
}

export function createProductGroupingNav(data) {
    return myApi.create(data, "ProductGroupingNav/InsertProductGroupingNav")
}

export function updateProductGroupingNav(data) {

    return myApi.update(data.productGroupingNavId, data, "ProductGroupingNav/UpdateProductGroupingNav")
}
export function deleteProductGroupingNav(data) {

    return myApi.delete(data.productGroupingNavId, 'ProductGroupingNav/DeleteProductGroupingNav')
}

export function getProductGroupingNavDifferenceItems(id) {

    return myApi.getByID(id, 'ProductGroupingNavDifference/GetProductGroupingNavDifferenceItems')
}
export function getProductGroupingNavDifferenceData(data) {

    return myApi.getItem(data, "ProductGroupingNavDifference/GetProductGroupingNavDifferenceData")
}
export function createProductGroupingNavDifference(data) {
    return myApi.create(data, "ProductGroupingNavDifference/InsertProductGroupingNavDifference")
}

export function updateProductGroupingNavDifference(data) {

    return myApi.update(data.productGroupingNavDifferenceId, data, "ProductGroupingNavDifference/UpdateProductGroupingNavDifference")
}
export function deleteProductGroupingNavDifference(data) {

    return myApi.delete(data.productGroupingNavDifferenceId, 'ProductGroupingNavDifference/DeleteProductGroupingNavDifference')
}

export function getNAVItemForProductGrouping() {
    return myApi.getAll('NavItem/GetNAVItemForProductGrouping')
}

export function getNAVItemsByCompanies(ids) {
    return myApi.getItems(ids, 'NavItem/GetNAVItemsByCompanies')
}



