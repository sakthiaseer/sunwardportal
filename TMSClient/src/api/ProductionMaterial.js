import myApi from '@/util/api'

export function getProductionMaterial() {

  return myApi.getAll('ProductionMaterial/GetProductionMaterial')
}
export function getData(data) {

  return myApi.getItem(data, "ProductionMaterial/GetData")
}

export function getProductionMaterialGenericList(genericItemNameListingId) {

  return myApi.getAll("ProductionMaterial/GetProductionMaterialGenericList/?id=" + genericItemNameListingId)
}
export function createProductionMaterial(data) {
  return myApi.create(data, "ProductionMaterial/InsertProductionMaterial")
}

export function updateProductionMaterial(data) {

  return myApi.update(data.productionMaterialId, data, "ProductionMaterial/UpdateProductionMaterial")
}
export function deleteProductionMaterial(data) {

  return myApi.delete(data.productionMaterialId, 'ProductionMaterial/DeleteProductionMaterial')
}
export function getProductionMaterialLine(ProductionMaterialId) {
  return myApi.getAll('ProductionMaterial/GetProductionMaterialLine/?id=' + ProductionMaterialId)
}
export function createProductionMaterialLine(data) {
  return myApi.create(data, "ProductionMaterial/InsertProductionMaterialLine")
}
export function updateProductionMaterialLine(data) {

  return myApi.update(data.productionMaterialLineId, data, "ProductionMaterial/UpdateProductionMaterialLine")
}
export function deleteProductionMaterialLine(data) {

  return myApi.delete(data.productionMaterialLineId, 'ProductionMaterial/DeleteProductionMaterialLine')
}
export function getProductionMaterialByRefNo(data) {
  return myApi.getItems(data, 'ProductionMaterial/GetProductionMaterialByRefNo')
}
