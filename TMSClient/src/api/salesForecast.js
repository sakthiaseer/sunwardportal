import myApi from '@/util/api'

export function getAcByDistributors() {
    return myApi.getAll('SalesForeCast/GetAcByDistributors')
}