import myApi from '@/util/api'

export function getUsers() {

  return myApi.getAll('State/GetStates')
}

export function getDepartments() {

  return myApi.getAll('State/GetStates')
}

export function getCategories() {

  return myApi.getAll('State/GetStates')
}

const getFileMenu = [{
    icon: 'photo',
    title: 'Images',
    to: {
      path: '/media',
      query: {
        type: 'image'
      }
    }
  },
  {
    icon: 'videocam',
    title: 'Video',
    to: {
      path: '/media',
      query: {
        type: 'video'
      }
    }
  },
  {
    icon: 'volume_down',
    title: 'Audio',
    to: {
      path: '/media',
      query: {
        type: 'audio'
      }
    }
  },
  {
    icon: 'insert_drive_file',
    title: 'Document',
    to: {
      path: '/media',
      query: {
        type: 'doc'
      }
    }
  },

];
const Items = [{
    'uuid': 'a32c4aec-54de-4ff4-b165-8571ae805598',
    'fileName': 'HR Files',
    'fileType': false,
    'path': 'static/.DS_Store',
    'fullPath': '/Users/michael/themeforest/vue-material-admin/static/.DS_Store',
    'ext': '',
    'dir': 'static',
    'ctime': '2018-04-08T09:15:19.307Z',
    'size': 12292
  },
  {
    'uuid': 'a30f71db-7dcf-4467-978f-e32841d47825',
    'fileName': 'Dev Files',
    'fileType': false,
    'path': 'static/.gitkeep',
    'fullPath': '/Users/michael/themeforest/vue-material-admin/static/.gitkeep',
    'ext': '',
    'dir': 'static',
    'ctime': '2018-03-14T09:21:32.010Z',
    'size': 0
  },
  {
    'uuid': 'ca1bf511-a44e-4663-8b68-323419236ddf',
    'fileName': 'wikiworkflow.png',
    'fileType': 'image/png',
    'path': 'static/avatar/google.png',
    'fullPath': '/Users/michael/themeforest/vue-material-admin/static/avatar/google.png',
    'ext': '.png',
    'dir': 'static/avatar',
    'ctime': '2018-04-08T08:31:07.808Z',
    'size': 12734
  },
  {
    'uuid': '0693e01e-926c-4c95-818b-3f9b6d5413e7',
    'fileName': 'tmsworkflow.png',
    'fileType': 'image/png',
    'path': 'static/avatar/hangouts.png',
    'fullPath': '/Users/michael/themeforest/vue-material-admin/static/avatar/hangouts.png',
    'ext': '.png',
    'dir': 'static/avatar',
    'ctime': '2018-04-08T08:31:10.010Z',
    'size': 15266
  },

];

const getFile = (limit) => {
  return (limit) ? Items.slice(0, limit) : Items;
};

export {
  getFileMenu,
  getFile
};