import myApi from '@/util/api'

export function getProcessTransfer () {

    return myApi.getAll('ProcessTransfer/GetProcessTransfers')
}
export function getDatas (data) {

    return myApi.getItem(data, "ProcessTransfer/GetData")
}
export function createProcessTransfer (data) {
    return myApi.create(data, "ProcessTransfer/InsertProcessTransfer")
}
export function updateProcessTransfer (data) {

    return myApi.update(data.processTransferId, data, "ProcessTransfer/UpdateProcessTransfer")
}
export function deleteProcessTransfer (data) {

    return myApi.delete(data.processTransferId, 'ProcessTransfer/DeleteProcessTransfer')
}

export function getProcessTransferLine (id) {

    return myApi.getByID(id,'ProcessTransfer/GetProcessTransferLines')
}
export function createProcessTransferLine (data) {
    return myApi.create(data, "ProcessTransfer/InsertProcessTransferLine")
}
export function updateProcessTransferLine (data) {

    return myApi.update(data.processTransferLineId, data, "ProcessTransfer/updateProcessTransferLine")
}
export function deleteProcessTransferLine (data) {

    return myApi.delete(data.processTransferLineId, 'ProcessTransfer/DeleteProcessTransferLine')
}