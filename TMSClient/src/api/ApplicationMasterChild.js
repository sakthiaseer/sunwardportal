import myApi from '@/util/api'

export function getApplicationMasterChild (data) {
    return myApi.getByID(data, "ApplicationMasterChild/GetApplicationMasterChildByParentID")
}

export function getApplicationMasterChildtWithChild (data) {
    return myApi.getByID(data, "ApplicationMasterChild/GetApplicationMasterChildtWithChild")
}
export function getApplicationMasterChildByParent (data) {
    return myApi.getByID(data, "ApplicationMasterChild/getApplicationMasterChildByParent")
}
export function getData (data) {

    return myApi.getItem(data, "ApplicationMasterChild/GetData")
}
export function createApplicationMasterChild (data) {
    return myApi.create(data, "ApplicationMasterChild/InsertApplicationMasterChild")
}
export function updateApplicationMasterChild (data) {

    return myApi.update(data.applicationMasterChildId, data, "ApplicationMasterChild/UpdateApplicationMasterChild")
}
export function deleteApplicationMasterChild (data) {

    return myApi.delete(data.applicationMasterChildId, 'ApplicationMasterChild/DeleteApplicationMasterChild')
}
export function getApplicationMasterChildWikiTopicList (data) {

    return myApi.getItem(data, "ApplicationMasterChild/ApplicationMasterChildWikiTopicList")
}
export function searchApplicationMasterChildWikiTopicList (data) {

    return myApi.getItem(data, "ApplicationMasterChild/SearchApplicationMasterChildWikiTopicList")
}
export function getApplicationMasterChildListID (data) {

    return myApi.getByID(data, "ApplicationMasterChild/GetApplicationMasterChildListID")
}
