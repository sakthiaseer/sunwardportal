import myApi from '@/util/api'

export function getFileProfileTypeParent() {
    return myApi.getAll("FileProfileType/GetFileProfileTypeParent")
}
export function getFileProfileTypeItems(id) {
    return myApi.getByID(id, "FileProfileType/GetFileProfileTypeItems")
}
export function createFileProfileType(data) {

    return myApi.create(data, "FileProfileType/InsertFileProfileType")
}
export function updateFileProfileType(data) {

    return myApi.update(data.fileProfileTypeId, data, "FileProfileType/UpdateFileProfileType")
}
export function updateFileProfileTypeInfo(data) {

    return myApi.update(data.fileProfileTypeId, data, "FileProfileType/UpdateFileProfileTypeInfo")
}
export function deleteFileProfileType(data) {

    return myApi.delete(data.fileProfileTypeId, 'FileProfileType/DeleteFileProfileType')
}
export function getFileProfileTypeParentId(id) {
    return myApi.getByID(id, "FileProfileType/GetFileProfileTypeParentId")
}
export function getFileProfileTypeTree(id, userId,isHidden) {

    return myApi.getAll('FileProfileType/GetFileProfileTypeTree/?id='+id+'&&userId='+userId+'&&isHidden='+isHidden);
}
export function getFileProfileTypeDocument(id, userId) {
    return myApi.getByuserId(id, userId, "FileProfileType/GetFileProfileTypeDocument")
}
export function deleteDocument(data) {

    return myApi.getItem(data, 'FileProfileType/DeleteDocument')
}
export function downloadFile(data) {

    return myApi.downLoadProfileDocument(data, "FileProfileType/DownLoadDocument")
}
export function downLoadSelfTestDocument(data) {

    return myApi.downLoadProfileDocument(data, "FileProfileType/DownLoadSelfTestDocument")
}
export function uploadDocument(data) {

    return myApi.uploadFile(data, "FileProfileType/UploadDocument")
}

export function createFileProfileTypeAccess(data) {

    return myApi.create(data, "FileProfileType/InsertFileProfileTypeAccess")
}
export function createFileProfileTypeDocumentAccess(data) {

    return myApi.create(data, "FileProfileType/InsertFileProfileTypeDocumentAccess")
}
export function getFileProfileTypeAccess(id) {
    return myApi.getByID(id, 'FileProfileType/GetFileProfileTypeAccess')

}
export function updateFileProfileTypeAccess(data) {

    return myApi.update(data.documentUserRoleID, data, "FileProfileType/UpdateDocumentUserRoleAccess")
}
export function deleteDocumentUserRoleAccess(data) {

    return myApi.update(data.itemClassificationAccessID, data, "FileProfileType/DeleteDocumentUserRoleAccess")
}

export function getFileProfileTypeDocumentByHistory(data) {

    return myApi.getItem(data, "FileProfileType/GetFileProfileTypeDocumentByHistory")
}

export function createDocument(data) {

    return myApi.uploadFile(data, "FileProfileType/CreateDocument")
}

export function getFileProfileTypePermission(id, userId) {
    return myApi.getByuserId(id, userId, "FileProfileType/GetFileProfileTypePermission")
}
export function getDocumentInfo(id, userId) {
    return myApi.getByuserId(id, userId, "FileProfileType/GetDocumentInfo")
}
export function deleteFileProfileTypeByChild(data) {

    return myApi.getItem(data, 'FileProfileType/DeleteFileProfileType')
}
export function getlatestDocumentByFileProfileTypeId(id) {
    return myApi.getByID(id, "FileProfileType/GetlatestDocumentByFileProfileTypeId")
}
export function updatecloseDocuments(data) {

    return myApi.getItem(data, "FileProfileType/UpdatecloseDocuments")
}
export function getFileProfileTypeTreeItems(id, userId) {

    return myApi.getByuserId(id, userId, 'FileProfileType/GetFileProfileTypeTreeItems');
}
export function getFileProfileTypeMoveDocument(data) {
    return myApi.getItem(data, "FileProfileType/GetFileProfileTypeMoveDocument")
}
export function getFileProfileTypeUpdateArchiveDocument(data) {
    return myApi.getItem(data, "FileProfileType/GetFileProfileTypeUpdateArchiveDocument")
}
export function getFileProfileTypeArchiveDocument(id, userId) {
    return myApi.getByuserId(id, userId, "FileProfileType/GetFileProfileTypeArchiveDocument")
}
export function updateArchiveDocument(id) {
    return myApi.getByID(id, "FileProfileType/UpdateArchiveDocument")
}

export function updateDocumentRename(data) {

    return myApi.update(data.documentID, data, "FileProfileType/UpdateDocumentRename")
}

export function getFileProfileTypeDetailsDropDown(id) {
    return myApi.getByID(id, "FileProfileType/GetFileProfileTypeDetailsDropDown")
}
export function getShareDocument(data) {

    return myApi.getItem(data, "FileProfileType/GetShareDocument")
}
export function saveShareDocument(data) {

    return myApi.getItem(data, "FileProfileType/SaveShareDocument")
}
export function getSharedDocumentsByUser(id) {
    return myApi.getByID(id, "FileProfileType/GetSharedDocumentsByUser")
}

export function getSharedViewDocumentsByUser(id) {
    return myApi.getByID(id, "FileProfileType/GetSharedViewDocumentsByUser")
}
export function getFileProfileTypeById(id, userId) {
    return myApi.getByuserId(id, userId, 'FileProfileType/GetFileProfileTypeById')

}

export function saveSetAccessByUser(data) {

    return myApi.getItem(data, "FileProfileType/InsertSetAccessByUser")
}
export function getSetAccessByUser(id) {
    return myApi.getItem(id, "FileProfileType/GetSetAccessByUser")
}

export function getFileProfilePathAll() {
    return myApi.getAll("FileProfileType/GetFileProfilePathAll")
}
export function checkDocumentCreatedTask(id) {
    return myApi.getByID(id, "TaskMaster/CheckDocumentCreatedTask")
}
export function getDocumentIdPermission(id) {
    return myApi.getByID(id, "FileProfileType/GetDocumentIdPermission")
}
export function downloadFileDoc(data) {

    return myApi.downLoadProfileDocument(data, "Documents/ConvertDocumentWordToPdf")
}
export function getChildFileProfileType(id) {

    return myApi.getByID(id, 'FileProfileType/GetChildFileProfileType')
  }
  export function getExcel(data) {

    return myApi.downLoadProfileDocument(data, 'FileProfileType/GetExcel')
}

export function updateDescriptionField(data) {

    return myApi.update(data.documentID, data, 'FileProfileType/UpdateDescriptionField');
  }
  export function updateExpiryDateField(data) {

    return myApi.update(data.documentID, data, 'FileProfileType/UpdateExpiryDateField');
  }
  export function deletePermissionDocument(data) {

    return myApi.getItem(data, 'FileProfileType/DeletePermissionDocument')
  }
  export function getFileProfileTypeDynamicForm(id,fileprofiletypeid) {

    return myApi.getAll('FileProfileTypeDynamicForm/GetFileProfileTypeDynamicForm/?id='+id+'&&fileprofiletypeid='+fileprofiletypeid)
  }
  export function getFileProfileTypeDynamicFormByFileProfile(id) {

    return myApi.getByID(id, 'FileProfileTypeDynamicForm/GetFileProfileTypeDynamicFormByFileProfile')
  }
  export function getValidFileProfileSetUpFromIds(id) {

    return myApi.getByID(id, 'FileProfileSetupForm/GetValidFileProfileSetUpFromIds')
  }
export function createSubFileProfileTypes(data) {

    return myApi.update(data.fileProfileTypeId, data, "FileProfileType/CreateSubFileProfileTypes")
}

export function getFileProfileTypeDocumentAccess(data) {

    return myApi.getItem(data, "FileProfileType/GetFileProfileTypeDocumentAccess")
}
export function getValidFileProfileIds(id) {

    return myApi.getByID(id, 'FileProfileSetupForm/GetValidFileProfileIds')
  }
  export function getFileProfileTypeListDropdownItems(id) {

    return myApi.getByID(id, 'FileProfileType/GetFileProfileTypeTreeDropDown')
}
export function getFileProfileTypeListDropdownOneItems(id,userId) {

    return myApi.getAll('FileProfileType/getFileProfileTypeListDropdownOneItems?id='+id+"&&userId="+userId)
}

export function getDocumentPermissionData(id, userId) {

    return myApi.getByuserId(id, userId, 'FileProfileType/GetDocumentPermissionData')
}
export function updateReleaseForWiki(data) {

    return myApi.getItem(data, "FileProfileType/UpdateReleaseForWiki")
  }
  export function getlatestDocumentById(id) {
    return myApi.getByID(id, "FileProfileType/GetlatestDocumentById")
}
export function getFileProfileTypeCloseDocumentAccess(id) {
    return myApi.getByID(id, 'CloseDocumentPermission/GetFileProfileTypeCloseDocumentAccess')

}
export function createCloseDocumentPermission(data) {

    return myApi.create(data, "CloseDocumentPermission/InsertCloseDocumentPermission")
}
export function updateCloseDocumentPermission(data) {

    return myApi.update(data.closeDocumentPermissionId, data, "CloseDocumentPermission/UpdateCloseDocumentPermission")
}
export function deleteCloseDocumentPermission(data) {

    return myApi.delete(data.closeDocumentPermissionId, 'CloseDocumentPermission/DeleteCloseDocumentPermission')
}

export function getSubFileProfileTypeDocuments(data) {

    return myApi.getItem(data, 'FileProfileType/GetSubFileProfileTypeDocuments')
}
export function getReleaseForWikiDashboard() {
    return myApi.getAll("FileProfileType/ReleaseForWikiDashboard")
}
export function updateAppReleaseForWiki(data) {

    return myApi.getItem(data, "FileProfileType/updateAppReleaseForWiki")
  }
  export function getFileProfileTypeFlatItems(id) {
    return myApi.getByID(id, "FileProfileType/GetFileProfileTypeFlatItems")
}
export function getFileProfileSetupFormsById(id) {
    return myApi.getByID(id, "FileProfileSetupForm/GetFileProfileSetupFormsById")
}