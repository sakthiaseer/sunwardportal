import myApi from '@/util/api'

export function getCommonPackagingSetInfo () {

    return myApi.getAll('CommonPackagingSetInfo/GetCommonPackagingSetInfo')
}
export function getData (data) {

    return myApi.getItem(data, "CommonPackagingSetInfo/GetData")
}
export function createCommonPackagingSetInfo (data) {
    return myApi.create(data, "CommonPackagingSetInfo/InsertCommonPackagingSetInfo")
}
export function updateCommonPackagingSetInfo (data) {

    return myApi.update(data.setInformationId, data, "CommonPackagingSetInfo/UpdateCommonPackagingSetInfo")
}
export function deleteCommonPackagingSetInfo (data) {

    return myApi.delete(data.setInformationId, 'CommonPackagingSetInfo/DeleteCommonPackagingSetInfo')
}
export function getCommonPackagingItemCategory (id) {

    return myApi.getByID(id,'CommonPackagingSetInfo/GetCommonPackagingItemCategory')
}