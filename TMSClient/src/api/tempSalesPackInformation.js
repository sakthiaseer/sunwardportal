import myApi from '@/util/api'

export function getTempSalesPackInformationReport() {

    return myApi.getAll('TempSalesPackInformation/GetTempSalesPackInformationReport')
}

export function createTempSalesPackInformation(data) {
    return myApi.create(data, "TempSalesPackInformation/InsertTempSalesPackInformation")
}

export function updateTempSalesPackInformation(data) {

    return myApi.update(data.tempSalesPackInformationId, data, "TempSalesPackInformation/UpdateTempSalesPackInformation")
}

export function getTempSalesPackInformationFactor(id) {

    return myApi.getByID(id, "TempSalesPackInformationFactor/GetTempSalesPackInformationFactor")
}
export function deleteTempSalesPackInformation(data) {

    return myApi.delete(data.tempSalesPackInformationId, 'TempSalesPackInformation/DeleteTempSalesPackInformation')
}
export function createTempSalesPackInformationFactor(data) {
    return myApi.create(data, "TempSalesPackInformationFactor/InsertTempSalesPackInformationFactor")
}

export function updateTempSalesPackInformationFactor(data) {

    return myApi.update(data.tempSalesPackInformationId, data, "TempSalesPackInformationFactor/UpdateTempSalesPackInformationFactor")
}

export function deleteTempSalesPackInformationFactor(data) {

    return myApi.delete(data.tempSalesPackInformationFactorID, 'TempSalesPackInformationFactor/DeleteTempSalesPackInformationFactor')
}
