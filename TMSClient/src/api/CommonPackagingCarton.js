import myApi from '@/util/api'

export function getCommonPackagingCarton (refNo) {

  return myApi.getByRefNo(refNo,'CommonPackagingCarton/GetCommonPackagingCarton')
}
export function getData (data) {

  return myApi.getItem(data, "CommonPackagingCarton/GetData")
}
export function createCommonPackagingCarton (data) {
  return myApi.create(data, "CommonPackagingCarton/InsertCommonPackagingCarton")
}
export function updateCommonPackagingCarton (data) {

  return myApi.update(data.cartonSpecificationId, data, "CommonPackagingCarton/UpdateCommonPackagingCarton")
}
export function deleteCommonPackagingCarton (data) {

  return myApi.delete(data.cartonSpecificationId, 'CommonPackagingCarton/DeleteCommonPackagingCarton')
}
export function getCommonPackagingCartonByRefNo(data) {
  return myApi.getItems(data, 'CommonPackagingCarton/GetCommonPackagingCartonByRefNo')
}