import myApi from '@/util/api'

export function getLanguageMasters() {

    return myApi.getAll('LanguageMaster/GetLanguageMasters')
}
export function getLanguageMaster(data) {

    return myApi.getItem(data, "LanguageMaster/GetData")
}
export function createLanguageMaster(data) {
    return myApi.create(data, "LanguageMaster/InsertLanguageMaster")
}

export function updateLanguageMaster(data) {

    return myApi.update(data.languageID, data, "LanguageMaster/UpdateLanguageMaster")
}
export function deleteLanguageMaster(data) {

    return myApi.delete(data.languageID, 'LanguageMaster/DeleteLanguageMaster')
}