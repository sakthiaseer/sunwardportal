import myApi from '@/util/api'

export function getTaskInviteUsers(id, userId) {

    return myApi.getByuserId(id, userId,'TaskInviteUser/GetTaskInviteUser')
}

export function checkTaskInviteUser(id, userId) {

    return myApi.getByuserId(id, userId,'TaskInviteUser/CheckTaskInviteUser')
}
export function createTaskInviteUser(data) {
    return myApi.create(data, "TaskInviteUser/InsertTaskInviteUser")
}

export function updateTaskInviteUser(data) {

    return myApi.update(data.taskInviteUserId, data, "TaskInviteUser/UpdateTaskInviteUser")
}
export function deleteTaskInviteUser(data) {

    return myApi.delete(data.inviteUserId, 'TaskInviteUser/DeleteTaskInviteUser')
}
