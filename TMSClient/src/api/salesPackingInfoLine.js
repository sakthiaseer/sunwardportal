import myApi from '@/util/api'

export function getSalesItemPackingInfoLines(id) {

    return myApi.getByID(id,'SalesItemPackingInforLine/GetSalesItemPackingInfoLine')
}
export function getSalesItemPackingInfoLine(data) {

    return myApi.getItem(data, "SalesItemPackingInforLine/GetData")
}

export function createSalesItemPackingInfoLine(data) {
    return myApi.create(data, "SalesItemPackingInforLine/InsertSalesItemPackInfoLine")
}

export function updateSalesItemPackingInfoLine(data) {

    return myApi.update(data.salesItemPackingInfoLineId, data, "SalesItemPackingInforLine/UpdateSalesItemPackingInfoLine")
}
export function deleteSalesItemPackingInfoLine(data) {

    return myApi.delete(data.salesItemPackingInfoLineId, 'SalesItemPackingInforLine/DeleteSalesItemPackingInfoLine')
}


