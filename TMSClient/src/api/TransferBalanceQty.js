import myApi from '@/util/api'

export function getTransferBalanceQty() {

    return myApi.getAll('TransferBalanceQty/GetTransferBalanceQty')
}
export function getTransferBalanceQtyData(data) {

    return myApi.getItem(data, "TransferBalanceQty/GetData")
}

export function createTransferBalanceQty(data) {
    console.log('inserttransfer data'+JSON.stringify(data));
    return myApi.create(data, "TransferBalanceQty/InsertTransferBalanceQty")
}

export function updateTransferBalanceQty(data) {

    return myApi.update(data.transferBalanceQtyId, data, "TransferBalanceQty/UpdateTransferBalanceQty")
}
export function deleteTransferBalanceQty(data) {

    return myApi.delete(data.transferBalanceQtyId, 'TransferBalanceQty/DeleteTransferBalanceQty')
}
export function uploadTransferBalanceQtyDocument(data, sessionId) {

    return myApi.uploadFile(data, "TransferBalanceQty/UploadTransferBalanceQtyDocument?sessionId=" + sessionId)
}
export function getTransferBalanceQtyDocument(sessionId) {

    return myApi.getAll("TransferBalanceQty/GetTransferBalanceQtyDocument?sessionId=" + sessionId)
}
export function deleteTransferBalanceQtyDocument(transferBalanceQtyDocumentId) {

    return myApi.delete(transferBalanceQtyDocumentId, 'TransferBalanceQty/DeleteTransferBalanceQtyDocument')
}
export function downLoadTransferBalanceQtyDocument(data) {

    return myApi.downLoadDocument(data, "TransferBalanceQty/DownLoadTransferBalanceQtyDocument")
}
export function getTransferBalanceQtyByContractId(id) {

    return myApi.getByID(id, 'TransferBalanceQty/GetTransferBalanceQtyByContractId')
}
export function getContractNosByCustomer(id) {

    return myApi.getByID(id, 'SalesOrderEntry/GetContractNosByCustomer')
}
export function getCustomerTransferContractNos(data) {

    return myApi.getItem(data, 'SalesOrderEntry/GetCustomerTransferContractNos')
}
