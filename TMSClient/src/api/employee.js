import myApi from '@/util/api'

export function getEmployees () {

  return myApi.getAll('Employee/GetEmployees')
}

export function getCompanyDirectory (data) {

  return myApi.getItem(data, 'Employee/GetCompanyDirectory')
}
export function getEmployeesByDropdown () {

  return myApi.getAll('Employee/GetEmployeesByDropdown')
}

export function getEmployee (data) {

  return myApi.getItem(data, "Employee/GetData")
}

export function createEmployee (data) {
  return myApi.create(data, "Employee/InsertEmployee")
}

export function updateEmployee (data) {

  return myApi.update(data.employeeID, data, "Employee/UpdateEmployee")
}
export function deleteEmployee (data) {

  return myApi.delete(data.employeeID, 'Employee/DeleteEmployee')
}
export function getPlants () {

  return myApi.getAll('Plant/GetPlants')
}
export function getApplicationRoles () {

  return myApi.getAll('ApplicationRole/GetApplicationRoles')
}
export function getApplicationUsers () {

  return myApi.getAll('ApplicationUser/GetApplicationUsers')
}
export function getUserGroups () {

  return myApi.getAll('UserGroup/GetUserGroups')
}
export function getEmployeeInformation (id) {

  return myApi.getByID(id, "Employee/GetEmployeeInformation")
}
export function getDepartments () {

  return myApi.getAll('Department/GetDepartments')
}
export function getDesignations () {

  return myApi.getAll('Designation/GetDesignations')
}
export function getSections () {

  return myApi.getAll('Section/GetSections')
}
export function getSubSections () {

  return myApi.getAll('SubSection/GetSubSections')
}
export function getCityMasters () {

  return myApi.getAll('CityMaster/GetCityMasters')
}
export function getLanguageMasters () {

  return myApi.getAll('LanguageMaster/GetLanguageMasters')
}
export function getApplicationUserLine (id) {

  return myApi.getByID(id, "Employee/GetApplicationUserLine")
}
export function getDepartmentByDivision (id) {

  return myApi.getByID(id, "Department/GetDepartmentByDivision")
}
export function getSectionByDepartment (id) {

  return myApi.getByID(id, "Section/GetSectionByDepartment")
}
export function getSubSectionsBySection (id) {

  return myApi.getByID(id, "SubSection/GetSubSectionsBySection")
}
export function getSubSectionsTwoBySubSection (id) {

  return myApi.getByID(id, "SubSectionTwo/GetSubSectionsTwoBySubSection")
}
export function getDesignationBySubSectionTwo (id) {

  return myApi.getByID(id, "Designation/GetDesignationBySubSectionTwo")
}
export function getDivisionsByCompany (id) {

  return myApi.getByID(id, "Division/GetDivisionsByCompany")
}
export function deleteEmployeeInformation (data) {

  return myApi.delete(data.employeeInformationId, 'Employee/DeleteEmployeeInformation')
}
export function updateEmployeeResignation (data) {

  return myApi.update(data.employeeID, data, "Employee/UpdateEmployeeResignation")
}
export function createEmployeeResignation (data) {
  return myApi.create(data, "Employee/InsertEmployeeResignation")
}
export function createEmployeeInformation (data) {
  return myApi.create(data, "Employee/InsertEmployeeInformation")
}

export function updateEmployeeInformation (data) {

  return myApi.update(data.employeeInformationID, data, "Employee/UpdateEmployeeInformation")
}
export function getEmployeeResignation (id) {

  return myApi.getByID(id, "Employee/GetEmployeeResignation")
}

export function getDesignationHeadCount (id) {

  return myApi.getByID(id, 'Designation/GetDesignationHeadCount');
}
export function getExistingnHCEmployeeInformation (data) {

  return myApi.getItem(data, "Employee/GetExistingnHCEmployeeInformation")
}
export function getExistingnHeadCountEmployee (data) {

  return myApi.getItem(data, "Employee/GetExistingnHeadCountEmployee")
}
export function getEmployeeItemsByPlant (id) {

  return myApi.getByID(id, "Employee/GetgetEmployeeItemsByPlant")
}
export function getEmployeePersonItemsByPlant (id) {

  return myApi.getByID(id, "Employee/GetEmployeePersonItemsByPlant")
}
export function getEmployeesByWeek () {

  return myApi.getAll('Employee/GetEmployeesByWeek')
}
export function getEmployeesByStatus (id) {

  return myApi.getAll("Employee/GetEmployeesByStatus?name=" + id)
}

export function getEmployeesLeaveInformation () {

  return myApi.getAll('Employee/GetEmployeesLeaveInformation')
}
export function getEmployeesByCompany (id) {

  return myApi.getByID(id, "Employee/GetEmployeesByCompany")
}
export function getEmployeeSignature(data) {

  return myApi.downLoadDocument(data, "Employee/GetEmployeeSignature")
}
export function getEmployeesUserByDropdown (id,type) {

  return myApi.getAll('Employee/GetEmployeesUserByDropdown/?id='+id+'&&type='+type+'')
}