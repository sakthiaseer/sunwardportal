import myApi from '@/util/api'

export function getUserGroups() {

    return myApi.getAll('UserGroup/GetUserGroups')
}
export function getUserGroup(data) {

    return myApi.getItem(data, "UserGroup/GetData")
}

export function createUserGroup(data) {
    return myApi.create(data, "UserGroup/InsertUserGroup")
}

export function updateUserGroup(data) {

    return myApi.update(data.userGroupID, data, "UserGroup/UpdateUserGroup")
}
export function deleteUserGroup(data) {

    return myApi.delete(data.userGroupID, 'UserGroup/DeleteUserGroup')
}
export function getApplicationUsers() {

    return myApi.getAll('ApplicationUser/GetApplicationUsers')
}
export function getDocumentRoles() {

    return myApi.getAll('Folders/GetDocumentRoles')
}
export function updateProductActivityCaseTopicLine(data) {
    return myApi.update(data, "ProductActivityCase/updateProductActivityCaseTopicLine")
}