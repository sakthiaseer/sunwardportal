import myApi from '@/util/api'

export function getSOLotInformations(id) {

    return myApi.getByID(id,'SOLotInformation/GetSOLotInformations')
}
export function getSOLotInformationData(data) {

    return myApi.getItem(data, "SOLotInformation/GetSOLotInformationData")
}

export function createSOLotInformation(data) {
    return myApi.create(data, "SOLotInformation/InsertSoLotInformation")
}

export function updateSOLotInformation(data) {

    return myApi.update(data.solotInformationId, data, "SOLotInformation/UpdateSOLotInformation")
}
export function deleteSoLotInformation(data) {

    return myApi.delete(data.solotInformationId, 'SOLotInformation/DeleteSoLotInformation')
}
