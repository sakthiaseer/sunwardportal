import myApi from '@/util/api'

export function getNpraformulation() {

    return myApi.getAll('Npraformulation/GetNpraformulation')
}
export function getSearchData(data) {

  return myApi.getItem(data, "Npraformulation/GetData")
}
export function createNpraformulation(data) {
    return myApi.create(data, "Npraformulation/InsertNpraformulation")
  }
  export function updateNpraformulation(data) {

    return myApi.update(data.npraformulationId, data, "Npraformulation/UpdateNpraformulation")
  }
  export function deleteNpraformulation(data) {
  
    return myApi.delete(data.npraformulationId, 'Npraformulation/DeleteNpraformulation')
  }


  export function getNpraformulationExcipient(id) {

    return myApi.getByID(id,'Npraformulation/GetNpraformulationExcipient')
}
export function createNpraformulationExcipient(data) {
    return myApi.create(data, "Npraformulation/InsertNpraformulationExcipient")
  }
  export function updateNpraformulationExcipient(data) {

    return myApi.update(data.npraformulationExcipientId, data, "Npraformulation/UpdateNpraformulationExcipient")
  }
  export function deleteNpraformulationExcipient(data) {
  
    return myApi.delete(data.npraformulationExcipientId, 'Npraformulation/DeleteNpraformulationExcipient')
  }



  export function getNpraformulationActiveIngredient(id) {

    return myApi.getByID(id,'Npraformulation/GetNpraformulationActiveIngredient')
}
export function createNpraformulationActiveIngredient(data) {
    return myApi.create(data, "Npraformulation/InsertNpraformulationActiveIngredient")
  }
  export function updateNpraformulationActiveIngredient(data) {

    return myApi.update(data.npraformulationActiveIngredientId, data, "Npraformulation/UpdateNpraformulationActiveIngredient")
  }
  export function deleteNpraformulationActiveIngredient(data) {
  
    return myApi.delete(data.npraformulationActiveIngredientId, 'Npraformulation/DeleteNpraformulationActiveIngredient')
  }