import myApi from '@/util/api'

export function getDepartments() {

    return myApi.getAll('Department/GetDepartments')
}
export function getDepartment(data) {

    return myApi.getItem(data, "Department/GetData")
}
export function createDepartment(data) {
    return myApi.create(data, "Department/InsertDepartment")
}

export function updateDepartment(data) {

    return myApi.update(data.departmentID, data, "Department/UpdateDepartment")
}
export function deleteDepartment(data) {
    return myApi.delete(data.departmentId, 'Department/DeleteDepartment')
}
export function getCompanies() {

    return myApi.getAll('Plant/GetPlants')
}
export function getEmployeesCommonDropDown() {

    return myApi.getAll('Employee/GetEmployeesCommonDropDown')
}
export function getDesignations() {

    return myApi.getAll('Designation/GetDesignations')
}
export function getDepartmentsByPlant (id) {

    return myApi.getByID(id, "Department/GetDepartmentsByPlant")
  }
  export function getDepartmentsByPlantString(data) {

    return myApi.getItem(data, "Department/GetDepartmentsByPlantString")
}
  