import myApi from '@/util/api'

export function getPurchaseItemSalesEntryLineItems (id) {

    return myApi.getByID(id, 'PurchaseItemSalesEntryLine/GetPurchaseItemSalesEntryLines')
}
export function getPurchaseItemSalesEntryLineData (data) {

    return myApi.getItem(data, "PurchaseItemSalesEntryLine/GetData")
}

export function createPurchaseItemSalesEntryLine (data) {
    return myApi.create(data, "PurchaseItemSalesEntryLine/InsertPurchaseItemSalesEntryLine")
}

export function updatePurchaseItemSalesEntryLine (data) {

    return myApi.update(data.purchaseItemSalesEntryLineID, data, "PurchaseItemSalesEntryLine/UpdatePurchaseItemSalesEntryLine")
}
export function deletePurchaseItemSalesEntryLine (data) {

    return myApi.delete(data.purchaseItemSalesEntryLineID, 'PurchaseItemSalesEntryLine/DeletePurchaseItemSalesEntryLine')
}

export function getSocustomersItemCrossReferenceAll () {

    return myApi.getAll('SobyCustomers/GetSocustomersItemCrossReferenceAll')
}
export function getSOCustomersAll () {

    return myApi.getAll('SobyCustomers/GetSOCustomersAll')
}
export function getSobyCustomersAddressForSalesLine () {

    return myApi.getAll('SobyCustomers/GetSobyCustomersAddressForSalesLine')
}
export function getPurchaseItemSalesEntryLinesItems () {

    return myApi.getAll('PurchaseItemSalesEntryLine/GetPurchaseItemSalesEntryLinesItems')
}

export function getPurchaseItemsByProductNo(data) {

    return myApi.getItem(data,'PurchaseItemSalesEntryLine/GetPurchaseItemsByProductNo')
}

export function getNavItemsForSalesEntry(id) {

    return myApi.getByID(id,'NavItem/GetNavItemsForSalesEntry')
}
export function getSocustomersItemCrossReferenceForPurchaseItem (id) {

    return myApi.getByID(id,'SobyCustomers/GetSocustomersItemCrossReferenceForPurchaseItem')
}

export function getNavItemByDescription(itemNo) {

    return myApi.getByNavItem(itemNo,'NavItem/GetNavItemByDescription')
}

export function getSocustomersItemCrossReferenceByCustomer(id) {

    return myApi.getByID(id,'SobyCustomers/GetSocustomersItemCrossReferenceByCustomer')
}

export function getSocustomersItemCrossReferenceForSalesNovate (id) {

    return myApi.getByID(id,'SobyCustomers/GetSocustomersItemCrossReferenceForSalesNovate')
}

export function getGenericCodeForReference(id) {

    return myApi.getByID(id,'PurchaseItemSalesEntryLine/GetGenericCodeForReference')
}

export function getPurchaseItemSalesEntryLineById(id) {

    return myApi.getByID(id,'PurchaseItemSalesEntryLine/GetPurchaseItemSalesEntryLineById')
}

export function getNavisionReference(data) {

    return myApi.getItem(data,'PurchaseItemSalesEntryLine/GetNavisionReference')
}