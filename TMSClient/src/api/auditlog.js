import myApi from '@/util/api'


export function getAuditLogs(data) {
    return myApi.getItem(data, "AuditLog/GetAuditLogs")
}