import myApi from '@/util/api'

export function getProductionActivityApp () {

    return myApi.getAll('ProductActivityApp/GetProductActivityApp')
}
export function getSearchData (data) {

    return myApi.getItem(data, "ProductActivityApp/GetData")
}
export function createProductionActivityApp (data) {
    return myApi.create(data, "ProductActivityApp/InsertProductActivityApp")
}
export function updateProductionActivityApp (data) {

    return myApi.update(data.productionActivityAppId, data, "ProductActivityApp/UpdateProductActivityApp")
}
export function deleteProductionActivityApp (data) {

    return myApi.delete(data.productionActivityAppLineId, 'ProductActivityApp/DeleteProductionActivityApp')
}
export function getProductActivityCaseCategory(id,mprocessId) {
    return myApi.getAll("ProductActivityCase/GetProductActivityCaseCategory?mprocessId="+mprocessId+"&&id="+id)
}
export function getNavprodOrdersNo(id) {
    return myApi.getByID( id,"ProductActivityApp/GetNavprodOrdersNo")
}
export function getNavprodOrdersNoList(type) {
    return myApi.getByType(type, "ProductActivityApp/GetNavprodOrdersNoList")
}