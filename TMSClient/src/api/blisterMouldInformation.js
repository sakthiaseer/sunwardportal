import myApi from '@/util/api'

export function getBlisterMouldInformation () {

  return myApi.getAll('BlisterMouldInformation/GetBlisterMouldInformation')
}
export function getData (data) {

  return myApi.getItem(data, "BlisterMouldInformation/GetData")
}
export function createBlisterMouldInformation (data) {
  return myApi.create(data, "BlisterMouldInformation/InsertBlisterMouldInformation")
}
export function updateBlisterMouldInformation (data) {

  return myApi.update(data.blisterMouldInformationId, data, "BlisterMouldInformation/UpdateBlisterMouldInformation")
}
export function deleteBlisterMouldInformation (data) {

  return myApi.delete(data.blisterMouldInformationId, 'BlisterMouldInformation/DeleteBlisterMouldInformation')
}

export function removeDocumentBlisterMouldInformation (data) {

  return myApi.delete(data, 'BlisterMouldInformation/RemoveDocumentBlisterMouldInformation')
}
export function getBlisterMouldInformationByPartName (type) {

  return myApi.getByType(type, 'BlisterMouldInformation/GetBlisterMouldInformationByPartName')
}
export function getBlisterMouldInformationByRefNo (data) {
  return myApi.getItems(data, 'BlisterMouldInformation/GetBlisterMouldInformationByRefNo')
}