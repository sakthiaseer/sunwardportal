import myApi from '@/util/api'
export function getDistributorCompanyListingByID (id) {

    return myApi.getByID(id, 'CompanyListingLine/GetDistributorCompanyListingByID')
}
export function getSellingPriceInformationsByCompanyID (data) {

    return myApi.getItem(data, 'SellingPriceInformation/GetSellingPriceInformationsByCompanyID')
}
export function getInterCompanyPricingSummary (data) {

    return myApi.getItem(data, 'InterCompanyPricingSummary/GetInterCompanyPricingSummary')
}
export function getDistributorCompanyListingByName () {

    return myApi.getAll('CompanyListingLine/GetDistributorCompanyListingByName')
}