import myApi from '@/util/api'

export function getDesignations() {

    return myApi.getAll('Designation/GetDesignations')
}
export function getDesignation(data) {

    return myApi.getItem(data, "Designation/GetData")
}
export function createDesignation(data) {
    return myApi.create(data, "Designation/InsertDesignation")
}

export function updateDesignation(data) {

    return myApi.update(data.designationID, data, "Designation/UpdateDesignation")
}
export function deleteDesignation(data) {

    return myApi.delete(data.designationID, 'Designation/DeleteDesignation')
}
export function getCompanies() {

    return myApi.getAll('Plant/GetPlants')
}
export function getLevelMasters() {

    return myApi.getAll('LevelMaster/GetLevelMasters')
}

