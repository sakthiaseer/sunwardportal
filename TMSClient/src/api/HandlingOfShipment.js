import myApi from '@/util/api'

export function createHandlingOfShipmentClassification (data) {
    return myApi.create(data, "HandlingOfShipmentClassification/InsertHandlingOfShipmentClassification")
}

export function updateHandlingOfShipmentClassification (data) {

    return myApi.update(data.handlingOfShipmentId, data, "HandlingOfShipmentClassification/UpdateHandlingOfShipmentClassification")
}
export function deleteHandlingOfShipmentClassification (data) {

    return myApi.delete(data.handlingOfShipmentId, 'HandlingOfShipmentClassification/DeleteHandlingOfShipmentClassification')
}
export function getHandlingOfShipmentClassificationByRefNo (id) {

    return myApi.getByID(id, 'HandlingOfShipmentClassification/GetHandlingOfShipmentClassificationByRefNo')
}