import myApi from '@/util/api'



export function getPackagingLabel(data) {

    return myApi.getItem(data, "PackagingLabel/GetData")
}
export function getPackagingLabels(){
    return myApi.getAll("PackagingLabel/GetPackagingLabels")
}
export function createPackagingLabel(data) {
    return myApi.create(data, "PackagingLabel/InsertPackagingLabel")
}

export function updatePackagingLabel(data) {

    return myApi.update(data.packagingLabelId, data, "PackagingLabel/UpdatePackagingLabel")
}
export function deletePackagingLabel(data) {

    return myApi.delete(data.packagingLabelId, 'PackagingLabel/DeletePackagingLabel')
}
export function getPackagingLabelsByRefNo(data) {
    return myApi.getItems(data, 'PackagingLabel/GetPackagingLabelsByRefNo')
  }

