import myApi from '@/util/api'

export function getGenericCodes () {

    return myApi.getAll('GenericCodes/GetGenericCodes')
}
export function getGenericCode (data) {

    return myApi.getItem(data, "GenericCodes/GetData")
}

export function getGenericCodesByStatus () {
    return myApi.getByID(id, "NavItem/GetGenericCodesByStatus");
}

export function createGenericCodes (data) {
    return myApi.create(data, "GenericCodes/InsertGenericCodes")
}

export function updateGenericCodes (data) {

    return myApi.update(data.genericCodeID, data, "GenericCodes/UpdateGenericCodes")
}
export function deleteGenericCodes (data) {

    return myApi.delete(data.genericCodeID, 'GenericCodes/DeleteGenericCodes')
}
export function getGenericCodesByCompany (id) {

    return myApi.getByID(id, 'GenericCodes/GetGenericCodesByCompany')
}
export function getGenericCodesAll () {

    return myApi.getAll('NavItem/GetGenericCodes')
}
export function getGenericCodeByPlantID (id) {

    return myApi.getByID(id, 'NavItem/GetGenericCodeByPlantID')
}

export function getGenericCodesByCompanyLisitng (refNo) {

    return myApi.getByRefNo(refNo, 'GenericCodes/GetGenericCodesByCompanyLisitng')
}

export function getGenericCodesByCompanyLisitngID (id) {
    return myApi.getByID(id, 'GenericCodes/GetGenericCodesByCompanyLisitngID')
}

export function getGenericCodeItems () {
    return myApi.getAll('GenericCodes/GetGenericCodeItems')
}

export function getGenericCodeSupplyToMultipleByCountry (id, countryId) {

    return myApi.getAll('GenericCodes/GetGenericCodeSupplyToMultipleByCountry?id=' + id + "&&countryId=" + countryId)
}
export function getGenericCodeSupplyToMultipleById (id) {

    return myApi.getByID(id, 'GenericCodes/GetGenericCodeSupplyToMultipleById')
}


export function deleteGenericCodeCountry (data) {

    return myApi.update(data.genericCodeId, data, "GenericCodes/DeleteGenericCodeCountry")
}
export function deleteGenericCodeSupplyToMultiple (data) {

    return myApi.update(data.genericCodeId, data, "GenericCodes/DeleteGenericCodeSupplyToMultiple")
}