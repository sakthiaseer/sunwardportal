import myApi from '@/util/api'

export function getBlisterScrapMasters() {

    return myApi.getAll('BlisterScrapMaster/GetBlisterScrapMasters')
}
export function getBlisterScrapMaster(data) {

    return myApi.getItem(data, "BlisterScrapMaster/GetData")
}

export function createBlisterScrapMaster(data) {
    return myApi.create(data, "BlisterScrapMaster/InsertBlisterScrapMaster")
}

export function updateBlisterScrapMaster(data) {
    console.log(data);
    return myApi.update(data.blisterScrapId, data, "BlisterScrapMaster/UpdateBlisterScrapMaster")
}
export function deleteBlisterScrapMaster(data) {

    return myApi.delete(data.blisterScrapId, 'BlisterScrapMaster/DeleteBlisterScrapMaster')
}

export function getPlasticbags() {

    return myApi.getAll('PlasticBag/GetPlasticBags')
}
export function getScrapType() {

    return myApi.getAll('BlisterScrapCode/GetBlisterScrapCodes')
}