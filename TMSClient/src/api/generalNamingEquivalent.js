import myApi from '@/util/api'

export function getGeneralEquivalentNavisions() {
  return myApi.getAll('GeneralEquivalentNavision/GetGeneralEquivalentNavisions')
}

export function getGeneralEquivalentNavision(data) {

  return myApi.getItem(data, "GeneralEquivalentNavision/GetData")
}
export function createGeneralEquivalentNavision(data) {
  return myApi.create(data, "GeneralEquivalentNavision/InsertGeneralEquivalentNavision")
}
export function updateGeneralEquivalentNavision(data) {

  console.log(data);
  return myApi.update(data.generalEquivalentNavisionID, data, "GeneralEquivalentNavision/UpdateGeneralEquivalentNavision")
}
export function deleteGeneralEquivalentNavision(data) {

  return myApi.delete(data.generalEquivalentNavisionID, 'GeneralEquivalentNavision/DeleteGeneralEquivalentNavision')
}
export function getGeneralEquivalentNavisionsByRefNo(data) {
  return myApi.getItems(data, 'GeneralEquivalentNavision/GetGeneralEquivalentNavisionsByRefNo')
}