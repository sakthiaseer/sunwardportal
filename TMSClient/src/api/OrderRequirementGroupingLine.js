import myApi from '@/util/api'

export function getOrderRequirementGroupingLine (id) {

    return myApi.getByID(id, 'OrderRequirementGroupingLine/GetOrderRequirementGroupingLine')
}
export function createOrderRequirementGroupingLine (data) {
    return myApi.create(data, "OrderRequirementGroupingLine/InsertOrderRequirementGroupingLine")
}

export function updateOrderRequirementGroupingLine (data) {

    return myApi.update(data.orderRequirementGroupingLineId, data, "OrderRequirementGroupingLine/UpdateOrderRequirementGroupingLine")
}
export function deleteOrderRequirementGroupingLine (data) {

    return myApi.delete(data.orderRequirementGroupingLineId,'OrderRequirementGroupingLine/DeleteOrderRequirementGroupingLine')
}


export function gettOrderRequirementGroupingLineSplit (id) {

    return myApi.getByID(id, 'OrderRequirementGroupingLine/GetOrderRequirementGroupingLineSplit')
}
export function createtOrderRequirementGroupingLineSplit (data) {
    return myApi.create(data, "OrderRequirementGroupingLine/InsertOrderRequirementGroupingLineSplit")
}

export function updatetOrderRequirementGroupingLineSplit(data) {

    return myApi.update(data.orderRequirementGrouPinglineSplitId, data, "OrderRequirementGroupingLine/UpdateOrderRequirementGroupingLineSplit")
}
export function deletetOrderRequirementGroupingLineSplit (data) {

    return myApi.delete(data.orderRequirementGrouPinglineSplitId,'OrderRequirementGroupingLine/DeleteOrderRequirementGroupingLineSplit')
}