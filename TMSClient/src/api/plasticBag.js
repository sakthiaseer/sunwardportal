import myApi from '@/util/api'

export function getPlasticBags() {

    return myApi.getAll('PlasticBag/GetPlasticBags')
}
export function getPlasticBag(data) {

    return myApi.getItem(data, "PlasticBag/GetData")
}

export function createPlasticBag(data) {
    return myApi.create(data, "PlasticBag/InsertPlasticBag")
}

export function updatePlasticBag(data) {

    return myApi.update(data.plasticBagID, data, "PlasticBag/UpdatePlasticBag")
}
export function deletePlasticBag(data) {

    return myApi.delete(data.plasticBagID, 'PlasticBag/DeletePlasticBag')
}