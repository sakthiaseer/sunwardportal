import myApi from '@/util/api'

export function getCommitmentOrders() {

    return myApi.getAll('CommitmentOrders/GetCommitmentOrders')
}
export function getCommitmentOrdersData(data) {

    return myApi.getItem(data, "CommitmentOrders/GetCommitmentOrdersData")
}
export function createCommitmentOrders(data) {
    return myApi.create(data, "CommitmentOrders/InsertCommitmentOrders")
}
export function updateCommitmentOrders(data) {

    return myApi.update(data.commitmentOrdersId, data, "CommitmentOrders/UpdateCommitmentOrders")
}
export function deleteCommitmentOrders(data) {

    return myApi.delete(data.commitmentOrdersId, 'CommitmentOrders/DeleteCommitmentOrders')
}


export function getCommitmentOrdersLine(commitmentOrdersId) {
    return myApi.getAll('CommitmentOrders/GetCommitmentOrdersLine/?id=' + commitmentOrdersId)
}
export function createCommitmentOrdersLine(data) {
    return myApi.create(data, "CommitmentOrders/InsertCommitmentOrdersLine")
}
export function updateCommitmentOrdersLine(data) {

    return myApi.update(data.commitmentOrdersLineId, data, "CommitmentOrders/UpdateCommitmentOrdersLine")
}
export function deleteCommitmentOrdersLine(data) {

    return myApi.delete(data.commitmentOrdersLineId, 'CommitmentOrders/DeleteCommitmentOrdersLine')
}

export function getCommitmentOrdersReports() {

    return myApi.getAll('CommitmentOrders/GetCommitmentOrdersReports')
}
export function getCommitmentOrdersReportBySearch(data) {

    return myApi.getItem(data, "CommitmentOrders/GetCommitmentOrdersReportBySearch")
}
export function updateCommitmentOrdersLineStatus(data) {

    return myApi.update(data.commitmentOrdersLineId, data, "CommitmentOrders/UpdateCommitmentOrdersLineStatus")
}





