import myApi from '@/util/api'

export function getSobyCustomersByRefNo(data) {
    return myApi.getItems(data, 'SobyCustomers/GetSobyCustomersByRefNo')
}
export function createSobyCustomers(data) {
    return myApi.create(data, "SobyCustomers/InsertSobyCustomers")
}
export function updateSobyCustomers(data) {

    return myApi.update(data.sobyCustomersId, data, "SobyCustomers/UpdateSobyCustomers")
}
export function deleteSobyCustomers(data) {

    return myApi.delete(data.sobyCustomersId, 'SobyCustomers/DeleteSobyCustomers')
}
export function getSobyCustomersAddress(id) {

    return myApi.getByID(id, 'SobyCustomers/GetSobyCustomersAddress')
}
export function deleteSobyCustomersAddress(data) {

    return myApi.delete(data.sobyCustomersAddressId, 'SobyCustomers/DeleteSobyCustomersAddress')
}
export function getSobyCustomerSunwardEquivalent(id) {

    return myApi.getByID(id, 'SobyCustomers/GetSobyCustomerSunwardEquivalent')
}
export function deleteSobyCustomerSunwardEquivalent(data) {

    return myApi.delete(data.sobyCustomerSunwardEquivalentId, 'SobyCustomers/DeleteSobyCustomerSunwardEquivalent')
}
export function getSobyCustomersAddressDetails() {

    return myApi.getAll('SobyCustomers/GetSobyCustomersAddressDetails')
}
export function getSocustomersIssue(id) {

    return myApi.getByID(id, 'SobyCustomers/GetSocustomersIssue')
}
export function getSocustomersSalesInfomation(id) {

    return myApi.getByID(id, 'SobyCustomers/GetSocustomersSalesInfomation')
}
export function getSoCustomersDelivery(id) {

    return myApi.getByID(id, 'SobyCustomers/GetSoCustomersDelivery')
}

export function getSocustomersItemCrossReference(id) {

    return myApi.getByID(id, 'SobyCustomers/GetSocustomersItemCrossReference')
}
export function deleteSobyCustomersIssue(data) {

    return myApi.delete(data.socustomersIssueId, 'SobyCustomers/DeleteSobyCustomersIssue')
}

export function deleteSobyCustomersDelivery(data) {

    return myApi.delete(data.socustomersDeliveryId, 'SobyCustomers/DeleteSobyCustomersDelivery')
}
export function deleteSocustomersSalesInfomation(data) {

    return myApi.delete(data.socustomersSalesInfomationId, 'SobyCustomers/DeleteSocustomersSalesInfomation')
}
export function deleteSocustomersItemCrossReference(data) {

    return myApi.delete(data.socustomersItemCrossReferenceId, 'SobyCustomers/DeleteSocustomersItemCrossReference')
}
export function getSocustomersItemCrossReferenceReport(id) {

    return myApi.getByID(id, 'SobyCustomers/GetSocustomersItemCrossReferenceReport')
}
export function getCompanyListings() {

    return myApi.getAll('CompanyListing/GetCompanyListings')
}
export function getNavisionCompanyReport(data) {

    return myApi.getItems(data, 'NavisionCompany/NavisionCompanyReport')
}

export function createSobyCustomersHeaders(data) {
    return myApi.create(data, "SobyCustomers/InsertSobyCustomersHeaders")
}
export function createSocustomersItemCrossReference(data) {
    return myApi.create(data, "SobyCustomers/InsertSocustomersItemCrossReference")
}
export function createSobyCustomerSunwardEquivalent(data) {
    return myApi.create(data, "SobyCustomers/InsertSobyCustomerSunwardEquivalent")
}
export function createSocustomersIssue(data) {
    return myApi.create(data, "SobyCustomers/InsertSocustomersIssue")
}
export function updateSocustomersIssue(data) {

    return myApi.update(data.socustomersIssueId, data, "SobyCustomers/UpdateSocustomersIssue")
}

export function updateSocustomersItemCrossReference(data) {

    return myApi.update(data.socustomersItemCrossReferenceId, data, "SobyCustomers/UpdateSocustomersItemCrossReference")
}
export function updateSobyCustomerSunwardEquivalent(data) {

    return myApi.update(data.sobyCustomerSunwardEquivalentId, data, "SobyCustomers/UpdateSobyCustomerSunwardEquivalent")
}

export function updateSobyCustomerSunwardEquivalentStatus(data) {

    return myApi.update(data.sobyCustomerSunwardEquivalentId, data, "SobyCustomers/UpdateSobyCustomerSunwardEquivalentStatus")
}
export function getNAVItemsByCompanies() {

    return myApi.getAll('SobyCustomers/GetNAVItemsByCompanies')
}
export function createSocustomersDelivery(data) {
    return myApi.create(data, "SobyCustomers/InsertSocustomersDelivery")
}
export function updateSocustomersDelivery(data) {

    return myApi.update(data.socustomersDeliveryId, data, "SobyCustomers/UpdateSocustomersDelivery")
}
export function createSobyCustomersAddress(data) {
    return myApi.create(data, "SobyCustomers/InsertSobyCustomersAddress")
}
export function updateSobyCustomersAddress(data) {

    return myApi.update(data.sobyCustomersAddressId, data, "SobyCustomers/UpdateSobyCustomersAddress")
}


export function getSobyCustomersShippingAddressBySO(id) {

    return myApi.getByID(id, 'SobyCustomers/GetSobyCustomersShippingAddressBySO')
}
export function createSobyCustomersContactInfo(data) {
    return myApi.create(data, "SobyCustomers/InsertSobyCustomersContactInfo")
}
export function updateSobyCustomersContactInfo(data) {

    return myApi.update(data.sobyCustomerContactInfoId, data, "SobyCustomers/UpdateSobyCustomersContactInfo")
}
export function getSobyCustomersContactInfo(id) {

    return myApi.getByID(id, 'SobyCustomers/GetSobyCustomersContactInfo')
}
export function deleteSobyCustomersContactInfo(data) {

    return myApi.delete(data.sobyCustomerContactInfoId, 'SobyCustomers/DeleteSobyCustomersContactInfo')
}