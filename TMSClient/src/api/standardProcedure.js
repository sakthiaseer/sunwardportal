import myApi from '@/util/api'



export function getStandardProcedure(data) {

    return myApi.getItem(data, "StandardProcedure/GetData")
}
export function getStandardProcedures(id){
    return myApi.getByID(id,"StandardProcedure/GetStandardProcedures")
}
export function createStandardProcedure(data) {
    return myApi.create(data, "StandardProcedure/InsertStandardProcedure")
}

export function updateStandardProcedure(data) {

    return myApi.update(data.standardProcedureID, data, "StandardProcedure/UpdateStandardProcedure")
}
export function deleteStandardProcedure(data) {

    return myApi.delete(data.standardProcedureID, 'StandardProcedure/DeleteStandardProcedure')
}

