import myApi from '@/util/api'

export function getFinishProductLine (finishProductId) {
    return myApi.getAll('FinishProductLine/GetFinishProductLine/?id='+finishProductId)
  }
export function createFinishProductLine(data) {
    return myApi.create(data, "FinishProductLine/InsertFinishProductLine")
  }
  export function updatefinishProductLine(data) {

    return myApi.update(data.finishProductLineId, data, "FinishProductLine/UpdateFinishProductLine")
  }
  export function deleteFinishProductLine(data) {

    return myApi.delete(data.finishProductLineId, 'FinishProductLine/DeleteFinishProductLine')
  }