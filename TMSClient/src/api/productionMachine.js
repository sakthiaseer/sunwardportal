import myApi from '@/util/api'

export function getProductionMachines() {
    return myApi.getAll('ProductionMachine/GetProductionMachines')
}
export function getProductionMachine(data) {
    return myApi.getItem(data, "ProductionMachine/GetData")
}

export function createProductionMachine(data) {
    return myApi.create(data, "ProductionMachine/InsertProductionMachine")
}

export function updateProductionMachine(data) {
    return myApi.update(data.productionMachineID, data, "ProductionMachine/UpdateProductionMachine")
}
export function deleteProductionMachine(data) {
    return myApi.delete(data.productionMachineID, 'ProductionMachine/DeleteProductionMachine')
}
export function getProductionMachinesByRefNo(data) {
    return myApi.getItems(data, 'ProductionMachine/GetProductionMachinesByRefNo')
}