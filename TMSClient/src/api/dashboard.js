import myApi from '@/util/api'


export function getDashboard(data) {
    return myApi.getByID(data, "TaskMaster/GetDashboard")
}
export function getEmailDashboard(data) {
    return myApi.getByID(data, "TaskMaster/GetIsemailDashboard")
}
export function getUnreadDashboard(data) {
    return myApi.getByID(data, "TaskMaster/GetUnreadDashboard")
}
export function getReminderDashboard(data) {
    return myApi.getByID(data, "TaskMaster/GetReminderDashboard")
}
export function getTaskCommentDashboard(data) {
    return myApi.getByID(data, "TaskMaster/GetTaskCommentDashboard")
}
export function getUpDueDashboard(data) {
    return myApi.getByID(data, "TaskMaster/GetUpDueDashboard")
}
export function getUpDueMessageDashboard(data) {
    return myApi.getByID(data, "TaskMaster/GetUpDueMessageDashboard")
}
export function getDueDashboard(data) {
    return myApi.getByID(data, "TaskMaster/GetDueDashboard")
}
export function getDashAssigned(data) {
    return myApi.getByID(data, "TaskMaster/getDashAssigned")
}
export function getDashAssignedBy(data) {
    return myApi.getByID(data, "TaskMaster/GetDashAssignedBy")
}
export function getDashInbox(data) {
    return myApi.getByID(data, "TaskMaster/GetDashInbox")
}
export function getTaskAppointmentsByUser(data) {
    return myApi.getByID(data, "TaskAppointment/GetTaskAppointmentsByUser")
}
export function getSuperUserDashboard(id) {
    return myApi.getByID(id, 'TaskMaster/GetSuperUserDashboard')
}

export function getApplicationFormByScreenID(data) {
    return myApi.getByScreen(data, "ApplicationForm/GetApplicationFormByScreenID")
}
export function getTaskCommentMessageDashboard(id) {
    return myApi.getByID(id, 'TaskMaster/GetTaskCommentMessageDashboard')
}
export function getUpComingDueMessageDashboard(id) {
    return myApi.getByID(id, 'TaskMaster/GetUpComingDueMessageDashboard')
}

