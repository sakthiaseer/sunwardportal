import myApi from '@/util/api'

export function getVendorPayment() {

    return myApi.getAll('VendorPayment/GetVendorPayment')
}
export function getVendorPayment(data) {

    return myApi.getItem(data, "VendorPayment/GetData")
}

export function createVendorPayment(data) {
    return myApi.create(data, "VendorPayment/InsertVendorPayment")
}

export function updateVendorPayment(data) {

    return myApi.update(data.paymentID, data, "VendorPayment/UpdateVendorPayment")
}
export function deleteVendorPayment(data) {

    return myApi.delete(data.paymentID, 'VendorPayment/DeleteVendorPayment')
}
