import myApi from '@/util/api'

export function getFPManufacturingRecipes () {

    return myApi.getAll('FPManufacturingRecipe/GetFPManufacturingRecipes')
}
export function getFPManufacturingRecipe (data) {

    return myApi.getItem(data, "FPManufacturingRecipe/GetData")
}

export function createFPManufacturingRecipe (data) {
    return myApi.create(data, "FPManufacturingRecipe/InsertFPManufacturingRecipe")
}

export function updateFPManufacturingRecipe (data) {

    return myApi.update(data.fpManufacturingRecipeID, data, "FPManufacturingRecipe/UpdateFPManufacturingRecipe")
}
export function deleteFPManufacturingRecipe (data) {

    return myApi.delete(data.fpManufacturingRecipeID, 'FPManufacturingRecipe/DeleteFPManufacturingRecipe')
}
export function generateFPManufacturingRecipes (data) {

    return myApi.getItem(data, "FPManufacturingRecipe/GenerateFPManufacturingRecipes")
}

export function updateFPManufacturingRecipeMaster (data) {

    return myApi.update(data.fpManufacturingRecipeID, data, "FPManufacturingRecipe/updateFPManufacturingRecipeMaster")
}

export function getFPManufacturingRecipesByRefNo (data) {
    return myApi.getItems(data, 'FPManufacturingRecipe/GetFPManufacturingRecipesByRefNo')
}
export function getManufacturingRecipesForBOMByRefNo (data) {
    return myApi.getItems(data, 'FPManufacturingRecipe/GetManufacturingRecipesForBOMByRefNo')
}