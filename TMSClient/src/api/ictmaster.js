import myApi from '@/util/api'

export function getICTMasters() {

  return myApi.getAll('ICTMaster/GetICTMasters')
}
export function getICTMasterByID(id) {

  return myApi.getByID(id, "ICTMaster/GetICTMaster")
}
export function getPublicFolderDocuments(id) {

  return myApi.getByID(id, "ICTMaster/GetPublicFolderDocuments")
}
export function getPublicFolderCheckDocuments(id) {

  return myApi.getByID(id, "ICTMaster/GetPublicFolderCheckDocuments")
}
export function getICTMasterActive(id) {

  return myApi.getByID(id, "ICTMaster/GetICTMasterActive")
}
export function getICTMaster(data) {

  return myApi.getItem(data, "ICTMaster/GetData")
}
export function createICTMaster(data) {
  return myApi.create(data, "ICTMaster/InsertICTMaster")
}

export function updateICTMaster(data) {

  return myApi.update(data.ictMasterID, data, "ICTMaster/UpdateICTMaster")
}
export function deleteICTMaster(data) {

  return myApi.delete(data.ictMasterID, 'ICTMaster/DeleteICTMaster')
}
export function getPlants() {

  return myApi.getAll('Plant/GetPlants')
}
export function getICTLayoutPlanType(id) {

  return myApi.getByID(id, "ICTMaster/GetICTLayoutPlanType")
}


export function downLoadICTMasterDocument(data) {

  return myApi.downLoadDocument(data, "ICTMaster/DownLoadICTMasterDocument")
}
export function uploadICTMasterDocuments(data, sessionId) {

  return myApi.uploadFile(data, "ICTMaster/UploadICTMasterDocuments?sessionId=" + sessionId)
}

export function getICTMasterDocument(sessionId) {

  return myApi.getAll("ICTMaster/GetICTMasterDocument?sessionId=" + sessionId)
}
export function deleteICTMasterDocuments(data) {

  return myApi.delete(data.ictMasterDocumentID, 'ICTMaster/DeleteICTMasterDocuments')
}

export function createLayoutTypes(data) {
  return myApi.create(data, "ICTMaster/InsertLayoutTypes")
}

export function updateLayoutTypes(data) {

  return myApi.update(data.ictlayoutTypeId, data, "ICTMaster/UpdateLayoutTypes")
}
export function deleteLayoutTypes(data) {

  return myApi.delete(data.ictlayoutTypeId, 'ICTMaster/DeleteLayoutTypes')
}

export function getICTMasterLocationDropDown(id) {

  return myApi.getByID(id, "ICTMaster/GetICTMasterLocationDropDown")
}
export function getICTMasterByCompany(data) {

  return myApi.getItem(data, "ICTMaster/GetICTMasterByCompany")
}
