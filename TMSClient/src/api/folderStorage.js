import myApi from '@/util/api'

export function getFolderStorages() {

    return myApi.getAll('FolderStorage/GetFolderStorageModel')
}
export function getFolderStorage(data) {

    return myApi.getItem(data, "FolderStorage/GetData")
}

export function createFolderStorage(data) {
    return myApi.create(data, "FolderStorage/InsertFolderStorage")
}

export function updateFolderStorage(data) {

    return myApi.update(data.folderStorageID, data, "FolderStorage/UpdateFolderStorage")
}
export function deleteFolderStorage(data) {

    return myApi.delete(data.folderStorageID, 'FolderStorage/DeleteFolderStorage')
}
export function getApplicationUsers() {

    return myApi.getAll('ApplicationUser/GetApplicationUsers')
}
export function getUserGroups() {

    return myApi.getAll('TaskMaster/GetUserGroups')
}
export function getUserById(id) {

    return myApi.getByID(id, 'ApplicationUser/GetUsersById')
}
export function getFolders () {

    return myApi.getAll('Folders/GetFolders')
  }