import myApi from '@/util/api'


export function getApplicationRolePermissions() {

    return myApi.getAll('ApplicationRolePermission/GetApplicationRolePermissions')
}
export function getApplicationRoles() {

    return myApi.getAll('ApplicationRole/GetApplicationRoles')
}
export function getApplicationPermissions(id) {

    return myApi.getByID(id, 'ApplicationRolePermission/GetApplicationPermissions')
}
export function getApplicationRolePermission(data) {

    return myApi.getItem(data, "ApplicationRolePermission/GetData")
}

export function createApplicationRolePermission(data) {
    return myApi.create(data, "ApplicationRolePermission/InsertApplicationRolePermission")
}

export function updateApplicationRolePermission(data) {

    return myApi.update(data.roleID, data, "ApplicationRolePermission/UpdateApplicationRolePermission")
}
export function deleteApplicationRolePermission(data) {

    return myApi.delete(data.roleID, 'ApplicationRolePermission/DeleteApplicationRolePermission')
}

export function insertApplicationRolePermissions(data) {
    return myApi.create(data, "ApplicationRolePermission/InsertApplicationRolePermissions")
}

export function getFormPermission(id, userId) {

    return myApi.getByuserId(id, userId, 'ApplicationPermission/GetFormPermission')
}