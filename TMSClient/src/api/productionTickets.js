import myApi from '@/util/api'

export function getProductionTickets() {

    return myApi.getAll('ProductionTicket/GetProductionTickets')
  }
  export function getProductionTicketsByID(id) {
  
    return myApi.getByID(id, "ProductionTicket/GetProductionTicketsByID")
  }
 
export function getProductionTicket(data) {

  return myApi.getItem(data, "ProductionTicket/GetProductionTicket")
}
export function updateProductionTicket(data) {

  return myApi.update(data.productionTicketId, data, "ProductionTicket/UpdateProductionTicket")
}
  
export function createProductionTicket(data) {

    return myApi.create(data, "ProductionTicket/InsertProductionTicket")
  }
  export function deleteProductionTicket(data) {

    return myApi.delete(data.productionTicketId, 'ProductionTicket/DeleteProductionTicket')
}
export function getNavBaseUnitByID(id) {
  
    return myApi.getByID(id, "ProductionTicket/GetNavBaseUnitByID")
  }