import myApi from '@/util/api'

export function getDisposalItem() {

    return myApi.getAll('DisposalItem/GetDisposalItems')
}
export function getDisposalItemData(data) {

    return myApi.getItem(data, "DisposalItem/GetDisposalItemData")
}
export function createDisposalItem(data) {
    return myApi.create(data, "DisposalItem/InsertDisposalItem")
}


export function deleteDisposalItem(data) {

    return myApi.delete(data.disposalItemId, 'DisposalItem/DeleteDisposalItem')
}
export function getItemStockInfoByItemId (id) {
    return myApi.getByID(id, "ItemBatchInfo/GetItemStockInfoByItemId");
}
