import myApi from '@/util/api'

export function getWikiPage(pageCount,userId) {
    return myApi.getByPage(pageCount,userId, 'WikiPedia/GetWikiPage')
}

export function getInfo(PageID,userId) {
    return myApi.getByuserId(PageID,userId, 'WikiPedia/GetWikiInfo')
}


export function getPageDetail(PageID,userId) {
    return myApi.getByuserId(PageID,userId, 'WikiPedia/GetPageDetail')
}
export function getDuty(PageID) {
    return myApi.getByID(PageID, 'WikiPedia/GetDuty')
}

export function getSearchWiki(search,userId) {
    return myApi.getSearchResult(search,userId, 'WikiPedia/GetSearchWiki')
}

export function createComment(value) {
    return myApi.create(value, 'WikiPedia/InsertComment')
}

export function updateComment(data) {

    return myApi.update(data.commentID ,data, 'WikiPedia/UpdateComment')
}

export function deleteComment(id) {
    return myApi.delete(id, 'WikiPedia/DeleteComment')
}


export function getSeeAlso(PageID,userId) {
    return myApi.getByuserId(PageID,userId, 'WikiPedia/GetSeeAlso')
}

export function getAttach(sessionID,userId) {
    return myApi.getBySession(sessionID,userId, 'WikiPedia/GetAttachFile')
}


export function getPermissionPage(pageCount,userId) {
    return myApi.getByPage(pageCount,userId, 'WikiPedia/GetOnlyPermission')
}
//drop down function

export function GetWikiCategory() {
    return myApi.getAll('WikiPedia/GetWikiCategory')
}

export function GetWikiDepartment() {
    return myApi.getAll('WikiPedia/GetWikiDepartment')
}

export function GetPlant() {
    return myApi.getAll('WikiPedia/GetPlant')
}

export function GetLanguage() {
    return myApi.getAll('WikiPedia/GetLanguage')
}

export function GetWikiTag() {
    return myApi.getAll('WikiPedia/GetWikiTag')
}

export function GetTopic() {
    return myApi.getAll('WikiPedia/GetTopic')
}

export function GetDocumentType() {
    return myApi.getAll('WikiPedia/GetDocumentType')
}

export function GetTrainingMethod() {
    return myApi.getAll('WikiPedia/GetTrainingMethod')
}

export function GetStatus() {
    return myApi.getAll('WikiPedia/GetStatus')
}

export function GetTrainer() {
    return myApi.getAll('WikiPedia/GetTrainer')
}

export function GetOwner() {
    return myApi.getAll('WikiPedia/GetOwner')
}

export function GetPrimaryApp() {
    return myApi.getAll('WikiPedia/GetPrimaryApp')
}

export function GetNextApp() {
    return myApi.getAll('WikiPedia/GetNextApp')
}

export function GetSearchData(data) {
    return myApi.create(data, "WikiPedia/GetAdvSearch")
}








