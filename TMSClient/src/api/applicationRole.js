import myApi from '@/util/api'

export function getApplicationRoles() {

  return myApi.getAll('ApplicationRole/GetApplicationRoles')
}
export function getApplicationRole(data) {

  return myApi.getItem(data, "ApplicationRole/GetData")
}

export function createApplicationRole(data) {
  return myApi.create(data, "ApplicationRole/InsertApplicationRole")
}

export function updateApplicationRole(data) {

  return myApi.update(data.roleID, data, "ApplicationRole/UpdateApplicationRole")
}
export function deleteApplicationRole(data) {
  return myApi.delete(data.roleID, 'ApplicationRole/DeleteApplicationRole')
}