import myApi from '@/util/api'

export function getSalesOrders () {

    return myApi.getAll('SalesOrder/GetSalesOrders')
}
export function getSalesOrderData (data) {

    return myApi.getItem(data, "SalesOrder/GetSalesOrderData")
}

export function createSalesOrder (data) {
    return myApi.create(data, "SalesOrder/InsertSalesOrder")
}

export function updateSalesOrder (data) {

    return myApi.update(data.salesOrderId, data, "SalesOrder/UpdateSalesOrder")
}
export function deleteSalesOrder (data) {

    return myApi.delete(data.salesOrderId, 'SalesOrder/DeleteSalesOrder')
}
export function getSOCustomerSalesAddres () {

    return myApi.getAll('SobyCustomers/GetSOCustomerSalesAddres')
}


export function getSalesOrderProducts (id) {

    return myApi.getByID(id, 'SalesOrderProduct/GetSalesOrderProducts')
}
export function getSobyCustomersAddressById (id) {

    return myApi.getByID(id, 'SobyCustomers/GetSobyCustomersAddressById')
}
export function getSobyCustomersShippingAddress (id) {

    return myApi.getByID(id, 'SobyCustomers/GetSobyCustomersShippingAddress')
}
export function getSalesOrderByID (id) {

    return myApi.getByID(id, 'SalesOrder/GetSalesOrderByID')
}