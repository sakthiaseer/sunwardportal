import myApi from '@/util/api'

export function getManufacturingProcess() {

    return myApi.getAll('ManufacturingProcess/GetManufacturingProcess')
}
export function getDataManufacturingProcess(data) {

  return myApi.getItem(data, "ManufacturingProcess/GetData")
}
export function createManufacturingProcess(data) {
    return myApi.create(data, "ManufacturingProcess/InsertManufacturingProcess")
  }
  export function getManufacturingProcessByFinishProduct(id) {  
    return myApi.getByID(id, "ManufacturingProcess/GetManufacturingProcessByFinishProduct")
}
  export function isMasterExist(id){
    return myApi.getByID(id, "ManufacturingProcess/isMasterExist")
  }
  export function updateManufacturingProcess(data) {

    return myApi.update(data.manufacturingprocessId, data, "ManufacturingProcess/UpdateManufacturingProcess")
  }
  export function deleteManufacturingProcess(data) {
  
    return myApi.delete(data.manufacturingprocessId, 'ManufacturingProcess/DeleteManufacturingProcess')
  }


export function getManufacturingProcessLine (finishProductId) {
    return myApi.getAll('ManufacturingProcess/GetManufacturingProcessLine/?id='+finishProductId)
  }
export function createManufacturingProcessLine(data) {
    return myApi.create(data, "ManufacturingProcess/InsertManufacturingProcessLine")
  }
  export function updateManufacturingProcessLine(data) {

    return myApi.update(data.manufacturingProcessLineId, data, "ManufacturingProcess/UpdateManufacturingProcessLine")
  }
  export function deleteManufacturingProcessLine(data) {

    return myApi.delete(data.manufacturingProcessLineId, 'ManufacturingProcess/DeleteManufacturingProcessLine')
  }