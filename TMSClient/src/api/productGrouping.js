import myApi from '@/util/api'

export function getProductGroupings() {

    return myApi.getAll('ProductGrouping/GetProductGroupings')
}
export function getProductGroupingData(data) {

    return myApi.getItem(data, "ProductGrouping/GetData")
}
export function createProductGrouping(data) {
    return myApi.create(data, "ProductGrouping/InsertProductGrouping")
}

export function updateProductGrouping(data) {

    return myApi.update(data.productGroupingId, data, "ProductGrouping/UpdateProductGrouping")
}
export function deleteProductGrouping(data) {

    return myApi.delete(data.productGroupingId, 'ProductGrouping/DeleteProductGrouping')
}

export function getProductGroupingManufactures(id) {

    return myApi.getByID(id, 'ProductGroupingManufacture/GetProductGroupingManufactures')
}
export function getProductGroupingManufactureData(data) {

    return myApi.getItem(data, "ProductGroupingManufacture/GetProductGroupingManufactureData")
}
export function createProductGroupingManufacture(data) {
    return myApi.create(data, "ProductGroupingManufacture/InsertProductGroupingManufacture")
}

export function updateProductGroupingManufacture(data) {

    return myApi.update(data.productGroupingManufactureId, data, "ProductGroupingManufacture/UpdateProductGroupingManufacture")
}
export function deleteProductGroupingManufacture(data) {

    return myApi.delete(data.productGroupingManufactureId, 'ProductGroupingManufacture/DeleteProductGroupingManufacture')
}


export function getCompanyListingForProductGroup() {

    return myApi.getAll('CompanyListing/GetCompanyListingForProductGroup')
}
export function getCompanyListingForProductGroupingManufacture() {

    return myApi.getAll('CompanyListing/GetCompanyListingForProductGroupingManufacture')
}

export function getCompanyListingByIDs(ids) {

    return myApi.getItems(ids, 'CompanyListing/GetCompanyListingByIDs')
}


