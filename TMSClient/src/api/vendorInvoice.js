import myApi from '@/util/api'

export function getVendorInvoice() {

    return myApi.getAll('VendorInvoice/GetVendorInvoice')
}
export function getVendorInvoice(data) {

    return myApi.getItem(data, "VendorInvoice/GetData")
}

export function createVendorInvoice(data) {
    return myApi.create(data, "VendorInvoice/InsertVendorInvoice")
}

export function updateVendorInvoice(data) {

    return myApi.update(data.invoiceID, data, "VendorInvoice/UpdateVendorInvoice")
}
export function deleteVendorInvoice(data) {

    return myApi.delete(data.invoiceID, 'VendorInvoice/DeleteVendorInvoice')
}
