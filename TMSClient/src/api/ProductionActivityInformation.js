import myApi from '@/util/api'

export function getProductionActivityInformation() {

    return myApi.getAll('ProductionActivity/GetProductionActivity')
}
export function getSearchData(data) {

  return myApi.getItem(data, "ProductionActivity/GetData")
}
export function createProductionActivityInformation(data) {
    return myApi.create(data, "ProductionActivity/InsertProductionActivity")
  }
  export function updateProductionActivityInformation(data) {

    return myApi.update(data.productionActivityId, data, "ProductionActivity/UpdateProductionActivity")
  }
  export function deleteProductionActivityInformation(data) {
  
    return myApi.delete(data.productionActivityId, 'ProductionActivity/DeleteProductionActivity')
  }
  export function getFileProfileTypeMobileItems() {

    return myApi.getAll('FileProfileType/GetFileProfileTypeListItems')
}
export function getFileProfileTypeByDocumentList(data) {
  return myApi.getByID(data, "FileProfileType/GetFileProfileTypeByDocumentList")
}
export function getProductionActivitiesRefByNo(data) {
  return myApi.getItems(data, 'ProductionColor/GetProductionActivitiesRefByNo')
}