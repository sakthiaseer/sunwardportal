import myApi from '@/util/api'

export function getSalesTargetByStaffLine(id) {

    return myApi.getByID(id,'SalesTargetByStaff/GetSalesTargetByStaffLine')
}
export function createSalesTargetByStaffLine(data) {
    return myApi.create(data, "SalesTargetByStaff/InsertSalesTargetByStaffLine")
}

export function updateSalesTargetByStaffLine(data) {

    return myApi.update(data.salesTargetByStaffLineId, data, "SalesTargetByStaff/UpdateSalesTargetByStaffLine")
}
export function deleteSalesTargetByStaffLine(data) {

    return myApi.delete(data.salesTargetByStaffLineId, 'SalesTargetByStaff/DeleteSalesTargetByStaffLine')
}