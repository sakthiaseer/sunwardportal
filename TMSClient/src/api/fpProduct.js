import myApi from '@/util/api'

export function getFPProducts () {
  return myApi.getAll('FPProduct/GetFpproducts')
}
export function getFpproductLines (id) {
  return myApi.getByID(id, 'FPProductLine/GetFpproductLines')
}
export function getFPProductNavisionLines () {
  return myApi.getAll('FPProductNavisionLine/GetFPProductNavisionLines')
}
export function getFPProduct (data) {

  return myApi.getItem(data, "FPProduct/GetData")
}
export function createFPProduct (data) {
  return myApi.create(data, "FPProduct/InsertFpproduct")
}
export function updateFPProduct (data) {

  console.log(data);
  return myApi.update(data.fpProductID, data, "FPProduct/UpdateFpproduct")
}
export function deleteFPProduct (data) {

  return myApi.delete(data.fpProductID, 'FPProduct/DeleteFpproduct')
}

export function createFPProductNavisionLine (data) {
  return myApi.create(data, "FPProductNavisionLine/InsertFPProductNavisionLine")
}
export function updateFPProductNavisionLine (data) {

  console.log(data);
  return myApi.update(data.fpProductNavisionLineID, data, "FPProductNavisionLine/UpdateFPProductNavisionLine")
}
export function deleteFPProductNavisionLine (data) {

  return myApi.delete(data.fpProductNavisionLineID, 'FPProductNavisionLine/DeleteFPProductNavisionLine')
}