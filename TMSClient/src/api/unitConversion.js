import myApi from '@/util/api'

export function getApplicationMasterDetailByType() {

    return myApi.getAll('UnitConversion/GetApplicationMasterDetailByType')
}

export function getUnitConversions() {

    return myApi.getAll('UnitConversion/GetUnitConversion')
}

export function getUnitConversion(data) {

    return myApi.getItem(data, "UnitConversion/GetData")
}

export function createUnitConversion(data) {
    return myApi.create(data, "UnitConversion/InsertUnitConversion")
}

export function updateUnitConversion(data) {

    return myApi.update(data.unitConversionID, data, "UnitConversion/UpdateUnitConversion")
}
export function deleteUnitConversion(data) {

    return myApi.delete(data.unitConversionID, 'UnitConversion/DeleteUnitConversion')
}