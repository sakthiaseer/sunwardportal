import myApi from '@/util/api'

export function getStates() {

    return myApi.getAll('State/GetStates')
}
export function getCountries() {

    return myApi.getAll('Country/GetCountries')
}
export function getState(data) {

    return myApi.getItem(data, "State/GetData")
}

export function createState(data) {
    return myApi.create(data, "State/InsertState")
}

export function updateState(data) {

    return myApi.update(data.stateID, data, "State/UpdateState")
}
export function deleteState(data) {

    return myApi.delete(data.stateID, 'State/DeleteState')
}