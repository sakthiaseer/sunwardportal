import myApi from '@/util/api'

export function getCompanyCalendar (date) {

    return myApi.getAll('CompanyCalendar/GetCompanyCalendar?calenderDate=' + date)
}
export function getCompanyCalendarItems () {

    return myApi.getAll("CompanyCalendar/GetCompanyCalendarItems")
}

export function getCompanyCalendarAssistance (data) {

    return myApi.getItem(data,"CompanyCalendar/GetCompanyCalendarAssistance")
}


export function getCompanyCalendarEvents (id) {

    return myApi.getByID(id, "CompanyCalendar/GetCompanyCalendarEvents")
}

export function getDataList (data) {

    return myApi.getItem(data, "CompanyCalendar/GetData")
}
export function createCompanyCalendar (data) {
    return myApi.create(data, "CompanyCalendar/InsertCompanyCalendar")
}

export function updateCompanyCalendar (data) {

    return myApi.update(data.companyCalendarId, data, "CompanyCalendar/UpdateCompanyCalendar")
}
export function deleteCompanyCalendar (data) {

    return myApi.delete(data.companyCalendarId, 'CompanyCalendar/DeleteCompanyCalendar')
}
export function getCompanyCalendarByEvent (startDate, endDate) {

    return myApi.getAll('CompanyCalendar/GetCompanyCalendarByEvent?startDate=' + startDate + '&&endDate=' + endDate)
}
export function getCompanyCalendarDetails () {

    return myApi.getAll('CompanyCalendarLine/GetCompanyCalendarDetails')
}


