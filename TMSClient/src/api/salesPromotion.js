import myApi from '@/util/api'

export function getSalesPromotions() {

    return myApi.getAll('SalesPromotion/GetSalesPromotion')
}
export function getSearchData(data) {

    return myApi.getItem(data, "SalesPromotion/GetSalesPromotionData")
}
export function createSalesPromotion(data) {
    return myApi.create(data, "SalesPromotion/InsertSalesPromotion")
}
export function updateSalesPromotion(data) {

    return myApi.update(data.salesPromotionId, data, "SalesPromotion/UpdateSalesPromotion")
}
export function deleteSalesPromotion(data) {

    return myApi.delete(data.salesPromotionId, 'SalesPromotion/DeleteSalesPromotion')
}


export function getSalesMainPromotionListsByID(id) {
    return myApi.getByID('SalesPromotion/GetSalesMainPromotionListsByID')
}
export function createSalesMainPromotionList(data) {
    return myApi.create(data, "SalesPromotion/InsertSalesMainPromotionList")
}
export function updateSalesMainPromotionList(data) {

    return myApi.update(data.salesMainPromotionListId, data, "SalesPromotion/UpdateSalesMainPromotionList")
}
export function deleteSalesMainPromotionList(data) {

    return myApi.delete(data.salesMainPromotionListId, 'SalesPromotion/DeleteSalesMainPromotionList')
}
export function getGenericCodesByStatus() {
    return myApi.getAll("GenericCodes/GetGenericCodesByStatus");
  }



