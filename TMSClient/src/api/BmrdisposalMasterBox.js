import myApi from '@/util/api'

export function getBmrdisposalMasterBoxs (screenID) {

    return myApi.getByRefNo(screenID, 'BmrdisposalMasterBox/GetBmrdisposalMasterBoxs')
}
export function getBmrdisposalMasterBox (data) {

    return myApi.getItem(data, "BmrdisposalMasterBox/GetData")
}

export function createBmrdisposalMasterBox (data) {
    return myApi.create(data, "BmrdisposalMasterBox/InsertBmrdisposalMasterBox")
}
export function updateBmrdisposalMasterBox (data) {

    return myApi.update(data.bmrdisposalMasterBoxId, data, "BmrdisposalMasterBox/UpdateBmrdisposalMasterBox")
}
export function deleteBmrdisposalMasterBox (data) {

    return myApi.delete(data.bmrdisposalMasterBoxId, 'BmrdisposalMasterBox/DeleteBmrdisposalMasterBox')
}
export function getBmrdisposalMasterBoxByRefNo (data) {
    return myApi.getItems(data, 'BmrdisposalMasterBox/GetBmrdisposalMasterBoxByRefByNo')
}
export function getICTMasterByLocation (data) {
    return myApi.getByID(data, 'ICTMaster/GetICTMasterByLocation')
}