import myApi from '@/util/api'

export function getIcmasterOperation() {

    return myApi.getAll('IcmasterOperation/GetIcmasterOperation')
}
export function getData(data) {

  return myApi.getItem(data, "IcmasterOperation/GetData")
}
export function createIcmasterOperation(data) {
    return myApi.create(data, "IcmasterOperation/InsertIcmasterOperation")
  }
  export function updateIcmasterOperation(data) {

    return myApi.update(data.icmasterOperationId, data, "IcmasterOperation/UpdateIcmasterOperation")
  }
  export function deleteIcmasterOperation(data) {
  
    return myApi.delete(data.icmasterOperationId, 'IcmasterOperation/DeleteIcmasterOperation')
  }
  export function getCommonProcessLineItems() {

    return myApi.getAll('CommonProcess/GetCommonProcessLineItems')
}
export function getIcmasterOperationByRefNo(data) {
  return myApi.getItems(data, 'IcmasterOperation/GetIcmasterOperationByRefNo')
}