import myApi from '@/util/api'

export function getInstructionTypes() {

    return myApi.getAll('InstructionType/GetInstructionTypes')
}
export function getInstructionType(data) {

    return myApi.getItem(data, "InstructionType/GetData")
}

export function createInstructionType(data) {
    return myApi.create(data, "InstructionType/InsertInstructionType")
}

export function updateInstructionType(data) {

    return myApi.update(data.instructionTypeID, data, "InstructionType/UpdateInstructionType")
}
export function deleteInstructionType(data) {

    return myApi.delete(data.instructionTypeID, 'InstructionType/DeleteInstructionType')
}
