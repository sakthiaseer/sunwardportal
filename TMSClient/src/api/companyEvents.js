import myApi from '@/util/api'

export function getCompanyEvents() {

    return myApi.getAll('CompanyEvents/GetCompanyEvents')
}
export function getCompanyEvent(data) {

    return myApi.getItem(data, "CompanyEvents/GetData")
}
export function getStates() {

    return myApi.getAll('State/GetStates')
}
export function getCountries() {

    return myApi.getAll('Country/GetCountries')
}
export function createCompanyEvents(data) {
    return myApi.create(data, "CompanyEvents/InsertCompanyEvents")
}

export function updateCompanyEvents(data) {

    return myApi.update(data.companyEventID, data, "CompanyEvents/UpdateCompanyEvents")
}
export function deleteCompanyEvents(data) {

    return myApi.delete(data.companyEventID, 'CompanyEvents/DeleteCompanyEvents')
}
// export function getHolidayDetails(id) {

//     return myApi.getByID(id, 'CompanyHolidayDetail/GetCompanyHolidayDetails')
// }

export function getusers() {

    return myApi.getAll('ApplicationUser/GetApplicationUsers')
}