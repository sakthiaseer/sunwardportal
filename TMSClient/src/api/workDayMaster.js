import myApi from '@/util/api'

export function getCompanyWorkDayMasters() {

    return myApi.getAll('CompanyWorkDayMaster/GetCompanyWorkDayMasters')
}
export function getCompanyWorkDayMaster(data) {

    return myApi.getItem(data, "CompanyWorkDayMaster/GetData")
}
export function getStates() {

    return myApi.getAll('State/GetStates')
}
export function getCountries() {

    return myApi.getAll('Country/GetCountries')
}
export function getShiftMasters() {

    return myApi.getAll('ShiftMaster/GetShiftMasters')
}
export function createCompanyWorkDayMaster(data) {
    return myApi.create(data, "CompanyWorkDayMaster/InsertCompanyWorkDayMaster")
}

export function updateCompanyWorkDayMaster(data) {

    return myApi.update(data.workDateID, data, "CompanyWorkDayMaster/UpdateCompanyWorkDayMaster")
}
export function deleteCompanyWorkDayMaster(data) {

    return myApi.delete(data.workDateID, 'CompanyWorkDayMaster/DeleteCompanyWorkDayMaster')
}