import myApi from '@/util/api'

export function getExcipientInformationLineByProcessByID(id) {

    return myApi.getByID(id,'ExcipientInformationLineByProcess/GetExcipientInformationLineByProcessByID')
}
export function getExcipientInformationLineByProcess(data) {

    return myApi.getItem(data, "ExcipientInformationLineByProcess/GetData")
}
export function createExcipientInformationLineByProcess(data) {
    return myApi.create(data, "ExcipientInformationLineByProcess/InsertExcipientInformationLineByProcess")
}

export function updateExcipientInformationLineByProcess(data) {

    return myApi.update(data.excipientInformationLineByProcessID, data, "ExcipientInformationLineByProcess/UpdateExcipientInformationLineByProcess")
}
export function deleteExcipientInformationLineByProcess(data) {

    return myApi.delete(data.excipientInformationLineByProcessID, 'ExcipientInformationLineByProcess/DeleteExcipientInformationLineByProcess')
}


