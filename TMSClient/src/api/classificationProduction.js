import myApi from '@/util/api'
export function getClassificationProductionsByRefNo (id) {

    return myApi.getByID(id, 'ClassificationProduction/GetClassificationProductionsByRefNo')
}
export function createClassificationProduction (data) {
    return myApi.create(data, "ClassificationProduction/InsertClassificationProduction")
}

export function updateClassificationProduction (data) {

    return myApi.update(data.classificationProductionId, data, "ClassificationProduction/UpdateClassificationProduction")
}
export function deleteClassificationProduction (data) {

    return myApi.delete(data.classificationProductionId, 'ClassificationProduction/DeleteClassificationProduction')
}
