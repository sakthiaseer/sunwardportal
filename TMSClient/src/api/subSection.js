import myApi from '@/util/api'

export function getSubSections() {

    return myApi.getAll('SubSection/GetSubSections')
}
export function getSubSection(data) {

    return myApi.getItem(data, "SubSection/GetData")
}
export function createSubSection(data) {
    return myApi.create(data, "SubSection/InsertSubSection")
}

export function updateSubSection(data) {

    return myApi.update(data.subSectionID, data, "SubSection/UpdateSubSection")
}
export function deleteSubSection(data) {

    return myApi.delete(data.subSectionID, 'SubSection/DeleteSubSection')
}
export function getSections() {

    return myApi.getAll('Section/GetSections')
}