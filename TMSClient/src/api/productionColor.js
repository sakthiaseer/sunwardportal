import myApi from '@/util/api'

export function getProductionColors() {

    return myApi.getAll('ProductionColor/GetProductionColors')
}
export function getProductionColor(data) {

    return myApi.getItem(data, "ProductionColor/GetData")
}

export function createProductionColor(data) {
    return myApi.create(data, "ProductionColor/InsertProductionColor")
}

export function updateProductionColor(data) {

    return myApi.update(data.productionColorID, data, "ProductionColor/UpdateProductionColor")
}
export function deleteProductionColor(data) {

    return myApi.delete(data.productionColorID, 'ProductionColor/DeleteProductionColor')
}

export function getProductionColorLinesByID(id) {

    return myApi.getByID(id, "ProductionColorLine/GetProductionColorLinesByID")
}

export function createProductionColorLine(data) {
    return myApi.create(data, "ProductionColorLine/InsertProductionColorLine")
}

export function updateProductionColorLine(data) {

    return myApi.update(data.productionColorLineID, data, "ProductionColorLine/UpdateProductionColorLine")
}
export function deleteProductionColorLine(data) {

    return myApi.delete(data.productionColorLineID, 'ProductionColorLine/DeleteProductionColorLine')
}
export function getProductionColorsByRefNo(data) {
    return myApi.getItems(data, 'ProductionColor/GetProductionColorsByRefNo')
}