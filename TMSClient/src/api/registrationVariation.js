import myApi from '@/util/api'

export function getRegistrationVariation() {

    return myApi.getAll('RegistrationVariation/GetRegistrationVariation')
}
export function getSearchData(data) {

  return myApi.getItem(data, "RegistrationVariation/GetData")
}
export function createRegistrationVariation(data) {
    return myApi.create(data, "RegistrationVariation/InsertRegistrationVariation")
  }
  export function updateRegistrationVariation(data) {

    return myApi.update(data.registrationVariationId, data, "RegistrationVariation/UpdateRegistrationVariation")
  }
  export function deleteRegistrationVariation(data) {
  
    return myApi.delete(data.registrationVariationId, 'RegistrationVariation/DeleteRegistrationVariation')
  }