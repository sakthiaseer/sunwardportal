import myApi from '@/util/api'

export function getTenderPeriodPricing () {

    return myApi.getAll('TenderPeriodPricing/GetTenderPeriodPricing')
}
export function getTenderPeriodPricingData (data) {

    return myApi.getItem(data, "TenderPeriodPricing/GetData")
}

export function createTenderPeriodPricing (data) {
    return myApi.create(data, "TenderPeriodPricing/InsertTenderPeriodPricing")
}

export function updateTenderPeriodPricing (data) {

    return myApi.update(data.tenderPeriodPricingId, data, "TenderPeriodPricing/UpdateTenderPeriodPricing")
}
export function deleteTenderPeriodPricing (data) {

    return myApi.delete(data.tenderPeriodPricingId, 'TenderPeriodPricing/DeleteTenderPeriodPricing')
}
export function getTenderPeriodPricingByContractId (id) {

    return myApi.getByID(id, 'TenderPeriodPricing/GetTenderPeriodPricingByContractId')
}