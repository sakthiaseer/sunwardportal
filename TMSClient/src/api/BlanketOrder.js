import myApi from '@/util/api'

export function getBlanketOrder() {

    return myApi.getAll('BlanketOrder/GetBlanketOrder')
}
export function getBlanketOrderData(data) {

    return myApi.getItem(data, "BlanketOrder/GetData")
}

export function createBlanketOrder(data) {
    return myApi.create(data, "BlanketOrder/InsertBlanketOrder")
}

export function updateBlanketOrder(data) {

    return myApi.update(data.blanketOrderId, data, "BlanketOrder/UpdateBlanketOrder")
}
export function deleteBlanketOrder(data) {

    return myApi.delete(data.blanketOrderId, 'BlanketOrder/DeleteBlanketOrder')
}
export function uploadBlanketOrderDocuments(data, sessionId) {

    return myApi.uploadFile(data, "BlanketOrder/uploadBlanketOrderDocuments?sessionId=" + sessionId)
}
export function getBlanketOrderDocument(sessionId) {

    return myApi.getAll("BlanketOrder/GetBlanketOrderDocument?sessionId=" + sessionId)
}
export function deleteBlanketOrderDocument(blanketOrderAttachmentId) {

    return myApi.delete(blanketOrderAttachmentId, 'BlanketOrder/DeleteBlanketOrderDocument')
}
export function downLoadBlanketOrderDocument(blanketOrderAttachmentId) {

    return myApi.downLoadDocument(blanketOrderAttachmentId, "BlanketOrder/DownLoadBlanketOrderDocument")
}
export function getContractDistributionCustomerBySalesId(id) {

    return myApi.getByID(id, 'BlanketOrder/GetContractDistributionCustomerBySalesId')
}
export function getTenderInformationById(id) {

    return myApi.getByID(id, 'TenderInformation/GetTenderInformationById')
}
export function getProductBySalesId(id) {

    return myApi.getByID(id, 'BlanketOrder/GetProductBySalesId')
}

export function getBlanketOrderBySalesId(id) {

    return myApi.getByID(id, 'BlanketOrder/GetBlanketOrderBySalesId')
}
export function getProductReferenceByProductId(id,userId) {

    return myApi.getByuserId(id,userId,"BlanketOrder/GetProductReferenceByProductId")
  }

  export function getTenderQty(data) {

    return myApi.getItem(data, "TenderInformation/GetTenderQty")
}