import myApi from '@/util/api'

export function getSellingPriceInformationsByID(id) {

    return myApi.getByID(id, 'SellingPriceInformation/GetSellingPriceInformationsByID')
}
export function getSellingPriceInformationData(data) {

    return myApi.getItem(data, "SellingPriceInformation/GetSellingPriceInformationData")
}
export function createSellingPriceInformation(data) {
    return myApi.create(data, "SellingPriceInformation/InsertSellingPriceInformation")
}

export function updateSellingPriceInformation(data) {

    return myApi.update(data.sellingPriceInformationID, data, "SellingPriceInformation/UpdateSellingPriceInformation")
}
export function deleteSellingPriceInformation(data) {

    return myApi.delete(data.sellingPriceInformationID, 'SellingPriceInformation/DeleteSellingPriceInformation')
}

export function getSellingPriceTiersByID(id) {

    return myApi.getByID(id, 'SellingPriceInformation/GetSellingPriceTiersByID')
}

export function createSellingPriceTiers(data) {
    return myApi.create(data, "SellingPriceInformation/InsertSellingPriceTiers")
}

export function updateSellingPriceTiers(data) {

    return myApi.update(data.sellingPricingTiersID, data, "SellingPriceInformation/UpdateSellingPriceTiers")
}
export function deleteSellingPriceTiers(data) {
    return myApi.delete(data.sellingPricingTiersID, 'SellingPriceInformation/DeleteSellingPriceTiers')
}
export function getNavItemByGenericID(id, companyId) {

    return myApi.getGenericCodeByCompanyID(id, companyId, 'NavItem/GetNavItemByGenericID')
}