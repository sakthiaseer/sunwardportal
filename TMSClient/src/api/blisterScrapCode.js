import myApi from '@/util/api'

export function getBlisterScrapCodes() {

    return myApi.getAll('BlisterScrapCode/GetBlisterScrapCodes')
}
export function getBlisterScrapCode(data) {

    return myApi.getItem(data, "BlisterScrapCode/GetData")
}

export function createBlisterScrapCode(data) {
    return myApi.create(data, "BlisterScrapCode/InsertBlisterScrapCode")
}

export function updateBlisterScrapCode(data) {

    return myApi.update(data.scrapCodeID, data, "BlisterScrapCode/UpdateBlisterScrapCode")
}
export function deleteBlisterScrapCode(data) {

    return myApi.delete(data.scrapCodeID, 'BlisterScrapCode/DeleteBlisterScrapCode')
}