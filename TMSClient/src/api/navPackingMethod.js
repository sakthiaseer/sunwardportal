import myApi from '@/util/api'

export function getNavPackingMethodLines(id) {

    return myApi.getByID(id, 'NavItemPackingMethod/GetNavPackingMethodLines')
}
export function createNavPackingMethodLine(data) {
    return myApi.create(data, "NavItemPackingMethod/InsertNavPackingMethodLine")
}

export function updateNavPackingMethodLine(data) {

    return myApi.update(data.packingMethodId, data, "NavItemPackingMethod/UpdateNavPackingMethodLine")
}
export function deleteNavPackingMethod(data) {

    return myApi.delete(data.packingMethodId, 'NavItemPackingMethod/DeleteNavPackingMethod')
}