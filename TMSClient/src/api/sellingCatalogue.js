import myApi from '@/util/api'

export function getSellingCatalogues () {

    return myApi.getAll('SellingCatalogue/GetSellingCatalogues')
}
export function getDataList (data) {

    return myApi.getItem(data, "SellingCatalogue/GetSellingCatalogueData")
}
export function createSellingCatalogue (data) {
    return myApi.create(data, "SellingCatalogue/InsertSellingCatalogue")
}

export function updateSellingCatalogue (data) {

    return myApi.update(data.sellingCatalogueID, data, "SellingCatalogue/UpdateSellingCatalogue")
}
export function deleteSellingCatalogue (data) {

    return myApi.delete(data.sellingCatalogueID, 'SellingCatalogue/DeleteSellingCatalogue')
}
export function updateSellingCatalogueVersion (data) {

    return myApi.update(data.sellingCatalogueID, data, "SellingCatalogue/UpdateSellingCatalogueVersion")
}
export function getSellingCatalogueReport (data) {

    return myApi.getItem(data, "SellingCatalogue/GetSellingCatalogueReprt")
}
export function createVersion (data) {

    return myApi.update(data.sellingCatalogueID, data, "SellingCatalogue/CreateVersion")
}
export function applyVersion (data) {

    return myApi.update(data.sellingCatalogueID, data, "SellingCatalogue/ApplyVersion")
}
export function checkExitDataVersion (data) {

    return myApi.update(data.sellingCatalogueID, data, "SellingCatalogue/CheckExitDataVersion")
}
export function tempVersion (data) {

    return myApi.update(data.sellingCatalogueID, data, "SellingCatalogue/TempVersion")
}
export function undoVersion (id) {

    return myApi.getByID(id, "SellingCatalogue/UndoVersion")
}
export function deleteVersion (id) {

    return myApi.delete(id, 'SellingCatalogue/DeleteVersion')
}