import myApi from '@/util/api'

export function getProductionFlavours() {

    return myApi.getAll('ProductionFlavour/GetProductionFlavours')
}
export function getProductionFlavour(data) {

    return myApi.getItem(data, "ProductionFlavour/GetData")
}

export function createProductionFlavour(data) {
    return myApi.create(data, "ProductionFlavour/InsertProductionFlavour")
}

export function updateProductionFlavour(data) {

    return myApi.update(data.productionFlavourID, data, "ProductionFlavour/UpdateProductionFlavour")
}
export function deleteProductionFlavour(data) {

    return myApi.delete(data.productionFlavourID, 'ProductionFlavour/DeleteProductionFlavour')
}

export function getProductionFlavourLinesByID(id) {

    return myApi.getByID(id, "ProductionFlavourLine/GetProductionFlavourLinesByID")
}

export function createProductionFlavourLine(data) {
    return myApi.create(data, "ProductionFlavourLine/InsertProductionFlavourLine")
}

export function updateProductionFlavourLine(data) {
    console.log(JSON.stringify(data));
    return myApi.update(data.productionFlavourLineID, data, "ProductionFlavourLine/UpdateProductionFlavourLine")
}
export function deleteProductionFlavourLine(data) {

    return myApi.delete(data.productionFlavourLineID, 'ProductionFlavourLine/DeleteProductionFlavourLine')
}
export function getProductionFlavoursByRefNo(data) {
    return myApi.getItems(data, 'ProductionFlavour/GetProductionFlavoursByRefNo')
}