import myApi from '@/util/api'

export function getSimulationForForecastPlanningsByID (id) {

    return myApi.getByID(id, 'SimulationForForecastPlanning/GetSimulationForForecastPlanningByID')
}
export function createSimulationForForecastPlanning (data) {
    return myApi.create(data, "SimulationForForecastPlanning/InsertSimulationForForecastPlanning")
}

export function updateSimulationForForecastPlanning (data) {

    return myApi.update(data.productionSimulationGroupingId, data, "SimulationForForecastPlanning/UpdateSimulationForForecastPlanning")
}
export function deleteSimulationForForecastPlanning (data) {

    return myApi.update(data.productionSimulationGroupingId, data,'SimulationForForecastPlanning/DeleteSimulationForForecastPlanning')
}
export function getNAVRecipes (id) {

    return myApi.getByID(id, 'Simulation/GetNAVRecipes')
}

export function getItemInformationByID (id) {

    return myApi.getByID(id, 'SimulationForForecastPlanning/GetItemInformation')
}
export function createItemInformation (data) {
    return myApi.create(data, "SimulationForForecastPlanning/InsertItemInformation")
}

export function updateItemInformation (data) {

    return myApi.update(data.itemInformationId, data, "SimulationForForecastPlanning/UpdateItemInformation")
}
export function deleteItemInformation (data) {

    return myApi.delete(data.itemInformationId, 'SimulationForForecastPlanning/DeleteItemInformation')
}

export function getProdOrderByID (id) {

    return myApi.getByID(id, 'SimulationForForecastPlanning/GetProdOrder')
}
export function createProdOrder (data) {
    return myApi.create(data, "SimulationForForecastPlanning/InsertProdOrder")
}

export function updateProdOrder (data) {

    return myApi.update(data.prodOrderId, data, "SimulationForForecastPlanning/UpdateProdOrder")
}
export function deleteProdOrder (data) {

    return myApi.delete(data.prodOrderId, 'SimulationForForecastPlanning/DeleteProdOrder')
}
export function getNAVRecipesByBatchSize(id) {
    return myApi.getAll("Simulation/GetNAVRecipesByBatchSize?id="+id);
  }

  export function getNavmethodCodeBatch (id) {

    return myApi.getByID(id, 'Simulation/GetNavmethodCodeBatch')
}