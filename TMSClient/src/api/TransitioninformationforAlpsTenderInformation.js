import myApi from '@/util/api'

export function getTenderInformation () {

    return myApi.getAll('TenderInformation/GetTenderInformation')
}
export function getTenderInformationData (data) {

    return myApi.getItem(data, "TenderInformation/GetData")
}

export function createTenderInformation (data) {
    return myApi.create(data, "TenderInformation/InsertTenderInformation")
}

export function updateTenderInformation (data) {

    return myApi.update(data.tenderInformationId, data, "TenderInformation/UpdateTenderInformation")
}
export function deleteTenderInformation (data) {

    return myApi.delete(data.tenderInformationId, 'TenderInformation/DeleteTenderInformation')
}
export function getPacketInformationLines (id) {

    return myApi.getByID(id, 'PacketInformationLine/GetPacketInformationLines')
}

export function createPacketInformationLine (data) {
    return myApi.create(data, "PacketInformationLine/InsertPacketInformationLine")
}

export function updatePacketInformationLine (data) {

    return myApi.update(data.packetInformationId, data, "PacketInformationLine/UpdatePacketInformationLine")
}
export function deletePacketInformation (data) {

    return myApi.delete(data.packetInformationId, 'PacketInformationLine/DeletePacketInformation')
}


export function getNovateInformationLines (id) {

    return myApi.getByID(id, 'NovateInformationLine/GetNovateInformationLines')
}

export function createNovateInformationLine (data) {
    return myApi.create(data, "NovateInformationLine/InsertNovateInformationLine")
}

export function updateNovateInformationLine (data) {

    return myApi.update(data.novateInformationLineId, data, "NovateInformationLine/UpdateNovateInformationLine")
}
export function deleteNovateInformationLine (data) {

    return myApi.delete(data.novateInformationLineId, 'NovateInformationLine/DeleteNovateInformation')
}


export function getExtensionInformationLines (id) {

    return myApi.getByID(id, 'ExtensionInformationLine/GetExtensionInformationLines')
}

export function createExtensionInformationLine (data) {
    return myApi.create(data, "ExtensionInformationLine/InsertExtensionInformationLine")
}

export function updateExtensionInformationLine (data) {

    return myApi.update(data.extensionInformationLineId, data, "ExtensionInformationLine/UpdateExtensionInformationLine")
}
export function deleteExtensionInformationLine (data) {

    return myApi.delete(data.extensionInformationLineId, 'ExtensionInformationLine/DeleteExtensionInformation')
}
