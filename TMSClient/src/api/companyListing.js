import myApi from '@/util/api'

export function getCompanyListings() {

    return myApi.getAll('CompanyListing/GetCompanyListings')
}
export function getCompanyListingForAlpsTenderInformation() {
    return myApi.getAll('CompanyListing/GetCompanyListingForAlpsTenderInformation')
}

export function getCompanyListing(data) {

    return myApi.getItem(data, "CompanyListing/GetData")
}
export function createCompanyListing(data) {
    return myApi.create(data, "CompanyListing/InsertCompanyListing")
}

export function updateCompanyListing(data) {

    return myApi.update(data.companyListingID, data, "CompanyListing/UpdateCompanyListing")
}
export function deleteCompanyListing(data) {

    return myApi.delete(data.companyListingID, 'CompanyListing/DeleteCompanyListing')
}
export function getCompanyListingsByID(id) {

    return myApi.getByID(id, 'CompanyListingLine/GetCompanyListingsByID')
}
export function getCompanyListingLine(data) {

    return myApi.getItem(data, "CompanyListingLine/GetData")
}
export function createCompanyListingLine(data) {
    return myApi.create(data, "CompanyListingLine/InsertCompanyListingLine")
}

export function updateCompanyListingLine(data) {

    return myApi.update(data.companyListingLineID, data, "CompanyListingLine/UpdateCompanyListingLine")
}
export function deleteCompanyListingLine(data) {

    return myApi.delete(data.companyListingLineID, 'CompanyListingLine/DeleteCompanyListingLine')
}
export function getCompanyListingCompanyType(type) {

    return myApi.getByType(type, 'CompanyListing/GetCompanyListingCompanyType')
}
export function getCompanyListingsByRefNo(id) {

    return myApi.getByID(id, 'CompanyListing/GetCompanyListingsByRefNo')
}
export function getCompanyListingCompanyTypeList() {

    return myApi.getAll('CompanyListing/GetCompanyListingCompanyTypeList')
}
export function getCompanyListingForBuyerDistributor() {

    return myApi.getAll('CompanyListing/GetCompanyListingForBuyerDistributor')
}
export function getCompanyReports(data) {

    return myApi.getItems(data, "CompanyListing/GetCompanyReports")
}

export function getCompanyListingForBlanketEntry() {

    return myApi.getAll('CompanyListing/GetCompanyListingForBlanketEntry')
}
export function getCompanyListingsForNonTransactions() {

    return myApi.getAll('CompanyListing/GetCompanyListingsForNonTransactions')
}

export function getCompanyListingForSampleRequestForm() {

    return myApi.getAll('CompanyListing/GetCompanyListingForSampleRequestForm')
}

export function getCustomerCompanyId(id) {

    return myApi.getByID(id, 'CompanyListing/GetCustomerCompanyId')
}

export function createCompanyListingByCustomer(data) {
    return myApi.create(data, "CompanyListing/InsertCompanyListingByCustomer")
}
export function updateCompanyListingByCustomer(data) {

    return myApi.update(data.companyListingID, data, "CompanyListing/UpdateCompanyListingByCustomer")
}
export function getSobyCustomersMasterAddress(RefNo) {

    return myApi.getAll( 'SobyCustomers/GetSobyCustomersMasterAddress?RefNo='+RefNo)
}
export function getCompanyListingForSWDistributor() {

    return myApi.getAll( 'CompanyListing/GetCompanyListingForSWDistributor')
}
