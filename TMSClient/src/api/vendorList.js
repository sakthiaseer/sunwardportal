import myApi from '@/util/api'

export function getVendorLists() {

    return myApi.getAll('VendorList/GetVendorList')
}
export function getVendorList(data) {

    return myApi.getItem(data, "VendorList/GetData")
}
export function getVendorByID(id) {

    return myApi.getByID(id, 'VendorList/GetVendorByID')
}
export function createVendorList(data) {
    return myApi.create(data, "VendorList/InsertVendorList")
}

export function updateVendorList(data) {

    return myApi.update(data.vendorsListID, data, "VendorList/UpdateVendorList")
}
export function deleteVendorList(data) {

    return myApi.delete(data.vendorsListID, 'VendorList/DeleteVendorList')
}
