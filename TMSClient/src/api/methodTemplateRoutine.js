import myApi from '@/util/api'

export function getMethodTemplateRoutine () {

    return myApi.getAll('MethodTemplateRoutine/GetMethodTemplateRoutine')
}
export function getData (data) {

    return myApi.getItem(data, "MethodTemplateRoutine/GetData")
}

export function createMethodTemplateRoutine (data) {
    return myApi.create(data, "MethodTemplateRoutine/InsertMethodTemplateRoutine")
}

export function updateMethodTemplateRoutine (data) {

    return myApi.update(data.methodTemplateRoutineId, data, "MethodTemplateRoutine/UpdateMethodTemplateRoutine")
}
export function deleteMethodTemplateRoutine (data) {

    return myApi.delete(data.methodTemplateRoutineId, 'MethodTemplateRoutine/DeleteMethodTemplateRoutine')
}


export function getMethodTemplateRoutineLine (methodTemplateRoutineId) {
    return myApi.getAll('MethodTemplateRoutineLine/GetMethodTemplateRoutineLine/?id=' + methodTemplateRoutineId)
}
export function createMethodTemplateRoutineLine (data) {
    return myApi.create(data, "MethodTemplateRoutineLine/InsertMethodTemplateRoutineLine")
}
export function updateMethodTemplateRoutineLine (data) {

    return myApi.update(data.methodTemplateRoutineLineId, data, "MethodTemplateRoutineLine/UpdateMethodTemplateRoutineLine")
}
export function deleteMethodTemplateRoutineLine (data) {

    return myApi.delete(data.methodTemplateRoutineLineId, 'MethodTemplateRoutineLine/DeleteMethodTemplateRoutineLine')
}