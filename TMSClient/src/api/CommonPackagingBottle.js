import myApi from '@/util/api'

export function getCommonPackagingBottle (refNo) {

  return myApi.getByRefNo(refNo,'CommonPackagingBottle/GetCommonPackagingBottle')
}
export function getData (data) {

  return myApi.getItem(data, "CommonPackagingBottle/GetData")
}
export function createCommonPackagingBottle (data) {
  return myApi.create(data, "CommonPackagingBottle/InsertCommonPackagingBottle")
}
export function updateCommonPackagingBottle (data) {

  return myApi.update(data.bottleSpecificationId, data, "CommonPackagingBottle/UpdateCommonPackagingBottle")
}
export function deleteCommonPackagingBottle (data) {

  return myApi.delete(data.bottleSpecificationId, 'CommonPackagingBottle/DeleteCommonPackagingBottle')
}
export function getCommonPackagingBottleByRefNo(data) {
  return myApi.getItems(data, 'CommonPackagingBottle/GetCommonPackagingBottleByRefNo')
}