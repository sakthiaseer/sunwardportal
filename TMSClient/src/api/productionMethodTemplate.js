import myApi from '@/util/api'



export function getProductionMethodTemplate(data) {

    return myApi.getItem(data, "ProductionMethodTemplate/GetData")
}
export function getProductionMethodTemplates(){
    return myApi.getAll("ProductionMethodTemplate/GetProductionMethodTemplates")
}
export function createProductionMethodTemplate(data) {
    return myApi.create(data, "ProductionMethodTemplate/InsertProductionMethodTemplate")
}

export function updateProductionMethodTemplate(data) {

    return myApi.update(data.productionMethodTemplateID, data, "ProductionMethodTemplate/UpdateProductionMethodTemplate")
}
export function deleteProductionMethodTemplate(data) {

    return myApi.delete(data.productionMethodTemplateID, 'ProductionMethodTemplate/DeleteProductionMethodTemplate')
}

