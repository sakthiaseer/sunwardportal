import myApi from '@/util/api'



export function getPackagingPvcFoil(data) {

    return myApi.getItem(data, "PackagingPvcFoil/GetData")
}
export function getPackagingPvcFoils(){
    return myApi.getAll("PackagingPvcFoil/GetPackagingPvcFoils")
}
export function createPackagingPvcFoil(data) {
    return myApi.create(data, "PackagingPvcFoil/InsertPackagingPvcFoil")
}

export function updatePackagingPvcFoil(data) {

    return myApi.update(data.pvcFoilId, data, "PackagingPvcFoil/UpdatePackagingPvcFoil")
}
export function deletePackagingPvcFoil(data) {

    return myApi.delete(data.pvcFoilId, 'PackagingPvcFoil/DeletePackagingPvcFoil')
}
export function getPackagingPvcFoilsByRefNo(data) {
    return myApi.getItems(data, 'PackagingPvcFoil/GetPackagingPvcFoilsByRefNo')
  }

