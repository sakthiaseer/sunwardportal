import myApi from '@/util/api'

export function getCalibrationVendorInformations() {

    return myApi.getAll('CalibrationVendorInformation/GetCalibrationVendorInformations')
}
export function getCalibrationVendorInformation(data) {

    return myApi.getItem(data, "CalibrationVendorInformation/GetData")
}

export function createCalibrationVendorInformation(data) {
    return myApi.create(data, "CalibrationVendorInformation/InsertCalibrationVendorInformation")
}

export function updateCalibrationVendorInformation(data) {

    return myApi.update(data.calibrationVendorInformationID, data, "CalibrationVendorInformation/UpdateCalibrationVendorInformation")
}
export function deleteCalibrationVendorInformation(data) {

    return myApi.delete(data.calibrationVendorInformationID, 'CalibrationVendorInformation/DeleteCalibrationVendorInformation')
}
