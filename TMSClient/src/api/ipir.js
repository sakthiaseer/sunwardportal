import myApi from '@/util/api'

export function getIPIRItems() {

    return myApi.getAll('IPIR/GetIPIRItems')
}
export function getIPIRData(data) {

    return myApi.getItem(data, "IPIR/GetIPIRData")
}

export function createIPIR(data) {
    return myApi.create(data, "IPIR/InsertIPIR")
}

export function updateIPIR(data) {

    return myApi.update(data.IPIRId, data, "IPIR/UpdateIPIR")
}
export function deleteIPIR(data) {

    return myApi.delete(data.IPIRId, 'IPIR/DeleteIPIR')
}

export function getDescriptionFPandPRItems(id) {

    return myApi.getByID(id, 'NavItem/GetDescriptionFPandPRItems')
}
export function getIPIRLinesByID(id) {

    return myApi.getByID(id, "IPIR/GetIPIRLinesByID")
}

export function createIPIRLine(data) {
    return myApi.create(data, "IPIR/InsertIPIRLine")
}

export function updateIPIRLine(data) {

    return myApi.update(data.ipirlineId, data, "IPIR/UpdateIPIRLine")
}
export function deleteIPIRLine(data) {

    return myApi.delete(data.ipirlineId, 'IPIR/DeleteIPIRLine')
}
export function downLoadIPIRLineDocument(data) {

    return myApi.downLoadDocument(data, "IPIR/DownLoadIPIRLineDocument")
}
export function uploadIPIRLineDocuments(data, sessionId) {

    return myApi.uploadFile(data, "IPIR/UploadIPIRLineDocuments?sessionId=" + sessionId)
}
export function getCommonFieldsProductionMachineAll() {

    return myApi.getAll('commonFieldsProductionMachine/GetCommonFieldsProductionMachineAll')
}

export function getIPIRLineDocument(sessionId) {

    return myApi.getAll("IPIR/GetIPIRLineDocument?sessionId=" + sessionId)
}
export function deleteDocuments(data) {

    return myApi.delete(data.ipirlineDocumentId, 'IPIR/DeleteDocuments')
}

export function getNavItemBatchNo(id) {

    return myApi.getByID(id, 'NavItemBatchNo/GetNavItemBatchNo')
}

export function getIPIRLineDocumentById(id) {

    return myApi.getByID(id, 'IPIR/GetIPIRLineDocumentById')
}