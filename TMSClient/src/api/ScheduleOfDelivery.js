import myApi from '@/util/api'

export function getScheduleOfDelivery () {

    return myApi.getAll('ScheduleOfDeliverys/GetScheduleOfDelivery')
}
export function createScheduleOfDelivery (data) {
    return myApi.create(data, "ScheduleOfDelivery/InsertScheduleOfDelivery")
}

export function updateScheduleOfDelivery (data) {

    return myApi.update(data.scheduleOfDeliveryId, data, "ScheduleOfDelivery/UpdateScheduleOfDelivery")
}
export function deleteScheduleOfDelivery (data) {

    return myApi.delete(data.scheduleOfDeliveryId, 'ScheduleOfDelivery/DeleteScheduleOfDelivery')
}
export function getScheduleOfDeliveryByRefNo (data) {
    return myApi.getItems(data, 'ScheduleOfDelivery/GetScheduleOfDeliverysByRefNo')
}
