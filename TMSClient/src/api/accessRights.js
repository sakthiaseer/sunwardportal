import myApi from '@/util/api'

export function getAccessRights() {

  return myApi.getAll('DocumentPermission/GetDocumentPermissions')
}
export function getAccessRight(data) {

  return myApi.getItem(data, "DocumentPermission/GetData")
}
export function getDocuments() {

  return myApi.getAll('Documents/GetDocuments')
}

export function createAccessRight(data) {
  return myApi.create(data, "DocumentPermission/InsertDocumentPermission")
}

export function updateAccessRight(data) {

  return myApi.update(data.documentPermissionID, data, "DocumentPermission/UpdateDocumentPermission")
}
export function deleteAccessRight(data) {

  return myApi.delete(data.documentPermissionID, 'DocumentPermission/DeleteDocumentPermission')
}