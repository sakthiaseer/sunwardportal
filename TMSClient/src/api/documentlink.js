import myApi from '@/util/api'



export function getDocumentLinkByDocumentId(id,wikiStatus) {

    return myApi.getAll("DocumentLink/GetDocumentLinkByDocumentId?id="+id+"&&wikiStatus="+wikiStatus)
}

export function createDocumentLink(data) {

    return myApi.create(data, "DocumentLink/InsertDocumentLink")
}
export function updateDocumentLink(data) {

    return myApi.update(data.documentLinkId, data, "DocumentLink/UpdateDocumentLink")
}
export function deleteDocumentLink(data) {

    return myApi.delete(data.documentLinkId, 'DocumentLink/DeleteDocumentLink')
}
export function getDocumentDirectory (data) {

    return myApi.getItem(data, "DocumentLink/GetDocumentDirectory")
}
export function uploadFromDocumentLink(data) {

    return myApi.uploadFile(data, "DocumentLink/UploadFromDocumentLink")
  }
  export function getParentDocumentsByLinkDocumentId(id) {

    return myApi.getByID(id, "DocumentLink/GetParentDocumentsByLinkDocumentId")
}
export function getFileProfileTypeInfoById(id) {

    return myApi.getByID(id, "FileProfileType/GetFileProfileTypeInfoById")
}