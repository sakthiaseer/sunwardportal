import myApi from '@/util/api'

export function getSalesItemPackingInfos() {

    return myApi.getAll('SalesItemPackingInfo/GetSalesItemPackingInfo')
}
export function getSalesItemPackingInfo(data) {

    return myApi.getItem(data, "SalesItemPackingInfo/GetData")
}

export function createSalesItemPackingInfo(data) {
    return myApi.create(data, "SalesItemPackingInfo/InsertSalesItemPackingInfo")
}

export function updateSalesItemPackingInfo(data) {

    return myApi.update(data.salesItemPackingInfoId, data, "SalesItemPackingInfo/UpdateSalesItemPackingInfo")
}
export function deleteSalesItemPackingInfo(data) {

    return myApi.delete(data.salesItemPackingInfoId, 'SalesItemPackingInfo/DeleteSalesItemPackingInfo')
}
export function generateSalesItemPackingInfos(data) {

    return myApi.getItem(data, "SalesItemPackingInfo/GenerateSalesItemPackingInfos")
}
export function getSalesItemPackingInfoItems(id,userId) {

    return myApi.getByuserId(id,userId,'SalesItemPackingInfo/GetSalesItemPackingInfoItems')
}
export function getSalesItemPackingInfoLinkProfileNo(type) {

    return myApi.getByType(type,'SalesItemPackingInfo/GetSalesItemPackingInfoLinkProfileNo')
}

