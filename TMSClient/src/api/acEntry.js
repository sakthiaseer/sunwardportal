import myApi from '@/util/api'

export function getACEntries() {

    return myApi.getAll('ACEntry/GetACEntries')
}
export function getACEntry(data) {

    return myApi.getItem(data, "ACEntry/GetData")
}
export function getACEntryesByID(id) {

    return myApi.getByID(id, 'ACEntry/GetACEntryByID')
}
export function createACEntry(data) {
    return myApi.create(data, "ACEntry/InsertACEntry")
}
export function copyACEntry(data) {
    return myApi.create(data, "ACEntry/CopyACEntry")
}
export function updateACEntry(data) {

    return myApi.update(data.acEntryId, data, "ACEntry/UpdateACEntry")
}
export function deleteACEntry(data) {

    return myApi.delete(data.acEntryId, 'ACEntry/DeleteACEntry')
}
export function getCustomers() {
    return myApi.getAll('NavCustomer/GetCustomers')
}
export function getItems() {

    return myApi.getAll('NavItem/GetItems')
}
export function getACEntryLinesByID(id) {

    return myApi.getByID(id, "ACEntry/GetACEntryLinesByID")
}
export function getACEntryLines() {

    return myApi.getAll("ACEntry/GetACEntryLines")
}
export function createACEntryLine(data) {
    return myApi.create(data, "ACEntry/InsertACEntryLine")
}

export function updateACEntryLine(data) {

    return myApi.update(data.aCEntryLineId, data, "ACEntry/UpdateACEntryLine")
}
export function deleteACEntryLine(data) {

    return myApi.delete(data.acEntryLineId, 'ACEntry/DeleteACEntryLine')
}