import myApi from '@/util/api'

export function getCityMasters() {

    return myApi.getAll('CityMaster/GetCityMasters')
}
export function getCityMaster(data) {

    return myApi.getItem(data, "CityMaster/GetData")
}
export function createCityMaster(data) {
    return myApi.create(data, "CityMaster/InsertCityMaster")
}

export function updateCityMaster(data) {

    return myApi.update(data.cityID, data, "CityMaster/UpdateCityMaster")
}
export function deleteCityMaster(data) {

    return myApi.delete(data.cityID, 'CityMaster/DeleteCityMaster')
}