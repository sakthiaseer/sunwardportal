import myApi from '@/util/api'

export function getProdBasicCycles () {

    return myApi.getAll('ProductionBasicCycle/GetProdBasicCycle')
}
export function getProdBasicCycle (data) {

    return myApi.getItem(data, "ProductionBasicCycle/GetData")
}
export function getProdRecipeCycleByID (id) {

    return myApi.getByID(id, 'ProductionBasicCycle/GetProdRecipeCycleByID')
}
export function createProductionCycle (data) {
    return myApi.create(data, "ProductionBasicCycle/InsertProductionCycle")
}

export function updateProductionCycle (data) {

    return myApi.update(data.prodCycleId, data, "ProductionBasicCycle/UpdateProductionCycle")
}
export function deleteProductionCycle (data) {

    return myApi.delete(data.prodCycleId, 'ProductionBasicCycle/DeleteProductionCycle')
}


export function createVersion (data) {

    return myApi.update(data.prodCycleId, data, "ProductionBasicCycle/CreateVersion")
}
export function applyVersion (data) {

    return myApi.update(data.prodCycleId, data, "ProductionBasicCycle/ApplyVersion")
}
export function tempVersion(data) {

    return myApi.update(data.prodCycleId, data, "ProductionBasicCycle/TempVersion")
}
export function undoVersion(id) {

    return myApi.getByID(id, "ProductionBasicCycle/UndoVersion")
}
export function deleteVersion(id) {

    return myApi.delete(id, 'ProductionBasicCycle/DeleteVersion')
}
export function createProdRecipeCycle (data) {
    return myApi.create(data, "ProductionBasicCycle/InsertProdRecipeCycle")
}

export function updateProdRecipeCycle (data) {

    return myApi.update(data.prodRecipeCycleId, data, "ProductionBasicCycle/UpdateProdRecipeCycle")
}
export function deleteProdRecipeCycle (data) {

    return myApi.delete(data.prodRecipeCycleId, 'ProductionBasicCycle/DeleteProdRecipeCycle')
}


//

export function getNavSaleCategory () {

    return myApi.getAll('NavSaleCategory/GetNavCategory')
}
//GetNAVMethodCodeBatchByID
export function getNavMethodCodes (id) {

    return myApi.getByID(id, 'ProductionBasicCycle/GetNavMethodCode')
}

export function getMonthsAndYears () {

    return myApi.getAll('ProductionBasicCycle/GetMonthsAndYears')
}


export function getNAVMethodCodeBatchByID (id) {

    return myApi.getByID(id, "ProductionBasicCycle/GetNAVMethodCodeBatchByID")
}


export function uploadPackagingItemHistoryDocuments (data, sessionId) {

    return myApi.uploadFile(data, "ProductionBasicCycle/UploadProductionBasicCycleLineDocuments?sessionId=" + sessionId)
}
export function getProductionBasicCycleLineDocuments (id) {

    return myApi.getByID(id, "ProductionBasicCycle/GetProductionBasicCycleLineDocuments")
}
export function deleteProductionBasicCycleLineDocuments (productRecipeDocumentId) {


    return myApi.delete(productRecipeDocumentId, 'ProductionBasicCycle/DeleteProductionBasicCycleLineDocuments')
}
export function downLoadProductionBasicCycleLineDocuments (data) {

    return myApi.downLoadDocument(data, "ProductionBasicCycle/DownLoadProductionBasicCycleLineDocuments")
}

export function getProductionBasicCycleLineDocumentsById (productRecipeDocumentId) {

    return myApi.getByID(productRecipeDocumentId, 'ProductionBasicCycle/GetProductionBasicCycleLineDocumentsById')
}
export function updateProductionCycleVersion (data) {

    return myApi.update(data.prodRecipeCycleId, data, "ProductionBasicCycle/UpdateProductionCycleVersion")
}




