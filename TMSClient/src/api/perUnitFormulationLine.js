import myApi from '@/util/api'

export function perUnitFormulationLineByID(id) {

    return myApi.getByID(id,'PerUnitFormulationLine/PerUnitFormulationLineByID')
}
export function getPerUnitFormulationLine(data) {

    return myApi.getItem(data, "PerUnitFormulationLine/GetData")
}
export function createPerUnitFormulationLine(data) {
    return myApi.create(data, "PerUnitFormulationLine/InsertPerUnitFormulationLine")
}

export function updatePerUnitFormulationLine(data) {

    return myApi.update(data.perUnitFormulationLineID, data, "PerUnitFormulationLine/UpdatePerUnitFormulationLine")
}
export function deletePerUnitFormulationLine(data) {

    return myApi.delete(data.perUnitFormulationLineID, 'PerUnitFormulationLine/DeletePerUnitFormulationLine')
}


