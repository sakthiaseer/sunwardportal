import myApi from '@/util/api'

export function getSourceLists() {

    return myApi.getAll('SourceList/GetSourceLists')
}
export function getSourceList(data) {

    return myApi.getItem(data, "SourceList/GetData")
}
export function getSourceByID(id) {

    return myApi.getByID(id, 'SourceList/GetSourceByID')
}
export function createSourceList(data) {
    return myApi.create(data, "SourceList/InsertSourceList")
}

export function updateSourceList(data) {

    return myApi.update(data.sourceListID, data, "SourceList/UpdateSourceList")
}
export function deleteSourceList(data) {

    return myApi.delete(data.sourceListID, 'SourceList/DeleteSourceList')
}
