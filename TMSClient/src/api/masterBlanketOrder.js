import myApi from '@/util/api'

export function getMasterBlanketOrders() {

    return myApi.getAll('MasterBlanketOrder/GetMasterBlanketOrders')
}
export function getMasterBlanketOrderData(data) {

    return myApi.getItem(data, "MasterBlanketOrder/GetMasterBlanketOrderData")
}
export function createMasterBlanketOrder(data) {
    return myApi.create(data, "MasterBlanketOrder/InsertMasterBlanketOrder")
}

export function updateMasterBlanketOrder(data) {

    return myApi.update(data.masterBlanketOrderId, data, "MasterBlanketOrder/UpdateMasterBlanketOrder")
}

export function updateMasterBlanketOrderVersion(data) {
    return myApi.update(data.masterBlanketOrderId, data, "MasterBlanketOrder/UpdateMasterBlanketOrderVersion")
}


export function deleteMasterBlanketOrder(data) {

    return myApi.delete(data.masterBlanketOrderId, 'MasterBlanketOrder/DeleteMasterBlanketOrder')
}

export function getMasterBlanketOrderLines(id) {

    return myApi.getByID(id, 'MasterBlanketOrderLine/GetMasterBlanketOrderLines')
}
export function getMasterBlanketOrderLineData(data) {

    return myApi.getItem(data, "MasterBlanketOrderLine/GetMasterBlanketOrderLineData")
}
export function createMasterBlanketOrderLine(data) {
    return myApi.create(data, "MasterBlanketOrderLine/InsertMasterBlanketOrderLine")
}

export function updateMasterBlanketOrderLine(data) {

    return myApi.update(data.masterBlanketOrderLineId, data, "MasterBlanketOrderLine/UpdateMasterBlanketOrderLine")
}
export function deleteMasterBlanketOrderLine(data) {

    return myApi.delete(data.masterBlanketOrderLineID, 'MasterBlanketOrderLine/DeleteMasterBlanketOrderLine')
}

export function getCompanyListingsForBlanketOrder() {

    return myApi.getAll('CompanyListing/GetCompanyListingsForBlanketOrder')
}
export function createVersion(data) {

    return myApi.update(data.masterBlanketOrderId, data, "MasterBlanketOrder/CreateVersion")
}
export function applyVersion(data) {

    return myApi.update(data.masterBlanketOrderId, data, "MasterBlanketOrder/ApplyVersion")
}
export function tempVersion(data) {

    return myApi.update(data.masterBlanketOrderId, data, "MasterBlanketOrder/TempVersion")
}
export function undoVersion(id) {

    return myApi.getByID(id, "MasterBlanketOrder/UndoVersion")
}
export function deleteVersion(id) {

    return myApi.delete(id, 'MasterBlanketOrder/DeleteVersion')
}