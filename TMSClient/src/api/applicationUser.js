import myApi from '@/util/api'

export function getApplicationUsers() {

  return myApi.getAll('ApplicationUser/GetApplicationUsers')
}
export function getApplicationUser(data) {

  return myApi.getItem(data, "ApplicationUser/GetData")
}

export function createApplicationUser(data) {
  return myApi.create(data, "ApplicationUser/InsertApplicationUser")
}

export function updateApplicationUser(data) {

  return myApi.update(data.userID, data, "ApplicationUser/UpdateApplicationUser")
}
export function deleteApplicationUser(data) {

  return myApi.delete(data.userID, 'ApplicationUser/DeleteApplicationUser')
}
export function getPlants() {

  return myApi.getAll('Plant/GetPlants')
}
export function getApplicationRoles() {

  return myApi.getAll('ApplicationRole/GetApplicationRoles')
}
export function getUserGroups() {

  return myApi.getAll('UserGroup/GetUserGroups')
}

export function getCompanies() {
  return myApi.getAll('ApplicationUser/GetCompanies')
}

// export function getDepartments() {
//   return myApi.getAll('ApplicationUser/GetDepartments')
// }
export function getICTMasterActive(id) {
  return myApi.getByID(id, "ICTMaster/GetICTMasterActive")
}
export function getDepartments() {

  return myApi.getAll('Department/GetDepartments')
}
export function getDepartmentsByStatus() {

  return myApi.getAll('Department/GetDepartmentsByStatus')
}
export function getDesignations() {

  return myApi.getAll('Designation/GetDesignations')
}
export function getSections() {

  return myApi.getAll('Section/GetSections')
}
export function getSubSections() {

  return myApi.getAll('SubSection/GetSubSections')
}
export function getCityMasters() {

  return myApi.getAll('CityMaster/GetCityMasters')
}
export function getLanguageMasters() {

  return myApi.getAll('LanguageMaster/GetLanguageMasters')
}
export function getCodeMasterByType(type) {
  return myApi.getByType(type, "ApplicationUser/GetCodeMasterByType")
}

export function postError(data) {
  return myApi.create(data, "ApplicationUser/PostError");
}
export function getDepartmentsByDivisionStatus (id) {

  return myApi.getByID(id, "Department/GetDepartmentsByDivisionStatus")
}
export function getDivisionsByCompany (id) {

  return myApi.getByID(id, 'Division/GetDivisionsByCompany')
}
export function getDepartmentByDivision (id) {

  return myApi.getByID(id, 'Department/GetDepartmentByDivision')
}

