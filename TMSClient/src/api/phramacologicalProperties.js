import myApi from '@/util/api'


export function getPhramacologicalProperties() {

    return myApi.getAll('PhramacologicalProperties/GetPhramacologicalProperties')
}
export function getDataPhramacologicalProperties(data) {

  return myApi.getItem(data, "PhramacologicalProperties/GetData")
}
export function createPhramacologicalProperties(data) {
    return myApi.create(data, "PhramacologicalProperties/InsertPhramacologicalProperties")
  }
  export function getPhramacologicalPropertiesByFinishProduct(id) {  
    return myApi.getByID(id, "PhramacologicalProperties/GetPhramacologicalPropertiesByFinishProduct")
}
  export function isMasterExist(id){
    return myApi.getByID(id, "PhramacologicalProperties/isMasterExist")
  }
  export function updatePharmacologicalProperties(data) {

    return myApi.update(data.pharmacologicalpropertiesId, data, "PhramacologicalProperties/UpdatePhramacologicalProperties")
  }
  export function deletePharmacologicalProperties(data) {
  
    return myApi.delete(data.pharmacologicalpropertiesId, 'PhramacologicalProperties/DeletePhramacologicalProperties')
  }