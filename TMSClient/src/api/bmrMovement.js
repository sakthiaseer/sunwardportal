import myApi from '@/util/api'

export function getBmrmovement() {

    return myApi.getAll('Bmrmovement/GetBmrmovement')
}
export function createBmrmovement(data) {
    return myApi.create(data, "Bmrmovement/InsertBmrmovement")
}
export function updateBmrmovement(data) {

    return myApi.update(data.bmrmovementId, data, "Bmrmovement/UpdateBmrmovement")
}
export function deleteBmrmovement(data) {

    return myApi.delete(data.bmrmovementId, 'Bmrmovement/DeleteBmrmovement')
}
export function getBmrmovementLine(id) {
    return myApi.getByID(id, 'Bmrmovement/GetBmrmovementLine')
}
export function deleteBmrmovementLine(data) {

    return myApi.delete(data.bmrmovementLineId, 'Bmrmovement/DeleteBmrmovementLine')
}