
import myApi from '@/util/api'
export function importuploadDocuments(data) {

    return myApi.uploadFile(data, "historicalDetails/ImportuploadDocuments");
}
export function getHistoricalDetailsGraph(data) {

    return myApi.getItem(data,"historicalDetails/GetHistoricalDetailsGraph");
}
export function getHistoricalDetailsRef() {

    return myApi.getAll('historicalDetails/GetHistoricalDetailsRef')
}