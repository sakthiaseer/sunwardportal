import myApi from '@/util/api'

export function getFolderDiscussions() {

    return myApi.getAll('FolderDiscussion/GetFolderDiscussions')
}
export function getFolderDiscussion(data) {

    return myApi.getItem(data, "FolderDiscussion/GetData")
}
export function createFolderDiscussion(data) {
    return myApi.create(data, "FolderDiscussion/InsertFolderDiscussion")
}

export function updateFolderDiscussion(data) {

    return myApi.update(data.discussionNotesID, data, "FolderDiscussion/UpdateFolderDiscussion")
}
export function deleteFolderDiscussion(data) {

    return myApi.delete(data.discussionNotesID, 'FolderDiscussion/DeleteFolderDiscussion')
}
export function getFolderDiscussionsByID(id, userId) {

    return myApi.getByuserId(id, userId, 'FolderDiscussion/GetFolderDiscussions')
}
export function updateReadFolderDiscussion(data) {
    return myApi.update(data.discussionNotesID, data, "FolderDiscussion/UpdateRead");
}
export function updateReadFileFolderDiscussion(data) {
    return myApi.update(data.discussionNotesID, data, "FolderDiscussion/UpdateDocRead");
}
export function getDocFolderDiscussion(id, userId) {

    return myApi.getByuserId(id, userId, 'FolderDiscussion/GetDocDiscussion')
}

export function getUpdateFolderAttachment(id) {

    return myApi.delete(id, 'FolderDiscussion/UpdateFolderAttachment')
}
