import myApi from '@/util/api'



export function getOperationSequence(data) {

    return myApi.getItem(data, "OperationSequence/GetData")
}
export function getOperationSequences(id){
    return myApi.getByID(id,"OperationSequence/GetOperationSequences")
}
export function createOperationSequence(data) {
    return myApi.create(data, "OperationSequence/InsertOperationSequence")
}

export function updateOperationSequence(data) {

    return myApi.update(data.operationSequenceId, data, "OperationSequence/UpdateOperationSequence")
}
export function deleteOperationSequence(data) {

    return myApi.delete(data.operationSequenceId, 'OperationSequence/DeleteOperationSequence')
}

