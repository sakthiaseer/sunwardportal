import myApi from '@/util/api'

export function createSampleRequestForm(data) {
    return myApi.create(data, "SampleRequestForm/InsertSampleRequestForm")
}
export function getRequestForPersonById(id) {
    return myApi.getByID(id, 'SampleRequestForm/GetRequestForPersonById')
}

export function getSampleRequestDataForm(data) {

    return myApi.getItem(data, "SampleRequestForm/GetData")
}
export function updateSampleRequestForm(data) {

    return myApi.update(data.sampleRequestFormId, data, "SampleRequestForm/UpdateSampleRequestForm")
}
export function deleteSampleRequestForm(data) {

    return myApi.delete(data.sampleRequestFormId, 'SampleRequestForm/DeleteSampleRequestForm')
}
export function getSampleRequestForm() {

    return myApi.getAll('SampleRequestForm/GetSampleRequestForm')
}
export function getEmployeePersonSampleRequestForm() {

    return myApi.getAll('Employee/GetEmployeePersonSampleRequestForm')
}
export function getNAVLocation(id) {

    return myApi.getByID(id,'NAVLocation/GetNAVLocation')
}