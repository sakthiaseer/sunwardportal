import myApi from '@/util/api'
export function getCommonFieldsProductionMachineLine(id) {

    return myApi.getByID(id, "CommonFieldsProductionMachineLine/GetCommonFieldsProductionMachineLine")
}
export function createCommonFieldsProductionMachineLine(data) {
    return myApi.create(data, "CommonFieldsProductionMachineLine/InsertCommonFieldsProductionMachineLine")
}

export function updateCommonFieldsProductionMachineLine(data) {

    return myApi.update(data.commonFieldsProductionMachineLineId, data, "CommonFieldsProductionMachineLine/UpdateCommonFieldsProductionMachineLine")
}
export function deleteCommonFieldsProductionMachineLine(data) {

    return myApi.delete(data.commonFieldsProductionMachineLineId, 'CommonFieldsProductionMachineLine/DeleteCommonFieldsProductionMachineLine')
}
export function uploadMachineDocuments(data, sessionId) {

    return myApi.uploadFile(data, "CommonFieldsProductionMachineLine/UploadMachineDocuments?sessionId=" + sessionId)
}
export function getMachineDocument(sessionId) {

    return myApi.getAll("CommonFieldsProductionMachineLine/GetMachineDocument?sessionId=" + sessionId)
}
export function deleteMachineDocument(machineDocumentId) {

    return myApi.delete(machineDocumentId, 'CommonFieldsProductionMachineLine/DeleteMachineDocument')
}
export function downloadMachineDocument(data) {

    return myApi.downLoadDocument(data, "CommonFieldsProductionMachineLine/DownLoadMachineDocument")
}