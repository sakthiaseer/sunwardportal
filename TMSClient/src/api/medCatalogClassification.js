import myApi from '@/util/api'

export function getMedCatalogClassification () {

    return myApi.getAll('MedCatalogClassification/GetMedCatalogClassification')
}
export function getMedCatalogClassificationByRefNo (id) {

    return myApi.getByID(id, 'MedCatalogClassification/GetMedCatalogClassificationByRefNo')
}
export function createMedCatalogClassification (data) {
    return myApi.create(data, "MedCatalogClassification/InsertMedCatalogClassification")
}

export function updateMedCatalogClassification (data) {

    return myApi.update(data.medCatalogId, data, "MedCatalogClassification/UpdateMedCatalogClassification")
}
export function deleteMedCatalogClassification (data) {

    return myApi.delete(data.medCatalogId, 'MedCatalogClassification/DeleteMedCatalogClassification')
}