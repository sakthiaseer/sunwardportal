import myApi from '@/util/api'
export function getACItems() {
    return myApi.getAll('ACItems/GetACItems')
}
export function getACItemByCompany(id) {
    return myApi.getByID(id, 'ACItems/GetACItembyCountry')
}
export function getNoACItems() {
    return myApi.getAll('ACItems/GetNoACItems')
}

export function createACItems(data) {
    return myApi.create(data, "ACItems/InsertACItems")
}

export function updateACItems(data) {

    return myApi.update(data.distACID, data, "ACItems/UpdateACItems")
}
export function addDistMaster(data) {

    return myApi.update(data.distACID, data, "ACItems/addDistMaster")
}

export function updateDistStockBalance(data) {
    return myApi.update(data.distACID, data, "ACItems/UpdateDistStockBalance")
}

export function deleteACItems(data) {

    return myApi.delete(data.distACID, 'ACItems/DeleteACItems')
}
export function getDDACItems() {
    return myApi.getAll('ACItems/GetDDACItems')
}

export function getDistStockBalanceItemsById(id) {
    return myApi.getByID(id, 'ACItems/GetDistStockBalanceItemsById')
}


export function saveDistStockBalance(data) {
    return myApi.create(data, "ACItems/SaveDistStockBalance")
}

export function deleteStockBalanceItem(data) {

    return myApi.delete(data.distStockBalanceId, 'ACItems/DeleteStockBalanceItem')
}
export function getItemNo(data) {

    return myApi.getItem(data, "ACItems/GetItemNo")
  }