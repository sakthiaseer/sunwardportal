import myApi from '@/util/api'

export function getSalesTargetByStaffs() {

    return myApi.getAll('SalesTargetByStaff/GetSalesTargetByStaff')
}
export function getSalesTargetData(data) {

    return myApi.getItem(data, "SalesTargetByStaff/GetData")
}
export function createSalesTargetByStaff(data) {
    return myApi.create(data, "SalesTargetByStaff/InsertSalesTargetByStaff")
}

export function updateSalesTargetByStaff(data) {

    return myApi.update(data.salesTargetByStaffId, data, "SalesTargetByStaff/UpdateSalesTargetByStaff")
}
export function deleteSalesTargetByStaff(data) {

    return myApi.delete(data.salesTargetByStaffId, 'SalesTargetByStaff/DeleteSalesTargetByStaff')
}
export function getEmployeesSalesByDropdown(id) {

    return myApi.getByID(id,'Employee/GetEmployeesByDepartent')
}

export function createVersion(data) {

    return myApi.update(data.salesTargetByStaffId, data, "SalesTargetByStaff/CreateVersion")
}
export function applyVersion(data) {

    return myApi.update(data.salesTargetByStaffId, data, "SalesTargetByStaff/ApplyVersion")
}
export function tempVersion(data) {

    return myApi.update(data.salesTargetByStaffId, data, "SalesTargetByStaff/TempVersion")
}
export function undoVersion(id) {

    return myApi.getByID(id, "SalesTargetByStaff/UndoVersion")
}
export function deleteVersion(id) {

    return myApi.delete(id, 'SalesTargetByStaff/DeleteVersion')
}