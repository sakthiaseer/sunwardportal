import myApi from "@/util/topicapi";

export function uploadTopicFiles(data) {

    return myApi.uploadFile(data, "topic/AddTopic");
}
export function updateSeqNoByForum(topicId, seqNo) {

    return myApi.getAll("topic/UpdateSeqNo?topicId=" + topicId + "&seqNo=" + seqNo);
}
export function syncEmployeeToForum() {
    var data = {}; data.id = 0;
    return myApi.getItem(data, "Participants/SyncAll");
}