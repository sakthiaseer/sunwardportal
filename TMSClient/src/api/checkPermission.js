export function isValidFunction (permissionCode, screenID, store) {
  var portalPermissions = store.state.auth.authMenu.portalPermissionModels;
  if (
    portalPermissions.filter(
      (s) => s.permissionCode == permissionCode && s.screenID == screenID
    ) != undefined
  ) {
    // console.log('test', portalPermissions.filter(s => s.permissionCode == permissionCode && s.screenID == screenID)[0]);
    return (
      portalPermissions.filter(
        (s) => s.permissionCode == permissionCode && s.screenID == screenID
      ).length > 0
    );
  } else {
    return false;
  }
}

export function isClassificationPermission (codeId,store) {
  var portalPermissions = store.state.auth.classificationPermission;
  if (portalPermissions.length == 0) {
    return false;
  }
  else {
    let list = portalPermissions.filter(o => o.applicationCodeId == codeId);
    return list.length > 0 ? true : false;
  }
}
