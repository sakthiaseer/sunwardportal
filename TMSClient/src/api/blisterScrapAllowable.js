import myApi from '@/util/api'

export function getBlisterAllowables() {

    return myApi.getAll('BlisterAllowable/GetBlisterAllowable')
}
export function getBlisterAllowable(data) {

    return myApi.getItem(data, "BlisterAllowable/GetData")
}

export function createBlisterAllowable(data) {
    return myApi.create(data, "BlisterAllowable/InsertBlisterAllowable")
}

export function updateBlisterAllowable(data) {

    return myApi.update(data.BlisterScrapId, data, "BlisterAllowable/UpdateBlisterAllowable")
}
export function deleteBlisterAllowable(data) {

    return myApi.delete(data.BlisterScrapId, 'BlisterAllowable/DeleteBlisterAllowable')
}
export function getBlisterScrapAllowable(id) {
    console.log(id);
    return myApi.getByID(id, 'BlisterAllowable/GetBlisterAllowable')
}