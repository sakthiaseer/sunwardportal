import myApi from '@/util/api'

export function getCommonProcess(id) {

    return myApi.getByID(id,'CommonProcess/GetCommonProcess')
}
export function getData(data) {

    return myApi.getItem(data, "CommonProcess/GetData")
}
export function createCommonProcess(data) {
    return myApi.create(data, "CommonProcess/InsertCommonProcess")
}

export function updateCommonProcess(data) {

    return myApi.update(data.commonProcessId, data, "CommonProcess/UpdateCommonProcess")
}
export function deleteCommonProcess(data) {

    return myApi.delete(data.commonProcessId, 'CommonProcess/DeleteCommonProcess')
}




