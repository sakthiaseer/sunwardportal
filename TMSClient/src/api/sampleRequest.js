import myApi from '@/util/api'

export function getSampleRequestForDoctor() {

    return myApi.getAll('SampleRequestForDoctor/GetSampleRequestForDoctor')
}
export function getSampleRequestForDoctorLine(data) {

    return myApi.getByID(data, "SampleRequestForDoctor/GetSampleRequestForDoctorLine")
}
export function getSampleRequestForDoctorData(data) {

    return myApi.getItem(data, "SampleRequestForDoctor/GetSampleRequestForDoctorData")
}
export function createSampleRequestForDoctor(data) {
    return myApi.create(data, "SampleRequestForDoctor/InsertSampleRequestForDoctor")
}

export function updateSampleRequestForDoctor(data) {

    return myApi.update(data.sampleRequestForDoctorID, data, "SampleRequestForDoctor/UpdateSampleRequestForDoctor")
}
export function deleteSampleRequestForDoctor(data) {

    return myApi.delete(data.sampleRequestForDoctorID, 'SampleRequestForDoctor/DeleteSampleRequestForDoctor')
}

  export function getSampleRequestForDoctorByItemId(id) {
    return myApi.getByID(id, "SampleRequestForDoctor/GetSampleRequestForDoctorByItemId");
  }

