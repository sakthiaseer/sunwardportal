import myApi from '@/util/api'

export function getApplicationMasterDetailByType(type) {
    return myApi.getByType(type, "ApplicationMaster/GetApplicationMasterDetailByType")
  }
  export function getApplicationMasterDetailByTypewithDesc(type) {
    return myApi.getByType(type, "ApplicationMaster/GetApplicationMasterDetailByTypewithDesc")
  }
  export function getApplicationMasters(data) {

    return myApi.getItem(data, "ApplicationMaster/GetData")
}
  export function getApplicationMaster() {
    return myApi.getAll("ApplicationMaster/GetApplicationMaster")
  }
  export function getApplicationMasterDetailById(id) {
    return myApi.getAll("ApplicationMaster/GetApplicationMasterDetailById/"+id)
  }
  export function updateApplicationMasterDetail(data)
  {
    return myApi.update(data.applicationMasterDetailId, data, "ApplicationMaster/UpdateApplicationMasterDetail")
  }
  export function createApplicationMaster(data) {
    return myApi.create(data, "ApplicationMaster/InsertApplicationMasterDetail")
}
export function deleteApplicationMaster(data) {

  return myApi.delete(data.applicationMasterDetailId, 'ApplicationMaster/DeleteApplicationMasterDetail')
}
export function getCodeMasterDetailByName(name) {
  return myApi.getByType(name, "ApplicationMaster/GetCodeMasterDetailByName")
}
export function updateCodeMaster(data)
  {
    return myApi.update(data.applicationMasterDetailId, data, "ApplicationMaster/UpdateCodeMaster")
  }
  export function createCodeMaster(data) {
    return myApi.create(data, "ApplicationMaster/InsertCodeMsater")
}
export function deleteCodeMaster(data) {

  return myApi.delete(data.applicationMasterDetailId, 'ApplicationMaster/DeleteCodeMaster')
}
export function getCodeMasterData(data) {

  return myApi.getItem(data, "ApplicationMaster/GetCodeMasterData")
}
export function createApplicationMasterDetailByCodeID(data) {
  return myApi.create(data, "ApplicationMaster/InsertApplicationMasterDetailByCodeID")
}