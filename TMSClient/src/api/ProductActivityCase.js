import myApi from '@/util/api'

export function getProductActivityCase() {

  return myApi.getAll('ProductActivityCase/GetProductActivityCase')
}
export function getSearchData(data) {

  return myApi.getItem(data, "ProductActivityCase/GetData")
}

export function createProductActivityCase(data) {
  return myApi.create(data, "ProductActivityCase/InsertProductActivityCase")
}
export function updateProductActivityCase(data) {

  return myApi.update(data.productActivityCaseId, data, "ProductActivityCase/UpdateProductActivityCase")
}
export function deleteProductActivityCase(data) {

  return myApi.delete(data.productActivityCaseId, 'ProductActivityCase/DeleteProductActivityCase')
}




export function getProductActivityCaseLine(id) {

    return myApi.getByID(id,'ProductActivityCase/GetProductActivityCaseLine')
  }
export function createProductActivityCaseLine(data) {
    return myApi.create(data, "ProductActivityCase/InsertProductActivityCaseLine")
  }
export function updateProductActivityCaseLine(data) {

    return myApi.update(data.productActivityCaseLineId, data, "ProductActivityCase/UpdateProductActivityCaseLine")
  }
  export function deleteProductActivityCaseLine(data) {
  
    return myApi.delete(data.productActivityCaseLineId, 'ProductActivityCase/DeleteProductActivityCaseLIne')
  }