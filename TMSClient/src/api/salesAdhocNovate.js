import myApi from '@/util/api'


export function getSalesOrderEntryItems() {

    return myApi.getAll('SalesOrderNovate/GetSalesOrderEntryItems')
}

export function getDescriptionExceptFPandPRItems() {

    return myApi.getAll('SalesOrderNovate/GetDescriptionExceptFPandPRItems')
}
export function getSobyCustomers() {

    return myApi.getAll('SalesOrderNovate/GetSobyCustomers')
}

export function getSalesOrderNovate() {

    return myApi.getAll('SalesOrderNovate/GetSalesOrderNovateItems')
}
export function getSalesOrderNovateData(data) {

    return myApi.getItem(data, "SalesOrderNovate/GetSalesAdhocNovateData")
}
export function createSalesOrderNovate(data) {
    return myApi.create(data, "SalesOrderNovate/InsertSalesAdhocNovate")
}
export function updateSalesOrderNovate(data) {

    return myApi.update(data.salesAdhocNovateId, data, "SalesOrderNovate/UpdateSalesAdhocNovate")
}
export function deleteSalesOrderNovate(data) {

    return myApi.delete(data.salesAdhocNovateId, 'SalesOrderNovate/DeleteSalesAdhocNovate')
}


export function getNavItems() {
    return myApi.getAll('NavItem/GetNavItems')
}



export function getApplicationMasterDetailById(id) {
    return myApi.getAll("SalesOrderNovate/GetApplicationMasterDetailById/" + id)
}


export function uploadSalesOrderNovateDocuments(data, sessionId) {
    return myApi.uploadFile(data, "SalesOrderNovate/UploadSalesAdhocNovateAttachments?sessionId=" + sessionId)
}
export function getSalesOrderNovateDocuments(id) {
    return myApi.getByID(id, "SalesOrderNovate/GetSalesAdhocNovateAttachment")
}

export function getSalesAdhocNovateAttachmentBySessionID(sessionId) {
    return myApi.getByID(sessionId, "SalesOrderNovate/GetSalesAdhocNovateAttachmentBySessionID")
}
export function deleteSalesOrderNovateDocuments(salesAdhocNovateAttachmentId) {

    return myApi.delete(salesAdhocNovateAttachmentId, 'SalesOrderNovate/DeleteSalesAdhocNovateAttachment')
}
export function downloadSalesOrderNovateDocuments(data) {

    return myApi.downLoadDocument(data, "SalesOrderNovate/DownLoadSalesAdhocNovateAttachment")
}
export function getSalesOrderEntryCustomerItems(id) {

    return myApi.getByID(id, "SalesOrderNovate/GetSalesOrderEntryCustomerItems")
}

export function getSalesOrderNovateItemsByContractId(id) {

    return myApi.getByID(id, "SalesOrderNovate/GetSalesOrderNovateItemsByContractId")
}
