import myApi from '@/util/api'


export function getProductionOutputs() {

  return myApi.getAll('ProductionOutput/GetProductionOutputs')
}

export function updateProductionOutput(data) {

  return myApi.update(data.productionOutputId, data, "ProductionOutput/UpdateProductionOutput")
}

export function getProductionOutputReports(data) {

  return myApi.getItem(data, "ProductionOutput/GetProductionOutputReports")
}

export function getProductionOutputFilter(data) {

  return myApi.getItem(data, "ProductionOutput/GetProductionOutputFilter")
}
export function getProductionOutputExcel(data) {

  return myApi.downLoadProfileDocument(data, 'ProductionOutput/GetProductionOutputExcel')
}
export function getProductionOutputReportExcel(data) {

  return myApi.downLoadProfileDocument(data, 'ProductionOutput/GetProductionOutputReportExcel')
}