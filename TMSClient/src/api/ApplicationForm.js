import myApi from '@/util/api'

export function getApplicationForm () {
  return myApi.getAll('ApplicationForm/GetApplicationForm')
}
export function getData (data) {

  return myApi.getItem(data, "ApplicationForm/GetData")
}
export function createApplicationForm (data) {
  return myApi.create(data, "ApplicationForm/InsertApplicationForm")
}
export function createApplicationAccess (data) {
  return myApi.create(data, "ApplicationForm/InsertApplicationAccess")
}
export function updateApplicationForm (data) {
  return myApi.update(data.formID, data, "ApplicationForm/UpdateApplicationForm")
}
export function deleteApplicationForm (data) {

  return myApi.delete(data.formID, 'ApplicationForm/DeleteApplicationForm')
}
export function getApplicationFormAccess (id) {
  return myApi.getByID(id, 'ApplicationForm/GetApplicationFormAccess')

}
export function updateApplicationFormAccess (data) {

  return myApi.update(data.itemClassificationAccessID, data, "ApplicationForm/UpdateApplicationFormAccess")
}
export function deleteApplicationFormAccess (data) {

  return myApi.update(data.itemClassificationAccessID, data, "ApplicationForm/DeleteApplicationFormAccess")
}
export function getApplicationFormSearch (id, userId) {

  return myApi.getByuserId(id, userId, "ApplicationForm/GetApplicationFormSearch")
}

export function createApplicationFormSearch (data) {
  return myApi.create(data, "ApplicationForm/InsertApplicationFormSearch")
}
export function getSearchDetail (id) {
  return myApi.getByID(id, "ApplicationFormSearch/GetSearchDetail")
}

export function getApplicationFormByScreenID (screenId) {

  return myApi.getByScreen(screenId, "ApplicationForm/GetApplicationFormByScreenID")
}

