import myApi from '@/util/api'

export function getApproveMasters() {

    return myApi.getAll('ApproveMaster/GetApproveMasters')
}

export function getApproveMastersByScreen(data) {

    return myApi.getByScreen(data, 'ApproveMaster/GetApproveMastersByScreen')
}

export function getApproveMaster(data) {

    return myApi.getItem(data, "ApproveMaster/GetData")
}

export function createApproveMaster(data) {
    return myApi.create(data, "ApproveMaster/InsertApproveMaster")
}

export function updateApproveMaster(data) {

    return myApi.update(data.approveMasterId, data, "ApproveMaster/UpdateApproveMaster")
}
export function deleteApproveMaster(data) {
    return myApi.delete(data.approveMasterId, 'ApproveMaster/DeleteApproveMaster')
}