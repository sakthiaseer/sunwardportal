import myApi from '@/util/api'

export function getApplicationForm() {
    return myApi.getAll('ApplicationFormFields/GetApplicationForm')
  }

export function getApplicationFormFields() {

    return myApi.getAll('ApplicationFormFields/GetApplicationFormFields')
}

export function getApplicationFormFieldSearch(id,userId) {

    return myApi.getByuserId(id,userId,"ApplicationFormFields/GetApplicationFormFieldSearch")
  }

export function getApplicationFormField(data) {

    console.log(data);
    return myApi.getItem(data, "ApplicationFormFields/GetData")
}

export function createApplicationFormField(data) {
    return myApi.create(data, "ApplicationFormFields/InsertApplicationFormField")
}

export function updateApplicationFormField(data) {

    return myApi.update(data.unitConversionID, data, "ApplicationFormFields/UpdateApplicationFormField")
}
export function deleteApplicationFormField(data) {

    return myApi.delete(data.unitConversionID, 'ApplicationFormFields/DeleteApplicationFormField')
}