import myApi from '@/util/api'

export function getOperationProcedure() {

    return myApi.getAll('OperationProcedure/GetOperationProcedure')
}
export function getData(data) {

  return myApi.getItem(data, "OperationProcedure/GetData")
}
export function createOperationProcedure(data) {
    return myApi.create(data, "OperationProcedure/InsertOperationProcedure")
  }
  export function getOperationProcedureByFinishProduct(id) {  
    return myApi.getByID(id, "OperationProcedure/GetOperationProcedureByFinishProduct")
}
  export function isMasterExist(id){
    return myApi.getByID(id, "OperationProcedure/isMasterExist")
  }
  export function updateOperationProcedure(data) {

    return myApi.update(data.operationProcedureId, data, "OperationProcedure/UpdateOperationProcedure")
  }
  export function deleteOperationProcedure(data) {
  
    return myApi.delete(data.operationProcedureId, 'OperationProcedure/DeleteOperationProcedure')
  }


export function getOperationProcedureLine (operationProcedureId) {
    return myApi.getAll('OperationProcedure/GetOperationProcedureLine/?id='+operationProcedureId)
  }
export function createOperationProcedureLine(data) {
    return myApi.create(data, "OperationProcedure/InsertOperationProcedureLine")
  }
  export function updateOperationProcedureLine(data) {

    return myApi.update(data.operationProcedureLineId, data, "OperationProcedure/UpdateOperationProcedureLine")
  }
  export function deleteOperationProcedureLine(data) {

    return myApi.delete(data.operationProcedureLineId, 'OperationProcedure/DeleteOperationProcedureLine')
  }