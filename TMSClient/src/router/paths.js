export default [
  {
    path: "*",
    meta: {
      public: true,
    },
    redirect: {
      path: "/404",
    },
  },
  {
    path: "/404",
    meta: {
      public: true,
    },
    name: "NotFound",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/NotFound.vue`
      ),
  },
  {
    path: "/403",
    meta: {
      public: true,
    },
    name: "AccessDenied",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/Deny.vue`
      ),
  },
  {
    path: "/500",
    meta: {
      public: true,
    },
    name: "ServerError",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/Error.vue`
      ),
  },
  {
    path: "/login",
    meta: {
      public: true,
    },
    name: "Login",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/UserAuth.vue`
      ),
  },
  {
    path: "/pageLoader",
    meta: {
      public: true,
    },
    name: "pageLoader",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/components/pageLoader.vue`
      ),
  },
  {
    path: "/login",
    meta: {
      public: true,
    },
    name: "SignOut",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/UserAuth.vue`
      ),
  },

  {
    path: "/",
    meta: {},
    name: "Root",
    redirect: {
      name: "Login",
    },
  },
  {
    path: "/Dashboard/Dashboard",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "Dashboard/Dashboard",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/Dashboard.vue`
      ),
  },

  {
    path: "/Dashboard/UserDashboard",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "Dashboard/UserDashboard",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/userdashboard.vue`
      ),
  },
  {
    path: "/Dashboard/DashboardPage",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "Dashboard/DashboardPage",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/DashboardPage.vue`
      ),
  },
  {
    path: "/Dashboard/EntryDashboard",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "MDashboard/EntryDashboard",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/mobileDashboard/productionEntryDashboard.vue`
      ),
  },
  {
    path: "/Dashboard/EntryDashboard",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "MDashboard/EntryDashboard",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/mobileDashboard/productionEntryDashboard.vue`
      ),
  },
  {
    path: "/Dashboard/ConsumptionDashboard",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "MDashboard/ConsumptionDashboard",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/mobileDashboard/consumptionEntryDashboard.vue`
      ),
  },

  {
    path: "/calendarview",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "calendarview",
    component: (a) =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/calendar/Calendarpage.vue`
      ),
  },
  {
    path: "/companyCalendar",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "companyCalendar",
    component: (a) =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/calendar/companyCalendar.vue`
      ),
  },
  {
    path: "/calendar/personalCalendar",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "calendar/personalCalendar",
    component: (a) =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/calendar/personalCalendar/personalCalendar.vue`
      ),
  },
  {
    path: "/process/processAvailability",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "process/processAvailability",
    component: (a) =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/Process/processAvailability.vue`
      ),
  },

  {
    path: "process/navManufacturingProcess",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "process/navManufacturingProcess",
    component: (a) =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/Process/navManufacturingProcess.vue`
      ),
  },
  {
    path: "/task",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "Task",
    component: (a) =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        // `@/components/task/TaskLayout.vue`
        `@/pages/task/taskview.vue`
        // `@/pages/task/sataskListview.vue`
      ),
  },
  {
    path: "/wikigroup/wiki",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "wikigroup/wiki",
    component: () => import(`@/pages/wiki/wikiListing.vue`),
  },
  {
    path: "/wikiPage",
    meta: {
      breadcrumb: false,
      requiresAuth: true,
    },
    name: "wikiPage",
    component: (a) => import(`@/pages/wiki/wikiPage.vue`),
  },

  {
    path: "/task",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "TaskDash",
    component: (a) =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        // `@/components/task/TaskLayout.vue`
        `@/pages/task/taskview.vue`
        // `@/pages/task/sataskListview.vue`
      ),
  },
  {
    path: "/tasktreeview",
    meta: {
      breadcrumb: false,
      requiresAuth: true,
    },
    name: "taskTreeView",
    component: (a) => import(`@/pages/task/taskformdetails.vue`),
  },

  {
    path: "/workOrderComment",
    meta: {
      breadcrumb: false,
      requiresAuth: true,
    },
    name: "workOrderComment",
    component: (a) => import(`@/pages/WorkOrder/workOrderComment.vue`),
  },

  {
    path: "documentsettings/documentrole",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "documentapp/DocumentRole",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/document/DocumentRole.vue`
      ),
  },
  {
    path: "documentsettings/documentpermission",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "documentapp/DocumentPermission",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/document/DocumentPermission.vue`
      ),
  },
  {
    path: "ictsettings/Company",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "ictsettings/Company",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/ict/company.vue`
      ),
  },
  {
    path: "ictsettings/LayoutPlanType",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "ictsettings/LayoutPlanType",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/ict/layoutPlantype.vue`
      ),
  },
  {
    path: "ictsettings/Site",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "ictsettings/Site",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/ict/site.vue`
      ),
  },
  {
    path: "ictsettings/zone",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "ictsettings/Zone",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/ict/zone.vue`
      ),
  },
  {
    path: "ictsettings/location",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "ictsettings/Location",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/ict/location.vue`
      ),
  },
  {
    path: "ictsettings/Area",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "ictsettings/Area",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/ict/area.vue`
      ),
  },
  {
    path: "ictsettings/SpecificArea",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "ictsettings/SpecificArea",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/ict/specificArea.vue`
      ),
  },
  {
    path: "tmsapp/BulkTag",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "tmsapp/BulkTag",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/task/bulkTag.vue`
      ),
  },
  {
    path: "tmsapp/assignTaskPermission",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "tmsapp/assignTaskPermission",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/task/assignTaskPermission.vue`
      ),
  },
  {
    path: "tmsapp/activeMessage",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "tmsapp/activeMessage",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/task/activeMessage.vue`
      ),
  },
  {
    path: "material/ProductionColorForm",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "material/ProductionColorForm",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/ProductionColorForm.vue`
      ),
  },
  {
    path: "machineItems/BlisterMouldSetInformation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "machineItems/BlisterMouldSetInformation",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" ra */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/blisterMouldSetInformation.vue`
      ),
  },
  {
    path: "machineItems/PunchMouldSetInformation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "machineItems/PunchMouldSetInformation",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" ra */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/punchMouldSetInformation.vue`
      ),
  },
  {
    path: "machineItems/CommonMasterMouldChangePart",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "machineItems/CommonMasterMouldChangePart",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" ra */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/CommonMasterMouldChangePart.vue`
      ),
  },

  {
    path: "material/ProductionCapsuleForm",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "material/ProductionCapsuleForm",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/ProductionCapsuleForm.vue`
      ),
  },
  {
    path: "material/ProductionFlavourForm",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "material/ProductionFlavourForm",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/ProductionFlavourForm.vue`
      ),
  },
  {
    path: "manpower/ProductionMachineForm",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "manpower/ProductionMachineForm",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/ProductionMachineForm.vue`
      ),
  },
  {
    path: "material/ProductionMaterialForm",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "material/ProductionMaterialForm",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/ProductionMaterialForm.vue`
      ),
  },
  {
    path: "itemclassificationmaster/ProductionColorForm",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "itemclassificationmaster/ProductionColorForm",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/ProductionColorForm.vue`
      ),
  },
  {
    path: "packaging/SetInformation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/SetInformation",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/SetInformation.vue`
      ),
  },
  {
    path: "packaging/CartonSpecification",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/CartonSpecification",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/CommonPackagingCarton.vue`
      ),
  },
  {
    path: "packaging/CommonPackagingItems",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/CommonPackagingItems",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/CommonPackagingItem.vue`
      ),
  },

  {
    path: "packaging/BottleCapSpecification",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/BottleCapSpecification",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/CommonPackagingBottleCap.vue`
      ),
  },
  {
    path: "packaging/CommonFieldsProductionMachine",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/CommonFieldsProductionMachine",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/CommonFieldsProductionMachine.vue`
      ),
  },
  {
    path: "packaging/BottleSpecification",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/BottleSpecification",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/CommonPackagingBottle.vue`
      ),
  },
  {
    path: "packaging/CapsealSpecification",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/CapsealSpecification",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/CommonPackagingCapseal.vue`
      ),
  },
  {
    path: "packaging/BottleInsertSpecification",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/BottleInsertSpecification",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/CommonPackagingBottleInsert.vue`
      ),
  },
  {
    path: "packaging/DividerSpecification",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/DividerSpecification",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/CommonPackagingDivider.vue`
      ),
  },
  {
    path: "packaging/LayerPadSpecification",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/LayerPadSpecification",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/CommonPackagingLayerPad.vue`
      ),
  },
  {
    path: "packaging/Nesting",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/Nesting",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/CommonPackagingNesting.vue`
      ),
  },
  {
    path: "packaging/ShrinkWrapSpecification",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/ShrinkWrapSpecification",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/CommonPackagingShrinkWrap.vue`
      ),
  },

  //
  {
    path: "Transport/ScheduleofDelivery",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "Transport/ScheduleofDelivery",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/ScheduelofDelevery.vue`
      ),
  },

  {
    path: "Transport/ClassificationTransport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "Transport/ClassificationTransport",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/TransportClassification.vue`
      ),
  },

  {
    path: "Transport/HandlingofDrugClassification",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "Transport/HandlingofDrugClassification",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/FPDrugClassification.vue`
      ),
  },

  {
    path: "Transport/HandlingofShipment",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "Transport/HandlingofShipment",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/HandlingOfShipment.vue`
      ),
  },

  //

  {
    path: "packaging/CompanyListing",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/CompanyListing",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/CompanyListing.vue`
      ),
  },
  {
    path: "packaging/CommonProcess",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/CommonProcess",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/CommonProcess.vue`
      ),
  },
  {
    path: "packaging/ICMasterOperation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/ICMasterOperation",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/ICMasterOperation.vue`
      ),
  },
  {
    path: "packaging/MethodTemplateRoutine",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/MethodTemplateRoutine",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/MethodTemplateRoutine.vue`
      ),
  },
  {
    path: "packaging/BMP",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/BMP",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/BMP.vue`
      ),
  },
  {
    path: "packaging/PackagingLabel",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/PackagingLabel",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/PackagingLabel.vue`
      ),
  },
  {
    path: "packaging/PackagingLabelForCost",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/PackagingLabelForCost",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/PackagingLabelForCost.vue`
      ),
  },

  {
    path: "packaging/PackagingLeafletForCost",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/PackagingLeafletForCost",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/PackagingLeafletForCost.vue`
      ),
  },
  {
    path: "packaging/PackagingTubeForCost",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/PackagingTubeForCost",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/PackagingTubeForCost.vue`
      ),
  },
  {
    path: "packaging/PackagingUnitBoxForCost",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/PackagingUnitBoxForCost",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/PackagingUnitBoxForCost.vue`
      ),
  },

  {
    path: "machineItems/CommonMasterMould",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "machineItems/CommonMasterMould",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/CommonMasterMould.vue`
      ),
  },
  {
    path: "packaging/PackagingLeaflet",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/PackagingLeaflet",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/PackagingLeaflet.vue`
      ),
  },

  {
    path: "packaging/PackagingTube",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/PackagingTube",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/PackagingTube.vue`
      ),
  },
  {
    path: "packaging/PackagingUnitBox",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/PackagingUnitBox",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/PackagingUnitBox.vue`
      ),
  },
  {
    path: "packaging/PackagingaluminiumFoil",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/PackagingaluminiumFoil",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/PackagingaluminiumFoil.vue`
      ),
  },

  {
    path: "packaging/PackagingPvcFoil",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packaging/PackagingPvcFoil",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/PackagingPvcFoil.vue`
      ),
  },
  {
    path: "manpower/operationSequence",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "manpower/operationSequence",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/operationSequence.vue`
      ),
  },
  {
    path: "manpower/manpowerInformation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "manpower/manpowerInformation",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/manpowerInformation.vue`
      ),
  },
  {
    path: "manpower/StandardManufacturingProcess",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "manpower/StandardManufacturingProcess",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/StandardManufacturingProcess.vue`
      ),
  },
  {
    path: "manpower/productionMethodTemplate",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "manpower/productionMethodTemplate",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/ProductionMethodTemplate.vue`
      ),
  },

  {
    path: "manpower/standardProcedure",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "manpower/standardProcedure",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/StandardProcedure.vue`
      ),
  },
  // {
  //   path: 'generalitemssettings/newdocument',
  //   meta: {
  //     breadcrumb: true
  //   },
  //   name: 'generalitemsapp/NewDocument',
  //   props: (route) => ({
  //     type: route.query.type
  //   }),
  //   component: () => import(
  //     /* webpackChunkName: "routes" */
  //     /* webpackMode: "lazy-once" */
  //     `@/pages/document/NewDocument.vue`
  //   )
  // },
  // {
  //   path: 'generalitemssettings/newfolder',
  //   meta: {
  //     breadcrumb: true
  //   },
  //   name: 'generalitemsapp/NewFolder',
  //   props: (route) => ({
  //     type: route.query.type
  //   }),
  //   component: () => import(
  //     /* webpackChunkName: "routes" */
  //     /* webpackMode: "lazy-once" */
  //     `@/pages/document/NewFolder.vue`
  //   )
  // },
  // {
  //   path: 'generalitemssettings/shortcut',
  //   meta: {
  //     breadcrumb: true
  //   },
  //   name: 'generalitemsapp/ShortCut',
  //   props: (route) => ({
  //     type: route.query.type
  //   }),
  //   component: () => import(
  //     /* webpackChunkName: "routes" */
  //     /* webpackMode: "lazy-once" */
  //     `@/pages/document/ShortCut.vue`
  //   )
  // },
  // {
  //   path: 'generalitemssettings/newcompounddoc',
  //   meta: {
  //     breadcrumb: true
  //   },
  //   name: 'generalitemsapp/NewCompoundDoc',
  //   props: (route) => ({
  //     type: route.query.type
  //   }),
  //   component: () => import(
  //     /* webpackChunkName: "routes" */
  //     /* webpackMode: "lazy-once" */
  //     `@/pages/document/NewCompoundDoc.vue`
  //   )
  // },

  /*  {
     path: "/DMS/publicfolder",
     meta: {
       breadcrumb: true,
       requiresAuth: true,
     },
     name: "DMS/publicfolder",
     component: (a) =>
       import( */
  /* webpackChunkName: "routes" */
  /* webpackMode: "lazy-once" */
  // `@/components/task/TaskLayout.vue`
  // `@/pages/nonlivepublicfolder/publicfolderview.vue`
  // `@/pages/task/sataskListview.vue`
  //  ),
  // },
  {
    path: "/DMS/publicfolder",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "DMS/publicfolder",
    component: (a) =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        // `@/components/task/TaskLayout.vue`
        `@/pages/nonlivepublicfolder/publicfolderviews.vue`
        // `@/pages/task/sataskListview.vue`
      ),
  },

  {
    path: "/DMS/portfolio",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "DMS/portfolio",
    component: (a) => import(`@/pages/portfolio/portfolio.vue`),
  },
  {
    path: "/publicfolder",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "PublicFolderDash",
    component: (a) =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        // `@/components/task/TaskLayout.vue`
        `@/pages/nonlivepublicfolder/publicfolderview.vue`
        // `@/pages/task/sataskListview.vue`
      ),
  },
  {
    path: "/publicfoldertreeview",
    meta: {
      breadcrumb: false,
      requiresAuth: true,
    },
    name: "publicfolderTreeView",
    component: (a) =>
      import(`@/pages/nonlivepublicfolder/publicfolderformdetails.vue`),
  },
  {
    path: "/plantmaintenanceview",
    meta: {
      breadcrumb: false,
      requiresAuth: true,
    },
    name: "plantmaintenanceview",
    component: (a) =>
      import(`@/pages/mobile/plantmaintenance/plantmaintenanceentry.vue`),
  },

  {
    path: "/production/ProductionOutput",
    meta: {
      breadcrumb: false,
      requiresAuth: true,
    },
    name: "production/ProductionOutput",
    component: (a) =>
      import(`@/pages/mobile/ProductionOutput/productionOutput.vue`),
  },
  {
    path: "/production/ProductionActivityInformation",
    meta: {
      breadcrumb: false,
      requiresAuth: true,
    },
    name: "production/ProductionActivityInformation",
    component: (a) =>
      import(
        `@/pages/productionplanning/ProductionActivityInformation/ProductionActivityInformation.vue`
      ),
  },
  {
    path: "production/startOfDay",
    meta: {
      breadcrumb: false,
      requiresAuth: true,
    },
    name: "production/startOfDay",
    component: (a) => import(`@/pages/mobile/StartOfDay/startOfDay.vue`),
  },
  {
    path: "/downloadFile",
    meta: {
      breadcrumb: false,
      requiresAuth: true,
    },
    name: "PublicFolderdownload",
    component: (a) => import(`@/pages/nonlivepublicfolder/downloadFile.vue`),
  },
  {
    path: "/fileProfileTypeDownload",
    meta: {
      breadcrumb: false,
      requiresAuth: true,
    },
    name: "FileProfileTypedownload",
    component: (a) =>
      import(`@/pages/fileProfileTypeView/fileProfileTypeDownload.vue`),
  },
  {
    path: "/Glossaryview",
    meta: {
      breadcrumb: false,
      requiresAuth: true,
    },
    name: "Glossaryview",
    component: (a) =>
      import(`@/pages/masters/ApplicationUser/Glossaryview.vue`),
  },

  // {
  //   path: 'dmssettings/publicfolder',
  //   meta: {
  //     breadcrumb: true
  //   },
  //   name: 'dmsapp/PublicFolder',
  //   props: (route) => ({
  //     type: route.query.type
  //   }),
  //   component: () => import(
  //     /* webpackChunkName: "routes" */
  //     /* webpackMode: "lazy-once" */
  //     `@/pages/document/PublicFolder.vue`
  //   )
  // },
  {
    path: "dmssettings/folderstorage",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "dmsapp/FolderStorage",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/FolderStorage.vue`
      ),
  },

  // {
  //   path: 'dmssettings/accessrights',
  //   meta: {
  //     breadcrumb: true
  //   },
  //   name: 'dmsapp/AccessRights',
  //   props: (route) => ({
  //     type: route.query.type
  //   }),
  //   component: () => import(
  //     /* webpackChunkName: "routes" */
  //     /* webpackMode: "lazy-once" */
  //     `@/pages/document/AccessRights.vue`
  //   )
  // },
  {
    path: "dmssettings/documentrole",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "dmsapp/DocumentRole",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/document/DocumentRole.vue`
      ),
  },
  {
    path: "dmssettings/documentpermission",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "dmsapp/DocumentPermission",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/document/DocumentPermission.vue`
      ),
  },
  {
    path: "dmssettings/inActiveFolder",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "dmsapp/inActiveFolder",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/nonlivepublicfolder/inActivePublicFolderview.vue`
      ),
  },
  {
    path: "plantmaintenance/plantmaintenance",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "plantmaintenance/PlantMaintenance",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/mobile/plantmaintenance/plantmaintenanceentrylist.vue`
      ),
  },

  {
    path: "/production/productionEntry",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "production/ProductionEntry",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/mobile/productionentry/productionentry.vue`
      ),
  },
  {
    path: "/production/bmrMovement",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "production/bmrMovement",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/mobile/bmrMovement/bmrMovement.vue`
      ),
  },
  {
    path: "/production/IPIRMobile",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "production/IPIRMobile",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/mobile/productionentry/IPIRMobile.vue`
      ),
  },

  {
    path: "/production/IssueReportIPIR",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "production/IssueReportIPIR",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/mobile/productionentry/IssueReportIPIR.vue`
      ),
  },
  {
    path: "mobilesetting/sunwardGroupCompany",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "mobile/sunwardGroupCompany",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/SunwardGroupCompany.vue`
      ),
  },
  {
    path: "/production/consumptionEntry",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "production/ConsumptionEntry",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/mobile/Consumption/ConsumptionEntry.vue`
      ),
  },
  {
    path: "production/dispensingEntry",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "production/DispensingEntry",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/mobile/Dispensing/DispensingEntry.vue`
      ),
  },
  {
    path: "production/dispenserDispensing",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "production/DispenserDispensing",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/mobile/Dispensing/DispenserDispensing.vue`
      ),
  },
  {
    path: "production/transferReclassEntry",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "production/TransferReclassEntry",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/mobile/TransferReclass/TransferReclassEntry.vue`
      ),
  },
  {
    path: "production/materialReturn",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "production/MaterialReturn",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/mobile/MaterialReturn/MaterialReturn.vue`
      ),
  },
  {
    path: "/applicationsetting/applicationRole",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "application/ApplicationRole",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/application/ApplicationRole.vue`
      ),
  },
  {
    path: "/applicationsetting/applicationmastersetting",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "application/ApplicationMasterSettings",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/application/ApplicationMasterSettings.vue`
      ),
  },
  {
    path: "/applicationsetting/applicationRolePermission",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "application/ApplicationRolePermission",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/application/ApplicationFormFields.vue`
      ),
  },
  {
    path: "/applicationsetting/applicationUser",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "application/ApplicationUser",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/application/ApplicationUser.vue`
      ),
  },
  {
    path: "/applicationsetting/ApplicationMasterParent",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "application/ApplicationMasterParent",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/application/ApplicationMasters/ApplicationMasterParent.vue`
      ),
  },
  {
    path: "/applicationsetting/ApplicationMasterChild",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "application/ApplicationMasterChild",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/application/ApplicationMasters/ApplicationMasterChild.vue`
      ),
  },
  {
    path: "/hr/employee",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "hr/Employee",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/application/Employee.vue`
      ),
  },
  {
    path: "hr/hrExternalPersonal",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "hr/hrExternalPersonal",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/application/HrExternalPersonal.vue`
      ),
  },
  {
    path: "/hr/portalLinks",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "hr/portalLinks",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/application/portalLinks.vue`
      ),
  },
  {
    path: "hr/resetPassword",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "hr/resetPassword",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/application/resetPassword.vue`
      ),
  },

  {
    path: "transfersettings/transferSettings",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "transfersettings/transferSettings",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/ict/transferSettings.vue`
      ),
  },
  {
    path: "/profilesetting/documentprofile",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "profilesetting/DocumentProfile",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/DMS/DocumentProfile/documentProfileView.vue`
      ),
  },
  {
    path: "/profilesetting/documentnogeneration",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "profilesetting/DocumentNoGeneration",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/DMS/DocumentProfile/documentNoGeneration.vue`
      ),
  },

  {
    path: "/profilesetting/documentNoGenerationListView",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "profilesetting/documentNoGenerationListView",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/DMS/DocumentProfile/documentNoGenerationListView.vue`
      ),
  },
  {
    path: "profilesetting/voidNoSeries",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "profile/voidNoSeries",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/DMS/DocumentProfile/voidNoSeries.vue`
      ),
  },
  {
    path: "/itemclassmaster/itemclassmaster",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "itemclassmaster/itemclassmaster",
    component: (a) =>
      import(`@/pages/itemClassification/itemClassificationView.vue`),
  },
  {
    path: "/itemclassmaster/itemClassificationTree",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "itemclassmaster/itemClassificationTree",
    component: (a) =>
      import(`@/pages/itemClassification/itemClassificationDetails.vue`),
  },
  {
    path: "/QA/QABMRDisposalMasterForm",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "QA/QABMRDisposalMasterForm",
    component: (a) =>
      import(`@/pages/itemClassificationForm/BMRDisposalMasterBox.vue`),
  },
  {
    path: "/classification/itemClassificationForm",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "classification/itemClassificationForm",
    component: (a) =>
      import(`@/pages/itemClassificationForm/itemClassificationFormView.vue`),
  },
  {
    path: "/classification/MasterBOMForPacking",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "classification/MasterBOMForPacking",
    component: (a) =>
      import(`@/pages/itemClassificationForm/MasterBOMForPacking.vue`),
  },
  {
    path: "/classification/itemClassificationFormTree",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "classification/itemClassificationFormTree",
    component: (a) =>
      import(
        `@/pages/itemClassificationForm/itemClassificationFormDetails.vue`
      ),
  },
  {
    path: "/application/ApplicationForm",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "application/ApplicationForm",
    component: (a) =>
      import(
        `@/pages/itemClassificationForm/ApplicationForm/ApplicationFormDetails.vue`
      ),
  },
  {
    path: "/applicationsetting/user",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "application/User",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/application/User.vue`
      ),
  },
  // {
  //   path: 'dmssettings/audittrail',
  //   meta: {
  //     breadcrumb: true
  //   },
  //   name: 'dmsapp/AuditTrail',
  //   props: (route) => ({
  //     type: route.query.type
  //   }),
  //   component: () => import(
  //     /* webpackChunkName: "routes" */
  //     /* webpackMode: "lazy-once" */
  //     `@/pages/document/AuditTrail.vue`
  //   )
  // },
  {
    path: "tmsapp/project",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "tmsapp/Projects",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/Projects.vue`
      ),
  },
  {
    path: "tmsapp/team",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "tmsapp/Team",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/Team.vue`
      ),
  },
  {
    path: "tmsapp/tags",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "tmsapp/tags",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/Tag.vue`
      ),
  },
  {
    path: "tmsapp/bulkTransfer",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "tmsapp/BulkTransfer",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/task/bulkTransferTask.vue`
      ),
  },
  {
    path: "tmsapp/usergroup",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "tmsapp/UserGroup",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/task/userGroup.vue`
      ),
  },
  {
    path: "hrmaster/citymasters",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "hrmaster/CityMasters",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/ApplicationUser/CityMasters.vue`
      ),
  },
  {
    path: "hrmaster/languagemasters",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "hrmaster/LanguageMasters",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/ApplicationUser/LanguageMasters.vue`
      ),
  },
  {
    path: "hrmaster/levelmasters",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "hrmaster/LevelMasters",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/ApplicationUser/LevelMasters.vue`
      ),
  },

  {
    path: "/hrmaster/Notice",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "hrmaster/Notice",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/ApplicationUser/Notice.vue`
      ),
  },
  {
    path: "/hrmaster/NoticeReport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "hrmaster/NoticeReport",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/ApplicationUser/NoticeTab.vue`
      ),
  },
  {
    path: "hrmaster/Notice",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "hrmaster/DocumentAlert",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/ApplicationUser/DocumentAlert.vue`
      ),
  },
  {
    path: "hrmaster/designation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "hrmaster/Designation",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/ApplicationUser/Designation.vue`
      ),
  },
  {
    path: "hrmaster/department",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "hrmaster/Department",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/ApplicationUser/Department.vue`
      ),
  },
  {
    path: "hrmaster/section",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "hrmaster/Section",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/ApplicationUser/Section.vue`
      ),
  },
  {
    path: "hrmaster/division",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "hrmaster/Division",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/ApplicationUser/Division.vue`
      ),
  },
  {
    path: "hrmaster/subsection",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "hrmaster/SubSection",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/ApplicationUser/SubSection.vue`
      ),
  },
  {
    path: "hrmaster/subsectionTwo",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "hrmaster/SubSectionTwo",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/ApplicationUser/SubSectionTwo.vue`
      ),
  },
  {
    path: "/scrapsettings/plasticbag",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "app/plasticbag",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/PlasticBag.vue`
      ),
  },
  {
    path: "/scrapsettings/blisterType",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "app/blisterType",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/BlisterScrapCode.vue`
      ),
  },

  {
    path: "/assetmasters/Manufacturers",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "assetmaster/Manufacturers",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/assetmanagement/master/manufacturer/manufacturerlist.vue`
      ),
  },

  {
    path: "/assetmasters/Vendors",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "assetmaster/Vendors",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/assetmanagement/master/vendors/vendorslist.vue`
      ),
  },

  {
    path: "/assetmasters/CalibrationType",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "assetmaster/CalibrationType",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/assetmanagement/master/calibrationtype.vue`
      ),
  },

  {
    path: "/assetmasters/DeviceGroup",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "assetmaster/DeviceGroup",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/assetmanagement/master/devicegroup.vue`
      ),
  },

  {
    path: "plantmaintenance/devicecatalogue",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "plantmaintenance/devicecatalogue",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/assetmanagement/devicecatalogue/devicecataloguelist.vue`
      ),
  },

  // {
  //   path: '/devicecatalogue',
  //   meta: {
  //     breadcrumb: true,
  //     requiresAuth: true,
  //   },
  //   name: 'DeviceCatalogue',
  //   component: () => import(
  //     /* webpackChunkName: "routes" */
  //     /* webpackMode: "lazy-once" */
  //     `@/pages/assetmanagement/devicecatalogue/devicecataloguelist.vue`
  //   )
  // },

  {
    path: "plantmaintenance/sunwardasset",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "plantmaintenance/sunwardasset",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/assetmanagement/sunwardassetlist/assetlist.vue`
      ),
  },

  {
    path: "/calendarsettings/companyevent",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "calendar/companyevent",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/calendar/MasterEventList.vue`
      ),
  },
  {
    path: "/calendarsettings/prodplanning",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "calendar/prodplanning",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/calendar/ProductionPlan.vue`
      ),
  },
  {
    path: "/calendarsettings/workdate",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "calendar/workdate",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/calendar/WorkDate.vue`
      ),
  },
  {
    path: "/calendarsettings/workday",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "calendar/workday",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/calendar/WorkDay.vue`
      ),
  },

  {
    path: "/scrap/srapmaster",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "scrap/srapmaster",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/scrap/ScrapMaster.vue`
      ),
  },
  {
    path: "/scrap/blisterstrip",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "scrap/blisterstrip",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/scrap/BlisterStripWeight.vue`
      ),
  },
  {
    path: "/scrap/postscrap",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "scrap/postscrap",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/scrap/BlisterScrapEntry.vue`
      ),
  },
  {
    path: "/scrap/scrapentry",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "scrap/scrapentry",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/scrap/BlisterScrapEntryList.vue`
      ),
  },
  {
    path: "/scrap/scrapmaster",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "scrap/allowablescrap",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/scrap/BlisterScrapList.vue`
      ),
  },
  {
    path: "/scrap/prodscrapjournal",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "scrap/prodscrapjournal",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/scrap/ProductionScrapJournal.vue`
      ),
  },
  {
    path: "/scrap/productionBOM",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "scrap/productionBOM",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/scrap/ProductionBOM.vue`
      ),
  },

  {
    path: "/appsettings/country",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "app/country",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/Country.vue`
      ),
  },
  {
    path: "/appsettings/state",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "app/state",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/State.vue`
      ),
  },
  {
    path: "/appsettings/shift",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "app/shift",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/Shift.vue`
      ),
  },

  {
    path: "/planning/itemneedproduction",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "planning/itemneedproduction",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionreport/itemneedproduction.vue`
      ),
  },

  {
    path: "/planning/itemscalendarview",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "planning/itemscalendarview",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionreport/itemscalendarview.vue`
      ),
  },
  {
    path: "/salesorder/requestAC",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesorder/requestAC",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionreport/requestAC.vue`
      ),
  },
  {
    path: "/salesorder/requestACApproveLine",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesorder/requestACApproveLine",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionreport/requestACApproveLine.vue`
      ),
  },
  {
    path: "planning/productGrouping",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "planning/productGrouping",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/ProductGrouping/productGrouping.vue`
      ),
  },
  {
    path: "/productionv1/itemdetailreport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "productionv1/itemdetailreport",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionreport/itemdetailreport.vue`
      ),
  },
  {
    path: "/salesorder/genericcode",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesorder/genericcode",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/GenericCode/genericcode.vue`
      ),
  },
  {
    path: "/survey/ProductGroupSurvey",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "survey/ProductGroupSurvey",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/ProductGroupSurvey/ProductGroupSurvey.vue`
      ),
  },
  {
    path: "/survey/localClinic",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "survey/localClinic",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/localClinic/localClinic.vue`
      ),
  },
  {
    path: "/survey/surveyList",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "survey/surveyList",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/SalesSurveyByFieldForce/SalesSurveyByFieldForce.vue`
      ),
  },
  {
    path: "/salesorder/itemMaster",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesorder/itemMaster",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/itemMaster.vue`
      ),
  },
  {
    path: "/salesorder/itemBatchInfo",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesorder/itemBatchInfo",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/itemBatchInfo.vue`
      ),
  },
  {
    path: "/planning/monthlyStockBalanceTab",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "planning/monthlyStockBalanceTab",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/monthlyStockBalanceTab.vue`
      ),
  },
  {
    path: "/salesorder/itemAC",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesorder/itemAC",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/itemAC.vue`
      ),
  },

  {
    path: "/salesorder/salesOrderProfile",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesorder/salesOrderProfile",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/salesOrderProfile.vue`
      ),
  },
  {
    path: "/salesorder/tenderOrders",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesorder/tenderOrders",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/simulationAdhoc.vue`
      ),
  },
  {
    path: "/planning/productionbatchsize",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "planning/productionbatchsize",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/productionbatchsize.vue`
      ),
  },

  {
    path: "/planning/simulation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "planning/simulation",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/simulationLayout.vue`
      ),
  },
  {
    path: "/planning/orderRequirement",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "planning/orderRequirement",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/orderRequirement.vue`
      ),
  },
  {
    path: "/planning/FulfillmentofAdhoc",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "planning/FulfillmentofAdhoc",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/FulfillmentofAdhoc.vue`
      ),
  },
  {
    path: "/salesorder/ProductionSimulation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesorder/ProductionSimulation",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/ProductionSimulation.vue`
      ),
  },

  {
    path: "/salesorder/GroupPlanning",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesorder/GroupPlanning",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/groupPlanning.vue`
      ),
  },
  {
    path: "/planning/navMonthlyStockBalanceUpload",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "planning/navMonthlyStockBalanceUpload",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/monthlyStockBalanceTabUpload.vue`
      ),
  },
  {
    path: "/planning/simulationAdhocOrders",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "planning/simulationAdhocOrders",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/simulationAdhoc/simulationAdhocOrders.vue`
      ),
  },
  {
    path: "/planning/simulationAdhocOrdersAlps",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "planning/simulationAdhocOrdersAlps",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/simulationAdhoc/simulationAdhocOrdersAlps.vue`
      ),
  },

  {
    path: "/planning/simulationAdhocOrdersV3",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "planning/simulationAdhocOrdersV3",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/simulationAdhoc/simulationAdhocV3.vue`
      ),
  },
  {
    path: "/planning/simulationReport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "planning/simulationReport",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/simulationAdhoc/simulationReport.vue`
      ),
  },
  {
    path: "/planning/groupPlanningReport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "planning/groupPlanningReport",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/groupPlanningReport/groupPlanningReport.vue`
      ),
  },
  {
    path: "/planning/forecastProductionSimulation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "planning/forecastProductionSimulation",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/forcastProductionSimulation/forecastProductionSimulation.vue`
      ),
  },
  {
    path: "/planning/simulationAdhocV2",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "planning/simulationAdhocV2",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/simulationAdhocV2/simulationAdhocV2.vue`
      ),
  },

  {
    path: "/planning/noOfTicketsAllocate",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "planning/noOfTicketsAllocate",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/groupPlanningReport/noOfticketsAllocate.vue`
      ),
  },
  {
    path: "/planning/simulationMidMonth",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "planning/simulationMidMonth",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/simulationMidMonth/simulationMidMonth.vue`
      ),
  },
  {
    path: "/salesorder/NavSaleCategory",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesorder/NavSaleCategory",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/navSaleCategory.vue`
      ),
  },
  {
    path: "/salesorder/UnitConversion",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesorder/UnitConversion",
    component: () =>
      import(
        /* webpackChunkName: "routes" ra */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/UnitConversion.vue`
      ),
  },

  {
    path: "/salesorder/ProductionBasicCycle",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesorder/ProductionBasicCycle",
    component: () =>
      import(
        /* webpackChunkName: "routes" ra */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/ProductionBasicCycle.vue`
      ),
  },

  {
    path: "/salesorder/ExchangeRate",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesorder/ExchangeRate",
    component: () =>
      import(
        /* webpackChunkName: "routes" ra */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/ExchangeRate.vue`
      ),
  },

  {
    path: "/salegroup/Salesdeliveryorder",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salegroup/Salesdeliveryorder",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/deliveryOrder.vue`
      ),
  },

  {
    path: "/quotationhistory",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "quotationhistory",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/quotationHistory.vue`
      ),
  },

  {
    path: "/commitmentOrders",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "commitmentOrders",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/commitmentOrders.vue`
      ),
  },

  {
    path: "production/ProcessMachineTime",
    meta: {
      breadcrumb: false,
      requiresAuth: true,
    },
    name: "production/ProcessMachineTime",
    component: (a) =>
      import(
        `@/pages/productionplanning/processMachineTime/processMachineTime.vue`
      ),
  },
  {
    path: "/packagingItemHistory",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "packagingItemHistory",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/packagingItemHistory.vue`
      ),
  },

  {
    path: "/interCompanyPurchaseOrder",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "interCompanyPurchaseOrder",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/interCompanyPurchaseOrder.vue`
      ),
  },
  // {
  //   path: '/salesAdhoc',
  //   meta: {
  //     breadcrumb: true,
  //     requiresAuth: true
  //   },
  //   name: 'salesAdhoc',
  //   component: () => import(
  //     /* webpackChunkName: "routes" */
  //     /* webpackMode: "lazy-once" */
  //     // `@/pages/productionplanning/productionplanningv1/salesAdhoc.vue`
  //     `@/pages/SalesOrders/SalesOrder.vue`
  //   )
  // },

  {
    path: "/inventory/itemBatchEntry",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "inventory/itemBatchEntry",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/inventory/itemBatchEntry.vue`
      ),
  },
  {
    path: "/inventory/itemStockInfo",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "inventory/itemStockInfo",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/inventory/itemStockInfo.vue`
      ),
  },

  {
    path: "/inventory/disposalItem",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "inventory/disposalItem",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/inventory/DisposalItem.vue`
      ),
  },
  {
    path: "/inventory/movementOfStockOutConfirm",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "inventory/movementOfStockOutConfirm",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/inventory/movementStockOutConfirm.vue`
      ),
  },
  {
    path: "/inventory/sampleRequestforDoctors",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "inventory/sampleRequestforDoctors",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/inventory/sampleRequestforDoctors.vue`
      ),
  },
  {
    path: "/inventory/SampleRequestForm",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "inventory/SampleRequestForm",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/inventory/SampleRequestForm.vue`
      ),
  },
  {
    path: "/inventory/requestForPerson",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "inventory/requestForPerson",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/inventory/requestForPerson.vue`
      ),
  },

  {
    path: "/inventory/inventoryType",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "inventory/inventoryType",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/inventory/inventoryType.vue`
      ),
  },
  {
    path: "/inventory/openingBalanceInventoryType",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "inventory/openingBalanceInventoryType",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/inventory/openingBalanceInventoryType.vue`
      ),
  },
  {
    path: "/inventory/StockMovement",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "inventory/StockMovement",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/inventory/StockMovement.vue`
      ),
  },
  {
    path: "salegroup/SalesOrder",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salegroup/SalesOrder",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        // `@/pages/productionplanning/productionplanningv1/salesAdhoc.vue`
        `@/pages/SalesOrders/SalesOrder.vue`
      ),
  },
  {
    path: "/salegroup/SalesblanketOrderEntry",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salegroup/SalesblanketOrderEntry",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/SalesOrders/BlanketOrderEntry/BlanketOrderEntry.vue`
      ),
  },

  {
    path: "/salegroup/SalesborrowItemsFromDistributor",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salegroup/SalesborrowItemsFromDistributor",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/SalesOrders/BorrowItems/borrowItemsFromDistributor.vue`
      ),
  },
  {
    path: "/salegroup/SalesblanketTenderAgency",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salegroup/SalesblanketTenderAgency",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/SalesOrders/BlanketTenderAgency/BlanketTenderAgency.vue`
      ),
  },
  {
    path: "salegroup/SalesblanketOthers",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salegroup/SalesblanketOthers",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/SalesOrders/BlanketOthers/BlanketOthers.vue`
      ),
  },
  {
    path: "/salegroup/SalessalesOrderStandAlone",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salegroup/SalessalesOrderStandAlone",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/SalesOrders/SalesOrderStandAlone/salesOrderStandAlone.vue`
      ),
  },
  {
    path: "/salegroup/SalessellingCatalogue",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salegroup/SalessellingCatalogue",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/sellingCatalogue.vue`
      ),
  },

  {
    path: "/salegroup/SalesPromotion",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salegroup/SalesPromotion",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/salesPromotion/salesPromotion.vue`
      ),
  },
  {
    path: "/reportList/sellingCatalogueReport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/sellingCatalogueReport",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/sellingCatalogueReport.vue`
      ),
  },

  {
    path: "/salesorder/ProductionOrderLineSync",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesorder/ProductionOrderLineSync",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/prodorderLineSync.vue`
      ),
  },
  {
    path: "/reportList/quotationHistoryReport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/quotationHistoryReport",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/quotationHistoryReport.vue`
      ),
  },
  {
    path: "/reportList/dynamicFormReport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/dynamicFormReport",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/FileProfileTypeView/DynamicFormReport.vue`
      ),
  },
  {
    path: "/reportList/commitmentOrderReport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/commitmentOrderReport",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/commitmentOrderReport.vue`
      ),
  },
  {
    path: "/salegroup/SalesmasterBlanketOrder",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salegroup/SalesmasterBlanketOrder",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/masterBlanketOrder.vue`
      ),
  },

  {
    path: "salegroup/SalessalesUpdateInformation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salegroup/SalessalesUpdateInformation",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/SalesOrders/SalesOrderSearch/salesOrderUpdateInformation.vue`
      ),
  },

  {
    path: "/purchaseorder",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "purchaseorder",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/purchaseOrder.vue`
      ),
  },
  {
    path: "/process/navManufacturingProcessgantt",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "process/navManufacturingProcessgantt",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/NavManufacturingProcess/NavManufacturingProcess.vue`
      ),
  },
  {
    path: "/SalesOrderNovate",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "SalesOrderNovate",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/SalesOrderNovate.vue`
      ),
  },
  {
    path: "/salegroup/SalesTransitionInformationforAlpstenderInformation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salegroup/SalesTransitionInformationforAlpstenderInformation",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/TransitioninformationforAlpsTenderInformation.vue`
      ),
  },
  {
    path: "/BlanketOrder-TransferbalanceQuntityforNewContract",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "BlanketOrder-TransferbalanceQuntityforNewContract",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/BlanketOrder-TransferbalanceQuntityforNewContract.vue`
      ),
  }, //

  {
    path: "/salegroup/SalesChangePriceDuringTenderPeriod",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salegroup/SalesChangePriceDuringTenderPeriod",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/ChangePriceDuringTenderPeriod.vue`
      ),
  }, //

  {
    path: "/RequestforExtentionforBlanketOrder",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "RequestforExtentionforBlanketOrder",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/RequestforExtentionforBlanketOrder.vue`
      ),
  }, //

  {
    path: "/salesorder/navitemLinks",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesorder/navitemsLinks",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/navItemLinks.vue`
      ),
  },
  {
    path: "/qc/COATickets",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "qc/COATickets",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/mobile/productionentry/productionTickets.vue`
      ),
  },
  {
    path: "/reportList/dailySalesReportByDistributor",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/dailySalesReportByDistributor",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/dailySalesReport/dailySalesReportByDistributorTab.vue`
      ),
  },
  {
    path: "/salesgroup/salesTarget",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesgroup/salesTarget",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/SalesTarget/SalesTarget.vue`
      ),
  },
  {
    path: "/production/QcApproval",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "production/QcApproval",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/mobile/QcApproval/QcApproval.vue`
      ),
  },
  {
    path: "/production/ProcessTransfer",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "production/ProcessTransfer",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/mobile/ProcessTransfer/ProcessTransfer.vue`
      ),
  },
  {
    path: "/production/AppTranLocation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "production/AppTranLocation",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/mobile/AppTranLocation/AppTranLocation.vue`
      ),
  },
  {
    path: "/production/AppTranStock",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "production/AppTranStock",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/mobile/AppTranStock/AppTranStock.vue`
      ),
  },
  {
    path: "/salesForecast/itemSalesPrice",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesForecast/itemSalesPrice",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/salesForecast/itemSalesPrice.vue`
      ),
  },
  {
    path: "/salesForecast/acByDistributor",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesForecast/acByDistributor",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/salesForecast/acByDistributor.vue`
      ),
  },
  // {
  //   path: '/productionreport/plannercomment',
  //   meta: {
  //     breadcrumb: true
  //   },
  //   name: 'productionreport/plannercomment',
  //   component: () => import(
  //     /* webpackChunkName: "routes" */
  //     /* webpackMode: "lazy-once" */
  //     `@/pages/productionplanning/productionreport/plannercomment.vue`
  //   )
  // },

  {
    path: "/salesorder/acentry",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesorder/acentry",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/acentry.vue`
      ),
  },
  {
    path: "/salesorder/navMethodCode",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesorder/navMethodCode",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/navMethodCode.vue`
      ),
  },
  {
    path: "/productdevelopment/registered",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    props: (route) => ({
      type: route.query.type,
    }),
    name: "productdevelopment/registered",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/registered.vue`
      ),
  },
  {
    path: "/search/commonSearch",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    props: (route) => ({
      type: route.query.type,
    }),
    name: "search/commonSearch",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/commonsearch/commonSearch.vue`
      ),
  },
  {
    path: "fpItems/activeIngredientInformation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    props: (route) => ({
      type: route.query.type,
    }),
    name: "fpItems/activeIngredientInformation",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/FPItemForm.vue`
      ),
  },

  {
    path: "fpItems/fpEquivalentNavision",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    props: (route) => ({
      type: route.query.type,
    }),
    name: "fpItems/fpEquivalentNavision",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/FPProductNavision/FPEquivalentNavisionLine.vue`
      ),
  },

  {
    path: "registration/SalesPackingInformation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    props: (route) => ({
      type: route.query.type,
    }),
    name: "registration/SalesPackingInformation",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/PackagingItem/SalesPackingInformation.vue`
      ),
  },

  //

  {
    path: "registration/SalesItemPackingInformation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    props: (route) => ({
      type: route.query.type,
    }),
    name: "registration/SalesItemPackingInformation",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/PackagingItem/SalesItemPackingInformation.vue`
      ),
  },

  //
  {
    path: "packaging/NonContactPackagingItem",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    props: (route) => ({
      type: route.query.type,
    }),
    name: "packaging/NonContactPackagingItem",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/PackagingItem/NonContactPackagingItem.vue`
      ),
  },
  {
    path: "packaging/CommonInfoNoncontactPackaging",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    props: (route) => ({
      type: route.query.type,
    }),
    name: "packaging/CommonInfoNoncontactPackaging",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/PackagingItem/CommonInfoNonPackaging.vue`
      ),
  },
  {
    path: "/registration/working",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    props: (route) => ({
      type: route.query.type,
    }),
    name: "registration/working",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/registered.vue`
      ),
  },
  {
    path: "/production/productBatchInformation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    props: (route) => ({
      type: route.query.type,
    }),
    name: "production/productBatchInformation",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/ProductBatchInformation.vue`
      ),
  },
  {
    path: "/fpItems/FinishProductExcipient",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    props: (route) => ({
      type: route.query.type,
    }),
    name: "fpItems/FinishProductExcipient",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/ProductBOMByPerUnit.vue`
      ),
  },
  {
    path: "/registration/product",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    props: (route) => ({
      type: route.query.type,
    }),
    name: "registration/product",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/product.vue`
      ),
  },
  {
    path: "/registration/phramacologicalProperties",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "registration/phramacologicalProperties",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/phramacologicalProperties.vue`
      ),
  },
  {
    path: "/fpItems/FPManufacturingRecipe",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    props: (route) => ({
      type: route.query.type,
    }),
    name: "fpItems/FPManufacturingRecipe",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/itemClassificationForm/Dynamic/FPManufacturingRecipe.vue`
      ),
  },
  {
    path: "/productdevelopment/ManufacturingProcess",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "productdevelopment/ManufacturingProcess",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/ManufacturingProcess.vue`
      ),
  },

  {
    path: "navisionitems/GeneralNamingEquivalent",
    meta: {
      breadcrumb: true,
    },
    props: (route) => ({
      type: route.query.type,
    }),
    name: "navisionitems/GeneralNamingEquivalent",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/PackagingItem/GeneralNamingEquivalent.vue`
      ),
  },
  {
    path: "/registration/productGeneralInfo",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "registration/productGeneralInfo",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/productGeneralinfo.vue`
      ),
  },

  {
    path: "productdevelopment/CriticalStepsandIntermediate",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "productdevelopment/CriticalStepsandIntermediate",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/CriticalStepsandIntermediate.vue`
      ),
  },
  {
    path: "/registration/RegistrationVariation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "registration/RegistrationVariation",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/RegistrationVariation.vue`
      ),
  },
  {
    path: "search/registeredPackSizeSearch",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "search/registeredPackSizeSearch",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/registeredPackSizeSearch.vue`
      ),
  },
  {
    path: "registration/ProductionMaterial",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "registration/ProductionMaterial",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/ProductionMaterial.vue`
      ),
  },
  {
    path: "/registration/RecordVariation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    props: (route) => ({
      type: route.query.type,
    }),
    name: "registration/RecordVariation",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/RecordVariation.vue`
      ),
  },
  {
    path: "/navisionitems/GenericNameListing",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "navisionitems/GenericNameListing",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/GenericNameListing.vue`
      ),
  },
  {
    path: "/registration/RegistrationFormat",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    props: (route) => ({
      type: route.query.type,
    }),
    name: "registration/RegistrationFormat",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/RegistrationFormat.vue`
      ),
  },

  {
    path: "/registration/pkgApprovalInformation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    props: (route) => ({
      type: route.query.type,
    }),
    name: "registration/pkgApprovalInformation",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/PKGApproval/pkgApprovalInformation.vue`
      ),
  },
  {
    path: "/manpower/ProductionProcessLibrary",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    props: (route) => ({
      type: route.query.type,
    }),
    name: "manpower/ProductionProcessLibrary",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/ProductionProcessLibrary.vue`
      ),
  },
  {
    path: "/salesorder/acinfo",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesorder/acinfo",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/acinfo.vue`
      ),
  },
  {
    path: "/salesorder/Ipir",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesorder/Ipir",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/IPIRForm.vue`
      ),
  },
  {
    path: "/salesorder/customer",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salesorder/customer",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/navision/master/customer.vue`
      ),
  },
  {
    path: "/master/item",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "master/item",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/navision/master/item.vue`
      ),
  },

  {
    path: "/master/salesOrderKiv",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "master/salesOrderKiv",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/navision/master/salesOrderKiv.vue`
      ),
  },

  {
    path: "/workorder/workorder",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "workorder/workorder",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/WorkOrder/workorder.vue`
      ),
  },
  {
    path: "/workorder/workorderReport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "workorder/workorderReport",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/WorkOrder/workOrderReport.vue`
      ),
  },
  {
    path: "/wikigroup/abbreviation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "wikigroup/abbreviation",

    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/ApplicationUser/Abbreviation.vue`
      ),
  },
  {
    path: "/wikigroup/glossary",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "wikigroup/glossary",

    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/ApplicationUser/Glossary.vue`
      ),
  },
  {
    path: "/wikigroup/glossaryabbreviationsearch",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "wikigroup/glossaryabbreviationsearch",

    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/masters/ApplicationUser/Glossaryabbreviationsearch.vue`
      ),
  },
  {
    path: "/wikigroup/appwiki",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "wikigroup/appwiki",
    component: (a) => import(`@/pages/application/ApplicationWiki.vue`),
  },
  {
    path: "/wikigroup/draftwiki",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "wikigroup/draftwiki",
    component: (a) =>
      import(`@/pages/application/DraftApplicationWiki/ApplicationWiki.vue`),
  },
  {
    path: "/wikigroup/reviseWiki",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "wikigroup/reviseWiki",
    component: (a) =>
      import(`@/pages/application/reviseWorkInstruction/reviseWorkWiki.vue`),
  },
  {
    path: "/wikigroup/appWikilisting",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "wikigroup/appWikilisting",
    component: (a) => import(`@/pages/application/ApplicationWikiListing.vue`),
  },
  {
    path: "/wikigroup/viewDocument",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "wikigroup/viewDocument",
    component: (a) =>
      import(`@/pages/application/ApplicationWikiViewDocument.vue`),
  },
  {
    path: "reportList/manufactureSitereportLists",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/manufactureSitereportLists",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/ManufacturingSiteReport.vue`
      ),
  },
  {
    path: "/reportList/selfTest",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/selfTest",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/selfTest/selfTest.vue`
      ),
  },
  {
    path: "/reportList/selfTestSecurity",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/selfTestSecurity",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/selfTest/selfTestSecurity.vue`
      ),
  },
  {
    path: "/reportList/RecordVariationReport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    props: (route) => ({
      type: route.query.type,
    }),
    name: "reportList/RecordVariationReport",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/RecordVariationReport.vue`
      ),
  },
  {
    path: "reportList/companyInformationReport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/companyInformationReport",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/CompanyInformationReport.vue`
      ),
  },
  {
    path: "reportList/pharmacologicalInformationReport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/pharmacologicalInformationReport",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/PharmacologicalInformationReport.vue`
      ),
  },
  {
    path: "/reportList/humanmovement",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/humanmovement",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/Humanmovement/humanmovement.vue`
      ),
  },
  {
    path: "/reportList/HumanMovementSage",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/HumanMovementSage",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/Humanmovement/HumanMovementSage.vue`
      ),
  },
  {
    path: "/reportList/HumanMovementAction",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/HumanMovementAction",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/Humanmovement/HumanMovementAction.vue`
      ),
  },
  {
    path: "/reportList/ProductionBatchReport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/ProductionBatchReport",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/ProductionBatchNoReport/ProductionBatchReport.vue`
      ),
  },
  {
    path: "/reportList/ProductionBatchNoQrCodeReport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/ProductionBatchNoQrCodeReport",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/ProductionBatchNoReport/ProductionBatchNoQrCodeReport.vue`
      ),
  },
  {
    path: "reportList/salesOrderReport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/salesOrderReport",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/productionplanningv1/salesOrderReport.vue`
      ),
  },
  {
    path: "/reportList/salesnondeliversoreport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/salesnondeliversoreport",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/salesorder/nondeliversoreport.vue`
      ),
  },
  {
    path: "/reportList/productionOutputReport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/productionOutputReport",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/mobile/productionoutput/productionOutputReport.vue`
      ),
  },
  {
    path: "/reportList/SocustomersItemCrossReferenceReport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/SocustomersItemCrossReferenceReport",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/SoBycustomersReport/SocustomersItemCrossReferenceReport.vue`
      ),
  },
  {
    path: "/reportList/CompanyListingNavisionReport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/CompanyListingNavisionReport",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/SoBycustomersReport/CompanyListingNavisionReport.vue`
      ),
  },
  {
    path: "/reportList/DistributorPricing",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/DistributorPricing",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/finance/DistributorPricing.vue`
      ),
  },
  {
    path: "/reportList/InterCompanyPricingSummary",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/InterCompanyPricingSummary",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/finance/InterCompanyPricingSummary.vue`
      ),
  },
  {
    path: "Finance/MarginInformation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "Finance/MarginInformation",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/finance/MarginInformation.vue`
      ),
  },
  {
    path: "/Finance/MasterInterCompanyPricing",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    props: (route) => ({
      type: route.query.type,
    }),
    name: "Finance/MasterInterCompanyPricing",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/finance/MasterInterCompanyPricing.vue`
      ),
  },
  {
    path: "/Finance/itemCost",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    props: (route) => ({
      type: route.query.type,
    }),
    name: "Finance/itemCost",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/productionplanning/finance/itemCost.vue`
      ),
  },
  {
    path: "/salegroup/SalesDistributorReplenishment",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salegroup/SalesDistributorReplenishment",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/policy/DistributorReplenishment.vue`
      ),
  },
  {
    path: "/salegroup/Salesemail",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salegroup/Salesemail",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/Email/ReceiveEmails.vue`
      ),
  },
  {
    path: "/salegroup/PortalEmail",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salegroup/PortalEmail",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/Email/PortalEmail.vue`
      ),
  },
  {
    path: "/salegroup/outLookEmail",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "salegroup/outLookEmail",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/Email/outLookEmail.vue`
      ),
  },
  {
    path: "/DMS/fileProfileTypeView",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "DMS/fileProfileTypeView",
    component: (a) => import(`@/pages/FileProfileType/FileProfileTypeView.vue`),
  },
  {
    path: "/DMS/reserveNumberSeries",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "DMS/reserveNumberSeries",
    component: (a) => import(`@/pages/FileProfileTypeView/reserveProfileNumberSeriesAll.vue`),
  },
  {
    path: "/DMS/fileProfileTypeTree",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "DMS/fileProfileTypeTree",
    component: (a) =>
      import(`@/pages/FileProfileType/FileProfileTypeDetails.vue`),
  },
  {
    path: "/DMS/fileProfileTypeDocView",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "DMS/fileProfileTypeDocView",
    component: (a) =>
      import(`@/pages/FileProfileTypeView/FileProfileTypeView.vue`),
  },
  {
    path: "/DMS/fileProfileTypeDocTree",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "DMS/fileProfileTypeDocTree",
    component: (a) =>
      import(`@/pages/FileProfileTypeView/FileProfileTypeDetails.vue`),
  },
  {
    path: "/DMS/documentDirectory",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "DMS/documentDirectory",
    component: (a) =>
      import(`@/pages/FileProfileTypeView/DocumentDirectory.vue`),
  },
  {
    path: "/planning/psb",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "/planning/psb",
    component: (a) => import(`@/pages/productionplanning/psb/psbReport.vue`),
  },

  {
    path: "/planning/psbReportV2",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "/planning/psbReportV2",
    component: (a) => import(`@/pages/productionplanning/psb/psbReportTab.vue`),
  },

  {
    path: "/planning/productAvailabilityReport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "planning/productAvailabilityReport",
    component: (a) =>
      import(
        `@/pages/productionplanning/PARReport/ProductAvailabilityReport.vue`
      ),
  },
  {
    path: "/calendar/companyCalendar",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "/calendar/companyCalendar",
    component: (a) => import(`@/pages/calendar/companyCalendar.vue`),
  },
  {
    path: "/AttachmentDownload",
    meta: {
      breadcrumb: false,
      requiresAuth: true,
    },
    name: "AttachmentDownload",
    component: (a) => import(`@/components/Attachment/AttachmentDownload.vue`),
  },
  {
    path: "/application/JobProgressTemplate",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "application/JobProgressTemplate",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/JobProgressTemplate/JobProgressTemplate.vue`
      ),
  },
  {
    path: "/application/JobProgressTemplateView",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "application/JobProgressTemplateView",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/JobProgressTemplate/JobProgressTemplateView.vue`
      ),
  },
  {
    path: "/reportList/itemstockinfoReport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/itemstockinfoReport",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/inventory/ItemStockReport.vue`
      ),
  },
  {
    path: "/reportList/itemStockByClosingDateReport",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/itemStockByClosingDateReport",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/inventory/itemStockByClosingDateReport.vue`
      ),
  },
  {
    path: "/asset/assetMaster",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "/asset/assetMaster",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/assets/assetMaster.vue`
      ),
  },
  {
    path: "/asset/assetTicket",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "/asset/assetTicket",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/assets/assetTicket.vue`
      ),
  },
  {
    path: "/medMaster/medMaster",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "/medMaster/medMaster",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/medMaster/medMaster.vue`
      ),
  },
  {
    path: "/MobileShift/MobileShift",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "/MobileShift/MobileShift",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/mobile/MobileShift/MobileShift.vue`
      ),
  },
  {
    path: "/reAssignmentJob/reAssignmentJob",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "/reAssignmentJob/reAssignmentJob",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/mobile/reAssignmentJob/reAssignmentJob.vue`
      ),
  },
  {
    path: "/documentAlert",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "DocumentAlert",
    component: (a) =>
      import(`@/pages/masters/ApplicationUser/DocumentAlert.vue`),
  },
  {
    path: "/applicationsetting/npraformulation",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "/applicationsetting/npraformulation",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/Npraformulation/Npraformulation.vue`
      ),
  },

  {
    path: "/DMS/fileProfileTypeDocViewArchive",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "DMS/fileProfileTypeDocViewArchive",
    component: (a) =>
      import(`@/pages/FileProfileTypeViewArchive/FileProfileTypeView.vue`),
  },
  {
    path: "/DMS/fileProfileTypeDocArchiveTree",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "DMS/fileProfileTypeDocArchiveTree",
    component: (a) =>
      import(`@/pages/FileProfileTypeViewArchive/FileProfileTypeDetails.vue`),
  },
  {
    path: "/video",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "video",
    component: (a) => import(`@/components/video/videos.vue`),
  },
  {
    path: "/DMS/monthlyDocumentReport",
    meta: {
      breadcrumb: false,
      requiresAuth: true,
    },
    name: "DMS/monthlyDocumentReport",
    component: (a) => import(`@/pages/FileProfileTypeView/MonthlyDocumentReport.vue`),
  },
  {
    path: "/DMS/uploadZeroSizeFile",
    meta: {
      breadcrumb: false,
      requiresAuth: true,
    },
    name: "DMS/uploadZeroSizeFile",
    component: (a) => import(`@/pages/uploadZeroSizeFile.vue`),
  },
  {
    path: "/tmsapp/permissionTransfer",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "tmsapp/permissionTransfer",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/permissionTransfer/permissionTransfer.vue`
      ),
  },
  {
    path: "/wikigroup/AppwikiPdf",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "wikigroup/AppwikiPdf",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/application/reviseWorkInstruction/appWikipdf.vue`
      ),
  },

  {
    path: "/unReadSubject",
    /* meta: {
      public: true,
    }, */
    name: "Un Read Subject",
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/components/widgets/task/taskUnReadView.vue`
      ),
  },
  {
    path: "/application/templateTestCase",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "application/templateTestCase",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/templateTestCase/templateTestCase.vue`
      ),
  },
  
  {
    path: "/application/templateTestCaseForm",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "application/templateTestCaseForm",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/templateTestCase/templateTestCaseForm.vue`
      ),
  },
  {
    path: "/application/ProductionActivityCase",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "application/ProductionActivityCase",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/ProductionActivityCase/ProductionActivityCase.vue`
      ),
  },
  {
    path: "/reportList/productionActivityApp",
    meta: {
      breadcrumb: true,
      requiresAuth: true,
    },
    name: "reportList/productionActivityApp",
    props: (route) => ({
      type: route.query.type,
    }),
    component: () =>
      import(
        /* webpackChunkName: "routes" */
        /* webpackMode: "lazy-once" */
        `@/pages/ProductionActivityApp/ProductionActivityApp.vue`
      ),
  },
];
