import Vue from 'vue';
import Router from 'vue-router';
import store from '../store';
import paths from './paths';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';

Vue.use(Router);
const router = new Router({
  base: '/',
  mode: 'hash',
  linkActiveClass: 'active',
  routes: paths
});
let entryUrl = null;
// router gards
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isAuthenticated) {
      console.log('isAuthenticated', entryUrl);
      if (entryUrl) {
        const url = entryUrl;
        entryUrl = null;
        next(url);
        return;
      }
      next();
      return;
    }
    // console.log(to.fullPath);
    //console.log(to.query.folder);
    //console.log(to.query);
    entryUrl = to.fullPath; // store entry url before redirect
    console.log('notisAuthenticated', entryUrl);
    next('/');
  } else {
    // entryUrl = to.path; // store entry url before redirect
    console.log('not required', entryUrl);
    next();
  }
});

router.afterEach((to, from) => {
  // ...
  NProgress.done();
});

export default router;