// Import TinyMCE
import tinymce from "tinymce/tinymce";
// A theme is also required
import "tinymce/themes/modern/theme";

// Any plugins you want to use has to be imported
// import 'tinymce/plugins/advlist';
import "tinymce/plugins/wordcount";
import "tinymce/plugins/autolink";
import "tinymce/plugins/autosave";
import "tinymce/plugins/charmap";
import "tinymce/plugins/codesample";
import "tinymce/plugins/contextmenu";
import "tinymce/plugins/emoticons";
import "tinymce/plugins/fullscreen";
import "tinymce/plugins/hr";
import "tinymce/plugins/imagetools";
import "tinymce/plugins/insertdatetime";
import "tinymce/plugins/link";
import "tinymce/plugins/media";
import "tinymce/plugins/noneditable";
import "tinymce/plugins/paste";
import "tinymce/plugins/print";
import "tinymce/plugins/searchreplace";
import "tinymce/plugins/tabfocus";
import "tinymce/plugins/template";
import "tinymce/plugins/textpattern";
import "tinymce/plugins/visualblocks";
import "tinymce/plugins/anchor";
import "tinymce/plugins/autoresize";
import "tinymce/plugins/bbcode";
import "tinymce/plugins/code";
import "tinymce/plugins/colorpicker";
import "tinymce/plugins/directionality";
import "tinymce/plugins/fullpage";
import "tinymce/plugins/help";
import "tinymce/plugins/image";
import "tinymce/plugins/importcss";
import "tinymce/plugins/legacyoutput";
import "tinymce/plugins/lists";
import "tinymce/plugins/nonbreaking";
import "tinymce/plugins/pagebreak";
import "tinymce/plugins/preview";
import "tinymce/plugins/save";
import "tinymce/plugins/spellchecker";
import "tinymce/plugins/table";
import "tinymce/plugins/textcolor";
import "tinymce/plugins/toc";
import "tinymce/plugins/visualchars";

import "tinymce/skins/lightgray/skin.min.css";
export default {
  components: {
    tinymce,
  },
  name: "tinymce",
  props: {
    config: {
      type: Object,
      default: () => ({}),
    },
    value: {
      type: String,
      default: "",
    },
    name: {
      type: String,
      default: "",
    },
  },

  data: () => ({
    editorInstance: null,
    content: "",
  }),

  methods: {
    handleError(err) {
      // eslint-disable-next-line no-console
      console.error(err);
      this.$emit("error", err);
    },

    handleSuccess(editor) {
      this.editorInstance = editor;
      this.$emit("init", editor);

      const content = this.value;

      editor.setContent(content);
      editor.on("input change undo redo setcontent focus", this.handleInput);
      editor.on("change setcontent", this.handleChange);
      editor.on("focus", this.handleFocus);
    },

    setContent(content) {
      try {
        this.editorInstance.setContent(content);
      } catch (err) {
        this.handleError(err);
      }
    },

    getContent() {
      const { editorInstance } = this;

      if (editorInstance) {
        return editorInstance.getContent();
      }

      this.handleError(new Error("vue-mce is not initialized yet"));
      return null;
    },

    handleInput() {
      const content = this.editorInstance.getContent();
      this.content = content;
      this.$emit("input", content);
    },

    handleChange() {
      this.$emit("change", this.content);
    },
    handleFocus() {
      this.$emit("focus", this.content);
    },
    focusinTinymce(event) {
      if ($(event.target).closest(".mce-window").length) {
        event.stopImmediatePropagation();
      }
    },
  },

  watch: {
    value(newValue) {
      if (newValue !== this.content) {
        this.setContent(newValue);
      }
    },
  },

  mounted() {
    if (!window.tinymce) {
      return this.handleError(new Error("TinyMce was not found"));
    }

    const config = this.config;
    const target = this.$refs.textarea;

    config.target = target;
    config.init_instance_callback = this.handleSuccess;

    window.tinymce.init(config).catch(this.handleError);
    document.addEventListener("focusin", this.focusinTinymce, true);
  },

  beforeDestroy() {
    this.$emit("destroy", this.editorInstance);

    window.tinymce.remove(this.$refs.textarea);
    document.removeEventListener("focusin", this.focusinTinymce, true);
  },
};
