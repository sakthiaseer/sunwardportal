export default Object.freeze({
  SAVE: "Record Saved Successfully",
  UPDATE: "Record Updated Successfully",
  DELETE: "Record Deleted Successfully",
  VALIDATE: "validation error occured.Please check the required field values!.",
  DATEFORMAT: "DD-MMM-YYYY",
  YEARMONTHDAYFORMAT: "YYYY-MMM-DD",
  MONTHYEARFORMAT: "MMM-YYYY",
  DATETIMEFORMAT: "DD-MMM-YYYY hh:mm A",
  VERSION: "Version Saved Successfully",
  ITEMCLASSIFICATIONPROFILEERROR: "Profile Not Set In Application Form",
  AllowMaxSize: "1024",
  SuperUser: "47",



  //DEV
    BASE_URL: "https://portal.sunwardpharma.com:2025/#/",
   API_URL: "https://portal.sunwardpharma.com/tmsapidev/api/",
   BASEURL: "https://portal.sunwardpharma.com:2025/#/tasktreeview?",
   DOWNURL: "https://portal.sunwardpharma.com:2025/#/downloadFile?",
   APPUpload: "https://portal.sunwardpharma.com:2025/AppUpload/",
   GLOSSARYURL: "https://portal.sunwardpharma.com:2025/#/Glossaryview?",
   NotificationHub: "https://portal.sunwardpharma.com/tmsapidev/chatHub",
   FILE_PATH_URL: "https://portal.sunwardpharma.com:2025/AppUpload",
   WORKORDERURL:
     "https://portal.sunwardpharma.com:2025/#/workorder/workorderReport?",
   FOLDERURL: "https://portal.sunwardpharma.com:2025/#/publicfoldertreeview?",
   ATTACH_DOWNURL: "https://portal.sunwardpharma.com:2025/#/AttachmentDownload?",
   FILEPROFILETYPE_DOWNURL:
     "https://portal.sunwardpharma.com:2025/#/fileProfileTypeDownload?",
   UNREADSUBJECT: "https://portal.sunwardpharma.com:2025/#/unReadSubject?",
   DYNAMICFORM_PROFILEURL: "https://portal.sunwardpharma.com:2289",
   PERSONAL_CALENDER_URL: "https://portal.sunwardpharma.com:2055",
   PRODUCTION_ACTIVITY_APP: "https://portal.sunwardpharma.com:2287/",
   TOPICCHAT_URL: "https://portal.sunwardpharma.com:2384/", 
   DOCUMENT_VIWER_URL:"https://portal.sunwardpharma.com/FileViewerUAT/",

  //LIVE

  //   NotificationHub: "https://portal.sunwardpharma.com/tmsapi/chatHub",
  // BASE_URL: "https://portal.sunwardpharma.com/#/",
  // API_URL: "https://portal.sunwardpharma.com/tmsapi/api/",
  // BASEURL: "https://portal.sunwardpharma.com/#/tasktreeview?",
  // FILE_PATH_URL: "https://portal.sunwardpharma.com/AppUpload",
  // FOLDERURL: "https://portal.sunwardpharma.com/#/publicfoldertreeview?",
  // DOWNURL: "https://portal.sunwardpharma.com/#/downloadFile?",
  // APPUpload: "https://portal.sunwardpharma.com/AppUpload/",
  // WORKORDERURL: "https://portal.sunwardpharma.com/#/workorder/workorderReport?",
  // GLOSSARYURL: "https://portal.sunwardpharma.com/#/Glossaryview?",
  // ATTACH_DOWNURL: "https://portal.sunwardpharma.com/#/AttachmentDownload?",
  // FILEPROFILETYPE_DOWNURL:
  //   "https://portal.sunwardpharma.com/#/fileProfileTypeDownload?",
  // UNREADSUBJECT: "https://portal.sunwardpharma.com/#/unReadSubject?",
  // DYNAMICFORM_PROFILEURL: "https://portal.sunwardpharma.com:2288",
  // PERSONAL_CALENDER_URL: "https://portal.sunwardpharma.com:2045",
  // PRODUCTION_ACTIVITY_APP: "https://portal.sunwardpharma.com:2286/",
  // TOPICCHAT_URL: "https://portal.sunwardpharma.com:2284/", 
  // DOCUMENT_VIWER_URL:"https://portal.sunwardpharma.com/FileViewer/", 
});
