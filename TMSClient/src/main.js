// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import Vuetify from 'vuetify';
import router from './router';
import 'font-awesome/css/font-awesome.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css' // Ensure you are using css-loader
import './theme/default.styl';
import VeeValidate from 'vee-validate';
import GlobalMessage from "@/constant";
import '../node_modules/timeline-vuejs/dist/timeline-vuejs.css'
import VueCoreVideoPlayer from 'vue-core-video-player'

Vue.use(VueCoreVideoPlayer)

import colors from 'vuetify/es5/util/colors';
import 'vuetify/dist/vuetify.min.css'; // Ensure you are using css-loader
//import 'vuetify/src/stylus/main' // Ensure you are using stylus-loader
import Truncate from 'lodash.truncate';
import store from './store';

//import 'material-design-icons-iconfont/dist/material-design-icons.css' // Ensure you are using css-loader
import '@mdi/font/css/materialdesignicons.css'; // Ensure you are using css-loader

// global components
import GlobalComponents from './globalComponents';
import moment from 'moment';
Vue.use(moment);
import VueHotkey from 'v-hotkey';
Vue.use(VueHotkey);

import VueCookies from 'vue-cookies';
Vue.use(VueCookies);

import VueGoogleCharts from 'vue-google-charts';
Vue.use(VueGoogleCharts);
import VueClipboard from 'vue-clipboard2';
Vue.use(VueClipboard);
Vue.prototype.$eventHub = new Vue(); // Global event bus

import {
  DropDownListPlugin
} from "@syncfusion/ej2-vue-dropdowns";
Vue.use(DropDownListPlugin);

import {
  TreeGridPlugin
} from "@syncfusion/ej2-vue-treegrid";
Vue.use(TreeGridPlugin);
window.$ = require('jquery')
window.JQuery = require('jquery')
import VueMce from './vue-mce'

if (window) {
  window.VueMce = VueMce

  if (window.Vue) {
    window.Vue.use(VueMce)
  }
}

Vue.use(VueMce);

import VueQrcode from '@chenfengyuan/vue-qrcode';

Vue.component(VueQrcode.name, VueQrcode);

import VueDragTree from 'vue-drag-tree';
import 'vue-drag-tree/dist/vue-drag-tree.min.css';
Vue.use(VueDragTree);

import VueSignaturePad from 'vue-signature-pad';
Vue.use(VueSignaturePad);

import VueSignalR from '@latelier/vue-signalr'
Vue.use(VueSignalR, GlobalMessage.NotificationHub);

import VueNativeNotification from 'vue-native-notification'
Vue.use(VueNativeNotification, {
  // Automatic permission request before
  // showing notification (default: true)
  requestOnNotify: true
})

import VueHtmlToPaper from 'vue-html-to-paper';

import JsonExcel from 'vue-json-excel'
Vue.component('downloadExcel', JsonExcel)
import mixins from 'vuetify-multiple-draggable-dialogs';

Vue.mixin(mixins);
const options = {
  name: '_blank',
  specs: [
    'fullscreen=yes',
    'titlebar=yes',
    'scrollbars=yes'
  ],
  styles: [
    // 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
    // 'https://unpkg.com/kidlat-css/css/kidlat.css'
    'https://cdnjs.cloudflare.com/ajax/libs/vuetify/1.5.6/vuetify.min.css',
    'https://portal.sunwardpharma.com:2025/static/print.css',
    //'https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css'
  ]
};

Vue.use(VueHtmlToPaper, options);
Vue.filter("formatNumber", function (value) {
  return numeral(value).format("0,0"); // displaying other groupings/separators is possible, look at the docs
});
Vue.filter('formatDate', function (value) {
  if (value) {
    return moment(value).format('DD-MMM-YYYY');
  }
});
Vue.filter('monthYear', function (value) {
  if (value) {
    return moment(value).format('MMM-YYYY');
  }
});
Vue.filter('formatDateTime', function (value) {
  if (value) {
    return moment(value).format('DD-MMM-YYYY hh:mm A');
  }
});
Vue.filter('formatTime', function (value) {
  if (value) {
    return moment(value).format('hh:mm A');
  }
});
Vue.filter("formatDateView", function (value) {
  if (value) {
    return moment(value).format("DD/MM/YYYY");
  }
});
Vue.filter('format_date', function fmt(date = '') {
  let date_parts = date.match(/^(\d{4})-(\d+)-(\d+)(.*)$/);
  let year;
  let month;
  let day;

  if (date_parts) {
    year = ('00' + date_parts[1]).slice(-4);
    month = ('0' + date_parts[2]).slice(-2);
    day = ('0' + date_parts[3]).slice(-2);

    return month + '-' + day + '-' + year + date_parts[4];
  } else {
    return date;
  }
});

Vue.config.productionTip = false;

Vue.filter('uppercase', function (value) {
  return value.toUpperCase();
});
// Helpers
// Global filters
Vue.filter('truncate', Truncate);
Vue.use(VeeValidate, {
  fieldsBagName: 'formFields'
});
Vue.use(GlobalComponents);
Vue.use(colors);
Vue.use(Vuetify, {
  // theme: {
  //   primary: colors.indigo.base, // #E53935
  //   secondary: colors.indigo.lighten4, // #FFCDD2
  //   accent: colors.indigo.base // #3F51B5
  // },
  iconfont: 'mdi' || 'fa4',


  options: {
    themeVariations: ['primary', 'secondary', 'accent'],
    extra: {
      mainToolbar: {
        color: 'primary',
      },
      sideToolbar: {},
      sideNav: 'primary',
      mainNav: 'primary lighten-1',
      bodyBg: '',
    }
  }
});
// Bootstrap application components



/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {
    App
  },

  template: '<App/>',

  created() {
    this.$socket.start({
      log: false // Active only in development for debugging.
    });
  },

});