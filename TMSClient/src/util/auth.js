// import Cookies from 'js-cookie'

// const TokenKey = 'Admin-Token'
// const userName = 'User-Name'
// const userID = 'USER-ID'
// const theme = 'USER-Theme'
// export function getToken() {
//     return Cookies.get(TokenKey)
// }
// export function getuserName() {
//     return Cookies.get(userName)
// }
// export function getuserID() {
//     return Cookies.get(userID)
// }
// export function setToken(token) {
//     return Cookies.set(TokenKey, token)
// }
// export function setuserName(token) {
//     return Cookies.set(userName, token)
// }
// export function setuserID(token) {
//     return Cookies.set(userID, token)
// }
// export function removeToken() {
//     return Cookies.remove(TokenKey)
// }
// export function setTheme(color) {
//     return Cookies.set(theme, color)
// }
// export function getTheme() {
//     return Cookies.get(theme)
// }

import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'
const userName = 'User-Name'
const userID = 'USER-ID'
const theme = 'USER-Theme'
const pageSize = 'USER-PAGE'
const userCode='User-Code'
const sessionId='Session-Code'
export function getToken() {
    //console.log(store);
    return localStorage.getItem(TokenKey)
}
export function getSessionId() {
    return localStorage.getItem(sessionId)
}
export function getuserName() {
    return localStorage.getItem(userName)
}
export function getuserID() {
    return localStorage.getItem(userID)
}
export function getuserCode() {
    return localStorage.getItem(userCode)
}
export function setToken(token) {
    return localStorage.setItem(TokenKey, token)
}
export function setSessionId(token) {
    return localStorage.setItem(sessionId, token)
}
export function setuserName(uName) {
    return localStorage.setItem(userName, uName)
}
export function setuserID(token) {
    return localStorage.setItem(userID, token)
}
export function setuserCode(uCode) {
    return localStorage.setItem(userCode, uCode)
}
export function removeToken() {
    return localStorage.removeItem(TokenKey)
}
export function setTheme(color) {
    return localStorage.setItem(theme, color)
}
export function getTheme() {
    return localStorage.getItem(theme)
}
export function setPage(page) {
    return localStorage.setItem(pageSize, page)
}
export function getPage() {
    return localStorage.getItem(pageSize)
}