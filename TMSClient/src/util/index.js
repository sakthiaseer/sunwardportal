// export function camel (str) {
//   const camel = (str || '').replace(/-([^-])/g, g => g[1].toUpperCase());

//   return capitalize(camel);
// }

// export function camelActual (str) {
//   return (str || '').replace(/-(\w)/g, (_, c) => (c ? c.toUpperCase() : ''));
// }

// export function kebab (str) {
//   return (str || '').replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
// }

// export function capitalize (str) {
//   str = str || '';

//   return `${str.substr(0, 1).toUpperCase()}${str.slice(1)}`;
// }

// export function findProduct (store, id) {
//   return store.state.store.products.find(p => p.id === id);
// }

// export function isOnSale (variants) {
//   return variants.some(variant => {
//     return parseFloat(variant.price) < parseFloat(variant.compareAtPrice);
//   });
// }

// export function randomNumber (min, max) {
//   return Math.floor(Math.random() * max) + min;
// }

// export function randomString (length = 5) {
//   let text = '';
//   const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

//   for (let i = 0; i < length; i++) {
//     text += possible.charAt(Math.floor(Math.random() * possible.length));
//   }

//   return text;
// }
const randomElement = (arr = []) => {
  return arr[Math.floor(Math.random() * arr.length)];
};

const kebab = (str) => {
  return (str || "").replace(/([a-z])([A-Z])/g, "$1-$2").toLowerCase();
};

const toggleFullScreen = () => {
  let doc = window.document;
  let docEl = doc.documentElement;

  let requestFullScreen =
    docEl.requestFullscreen ||
    docEl.mozRequestFullScreen ||
    docEl.webkitRequestFullScreen ||
    docEl.msRequestFullscreen;
  let cancelFullScreen =
    doc.exitFullscreen ||
    doc.mozCancelFullScreen ||
    doc.webkitExitFullscreen ||
    doc.msExitFullscreen;

  if (
    !doc.fullscreenElement &&
    !doc.mozFullScreenElement &&
    !doc.webkitFullscreenElement &&
    !doc.msFullscreenElement
  ) {
    requestFullScreen.call(docEl);
  } else {
    cancelFullScreen.call(doc);
  }
};
const tinyconfig = (height = 300, isFullScreen = true) => {
  var toolbarSetup =
    "formatselect fontsizeselect | bold italic underline strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat undo redo | image table fullscreen charmap fontselect print";
  return {
    branding: false,
    height: height != "" ? height : 300,
    statusbar: false,
    forced_root_block: "div",
    //content_style: ".mce-content-body {font-size:16px;font-family:Arial;}",
    menubar: "file tools table format view insert edit",
    fontsize_formats:
      "8px 10px 12px 14px 16px 18px 20px 22px 24px 26px 39px 34px 38px 42px 48px",
    plugins: [
      "autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "template textcolor colorpicker textpattern imagetools toc help emoticons hr codesample",
    ],
    external_plugins: {
      advlist: "/static/tinymce/advlist/plugin.js",
      powerpaste: "/static/tinymce/powerpaste/plugin.js",
      nanospell: "/static/tinymce/nanospell/plugin.js",
    },
    nanospell_server: "asp",
    toolbar1:
      isFullScreen == true
        ? toolbarSetup
        : toolbarSetup.replace("fullscreen", ""),
    // paste_data_images :true,
    powerpaste_allow_local_images: true,
    powerpaste_word_import: "prompt",
    powerpaste_html_import: "prompt",
    images_upload_url: "postAcceptor.php",
    document_base_url : 'http://www.example.com/path1/',
    images_upload_handler: function(blobInfo, success, failure) {
      setTimeout(function() {
        var files = blobInfo.blob();
        var reader = new FileReader();
        // Set the reader to insert images when they are loaded.
        reader.onload = function(e) {
          var result = e.target.result;
          success(result);
        };
        // Read image as base64.
        reader.readAsDataURL(files);
        //  }
      }, 2000);
    },
    init_instance_callback : function(editor) {
      editor.setContent('custom');
    },
    setup: function(ed) {
      ed.on("init", function() {
        //ed.setContent('custom');
        this.getDoc().body.style.fontSize = "16px";
        this.getDoc().body.style.fontFamily = "Arial";
        // this.editorInstance.setContent("custom");
      });
    },
  };
};
export default {
  randomElement,
  toggleFullScreen,
  kebab,
  tinyconfig,
};
