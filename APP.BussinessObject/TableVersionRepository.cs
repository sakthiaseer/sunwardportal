﻿using APP.DataAccess.Models;
using APP.EntityModel;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP.BussinessObject
{
    public class TableVersionRepository
    {
        private readonly CRT_TMSContext _context;
        private readonly IMapper _mapper;
        public TableVersionRepository(CRT_TMSContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<TableDataVersionInfoModel<T>> Get<T>(long Id)
        {

            var results = new TableDataVersionInfoModel<T>();

            var data = _context.TableDataVersionInfo
                .Include(a => a.AddedByUser)
                .FirstOrDefault(f => f.VersionTableId == Id);


            results = new TableDataVersionInfoModel<T>
            {
                AddedByUserID = data.AddedByUserId,
                AddedByUserId = data.AddedByUserId,
                AddedByUser = data.AddedByUser.UserName,
                AddedDate = data.AddedDate,
                JsonData = data.JsonData,
                SessionId = data.SessionId,
                TableName = data.TableName,
                ReferenceInfo = data.ReferenceInfo,
                PrimaryKey = data.PrimaryKey,
                ScreenId = data.ScreenId,
                VersionNo = data.VersionNo,
                VersionTableId = data.VersionTableId,
                VersionData = System.Text.Json.JsonSerializer.Deserialize<T>(data.JsonData),
            };

            return results;
        }
        public async Task<IEnumerable<TableDataVersionInfoModel<T>>> GetList<T>(string sessionID)
        {
            var guid = Guid.Parse(sessionID);
            var results = new List<TableDataVersionInfoModel<T>>();

            var data = await _context.TableDataVersionInfo
                .Include(a => a.AddedByUser)
                .Where(f => f.SessionId == guid).AsNoTracking().ToListAsync();

            data.ForEach(f =>
            {
                results.Add(new TableDataVersionInfoModel<T>
                {
                    AddedByUserID = f.AddedByUserId,
                    AddedByUserId = f.AddedByUserId,
                    AddedByUser = f.AddedByUser.UserName,
                    AddedDate = f.AddedDate,
                    JsonData = f.JsonData,
                    SessionId = f.SessionId,
                    TableName = f.TableName,
                    ReferenceInfo = f.ReferenceInfo,
                    PrimaryKey = f.PrimaryKey,
                    ScreenId = f.ScreenId,
                    VersionNo = f.VersionNo,
                    VersionTableId = f.VersionTableId,
                    VersionData = System.Text.Json.JsonSerializer.Deserialize<T>(f.JsonData),
                });
            });

            return results;
        }
        public async Task<TableDataVersionInfoModel<T>> Insert<T>(TableDataVersionInfoModel<T> model)
        {
            var versionNo = 1;
            var existingVersion = _context.TableDataVersionInfo.OrderBy(o => o.VersionTableId).LastOrDefault(f => f.SessionId == model.SessionId);
            if (existingVersion != null)
            {
                versionNo = existingVersion.VersionNo + 1;
            }

            var tableVersion = new TableDataVersionInfo
            {
                AddedByUserId = model.AddedByUserId,
                AddedDate = DateTime.Now,
                JsonData = model.JsonData,
                PrimaryKey = model.PrimaryKey,
                ScreenId = model.ScreenId,
                SessionId = model.SessionId,
                TableName = model.TableName,
                VersionNo = versionNo,
                ReferenceInfo = model.ReferenceInfo,
            };
            _context.TableDataVersionInfo.Add(tableVersion);
            await _context.SaveChangesAsync();
            var versionId = tableVersion.VersionTableId;
            var screenData = _context.ApplicationForm.FirstOrDefault(s => s.ScreenId == model.ScreenId);
            if (screenData != null && screenData.PersonInchargeId != null && model.TableName == "SellingCatalogue")
            {
                var PersonInchargeId = screenData.PersonInchargeId;
                var tempversion = new TempVersion
                {
                    AddedByUserId = model.AddedByUserID,
                    VersionId = versionId,
                    StatusCodeId = model.StatusCodeID.Value,
                    PersonInchargeId = PersonInchargeId,
                    AddedDate = DateTime.Now,
                };

                _context.TempVersion.Add(tempversion);
                await _context.SaveChangesAsync();
            }
            model.VersionTableId = tableVersion.VersionTableId;
            return model;
        }
    }
}
