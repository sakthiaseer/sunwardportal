﻿using APP.DataAccess.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace APP.BussinessObject
{
    public class SqlBulkUpload
    {
        private readonly CRT_TMSContext _context;
        private readonly IConfiguration _config;       
        public SqlBulkUpload(CRT_TMSContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        #region Bulk Insert

        private IEnumerable<string> GetMapping(string tableName)
        {
            return Enumerable.Intersect(
                GetSchema(tableName),
                GetSchema(tableName),
                StringComparer.Ordinal); // or StringComparer.OrdinalIgnoreCase
        }

        private IEnumerable<string> GetSchema(string tableName)
        {
            using (SqlConnection connection = new SqlConnection(_config.GetValue<string>("ConnectionStrings:TaskDatabase")))
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandText = "sp_Columns";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@table_name", SqlDbType.NVarChar, 384).Value = tableName;                

                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        yield return (string)reader["column_name"];
                    }
                }
            }
        }

        public async Task<int> BulkInsertAsync<T>(List<T> list, string TableName, List<string> includedColumns = null)
        {
            DataTable dt = new DataTable("MyTable");
            dt = ConvertToDataTable(list, includedColumns);
            using (SqlConnection connection = new SqlConnection(_config.GetValue<string>("ConnectionStrings:TaskDatabase")))
            {
                SqlBulkCopy bulkCopy = new SqlBulkCopy(connection);
                try
                {
                    foreach (string columnName in GetMapping(TableName))
                    {
                        bulkCopy.ColumnMappings.Add(new SqlBulkCopyColumnMapping(columnName, columnName));
                    }

                    connection.Open();

                    bulkCopy.BulkCopyTimeout = 660;
                    bulkCopy.DestinationTableName = TableName;
                    bulkCopy.WriteToServer(dt);
                    await connection.CloseAsync();
                }
                catch (Exception exception)
                {
                    // loop through all inner exceptions to see if any relate to a constraint failure
                    bool dataExceptionFound = false;
                    Exception tmpException = exception;
                    while (tmpException != null)
                    {
                        if (tmpException is SqlException
                           && tmpException.Message.Contains("constraint"))
                        {
                            dataExceptionFound = true;
                            break;
                        }
                        tmpException = tmpException.InnerException;
                    }

                    if (dataExceptionFound)
                    {
                        // call the helper method to document the errors and invalid data
                        string errorMessage = GetBulkCopyFailedData(
                           connection.ConnectionString,
                           bulkCopy.DestinationTableName,
                           dt.CreateDataReader());
                        throw new Exception(errorMessage, exception);
                    }
                }
            }
            // reset
            dt.Clear();
            return dt.Rows.Count;
        }
  

        public string BulkInsert<T>(List<T> list, string TableName, List<string> includedColumns = null)
        {
            DataTable dt = new DataTable("MyTable");
            dt = ConvertToDataTable(list, includedColumns);
            using (SqlConnection connection = new SqlConnection(_config.GetValue<string>("ConnectionStrings:TaskDatabase")))
            {
                SqlBulkCopy bulkCopy = new SqlBulkCopy(connection);
                try
                {
                    foreach (string columnName in GetMapping(TableName))
                    {
                        bulkCopy.ColumnMappings.Add(new SqlBulkCopyColumnMapping(columnName, columnName));
                    }

                    connection.Open();

                    bulkCopy.BulkCopyTimeout = 660;
                    bulkCopy.DestinationTableName = TableName;
                    bulkCopy.WriteToServer(dt);
                    connection.Close();
                }
                catch (Exception exception)
                {
                    // loop through all inner exceptions to see if any relate to a constraint failure
                    bool dataExceptionFound = false;
                    Exception tmpException = exception;
                    while (tmpException != null)
                    {
                        if (tmpException is SqlException
                           && tmpException.Message.Contains("constraint"))
                        {
                            dataExceptionFound = true;
                            break;
                        }
                        tmpException = tmpException.InnerException;
                    }

                    if (dataExceptionFound)
                    {
                        // call the helper method to document the errors and invalid data
                        string errorMessage = GetBulkCopyFailedData(
                           connection.ConnectionString,
                           bulkCopy.DestinationTableName,
                           dt.CreateDataReader());
                        connection.Close();
                        throw new Exception(errorMessage, exception);
                    }
                }
            }
            // reset
            dt.Clear();
            return dt.TableName;
        }


        public static string GetBulkCopyFailedData(
   string connectionString,
   string tableName,
   IDataReader dataReader)
        {
            StringBuilder errorMessage = new StringBuilder("Bulk copy failures:" + Environment.NewLine);
            SqlConnection connection = null;
            SqlTransaction transaction = null;
            SqlBulkCopy bulkCopy = null;
            DataTable tmpDataTable = new DataTable();

            try
            {
                connection = new SqlConnection(connectionString);
                connection.Open();
                transaction = connection.BeginTransaction();
                bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.CheckConstraints, transaction);
                bulkCopy.DestinationTableName = tableName;

                // create a datatable with the layout of the data.
                DataTable dataSchema = dataReader.GetSchemaTable();
                foreach (DataRow row in dataSchema.Rows)
                {
                    tmpDataTable.Columns.Add(new DataColumn(
                       row["ColumnName"].ToString(),
                       (Type)row["DataType"]));
                }

                // create an object array to hold the data being transferred into tmpDataTable 
                //in the loop below.
                object[] values = new object[dataReader.FieldCount];

                // loop through the source data
                while (dataReader.Read())
                {
                    // clear the temp DataTable from which the single-record bulk copy will be done
                    tmpDataTable.Rows.Clear();

                    // get the data for the current source row
                    dataReader.GetValues(values);

                    // load the values into the temp DataTable
                    tmpDataTable.LoadDataRow(values, true);

                    // perform the bulk copy of the one row
                    try
                    {
                        bulkCopy.WriteToServer(tmpDataTable);
                    }
                    catch (Exception ex)
                    {
                        // an exception was raised with the bulk copy of the current row. 
                        // The row that caused the current exception is the only one in the temp 
                        // DataTable, so document it and add it to the error message.
                        DataRow faultyDataRow = tmpDataTable.Rows[0];
                        errorMessage.AppendFormat("Error: {0}{1}", ex.Message, Environment.NewLine);
                        errorMessage.AppendFormat("Row data: {0}", Environment.NewLine);
                        foreach (DataColumn column in tmpDataTable.Columns)
                        {
                            errorMessage.AppendFormat(
                               "\tColumn {0} - [{1}]{2}",
                               column.ColumnName,
                               faultyDataRow[column.ColumnName].ToString(),
                               Environment.NewLine);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(
                   "Unable to document SqlBulkCopy errors. See inner exceptions for details.",
                   ex);
            }
            finally
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
            return errorMessage.ToString();
        }

        #endregion

        #region Bulk Delete

        public async Task BulkDeleteAsync<T>(List<T> list, string TableName, string primarkKeyFieldName)
        {
            //await _context.GroupPlaningTicket.BulkDeleteAsync(list);          

            DataTable dt = new DataTable("MyTable");
            dt = ConvertToDataTable(list);
            using (SqlConnection sqlConn = new SqlConnection(_config.GetValue<string>("ConnectionStrings:TaskDatabase")))
            {
                await sqlConn.OpenAsync();

                using (SqlTransaction sqlTran = sqlConn.BeginTransaction())
                {
                    var deleteQuery = $@"
                        DELETE FROM {TableName}
                        OUTPUT Deleted.{primarkKeyFieldName}
                        // NOTE: Be aware that 'Id' column naming depends on your project conventions
                        WHERE {primarkKeyFieldName} IN({string.Join(", ", list)});

                    ";
                    //string deleteQuery = "DELETE FROM " + TableName + " WHERE CompanyId = " + CompanyId; // just delete them all
                    SqlCommand sqlComm = new SqlCommand(deleteQuery, sqlConn, sqlTran);
                    sqlConn.Open();
                    sqlComm.ExecuteNonQuery();
                }
            }
            // reset
            dt.Clear();
        }
        #endregion     

        private string GetSqlDataType(Type type, bool isPrimary = false)
        {
            var sqlType = new StringBuilder();
            var isNullable = false;
            if (Nullable.GetUnderlyingType(type) != null)
            {
                isNullable = true;
                type = Nullable.GetUnderlyingType(type);
            }
            switch (Type.GetTypeCode(type))
            {
                case TypeCode.String:
                    isNullable = true;
                    sqlType.Append("nvarchar(MAX)");
                    break;
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Int16:
                    sqlType.Append("int");
                    break;
                case TypeCode.Boolean:
                    sqlType.Append("bit");
                    break;
                case TypeCode.DateTime:
                    sqlType.Append("datetime");
                    break;
                case TypeCode.Decimal:
                case TypeCode.Double:
                    sqlType.Append("decimal");
                    break;
            }
            if (!isNullable || isPrimary)
            {
                sqlType.Append(" NOT NULL");
            }
            return sqlType.ToString();
        }

        public bool BulkUpdateData<T>(List<T> rows, string primaryKeyFieldName, List<string> excludedColumns = null, string tableName = null)
        {
            var template = rows.FirstOrDefault();
            string tn = tableName ?? template.GetType().Name + "s";
            int updated = 0;
            using (SqlConnection con = new SqlConnection(_config.GetValue<string>("ConnectionStrings:TaskDatabase")))
            {
                using (SqlCommand command = new SqlCommand("", con))
                {
                    using (SqlBulkCopy sbc = new SqlBulkCopy(con))
                    {
                        var dt = new DataTable();
                        var columns = template.GetType().GetProperties();
                        var colNames = new List<string>();
                        string keyName = "";
                        var setStatement = new StringBuilder();
                        int rowNum = 0;
                        foreach (var row in rows)
                        {
                            dt.Rows.Add();
                            int colNum = 0;
                            foreach (var col in columns)
                            {
                                if (excludedColumns != null && excludedColumns.Any(x => x == col.Name))
                                    continue;


                                var attributes = row.GetType().GetProperty(col.Name).GetCustomAttributes(false);
                                bool isPrimary = col.Name == primaryKeyFieldName;// IsPrimaryKey(attributes);
                                var value = row.GetType().GetProperty(col.Name).GetValue(row);

                                if (rowNum == 0)
                                {
                                    colNames.Add($"{col.Name} {GetSqlDataType(col.PropertyType, isPrimary)}");
                                    dt.Columns.Add(new DataColumn(col.Name, Nullable.GetUnderlyingType(col.PropertyType) ?? col.PropertyType));
                                    if (!isPrimary)
                                    {
                                        setStatement.Append($" ME.{col.Name} = T.{col.Name},");
                                    }

                                }
                                if (isPrimary)
                                {
                                    keyName = col.Name;
                                    if (value == null)
                                    {
                                        throw new Exception("Trying to update a row whose primary key is null; use insert instead.");
                                    }
                                }
                                dt.Rows[rowNum][colNum] = value ?? DBNull.Value;
                                colNum++;
                            }
                            rowNum++;
                        }
                        setStatement.Length--;
                        try
                        {
                            con.Open();

                            command.CommandText = $"CREATE TABLE [dbo].[#TmpTable]({String.Join(",", colNames)})";
                            //command.CommandTimeout = CmdTimeOut;
                            command.ExecuteNonQuery();

                            sbc.DestinationTableName = "[dbo].[#TmpTable]";
                            sbc.BulkCopyTimeout = 400;
                            sbc.WriteToServer(dt);
                            sbc.Close();

                            command.CommandTimeout = 400;
                            command.CommandText = $"UPDATE ME SET {setStatement} FROM {tn} as ME INNER JOIN #TmpTable AS T on ME.{keyName} = T.{keyName}; DROP TABLE #TmpTable;";
                            updated = command.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            if (con.State != ConnectionState.Closed)
                            {
                                sbc.Close();
                                con.Close();
                            }
                            //well logging to sql might not work... we could try... but no.
                            //So Lets write to a local file.
                            throw ex;
                        }
                    }
                }
            }
            return (updated > 0) ? true : false;
        }


        #region Common
        public static DataTable ConvertToDataTable<T>(IList<T> data, List<string> includedColumns = null)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                //if (includedColumns == null || includedColumns.Any(x => x == prop.Name))
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    //if (includedColumns == null || includedColumns.Any(x => x == prop.Name))
                        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }
                table.Rows.Add(row);
            }
            return table;
        }
        #endregion




        //public int BulkInsert1<T>(IEnumerable<T> list, string tableName = null) where T : class
        //{
        //    tableName = GetTableName<T>(tableName);
        //    DataTable table = ConvertToDataTable(list, tableName);
        //    using (var bulkCopy = new SqlBulkCopy(_config.GetValue<string>("ConnectionStrings:TaskDatabase")))
        //    {
        //        bulkCopy.DestinationTableName = tableName;
        //        bulkCopy.BatchSize = list.Count();
        //        bulkCopy.BulkCopyTimeout = 200;
        //        bulkCopy.WriteToServer(table);
        //        return table.Rows.Count;
        //    }
        //}

        //private string GetColumns(DataTable dt)
        //{
        //    string output = string.Empty;
        //    foreach (DataColumn col in dt.Columns)
        //        output += col.ColumnName + " " + ConvertToSqlType[col.DataType.ToString()] + ", ";

        //    return output.Substring(0, output.Length - 2);  // remove the last ','
        //}

        private DataTable ConvertToDataTable<T>(IEnumerable<T> list, string tableName) where T : class
        {
            var table = new DataTable(tableName);
            var props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                       //Dirty hack to make sure we only have system data types 
                                       //i.e. filter out the relationships/collections
                                       .Where(x => x.CanRead && x.CanWrite && x.PropertyType.Namespace != null && x.PropertyType.Namespace.Equals("System"))
                                       .Where(y => y.CustomAttributes.Any(z => z.AttributeType.Name == "DataMemberAttribute"))
                                       .ToList();

            foreach (var x in props)
            {
                // bulkCopy.ColumnMappings.Add(x.Name, x.Name);
                var type = Nullable.GetUnderlyingType(x.PropertyType) ?? x.PropertyType;
                table.Columns.Add(x.Name, type);
                // Debug.Print("[" + x.Name + "]   " + type.ToString());
            }

            var values = new object[props.Count()];
            foreach (var item in list)
            {
                for (var i = 0; i < values.Length; i++)
                    values[i] = props[i].GetValue(item);

                table.Rows.Add(values);
            }

            return table;
        }

        private static string GetTableName<T>(string tableName) where T : class
        {
            TableAttribute tableAttribute = (TableAttribute)System.Attribute.GetCustomAttribute(typeof(T), typeof(TableAttribute));
            if (tableAttribute != null && tableName == null)
                tableName = tableAttribute.Name;

            if (string.IsNullOrWhiteSpace(tableName))
                throw new Exception("The Table name was not found in BulkInsert.");

            return tableName;
        }



    }
}
