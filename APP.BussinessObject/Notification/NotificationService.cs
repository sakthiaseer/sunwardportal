﻿using APP.DataAccess.Models;
using APP.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace APP.BussinessObject
{
    public class NotificationService
    {
        private readonly CRT_TMSContext _context;

        public NotificationService(CRT_TMSContext context)
        {
            _context = context;
        }

        public void GetNotificationHandlerForRecurrence()
        {
            var notificationHandlerItems = _context.NotificationHandler.Where(w => w.NextNotifyDate.Value.Date == DateTime.Now.Date && w.NotifyStatusId == 1).ToList();
            if (notificationHandlerItems != null)
            {
                notificationHandlerItems.ForEach(h =>
                {
                    NotificationHandler NotificationHandler = new NotificationHandler();
                    NotificationHandler.NextNotifyDate = h.NextNotifyDate.Value.Date.AddDays((int)h.AddedDays);
                    NotificationHandler.SessionId = h.SessionId;
                    NotificationHandler.NotifyStatusId = h.NotifyStatusId;
                    NotificationHandler.AddedDays = h.AddedDays;
                    NotificationHandler.Title = h.Title;
                    NotificationHandler.Message = h.Message;
                    NotificationHandler.ScreenId = h.ScreenId;
                    NotificationHandler.NotifyTo = h.NotifyTo;
                    _context.NotificationHandler.Add(NotificationHandler);
                    _context.SaveChanges();
                });
            }
        }

        public NotificationHandlerModel Create(NotificationHandlerModel value)
        {
            NotificationHandler notificationHandler = new NotificationHandler
            {
                SessionId = value.SessionId,
                NotifyStatusId = value.NotifyStatusId,
                Title = value.Title,
                Message = value.Message,
                NotifyTo = value.NotifyTo,
                ScreenId = value.ScreenId,
            };

            _context.NotificationHandler.Add(notificationHandler);
            _context.SaveChanges();
            value.NotificationHandlerId = notificationHandler.NotificationHandlerId;
            return value;
        }

        public NotificationHandlerModel Update(NotificationHandlerModel value)
        {

            NotificationHandler notificationHandler = new NotificationHandler
            {
                SessionId = value.SessionId,
                NotifyStatusId = value.NotifyStatusId,
                Title = value.Title,
                Message = value.Message,
                NotifyTo = value.NotifyTo,
                ScreenId = value.ScreenId,
            };

            _context.NotificationHandler.Add(notificationHandler);
            _context.SaveChanges();
            value.NotificationHandlerId = notificationHandler.NotificationHandlerId;
            return value;
        }

        public void Delete(int id)
        {
            var notificationHandler = _context.NotificationHandler.FirstOrDefault(n => n.NotificationHandlerId == id);
            _context.NotificationHandler.Remove(notificationHandler);
            _context.SaveChanges();
        }

    }
}
