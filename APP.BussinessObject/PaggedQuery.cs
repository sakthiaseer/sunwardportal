﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;

namespace APP.BussinessObject
{
    public static class PaggedQuery
    {
        public static async Task<PagedResult<T>> GetPaged<T>(this IQueryable<T> query,
                                         int page, int pageSize, string sortCol, string sortby, List<string> filterFields, string searchText, long? primaryId = null, string primaryColumn = "") where T : class
        {

            string sortorder = "AddedDate desc";
            if (!string.IsNullOrEmpty(sortCol) && !string.IsNullOrEmpty(sortby))
            {
                sortorder = sortCol + " " + sortby;
            }


            var whereCondition = "";

            filterFields.ForEach(f =>
            {
                whereCondition = !string.IsNullOrEmpty(whereCondition) ? whereCondition + " or " + f + ".Contains(@0)" : f + ".Contains(@0)";
            });

            if (primaryId > 0)
            {
                var filter = primaryColumn + "= @0";
                query = query.Where(filter, primaryId);
            }

            var result = new PagedResult<T>
            {
                CurrentPage = page,
                PageSize = pageSize,
            };


            var skip = (page - 1) * pageSize;
            if (string.IsNullOrEmpty(searchText))
            {
                result.RowCount = query.Count();
                result.Results = await query.OrderBy(sortorder).Skip(skip).Take(pageSize).AsNoTracking().ToListAsync();

            }
            else
            {
                result.RowCount = query.Where(whereCondition, searchText).Count();
                result.Results = await query.Where(whereCondition, searchText.Trim()).OrderBy(sortorder).Skip(skip).Take(pageSize).AsNoTracking().ToListAsync();

            }
            var pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);


            return result;
        }

        public static async Task<PagedResult<T>> GetTaskListPaged<T>(this IQueryable<T> query,
                                        int page, int pageSize, string sortCol, string sortby, List<string> filterFields, string searchText, List<long> TaskIds) where T : class
        {

            string sortorder = "AddedDate desc";
            if (!string.IsNullOrEmpty(sortCol) && !string.IsNullOrEmpty(sortby))
            {
                sortorder = sortCol + " " + sortby;
            }

            var whereCondition = "";
            filterFields.ForEach(f =>
            {
                whereCondition = !string.IsNullOrEmpty(whereCondition) ? whereCondition + " or " + f + ".Contains(@0)" : f + ".Contains(@0)";
            });
            var skip = (page - 1) * pageSize;

            if (TaskIds.Any())
            {
                var filter = "@0.Contains(TaskId)";
                query = query.Where(filter, TaskIds);
            }

            var result = new PagedResult<T>
            {
                CurrentPage = page,
                PageSize = pageSize,
            };

            if (string.IsNullOrEmpty(searchText))
            {
                result.RowCount = query.Count();
                result.QueryResult = query.OrderBy(sortorder).Skip(skip).Take(pageSize)
                    .AsNoTracking().AsQueryable();
            }
            else
            {
                result.RowCount = query.Where(whereCondition, searchText).Count();
                result.QueryResult = query.Where(whereCondition, searchText).OrderBy(sortorder).Skip(skip).Take(pageSize).AsNoTracking().AsQueryable();
            }

            var pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            return result;
        }

        public static async Task<PagedResult<T>> GetWorkOrderPaged<T>(this IQueryable<T> query,
                                       int page, int pageSize, string sortCol, string sortby, List<string> filterFields, string searchText, List<long> workorderIds) where T : class
        {

            string sortorder = "AddedDate desc";
            if (!string.IsNullOrEmpty(sortCol) && !string.IsNullOrEmpty(sortby))
            {
                sortorder = sortCol + " " + sortby;
            }

            var whereCondition = "";
            filterFields.ForEach(f =>
            {
                whereCondition = !string.IsNullOrEmpty(whereCondition) ? whereCondition + " or " + f + ".Contains(@0)" : f + ".Contains(@0)";
            });
            var skip = (page - 1) * pageSize;

            if (workorderIds.Any())
            {
                var filter = "@0.Contains(WorkOrderID)";
                query = query.Where(filter, workorderIds);
            }

            var result = new PagedResult<T>
            {
                CurrentPage = page,
                PageSize = pageSize,
            };

            if (string.IsNullOrEmpty(searchText))
            {
                result.RowCount = query.Count();
                result.QueryResult = query.OrderBy(sortorder).Skip(skip).Take(pageSize)
                    .AsNoTracking().AsQueryable();
            }
            else
            {
                result.RowCount = query.Where(whereCondition, searchText).Count();
                result.QueryResult = query.Where(whereCondition, searchText).OrderBy(sortorder).Skip(skip).Take(pageSize).AsNoTracking().AsQueryable();
            }

            var pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            return result;
        }

        public static async Task<PagedResult<T>> GetRecordVariationPaged<T>(this IQueryable<T> query,
                                      int page, int pageSize, string sortCol, string sortby, List<string> filterFields, string searchText, Dictionary<string, List<long>> applicationMasterDetailIds) where T : class
        {

            string sortorder = "AddedDate desc";
            if (!string.IsNullOrEmpty(sortCol) && !string.IsNullOrEmpty(sortby))
            {
                sortorder = sortCol + " " + sortby;
            }

            var whereCondition = "";
            filterFields.ForEach(f =>
            {
                whereCondition = !string.IsNullOrEmpty(whereCondition) ? whereCondition + " or " + f + ".Contains(@0)" : f + ".Contains(@0)";
            });
            var skip = (page - 1) * pageSize;

            foreach (var item in applicationMasterDetailIds)
            {
                var filter = "@0.Contains(" + item.Key + ")";
                query = query.Where(filter, item.Value);
            }
            var result = new PagedResult<T>
            {
                CurrentPage = page,
                PageSize = pageSize,
            };

            if (string.IsNullOrEmpty(searchText))
            {
                result.RowCount = query.Count();
                result.QueryResult = query.OrderBy(sortorder).Skip(skip).Take(pageSize)
                    .AsNoTracking().AsQueryable();
            }
            else
            {
                result.RowCount = query.Where(whereCondition, searchText).Count();
                result.QueryResult = query.Where(whereCondition, searchText).OrderBy(sortorder).Skip(skip).Take(pageSize).AsNoTracking().AsQueryable();
            }

            var pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            return result;
        }
        public static async Task<PagedResult<T>> GetWikiListPaged<T>(this IQueryable<T> query,
                                       int page, int pageSize, string sortCol, string sortby, List<string> filterFields, string searchText, List<long> wikiIds) where T : class
        {

            string sortorder = "AddedDate desc";
            if (!string.IsNullOrEmpty(sortCol) && !string.IsNullOrEmpty(sortby))
            {
                sortorder = sortCol + " " + sortby;
            }

            var whereCondition = "";
            filterFields.ForEach(f =>
            {
                whereCondition = !string.IsNullOrEmpty(whereCondition) ? whereCondition + " or " + f + ".Contains(@0)" : f + ".Contains(@0)";
            });
            var skip = (page - 1) * pageSize;

            if (wikiIds.Any())
            {
                var filter = "@0.Contains(ApplicationWikiId)";
                query = query.Where(filter, wikiIds);
            }

            var result = new PagedResult<T>
            {
                CurrentPage = page,
                PageSize = pageSize,
            };

            if (string.IsNullOrEmpty(searchText))
            {
                result.RowCount = query.Count();
                result.QueryResult = query.OrderBy(sortorder).Skip(skip).Take(pageSize)
                    .AsNoTracking().AsQueryable();
            }
            else
            {
                result.RowCount = query.Where(whereCondition, searchText).Count();
                result.QueryResult = query.Where(whereCondition, searchText).OrderBy(sortorder).Skip(skip).Take(pageSize).AsNoTracking().AsQueryable();
            }

            var pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            return result;
        }

        public static async Task<PagedResult<T>> GetClassificationPaged<T>(this IQueryable<T> query,
                                        int page, int pageSize, string sortCol, string sortby, List<string> filterFields, string searchText, string ProfileReferenceNo, bool IsHeader, string NavType = "") where T : class
        {

            string sortorder = "AddedDate desc";
            if (!string.IsNullOrEmpty(sortCol) && !string.IsNullOrEmpty(sortby))
            {
                sortorder = sortCol + " " + sortby;
            }

            var whereCondition = "";
            if (!string.IsNullOrEmpty(ProfileReferenceNo))
            {
                if (!string.IsNullOrEmpty(NavType))
                {
                    if (IsHeader)
                    {
                        query = query.Where("@0.Contains(LinkProfileRefNo)", ProfileReferenceNo);
                    }
                    else
                    {
                        query = query.Where("@0.Contains(MasterProfileRefNo)", ProfileReferenceNo);
                    }
                }
                else
                {
                    if (IsHeader)
                    {
                        query = query.Where("@0.Contains(LinkProfileReferenceNo)", ProfileReferenceNo);
                    }
                    else
                    {
                        query = query.Where("@0.Contains(MasterProfileReferenceNo)", ProfileReferenceNo);
                    }
                }
            }
            filterFields.ForEach(f =>
            {
                whereCondition = !string.IsNullOrEmpty(whereCondition) ? whereCondition + " or " + f + ".Contains(@0)" : f + ".Contains(@0)";
            });
            var skip = (page - 1) * pageSize;
            var result = new PagedResult<T>
            {
                CurrentPage = page,
                PageSize = pageSize,
            };

            if (string.IsNullOrEmpty(searchText))
            {
                result.RowCount = query.Count();
                result.Results = await query.OrderBy(sortorder).Skip(skip).Take(pageSize).AsNoTracking().ToListAsync();
            }
            else
            {
                result.RowCount = query.Where(whereCondition, searchText).Count();
                result.Results = await query.Where(whereCondition, searchText).OrderBy(sortorder).Skip(skip).Take(pageSize).AsNoTracking().ToListAsync();
            }

            var pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            return result;
        }
    }
    public abstract class PagedResultBase
    {
        public int CurrentPage { get; set; }
        public int PageCount { get; set; }
        public int PageSize { get; set; }
        public int RowCount { get; set; }

        public int FirstRowOnPage
        {

            get { return (CurrentPage - 1) * PageSize + 1; }
        }

        public int LastRowOnPage
        {
            get { return Math.Min(CurrentPage * PageSize, RowCount); }
        }

    }

    public class PagedResult<T> : PagedResultBase where T : class
    {
        public IList<T> Results { get; set; }
        public IQueryable<T> QueryResult { get; set; }
        public PagedResult()
        {
            Results = new List<T>();

        }


    }
}
