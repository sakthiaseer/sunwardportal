﻿using APP.DataAccess.Models;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Z.Dapper.Plus;

namespace APP.BussinessObject
{
    public class Dapper
    {
        private readonly CRT_TMSContext _context;
        private readonly IConfiguration _config;
        string licenseName = "837;701-DELTAFOX";
        string licenseKey = "5325383-0330280-9883750-C179866-4192";

        public Dapper(CRT_TMSContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
            DapperPlusManager.AddLicense(licenseName, licenseKey);
        }


        #region Bulk insert  
        public Task DapperBulkInsert<T>(List<T> list)
        {
            using (SqlConnection connection = new SqlConnection(_config.GetValue<string>("ConnectionStrings:TaskDatabase")))
            {
                connection.BulkInsert(list);
            }

            return Task.CompletedTask;
        }
        #endregion

        #region bulk update
        public Task DapperBulkUpdate<T>(List<T> list)
        {
            using (SqlConnection connection = new SqlConnection(_config.GetValue<string>("ConnectionStrings:TaskDatabase")))
            {
                connection.BulkUpdate(list);
            }

            return Task.CompletedTask;
        }        
        #endregion

        #region Bulk Delete
        public Task DapperBulkDelete<T>(List<T> list)
        {
            using (SqlConnection connection = new SqlConnection(_config.GetValue<string>("ConnectionStrings:TaskDatabase")))
            {
                connection.BulkDelete(list);
            }

            return Task.CompletedTask;
        }
        #endregion
    }
}
