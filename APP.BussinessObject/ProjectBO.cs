﻿using APP.DataAccess.Models;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
namespace APP.BussinessObject
{
    public class ProjectBO
    {
        private readonly CRT_TMSContext _context;

       

        public ProjectBO(CRT_TMSContext context)
        {
            _context = context;
        }

        public List<Project> GetListAsync()
        {
            //using (var context = new CRT_TMSContext())
            //{
                return _context.Project.ToList();
            //}
             
        }
        public Project GetAsync(long projectId)
        {
            //using (var context = new CRT_TMSContext())
            //{
               return _context.Project.SingleOrDefault(p => p.ProjectId == projectId);
            //}
           // return null;
        }
    }
}
