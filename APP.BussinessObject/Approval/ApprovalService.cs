﻿using APP.DataAccess.Models;
using APP.EntityModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace APP.BussinessObject
{
    public class ApprovalService
    {
        #region Constructor
        private readonly CRT_TMSContext _context;
        private readonly NotificationService _notificationService;
        public ApprovalService(CRT_TMSContext context, NotificationService notificationService)
        {
            _context = context;
            _notificationService = notificationService;
        }
        #endregion

        #region Public Methods  
        public bool Approve(ApprovalDetailModel approvalDetailModel)
        {
            var approvalDetails = _context.ApprovalDetails.Include(s => s.Approver).Where(s => s.SessionId == approvalDetailModel.SessionId && s.AddedByUserId == approvalDetailModel.AddedByUserID && s.StatusCodeId == 2200).ToList();
            var approvalDetail = approvalDetails.FirstOrDefault();
            bool isApproved = false;
            if (approvalDetail != null)
            {
                var type = approvalDetail.Approver.ApproveTypeId;
                switch (type)
                {
                    case 2190:
                        isApproved = ApproveByLevels(approvalDetails, approvalDetailModel);
                        break;
                    case 2191:
                        isApproved = ApproveAll(approvalDetail, approvalDetailModel);
                        break;
                    case 2192:
                        isApproved = ApproveAny(approvalDetail, approvalDetailModel);
                        break;
                    default:
                        break;
                }
            }
            return isApproved;
        }

        public void CreateApprovalDetails(ApproveParam approveParam)
        {
            var approveMasters = _context.ApproveMaster.Where(s => s.ScreenId == approveParam.ScreenId).ToList();
            var userIds = approveMasters.Select(a => a.UserId).ToList();
            var employees = _context.Employee.Where(e => userIds.Contains(e.EmployeeId)).ToList();
            approveMasters.ForEach(a =>
            {
                var employeeItem = employees.FirstOrDefault(e => e.EmployeeId == a.UserId);
                ApprovalDetails approvalDetails = new ApprovalDetails
                {
                    ApproverId = a.ApproveMasterId,
                    SessionId = approveParam.SessionId,
                    AddedDate = DateTime.Now,
                    AddedByUserId = employeeItem.UserId.Value,
                    StatusCodeId = 2200
                };
                _context.ApprovalDetails.Add(approvalDetails);
            });
            _context.SaveChanges();
            ApprovalNotification(approveParam);
        }

        private void ApprovalNotification(ApproveParam approveParam)
        {
            var approveMasters = _context.ApproveMaster.Where(s => s.ScreenId == approveParam.ScreenId).ToList();
            var approvalDetails = _context.ApprovalDetails.Include(s => s.Approver).Where(s => s.SessionId == approveParam.SessionId).ToList();
            if (approveMasters.FirstOrDefault() != null)
            {
                var type = approveMasters.FirstOrDefault().ApproveTypeId;

                switch (type)
                {
                    case 2190:
                        ApprovalNotificationByLevels(approvalDetails);
                        break;
                    case 2191:
                    case 2192:
                        ApprovalNotificationAll(approvalDetails);
                        break;
                    default:
                        break;
                }
            }
        }

        public bool CheckIsApprovalWaiting(ApproveParam approveParam)
        {
            var approveMasters = _context.ApproveMaster.Where(s => s.ScreenId == approveParam.ScreenId).ToList();
            var approvalDetails = _context.ApprovalDetails.Include(s => s.Approver).Where(s => s.SessionId == approveParam.SessionId).ToList();
            if (approveMasters.FirstOrDefault() != null)
            {
                var type = approveMasters.FirstOrDefault().ApproveTypeId;
                switch (type)
                {
                    case 2190:
                    case 2191:
                        return !approvalDetails.All(a => a.StatusCodeId == 2201);
                    case 2192:
                        return !approvalDetails.Any(a => a.StatusCodeId == 2201);
                    default:
                        return false;
                }
            }
            return false;
        }
        #endregion

        #region Private Methods
        private bool ApproveByLevels(List<ApprovalDetails> approvalDetails, ApprovalDetailModel approvalDetailModel)
        {
            List<ApprovalDetails> nextApprovalDetails = null;
            var approvalDetail = approvalDetails.OrderBy(s => s.Approver.ApproveLevelId).FirstOrDefault();
            var nextApprovalDetail = approvalDetails.OrderBy(s => s.Approver.ApproveLevelId).FirstOrDefault(a => a.Approver.ApproveLevelId > approvalDetail.Approver.ApproveLevelId);
            if (nextApprovalDetail != null)
            {
                nextApprovalDetails = approvalDetails.OrderBy(s => s.Approver.ApproveLevelId).Where(a => a.Approver.ApproveLevelId > nextApprovalDetail.Approver.ApproveLevelId).ToList();
            }
            var notificationHandler = _context.NotificationHandler.FirstOrDefault(h => h.SessionId == approvalDetail.SessionId && h.NotifyTo == approvalDetail.AddedByUserId);
            notificationHandler.NotifyStatusId = 2;
            approvalDetail.StatusCodeId = approvalDetailModel.StatusCodeID.Value;
            approvalDetail.Remarks = approvalDetailModel.Remarks;
            _context.SaveChanges();

            var approveDetails = _context.ApprovalDetails.Where(a => a.SessionId == approvalDetail.SessionId);
            var isApproved = approveDetails.All(a => a.StatusCodeId == 2201);

            if (!isApproved && nextApprovalDetails != null)
            {
                nextApprovalDetails.ForEach(d =>
                {
                    var notificationHandlerModel = GenerateNotificationHandlerItem(d);
                    _notificationService.Create(notificationHandlerModel);
                });
            }
            return isApproved;
        }
        private bool ApproveAll(ApprovalDetails approvalDetail, ApprovalDetailModel approvalDetailModel)
        {
            var notificationHandler = _context.NotificationHandler.FirstOrDefault(h => h.SessionId == approvalDetail.SessionId && h.NotifyTo == approvalDetail.AddedByUserId);
            notificationHandler.NotifyStatusId = 2;
            approvalDetail.StatusCodeId = approvalDetailModel.StatusCodeID.Value;
            approvalDetail.Remarks = approvalDetailModel.Remarks;
            _context.SaveChanges();

            var approveDetails = _context.ApprovalDetails.Where(a => a.SessionId == approvalDetail.SessionId);
            return approveDetails.All(a => a.StatusCodeId == 2201);
        }
        private bool ApproveAny(ApprovalDetails approvalDetail, ApprovalDetailModel approvalDetailModel)
        {
            var notificationHandler = _context.NotificationHandler.FirstOrDefault(h => h.SessionId == approvalDetail.SessionId && h.NotifyTo == approvalDetail.AddedByUserId);
            notificationHandler.NotifyStatusId = 2;
            approvalDetail.StatusCodeId = approvalDetailModel.StatusCodeID.Value;
            approvalDetail.Remarks = approvalDetailModel.Remarks;
            _context.SaveChanges();

            var approveDetails = _context.ApprovalDetails.Where(a => a.SessionId == approvalDetail.SessionId);
            return approveDetails.Any(a => a.StatusCodeId == 2201);
        }
        private void ApprovalNotificationByLevels(List<ApprovalDetails> approvalDetails)
        {
            var firstDetail = approvalDetails.OrderBy(s => s.Approver.ApproveLevelId).FirstOrDefault(s => s.StatusCodeId == 2200);
            var details = approvalDetails.Where(s => s.Approver.ApproveLevelId == firstDetail.Approver.ApproveLevelId && s.StatusCodeId == 2200).ToList();
            details.ForEach(d =>
            {
                if (d != null)
                {
                    var notificationHandlerModel = GenerateNotificationHandlerItem(d);
                    _notificationService.Create(notificationHandlerModel);
                }
            });


        }
        private void ApprovalNotificationAll(List<ApprovalDetails> approvalDetails)
        {
            var allDetails = approvalDetails.OrderBy(s => s.ApprovalId).Where(s => s.StatusCodeId == 2200).ToList();
            if (allDetails.Count > 0)
            {
                allDetails.ForEach(detail =>
                {
                    GenerateNotificationHandlerItem(detail);
                });
            }
        }
        private NotificationHandlerModel GenerateNotificationHandlerItem(ApprovalDetails approvalDetails)
        {
            NotificationHandlerModel notificationHandlerModel = new NotificationHandlerModel
            {
                SessionId = approvalDetails.SessionId,
                NotifyStatusId = 1,
                Title = "Approval Notification!!!",
                Message = approvalDetails.Approver.ScreenId + "Item is waiting for your approval",
                NotifyTo = approvalDetails.AddedByUserId,
                ScreenId = approvalDetails.Approver.ScreenId

            };
            return notificationHandlerModel;
        }
        #endregion
    }
}
