import { createApp } from "vue";
import App from "./App.vue";
import { registerLicense } from "@syncfusion/ej2-base";
registerLicense(
  "ORg4AjUWIQA/Gnt2VVhhQlFaclhJXGFWfVJpTGpQdk5xdV9DaVZUTWY/P1ZhSXxRdkFhXX1fdHZVRmFZVkA="
);
createApp(App).mount("#app");
