import myApi from "@/util/api";

export function getNavMethodCodes() {
    return myApi.getAll("NavMethodCode/GetNavMethodCode");
}
export function getPlants() {
    return myApi.getAll("Plant/GetMobilePlants");
}
export function getCustomers(id) {
    return myApi.getByID(id, "NavCustomer/GetCustomers");
}
export function getApplicationUsersBySession(data) {

    return myApi.getAll('ApplicationUser/GetApplicationUsersBySession?sessionId=' + data)
}
export function getPARReport(item) {
    return myApi.getItem(item, "Simulation/GetPARReport");
}