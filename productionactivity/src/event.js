
export default [
    {
        name: 'APP_API_SUCCESS',
        callback: function (e) {
            this.snackbar = {
                show: true,
                color: 'green',
                text: e
            };
        }
    },
    {
        name: 'APP_API_ERROR',
        callback: function (e) {
            this.snackbar = {
                show: true,
                color: 'red',
                text: e
            };
        }
    },
    {
        name: 'APP_API_VAL',
        callback: function (e) {
            this.snackbar = {
                show: true,
                color: 'orange',
                text: e
            };
        }
    },



];