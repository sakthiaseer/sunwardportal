import myApi from '@/util/api'
export function getProductionActivityMaster () {

    return myApi.getAll("ProductionActivityMaster/Get")
}
export function createProductionActivityMaster (data) {
    return myApi.create(data, "ProductionActivityMaster/InsertProductionActivityMaster")
}

export function deleteProductionActivityMaster (data) {

    return myApi.delete(data.productionActivityMasterId, 'ProductionActivityMaster/DeleteProductionActivityMaster')
}
export function updateProductionActivityMaster (data) {

    return myApi.update(data.productionActivityMasterId, data, "ProductionActivityMaster/UpdateProductionActivityMaster")
}
export function getProductionActivityMasterRespons (id) {

    return myApi.getByID(id,"ProductionActivityMaster/GetProductionActivityMasterRespons")
}
export function createProductionActivityMasterRespons (data) {
    return myApi.create(data, "ProductionActivityMaster/InsertProductionActivityMasterRespons")
}
export function updateProductionActivityMasterRespons (data) {

    return myApi.update(data.productionActivityMasterResponsId, data, "ProductionActivityMaster/UpdateProductionActivityMasterRespons")
}
export function deleteProductionActivityMasterRespons (data) {

    return myApi.delete(data.productionActivityMasterResponsId, 'ProductionActivityMaster/DeleteProductionActivityMasterRespons')
}

export function createRecurrence (data) {
    return myApi.create(data, "ProductionActivityMaster/InsertRecurrence")
}
export function getApplicationWikiRecurrence (id) {

    return myApi.getByID(id, 'ProductionActivityMaster/GetRecurrence')
}






export function getProductionActivityMasterLine(id) {

    return myApi.getByID(id,'ProductionActivityMaster/GetProductionActivityMasterLine')
  }
export function createProductionActivityMasterLine(data) {
    return myApi.create(data, "ProductionActivityMaster/InsertProductionActivityMasterLine")
  }
export function updateProductionActivityMasterLine(data) {

    return myApi.update(data.productionActivityMasterLineId, data, "ProductionActivityMaster/UpdateProductionActivityMasterLine")
  }
  export function deleteProductionActivityMasterLine(data) {
  
    return myApi.delete(data.productionActivityMasterLineId, 'ProductionActivityMaster/DeleteProductionActivityMasterLine')
  }


export function getActivityInfo(id) {
    return myApi.getByID(id, "ProductionActivityMaster/GetActivityInfo")
}

export function createActivityInfo(data) {
    return myApi.create(data, "ProductionActivityMaster/InsertActivityInfo")
}
export function updateActivityInfo(data) {

    return myApi.update(data.productionActivityAppLineId, data, "ProductionActivityMaster/UpdateActivityInfo")
}
export function deleteActivityInfo(data) {

    return myApi.delete(data.activityMasterMultipleId, "ProductionActivityMaster/DeleteActivityInfo")
}


export function getProductionActivityCheckedDetails(id) {
    return myApi.getByID(id, "ProductActivityApp/GetProductionActivityCheckedDetails")
}

export function createProductionActivityCheckedDetails(data) {
    return myApi.create(data, "ProductActivityApp/InsertProductionActivityCheckedDetails")
}
export function updateProductionActivityCheckedDetails(data) {

    return myApi.update(data.productionActivityCheckedDetailsId, data, "ProductActivityApp/UpdateProductionActivityCheckedDetails")
}
export function deleteProductionActivityCheckedDetails(data) {

    return myApi.delete(data.productionActivityCheckedDetailsId, "ProductActivityApp/DeleteProductionActivityCheckedDetails")
}


export function getProductionRoutineCheckedDetails(id) {
    return myApi.getByID(id, "ProductionActivityRoutineApp/GetProductionRoutineCheckedDetails")
}

export function createProductionRoutineCheckedDetails(data) {
    return myApi.create(data, "ProductionActivityRoutineApp/InsertProductionRoutineCheckedDetails")
}
export function updateProductionRoutineCheckedDetails(data) {

    return myApi.update(data.productionActivityRoutineCheckedDetailsId, data, "ProductionActivityRoutineApp/UpdateProductionRoutineCheckedDetails")
}
export function deleteProductionRoutineCheckedDetails(data) {

    return myApi.delete(data.productionActivityRoutineCheckedDetailsId, "ProductionActivityRoutineApp/DeleteProductionRoutineCheckedDetails")
}