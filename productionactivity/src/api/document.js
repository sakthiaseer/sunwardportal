import myApi from '@/util/api'

export function getUsers() {

  return myApi.getAll('ApplicationUser/GetApplicationUsers')
}
export function getDocuments() {
  return myApi.getAll('Documents/GetDocuments')
}
export function getDocumentTypes() {
  return myApi.getAll('Documents/GetDocumentTypes')
}
export function getPublicFolders(id) {
  return myApi.getByID(id, 'Documents/GetPublicFolders')
}

export function getDocumentBySessionID(id) {
  return myApi.getBySessionID(id, 'Documents/GetDocumentBySessionID')
}
export function getSubPublicFolderId(id, userId) {

  return myApi.getByuserId(id, userId, 'Documents/GetSubPublicFolderId')
}
export function getPublicFolderTreeId(id, userId) {

  return myApi.getByuserId(id, userId, 'Documents/GetPublicFolderId')
}
export function getDocument(data) {

  return myApi.getItem(data, "Documents/GetData")
}

export function createDocuments(data) {

  return myApi.create(data, "Documents/InsertDocuments")
}

export function updateDocuments(data) {

  return myApi.update(data.documentID, data, "Documents/UpdateDocuments")
}
export function deleteDocuments(data) {

  return myApi.delete(data.documentID, 'Documents/DeleteDocuments')
}
export function uploadDocuments(data) {

  return myApi.uploadFile(data, "Documents/UploadDocuments")
}
export function documentUsers(data) {
  return myApi.getByID(data, "DocumentAccess/GetDocumentAccess")
}
export function getDocumentAccessUsers(data) {
  return myApi.getByID(data, "DocumentAccess/GetDocumentAccessUsers")
}
export function createDocumentAccess(data) {
  return myApi.create(data, "DocumentAccess/InsertDocumentAccess")
}
export function insertUpdateFileProfileTypeDocument(data) {
  return myApi.create(data, "Documents/InsertUpdateFileProfileTypeDocument")
}

export function updateDocumentAccess(data) {

  return myApi.update(data.documentAccessID, data, "DocumentAccess/UpdateDocumentAccess")
}
export function downloadFile(data) {

  return myApi.downLoadDocument(data, "Download/DownLoadDocument")
}
export function getLatestDocument(data) {

  return myApi.downLoadDocument(data, "Download/GetLatestDocument")
}

export function downloadLatestFile(data) {

  return myApi.downLatestDocument(data, "Download/DownLoadLatestDocument")
}
export function fileUploadDoc(data) {

  return myApi.update(data.taskAttachmentId, data, 'TaskMaster/FileUploadDoc')
}
export function setDocAccess(data) {

  return myApi.update(data.taskAttachmentId, data, 'TaskMaster/SetDocAccess')
}
export function documentInfo(data) {
  return myApi.downLatestDocument(data, "Download/LatestDocumentInfo")
}
export function updateDocumentRights(data) {
  return myApi.update(data.documentId, data, 'DocumentAccess/UpdateDocumentRights')
}
export function attachmentDownload(id, userId) {
  return myApi.getAll("Documents/AttachmentDownload?id=" + id + "&&userId=" + userId);
}
export function getDocumentCheckIn(id,userId) {
  return myApi.getAll("Documents/DocumentCheckIn?id=" + id+ "&&userId=" + userId);
}

export function fileProfileCheckOut(data) {

  return myApi.update(data.documentID, data, 'Documents/FileProfileCheckOut')
}
export function fileProfileDocumentCheckIn(data) {

  return myApi.update(data.documentID, data, 'Documents/FileProfileDocumentCheckIn')
}
export function uploadVideo(data) {

  return myApi.uploadFile(data, "Documents/UploadVideo")
}
export function getVideoFiles() {

  return myApi.getAll('Documents/GetVideoFiles')
}
export function deleteUploadVideo(data) {

  return myApi.update(data.videoId, data, 'Documents/DeleteUploadVideo')
}

export function createLinkFileProfileType(data) {

  return myApi.create(data, "Documents/InsertLinkFileProfileType")
}

export function deleteTaskDocuments(data) {

  return myApi.update(data.documentID,data, "Documents/DeleteTaskDocuments")
}


export function getDocumentsFileSize() {
  return myApi.getAll("Documents/GetDocumentsFileSize");
}
export function uploadDocumentsSize(data) {

  return myApi.uploadFile(data, "Documents/UploadDocumentsSize")
}

export function getDocumentById(id) {
  return myApi.getByID(id, 'Documents/GetDocumentById')
}