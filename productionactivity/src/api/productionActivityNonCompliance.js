import myApi from '@/util/api'
export function getProductionActivityNonCompliance(type, id,actionType) {

    return myApi.getAll("ProductActivityApp/GetProductionActivityNonCompliance?type=" + type + "&&id=" + id+"&&actionType="+actionType)
}
export function createProductionActivityNonCompliance(data) {
    return myApi.create(data, "ProductActivityApp/InsertProductionActivityNonCompliance")
}
export function updateProductionActivityNonCompliance(data) {

    return myApi.update(data.productionActivityNonComplianceId, data, "ProductActivityApp/UpdateProductionActivityNonCompliance")
}
export function deleteProductionActivityNonCompliance(data) {

    return myApi.delete(data.productionActivityNonComplianceUserId, "ProductActivityApp/deleteProductionActivityNonCompliance")
}