import myApi from '@/util/api'
export function getRoutineInfo(id) {
    return myApi.getByID(id, "ProductionActivityRoutineApp/GetRoutineInfo")
}
export function getRoutineInfoList() {
    return myApi.getByAll("ProductionActivityRoutineApp/GetRoutineInfoList")
}
export function createRoutineInfo(data) {
    return myApi.create(data, "ProductionActivityRoutineApp/InsertRoutineInfo")
}
export function updateRoutineInfo(data) {

    return myApi.update(data.routineInfoId, data, "ProductionActivityRoutineApp/UpdateRoutineInfo")
}
export function deleteRoutineInfo(data) {

    return myApi.delete(data.routineInfoId, "ProductionActivityRoutineApp/DeleteRoutineInfo")
}