import myApi from '@/util/api'

export function getApplicationUsersBySession(data) {

    return myApi.getAll('ApplicationUser/GetApplicationUsersBySession?sessionId=' + data)
}
export function getPlants() {

    return myApi.getAll('Plant/GetPlants')
}
export function getApplicationMasterDetailByType(type) {
    return myApi.getByType(type, "ApplicationMaster/GetApplicationMasterDetailByType")
}
export function createProcess(data) {
    return myApi.create(data, "ProductActivityApp/InsertProductActivityApp")
}
export function updateProductionActivityApp(data) {

    return myApi.update(data.productionActivityAppId, data, "ProductActivityApp/UpdateProductActivityApp")
}
export function updateProductActivityRoutineStatus(data) {

    return myApi.update(data.productionActivityRoutineAppLineId, data, "ProductActivityApp/UpdateProductActivityRoutineStatus")
}
export function updateProductionActivityAppQaCheck(data) {

    return myApi.update(data.productionActivityAppLineId, data, "ProductActivityApp/UpdateProductActivityAppQaCheck")
}
export function updateProductActivityAppLineStatus(data) {

    return myApi.update(data.productionActivityAppLineId, data, "ProductActivityApp/UpdateProductActivityAppLineStatus")
  }
  export function getProductionActivityAppExcel(data) {

    return myApi.downLoadProfileDocument(data, 'ProductActivityApp/GetProductionActivityAppExcel')
}

export function authenticate(data) {
    return myApi.getItem(data, "UserAuth/Login")
}
export function getProductActivityCaseCategory(id, mprocessId) {
    return myApi.getAll("ProductActivityCase/GetProductActivityCaseCategory?mprocessId=" + mprocessId + "&&id=" + id)
}
export function getNavprodOrdersNo(id) {
    return myApi.getByID(id, "ProductActivityApp/GetNavprodOrdersNo")
}
export function getNavprodOrdersNoList(type, id) {
    return myApi.getAll("ProductActivityApp/GetNavprodOrdersNoList?type=" + type + "&&id=" + id)
}
export function getProductActivityAppLine(id, type, userId, locationId) {
    return myApi.getAll("ProductActivityApp/GetProductActivityAppLine?id=" + id + "&&type=" + type + "&&userId=" + userId + "&&locationId=" + locationId)
}
export function createApplicationMaster(data) {
    return myApi.create(data, "ApplicationMaster/InsertApplicationMasterDetailByCodeID")
}
export function getDocumentNoSeries(id) {
    return myApi.getByID(id, "DocumentNoSeries/GetDocumentNoSeriesItemsByProfileIdFlag")
}
export function getNavprodOrderLine(id, type) {
    return myApi.getAll("ProductActivityApp/GetNavprodOrderLine?id=" + id + "&&type=" + type)
}
export function getFileProfileTypeFlatItems(id) {
    return myApi.getByID(id, "FileProfileType/GetFileProfileTypeFlatItems")
}
export function deleteProductionActivityApp(data) {

    return myApi.delete(data.productionActivityAppLineId, 'ProductActivityApp/DeleteProductionActivityApp')
}
export function createProductionActivityApp(data) {
    return myApi.create(data, "ProductActivityApp/InsertProductActivityApp")
}
export function getApplicationMasterChild(data) {
    return myApi.getByID(data, "ApplicationMasterChild/GetApplicationMasterChildByParentID")
}
export function getApplicationMasterChildtWithChild(data) {
    return myApi.getByID(data, "ApplicationMasterChild/GetApplicationMasterChildtWithChild")
}
export function getApplicationMasterChildByMultipleSelection(data) {
    return myApi.getItem(data, "ApplicationMasterChild/GetApplicationMasterChildByMultipleSelection")
}

export function getApplicationMasterChildtWithChildWithouCat(data, actionId) {
    return myApi.getAll("ApplicationMasterChild/GetApplicationMasterChildtWithChildWithouCat?id=" + data + "&&actionId=" + actionId);
}

export function getIpirReportLine(id, type, userId, locationId) {
    return myApi.getAll("IpirReport/GetIpirReportLine?id=" + id + "&&type=" + type + "&&userId=" + userId + "&&locationId=" + locationId)
}
export function getApplicationPermissions(id) {

    return myApi.getByID(id, 'ApplicationRolePermission/GetApplicationPermissions')
}
export function getCodeMasterByType(type) {
    return myApi.getByType(type, "ApplicationUser/GetCodeMasterByType")
}
export function getDocumentProfilesByStatus() {

    return myApi.getAll('DocumentProfile/GetDocumentProfilesByStatus')
}



export function createProductionActivityRoutineApp(data) {
    return myApi.create(data, "ProductionActivityRoutineApp/InsertProductionActivityRoutineApp")
}
export function updateProductionActivityRoutineAppLine(data) {

    return myApi.update(data.productionActivityAppId, data, "ProductionActivityRoutineApp/UpdateProductionActivityRoutineAppLine")
}
export function getProductionActivityRoutineAppLine(id, userId, appId, locationId, type) {
    return myApi.getAll("ProductionActivityRoutineApp/getProductionActivityRoutineAppLine?id=" + id + "&&userId=" + userId + "&&appId=" + appId + "&&locationId=" + locationId + "&&type=" + type)
}

export function uploadProductionActivityRoutineAppDocuments(data) {

    return myApi.uploadFile(data, "ProductionActivityRoutineApp/UploadDocuments")
}

export function deleteProductionActivityRoutineApp(data) {

    return myApi.delete(data.productionActivityRoutineAppLineId, 'ProductionActivityRoutineApp/deleteProductionActivityRoutineApp')
}
export function updateProductActivityAppRoutineQaCheck(data) {

    return myApi.update(data.productionActivityRoutineAppLineId, data, "ProductionActivityRoutineApp/UpdateProductActivityAppRoutineQaCheck")
}
export function getEmployeesByCompany(id) {

    return myApi.getByID(id, "Employee/GetEmployeesByCompany")
}
export function getSelectedEmployeesByCompany(data) {

    return myApi.getItem(data, "Employee/GetSelectedEmployeesByCompany")
}
export function getEmployeesByCompanyForRoutine(id) {

    return myApi.getByID(id, "Employee/GetEmployeesByCompanyForRoutine")
}
export function uploadProcessDocuments(data) {

    return myApi.uploadFile(data, "ProductActivityApp/UploadDocuments")
}
export function uploadMergePdfDocuments(data) {

    return myApi.uploadFile(data, "documents/UploadMergePdfDocuments")
}

export function getICTMasterLocationDropDown(data) {
    return myApi.getByID(data, "ICTMaster/GetICTMasterLocationDropDownByParentIctid")
}
export function getNavprodOrderLineBatchNo(data) {
    return myApi.getByID(data, "ProductionActivityRoutineApp/GetNavprodOrderLineBatchNo")
}



export function createProductionActivityPlanningApp(data) {
    return myApi.create(data, "ProductionActivityPlanningApp/InsertProductionActivityPlanningApp")
}
export function updateProductionActivityPlanningAppLine(data) {

    return myApi.update(data.productionActivityAppId, data, "ProductionActivityPlanningApp/UpdateProductionActivityPlanningAppLine")
}
export function getProductionActivityPlanningAppLine(id, userId, appId, locationId, type) {
    return myApi.getAll("ProductionActivityPlanningApp/getProductionActivityPlanningAppLine?id=" + id + "&&userId=" + userId + "&&appId=" + appId + "&&locationId=" + locationId + "&&type=" + type)
}

export function uploadProductionActivityPlanningAppDocuments(data) {

    return myApi.uploadFile(data, "ProductionActivityPlanningApp/UploadDocuments")
}

export function deleteProductionActivityPlanningApp(data) {

    return myApi.delete(data.productionActivityPlanningAppLineId, 'ProductionActivityPlanningApp/deleteProductionActivityPlanningApp')
}
export function updateProductActivityAppPlanningQaCheck(data) {

    return myApi.update(data.productionActivityPlanningAppLineId, data, "ProductionActivityPlanningApp/UpdateProductActivityAppPlanningQaCheck")
}
export function getProductionActivityAppRoutineExcel(data) {

    return myApi.downLoadProfileDocument(data, 'ProductionActivityRoutineApp/getProductionActivityAppRoutineExcel')
}

export function getNavprodOrderLineAll(id) {
    return myApi.getByID(id, "ProductActivityApp/GetNavprodOrderLineAll")
}
export function getProductActivityCaseByCategory(id) {
    return myApi.getByID(id, "ProductActivityApp/GetProductActivityCaseByCategory")
}
export function getProductActivityRoutineComment(id) {

    return myApi.getByID(id, "ProductionActivityRoutineApp/GetProductActivityRoutineComment")
}
export function getProductActivityComment(id) {

    return myApi.getByID(id, "ProductActivityApp/GetProductActivityComment")
}

export function getProductActivityApp(id) {

    return myApi.getByID(id, "ProductActivityApp/GetProductActivityApp")
}

export function getProductionActivityAppRoutine(id) {

    return myApi.getByID(id, "ProductionActivityRoutineApp/GetProductionActivityAppRoutine")
}

export function getProductionActivityAppRoutineSearch(data) {
    return myApi.getItem(data, "ProductionActivityRoutineApp/GetProductionActivityAppRoutineSearch")
}

export function getProductActivityAppSearch(data) {
    return myApi.getItem(data, "ProductActivityApp/GetProductActivityAppSearch")
}
export function getProductActivityPermissionByUserId(data) {

    return myApi.getItem(data, 'ProductActivityCase/GetProductActivityPermissionByUserId')
  }
  export function getProductionActivityAppRoutineFilter(data) {

    return myApi.getItem(data, 'ProductionActivityRoutineApp/GetProductionActivityAppRoutineFilter')
  }
  export function getProductionActivityAppFilter(data) {

    return myApi.getItem(data, 'ProductActivityApp/GetProductionActivityAppFilter')
  }

  export function updateProductActivityPermission(data) {

    return myApi.update(data.productActivityPermissionId, data, "ProductActivityCase/UpdateProductActivityPermission")
}

export function getProductActivityEmailActivitySubjects(data) {
    return myApi.getItem(data, "ProductActivityApp/GetProductActivityEmailActivitySubjects")
}


export function updateRoutineChecker(data) {

    return myApi.update(data.productionActivityRoutineAppLineId, data, "ProductionActivityRoutineApp/UpdateRoutineChecker")
}

export function updateProductActivityAppChecker(data) {

    return myApi.update(data.productionActivityAppLineId, data, "ProductActivityApp/UpdateProductActivityAppChecker")
}

export function getProductActivityChecker(id) {

    return myApi.getByID(id, "ProductActivityApp/GetProductActivityChecker")
}

export function getProductActivityRoutineChecker(id) {

    return myApi.getByID(id, "ProductionActivityRoutineApp/GetProductActivityRoutineChecker")
}