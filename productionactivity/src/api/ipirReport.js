import myApi from '@/util/api'
export function getIpirReport () {

    return myApi.getAll("IpirReport/Get")
}
export function createIpirReport (data) {
    return myApi.create(data, "IpirReport/InsertIpirReport")
}

export function updateIpirReport (data) {

    return myApi.update(data.ipirReportId, data, "IpirReport/UpdateIpirReport")
}
export function deleteIpirReport (data) {

    return myApi.delete(data.ipirReportId, 'IpirReport/DeleteIpirReport')
}
export function uploadProcessDocuments(data) {

    return myApi.uploadFile(data, "IpirReport/UploadDocuments")
}
export function updateIpirReportClose (data) {

    return myApi.update(data.ipirReportId, data, "IpirReport/UpdateIpirReportClose")
}
export function updateIpirReportTopic (data) {

    return myApi.update(data.ipirReportId, data, "IpirReport/UpdateIpirReportTopic")
}

export function getIpirReportLocationPlant (name) {

    return myApi.getItem(name,"IpirReport/getIpirReportLocationPlant")
}




export function getIpirReportAssignment (id) {

    return myApi.getByID(id,"IpirReport/GetIpirReportAssignment")
}
export function insertIpirAssignment (data) {
    return myApi.create(data, "IpirReport/InsertIpirAssignment")
}

export function updateIpirAssignment (data) {

    return myApi.update(data.ipirReportAssignmentId, data, "IpirReport/UpdateIpirAssignment")
}
export function deleteIpirReportAssignment (data) {

    return myApi.delete(data.ipirReportAssignmentId, 'IpirReport/DeleteIpirReportAssignment')
}

export function getIpirReportAssignmentTopicList (id) {

    return myApi.getByID(id,"IpirReport/GetIpirReportAssignmentTopicList")
}
export function updateIpirAssignmentTopic (data) {

    return myApi.update(data.ipirReportAssignmentId, data, "IpirReport/UpdateIpirAssignmentTopic")
}