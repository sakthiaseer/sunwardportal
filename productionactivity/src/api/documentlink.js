import myApi from '@/util/api'

export function getDocumentLinkByDocumentId(id) {

    return myApi.getByID(id, "DocumentLink/GetDocumentLinkByDocumentId")
}
export function createDocumentLink(data) {

    return myApi.create(data, "DocumentLink/InsertDocumentLink")
}
export function updateDocumentLink(data) {

    return myApi.update(data.documentLinkId, data, "DocumentLink/UpdateDocumentLink")
}
export function deleteDocumentLink(data) {

    return myApi.delete(data.documentLinkId, 'DocumentLink/DeleteDocumentLink')
}
export function getDocumentDirectory(data) {

    return myApi.getItem(data, "DocumentLink/GetDocumentDirectory")
}
export function uploadFromDocumentLink(data) {

    return myApi.uploadFile(data, "DocumentLink/UploadFromDocumentLink")
}