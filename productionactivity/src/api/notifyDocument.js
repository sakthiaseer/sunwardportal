import myApi from '@/util/api'
import GlobalMessage from "@/constant";
export function getNotifyDocuments() {

    return myApi.getAll('NotifyDocument/GetNotifyDocuments')
}
export function getNotifyDocumentByUser(data) {

    return myApi.getAll("NotifyDocument/GetNotifyDocumentByUser/?id="+data+"&&type=ProductionApp")
}
export function getNotifyDocumentBySendUser(data) {

    return myApi.getAll("NotifyDocument/GetNotifyDocumentBySendUser/?id="+data+"&&type=ProductionApp")
}
export function getNotifyHistory(data) {
    return myApi.getByID(data, "NotifyDocument/GetNotifyHistory")
}

export function createNotifyDocument(data) {
    data.baseurl=GlobalMessage.BASE_URL;
    return myApi.create(data, "NotifyDocument/InsertNotifyDocument")
}

export function updateNotifyDocument(data) {
    data.baseurl=GlobalMessage.BASE_URL;
    return myApi.update(data.notifyDocumentId, data, "NotifyDocument/UpdateNotifyDocument")
}
export function closeNotifyDocument(data) {
    return myApi.update(data.notifyDocumentId, data, "NotifyDocument/CloseNotifyDocument")
}
export function closeSenderNotifyDocument(data) {
    return myApi.update(data.notifyDocumentId, data, "NotifyDocument/CloseSenderNotifyDocument")
}
export function getNotifyDocumentById(id) {

    return myApi.getByID(id, "NotifyDocument/GetNotifyDocumentById")
}
export function getLatestNotifyDocumentById(id) {

    return myApi.getByID(id, "NotifyDocument/GetLatestNotifyDocumentById")
}
export function getNotifyDocumentByCCUser(data) {

    return myApi.getByID(data, "NotifyDocument/GetNotifyDocumentByCCUser")
}
export function createNotifylongNotes(data) {
    data.baseurl=GlobalMessage.BASE_URL;
    return myApi.update(data.notifyDocumentId, data, "NotifyDocument/CreateNotifylongNotes")
}
export function getLongNotesByNotifyId(id) {

    return myApi.getByID(id, "NotifyDocument/GetLongNotesByNotifyId")
}
export function getMailNotifyById(id) {
    return myApi.getByID(id, "NotifyDocument/GetMailNotifyById")
}

export function closeBothNotifyDocument(data) {
    return myApi.update(data.notifyDocumentId, data, "NotifyDocument/CloseBothNotifyDocument")
}

export function updateNotifyStatusById(id, userId) {

    return myApi.getByuserId(id, userId, "NotifyDocument/UpdateNotifyStatus")
}
export function updateSendNotifyDueDate(data) {
    return myApi.update(data.notifyDocumentId, data, "NotifyDocument/UpdateSendNotifyDueDate")
}