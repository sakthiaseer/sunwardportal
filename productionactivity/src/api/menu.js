import myApi from "@/util/api";
export function getuserMenu(userId) {
    return myApi.getByID(userId, "ApplicationUser/GetWebUserPermissionByProductionId");
}