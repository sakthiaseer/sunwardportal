import myApi from '@/util/api'

export function getProductActivityCase() {

  return myApi.getAll('ProductActivityCase/GetProductActivityCase')
}
export function getSearchData(data) {

  return myApi.getItem(data, "ProductActivityCase/GetData")
}

export function createProductActivityCase(data) {
  return myApi.create(data, "ProductActivityCase/InsertProductActivityCase")
}
export function updateProductActivityCase(data) {

  return myApi.update(data.productActivityCaseId, data, "ProductActivityCase/UpdateProductActivityCase")
}
export function deleteProductActivityCase(data) {

  return myApi.delete(data.productActivityCaseId, 'ProductActivityCase/DeleteProductActivityCase')
}

export function createProductActivityPermission(data) {
  return myApi.create(data, "ProductActivityCase/InsertProductActivityPermission")
}
export function updateProductActivityPermission(data) {

  return myApi.update(data.productActivityPermissionId, data, "ProductActivityCase/UpdateProductActivityPermission")
}
export function getProductActivityPermissionById(id) {

  return myApi.getByID(id, 'ProductActivityCase/GetProductActivityPermissionById')
}




export function getProductActivityCaseLine(id) {

  return myApi.getByID(id, 'ProductActivityCase/GetProductActivityCaseLine')
}
export function createProductActivityCaseLine(data) {
  return myApi.create(data, "ProductActivityCase/InsertProductActivityCaseLine")
}
export function updateProductActivityCaseLine(data) {

  return myApi.update(data.productActivityCaseLineId, data, "ProductActivityCase/UpdateProductActivityCaseLine")
}
export function deleteProductActivityCaseLine(data) {

  return myApi.delete(data.productActivityCaseLineId, 'ProductActivityCase/DeleteProductActivityCaseLIne')
}





export function getProductionActivityMasterRespons(id) {

  return myApi.getByID(id, "ProductActivityCase/GetProductionActivityMasterRespons")
}
export function createProductionActivityMasterRespons(data) {
  return myApi.create(data, "ProductActivityCase/InsertProductionActivityMasterRespons")
}
export function createProductionActivityMasterResponsByLine(data) {
  return myApi.create(data, "ProductActivityCase/InsertProductionActivityMasterResponsByLine")
}
export function updateProductionActivityMasterRespons(data) {

  return myApi.update(data.productActivityCaseResponsId, data, "ProductActivityCase/UpdateProductionActivityMasterRespons")
}
export function deleteProductionActivityMasterRespons(data) {

  return myApi.delete(data.productActivityCaseResponsId, 'ProductActivityCase/DeleteProductionActivityMasterRespons')
}

export function createRecurrence(data) {
  return myApi.create(data, "ProductActivityCase/InsertRecurrence")
}
export function getApplicationWikiRecurrence(id) {

  return myApi.getByID(id, 'ProductActivityCase/GetRecurrence')
}




export function getApplicationWikiLineDuty (applicationWikiLineId) {

  return myApi.getAll("ProductActivityCase/getApplicationWikiLineDuty?id=" + applicationWikiLineId);
}

export function createApplicationWikiLineDuty (data) {
  return myApi.create(data, "ProductActivityCase/InsertApplicationWikiLineDuty")
}

export function updateApplicationWikiLineDuty (data) {

  return myApi.update(data.productActivityCaseResponsDutyId, data, "ProductActivityCase/UpdateApplicationWikiLineDuty")
}
export function deleteApplicationWikiLineDuty (data) {

  return myApi.delete(data.productActivityCaseResponsDutyId, 'ProductActivityCase/DeleteApplicationWikiLineDuty')
}
export function deleteWikiResponsible (data) {

  return myApi.update(data.wikiResponsibilityID, data,'ProductActivityCase/DeleteWikiResponsible')
}
export function getWikiResponsibleByID (id) {

  return myApi.getByID(id, 'ProductActivityCase/GetWikiResponsibleByID')
}
export function deleteWikiResponsibles (data) {

  return myApi.update(data.wikiResponsibilityID, data, "ProductActivityCase/DeleteWikiResponsibles")
}
export function getWikiResponsiblesByExits(id, caseFormId) {

  return myApi.getAll("ProductActivityCase/getWikiResponsiblesByExits?id=" + id + "&&caseFormId=" + caseFormId)
}


export function updateProductActivityCaseVersion (data) {

  return myApi.update(data.productActivityCaseId, data, "ProductActivityCase/UpdateProductActivityCaseVersion")
}

export function createVersion (data) {

  return myApi.update(data.productActivityCaseId, data, "ProductActivityCase/CreateVersion")
}
export function applyVersion (data) {

  return myApi.update(data.productActivityCaseId, data, "ProductActivityCase/ApplyVersion")
}
export function checkExitDataVersion (data) {

  return myApi.update(data.productActivityCaseId, data, "ProductActivityCase/CheckExitDataVersion")
}
export function tempVersion (data) {

  return myApi.update(data.productActivityCaseId, data, "ProductActivityCase/TempVersion")
}
export function undoVersion (id) {

  return myApi.getByID(id, "ProductActivityCase/UndoVersion")
}
export function deleteVersion (id) {

  return myApi.delete(id, 'ProductActivityCase/DeleteVersion')
}
export function getResponsibilityUsersExcel(data) {

  return myApi.downLoadProfileDocument(data, 'ProductActivityCase/GetResponsibilityUsersExcel')
}
