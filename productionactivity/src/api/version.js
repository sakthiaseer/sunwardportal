import myApi from "@/util/api";

export function getversions(screenId, sessionId, controller, action) {
  let url = controller + "/" + action;
  return myApi.getBySessionID(sessionId, url);
}
export function deleteVersion(versionId) {
  return myApi.delete(versionId, "Versions/DeleteVersion");
}
