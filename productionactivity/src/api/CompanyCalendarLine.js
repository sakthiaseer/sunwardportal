import myApi from '@/util/api'
export function getCompanyCalendarLine (id) {

    return myApi.getByID(id, "companyCalendarLine/GetCompanyCalendarLine")
}
export function createCompanyCalendarLine (data) {
    return myApi.create(data, "companyCalendarLine/InsertCompanyCalendarLine")
}

export function updateCompanyCalendarLine (data) {

    return myApi.update(data.companyCalendarLineId, data, "companyCalendarLine/UpdateCompanyCalendarLine")
}
export function deleteCompanyCalendarLine (data) {

    return myApi.delete(data.companyCalendarLineId, 'companyCalendarLine/DeleteCompanyCalendarLine')
}

export function uploadCalandarLinkDocuments(data) {

    return myApi.uploadFile(data, "documents/UploadCalandarLinkDocuments")
}


export function getCompanyCalendarLineLink (id) {

    return myApi.getByID(id, "companyCalendarLine/GetCompanyCalendarLineLink")
}
export function createCompanyCalendarLineLink (data) {
    return myApi.create(data, "companyCalendarLine/InsertCompanyCalendarLineLink")
}

export function updateCompanyCalendarLineLinks (data) {

    return myApi.update(data.companyCalendarLineLinkId, data, "companyCalendarLine/UpdateCompanyCalendarLineLinks")
}
export function deleteCompanyCalendarLineLink (data) {

    return myApi.delete(data.companyCalendarLineLinkId, 'companyCalendarLine/DeleteCompanyCalendarLineLink')
}
export function getNotesUsers (id) {

    return myApi.getByID(id, "companyCalendarLine/getNotesUsers")
}
export function getCalandarNotes (id,userId) {

    return myApi.getByuserId(id,userId, "companyCalendarLine/GetCalandarNotes")
}
export function createCompanyCalendarLineNotes (data) {
    return myApi.create(data, "companyCalendarLine/InsertCompanyCalendarLineNotes")
}
export function getCompanyCalendarLineLinkDocuments(sessionId, userId) {

    return myApi.getAll('Documents/GetDocumentsByCalandarLink/?sessionID=' + sessionId + '&&userId=' + userId)
}
export function deleteDocuments(data) {

    return myApi.delete(data.documentID, 'Documents/DeleteDocuments')
}
export function uploadCalandarLinkAllDocuments(data,userId, sessionId) {

    return myApi.uploadFile(data, "Documents/uploadCalandarLinkAllDocuments?sessionId=" + sessionId+"&&userId="+userId);
}
export function createRestrictUsers(data) {

    return myApi.create(data, "companyCalendarLine/insertRestrictUsers")
}
export function getRestrictUsers(data) {

    return myApi.getByID(data, "companyCalendarLine/getRestrictUsers")
}