import myApi from '@/util/api'

export function getProductActivityCaseSetupFormsById(id) {

    return myApi.getByID(id,'FileProfileSetupForm/GetProductActivityCaseSetupFormsById')
}
export function getFileProfileSetupForm(data) {

    return myApi.getItem(data, "FileProfileSetupForm/GetData")
}
export function createFileProfileSetupForm(data) {
    return myApi.create(data, "FileProfileSetupForm/InsertFileProfileSetupForm")
}
export function getValidProductActivtySetUpFromIds(id) {

    return myApi.getByID(id, 'FileProfileSetupForm/GetValidProductActivtySetUpFromIds')
  }
export function updateFileProfileSetupForm(data) {

    return myApi.update(data.fileProfileSetupFormId, data, "FileProfileSetupForm/UpdateFileProfileSetupForm")
}
export function deleteFileProfileSetupForm(data) {

    return myApi.delete(data.fileProfileSetupFormId, 'FileProfileSetupForm/DeleteFileProfileSetupForm')
}
