import myApi from "@/util/topicapi";

export function uploadTopicFiles(data) {

    return myApi.uploadFile(data, "topic/AddTopic");
}