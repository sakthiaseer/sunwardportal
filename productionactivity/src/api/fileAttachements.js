import myApi from "@/util/api";

export function getfiles(screenId, sessionId, userId, controller, action, type) {
  let url = controller + "/" + action;
 
  return myApi.getBySession(sessionId, userId, url, type);
}

export function uploadFiles(data, controller, action) {
  if (action != null) {
    let url = controller + "/" + action;
    return myApi.uploadFile(data, url);
  }
  else {
    return myApi.uploadFile(data, "Documents/UploadDocuments");
  }
}
export function updateFiles(id) {
  return myApi.getByID(id, "Documents/UpdateDocument");

}
export function downloadFile(data, controller, action) {
  if (action != null) {
    let url = controller + "/" + action;
    console.log(url);
    return myApi.downLoadDocument(data, url);
  }
  else {

    return myApi.downLoadDocument(data, "Download/DownLoadDocument");
  }
}

export function downLoadHeaderDocument(data) {
  return myApi.getBySessionID(data, "Documents/DownLoadHeaderDocument");
}


export function deleteFiles(documentId, controller, action) {
  if (action != null) {
    let url = controller + "/" + action;
    return myApi.delete(documentId, url);
  }
  else {
    return myApi.delete(documentId, "Documents/DeleteDocuments");
  }
}
export function viewFile(data) {


  return myApi.getItem(data, "Documents/ViewDocument");

}

export function viewSelfTestDocument(data) {


  return myApi.getItem(data, "Documents/ViewSelfTestDocument");

}
export function deletewikiDocument(data) {

  return myApi.update(data.documentID, data, 'Documents/DeleteWikiDocuments');
}
export function updateDocumentIsPrint(data) {

  return myApi.update(data.documentID, data, "FileProfileType/updateDocumentIsPrint")
}