import myApi from '@/util/api'
export function uploadDocuments(data) {

    return myApi.uploadFile(data, "Upload/UploadDocuments")
}
export function getDocuments() {

    return myApi.getAll("Upload/GetDocuments")
}
export function downloadFileDoc(data) {

    return myApi.downLoadDocument(data, "Upload/DownLoadDocument");
}
export function viewFile(data) {
    return myApi.getItem(data, "Upload/ViewDocument?fileName=" + data.fileName);
}
export function getDownLoad(data) {

    return myApi.downLoadDocument(data, "Upload/GetDownLoad");
}
export function uploadDocumentsFile(data) {

    return myApi.uploadFile(data, "ImportEmployee/ImportuploadDocuments")
}