import GlobalMessage from "@/constant";
const onGetDownloadFiles = (item) => {
    item.documentId=item.documentID;
    const a = document.createElement("a");
    var url =
    //"http://localhost:2025/api/ "+ "Upload/GetDownLoadFiles?id=" + item.documentId;
    GlobalMessage.API_URL + "Upload/GetDownLoadFiles?id=" + item.documentId;
    a.href = url;
    a.download = url.split("/").pop();
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
};
const onGetDownloadFilesAuto = (item) => {
    item.documentId=item.documentID;
    var url =
        GlobalMessage.API_URL + "Upload/GetDownLoadFiles?id=" + item.documentId;
    var link = document.createElement('a');
    link.href = url;
    link.download = url.split("/").pop();
    link.click();
    setTimeout(function () {
        // For Firefox it is necessary to delay revoking the ObjectURL
        window.URL.revokeObjectURL(link);
    }, 100);
};
export default {
    onGetDownloadFiles, onGetDownloadFilesAuto
};