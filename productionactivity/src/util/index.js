
  const tinyconfig = (height = 300) => {
    var toolbarSetup =
    "formatselect fontsizeselect | bold italic underline strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat undo redo | image table fullscreen charmap fontselect print";
    return {
      branding: false,
      height: height != "" ? height : 300,
      statusbar: false,
      forced_root_block: "div",
      //content_style: ".mce-content-body {font-size:16px;font-family:Arial;}",
      menubar: "file tools table format view insert edit",
      fontsize_formats:
        "8px 10px 12px 14px 16px 18px 20px 22px 24px 26px 39px 34px 38px 42px 48px",
      plugins: [
        "autolink lists image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "template textcolor colorpicker textpattern imagetools toc help emoticons hr codesample link",
      ],
      external_plugins: {
        advlist: "/static/tinymce/advlist/plugin.js",
        powerpaste: "/static/tinymce/powerpaste/plugin.js",
        nanospell: "/static/tinymce/nanospell/plugin.js",
      },
      nanospell_server: "asp",
      toolbar1:toolbarSetup,
      // paste_data_images :true,
      powerpaste_allow_local_images: true,
      powerpaste_word_import: "prompt",
      powerpaste_html_import: "prompt",
      images_upload_url: "postAcceptor.php",
      images_upload_handler: function(blobInfo, success, failure) {
          console.log(failure);
        setTimeout(function() {
          var files = blobInfo.blob();
          var reader = new FileReader();
          // Set the reader to insert images when they are loaded.
          reader.onload = function(e) {
            var result = e.target.result;
            success(result);
          };
          // Read image as base64.
          reader.readAsDataURL(files);
          //  }
        }, 2000);
      },
      init_instance_callback : function(editor) {
        editor.setContent('custom');
      },
      setup: function(ed) {
        ed.on("init", function() {
          //ed.setContent('custom');
          this.getDoc().body.style.fontSize = "16px";
          this.getDoc().body.style.fontFamily = "Arial";
          // this.editorInstance.setContent("custom");
        });
      },
    };
  };
  export default {
    tinyconfig,
  };
  