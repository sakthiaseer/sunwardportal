import axios from "axios";
import { getToken } from "@/util/auth";
//import router from '@/router/path.js';
import store from '@/store/index.js';
const service = axios.create({
  baseURL: "http://localhost:2025/api",
  //baseURL: "https://portal.sunwardpharma.com/tmsapi/api",
  //baseURL: "https://portal.sunwardpharma.com/tmsapidev/api",
  json: true,
});

// request interceptor
service.interceptors.request.use(
  (config) => {
    // Do something before request is sent
    //if (store.getters.token) {
    // 让每个请求携带token-- ['X-Token']为自定义key 请根据实际情况自行修改
    //config.headers["X-Token"] = getToken();
    config.headers["Authorization"] = "bearer " + getToken();
    //}
    return config;
  },
  (error) => {
    // Do something with request error
    //console.log(error); // for debug
    Promise.reject(error);
  }
);

// // respone interceptor
service.interceptors.response.use(
  (response) => response,
  /**
   * 下面的注释为通过在response里，自定义code来标示请求状态
   * 当code返回如下情况则说明权限有问题，登出并返回到登录页
   * 如想通过xmlhttprequest来状态码标识 逻辑可写在下面error中
   * 以下代码均为样例，请结合自生需求加以修改，若不需要，则可删除
   */
 /*  response => {
    console.log(response);
  }, */
  (error) => {
    const response = error.response;
    console.log(error);
    if (response != undefined && response.data != undefined) {
      let errormsg = response.data;
      console.log(errormsg);
      console.log(errormsg.Message);
      // this.$cookies.set('errorTrace', errormsg);
      window.getApp.$emit("APP_API_ERROR", errormsg.Message);
      window.getApp.$emit("APP_API_ERRORTRACE", errormsg);
      // Message({ message: errormsg.Message, type: 'error', duration: 5 * 1000 });
      //throw error;
      return Promise.reject(errormsg.Message);
    } else {
      return Promise.reject(response);
    }
  }
);

// Add a response interceptor
// service.interceptors.response.use(
//   response => {
//     // Do something with response data
//     return response;
//   },
//   error => {
//     console.log(error);
//     // Do something with response error
//     return Promise.reject(error);
//   }
// );

export default {
  execute(method, resource, data) {
    // inject the accessToken for each request
    return service({
      method,
      url: resource,
      data,
      validateStatus: (status) => {
        return status < 500;
      },
      onUploadProgress: (event) => {
        var percentCompleted = Math.round((event.loaded * 100) / event.total);
        console.log(percentCompleted, resource, event);
        if (
          resource == "/FileProfileType/CreateDocument" ||
          resource == "/FileProfileType/UploadDocument"
        ) {
          //console.log(percentCompleted, resource, event);
          window.getApp.$emit("progress", percentCompleted);
        }
      },
    })
      .then((req) => {
        if (req.status == 401) {
         // store.dispatch('logout');
         // console.log(location.href);
          //window.location.href  = location.href;
         // route.push('/')
         store
          .dispatch("logout", store.state)
          .then(() =>location.reload())
          .catch((err) => console.log(err));
        }
        else {
          return req;
        }
      })
      .catch((error) => {
        console.log(method + "--" + resource);
        window.getApp.$emit("APP_API_ROOT", method + "--" + resource);
        console.log(error);
        // this.$cookies.set('erroRoot', method + "--" + resource);
        // this.$cookies.set('errormessage', error);

        // if (error != undefined) {
        //   window.getApp.$emit("APP_API_ERROR", error);
        // }
        //throw error;
      });
  },
  downloadFile(method, resource, data) {
    // inject the accessToken for each request
    return service({
      method,
      url: resource,
      data,
      responseType: "blob",
      validateStatus: (status) => {
        if (status == 500) {
          return false;
        } else {
          return true;
        }
      },
    })
      .then((req) => {
        return req;
      })
      .catch((error) => {
        //throw error;
        console.log(method + "--" + resource);
        window.getApp.$emit("APP_API_ROOT", method + "--" + resource);
        console.log(error);
      });
  },
  getAll(controllerName) {
    return this.execute("get", controllerName);
  },
  getByRefNo(refNo, controllerName) {
    return this.execute("get", `/` + controllerName + `/?refNo=${refNo}`);
  },
  getByID(id, controllerName) {
    return this.execute("get", `/` + controllerName + `/?id=${id}`);
  },
  getGenericCodeByCompanyID(id, countryId, controllerName) {
    return this.execute(
      "get",
      `/` + controllerName + `/?id=${id}&&countryId=${countryId}`
    );
  },
  getByType(type, controllerName) {
    return this.execute("get", `/` + controllerName + `/?type=${type}`);
  },
  getItem(data, controllerName) {
    return this.execute("post", `/` + controllerName, data);
  },
  getItems(data, controllerName) {
    return this.execute("post", `/` + controllerName, data);
  },
  create(data, controllerName) {
    // data.addedByUserID = this.$store.state.auth.userId;
    // data.addedDate = new Date();
    return this.execute("post", "/" + controllerName, data);
  },
  update(id, data, controllerName) {
    // data.modifiedByUserID = 2;
    // data.modifiedDate = new Date();
    return this.execute("put", `/` + controllerName + `?id=${id}`, data);
  },
  delete(id, controllerName) {
    return this.execute("delete", `/` + controllerName + `?id=${id}`);
  },
  uploadFile(data, controllerName) {
    return this.execute("post", "/" + controllerName, data);
  },
  downLoadDocument(data, controllerName) {
    return this.downloadFile("get", `/` + controllerName + `/?id=${data}`);
  },
  downLoadHeaderImageDocument(data, controllerName) {
    return this.downloadFile(
      "get",
      `/` + controllerName + `/?sessionID=${data}`
    );
  },
  downLoadProfileDocument(data, controllerName) {
    return this.downloadFile("post", `/` + controllerName, data);
  },
  downLatestDocument(data, controllerName) {
    return this.execute(
      "get",
      `/` +
      controllerName +
      `/?id=${data.documentId}&&folderId=${data.folderId}&&userId=${data.userId}`
    );
  },
  getByuserId(id, userId, controllerName) {
    //console.log(this.$store.state.auth.userId);
    //let userId = 2;
    return this.execute(
      "get",
      `/` + controllerName + `/?id=${id}&&userId=${userId}`
    );
  },
  getBycompany(company, controllerName) {
    //console.log(this.$store.state.auth.userId);
    //let userId = 2;
    return this.execute("get", `/` + controllerName + `/?company=${company}`);
  },
  getSearchResult(search, userId, controllerName) {
    return this.execute(
      "get",
      `/` + controllerName + `/?search=${search}&&userId=${userId}`
    );
  },
  getByPage(pageCount, userId, controllerName) {
    return this.execute(
      "get",
      `/` + controllerName + `/?pageCount=${pageCount}&&userId=${userId}`
    );
  },
  getBySession(sessionID, userId, controllerName, type) {
    return this.execute(
      "get",
      `/` +
      controllerName +
      `/?sessionID=${sessionID}&&userId=${userId}&&type=${type}`
    );
  },
  getBySessionID(sessionID, controllerName) {
    return this.execute(
      "get",
      `/` + controllerName + `/?sessionID=${sessionID}`
    );
  },
  getByNavItem(item, controllerName) {
    return this.execute("get", `/` + controllerName + `/?itemNo=${item}`);
  },
  getByScreen(item, controllerName) {
    return this.execute("get", `/` + controllerName + `/?screenId=${item}`);
  },
  getByStrings(item, company, controllerName) {
    return this.execute(
      "get",
      `/` + controllerName + `/?itemNo=${item}&&company=${company}`
    );
  },
};
