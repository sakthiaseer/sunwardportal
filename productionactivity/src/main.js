import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import moment from "moment";
import 'font-awesome/css/font-awesome.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css' 
Vue.use(moment);
import GlobalComponents from './globalComponents';
import VueCoreVideoPlayer from 'vue-core-video-player'
Vue.config.productionTip = false;
Vue.use(GlobalComponents);
Vue.use(VueCoreVideoPlayer);
import VueQrcodeReader from "vue-qrcode-reader";
import Croppa from 'vue-croppa';
window.$ = require('jquery')
window.JQuery = require('jquery')
import VueMce from './vue-mce'

if (window) {
  window.VueMce = VueMce

  if (window.Vue) {
    window.Vue.use(VueMce)
  }
}

Vue.use(VueMce);
Vue.use(Croppa);
Vue.use(VueQrcodeReader);
Vue.use(require('vue-shortkey'))
Vue.filter("formatDate", function (value) {
  if (value) {
    return moment(value).format("DD-MMM-YYYY");
  }
});
Vue.filter("monthYear", function (value) {
  if (value) {
    return moment(value).format("MM-YYYY");
  }
});
Vue.filter('formatTime', function (value) {
  if (value) {
    return moment(value).format('hh:mm A');
  }
});
Vue.filter('formatDateTime', function (value) {
  if (value) {
    return moment(value).format('DD-MMM-YYYY hh:mm A');
  }
});
Vue.filter('formatDateTimeView', function (value) {
  if (value) {
    return moment(value).format('DD-MMM-YY HH:mm');
  }
});
Vue.filter("formatDateView", function (value) {
  if (value) {
    return moment(value).format("DD/MM/YYYY");
  }
});
new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
