import Vue from 'vue'
import Vuex from 'vuex'
import { authenticate } from "@/api/applicationsUsers";
import createPersistedState from 'vuex-persistedstate'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userId: null,
    userName: "",
    sessionId: null,
    isLogin: false,
    status: "",
    token: "",
    sidebar_drawer: null,
    SidebarColor: 'white'
  },
  mutations: {
    auth_request(state) {
      state.status = 'loading'
    },
    auth_success(state, token) {
      state.status = 'success'
      state.token = token
      state.isLogin = true
      state.userId = null
      state.userName = ''
      state.sessionId = null
      state.sidebar_drawer = null
      state.SidebarColor = 'white'
    },
    auth_error(state) {
      state.status = 'error'
    },
    logout(state) {
      state.status = ''
      state.token = ''
      state.isLogin = false
      state.sessionId = null
      state.userId = null
      state.userName = ''
      state.sidebar_drawer = null
      state.SidebarColor = 'white'
    },
    SET_USER_ID(state, payload) {
      state.userId = payload
    },
    SET_USER_NAME(state, payload) {
      state.userName = payload
    },
    SET_SESSIONID(state, payload) {
      state.sessionId = payload
    },
    SET_SIDEBAR_COLOR(state, payload) {
      state.SidebarColor = payload
    },
    SET_SIDEBAR_DRAWER(state, payload) {
      state.sidebar_drawer = payload
    },
  },
  actions: {
    login({ commit }, user) {
      return new Promise((resolve, reject) => {
        commit('auth_request')

        authenticate(user.payload)
          .then(resp => {
            if (resp.data.userId != null) {
              const token = resp.data.token
              const user = resp.data;
              localStorage.setItem('token', token)
              commit('auth_success', token)
              commit('SET_USER_ID', user.userId)
              commit('SET_USER_NAME', user.userName)
              commit('SET_SESSIONID', user.sessionId)
              resolve(resp)
            }
            else {
              commit('auth_error')
              localStorage.removeItem('token')
              resolve(resp)
            }
          })
          .catch(err => {
            commit('auth_error')
            localStorage.removeItem('token')
            reject(err)
          })
      })
    },
    logout({ commit }) {
      return new Promise((resolve) => {
        commit('logout')
        localStorage.removeItem('token')
        window.localStorage.clear();
        window.localStorage.removeItem('vuex');
        window.localStorage.clear();
        resolve()
      })
    }
  },
  plugins: [createPersistedState({
    key: '@(&(&(*$%45793459djfdfg^%*&(*r44',
    storage: window.localStorage
  })],
  modules: {
  }
})
