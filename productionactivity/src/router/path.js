import LoginPage from '../components/Login.vue'
import Home from '../views/Home.vue'
import Report from '../views/ProductionActivityReport.vue'
import ProductionActivityRoutineReport from '../views/ProductionActivityRoutineReport.vue'
import ProductionActivityRoutine from '../components/ProductionActivityRoutine.vue'
import Dashboard from '../views/Dashboard.vue'
export default [
    {
        path: '/',
        component: LoginPage,
        meta: {
            public: true,
        },
    },
    {
        path: '/productionActivity/productionActivity',
        name: 'productionActivity',
        component: Home,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/productionActivity/productionActivityReport',
        name: 'productionActivityReport',
        component: Report,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/productionRoutine/productionRoutineReport',
        name: 'productionRoutineReport',
        component: ProductionActivityRoutineReport,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/productionRoutine/productionRoutine',
        name: 'productionRoutine',
        component: ProductionActivityRoutine,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/productionPlanning/productionPlanning',
        name: 'productionPlanning',
        component: () =>
            import(`../views/productionPlanning/productionPlanning.vue`),
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/productionPlanning/productionPlanningReport',
        name: 'productionPlanningReport',
        component: () =>
            import(`../views/productionPlanning/productionPlanningReport.vue`),
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: "/video",
        name: "video",
        component: () =>
            import(`@/components/video/videos.vue`),
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: "/ipir/ipirReportsin",
        name: "ipirReportsin",
        component: () =>
            import(`../views/IpirReporting.vue`),
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: "/ipir/ipiractionsin",
        name: "ipiractionsin",
        component: () =>
            import(`../views/IpirReportsin.vue`),
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: "/ipir/ipirReportmal",
        name: "ipirReportmal",
        component: () =>
            import(`../views/IpirReporting.vue`),
        meta: {
            requiresAuth: true,
        },
    },
    
    {
        path: "/ipir/ipiractionmal",
        name: "ipiractionmal",
        component: () =>
            import(`../views/IpirReportmal.vue`),
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: "/master/masterActivity",
        name: "master",
        component: () =>
            import(`../views/ProductionActivityMaster/ProductionActivityMaster.vue`),
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: "/masterReport/masterReport",
        name: "masterReport",
        component: () =>
            import(`../views/ProductionActivityMaster/ProductionActivityMasterReport.vue`),
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: "/upload",
        name: "upload",
        component: () =>
            import(`../views/upload.vue`),
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: "/productionActivityRoutine",
        name: "productionActivityRoutine",
        component: () =>
            import(`@/components/ProductionActivityRoutine.vue`),
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: "/filevideo",
        name: "filevideo",
        component: () =>
            import(`@/components/video/fileVideo.vue`),
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: "/filePathVideo",
        name: "filePathVideo",
        component: () =>
            import(`@/components/video/filePathVideo.vue`),
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: "/uploads",
        name: "uploads",
        component: () =>
            import(`../views/uploads.vue`),
        meta: {
            requiresAuth: true,
        },
    },
];