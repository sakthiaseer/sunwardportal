import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from '@/router/path.js';
import store from '@/store/index.js';
Vue.use(VueRouter)
const router = new VueRouter({
  //mode: 'history',
  base: process.env.BASE_URL,
  routes
});
let entryUrl = null;
router.beforeEach((to, from, next) => {
  console.log(to);
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.state.isLogin) {
      if (entryUrl) {
        const url = entryUrl;
        entryUrl = null;
        next(url);
        return;
      }
      next();
      return;
    }
    entryUrl = to.fullPath; // store entry url before redirect
    next('/');
  } else {
    next();
  }
})
export default router
