import DeleteConfirmationDialog from "@/components/DeleteConfirmationDialog/DeleteConfirmationDialog";
import ConfirmationDialog from "@/components/ConfirmationDialog/ConfirmationDialog";
import AppSectionLoader from "@/components/AppSectionLoader/AppSectionLoader";
const GlobalComponents = {
    install(Vue) {
        Vue.component('deleteConfirmationDialog', DeleteConfirmationDialog);
        Vue.component('confirmationDialog', ConfirmationDialog);
        Vue.component('appSectionLoader', AppSectionLoader);
    }
};
export default GlobalComponents;