import component from '@/components/tinymce/index.vue'

const VueMce = {
  component,

  install(vm) {
    vm.component('vue-mce', component)
  },
}

export { component }

export default VueMce
