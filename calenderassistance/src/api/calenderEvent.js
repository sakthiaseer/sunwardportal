import myApi from '@/util/api'

export function getCompanyCalendarLineAssistance(data) {

    return myApi.getItem(data, 'CompanyCalendarLine/GetCompanyCalendarLineAssistance')
}
export function getApplicationUsersBySession(data) {

    return myApi.getAll('ApplicationUser/GetApplicationUsersBySession?sessionId=' + data)
}
export function getApplicationMasterDetailByType(type) {
    return myApi.getByType(type, "ApplicationMaster/GetApplicationMasterDetailByType")
}
export function updateCompanyCalendarLine(data) {

    return myApi.update(data.companyCalendarLineId, data, "companyCalendarLine/UpdateCompanyCalendarEventLine")
}
export function getDocumentsBySessionID(sessionId, userId) {

    return myApi.getAll('Documents/GetDocumentsBySessionID/?sessionID=' + sessionId + '&&userId=' + userId + '&&type=')
}
export function downloadFile(data) {
    return myApi.downLoadDocument(data, "Download/DownLoadDocument");
}
export function getFileProfileTypeDetailsDropDown(id) {
    return myApi.getByID(id, "FileProfileType/GetFileProfileTypeDetailsDropDown")
}
export function uploadFiles(data, controller, action) {
    if (action != null) {
        let url = controller + "/" + action;
        return myApi.uploadFile(data, url);
    }
    else {
        return myApi.uploadFile(data, "Documents/UploadDocuments");
    }
}
export function deleteFiles(documentId, controller, action) {
    if (action != null) {
        let url = controller + "/" + action;
        return myApi.delete(documentId, url);
    }
    else {
        return myApi.delete(documentId, "Documents/DeleteDocuments");
    }
}
export function uploadDocument(data) {

    return myApi.uploadFile(data, "FileProfileType/UploadDocument")
}
export function getFileProfileTypeDocumentByHistory(data) {

    return myApi.getItem(data, "FileProfileType/GetFileProfileTypeDocumentByHistory")
}
export function getDocumentCheckIn(id,userId) {
    return myApi.getAll("Documents/DocumentCheckIn?id=" + id+ "&&userId=" + userId);
}
export function getApplicationUsers() {

    return myApi.getAll('ApplicationUser/GetApplicationUsers')
}
export function getUserGroups() {

    return myApi.getAll('UserGroup/GetUserGroups')
}
export function getNotifyDocumentById(id) {

    return myApi.getByID(id, "NotifyDocument/GetNotifyDocumentById")
}
export function createNotifyDocument(data) {
    return myApi.create(data, "NotifyDocument/InsertNotifyDocument")
}
export function updatecloseDocuments(data) {

    return myApi.getItem(data, "FileProfileType/UpdatecloseDocuments")
}
export function getDocumentTypes() {
    return myApi.getAll('Documents/GetDocumentTypes')
}
export function downloadFiles(data) {

    return myApi.downLoadProfileDocument(data, "FileProfileType/DownLoadDocument")
}
export function getFileProfilePathAll() {
    return myApi.getAll("FileProfileType/GetFileProfilePathAll")
}
export function downloadFileDoc(data) {

    return myApi.downLoadProfileDocument(data, "Documents/ConvertDocumentWordToPdf")
}
export function getPublicFolderMainPathAll(userId) {

    return myApi.getItem(userId, 'Folders/GetPublicFolderMainPathAll')
}
export function viewFile(data) {
    return myApi.getItem(data, "Documents/ViewDocument");
}
export function getFileProfileTypeDocument(id, userId) {
    return myApi.getByuserId(id, userId, "FileProfileType/GetFileProfileTypeDocument")
}
export function createLinkFileProfileType(data) {
    return myApi.create(data, "Documents/InsertLinkFileProfileType")
}
export function getParentPublicFoldersList(userId) {
    return myApi.getByID(userId, 'Folders/GetParentPublicFolders')
}
export function getPublicFolderPath(id, userId) {

    return myApi.getByuserId(id, userId, 'Folders/GetPublicFolderPath')
}
export function getDocumentsByFolderID(id, userId) {

    return myApi.getByuserId(id, userId, 'Folders/GetDocumentsByFolderID')
}
export function updateDocumentIsPrint(data) {

    return myApi.update(data.documentID, data, "FileProfileType/updateDocumentIsPrint")
}
export function insertCompanyCalendarLineLinkPrint(data) {

    return myApi.create(data, "companyCalendarLine/InsertCompanyCalendarLineLinkPrint")
}
export function getCompanyCalendarLineLinkAll(id, sessionId, userId) {
    return myApi.getAll('companyCalendarLine/getCompanyCalendarLineLinkAll/?sessionID=' + sessionId + '&&Id=' + id + '&&userId=' + userId)
}

export function createCompanyCalendarLineMeetingNotes(data) {
    return myApi.create(data, "companyCalendarLine/InsertCompanyCalendarLineMeetingNotes")
}
export function getCompanyCalendarLineMeetingNotes(userId) {
    return myApi.getByID(userId, 'companyCalendarLine/GetCompanyCalendarLineMeetingNotes')
}
export function getPlants() {

    return myApi.getAll('Plant/GetPlants')
}
export function updatecompanyCalendarLinePrintNotes(data) {

    return myApi.update(data.companyCalendarLineLinkId, data, "companyCalendarLine/UpdatecompanyCalendarLinePrintNotes")
}
export function getFileProfileTypeListDropdownItems(id) {

    return myApi.getByID(id, 'FileProfileType/GetFileProfileTypeTreeDropDown')
}

export function updateCompanyCalendarLineMeetingNotes(data) {

    return myApi.update(data.companyCalendarLineMeetingNotesId, data, "companyCalendarLine/UpdateCompanyCalendarLineMeetingNotes")
}

export function getDocumentPermissionData(id, userId) {

    return myApi.getByuserId(id, userId, 'FileProfileType/GetDocumentPermissionData')
}
export function getLongNotesByNotifyId(id) {

    return myApi.getByID(id, "NotifyDocument/GetLongNotesByNotifyId")
}

export function updateNotifyDocument(data) {
    return myApi.update(data.notifyDocumentId, data, "NotifyDocument/UpdateNotifyDocument")
}
export function createNotifylongNotes(data) {
    return myApi.update(data.notifyDocumentId, data, "NotifyDocument/CreateNotifylongNotes")
}
export function getParentDocumentsByLinkDocumentId(id) {

    return myApi.getByID(id, "DocumentLink/GetParentDocumentsByLinkDocumentId")
}

export function createDocumentLink(data) {

    return myApi.create(data, "DocumentLink/InsertDocumentLink")
}
export function updateDocumentLink(data) {

    return myApi.update(data.documentLinkId, data, "DocumentLink/UpdateDocumentLink")
}