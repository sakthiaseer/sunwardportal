import myApi from '@/util/api'
export function getCompanyCalendarLine (id) {

    return myApi.getByID(id, "companyCalendarLine/GetCompanyCalendarLine")
}
export function createCompanyCalendarLine (data) {
    return myApi.create(data, "companyCalendarLine/InsertCompanyCalendarLine")
}

export function updateCompanyCalendarLine (data) {

    return myApi.update(data.companyCalendarLineId, data, "companyCalendarLine/UpdateCompanyCalendarLine")
}
export function deleteCompanyCalendarLine (data) {

    return myApi.delete(data.companyCalendarLineId, 'companyCalendarLine/DeleteCompanyCalendarLine')
}


export function getCalandarDiscussions (id) {

    return myApi.getByID(id, "companyCalendarLine/GetCalandarDiscussions")
}
export function createCompanyCalendarLineNotes (data) {
    return myApi.create(data, "companyCalendarLine/InsertCompanyCalendarLineNotes")
}

export function updateCompanyCalendarLineNotes (data) {

    return myApi.update(data.companyCalendarLineNotesId, data, "companyCalendarLine/UpdateCompanyCalendarLineNotes")
}
export function deleteCompanyCalendarLineNotes (data) {

    return myApi.delete(data.companyCalendarLineNotesId, 'companyCalendarLine/DeleteCompanyCalendarLineNotes')
}

export function getCompanyCalendarLineLink (id) {

    return myApi.getByID(id, "companyCalendarLine/GetCompanyCalendarLineLink")
}
export function createCompanyCalendarLineLink (data) {
    return myApi.create(data, "companyCalendarLine/InsertCompanyCalendarLineLink")
}

export function updateCompanyCalendarLineLinks (data) {

    return myApi.update(data.companyCalendarLineLinkId, data, "companyCalendarLine/UpdateCompanyCalendarLineLinks")
}
export function deleteCompanyCalendarLineLink (data) {

    return myApi.delete(data.companyCalendarLineLinkId, 'companyCalendarLine/DeleteCompanyCalendarLineLink')
}