import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import moment from "moment";
import GlobalComponents from './globalComponents';
Vue.use(moment);
import VueCoreVideoPlayer from 'vue-core-video-player'
import VueMce from './util/vue-mce'

 if (window) {
  window.VueMce = VueMce

  if (window.Vue) {
    window.Vue.use(VueMce)
  }
}

Vue.use(VueMce); 
Vue.use(VueCoreVideoPlayer)
Vue.use(GlobalComponents);
Vue.config.productionTip = false
Vue.filter("formatDate", function (value) {
  if (value) {
    return moment(value).format("DD-MMM-YYYY");
  }
});
Vue.filter("monthYear", function (value) {
  if (value) {
    return moment(value).format("MM-YYYY");
  }
});
Vue.filter('formatTime', function (value) {
  if (value) {
    return moment(value).format('hh:mm A');
  }
});
Vue.filter('formatDateTime', function (value) {
  if (value) {
    return moment(value).format('DD-MMM-YYYY hh:mm A');
  }
});
new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
